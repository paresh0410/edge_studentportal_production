export const feature = {
  webinar: {
    'jisti': false,
    'openvidu': true,
  },
  activity: {
    'standardui': false, // OLD UI
    'modernui': true,   // Modern UI
  },
  catgoryDropDown: {
    // 'Bajaj': false,
    // 'other': true,
    'show': false,
  },
  wishlist: {
    'show': false,
    // 'Bajaj': false,
    // 'other': true,
  },
  footer: {
    'Bajaj': false,
    'other': false,
  },
  brandName: 'ILLUME',
  allowDynamicDbLogoImage: false,
  pageTitle: 'ILLUME',
  faviconPNG: '',
  faviconXICON: '',
};
