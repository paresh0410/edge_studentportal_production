import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { CommonModule } from "@angular/common";
import { NgModule, ApplicationRef, ErrorHandler, ApplicationInitStatus } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { SignupComponent } from './pages/signup/signup.component';
import { OtpComponent } from './pages/otp/otp.component';
import { WelcomePageComponent } from './pages/welcome-Page/welcome-page.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ResetPwdComponent } from './pages/reset-pwd/reset-pwd.component';
import { InterestsComponent } from './pages/interests/interests.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';


// import { AppComponent } from './app.component';
import { AppComponent } from './app-new.component';

import { IndexComponent } from './index/index.component';
import { MsalGuard } from '@azure/msal-angular'
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';
import { from } from 'rxjs';
import { LoginNewComponent } from './pages/login-new/login-new.component';

const routes: Routes = [
  { path: '', component: AppComponent, pathMatch:'full',},
  { path: 'index', component: IndexComponent},

  { path: 'logout', component: LogoutComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'otp', component: OtpComponent},
  { path: 'welcome', component: WelcomePageComponent},
  { path: 'forgotPass', component: ForgotPasswordComponent},
  { path: 'resetPass', component: ResetPwdComponent},
  { path: 'interests', component: InterestsComponent},
  { path: 'privacy', component: PrivacyComponent},
  { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule' }, //canActivate : [MsalGuard]
  // {
  //   path: 'module-activity',
  //   loadChildren : 'app/pages/lazy/lazy.module#LazyModule'
  //   // loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
  //   // loadChildren: './module-activity-module/module-activity.module#ModuleActivityModule',
  // },

  // {
  //   path: 'auth',
  //   component: NbAuthComponent,
  //   children: [
  //     {
  //       path: '',
  //       component: NbLoginComponent,
  //     },
  //     {
  //       path: 'login',
  //       component: NbLoginComponent,
  //     },
  //     {
  //       path: 'register',
  //       component: NbRegisterComponent,
  //     },
  //     {
  //       path: 'logout',
  //       component: NbLogoutComponent,
  //     },
  //     {
  //       path: 'request-password',
  //       component: NbRequestPasswordComponent,
  //     },
  //     {
  //       path: 'reset-password',
  //       component: NbResetPasswordComponent,
  //     },
  //   ],
  // },

  // { path: '', redirectTo: 'pages', pathMatch: 'full' },
  // { path: '**', redirectTo: 'pages' },

  // { path: '', redirectTo: 'index', pathMatch: 'full' },
  // { path: '**', redirectTo: 'index' },


  // { path: 'login', component: LoginComponent},
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: '**', redirectTo: 'login' },

  { path: 'login-new', component: LoginNewComponent},
  { path: '', redirectTo: 'login-new', pathMatch: 'full' },
  { path: '**', redirectTo: 'login-new' },
];

const config: ExtraOptions = {
  useHash: true,
  onSameUrlNavigation: 'reload',
};

@NgModule({
  imports: [
    RouterModule.forRoot(routes, config),
    // RouterModule.forRoot(routes, {
    //   onSameUrlNavigation: 'reload',
    // }),
  ],
  exports: [RouterModule],
  // providers: [ApplicationRef, ErrorHandler, ApplicationInitStatus]
})

export class AppRoutingModule {
}
