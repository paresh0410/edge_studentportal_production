import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
  AfterViewInit,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute , NavigationEnd} from '@angular/router';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { LearnSearchServiceProvider } from '../../../service/learn-search.service';
import { NotificationServiceProvider } from '../../../service/notification.service';
import { ENUM } from '../../../service/enum';
import { ENotificationRedirectPage } from '../../../service/notification.enum';
import { LearnServiceProvider } from '../../../service/learn-service';
import html2canvas from 'html2canvas';
import { BrandDetailsService } from '../../../service/brand-details.service';
import { feature } from '../../../../environments/feature-environment';
import { ToastrService } from 'ngx-toastr';
import { CatalogueService } from '../../../service/learn-container/catalogue.service';
import { AuthServiceProvider } from '../../../service/auth-service';
import { EventsService } from '../../../service/events.service';
import { MultiLanguageService } from '../../../service/multi-language.service';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() position = 'normal';
  @ViewChild('list', { read: TemplateRef }) templateList: TemplateRef<any>;
  featureConfig = feature;
  showSendFeedback: boolean = false;
  imageSrc: string = '';
  user: any;
  showfaqs: boolean;
  userDetails: any;
  unReadMessageCount: number;
  currentBrandData: any;
  toggelLanguageMenu = false;
  disabledLanguageChange = ['/pages/module-activity',
  'pages/call-coaching-list/course',
  'pages/nomination/course',
  'pages/trainer-dashboard/trainer-details/course'];
  userMenu = [
    { title: 'Profile', link: '../pages/profile' },
    { title: 'Help', data: { id: 'help' } },
    { title: 'Feedback', data: { id: 'feedback' } },
    { title: 'Log out', link: '../logout' },
  ];

  notificationMenu: any = [
    // { title: 'Demo Testing 1', message: 'Demo Testing Message' },
    // { title: 'Demo Testing 2', message: 'Demo Testing Message For Course' },
    // { title: 'Demo Testing 3', message: 'Demo Testing Message For Pulse' },
    // { title: 'Demo Testing 4', message: 'Demo Testing Message For Activity' },
  ];
  theme: any;

  catalogueCategories: any = [];

  innerItem: any = [];
  levelUp: boolean = false;
  parentCat: any = {};

  currentUserData: any;
  showOptions: boolean = true;

  searchTerm: any = '';
  languageList = [];
  defaultLangugae = {
    languageId: null,
    languageName : 'Select Language',
    // languageCode : null,
  };
  languageChangeSubscriber = null;
  private _routerSub = Subscription.EMPTY;
  disableLanguageChange = false;
  hideflag: any = true;
  innerArray: any = [];
  catIdArray: any = [];
  public searchControl: FormControl;
  selectedCatArr: any = [];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    public userService: UserService,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private NSP: NotificationServiceProvider,
    private LSSP: LearnSearchServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    public LSP: LearnServiceProvider,
    public brandService: BrandDetailsService,
    private toastr: ToastrService,
    public catalogueService: CatalogueService,
    public ASP: AuthServiceProvider,
    public events: EventsService,
    public multiLanguageService: MultiLanguageService,
    public cdf: ChangeDetectorRef,
  ) {
    this.languageChangeSubscriber = multiLanguageService.selectedLanguage;
    this.languageChangeSubscriber.subscribe(selectedLanguage => {
      //do what ever needs doing when data changes
      console.log('selectedLanguage', selectedLanguage);
      this.defaultLangugae = selectedLanguage;
    });

    this._routerSub = this.router.events
    .filter(event => event instanceof NavigationEnd)
    .subscribe((value) => {
       //do something with the value
       console.log('_routerSub', value);
       if(this.disabledLanguageChange.includes(value['urlAfterRedirects'])){
        this.disableLanguageChange = true;
        this.cdf.markForCheck();
       }else {
        this.disableLanguageChange = false;
        this.cdf.markForCheck();
       }
   });
   this.searchControl = new FormControl();
  }

  ngOnInit() {
    this.getUserData();
    this.user = this.userService.getUsers();
    this.currentBrandData = this.brandService.getCurrentBrandData();
    console.log('User Data =======>', this.user);
    this.sidebarService.collapse('menu-sidebar');
    this.catalogueCategories = this.catalogueService.getCategories();
    console.log('this.catalogueCategories', this.catalogueCategories);
    this.routes.params.subscribe(routeParams => {
      console.log('These are the route params your looking for ', routeParams);
    });
    this.subscribeEvent();
    this.initFormControl();
  }

  ngAfterViewInit() {
    // this.notificationMenu = PopoverListComponent
    if (this.userDetails) {
      // this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
      const url = ENUM.domain + ENUM.url.getNotification;
      const param = {
        userId: this.userDetails.id,
        pageNo: null,
        tenantId: this.userDetails.tenantId,
        currentRole: this.userDetails.roleId,
      };
      this.NSP.callApi(url, param)
        .then((result: any) => {
          console.log('TEMPLATELIST--->', result);
          this.notificationMenu = result.data;
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });

      this.getLanguageData();
    }
    this.menuService.onItemClick().pipe().subscribe((item) => {
      // console.log(item);
      if (item.item.data) {
        if (item.item.data.id === 'help') {
          console.log('Help Method');
          // this.logout();
          this.showfaqs = true;
        }
        if (item.item.data.id === 'feedback') {
          console.log('feedback Method');
          setTimeout(() => {
            this.takeScreenCapture();
          }, 1000);
        }

      }
    });
    // console.log('TEMPLATELIST--->', this.templateList);
    this.userService.data.subscribe(data => {
      //do what ever needs doing when data changes
      console.log('Data Changed ==>', data);
      this.user = this.userService.getUsers();
      console.log('Data Picture ==>', this.user);
    });
    this.getUnreadPushNotificationsData();

    // const searchStr = this.routes.snapshot.paramMap.get('search');
    // console.log('These are the route params your looking for ', searchStr);
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
      this.userDetails['eid'] = JSON.parse(localStorage.getItem('employeeId'));
      this.showOptions = true;
    } else {
      console.log('User not logged in...');
      const teantData =  this.ASP.getDomainBasedTenant();
      console.log('DATA--->', teantData);
      // if (teantData.tenantId) {
      //   this.showOptions = true;
      // } else {
      //   this.showOptions = false;
      // }
      this.showOptions = false;
    }
  }

  subscribeEvent() {
    this.events.subscribe('learn:search', (result: any) => {
      console.log('Learn search term is', result);
      if (result.status) {
        this.searchTerm = result.data;
      } else {
        // this.searchTerm = '';
        this.goBack();
      }
    });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  initFormControl() {
    this.searchControl.valueChanges.pipe(debounceTime(700)).subscribe(search => {
      // this.setFilteredItems();
      console.log('Search term : ', search);
      this.searchTerm = search;
      if (this.searchTerm.length >= 3) {
        // this.searching = true;
        this.searchOnPage(search);
      } else if (this.searchTerm.length == 0) {
        // this.searching = true;
        // this.searchOnPage(search);
        this.goBack();
      }
      // this.searchOnPage(search);
    });
  }

  clearSearch() {
    // this.searchTerm = '';
    // // this.searchOnPage(this.searchTerm);
    this.setValueForSearch("", { emitEvent: false });
    this.goBack();
  }

  goBack(){
    // window.history.back();
    const url = localStorage.getItem('searchTriggredFrom');
    if(url){
      this.router.navigateByUrl(url);
    }else {
      window.history.back();
    }

  }
  setValueForSearch(value, obj){
    this.searchControl.setValue(value, obj);
  }
  // searchOnPage(event) {
  //   // this.LSSP.updatedDataSelection('asdasd');
  //   // this.searchTerm = '';
  //   // event.target.value = '';

  //   // console.log('SEARCH==>', event.target.value);
  //   // const term = event.target.value;

  //   // this.searchTerm = term;

  //   const term = this.searchTerm;

  //   console.log('Current route is : ', this.router.url);
  //   const str = this.router.url;
  //   const catalogue = str.search('catalogue');
  //   const course = str.search('course');
  //   const pathway = str.search('pathway');
  //   const open = str.search('self');
  //   const wishlist = str.search('wishlist');
  //   if (catalogue !== -1) {
  //     const m = str.search('category');
  //     if (m !== -1) {
  //       const catId = this.catalogueService.currentCatId;
  //       this.router.navigate(['./learning/catalogue/category/' + catId + '/' + term], { relativeTo: this.routes });
  //     } else {
  //       this.router.navigate(['./learning/catalogue/' + term], { relativeTo: this.routes });
  //     }
  //   } else if (course !== -1) {
  //     // const m = str.search('category');
  //     // if (m !== -1) {
  //     //   const catId = this.catalogueService.currentCatId;
  //     //   this.router.navigate(['./learning/catalogue/category/' + catId + '/' + term], { relativeTo: this.routes });
  //     // } else {
  //     //   this.router.navigate(['./learning/catalogue/' + term], { relativeTo: this.routes });
  //     // }
  //     this.router.navigate(['./learning/course/' + term], { relativeTo: this.routes });
  //   } else if (pathway !== -1) {
  //     this.router.navigate(['./learning/pathway/' + term], { relativeTo: this.routes });
  //   } else if (open !== -1) {
  //     this.router.navigate(['./learning/self/' + term], { relativeTo: this.routes });
  //   } else if (wishlist !== -1) {
  //     this.router.navigate(['./learning/wishlist/' + term], { relativeTo: this.routes });
  //   } else {
  //     // this.LSSP.setSearchString(term);
  //     // let num = Math.random();
  //     // this.LSSP.updatedDataSelection(num);

  //     // this.router.navigate(['./learn-search', term], {
  //     //   relativeTo: this.routes,
  //     // });
  //     this.router.navigate(['./learning/course/' + term], { relativeTo: this.routes });
  //   }

  //   // this.router.events.filter((ev: any) => ev instanceof NavigationEnd).subscribe(ev => {
  //   //   console.log('This is what your looking for ', ev.url);
  //   // });
  // }

  searchOnPage(event) {
    // this.LSSP.updatedDataSelection('asdasd');
    // this.searchTerm = '';
    // event.target.value = '';

    // console.log('SEARCH==>', event.target.value);
    // const term = event.target.value;

    // this.searchTerm = term;

    // if(this.userDetails && Number(this.userDetails['roleId']) === 8){
    //   const term = this.searchTerm;
    //   if(String(this.router.url).indexOf('learn-search-container') === -1){
    //     localStorage.setItem('searchTriggredFrom', this.router.url);
    //   }
    //   this.router.navigate(['./learning/learn-search-container/' + term], { relativeTo: this.routes });
    // }

    const userdetails = JSON.parse(localStorage.getItem('userdetail'));
    if(userdetails && Number(userdetails['roleId']) === 8) {
      const term = this.searchTerm;
      if(String(this.router.url).indexOf('learn-search-container') === -1){
        localStorage.setItem('searchTriggredFrom', this.router.url);
      }
      this.router.navigate(['./learning/learn-search-container/' + term], { relativeTo: this.routes });
    }
  }
  closeFaq() {
    this.showfaqs = false;
  }
  closeSendFeedback(){

    this.showSendFeedback = false;
  }
  takeScreenCapture(){
    html2canvas(document.body).then(canvas => {
      // this.saveAs(canvas.toDataURL('image/png'), `canvas.png`)
      console.log(canvas);
      console.log(canvas.toDataURL());
      this.imageSrc = canvas.toDataURL();
      // this.imageSrc = canvas.toDataURL('image/jpeg', 1.0)
      // console.log(canvas.toDataURL('image/jpeg', 1.0));
      // console.log(canvas.toDataURL('image/png'));
    });
    // this.imageSrc = 'https://upload.wikimedia.org/wikipedia/commons/4/4b/What_Is_URL.jpg';
    this.showSendFeedback = true;
  }
  sendFeedback(data) {
    // this.closeSendFeedback();
    if (data) {
      this.showSendFeedback = false;
    } else {
       this.showSendFeedback = true;
    }
  }

  openNotification(notificationData) {
    console.log('notificationData', notificationData);
    // const res = {
    //   'notification': {
    //     'payload': {
    //       'additionalData' : JSON.parse(notificationData.additionalData);
    //     }
    //   }
    // }
    // result['notification']['payload']['additionalData'] = ;
    if (notificationData.isRead === 0) {
      this.updateNotificationsStatus(notificationData);
    }
    this.RedirectNotification(notificationData);
  }

  updateNotificationsStatus(notificationData) {
    const params = {
      notificationId : notificationData.id,
      currentRole: this.userDetails.roleId,
    };
    const url = ENUM.domain + ENUM.url.readPushNotifications;
    this.NSP.callApi(url, params).then(
      res => {
        if (res['type'] === true) {
          console.log('Read Notifications ', res);
          // this.load.dismiss();
          notificationData.isRead = 1;
          this.getUnreadPushNotificationsData();
        } else {
          // this.load.dismiss();
          console.log('err ', res);
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
      }
    );
  }

  RedirectNotification (result) {
    try {
      let data = {};
      if (result.additionalData) {
        data = JSON.parse(result.additionalData);
      } else if (result.notification.payload.additionalData) {
        data = result.notification.payload.additionalData;
      }
      const component = data['redirect_page'] || data['component'] || data['event'];
      switch (component) {
        // case statements
        // values must be of same type of expression
        // case ENotificationRedirectPage.WEBINAR :
        //   this.webinarService.setNotification(true);
        //   this.webinarService.setWebinarInfo(data);
        //   // this.webinarService.setOpenViduInfo(this.openViduWebinarInfo);
        //   this.navCtrl.navigateForward('/webinar-chat-list');
        //     break; // break is optional
        case ENotificationRedirectPage.MODULE :
          // const navigationExtras: NavigationExtras = {
          //   queryParams: {
          //     id: 'listView'
          //   }
          // };
          if (String(data['compStatus'])) {
            if (String(data['compStatus']).toLowerCase() === 'current') {
              this.LSP.setcourseDetailList(data);
              if(this.featureConfig.activity.modernui){
                this.router.navigate(['pages/module-activity']);
              }
              if(this.featureConfig.activity.standardui){
                this.router.navigate(['pages/course-detail']);
              }
              this.LSP.subject.next(Math.random());
            } else {
              this.router.navigate(['./learn'], { relativeTo: this.routes });
            }
          } else {
            this.router.navigate(['./learn'], { relativeTo: this.routes });
          }
        break;
        case ENotificationRedirectPage.COURSE :
        // Statement
            this.router.navigate(['./learn'], { relativeTo: this.routes });
        break;
        case ENotificationRedirectPage.POLL :
        // Statement
        break;
        case ENotificationRedirectPage.SURVEY :
        // Statement
        break;
        case ENotificationRedirectPage.NEWS_AND_ANNOUNCEMENT :
        // Statement
        break;
        default :
          // console.log('stop');
          this.router.navigate(['dashboard'], { relativeTo: this.routes });
      }
      if (result.action) {

      }
    } catch (e) {

    }
  }

  getUnreadPushNotificationsData() {
      // const userDetails = JSON.parse(localStorage.getItem('currentUser'));
        const url = ENUM.domain + ENUM.url.get_unread_push_notifications_count;
        // const userID = userDetails.id;
        const params = {
          userId : this.userDetails.id,
          currentRole : this.userDetails.roleId,
          tenantId : this.userDetails.tenantId,
        };

        this.NSP.callApi(url,params).then(
          res => {
            if (res['type'] === true) {
              console.log('UnRead Notifications res :', res);
              // this.load.dismiss();
              // this.notificationService.unReadCount = res.data[0].all_unread;
              // this.events.publish('user:getPushNotificationsCount', true);
              this.unReadMessageCount = res['data'][0].all_unread;
            } else {
              // this.load.dismiss();
              console.log('err ', res);
            }
          },
          err => {
            // this.load.dismiss();
            console.log(err);
          }
        );


  }

  loadNext(){
    console.log('data end');
  }
  themebind() {
    const themejson = localStorage.getItem('theme');
    if (themejson) this.theme = JSON.parse(themejson);
    if (this.theme && this.theme.logo) {
      return this.theme.logo;
    } else {
      return this.currentBrandData.logo;
    }
  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = this.currentBrandData.logo;
 }

  /**************************** ****************************/

  innnerTag(item, levelUp, index) {
    console.log('showitemn', item);
    if (String(index) === '0') {
      this.innerArray = []; this.parentCat = {}; this.catIdArray = []; this.selectedCatArr = [];
    }
    if (item.categoryId !== this.parentCat.categoryId && item.children.length > 0) {
      // this.innerArray.push(item);
      if (this.catIdArray.includes(item.categoryId)) {
        console.log('includes', this.catIdArray);
      } else {
        this.innerArray.push(item);
        this.catIdArray.push(item.categoryId);
        this.selectedCatArr.push(item);
        console.log('catIDDD', item.categoryId);
        console.log('catIdArray push', this.catIdArray);
      }
    }
    this.parentCat = item;
    this.innerItem = item.children;
    this.levelUp = true;
    console.log('showitemn1', this.innerArray);
  }

  removeInner(cat) {
    console.log('cat', cat);
    for(let i = 0; i < this.innerArray.legth; i++) {
      if(cat.categoryId === this.innerArray[i].categoryId) {
        this.innerArray.splice(i, 1);
      }
    }
    // for(let i = 0; i < this.catIdArray.legth; i++) {
    //   if(cat.categoryId === this.catIdArray[i]) {
    //     this.catIdArray.splice(i, 1);
    //     console.log('removecsat', this.catIdArray);
    //   }
    // }
    this.catIdArray = this.catIdArray.filter(function(item) {
      return item !== cat.categoryId
    });
    console.log('removecsat', this.catIdArray);
  }

  goToCategory(cat, subCat) {

    console.log(cat, ' Category selected!');

    console.log('Current route is : ', this.router.url);
    const currentRoute = this.router.url;
    const catalogue = currentRoute.search('catalogue');
    if (catalogue !== -1) {
      console.log('no need to reset breadcrumbs... ');
    } else {
      this.catalogueService.resetMainBreadcrumbs();
    }

    // this.catalogueService.resetMainBreadcrumbs();

    const mainCat = {
      categoryId: 0,
      categoryName: 'catalogue',
      visible: true,
      searchStr: '',
      filters: [],
      afterSearch: false,
    };
    let catId = 0;

    const breadcrumbs = {
      id: 1,
      subBreadCrumbs: [],
    };

    breadcrumbs.subBreadCrumbs.push(mainCat);

    if (subCat) {
      if (this.selectedCatArr.length > 0) {
        for (let i = 0; i < this.selectedCatArr.length; i++) {
          const parCat = this.selectedCatArr[i];
          const parentCat = this.makeBraedCrumbDataReady(parCat, true);
          breadcrumbs.subBreadCrumbs.push(parentCat);
        }
      }
      const childCat = this.makeBraedCrumbDataReady(cat, true);
      breadcrumbs.subBreadCrumbs.push(childCat);
      catId = childCat.categoryId;
      this.hideflag = true;
    } else {
      const parentCat = this.makeBraedCrumbDataReady(cat, false);
      breadcrumbs.subBreadCrumbs.push(parentCat);
      catId = parentCat.categoryId;
      this.hideflag = true;
    }

    // const newRoute = '/pages/learning/catalogue/category/' + catId;
    const currentCatId = this.catalogueService.currentCatId;
    // if (currentRoute === newRoute) {
    if (catId === currentCatId) {
      this.events.publish('breadcrumbs:update', {
        data: breadcrumbs,
        status: true,
      });
    } else {
      this.catalogueService.addSubBreadcrumbs(breadcrumbs);
      this.router.navigate(['./learning/catalogue/category/' + catId], { relativeTo: this.routes });
    }
    this.selectedCatArr = [];
  }

  makeBraedCrumbDataReady(data, directFlag) {
    const path = {
      categoryId: data.categoryId,
      categoryName: data.categoryName,
      visible: true,
      searchStr: '',
      filters: [],
      afterSearch: false,
      direct: directFlag,
    };
    return path;
  }

  goToWishList() {
    this.router.navigate(['./learning/wishlist'], { relativeTo: this.routes });
  }

  goToLogin() {
    const returnurl = window.location.hash;
    if (returnurl && returnurl !== '/' && returnurl !== '#/') {
      if (localStorage.hasOwnProperty('redirectURL') && localStorage.getItem('redirectURL') !== '/logout') {

      } else {
        localStorage.setItem('redirectURL', returnurl.replace('#', ''));
        this.router.navigate(['..' + returnurl.replace('#', '')], { relativeTo: this.routes });
      }
    }
    this.router.navigate(['../login-new']);
  }

 /**************************** ****************************/
  getLanguageData(){
    const url = ENUM.domain + ENUM.url.getMultiLanguageData;
    const param = {};
    this.multiLanguageService.callApi(url, param).then((res) => {
      console.log("multiLanguage DropDowns", res);
      if(res['type'] && res['data']){
        this.languageList = res['data'];
      }else {
        console.log('Language Data List is empty');
      }

    })
    .catch(function (err) {
      console.log('Language Data Api Error', err);
    });
  }

  updateLanguageData(item){
    // this.defaultLangugae = {
    //   languageId: item['languageId'] ? item['languageId'] : null,
    //   languageName : item['languageName'] ? item['languageName'] : null,
    //   // languageCode : null,
    // };
    // const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // userdetail.userLang['languageId'] = item['languageId'];
    // userdetail.userLang['languageName'] = item['languageName'];
    // localStorage.setItem('userdetail', JSON.stringify(userdetail));
    // this.toggelLanguageDropDown();
    // this.multiLanguageService.updateLangugae(this.defaultLangugae, true);
  }

  toggelLanguageDropDown(){
    this.toggelLanguageMenu = !this.toggelLanguageMenu;
    this.cdf.detectChanges();
  }
  ngOnDestroy(){
    // if(this.languageChangeSubscriber){
    //   this.languageChangeSubscriber.unsubscribe();
    // }
  }
  mouseEnter() {
    this.hideflag = false;
  }

  mouseLeave() {
    this.hideflag = true;
    this.innerArray = [];
    this.parentCat = {};
    this.catIdArray = [];
    this.selectedCatArr = [];
  }

  gotoProfile() {
  this.router.navigate(['pages/profile']);
  }

}
