import { Component } from '@angular/core';
import { EventsService } from '../../../service/events.service';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html',
})
export class FooterComponent {

  terms = 'https://www.edgelearning.co.in/pages/terms-of-services.html';
  policy_bfl = 'https://www.bajajfinserv.in/privacy-policy';
  policy_saas = 'https://www.edgelearning.co.in/pages/privacy-policy.html';

  helpOpened: boolean = false;

  constructor(
    public events: EventsService,
  ) {
  }

  goToTerms() {
    window.open(this.terms, "_blank");
  }

  goToPolicy() {
    window.open(this.policy_saas, "_blank");
  }

  goToHelp() {
    // window.open(this.terms, "_blank");
    this.helpOpened = !this.helpOpened;
    this.publishEvent(this.helpOpened);
  }

  publishEvent(stat) {
    this.events.publish('footer:help', {
      data: null,
      status: stat,
    });
  }
}
