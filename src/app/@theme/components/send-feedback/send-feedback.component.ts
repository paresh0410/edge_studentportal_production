import { Component, OnInit, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../../../service/enum';
// Send Feedback Service
import { SendFeedbackService } from '../../../service/send-feedback.service';

@Component({
  selector: 'ngx-send-feedback',
  templateUrl: './send-feedback.component.html',
  styleUrls: ['./send-feedback.component.scss'],
})
export class SendFeedbackComponent implements OnInit, OnChanges {
  @Input('screencapture') screencapture: string;
  @Output() feedBackSent = new EventEmitter();
  imgSrc: string = '';
  deviceInfo = null;
  message = '';
  showImage: boolean = true;
  sendFeedbackURL = ENUM.domain + ENUM.url.sendHelpFeedback;
  constructor(private deviceService: DeviceDetectorService,
    private toastr: ToastrService, private sendFeedbackService: SendFeedbackService) {
    this.preapreBrowserData();
   }

  ngOnInit() {
  }
  ngOnChanges() {
    this.imgSrc = this.screencapture;
    // console.log ('Changed Imaged ==>', this.imgSrc);
  }
  toggle(data) {
    // console.log('Toggle Event ===>', data);
    this.showImage = data.target.checked;
  }
  preapreBrowserData() {
    console.log('hello `Home` component');
    this.deviceInfo = this.deviceService.getDeviceInfo();
    const isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
    // this.deviceInfo.assign
    this.deviceInfo.isMobile = this.deviceService.isMobile();
    this.deviceInfo.isTablet = this.deviceService.isTablet();
    this.deviceInfo.isDesktopDevice = this.deviceService.isDesktop();
    console.log(this.deviceInfo);
    console.log(isMobile);  // returns if the device is a mobile device (android / iPhone / windows-phone etc)
    console.log(isTablet);  // returns if the device us a tablet (iPad etc)
    console.log(isDesktopDevice); // returns if the app is running on a Desktop browser.
  }
  sendFeedback(f) {
    // this.spinner.show();
    let currentuserData = JSON.parse(localStorage.getItem('userdetail'));
    // console.log('UserData Values ==>', currentuserData.data.data.id);
    // console.log('Form Values ==>', f.value.message);
    let data = {
      fbText: this.message,
      base64: this.imgSrc,
      fbPageURL: window.location.href,
      fbMetadata: JSON.stringify(this.deviceInfo),
      uId: currentuserData.userId,
      tenantId: currentuserData.tenantId,
      status: 1,
      resComments: '',
      resUserName: '',
      resDate: '',
      sourceId: 832,
    };
    console.log('Send Values ==>', data);
    this.sendFeedbackService.inserthelpfb(this.sendFeedbackURL, data).then(res => {
      console.log(res);
      if (res['type'] === true) {
        console.log('Data Sent ==>');
        this.feedBackSent.emit(res['type']);
        //success
        this.presentToast('success', 'Feedback sent');
      }
      // this.spinner.hide();
    }, err => {
      // this.spinner.hide();
      //error
      this.presentToast('error', '');
      console.log(err);
      this.feedBackSent.emit(false);
    });
  }
  presentToast(type, body) {
    if(type === 'success'){
      this.toastr.success(body, 'Success', {
        closeButton: false,
       });
    } else if(type === 'error'){
      // this.toastr.error('Please contact site administrator and <a (click)="giveFeedback()"> <strong> click here </strong></a> to leave your feedback.', 'Error', {
      //   timeOut: 0,
      //   closeButton: true,
      // });
      this.toastr.warning('Unable to submit feedback at this time', 'Warning', {
        closeButton: false,
        });
    } else{
      this.toastr.warning(body, 'Warning', {
        closeButton: false,
        });
    }
  }
}
