import { Component, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { delay, withLatestFrom, takeWhile } from 'rxjs/operators';


import {
	NbMediaBreakpoint,
	NbMediaBreakpointsService,
	NbMenuItem,
	NbMenuService,
	NbSidebarService,
	NbThemeService,
} from '@nebular/theme';

import { StateService } from '../../../@core/utils';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthServiceProvider } from '../../../service/auth-service';
import html2canvas from 'html2canvas';
import { Subscription } from 'rxjs';

import { feature } from '../../../../environments/feature-environment';
import { EventsService } from '../../../service/events.service';

// TODO: move layouts into the framework
@Component({
	selector: 'ngx-sample-layout',
	styleUrls: ['./sample.layout.scss'],
	// template: ``,
	templateUrl: './sample.layout.html',
})
export class SampleLayoutComponent implements OnDestroy {

  featureConfig = feature;

	subMenu: NbMenuItem[] = [
		{
			title: 'PAGE LEVEL MENU',
			group: true,
		},
		{
			title: 'Buttons',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/buttons',
		},
		{
			title: 'Grid',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/grid',
		},
		{
			title: 'Icons',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/icons',
		},
		{
			title: 'Modals',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/modals',
		},
		{
			title: 'Typography',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/typography',
		},
		{
			title: 'Animated Searches',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/search-fields',
		},
		{
			title: 'Tabs',
			icon: 'ion ion-android-radio-button-off',
			link: '/pages/ui-features/tabs',
		},
	];
	layout: any = {};
	sidebar: any = {};
	sidebarMenu = [
		// { icon: 'assets/images/HomeIcon.svg', title: 'Dashboard', link: '../pages/dashboard'},
		// { icon: 'assets/images/PulseIcon.svg', title: 'Learn', link: '../pages/learn' },
		// { icon: 'assets/images/LearnIcon.svg', title: 'Pulse', link: '../pages/pulse' },
		// { icon: 'assets/images/ChatIcon.svg', title: 'Chat', link: '../pages/chat' },
		// { icon: 'assets/images/FavIcon.svg', title: 'Favourites', link: '../pages/favourites' },
  ];

  sidebarMenuNew = [
    {
			icon: 'fab fa-leanpub',
			link: '',
			title: 'Learn',
			isShow: false,
			active: false,
			children: [
				{
					icon: 'fas fa-book',
					title: 'My Course',
					link: '/pages/learning/course',
					active: false,
				},
				{
					icon: 'fas fa-project-diagram',
					title: 'My Pathway',
					link: '/pages/learning/pathway',
					active: false,
				},
				{
					icon: 'fas fa-chalkboard-teacher',
					title: 'Open Courses',
					link: '/pages/learning/self',
					active: false,
				},
			]
    },
    {
      icon: 'fab fa-leanpub',
      link: '/pages/learning/catalogue',
      title: 'Catalogue',
      isShow: false,
      active: false,
      children: [],
    },
  ];

  selectedMenu: any = 0;

	private alive = true;

  currentTheme: string;

  currentUserData: any;
  showSidebar: boolean = true;

  showfaqs: any = false;
  showSendFeedback: boolean = false;
  imageSrc: string = '';

  private _routerSub = Subscription.EMPTY;

	constructor(
    protected stateService: StateService,
		protected menuService: NbMenuService,
		protected themeService: NbThemeService,
		protected bpService: NbMediaBreakpointsService,
    protected sidebarService: NbSidebarService,
    public router: Router,
    public routes: ActivatedRoute,
    public ASP: AuthServiceProvider,
    public cdf: ChangeDetectorRef,
    public events: EventsService,
  ) {
		this.stateService.onLayoutState()
			.pipe(takeWhile(() => this.alive))
			.subscribe((layout: string) => this.layout = layout);

		this.stateService.onSidebarState()
			.pipe(takeWhile(() => this.alive))
			.subscribe((sidebar: string) => {
				this.sidebar = sidebar;
			});

		if (localStorage.getItem('userOtherDetails')) {
			var UOD = JSON.parse(localStorage.getItem('userOtherDetails'));
			console.log('UOD', UOD);
			var temp = UOD[3];
			for (var i = 0; i < temp.length; i++) {
				if (temp[i].status == true) {
					const MENU_ITEM = temp[i].menu;
          this.sidebarMenu = MENU_ITEM;
          // let sideBarMenus = this.makeSideMenusReady(MENU_ITEM);
          // const updatedSideMenu = sideBarMenus.concat(this.sidebarMenuNew);
          // this.sidebarMenu = updatedSideMenu;
					// this.PagesComponent.menu=MENU_ITEM;
					console.log('SideMenuChg---->', this.sidebarMenu);
				}
      }
		}

		const isBp = this.bpService.getByName('is');
		this.menuService.onItemSelect()
			.pipe(
				takeWhile(() => this.alive),
				withLatestFrom(this.themeService.onMediaQueryChange()),
				delay(20),
			)
			.subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

				if (bpTo.width <= isBp.width) {
					this.sidebarService.collapse('menu-sidebar');
				}
			});

		this.themeService.getJsTheme()
			.pipe(takeWhile(() => this.alive))
			.subscribe(theme => {
				this.currentTheme = theme.name;
      });

    this.getUserData();

    this._routerSub = this.router.events
    .filter(event => event instanceof NavigationEnd)
    .subscribe((value) => {
       // do something with the value
       console.log('sample layout _routerSub', value);
      //  const supTabIndex = this.checkIfExists(value['urlAfterRedirects'], this.sidebarMenu);
      //  if (supTabIndex !== -1) {
      //   this.cdf.markForCheck();
      //  } else {
      //   this.cdf.markForCheck();
      //  }
       this.setSideMenuActive(value['urlAfterRedirects']);
       this.cdf.markForCheck();
   });

   this.subscribeEvent();
	}

	setSidebar(sBMenu){
		this.sidebarMenu = [];
		this.sidebarMenu = sBMenu;
	}

	// changeMenu(item, k) {
  //   if (this.selectedMenu !== k) {
  //     this.selectedMenu = k;
  //     if (this.sidebarMenu[k].children && this.sidebarMenu[k].children.length > 0) {
  //       const subTab = this.sidebarMenu[k].children[0];
  //       // this.router.navigate([subTab.link], { relativeTo: this.routes });
  //       // this.sidebarMenu[k].children[0].active = true;
  //       this.changeSubMenu(item, k, subTab, 0);
  //     } else {
  //       item.link ? this.router.navigate([item.link], { relativeTo: this.routes }) : '';
  //     }
  //     for (let i = 0; i < this.sidebarMenu.length; i++) {
  //       if (i == k) {
  //         if (this.sidebarMenu[i].children && this.sidebarMenu[i].children.length > 0) {
  //           this.sidebarMenu[i].isShow = true;
  //         }
  //         this.sidebarMenu[i].active = true;
  //       } else {
  //         this.sidebarMenu[i].active = false;
  //         this.sidebarMenu[i].isShow = false;
  //       }
  //     }
  //   } else {
  //     console.log('Same menu clicked...');
  //   }

  // }

  changeMenu(item, k) {
    if (this.selectedMenu !== k) {
          this.selectedMenu = k;
          if (this.sidebarMenu[k].children && this.sidebarMenu[k].children.length > 0) {
            const subTab = this.sidebarMenu[k].children[0];
            // this.router.navigate([subTab.link], { relativeTo: this.routes });
            // this.sidebarMenu[k].children[0].active = true;
            this.changeSubMenu(item, k, subTab, 0);
          } else {
            item.link ? this.router.navigate([item.link], { relativeTo: this.routes }) : '';
          }
          for (let i = 0; i < this.sidebarMenu.length; i++) {
            if (i == k) {
              if (this.sidebarMenu[i].children && this.sidebarMenu[i].children.length > 0) {
                this.sidebarMenu[i].isShow = true;
              }
              this.sidebarMenu[i].active = true;
            } else {
              this.sidebarMenu[i].active = false;
              this.sidebarMenu[i].isShow = false;
            }
          }
   } else {
    if (this.sidebarMenu[k].children && this.sidebarMenu[k].children.length > 0) {
      // const subTab = this.sidebarMenu[k].children[0];
      // // this.router.navigate([subTab.link], { relativeTo: this.routes });
      // // this.sidebarMenu[k].children[0].active = true;
      // this.changeSubMenu(item, k, subTab, 0);
    } else {
      item.link ? this.router.navigate([item.link], { relativeTo: this.routes }) : '';
    }
   }

  }
  changeSubMenu(mainTab, mainTabIndex, subTab, subTabIndex) {
		this.router.navigate([subTab.link], { relativeTo: this.routes });
    // subTab.active = !subTab.active;
    for (let i = 0; i < mainTab.children.length; i++) {
      if (i == subTabIndex) {
        mainTab.children[i].active = true;
      } else {
        mainTab.children[i].active = false;
      }
    }
    this.sidebarMenu[mainTabIndex] = mainTab;
  }

  makeSideMenusReady(sidebarMenu) {
    for (let i = 0; i < sidebarMenu.length; i++) {
			if (i == 0) {
        sidebarMenu[i]['active'] = true;
        sidebarMenu[i]['isShow'] = false,
        sidebarMenu[i]['children'] = [];
			} else {
        sidebarMenu[i]['active'] = false;
        sidebarMenu[i]['isShow'] = false,
        sidebarMenu[i]['children'] = [];
			}
    }
    return sidebarMenu;
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
      this.currentUserData['eid'] = JSON.parse(localStorage.getItem('employeeId'));
      this.showSidebar = true;
    } else {
      console.log('User not logged in...');
      // const teantData =  this.ASP.getDomainBasedTenant();
      // console.log('DATA--->', teantData);
      // if (teantData.tenantId) {
      //   this.showSidebar = true;
      // } else {
      //   this.showSidebar = false;
      // }
      this.showSidebar = false;
    }
  }

	ngOnDestroy() {
		this.alive = false;
	}

	sendFeedback(data) {
		// this.closeSendFeedback();
		if (data) {
		  this.showSendFeedback = false;
		} else {
		   this.showSendFeedback = true;
		}
	}

	takeScreenCapture(){
		html2canvas(document.body).then(canvas => {
		  // this.saveAs(canvas.toDataURL('image/png'), `canvas.png`)
		  console.log(canvas);
		  console.log(canvas.toDataURL());
		  this.imageSrc = canvas.toDataURL();
		  // this.imageSrc = canvas.toDataURL('image/jpeg', 1.0)
		  // console.log(canvas.toDataURL('image/jpeg', 1.0));
		  // console.log(canvas.toDataURL('image/png'));
		});
		// this.imageSrc = 'https://upload.wikimedia.org/wikipedia/commons/4/4b/What_Is_URL.jpg';
		this.showSendFeedback = true;
	  }

	gotoHelp() {
		this.showfaqs = true;
	}

	closeFaq() {
		this.showfaqs = false;
	}

	closeSendFeedback(){
		this.showSendFeedback = false;
	}

	gotoFeedback() {
		setTimeout(() => {
		  this.takeScreenCapture();
		}, 1000);
  }

  checkIfExists(path, array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].link == path) {
            return i;
        }
    }
    return -1;
  }

  setSideMenuActive(url) {
    for (let i = 0; i < this.sidebarMenu.length; i++) {
      const mainTab = this.sidebarMenu[i];
      if (mainTab.children && mainTab.children.length > 0) {
        let foundLink = false;
        for (let j = 0; j < mainTab.children.length; j++) {
          const subTab = mainTab.children[j];
          const found = url.search(subTab.link);
          if (found !== -1) {
            subTab.active = true;
            mainTab.isShow = true;
            mainTab.active = true;
            this.selectedMenu = i;
            foundLink = true;
          } else {
            subTab.active = false;
          }
          if (!foundLink &&  this.selectedMenu === i){
            this.selectedMenu = null;
          }
        }
      } else {
        const found = url.search(mainTab.link);
        if (found !== -1) {
          mainTab.active = true;
        } else {
          mainTab.active = false;
          mainTab.isShow = false;
        }
      }
    }
  }

  subscribeEvent() {
    this.events.subscribe('footer:help', (result: any) => {
      if (result.status) {
        this.gotoHelp();
      } else {
        this.closeFaq();
      }
    });
  }
}
