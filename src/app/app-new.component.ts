/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AnalyticsService } from './@core/utils/analytics.service';
import { LogServices } from './service/logservice';
import { UserService } from './service/user.service';
import { BroadcastService } from '@azure/msal-angular';
import { MsalService } from '@azure/msal-angular';
import { Subscription } from 'rxjs/Subscription';
import { AuthServiceProvider } from './service/auth-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from './service/enum';
// import { ToastrService } from 'ngx-toastr';
import { CatalogueService } from './service/learn-container/catalogue.service';
import { ToasterService } from './service/learn-container/toaster.service';
import { EventsService } from './service/events.service';
import { RoutingStateService } from './service/routing-state.service';
import { MultiLanguageService } from './service/multi-language.service';
// import { Jquery } from 'jquery/dist/jquery.min';
// import {SwUpdate} from '@angular/service-worker';
import { feature } from '../environments/feature-environment';

declare let gtag: Function;

@Component({
  selector: 'ngx-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  loggedIn: boolean;
  isError: boolean = false;
  errorMsg: string;
  public userInfo: any = null;
  private subscription: Subscription;
  private isSsoWorking: boolean;
  returnurl = window.location.hash;
  private themes = document.querySelector('body');
  featureConfig = feature;
  variables: any = {
    button: '--colorBgbtn',
    commonColor: '--colorCommon',
    header: '--colorHeader',
    icon: '--colorIcons',
    pageBackground: '--colorBgPage',
    sidemenu: '--colorSidebar',
  };

  catalogueCategories: any = [];
  edgeLearning: boolean = false;

  constructor(private analytics: AnalyticsService,
    public logService: LogServices,
    private userservice: UserService,
    private broadcastService: BroadcastService,
    private authService: MsalService,
    private router: Router,
    private routes: ActivatedRoute,
    private authservice: AuthServiceProvider,
    private ngxSpinner: NgxSpinnerService,
    private toastr: ToasterService,
    public catalogueService: CatalogueService,
    public events: EventsService,
    public routingStateService: RoutingStateService,
    public multiLanguageService: MultiLanguageService,
    ) {

      this.track_google_analytics();
    /*if (this.authService.getUser() && localStorage.getItem('msal.logoutflag') === 'true') {
      this.loggedIn = true;
      const user = this.authService.getUser();
      if (user) {
        const param = {
          email: user.displayableId,
          usrId: user.userIdentifier,
          sso: 1,
          type: 3
        }
        this.authservice.ssoverifyuser(param)
        .then((res: any) => {
          if (res.type == true) {
            this.router.navigate(['../pages/dashboard'], { relativeTo: this.routes })
          } else {
            // this.router.navigate(['../index'], { relativeTo: this.routes })
            this.redirectToLogin();
          }
        });
      }
    }
    else*/
    this.returnurl = window.location.hash;
    // if (this.returnurl && this.returnurl !== '/') {
    //   if (localStorage.hasOwnProperty('redirectURL') && localStorage.getItem('redirectURL') !== '/logout')
    //   {

    //   }else{
    //     localStorage.setItem('redirectURL', this.returnurl.replace('#', ''));
    //     this.router.navigate(['..' + this.returnurl.replace('#', '')], { relativeTo: this.routes });
    //   }

    // } else if (localStorage.getItem('userDetails')) {
    //   let rName = null;
    //   if (localStorage.getItem('roleName')) {
    //     rName = localStorage.getItem('roleName');
    //   }
    //   if (rName === 'Learner') {
    //     setTimeout(() => {
    //       this.router.navigate(['../pages/dashboard'], { relativeTo: this.routes });
    //     }, 500);
    //   } else if (rName === 'Trainer') {
    //     setTimeout(() => {
    //       this.router.navigate(['../pages/trainer-dashboard'], { relativeTo: this.routes });
    //     }, 500);
    //   } else if (rName === 'Partner') {
    //     setTimeout(() => {
    //       this.router.navigate(['../pages/partner'], { relativeTo: this.routes });
    //     }, 500)
    //   }
    // }
    // else {
    //   // localStorage.clear();
    //   this.loggedIn = false;
    //   // this.router.navigate(['../index'], { relativeTo: this.routes });
    //   this.redirectToLogin();
    // }

    this.routingStateService.subscribeToGetparams();
    this.catalogueService.resetMainBreadcrumbs();

    document['title'] = this.featureConfig.pageTitle;
  }

  track_google_analytics() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
          gtag('config', 'UA-144922434-1',
                {
                  'page_path': event.urlAfterRedirects,
                }
               );
       }
    });
  }

  ssologin() {
    // this.authService.loginPopup(["user.read" ,"api://54b9f2a8-dc1f-4173-8955-f762e6fd0b37/access_as_user"]);
    this.authService.loginPopup();
  }

  ssologout() {
    // this.authService.logout();
  }
  // ngAfterViewInit() {
  // }

  ngOnInit(): void {
    this.applyThemeData();
    // this.doNextProcess();
    const params = {
      tId: 1,
    };
    this.fetchCatelogueCategories(params);
  }

  doNextProcess(){
    this.returnurl = window.location.hash;
    this.applyThemeData();
    try {
      if (localStorage.getItem('userDetails')) {
        this.userservice.setUser(JSON.parse(localStorage.getItem('userDetails')));
      }
      this.analytics.trackPageViews();
      // this.logService.getAreasService();
      // this.logService.getIpAddress();
      this.setLanguageData();
      if (this.returnurl && this.returnurl !== '/' && this.returnurl !== '#/') {

        if (localStorage.hasOwnProperty('redirectURL') && localStorage.getItem('redirectURL') !== '/logout')
        {

        }else{
          localStorage.setItem('redirectURL', this.returnurl.replace('#', ''));
          this.router.navigate(['..' + this.returnurl.replace('#', '')], { relativeTo: this.routes });
        }
      } else if (localStorage.getItem('userDetails')) {

        let rName = null;
        if (localStorage.getItem('roleName')) {
          rName = localStorage.getItem('roleName');
        }
        if (rName === 'Learner') {
          setTimeout(() => {
            this.router.navigate(['../pages/dashboard'], { relativeTo: this.routes });
          }, 500);
        } else if (rName === 'Trainer') {
          setTimeout(() => {
            this.router.navigate(['../pages/trainer-dashboard'], { relativeTo: this.routes });
          }, 500);
        } else if (rName === 'Partner') {
          setTimeout(() => {
            this.router.navigate(['../pages/partner'], { relativeTo: this.routes });
          }, 500)
        }
      }
      else {
        // localStorage.clear();
        this.loggedIn = false;
        // this.router.navigate(['../index'], { relativeTo: this.routes });
        this.redirectToLogin();
      }
    } catch (e) {
      console.log(e);
    }
    // console.log("user", this.authService.getUser());
    this.broadcastService.subscribe("msal:loginFailure", (payload) => {
      console.log("login failure " + JSON.stringify(payload));
      this.loggedIn = false;
      this.ngxSpinner.hide();
    });

    this.broadcastService.subscribe("msal:loginSuccess", async (payload) => {
      this.ngxSpinner.hide();
      this.ngxSpinner.show();
      try {
        // console.log("login success " + JSON.stringify(payload));
      const user = this.authService.getUser();
      let tenant = localStorage.getItem('tenantinfo');
      let tenantId = null;
      if(tenant) {
        tenant = JSON.parse(tenant);
        tenantId = tenant['tenantId'];
      } else {
        tenantId = 1;
      }
      // let tenantList: any = {};
      // tenantList = await this.authservice.getTenantList();
      // let tenantId = null;
      // if (tenantList.data && tenantList.data.length > 0) {
      //   for(var i=0; i <  tenantList.data.length; i++) {
      //     const tenantitem = tenantList.data[i];
      //     if(tenantitem.clientId === user.idToken['aud']) {
      //       tenantId = tenantitem.tenantId;
      //       break;
      //     };
      //   }
      // } else {
      //   tenantId = 1;
      // }
      // console.log("user", user);
      if (user) {
        const param = {
          email: user.displayableId,
          tenantId: tenantId,
          sso: 1,
          usrId: user.userIdentifier,
          type: 3
        }
        this.authenticateUser(param, (res) => {
          this.ngxSpinner.hide();
          localStorage.removeItem('tenantinfo');
          if (res) {
            // this.router.navigate(['../welcome'], { relativeTo: this.routes });
            const redirectURL = localStorage.getItem('redirectURL') || '';
            if(redirectURL !== '/login-new' && redirectURL !== '/login' && redirectURL !== '') {
              setTimeout( () => {
              this.router.navigate([redirectURL], { relativeTo: this.routes });
              }, 500);
            } else {
              setTimeout( () => {
              this.router.navigate(['../welcome'], { relativeTo: this.routes });
              }, 500);
            }
          }
          else{
            this.router.navigate(['../login-new'], { relativeTo: this.routes });
          }
        });
      }
      this.loggedIn = true;
      } catch (e) {
        this.ngxSpinner.hide();
      }
    });
  }

  ngAfterViewInit(): void {
    this.checkIfSubDomainValid((flag, data) =>{

      if(!flag){
        console.log('Invalid domain');
        if(data){
          let found = false;
          if(data.params.subDomain){
             const arr1 = data.response.exceptions ? data.response.exceptions.split(',') : null;
             if(arr1){
              found = arr1.includes(data.params.subDomain);
             }
          }
          if(data.response && data.response.redirectionURL['link'] && !found){
            // window.location.replace(data.redirectionURL['link']);
            window.location.href = data.response.redirectionURL["link"];
          }else{
            const demoData = {
              organizationType: 1,
              subEndDate: null,
              subStartDate: null,
              tenantCode: data.params.subDomain,
              tenantId: null,
              tenantName: data.params.subDomain,
              visible: 1,
            }
            this.authservice.setDomainBasedTenant(demoData);
            this.edgeLearning = false;
          }
        }
      }else {
        this.edgeLearning = true;
        this.authservice.setDomainBasedTenant(data.response.data);
      }
      if(data.response['theme']){
        localStorage.setItem('theme', JSON.stringify(data.response['theme']))
      }

      this.doNextProcess();
    })
    this.applyThemeData();
    const jquery = window['jQuery'];
    const quill = window['Quill'];
    setTimeout( () => {
      window['jQuery'] = jquery;
      window['Quill'] = quill;
      // console.log(window['jQuery'].fn.jquery);
    }, 1000);
  }

  authenticateUser(param, cb) {
    this.ngxSpinner.show();
    if (localStorage.getItem('ssoaccess') && localStorage.getItem('ssoaccess') === 'false') {
      this.ngxSpinner.hide();
      cb(null)
    } else {
      this.authservice.msalLogin(param).then((res) => {
        console.log(res);
        var temp: any = res;
        this.ngxSpinner.hide();
        if (temp.type == false) {
          // alert(temp.data1);
          this.isError = true;
          this.errorMsg = temp.data1;
          localStorage.setItem('ssoaccess', 'false');
          cb(null);
          // this.authService.logout();
          //this.router.navigate(['../login'],{relativeTo:this.routes});
        } else if (temp.type == true) {
          var stringUserDetail = JSON.stringify(temp.data);
          var stringUserOtherDetail = JSON.stringify(temp.otherdata);
          var userdetail = temp.data;
          localStorage.setItem('userDetails', stringUserDetail);
          localStorage.setItem('token', temp.token);
          localStorage.setItem('employeeId', temp.employeeId);
          userdetail.userId = userdetail.id;
          const otherdata = temp.otherdata[3];
          // const themes = temp.theme;
          // localStorage.setItem('theme', JSON.stringify(themes));
          // localStorage.setItem('theme', JSON.stringify(themes));
          // if (themes) {
          //   for (let key in themes) {
          //     let value = themes[key];
          //     this.changeTheme(key, value);
          //   }
          // }
          for (let i = 0; i < otherdata.length; i++) {
            // if (otherdata[i].fieldmasterId) {
            //   userdetail.referenceId = otherdata[i].fieldmasterId;
            //   userdetail.roll = otherdata[i].roleName;
            //   userdetail.roleId = otherdata[i].roleId;
            // } else {
            //   userdetail.roll = 'Learner';
            //   userdetail.roleId = 8;
            // }
            if (otherdata[i].isDefault == 1) {
                userdetail.referenceId = otherdata[i].fieldmasterId;
                userdetail.roll = otherdata[i].roleName;
                userdetail.roleId = otherdata[i].roleId;
                localStorage.setItem('roleName', otherdata[i].roleName);
            }
          }

          if (localStorage.getItem('userOtherDetails')) {
            var UOD = JSON.parse(localStorage.getItem('userOtherDetails'));
            console.log('UOD', UOD);
            var temp = UOD[3];
            for (let i = 0; i < temp.length; i++) {
              if (otherdata[i].isDefault === 1) {
                userdetail.referenceId = otherdata[i].fieldmasterId;
                userdetail.roll = otherdata[i].roleName;
                userdetail.roleId = otherdata[i].roleId;
              }
            }
          }
          this.userservice.setUser(userdetail);
          localStorage.setItem('userdetail', JSON.stringify(userdetail));
          if(userdetail && userdetail['userLang']){
            this.multiLanguageService.updateLangugae(userdetail['userLang'], false);
            }
          localStorage.setItem('userOtherDetails', stringUserOtherDetail);
          localStorage.setItem('ssoaccess', 'true');
          cb(true);
        }
      }).catch((res) => {
        console.log(res);
        this.ngxSpinner.hide();
        cb(false);
      })
    }
  }

  ngOnDestroy() {
    this.broadcastService.getMSALSubject().next(1);
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  applyThemeData(){
    const themes = JSON.parse(localStorage.getItem('theme'));
    if(themes) {
      for(let key in themes) {
        let value = themes[key];
        this.changeTheme(key,value);
      }
    }
  }
  setLanguageData(){
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    if(userdetail && userdetail['userLang']){
      this.multiLanguageService.updateLangugae(userdetail['userLang'], false);
      }else{
        const userLang = {
          languageId: "1",
          languageName: "English",
        }
        this.multiLanguageService.updateLangugae(userLang, false);
      }
  }

  closeModal () {
    // this.authService.logout();
    this.isError = false;
  }
  redirectToLogin() {
    // if (window.location.pathname.includes('#/login') || window.location.pathname.includes('login')) {
    //   this.router.navigate(['../login'], { relativeTo: this.routes });
    // } else {
    //   this.router.navigate(['../index'], { relativeTo: this.routes });
    // }
    // this.router.navigate(['../login'], { relativeTo: this.routes });
    if (!ENUM.maintenance) {
      // this.edgeLearning = true;
      console.log('Edge learning environment flag : ', this.edgeLearning);
      // if (this.edgeLearning) {
      //   this.router.navigate(['../pages/learning/catalogue'], { relativeTo: this.routes });
      // } else {
      //   this.router.navigate(['../login-new'], { relativeTo: this.routes });
      // }
      this.router.navigate(['../login-new'], { relativeTo: this.routes });
    } else {
      this.router.navigate(['../index'], { relativeTo: this.routes });
    }
  }

  changeTheme(colorName, colorValue) {
    // Object.keys(this.variables).forEach(function(val) {
      // console.log('themes=========>>>>>>>>>>>', themes[key]);
      for(let val in this.variables) {
      if(val === colorName) {
        if (colorName === 'header') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const headerTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorHeadertxt', headerTxt);
        } else if (colorName === 'sidemenu') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const sidebarTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorSidebartxt', sidebarTxt);
        } else if (colorName === 'button') {
          this.themes.style.setProperty(this.variables[val], colorValue);
          const btnTxt = this.getContrastYIQ(colorValue);
          this.themes.style.setProperty('--colorBtnTxt', btnTxt);
        } else {
          this.themes.style.setProperty(this.variables[val], colorValue);
        }
      }
    }
    // });
  }

  getContrastYIQ(hexcolor){
    hexcolor = hexcolor.replace("#", "");
    var r = parseInt(hexcolor.substr(0,2),16);
    var g = parseInt(hexcolor.substr(2,2),16);
    var b = parseInt(hexcolor.substr(4,2),16);
    var match = ((r*299)+(g*587)+(b*114))/1000;
    return (match >= 128) ? '#000000' : '#ffffff';
  }

  checkIfSubDomainValid(cb){
    const hostName = window.location.hostname;
    const callBack = cb;
    let subDomain = '';
    if (hostName !== 'localhost') {
      const splited = hostName.split('.');
      if (splited && splited.length !== 0) {
        subDomain = splited[0];
      }
    }else {
      subDomain = 'localhost';
      // subDomain = 'shashank';
    }
    // this.subDomain = hostName ? const splited = hostName.split('.') ? [0] : '';
    // console.log('Subdomain ==>', this.subDomain);
    // subDomain = 'admin5';
    const param = {
      subDomain : subDomain,
      // subDomain : 'invaliddomain',
      requestFrom: 3,
    }
    this.authservice.checkSubDomainIsValid(param).then((response) => {
      // console.log(res);
      // const response: any = res;
      // this.ngxSpinner.hide();
      const data = {
        params: param,
        response: response,
      }
      if (response['type'] == false) {
        // alert(temp.data1);
        cb(false, data);
        // this.authService.logout();
        //this.router.navigate(['../login'],{relativeTo:this.routes});
      } else if (response['type'] === true) {
        cb(true, data);
      }
    }).catch((res) => {
      console.log(res);
      // this.ngxSpinner.hide();
      // cb(false, res);
      this.checkIfSubDomainValid(callBack);
    });
  }

  /**************************** fetch catalogue categories start ****************************/
  fetchCatelogueCategories(params: any) {
    this.catalogueService.getCatalogueCategories(params).then(
      res => {
        if (res.type === true) {
          // cb(res);
          this.catalogueCategories = res.categoryList;
          if (this.catalogueCategories.length) {
            this.catalogueService.setCategories(this.catalogueCategories);
          }
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
      }
    );
  }
  /**************************** fetch catalogue categories end ****************************/

  publishEvent(params) {
    this.events.publish('learn:search', {
      data: params,
      status: true,
    });
  }
}
