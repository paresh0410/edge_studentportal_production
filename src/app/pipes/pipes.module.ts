import { NgModule } from '@angular/core';
import { MandetoryquestionpipePipe } from './mandetoryquestionpipe.pipe';

@NgModule({
    imports:        [],
    declarations:   [MandetoryquestionpipePipe],
    exports:        [MandetoryquestionpipePipe],
})

export class PipeModule {

  static forRoot() {
     return {
         ngModule: PipeModule,
         providers: [],
     };
  }
} 