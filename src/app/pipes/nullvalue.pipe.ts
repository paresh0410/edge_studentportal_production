import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isUndefined'
})
export class NullvaluePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(typeof value === 'undefined') { 
      return args;
    } else if (value === null) {
      return args;
    } else {
      return value;
    }
  }

}
