import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the SettingsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsServiceProvider{

    constructor(public http: HttpClient, private toastr: ToastrService) {

    }

    changePassword(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     this.toastr.warning('Unable to change password','Warning!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }

}
