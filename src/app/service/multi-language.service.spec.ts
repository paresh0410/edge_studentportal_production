import { TestBed } from '@angular/core/testing';

import { MultiLanguageService } from './multi-language.service';

describe('MultiLanguageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MultiLanguageService = TestBed.get(MultiLanguageService);
    expect(service).toBeTruthy();
  });
});
