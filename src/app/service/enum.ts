export let ENUM = {
  // domain: 'http://localhost:9845',
  // domain: "https://devwebapi.edgelearning.co.in",
  domain: "https://webapi.edgelearning.co.in",
  // domain: 'https://uatwebserver.edgelearning.co.in',
  // domain: 'https://migwebapi.edgelearning.co.in',
  // domain: 'https://prodwebserver.edgelearning.co.in',
  // domain: 'https://saaswebserver.edgelearning.co.in',
  // domain: 'https://testsaaswebserver.edge-lms.co.in',

  maintenance: false,
  edgeLearning: false,
  url: {
    /*************** AUTH ***************/
    auth: "/api/studentPortal/userLogin",
    tenantList: "/api/studentPortal/tenantList",
    getToken: "/api/studentPortal/sendTokenForForgotPassword",
    verifyToken: "/api/studentPortal/verifyTokenForForgotPassword",
    resetPassword: "/api/studentPortal/setNewPasswordForForgotPassword",
    resendToken: "/api/studentPortal/resendTokenForForgotPassword",
    logout: "/api/studentPortal/user_logout",
    signUp: "/api/studentPortal/insertSignUpData",
    checkIfSubDomainExist: "/api/studentPortal/checkSubDomainIsValid",
    checkIfAuthTokenValid: "/api/studentPortal/checkIfAuthTokenValid",

    /*************** PROFILE ***************/
    getProfile: "/api/studentPortal/getProfile",
    getPathway: "/api/studentPortal/pathway",
    addPathway: "/api/studentPortal/addEditPathway",
    getBadges: "/api/studentPortal/getBadges",
    getCertificates: "/api/studentPortal/certificate",
    saveProfileImg: "/api/studentPortal/saveProfileImage",
    removeProfileImg: "/api/studentPortal/removeProfileImage",
    changePassword: "/api/studentPortal/changePassword",

    /*************** INTERESTS ***************/
    interestPage: "/api/studentPortal/interests",
    addInterests: "/api/studentPortal/addInterests",

    /*************** PULSE ***************/
    getPulse: "/api/studentPortal/pulse",
    pushPulse: "/api/studentPortal/pushPulse",
    getTrendingTags: "/api/studentPortal/trendingTags",
    getTrendingTagsData: "/api/studentPortal/trendingTagsData",
    addFeedback: "/api/studentPortal/addFeedback",
    removeFeedback: "/api/studentPortal/removeFeedback",

    /*************** LEARN ***************/
    getLearn: "/api/studentPortal/learn",
    get_all_leran_course_list: "/api/studentPortal/listLearnCourses",
    list_all_learning_pathway_courses:
      "/api/studentPortal/list_all_learning_pathway_courses",
    getLearnModules: "/api/studentPortal/learnModules",
    getModuleActivity: "/api/studentPortal/moduleActivity",
    addLearnFeedback: "/api/studentPortal/addLearnFeedback",
    removeLearnFeedback: "/api/studentPortal/removeLearnFeedback",
    saveActivityCompletion: "/api/studentPortal/activitycompletion",
    addCourseFeedback: "/api/studentPortal/addCourseFeedback",
    removeCourseFeedback: "/api/studentPortal/removeCourseFeedback",
    getScormList: "app/web/getScormList",
    saveScormTrackList: "app/web/saveScormTrackers",
    selfEnrol: "/api/studentPortal/selfEnrol",
    getScormUrl: "/app/web/getScormUrl",
    viewScormPackage: "/app/web/viewScromPackageById",
    deleteScormPackage: "/app/web/deleteScromPackageById",
    getScormPackage: "/app/web/getScorm",
    getMarkedAttendance: "/api/web/getMarkAttendanceActivityData",
    markAttendance: "/api/studentPortal/insert_attendance_usha_batch",
    getTrianerList: "/api/web/getWebinarTrainerList",
    getUsersList: "/api/web/getWebinarLearnerList",
    getSearchedLearn: "/api/web/getAllSearchedCourses",
    saveActivityCompletionWebinar:
      "/api/studentPortal/batch_webinar_activity_completion",
    getUpdatedActivityData: "/api/web/getUpdatedCourseActivityData",

    get_catalogue_categories: "/api/catelogue/getallcategorylist",
    get_catalogue_courses_with_categories:
      "/api/catelogue/getallcouserandsubcats",
    get_catalogue_courses_by_categories:
      "/api/catelogue/getallfilteraData_and_courseData",
    get_catalogue_course_details: "/api/catelogue/getcateloguecoursesDetails",

    add_course_to_wishlist: "/api/catelogue/updatewishlist_status",
    get_wishlisted_courses: "/api/catelogue/getwishlistcourses",

    get_my_courses_with_filter: "/api/learn/getAllmyCourses",
    get_my_pathways_with_filter: "/api/learn/getAllpathways",
    get_courses_from_my_pathways: "/api/learn/getAllpathwaycourses",
    get_open_courses_with_filter: "/api/learn/getAllopencourse",

    create_purchase_order: "/api/payment/insertOrderAnditemData",
    update_purchase_order: "/api/payment/insertPaymentData",

    enrol_free_course: "/api/catelogue/enrolFreeCourses",

    list_all_learn_courses_by_search_filter:
      "/api/studentPortal/list_all_learn_courses_by_search_filter",
    /*************** ASSESSMENT ***************/
    checkQuizUser: "/api/studentPortal/checkQuizUser",
    getQuizQuestions: "/api/studentPortal/getQuizQuestions",
    getQuizForReview: "/api/studentPortal/getQuizQuesForReview",
    submitQuizAnswers: "/api/studentPortal/submitQuizAnswers",
    checkFeedbackUser: "/api/studentPortal/checkFeedbackUser",
    getFeedbackQuestions: "/api/studentPortal/getFeedbackQuestions",
    submitFeedbackAnswers: "/api/studentPortal/submitFeedbackAnswers",

    /*************** REACTION ***************/
    getPollUser: "/api/studentPortal/getPollUser",
    getPollQuestionAnswers: "/api/studentPortal/getPollQuestionAnswers",
    submitPollAnswers: "/api/studentPortal/submitPollAnswers",
    getSurveyUser: "/api/studentPortal/getSurveyUser",
    getSurveyQuestionAnswers: "/api/studentPortal/getSurveyQuestionAnswers",
    submitSurveyAnswers: "/api/studentPortal/submitSurveyAnswers",

    /*************** CHAT ***************/
    getChatList: "/api/web/getChatUserlist",
    getActiveChatList: "/api/web/getActiveChatList",
    checkChatUserName: "/api/web/chatUsernameCheck",
    inviteUserForChat: "/api/web/inviteUserForChat",
    inviteChatResponse: "/api/web/inviteResponse",
    getChatMessage: "/api/web/getMessages",
    getChatUserList: "/api/web/getChatUserlist",
    getChatLogin: "/api/web/chatLogin",
    insertDirectUserFriendMap: "/api/web/insertDirectUserFriendMap",
    //End Chat Module

    /****************** MANAGER ********************/
    getEmployeeDetails: "/api/studentPortal/getEmployeeDetails",
    approveRejectCourse: "/api/studentPortal/approveRejectCourse",
    course_completion: "/api/studentPortal/course_completion_detail",
    /****************** LEARNER ********************/
    getLearnerDetails: "/api/studentPortal/getLearnerDashboardDetails",
    getLearnerRankDetails: "/api/studentPortal/getLearnerlederboardDetails",
    /****************** CALENDAR ********************/
    getCalendarEvents: "/api/studentPortal/getCalendarEvents",

    /****************** Common ********************/
    getKpointToken: "/api/kpoint/xauthtoken",
    getCurrentViewershipDetail:
      "https://#DOMAIN_NAME/api/v1/xapi/view/#KVTOKEN?xt=#XT",
    /****************** Notification ********************/
    getNotification: "/api/studentPortal/getPushNotifications",
    readPushNotifications: "/api/studentPortal/update_push_notification_status",
    get_unread_push_notifications_count:
      "/api/studentPortal/get_unread_push_notifications_count",
    /****************************USHA********************************* */
    getAllUshaParticipants:
      "/api/studentPortal/get_all_enrolled_emp_by_batchId",
    getCourseModules: "/api/studebtPortal/get_enrol_course_modules",
    getTrainerDashboardData: "/api/studentPortal/get_all_batches_by_trainerId",
    getAssessmentData: "/api/studentPortal/get_assessment_data_by_quizId",
    getFeedbackData: "/api/studentPortal/get_feedback_data_by_feedbackId",
    insertDeleteAttendance: "/api/studentPortal/insert_attendance_usha_batch",
    enrolTrainerToBatch: "/api/studentPortal/insert_trainer_enrol",
    unerolTrainerBatch: "/api/studentPortal/unenrol_trainer",
    getAllParticipantAttendance:
      "/api/studentPortal/get_all_participants_attendance",
    // getLevel1FeedbackData: '/api/studentPortal/get_Trainer_Dashboard_feedback_l_2',
    // getLevel2FeedbackData: '/api/studentPortal/get_Trainer_Dashboard_feedback_l_3',
    // getLevel1AssessData: '/api/studentPortal/get_Trainer_Dashboard_Assessment_l_2',
    // getLevel2AssessData: '/api/studentPortal/get_Trainer_Dashboard_Assessment_l_3',
    getEvalutionfbquestion: "/api/studentPortal/TA_Evalution_feedback_question",
    submitTAevalutionfb: "/api/studentPortal/submit_TA_feedback_answers",
    programList: "/api/studentPortal/get_all_programs_by_trainerId",
    getFeedbackSection: "/api/studentPortal/get_feedback_sections",
    getLevel1CourseList: "/api/studentPortal/get_Trainer_Dashboard_l_2_clist",
    getLevel1QuestionList:
      "/api/studentPortal/get_Trainer_Dashboard_feedback_l_2_qlist",
    getLevel2CourseList:
      "/api/studentPortal/get_Trainer_Dashboard_feedback_l_3_elist",
    getLevel2QuestionList:
      "/api/studentPortal/get_Trainer_Dashboard_feedback_l_3_qlist",
    // getKpointToken: '/api/kpoint/xauthtoken',
    /***************************Train The trainer********************************/
    getCocProfileData: "/api/studentPortal/get_coc_profile",
    getTttPrograms: "/api/studentPortal/get_ttt_workflow_program",
    insertWorflowIntroResponse:
      "/api/studentPortal/insert_ttt_workflow_program_response",
    updateProfileData: "/api/studentPortal/update_profile_status",
    getEmpData: "/api/studentPortal/get_emp_data",
    getAllWorkFlowSteps: "/api/studentPortal/workflow_step",
    workflowslotlist: "/api/studentPortal/booking_slotlist",
    BookSlot: "/api/studentPortal/bookslot",
    nomineeList: "/api/studentPortal/nominee_List",
    getcoursecontent: "/api/studentPortal/TTT_course_content",
    nomineelistbytrainer: "/api/studentPortal/nomineelistbytrainer",
    ttt_trainerfeedback: "/api/studentPortal/ttttrainerfeedback",
    trainer_fb_submit: "/api/studentPortal/submit_feedback_answers",
    evalution_fb_for_learner: "/api/studentPortal/evalution_fb_for_learner",
    confirm_batch: "/api/studentPortal/ttt_confirm_batch",
    getTrainerForWebinar: "/api/studentPortal/get_ttt_trainers_webinar",
    getUserForWebinar: "/api/studentPortal/get_ttt_users_webinar",
    saveWorkflowCompletion: "/api/studentPortal/save_workflow_completion",
    ttt_batch_list: "/api/studentPortal/get_batch_list_ttt",
    getmodules: "/api/studentPortal/get_modules",
    tttconfirmwebinar: "/api/studentPortal/webinar_book_for_TTT",
    observercourse: "/api/studentPortal/get_ttt_observer_course_modules",
    tttselectedbatch: "/api/studentPortal/ttt_batch_selction",
    getTTTCertificate: "/api/web/certificate",
    getTTTCertificateContent: "/api/web/certificate_content",
    getfeedbackscore: "/api/studentPortal/get_feedback_average",
    /*BH APROVAL */
    get_BH_aproval_list: "/api/studentPortal/ttt_get_BH_aproval_list",
    ttt_update_BH_aproval: "/api/studentPortal/ttt_update_BH_aproval",
    webinar_recording: "/api/studentPortal/get_webinar_recording",
    check_fb_evaluated_or_not: "/api/studentPortal/check_fb_evaluated_or_not",
    /*************************EEP***********************/
    eep_workflowstep: "/api/studentPortal/EEE_workflow_steps",
    eep_codeofconduct: "/api/studentPortal/EEE_get_coc_profile",
    eep_workflow_program: "/api/studentPortal/get_EEE_workflow_program",
    eep_insert_workflow_responce:
      "/api/studentPortal/insert_EEE_workflow_intro_response",
    eep_Updatetttprofile: "/api/studentPortal/EEE_update_profile_responce",
    eep_emp_data: "/api/studentPortal/EEE_get_emp_data",
    eep_getmodules: "/api/studentPortal/get_EEP_course_content",
    get_GBH_aproval_list: "/api/studentPortal/eep_get_BH_aproval_list",
    eep_update_GBH_aproval: "/api/studentPortal/eep_update_BH_aproval",

    /******************Call Coaching ********************/
    get_nominee_list: "/api/studentPortal/get_nominee_calls",
    cc_course_module: "/api/studentPortal/get_course_modules",
    cc_get_feedback: "/api/studentPortal/get_feedback_by_callId",
    cc_add_feedback: "/api/studentPortal/add_edit_feedback",
    cc_trainer_nomineelist: "/api/studentPortal/get_nomination_trainer",
    cc_trainer_call: "/api/studentPortal/get_calls_trainer",
    cc_assement: "/api/studentPortal/get_assessment_by_callId",
    cc_add_assessment: "/api/studentPortal/add_edit_assessment",
    cc_manager_call: "/api/studentPortal/get_calls_manager",
    cc_observation: "/api/studentPortal/get_manager_observation_by_callId",
    cc_add_observation: "/api/studentPortal/add_edit_observation",
    cc_get_userlist: "/api/studentPortal/get_call_userlist",
    cc_get_call_note: "/api/studentPortal/get_call_note",
    cc_insert_call_note: "/api/studentPortal/insert_call_note",
    cc_get_updated_call_status: "/api/studentPortal/get_updated_call_status",

    /*Partner*/
    partner_nominee_list: "/api/studentPortal/get_nominee_partner",
    partner_nominee_call: "/api/studentPortal/get_nominee_calls_partner",
    partner_trainer_list: "/api/studentPortal/get_trainer_partner",
    Add_Edit_trainer: "/api/studentPortal/add_edit_call_partner",
    /* Logged */
    logSaved: "/api/studentPortal/logsaved",
    getAreas: "/api/studentPortal/getareas",

    IPAddress: "https://jsonip.com/",
    /**
     * Sso URL
     */
    ssoLogin: "/api/studentPortal/ssouserLogin",
    ssoverifyuser: "/api/studentPortal/ssoverifyuser",

    /** Tools */
    get_all_section: "/api/studentPortal/get_sections",
    get_all_section_content: "/api/studentPortal/get_sections_content",

    /****************** Send Help Feedback ********************/
    sendHelpFeedback: "/api/web/insert_help_feedback_section",
    pdfviewerurl: "/pdf-viewer/web/viewer.html?file=",
    getPdfUrl: "/api/studentPortal/getPdfUrl?file=",
    deletePdfUrl: "/api/studentPortal/deletePdfUrl?file=",

    /****************** Send Help Feedback ********************/
    getDynamicLinkData: "/api/web/getLearnDataForFirebaseDynamicLink",

    /****************** Multi Langugage********************/
    getMultiLanguageData: "/api/web/get_all_language",
    updateUserLanguage: "/api/studentPortal/updateuserLang",
  },
};
