import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the InterestsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InterestsServiceProvider{

    interestArr:any = [];

    constructor(public http: HttpClient, private toastr: ToastrService) {

        this.interestArr = [
			{
				group: [
					{
						name: 'Category 1 Literature',
						checked: true,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},{
				group: [
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					},
					{
						name: 'Category 1 Literature',
						checked: false,
					}
				]
			},
		]

    }

    getInterestsData(url,param){

        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
          //       if(err){
          //           // this.toastr.error('Please check server connection','Server Error!');
          //           this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					// this.toastr.clear());
          //       }
            });
        });
    }

    addInterestsData(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
                this.toastr.success('Interests Saved Successfully','Success!').onHidden.subscribe(()=>
				this.toastr.clear());
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.error('Please check server connection','Server Error!');
                    this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					this.toastr.clear());
                }
            });
        });
    }

}
