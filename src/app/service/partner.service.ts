import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  nominee: any;
  constructor(private http: HttpClient, private toastr: ToastrService) { }
  get(url, param) {
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            // if (err) {
            //   this.toastr.warning('Unable to get data', 'Warning!').onHidden.subscribe(()=>
            //   this.toastr.clear());
            // }
          });
    });
  }
}
