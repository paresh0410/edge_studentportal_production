import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import io from "../../assets/js/socket";
// import io from '../assets/js/socket';
import { WebinarSource } from "../pages/webinar-multiple/webinar-entity";

import { ENUM } from "../service/enum";
@Injectable()
export class WebinarServiceProvider {
  socket: any;
  userId: number;
  customKey: any;
  constructor(public http: Http) {
    //this.http = http;
    this.socket = null;
    this.customKey = null;
  }
  httpCall(httpData) {
    if (
      httpData.url === undefined ||
      httpData.url === null ||
      httpData.url === ""
    ) {
      alert(`Invalid HTTP call`);
    }
    const HTTP = this.http;
    const params = httpData.params;
    // return new Promise( (resolve, reject) => {
    //     HTTP.post(httpData.url, params).then( (response) => {
    //         resolve(response.data);
    //     }).catch( (response, status, header, config) => {
    //         reject(response.data);
    //     });
    // });
    return new Promise((resolve) => {
      this.http
        .post(httpData.url, params)
        .map((res) => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              //this.toastr.error('Please check server connection','Server Error!');
              resolve(err);
            }
          }
        );
    });
  }
  connectSocketServer(userId) {
    this.userId = userId;
    //const socket = io.connect('http://localhost:9845', { query: `userId=${userId}` });
    const socketparam =
      `userId=${userId}` +
      `&source=${WebinarSource.PORTAL}` +
      `&customKey=${this.autoGenerateId()}`;
    const socket = io.connect(ENUM.domain, {
      query: socketparam,
      autoConnect: true,
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 5000,
      reconnectionAttempts: 5000,
      transports: ["polling", "websocket"],
    });
    this.socket = socket;
    socket.on("connect", () => {
      console.log("Connected");
    });
    socket.on("reconnect", () => {
      console.log("reconnection");
      // this.connectSocketServer(this.userId);
    });

    socket.on("disconnect", (reason) => {
      console.log(`Disconnected: ${reason}`);
    });
  }

  disconnect = () => {
    if(this.socket){
      this.socket.disconnect();
    }

  };
  socketEmit(eventName, params) {
    if(this.socket){
      this.socket.emit(eventName, params);
    }
  }

  socketOn(eventName, callback) {
    this.socket.on(eventName, (response) => {
      if (callback) {
        callback(response);
      }
    });
  }

  getMessages(userId, friendId, areaId, instanceId) {
    return new Promise((resolve, reject) => {
      this.httpCall({
        url: ENUM.domain + ENUM.url.getChatMessage, //'http://localhost:9845/api/web/getMessages',
        params: {
          userId: userId,
          toUserId: friendId,
          areaId: areaId,
          instanceId: instanceId,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  searchUser(name) {
    return new Promise((resolve, reject) => {
      this.httpCall({
        url: ENUM.domain + ENUM.url.getChatUserList, //'http://localhost:9845/api/web/getChatUserlist',
        params: {
          username: name,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  loginUser(username, password) {
    return new Promise((resolve, reject) => {
      this.httpCall({
        url: ENUM.domain + ENUM.url.getChatLogin, //'http://localhost:9845/api/web/chatLogin',
        params: {
          username: username,
          password: password,
        },
      })
        .then((response) => {
          resolve(response);
          //$location.path(`/home/${response.userId}`);
          //$scope.$apply();
        })
        .catch((error) => {
          alert(error.message);
        });
    });
  }

  scrollToBottom() {
    const messageThread = document.querySelector(".message-thread");
    setTimeout(() => {
      messageThread.scrollTop = messageThread.scrollHeight + 500;
    }, 10);
  }
  insertDirectUserFriendMap(param) {
    return new Promise((resolve, reject) => {
      this.httpCall({
        url: ENUM.domain + ENUM.url.insertDirectUserFriendMap, //'http://localhost:9845/api/web/getMessages',
        params: {
          userId: param.userId,
          friendId: param.friendId,
        },
      })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  socketRemoveListner(eventName) {
    this.socket.removeAllListeners(eventName);
    this.socket.destroy();
  }

  autoGenerateId() {
    this.customKey = "edge-" + Math.random();
    return this.customKey;
  }
}
