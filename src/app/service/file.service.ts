// import { Injectable } from '@angular/core';
// import { Platform, LoadingController } from '@ionic/angular';
// import { File } from '@ionic-native/file/ngx';
// import { MimetypeService } from './mimetype.service';
// import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
// @Injectable({
//   providedIn: 'root'
// })
// export class FileService {

//   window: any = Window;
//   initialized: any;
//   basePath: any;
//   progress: number;
//   private fileTransfer: FileTransferObject;
//   loading: any = {};
//   filecount: any = 0;
//   downloadfilecount: any = 0;
//   constructor(public platform: Platform, public file: File, public mimeUtils: MimetypeService, public transfer: FileTransfer,
//     public loadingController: LoadingController) {
//       this.createLoading();
//     }

//   init(): Promise<void> {
//     // if (this.initialized) {
//     //     return Promise.resolve();
//     // }

//     return this.platform.ready().then(() => {

//         if (this.platform.is('android')) {
//             this.basePath = this.file.externalApplicationStorageDirectory;
//         } else if (this.platform.is('ios')) {
//             this.basePath = this.file.documentsDirectory;
//         } else if (!this.isAvailable() || this.basePath === '') {
//             return Promise.reject(null);
//         }

//         this.initialized = true;
//     });
// }
// /**
//      * Check if the plugin is available.
//      *
//      * #return - boolean - Whether the plugin is available.
//      */
//     isAvailable(): boolean {
//       return typeof this.window.resolveLocalFileSystemURL !== 'undefined';
//   }

//   /**
//      * Get a file.
//      *
//      * #param { string } path Relative path to the file.
//      * #return {Promise<FileEntry>} Promise resolved when the file is retrieved.
//      */
//   //   getFile(path: string): Promise<FileEntry> {
//   //     return this.init().then(() => {
//   //         console.log('Get file: ' + path);

//   //         return this.file.resolveLocalFilesystemUrl(this.addBasePathIfNeeded(path));
//   //     }).then((entry) => {
//   //         return <FileEntry> entry;
//   //     });
//   // }

//   /**
//      * Adds the basePath to a path if it doesn't have it already.
//      *
//      * #param {string} path Path to treat.
//      * #return {string} Path with basePath added.
//      */
//     addBasePathIfNeeded(path: string): string {
//       if (path.indexOf(this.basePath) > -1) {
//           return path;
//       } else {
//           // return this.textUtils.concatenatePaths(this.basePath, path);
//       }
//   }

//   /**
//      * Writes some data in a file.
//      *
//      * #param {string} path Relative path to the file.
//      * #param {any} data Data to write.
//      * #return {Promise<any>} Promise to be resolved when the file is written.
//      */
//     writeFile(path: string, data: any): Promise<any> {
//       return this.init().then(() => {
//           // Remove basePath if it's in the path.
//           path = this.removeStartingSlash(path.replace(this.basePath, ''));
//           // this.logger.debug('Write file: ' + path);

//           // Create file (and parent folders) to prevent errors.
//           return this.createFile(path).then((fileEntry) => {
//               // if (this.isHTMLAPI && !this.appProvider.isDesktop() &&
//               //     (typeof data === 'string' || data.toString() === '[object ArrayBuffer]')) {
//                 if (typeof data === 'string' || data.toString() === '[object ArrayBuffer]') {
//                   // We need to write Blobs.
//                   const type = this.mimeUtils.getMimeType(this.mimeUtils.getFileExtension(path));
//                   data = new Blob([data], { type: type || 'text/plain' });
//               }

//               return this.file.writeFile(this.basePath, path, data, { replace: true }).then(() => {
//                   return fileEntry;
//               });
//           });
//       });
//   }

//   createFile(path: string, failIfExists?: boolean): Promise<File> {
//     return this.create(false, path, failIfExists);
// }
// protected create(isDirectory: boolean, path: string, failIfExists?: boolean, base?: string): Promise<any> {
//   return this.init().then(() => {
//       // Remove basePath if it's in the path.
//       path = this.removeStartingSlash(path.replace(this.basePath, ''));
//       base = base || this.basePath;

//       if (path.indexOf('/') === -1) {
//           if (isDirectory) {
//               // this.logger.debug('Create dir ' + path + ' in ' + base);

//               return this.file.createDir(base, path, !failIfExists);
//           } else {
//               // this.logger.debug('Create file ' + path + ' in ' + base);

//               return this.file.createFile(base, path, !failIfExists);
//           }
//       } else {
//           // The file plugin doesn't allow creating more than 1 level at a time (e.g. tmp/folder).
//           // We need to create them 1 by 1.
//           const firstDir = path.substr(0, path.indexOf('/')),
//               restOfPath = path.substr(path.indexOf('/') + 1);

//           // this.logger.debug('Create dir ' + firstDir + ' in ' + base);

//           return this.file.createDir(base, firstDir, true).then((newDirEntry) => {
//               return this.create(isDirectory, restOfPath, failIfExists, newDirEntry.toURL());
//           }).catch((error) => {
//               // this.logger.error('Error creating directory ' + firstDir + ' in ' + base);

//               return Promise.reject(error);
//           });
//       }
//   });
// }
//   /**
//      * Remove the starting slash of a path if it's there. E.g. '/sites/filepool' -> 'sites/filepool'.
//      *
//      * #param {string} path Path.
//      * #return {string} Path without a slash in the first position.
//      */
//     removeStartingSlash(path: string): string {
//       if (path[0] === '/') {
//           return path.substr(1);
//       }

//       return path;
//   }

//   checkFileDirectoryExist(foldername, cb) {

//     if (foldername) {
//       this.init().then(() => {
//         if (this.platform.is('cordova')) {
//           this.file.checkDir(this.basePath, foldername).then((res) => {
//             cb(res);
//           }).catch( (err) => {
//             cb(false);
//           });
//         }
//     });
//     }
//   }

//   createDir(foldername, cb) {
//     this.checkFileDirectoryExist(foldername, (isExist) => {
//       if (!isExist) {
//         this.file.createDir(this.basePath, foldername, false).then((res) => {
//           cb(res);
//         }).catch((error) => {
//           cb(error);
//         });
//       } else {
//         cb(true);
//       }
//     });
//   }

//   public download(fileurl, filename, foldername, courseId, totalfilecount, cb) {
//     // const fileName = item.picture.medium;
//     this.filecount = totalfilecount;
//     let text = null;
//     if (totalfilecount === 0) {
//       this.presentLoading('');
//       text = 'Total Files ' + this.filecount +  '/n Total Files Download {{downloadfilecount}} /n File is Downloading {{progress}}';
//       this.setLoadingText(text);
//     }
//     const url = encodeURI(fileurl);
//     this.fileTransfer = this.transfer.create();
//     this.fileTransfer.onProgress((progressEvent) => {
//       const perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
//       this.progress = perc;
//       console.log('Percentage ', perc);
//       // tslint:disable-next-line:max-line-length
//       text = 'Total Files ' + this.filecount + '/n Total Files Download ' + this.downloadfilecount + ' /n File is Downloading ' + this.progress + '%';
//       this.setLoadingText(text);
//     });
//     this.init().then(() => {
//       if (this.platform.is('cordova')) {
//         const courseFolder = foldername + '/' + courseId;
//         this.createDir(courseFolder, () => {
//           this.fileTransfer.download(url, this.basePath + courseFolder + '/' + filename, true).then((entry) => {
//             console.log('download completed: ' + entry.toURL());
//             this.downloadfilecount++;
//             if (totalfilecount === this.filecount) {
//               this.dismissLoading();
//             }
//             cb(entry.toURL());
//           }, (error) => {
//             this.dismissLoading();
//               console.log('download failed: ' + error);
//               cb(fileurl);
//           });
//         });
//       } else {
//         this.dismissLoading();
//         cb(fileurl);
//       }
//     });
//   }
//   presentLoading(msg) {
//     this.loading.message = msg;
//     this.loading.present();

//     // const { role, data } = await loading.onDidDismiss();

//     // console.log('Loading dismissed!');
//   }
//   async createLoading() {
//     this.loading = await this.loadingController.create({
//       spinner: 'crescent',
//       message: 'Total Files {{filecount}} /n Total Files Download {{downloadfilecount}} /n File is Downloading {{progress}}',
//       translucent: true,
//     });
//   }
//   async dismissLoading() {
//     this.loading.message = '';
//     await this.loading.dismiss();

//     console.log('Loading dismissed!');
//   }
//   setLoadingText(text: string) {
//     const elem = document.querySelector(
//           'loading-content');
//     if (elem) {
//       elem.innerHTML = text;
//     }
//   }
//   public downloadscorm(folder, fileurl, cb) {
//     // const fileName = item.picture.medium;
//     const url = encodeURI(fileurl);
//     this.fileTransfer = this.transfer.create();
//     this.fileTransfer.onProgress((progressEvent) => {
//       const perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
//       if (perc === 100) {
//       }
//     });
//     this.init().then(() => {
//       if (this.platform.is('cordova')) {
//         const fileName = fileurl.substring(fileurl.lastIndexOf('/') + 1);
//         // const Folder = foldername + '/' + courseId;
//         // folder = this.basePath + folder;
//         const finalpath = this.basePath + folder;
//         const destination = this.basePath + folder + '/' + fileName;
//         this.createMultipleDir(this.basePath, folder).then(( result ) => {
//           this.fileTransfer.download(url, destination, true).then((entry) => {
//             console.log('download completed: ' + entry.toURL());
//             cb(entry.toURL(), finalpath);
//           }, (error) => {
//               console.log('download failed: ' + error);
//               cb(fileurl, finalpath);
//           });
//         }).catch(() => {
//           cb(fileurl, finalpath);
//         });

//       } else {
//         cb(fileurl);
//       }
//     });
//   }
//   public createScormDir(folder): Promise<any> {
//    return this.init().then(() => {
//       if (this.platform.is('cordova')) {
//         // const fileName = fileurl.substring(fileurl.lastIndexOf('/') + 1);
//         // const Folder = foldername + '/' + courseId;
//         // folder = this.basePath + folder;
//         const finalpath = this.basePath + folder;
//         // const destination = this.basePath + folder + '/' + fileName;
//         return this.createMultipleDir(this.basePath, folder).then(( result ) => {
//           return Promise.resolve(finalpath);
//         }).catch((error) => {
//           return Promise.resolve(error);
//         });
//       }
//     });
//   }

//   protected createMultipleDir(destination: string, dirPath: string): Promise<void> {
//     // Create all the folders 1 by 1 in order, otherwise it fails.
//     const folders = dirPath.split('/');
//     let promise = Promise.resolve();

//     for (let i = 0; i < folders.length; i++) {
//         const folder = folders[i];

//         promise = promise.then(() => {
//             return this.file.createDir(destination, folder, true).then(() => {
//                 // Folder created, add it to the destination path.
//                 destination = this.concatenatePaths(destination, folder);
//             });
//         });
//     }

//     return promise;
// }
// concatenatePaths(leftPath: string, rightPath: string): string {
//   if (!leftPath) {
//       return rightPath;
//   } else if (!rightPath) {
//       return leftPath;
//   }

//   const lastCharLeft = leftPath.slice(-1),
//       firstCharRight = rightPath.charAt(0);

//   if (lastCharLeft === '/' && firstCharRight === '/') {
//       return leftPath + rightPath.substr(1);
//   } else if (lastCharLeft !== '/' && firstCharRight !== '/') {
//       return leftPath + '/' + rightPath;
//   } else {
//       return leftPath + rightPath;
//   }
// }
// fileExitsCheck(path, file): Promise<string> {

//   return this.file.checkFile(path, file).then((result) => {
//     return Promise.resolve(result);
//   }).catch( (error) => {
//     return Promise.resolve(error);
//   });
// }

// readFile(path, file): Promise<any> {

//   return this.file.readAsText(path, file).then( (text) => {
//     return Promise.resolve(text);
//   }).catch((error) => {
//     return Promise.resolve(error);
//   });
// }
// }
