import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IkPoint {
  host?: String;
  id: String;
  src: String;
  url?: String;
  player?: String;
  params?: Object;
}
