import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the PollServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PollServiceProvider {

    pollSummary: any;

    constructor(private http: HttpClient, private toastr: ToastrService) { }

    setPollSummary(obj){
        this.pollSummary = obj;
    }

    getPollSummary(){
        return this.pollSummary;
    }

    getPoll(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     this.toastr.warning('Unable to get poll list','Warning!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }

    submitPoll(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     this.toastr.warning('Please check server connection','Server Error!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }

}
