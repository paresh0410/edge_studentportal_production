import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ENUM } from './enum';
@Injectable({
    providedIn: 'root'
})
export class TrainTheTrainerServiceProvider {

    private headers = new HttpHeaders();

    baseUrl: any = ENUM.domain;
    workflow: any = [];
    cocProfileData: any;
    coursecontent: any = [];
    participantdata: any = [];
    activityData: any;
    currentstepdata: any = [];
    getrecording: any = this.baseUrl + ENUM.url.webinar_recording;
    constructor(private http: HttpClient, private toastr: ToastrService) { }

    setActivityData(data) {
        this.activityData = data;
    }

    getActivityData() {
        return this.activityData;
    }

    getCocProfileData(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getAllProrams(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }


    insertWorflowIntroResponse(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    updateProfileData(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getEmpData(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getAllWorkFlowSteps(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getWorkFlowslot(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    bookWorkFlowslot(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    nomineelist(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    get(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getTrianers(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getUsers(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    saveCompletion(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getCertificateFromUrl(url) {
        return new Promise(resolve => {
            this.http.get(url)
                // .map(res => res.json())
                .subscribe(response => this.downLoadFile(response, "application/pdf"));
        });
    }

    downLoadFile(data: any, type: string) {
        var blob = new Blob([data], { type: type });
        var url = window.URL.createObjectURL(blob);
        var pwa = window.open(url);
        if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
            alert('Please disable your Pop-up blocker and try again.');
        }
    }

    getAllAprocallist(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }


    updateAprovalStatus(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    getStepPoints(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
                // .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                },
                    err => {
                        resolve(err);
                        // if (err) {
                        //     // this.toastr.error('Please check server connection', 'Server Error!');
                        //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                        //     this.toastr.clear());
                        // }
                    });
        });
    }

    list(data: any): Promise<any> {
        // const url = this.getScormListUrl;
        return this.http.post(this.getrecording, data, { headers: this.headers })
            .toPromise()
            .then(response => {
                return response = response;
            })
            .catch(this.handleError);
    }
    private handleError(error: Response | any) {
        return Promise.reject(error.message || error);
    }
}
