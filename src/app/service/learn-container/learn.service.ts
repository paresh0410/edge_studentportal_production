import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../enum';

@Injectable({
  providedIn: 'root'
})
export class LearnService {

  baseUrl = ENUM.domain;
  private headers = new HttpHeaders();
  getLearnCourseListUrl = this.baseUrl + ENUM.url.get_all_leran_course_list;

	getMyCoursesWithFilterUrl = this.baseUrl + ENUM.url.get_my_courses_with_filter;
	getMyPathwaysWithFilterUrl = this.baseUrl + ENUM.url.get_my_pathways_with_filter;
  getOpenCoursesWithFilterUrl = this.baseUrl + ENUM.url.get_open_courses_with_filter;

	selfEnrolCourses = this.baseUrl + ENUM.url.selfEnrol;
	submitCoursefeedback = this.baseUrl + ENUM.url.addCourseFeedback;
  removeCoursefeedback = this.baseUrl + ENUM.url.removeCourseFeedback;

	getPathwayDetailsUrl = this.baseUrl + ENUM.url.get_courses_from_my_pathways;
	get_all_learn_courses_by_search_filter_url = this.baseUrl + ENUM.url.list_all_learn_courses_by_search_filter;
	// get_all_learn_data_by_firebase_dynamic_url = this.baseUrl + ENUM.url.get_learn_data_by_firebase_dynamic_link;

  getWishlistedCoursesUrl = this.baseUrl + ENUM.url.get_wishlisted_courses;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService
  ) { }

  getFetchUrlBasedOnType(type) {
    if (type == 1) {
      return this.getMyCoursesWithFilterUrl;
    } else if (type == 2) {
      return this.getMyPathwaysWithFilterUrl;
    } else if (type == 3) {
      return this.getOpenCoursesWithFilterUrl;
    } else if (type == 4) {
      return this.getWishlistedCoursesUrl;
    }
  }

  public getLearnCourseList(learnData: any, type): Promise<any> {
    const url = this.getFetchUrlBasedOnType(type);
    return this.http.post(url, learnData, { headers: this.headers })
    .toPromise()
    .then(response => {
      const res ={
        'data': response,
        'type': type,
      };
      // return response
      return res;
    })
    .catch(this.handleError);
  }

  public getMyCoursesWithFilter(learnData: any): Promise<any> {
    return this.http.post(this.getMyCoursesWithFilterUrl, learnData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getMyPathwaysWithFilter(learnData: any): Promise<any> {
    return this.http.post(this.getMyPathwaysWithFilterUrl, learnData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getOpenCoursesWithFilter(learnData: any): Promise<any> {
    return this.http.post(this.getOpenCoursesWithFilterUrl, learnData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public selfEnrolUserCourses(courseData: any): Promise<any> {
    return this.http.post(this.selfEnrolCourses, courseData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public submitUserCoursesFeedback(courseData: any): Promise<any> {
    return this.http.post(this.submitCoursefeedback, courseData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public removeUserCoursesFeedback(courseData: any): Promise<any> {
    return this.http.post(this.removeCoursefeedback, courseData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getPathwayDetails(learnData: any): Promise<any> {
    return this.http.post(this.getPathwayDetailsUrl, learnData, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getAllLearnCourseListBySearchFilter(learnData: any): Promise<any> {
    return this.http.post(this.get_all_learn_courses_by_search_filter_url, learnData, { headers: this.headers })
        .toPromise()
        .then(response => {
            return response = response;
        })
        .catch(this.handleError);
}
  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}
