import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(
    private toastr: ToastrService,
  ) { }

  async prsentToast(msg, title, type) {
    if (type == 'success') {
      this.toastr.success(msg, title).onHidden.subscribe(
        () => this.toastr.clear(),
      );
    } else if (type == 'warning') {
      this.toastr.warning(msg, title).onHidden.subscribe(
        () => this.toastr.clear(),
      );
    } else if (type == 'error') {
      // this.toastr.error(msg, title).onHidden.subscribe(
      //   () => this.toastr.clear(),
      // );
      this.toastr.warning(msg, title).onHidden.subscribe(
        () => this.toastr.clear(),
      );
    }
  }
}
