import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../enum';

@Injectable({
  providedIn: 'root'
})

export class CatalogueService {

  private headers = new HttpHeaders();

  baseUrl = ENUM.domain;

  getCatalogueCategoriesUrl = this.baseUrl + ENUM.url.get_catalogue_categories;
	getCatalogueCoursesWithCategoriesUrl = this.baseUrl + ENUM.url.get_catalogue_courses_with_categories;
  getCatalogueCoursesByCategoriesUrl = this.baseUrl + ENUM.url.get_catalogue_courses_by_categories;
  getCatalogueCoursesDetailsUrl = this.baseUrl + ENUM.url.get_catalogue_course_details;

  addCourseToWishlistUrl = this.baseUrl + ENUM.url.add_course_to_wishlist;
  getWishlistedCoursesUrl = this.baseUrl + ENUM.url.get_wishlisted_courses;

  createPurchaseOrderUrl = this.baseUrl + ENUM.url.create_purchase_order;
  updatePurchaseOrderUrl = this.baseUrl + ENUM.url.update_purchase_order;

  enrolFreeCoursesUrl = this.baseUrl + ENUM.url.enrol_free_course;

  breadCrumbs: any = [];
  catalogueCategories: any = [];

  currentCatId: any = 0;

  breadCrumbs1: any = [];

  constructor(
    private http: HttpClient,
    private toastr: ToastrService
  ) { }

  setCategories(categories) {
    this.catalogueCategories = categories;
  }

  getCategories() {
    return this.catalogueCategories;
  }

  addToBreadcrumbs(path) {
    const position = this.checkIfExists(path, this.breadCrumbs);
    if (position !== -1) {
      return false;
    } else {
      this.breadCrumbs.push(path);
      return true;
    }
  }

  removeFromBreadcrumbs(path) {
    const position = this.checkIfExists(path, this.breadCrumbs);
    if ( position  !== -1) {
      this.breadCrumbs.splice(position, 1);
      console.log('Path removed from array..');
    } else {
      console.log('Path does not exists in array..');
    }
  }

  removeAllAfterIndexFromBreadcrumbs(path) {
    const position = this.checkIfExists(path, this.breadCrumbs);
    if ( position  !== -1) {
      this.breadCrumbs.splice(position + 1, this.breadCrumbs.length - (position + 1) );
    } else {
      console.log('Path does not exists in array..');
    }
  }

  checkIfExists(path, array) {
    // const catId = path.categoryId ? path.categoryId : path;
    // const catId = path.categoryId;
    // for (let i = 0; i < this.breadCrumbs.length; i++) {
    //     if (this.breadCrumbs[i].categoryId == catId) {
    //         return i;
    //     }
    // }
    // return -1;
    const catId = path.categoryId;
    for (let i = 0; i < array.length; i++) {
        if (array[i].categoryId == catId) {
            return i;
        }
    }
    return -1;
  }

  resetBreadcrumbs() {
    this.breadCrumbs = [];
  }

  getCurrentPageBreadcrumbs() {
    if (this.breadCrumbs1.length > 0) {
      const currentBreadCrumbsIndex = this.breadCrumbs1.length - 1;
      const breadcrumbsList = this.breadCrumbs1[currentBreadCrumbsIndex].subBreadCrumbs;
      return breadcrumbsList;
    } else {
      return this.breadCrumbs1;
    }
  }

  addSubBreadcrumbs(subBreadCrumbs) {
    subBreadCrumbs.id = this.breadCrumbs1.length + 1;
    this.breadCrumbs1.push(subBreadCrumbs);
  }

  updateSubBreadcrumbs(subBreadCrumbs) {
    this.breadCrumbs1[this.breadCrumbs1.length - 1].subBreadCrumbs.push(subBreadCrumbs);
  }

  updateBreadcrumbs(subBreadCrumbs) {
    this.breadCrumbs1[this.breadCrumbs1.length - 1].subBreadCrumbs = subBreadCrumbs;
  }

  updateSubBreadcrumbsOptions(option, value) {
    const tempBreadCrumbsLen = this.breadCrumbs1.length;
    const currentBreadCrumbsIndex = tempBreadCrumbsLen - 1;
    const tempBreadCrumbs = this.breadCrumbs1[currentBreadCrumbsIndex];
    if (tempBreadCrumbs) {
      const tempSubBreadCrumbsLen = tempBreadCrumbs.subBreadCrumbs;
      const currentSubBreadCrumbsIndex = tempSubBreadCrumbsLen.length - 1;
      // const tempSubBreadCrumbs = tempBreadCrumbs.subBreadCrumbs[tempSubBreadCrumbsLen - 1];
      if (tempBreadCrumbsLen > 0) {
        if (option == 1) {
          this.breadCrumbs1[currentBreadCrumbsIndex].subBreadCrumbs[currentSubBreadCrumbsIndex].searchStr = value;
        } else if (option == 2) {
          this.breadCrumbs1[currentBreadCrumbsIndex].subBreadCrumbs[currentSubBreadCrumbsIndex].filters = value;
        } else if (option == 3) {
          this.breadCrumbs1[currentBreadCrumbsIndex].subBreadCrumbs[currentSubBreadCrumbsIndex].afterSearch = value;
        }
      }
    }
  }

  removeSubBreadcrumbs() {
    const tempBreadCrumbs = this.breadCrumbs1[this.breadCrumbs1.length - 1];
    const tempSubBreadCrumbs = tempBreadCrumbs.subBreadCrumbs;
    this.breadCrumbs1[this.breadCrumbs1.length - 1].subBreadCrumbs.splice(tempSubBreadCrumbs.length - 1, 1);
  }

  removeAllAfterIndexFromSubBreadcrumbs(path) {
    const tempBreadCrumbs = this.breadCrumbs1[this.breadCrumbs1.length - 1];
    const tempSubBreadCrumbs = tempBreadCrumbs.subBreadCrumbs;
    const position = this.checkIfExists(path, tempSubBreadCrumbs);
    if ( position  !== -1) {
      // this.breadCrumbs.splice(position + 1, this.breadCrumbs.length - (position + 1) );
      this.breadCrumbs1[this.breadCrumbs1.length - 1].subBreadCrumbs.splice(position + 1, tempSubBreadCrumbs.length - (position + 1) );
    } else {
      console.log('Path does not exists in array..');
    }
  }

  getUpdatedBreadCrumbs(path) {
    const tempBreadCrumbs = this.breadCrumbs1[this.breadCrumbs1.length - 1];
    const tempSubBreadCrumbs = tempBreadCrumbs.subBreadCrumbs;
    const position = this.checkIfExists(path, tempSubBreadCrumbs);
    if ( position  !== -1) {
      tempSubBreadCrumbs.splice(-1, 1);
      const updateBreadcrumbs = tempSubBreadCrumbs;
      return updateBreadcrumbs;
    } else {
      console.log('Path does not exists in array..');
      return null;
    }
  }

  removeBreadcrumbs() {
    this.breadCrumbs1.splice(this.breadCrumbs1.length - 1, 1);
  }

  resetMainBreadcrumbs() {
    this.breadCrumbs1 = [];
  }

  makeFilterDataReady(filtersData) {

    // let filtersData = filters.arrFilters;
    // let selectedFilters = filters.strFilters;

    let sortStr = '';
    let priceStr = '';
    let levelStr = '';
    let tagStr = '';
    let filterCount = 0;

    for (let i = 0; i < filtersData.length; i++) {
      const filter = filtersData[i];
      if (filter.id == 1) {
        for (let j = 0; j < filter.innerTag.length; j++) {
          const filterData = filter.innerTag[j];
          if (filterData.checked) {
            sortStr = this.getFilterString(sortStr, filterData);
            filterCount++;
          }
        }
      } else if (filter.id == 2) {
        for (let j = 0; j < filter.innerTag.length; j++) {
          const filterData = filter.innerTag[j];
          if (filterData.checked) {
            priceStr = this.getFilterString(priceStr, filterData);
            filterCount++;
          }
        }
      } else if (filter.id == 3) {
        for (let j = 0; j < filter.innerTag.length; j++) {
          const filterData = filter.innerTag[j];
          if (filterData.checked) {
            levelStr = this.getFilterString(levelStr, filterData);
            filterCount++;
          }
        }
      }  else if (filter.id == 5) {
        for (let j = 0; j < filter.innerTag.length; j++) {
          const filterData = filter.innerTag[j];
          if (filterData.checked) {
            tagStr = this.getFilterString(tagStr, filterData);
            filterCount++;
          }
        }
      }
    }

    const appliedFilters = {
      sort: sortStr,
      price: priceStr,
      level: levelStr,
      tag: tagStr,
    };

    if (filterCount > 0) {
      return appliedFilters;
    } else {
      return null;
    }
  }

  getFilterString(str, filter) {
    if (str == '') {
      str = filter.id;
    } else {
      str = str + ',' + filter.id;
    }
    return str;
  }

  public getCatalogueCategories(params: any): Promise<any> {
    return this.http.post(this.getCatalogueCategoriesUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getCatalogueCourses(params: any): Promise<any> {
    return this.http.post(this.getCatalogueCoursesWithCategoriesUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getCatalogueCoursesByCategories(params: any): Promise<any> {
    return this.http.post(this.getCatalogueCoursesByCategoriesUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getCatalogueCoursesDetails(params: any): Promise<any> {
    return this.http.post(this.getCatalogueCoursesDetailsUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public addCourseToWishlist(params: any): Promise<any> {
    return this.http.post(this.addCourseToWishlistUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public getWishlistedCourses(params: any): Promise<any> {
    return this.http.post(this.getWishlistedCoursesUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public createPurchaseOrder(params: any): Promise<any> {
    return this.http.post(this.createPurchaseOrderUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public updatePurchaseOrder(params: any): Promise<any> {
    return this.http.post(this.updatePurchaseOrderUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  public enrolFreeCourses(params: any): Promise<any> {
    return this.http.post(this.enrolFreeCoursesUrl, params, { headers: this.headers })
    .toPromise()
    .then(response => {
      return response = response;
    })
    .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}
