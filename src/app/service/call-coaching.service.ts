import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../service/enum';

@Injectable({
  providedIn: 'root'
})
export class CallCoachingService {

  frompage: any;
  employeedata: any = [];
  callId: any;
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  get(url, param) {
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
          //   if (err) {
          //     // this.toastr.error('Please check server connection', 'Server Error!');
          //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          //     this.toastr.clear());
          //   }
          });
    });
  }
}
