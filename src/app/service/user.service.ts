import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  user: any;
  userRole: any;
  constructor() { }

  setUser(userinfo) { // set User Info/Details
    this.user = userinfo;
  }
  getUser() { // fetch User Info/Details
    return this.user;
  }
  setUserRole(roles) {
    this.userRole = roles;
  }
  getUserRole() {
    return this.userRole;
  }
  clear() {
    this.user = null;
    this.userRole = null;
  }
}
