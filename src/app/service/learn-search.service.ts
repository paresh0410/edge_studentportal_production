import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../service/enum';
import { BehaviorSubject } from 'rxjs';
/*
  Generated class for the LearnSearchServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LearnSearchServiceProvider{

    searchString: string;
    private dataSource = new BehaviorSubject('Eric');
	data = this.dataSource.asObservable();
    constructor(private http: HttpClient, private toastr: ToastrService) {}

    setSearchString(data){
        this.searchString = data;
    }

    getSearchString(){
        return this.searchString;
    }

    getSearchedCourses(param){
        const url = ENUM.domain + ENUM.url.getSearchedLearn;
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     // this.toastr.error('Please check server connection','Server Error!');
                //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }
	updatedDataSelection(data) {
		this.dataSource.next(data);
	 }
}
