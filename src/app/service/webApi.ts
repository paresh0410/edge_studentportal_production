export let webApi = {
    domain: 'http://35.154.55.154:9855/',
    // domain: 'http://localhost:9845',                  //DEV
    url: {
        login: '/api/login',
         /*************** CHAT ***************/
         getChatList:'/api/web/getChatUserlist',
         getActiveChatList:'/api/web/getActiveChatList',
         checkChatUserName:'/api/web/chatUsernameCheck',
         inviteUserForChat:'/api/web/inviteUserForChat',
         inviteChatResponse:'/api/web/inviteResponse',
         getChatMessage : '/api/web/getMessages',
         getChatUserList : '/api/web/getChatUserlist',
         getChatLogin : '/api/web/chatLogin',
         insertDirectUserFriendMap: '/api/web/insertDirectUserFriendMap'
    }, 
}