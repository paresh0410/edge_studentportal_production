import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the ProfileServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProfileServiceProvider{

    constructor(public http: HttpClient, private toastr: ToastrService) { }

    getProfileData(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     // this.toastr.error('Please check server connection','Server Error!');
                //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }

    uploadProfilePic(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.error('Please check server connection','Server Error!');
                    this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                    this.toastr.clear());
                }
            });
        });
    }

    deleteProfilePic(url, param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.error('Please check server connection','Server Error!');
                    this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                    this.toastr.clear());
                }
            });
        });
    }

    getCertificateData(url){
        return new Promise(resolve => {
            this.http.get(url)
            // .map(res => res.json())
            .subscribe(response => this.downLoadFile(response, "application/pdf"));
        });
    }

    downLoadFile(data: any, type: string) {
        var blob = new Blob([data], { type: type});
        var url = window.URL.createObjectURL(blob);
        var pwa = window.open(url);
        if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
            alert( 'Please disable your Pop-up blocker and try again.');
        }
    }

}
