import { TestBed } from '@angular/core/testing';

import { CallCoachingService } from './call-coaching.service';

describe('CallCoachingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CallCoachingService = TestBed.get(CallCoachingService);
    expect(service).toBeTruthy();
  });
});
