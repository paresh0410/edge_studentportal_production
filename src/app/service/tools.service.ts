import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {
    flagData: any = {};
    contentData: any = {};
  sectionId: any;
  section_data: any = [];
  parentSecId: any;
  fromContent: any = false;
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  getSectionData(url, param) {
    return new Promise(resolve => {
        this.http.post(url, param)
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve(err);
            // if (err) {
            //     // this.toastr.error('Please check server connection', 'Server Error!');
            //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
            //     this.toastr.clear());
            // }
        });
    });
}

getSectionContent(url, param) {
  return new Promise(resolve => {
      this.http.post(url, param)
      .subscribe(data => {
          resolve(data);
      },
      err => {
          resolve(err);
          // if (err) {
          //     // this.toastr.error('Please check server connection', 'Server Error!');
          //     this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          //     this.toastr.clear());
          // }
      });
  });
}
}
