export enum ENotificationRedirectPage {
  COURSE= 'course',
  ACTIVITY= 'activity',
  MODULE= 'module',
  HOME= 'home',
  DASHBOARD= 'dashboard',
  POLL= 'poll',
  SURVEY= 'survey',
  POLLLIST= 'polllist',
  SURVEYLIST= 'surveylist',
  CHAT= 'chat',
  WEBINAR= 'webinar',
  NEWS_AND_ANNOUNCEMENT= 'news_and_announcement',
}
