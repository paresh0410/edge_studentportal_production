import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ENUM } from "./enum";
import { MsalService } from "@azure/msal-angular";
// import { resolve } from 'path';
// import {} from 'SSOTenantInfo';
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {
  TenantInfo: any;
  private domainBasedTenant: any;
  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private msalService: MsalService
  ) {}

  ssoverifyuser(param) {
    const url = ENUM.domain + "" + ENUM.url.ssoverifyuser;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection', 'Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  authUserData(param) {
    // var url =ENUM.domain + '' + ENUM.url.auth;
    const url = ENUM.domain + "" + ENUM.url.ssoLogin;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  getToken(param) {
    // var url =ENUM.domain + '' + ENUM.url.auth;
    const url = ENUM.domain + "" + ENUM.url.getToken;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection', 'Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  verifyToken(param) {
    // var url =ENUM.domain + '' + ENUM.url.auth;
    const url = ENUM.domain + "" + ENUM.url.verifyToken;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection', 'Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  resetPassword(param) {
    // var url =ENUM.domain + '' + ENUM.url.auth;
    const url = ENUM.domain + "" + ENUM.url.resetPassword;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection', 'Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  LoginUsingSSO() {
    const url = ENUM.domain + ENUM.url.ssoLogin;
    return new Promise((resolve) => {
      this.http
        .get(url)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }
  msalLogin(param) {
    const url = ENUM.domain + ENUM.url.ssoLogin;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            // if (err) {
            //   // this.toastr.error('Please check server connection','Server Error!');
            //   this.toastr.warning(
            //     "Something went wrong please try again later",
            //     "Warning!"
            //   );
            // }
          }
        );
    });
  }
  ssologin() {
    // this.authService.loginPopup(["user.read" ,"api://54b9f2a8-dc1f-4173-8955-f762e6fd0b37/access_as_user"]);
    this.msalService.loginPopup();
  }

  ssologout() {
    this.msalService.logout();
  }
  setTenant(tenant) {
    this.TenantInfo = tenant;
  }
  getTenant() {
    return this.TenantInfo;
  }
  getTenantList() {
    const url = ENUM.domain + ENUM.url.tenantList;
    return new Promise((resolve) => {
      this.http
        .post(url, {})
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
    // return SSOTenantInfo;
  }
  resendtoken(param) {
    const url = ENUM.domain + ENUM.url.resendToken;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  userLogout(param) {
    const url = ENUM.domain + ENUM.url.logout;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  signUp(param) {
    const url = ENUM.domain + ENUM.url.signUp;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }
  checkSubDomainIsValid(param) {
    const url = ENUM.domain + ENUM.url.checkIfSubDomainExist;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  setDomainBasedTenant(tenant) {
    this.domainBasedTenant = tenant;
  }
  getDomainBasedTenant() {
    return this.domainBasedTenant;
  }
}

export let SSOTenantInfo = [
  {
    text: "BFLFINATICS",
    value: {
      clientID: "bcd88b9b-1ef1-4f07-a923-05245da7d5f6",
      authority:
        "https://login.microsoftonline.com/bflfinatics.onmicrosoft.com/",
    },
  },
  {
    text: "MOBESERV",
    value: {
      clientID: "0b579e9b-ce14-4bdc-a2f9-224c6b0280fe",
      authority: "https://login.microsoftonline.com/finatics.onmicrosoft.com/",
    },
  },
];
