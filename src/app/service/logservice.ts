import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ENUM} from './enum';
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LogServices{

    areas : any;
    ipAddress : any;
    constructor(private http: HttpClient) {}

    saveLogged(param){
        var params = this.populateUserInfo(param);
        var url =  ENUM.domain + ENUM.url.logSaved;
        return new Promise(resolve => {
            this.http.post(url, params)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    //this.toastr.error('Please check server connection','Server Error!');
                }
            });
        });
    };
    getAreasService(){
        var url = ENUM.domain + ENUM.url.getAreas;
        return new Promise(resolve => {
            this.http.post(url,null)
            // .map(res => res.json())
            .subscribe(data => {
                if(data){
                    this.areas = data["data"];//resolve(data);
                }
                
            },
            err => {
                resolve(err);
                if(err){
                    //this.toastr.error('Please check server connection','Server Error!');
                }
            });
        });
    };
    getIpAddress(){
        var url = ENUM.url.IPAddress;
        // this.http.get('https://api.ipify.org?format=json').subscribe(data => {
        this.ipAddress= null;// data['ip'];
        // });
    };
    setAreasList(list){
        this.areas = list;
    };
    getAreasList(){
        return this.areas;
    };
    getArea(component){
        var area = {};
        if(this.areas){
            if(this.areas.length > 0){
                for(var i=0; i<this.areas.length; i++){
                    if(this.areas[i].name === component){
                        area = this.areas[i];
                        break;
                    }
                }
            }
        }
        
        return area;
    }
    populateUserInfo(param){
        var userDetail;
        if(localStorage.getItem("userDetails")){
            userDetail = JSON.parse(localStorage.getItem("userDetails"));
        }
        var userOtherDetails;
        let role:any;
        if(localStorage.getItem("userOtherDetails")){
            userOtherDetails = JSON.parse(localStorage.getItem("userOtherDetails"));
            if(userOtherDetails && userOtherDetails[0].length > 0){
                role = userOtherDetails[0][0];
            }
            
        }
        let area:any = this.getArea(param.area);
        var userInfo={
            roleid: role["roleId"] == undefined ? null : role["roleId"],
            role: role["roleName"] == undefined ? null : role["roleName"],
            tenantid: userDetail.tenantId,
            userid: userDetail.id,
            component: area["component"],
            subcomponent: area["name"],
            eventname : "\\"+area["component"]+"\\"+area["name"], 
            action: param.action,
            target: param.target,
            areaid: area["areaId"] == undefined ? null : area["areaId"],
            area: area["name"] == undefined ? null : area["name"],
            instanceid: param.instanceid,
            module: "student",
            other: param.other,
            origin: "web",
            ip: this.ipAddress,
            crud: param.crud,
            url: window.location.href
        }
        return userInfo;
}
};

@Injectable()
export class LogEnum{
    added:string = "added";
    assigned:string = "assigned";
    called:string = "called";
    completed:string = "completed";
    created:string = "created";
    deleted:string = "deleted";
    ended:string = "ended";
    failed:string = "failed";
    graded:string = "graded";
    graph:string = "graph";
    launched:string = "launched";
    loggedin:string = "loggedin";
    loggedout:string = "loggedout";
    removed:string = "removed";
    reset:string = "reset";
    resumed:string = "resumed";
    reviewed:string = "reviewed";
    searched:string = "searched";
    sent:string = "sent";
    shown:string = "shown";
    started:string = "started";
    submitted:string = "submitted";
    unassigned:string = "unassigned";
    updated:string = "updated";
    uploaded:string = "uploaded";
    viewed:string = "viewed";
    switched:string = "switched";
    changed:string= "changed";
}

export class AreaEnum {
    category = 'category';
    course = 'course';
    courseBundle = 'course bundle';
    content = 'content';
    activity = 'activity';
    post = 'post';
    badge = 'badge';
    // category = 'category';
    question = 'question';
    module = 'module';
    ladder = 'ladder';
    enroled = 'enroled';
    quiz = 'quiz';
    survey = 'survey';
    fb = 'FB';
}