import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ENUM } from "../service/enum";
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CourseServiceProvider {
  flag: any = {};
  courseDataSummary: any;
  courseDataContent: any;
  currentQuizData: any = {
    quizData: "",
    reviewFlag: false,
    quizSumitData: "",
    reattemptFlag: false,
  };
  resultData: any;

  constructor(private http: HttpClient, private toastr: ToastrService) {}

  getCourseData(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  getQuizForReview(param) {
    const url = ENUM.domain + ENUM.url.getQuizForReview;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection', 'Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  submitQuizData(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  saveResultData(data) {
    this.resultData = data;
  }

  getResultData() {
    return this.resultData;
  }

  getMarkedAttendence(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  markAttendence(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }
  gettrianers(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  getUsers(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  setCourse(summaryData, contentData) {
    this.courseDataContent = contentData;
    this.courseDataSummary = summaryData;
  }

  getDataContent() {
    return this.courseDataContent;
  }

  getDataSummary() {
    return this.courseDataSummary;
  }

  populateActivityCompletion(activity) {
    if (!activity) return null;

    let userdetail: any = {};
    if (localStorage.getItem("userdetail")) {
      userdetail = JSON.parse(localStorage.getItem("userdetail"));
    }
    let completion = {
      id: activity.activitycompletionid,
      activityId: activity.activityId,
      contentwatchedtime: activity.contentwatchedtime,
      contenttime: activity.contenttime,
      enrolId: activity.enrolId,
      moduleId: activity.moduleId,
      courseId: activity.courseId,
      //userid : activity.employeeId,
      userid: userdetail.userId,
      tenantId: activity.tenantId,
      viewed: 1,
      completionstatus: activity.completionstatus,
      completionstate:
        activity.completionstate == undefined ? 0 : activity.completionstate,
      languageId: activity.defaultlangId,
    };
    return completion;
  }
  saveActivityCompletion(activity) {
    let param = this.populateActivityCompletion(activity);
    let url = ENUM.domain + ENUM.url.saveActivityCompletion;
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }

  callPostApi(url, param) {
    return new Promise((resolve) => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          (data) => {
            resolve(data);
          },
          (err) => {
            resolve(err);
            if (err) {
              // this.toastr.error('Please check server connection','Server Error!');
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }
}
