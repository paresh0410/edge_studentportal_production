import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SendFeedbackService {

  constructor(private _httpClient: HttpClient) { }

  inserthelpfb(url, param) {
    console.log('Data recived ==>', url, param);
    return new Promise(resolve => {
      this._httpClient.post(url, param)
        //.map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve('err');
          });
    });
  }
}
