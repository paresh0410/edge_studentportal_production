import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { HttpClient } from '@angular/common/http';
import {ENUM} from '../service/enum';
// import Language from "../modal/language.ts";

@Injectable({
  providedIn: "root",
})
export class MultiLanguageService {

  defaultLangugae = {
    languageId: null,
    languageName : 'Select Language',
    languageCode : null,
  };
  private language = new BehaviorSubject(this.defaultLangugae);
  selectedLanguage = this.language.asObservable();


  private activityLanguage = new BehaviorSubject(null);
  selectedactivityLanguage = this.activityLanguage.asObservable();
  constructor(private http: HttpClient) {}

  updateLangugae(langugae, updateInDB){
    // this.userService.editUser(this.newUser);
    this.language.next(langugae);
    if(updateInDB){
      this.updateDefaultLanguageInDB(langugae);
    }
  }

  callApi(url,param){
    return new Promise(resolve => {
        this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve(err);
            if(err){
                // this.toastr.error('Please check server connection','Server Error!');
            }
        });
    });
  }

  updateDefaultLanguageInDB(langData){
    const url = ENUM.domain + ENUM.url.updateUserLanguage;
    const empId = localStorage.getItem('employeeId');
    // const param = {
    //   empId: empId,
    //   langId : langData.languageId,
    // };
    // this.callApi(url, param).then((res) => {
    //   console.log("updateDefaultLanguageInDB", res);
    //   if(res['type'] && res['data']){
    //     // this.languageList = res['data'];
    //   }else {
    //     // console.log('Language Data List is empty');
    //   }

    // })
    // .catch(function (err) {
    //   console.log('Language Data Api Error', err);
    // });
  }

  updateActivityLanguage(selectedActivity){
    this.activityLanguage.next(selectedActivity);
  }

}
