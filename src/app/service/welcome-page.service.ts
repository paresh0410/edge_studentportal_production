import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PageRouteEnum } from '../models/pageroute.enum';

@Injectable({providedIn: 'root'})
export class WelcomePageService {

    constructor(private routes: ActivatedRoute, private router: Router) {}

    onRoleRedirect(rolename: string = '') {
        if (!rolename) return rolename;

        rolename = String(rolename).toLowerCase();
        if (rolename === PageRouteEnum.LEARNER) {
            this.router.navigate(['../pages/dashboard'], { relativeTo: this.routes });
		} else if (rolename === PageRouteEnum.TRAINER) {
            this.router.navigate(['../pages/trainer-dashboard'], { relativeTo: this.routes });
		} else if (rolename === PageRouteEnum.PARTNER) {
            this.router.navigate(['../pages/partner'], { relativeTo: this.routes });
		} else if (rolename === PageRouteEnum.MANAGER) {
            this.router.navigate(['../pages/team-dashboard'], { relativeTo: this.routes });
		}
    }
}
