import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService {
  constructor(private http: HttpClient, private toastr: ToastrService) {}
  checkTokenAuthentication(url, param) {
    return new Promise(resolve => {
      this.http
        .post(url, param)
        // .map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            resolve(err);
            if (err) {
              // this.toastr.error(
              //   'Please check server connection',
              //   'Server Error!'
              // );
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          }
        );
    });
  }
}
