import { TestBed } from '@angular/core/testing';

import { EEPService } from './eep.service';

describe('EEPService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EEPService = TestBed.get(EEPService);
    expect(service).toBeTruthy();
  });
});
