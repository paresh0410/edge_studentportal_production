import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the PulseServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PulseServiceProvider{

    constructor(public http: HttpClient, private toastr: ToastrService) {

    }

    getPulseData(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     this.toastr.warning('Unable to get pulse list','Warning!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }

    pushPulseData(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.warning('Please check server connection','Server Error!').onHidden.subscribe(()=>
                    // this.toastr.clear());
                }
            });
        });
    }

    getTrendingTags(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.warning('Please check server connection','Server Error!').onHidden.subscribe(()=>
                    // this.toastr.clear());
                }
            });
        });
    }

    getTrendingTagsData(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.warning('Please check server connection','Server Error!').onHidden.subscribe(()=>
                    // this.toastr.clear());
                }
            });
        });
    }

    addFeedback(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.warning('Please check server connection','Server Error!').onHidden.subscribe(()=>
                    // this.toastr.clear());
                }
            });
        });
    }

    removeFeedback(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     this.toastr.warning('Please check server connection','Server Error!').onHidden.subscribe(()=>
                //     this.toastr.clear());
                // }
            });
        });
    }

}
