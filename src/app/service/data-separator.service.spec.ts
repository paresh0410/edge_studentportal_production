import { TestBed } from '@angular/core/testing';

import { DataSeparatorService } from './data-separator.service';

describe('DataSeparatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataSeparatorService = TestBed.get(DataSeparatorService);
    expect(service).toBeTruthy();
  });
});
