import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CourseDetailServiceProvider{
    completedList:any = [];
    partialCompletedList:any = [];
    upcomingCourse:any = [];
    feedBackSummary:any;
    constructor(private http: HttpClient, private toastr: ToastrService) {

        // this.completedList = [
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '01',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         coins: 12,
        //         wCompleted: true,
        //         completed: '12th May 2018',
        //         bookmarked: false,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '02',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         coins: 12,
        //         wCompleted: true,
        //         completed: '12th May 2018',
        //         bookmarked: false,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '03',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         coins: 12,
        //         wCompleted: true,
        //         completed: '12th May 2018',
        //         bookmarked: false,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '04',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         coins: 12,
        //         wCompleted: true,
        //         completed: '12th May 2018',
        //         bookmarked: false,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '05',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         coins: 12,
        //         wCompleted: true,
        //         completed: '12th May 2018',
        //         bookmarked: false,
        //     }
        // ];

        // this.partialCompletedList = [
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '01',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         coins: 12,
        //         wCompleted: true,
        //         completed: '12th May 2018',
        //         bookmarked: false,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '02',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '03',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '02',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '04',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '03',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '05',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '04',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     }
        // ];

        // this.upcomingCourse = [
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '01',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         // dependant: '01',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '02',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '01',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '03',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '02',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '04',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '03',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     },
        //     {
        //         img: 'assets/images/open-book-leaf.jpg',
        //         courseSrNo: '05',
        //         title: 'Design Reporting Analysis',
        //         desc: 'Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui. In malesuada enim in dolor euismod, id commodo mi consectetur. Curabitur at vestibulum nisi. Nullam vehicula nisi velit. Mauris turpis nisl, molestie ut ipsum et, suscipit vehicula odio. Vestibulum interdum.',
        //         dependant: '04',
        //         wCompleted: false,
        //         completed: '',
        //         bookmarked: true,
        //     }
        // ];

    }

    getCourseDetailsData(url,param){

        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                    // this.toastr.error('Please check server connection','Server Error!');
                    // this.toastr.warning('Something went wrong please try again later', 'Warning!');
                }
            });
        });
    }

    getCompletedList(){
        return this.completedList;
    }

    getPartialCompletedList(){
        return this.partialCompletedList;
    }

    getUpcomingCourse(){
        return this.upcomingCourse;
    }

    setFeedbackSummary(data){
        this.feedBackSummary = data;
    }

    getFeedbackSummary(){
        return this.feedBackSummary;
    }

}
