import { Injectable } from '@angular/core';
import { ENUM } from './enum';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class ScormService {
	baseUrl: any = ENUM.domain;
	getScormListUrl: any = this.baseUrl + ENUM.url.getScormList;
	saveScormTracklisturl: any = this.baseUrl + ENUM.url.saveScormTrackList;
	viewScormPackageUrl: any = this.baseUrl + ENUM.url.viewScormPackage;
	getScormPackageUrl: any = this.baseUrl + ENUM.url.getScormPackage;
	deleteScormPackageUrl: any = this.baseUrl + ENUM.url.deleteScormPackage;
	private headers = new HttpHeaders();

	constructor(private http: HttpClient) { }

	getScormList(data: any): Promise<any> {
		// const url = this.getScormListUrl;
		return this.http.post(this.getScormListUrl, data, { headers: this.headers })
			.toPromise()
			.then(response => {
				return response = response;
			})
			.catch(this.handleError);
	}
	viewScormPackage(data: any): Promise<any> {
		const url = this.getScormPackageUrl;
		// const url = this.getScormPackageUrl + '?id=' + data.id + '&key=' + data.key + '&extension=' + data.extension + '&eid=' + data.eid;
		return this.http.post(url, data, { headers: this.headers })
			.toPromise()
			.then(response => {
				return response = response;
			})
			.catch(this.handleError);
	}
	deleteScormPackage(id: any): Promise<any> {
		// const url = this.getScormListUrl;
		const url = this.deleteScormPackageUrl + '?id=' + id;
		return this.http.get(url, { headers: this.headers })
			.toPromise()
			.then(response => {
				return response = response;
			})
			.catch(this.handleError);
	}
	saveScormTrackList(list): Promise<any> {
		return this.http.post(this.saveScormTracklisturl, list, { headers: this.headers })
			.toPromise()
			.then(response => {
				return response = response;
			})
			.catch(this.handleError);
	}

	private handleError(error: Response | any) {
		return Promise.reject(error.message || error);
	}

}

