import {Injectable} from "@angular/core";
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {webApi} from './webApi';

@Injectable() 
export class webAPIService {
url:any;
apiUrls:any = webApi;

    constructor(public http: HttpClient) {
        this.url = webApi.url;
    }
    
    getService(url,param){

        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve('err');
            });
        });
    }
}