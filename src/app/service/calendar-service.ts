import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/*
  Generated class for the CalendarServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CalendarServiceProvider {
    data:any =[];
    constructor(private http: HttpClient, private toastr: ToastrService) { }

    getEvents(url,param){

        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     // this.toastr.error('Please check server connection','Server Error!');
                //     this.toastr.warning('Something went wrong please try again later', 'Warning!');
                // }
            });
        });
    }

    enroltrainer(url, param) {
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     // this.toastr.error('Please check server connection','Server Error!');
                //     this.toastr.warning('Something went wrong please try again later', 'Warning!');
                // }
            });
        });
    }

    confirmbatch(url, param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     // this.toastr.error('Please check server connection','Server Error!');
                //     this.toastr.warning('Something went wrong please try again later', 'Warning!');
                // }
            });
        });
    }

    getdata(url, param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                // if(err){
                //     // this.toastr.error('Please check server connection','Server Error!');
                //     this.toastr.warning('Something went wrong please try again later', 'Warning!');
                // }
            });
        });
    }
}
