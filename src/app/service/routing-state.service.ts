import { Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Location } from '@angular/common';
import { EventsService } from './events.service';

@Injectable({
  providedIn: 'root',
})
export class RoutingStateService {

  private history = [];

  constructor(
    private router: Router,
    private location: Location,
    public events: EventsService,
  ) {}

  public loadRouting(): void {
    // this.router.events
    //   .pipe(filter(event => event instanceof NavigationEnd))
    //   .subscribe(({urlAfterRedirects}: NavigationEnd) => {
    //     this.history = [...this.history, urlAfterRedirects];
    //   });
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 2] || '/index';
  }

  goBack() {
    this.location.back();
  }

  goForward() {
    this.location.forward();
  }

  getPath() {
    return this.location.path();
  }

  goToPath() {
    this.location.go("/product/1");
  }

  replacePath() {
    this.location.replaceState("/product/1");
  }

  getState() {
    // this.location.getState();
  }

  getUrlChnageEvent() {
    // this.location.onUrlChange( (url: string, state: unknown) => {
    //   console.log("Location changes to "+url);
    //   console.log(state);
    // })
  }

  getPopStateEvent() {
    this.location.subscribe(
      ( (value:PopStateEvent) => {
        console.log("locaton OnNext")
        console.log(value);
      }),
      ( ex => {
        console.log("Error occured postate event")
        console.log(ex);
      })
    )
  }

  subscribeToGetparams() {
    this.router.events
    .pipe(
        filter(e => (e instanceof ActivationEnd) && (Object.keys(e.snapshot.params).length > 0)),
        map(e => e instanceof ActivationEnd ? e.snapshot.params : {})
      )
    .subscribe(params => {
        console.log('Routing params : ', params);
        // Do whatever you want here!!!!
        if (params.search) {
          this.publishEvent(params.search);
        } else {
          this.publishEvent('');
        }
      });
  }

  getRouterDetails() {
    console.log('1 : ', this.router.routerState.root);
    console.log('2 : ', this.router.routerState.root.firstChild);
    console.log('3 : ', this.router.routerState.root.firstChild && this.router.routerState.root.firstChild.firstChild);
  }

  publishEvent(params) {
    this.events.publish('learn:search', {
      data: params,
      status: true,
    });
  }

}
