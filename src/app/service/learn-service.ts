import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from './enum';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
/*
  Generated class for the MenuServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable({
  providedIn: 'root',
})
export class LearnServiceProvider {
    list:any = [];
    subList:any = [];
	courseDetailList = [];
	workFlowData: any;

   subject = new BehaviorSubject(123);
	baseUrl = ENUM.domain;

  get_all_learning_pathway_courses_by_workflow_id_url = this.baseUrl + ENUM.url.list_all_learning_pathway_courses;

  private headers = new HttpHeaders();
	// getLearnTabsListUrl = this.baseUrl + ENUM.url.get_all_leran_tabs;
	getLearnCourseListUrl = this.baseUrl + ENUM.url.get_all_leran_course_list;
	// getCourseListUrl = this.baseUrl + ENUM.url.get_all_course_list;
	selfEnrolCourses = this.baseUrl + ENUM.url.selfEnrol;
	submitCoursefeedback = this.baseUrl + ENUM.url.addCourseFeedback;
	removeCoursefeedback = this.baseUrl + ENUM.url.removeCourseFeedback;
	// get_all_learning_pathway_courses_by_workflow_id_url = this.baseUrl + ENUM.url.list_all_learning_pathway_courses;
	// get_all_learn_courses_by_search_filter_url = this.baseUrl + ENUM.url.list_all_learn_courses_by_search_filter;
	// get_all_learn_data_by_firebase_dynamic_url = this.baseUrl + ENUM.url.get_learn_data_by_firebase_dynamic_link;

  breadCrumbs: any = [];

    constructor(private http: HttpClient, private toastr: ToastrService) {

        // this.list = [
		// 	{
		// 		id: 1,
		// 		name: 'All',
		// 		nocourse: '23 Courses',
		// 		sub:[
		// 			{
		// 				number: 2,
		// 				name: 'CLASSROOM',
		// 			},{
		// 				number: 10,
		// 				name: 'ONLINE',
		// 			}
		// 		],
		// 	},{
		// 		id: 2,
		// 		name: 'Mandatory',
		// 		nocourse: '12 Courses',
		// 		sub:[
		// 			{
		// 				number: 2,
		// 				name: 'CLASSROOM',
		// 			},{
		// 				number: 10,
		// 				name: 'ONLINE',
		// 			}
		// 		],
		// 	},{
		// 		id: 3,
		// 		name: 'Aspirational',
		// 		nocourse: '12 Courses',
		// 		sub:[
		// 			{
		// 				number: 2,
		// 				name: 'CLASSROOM',
		// 			},{
		// 				number: 10,
		// 				name: 'ONLINE',
		// 			}
		// 		],
		// 	},{
		// 		id: 4,
		// 		name: 'Recommended',
		// 		nocourse: '12 Courses',
		// 		sub:[
		// 			{
		// 				number: 2,
		// 				name: 'CLASSROOM',
		// 			},{
		// 				number: 10,
		// 				name: 'ONLINE',
		// 			}
		// 		],
		// 	},
        // ];

		this.subList = [
			{
				id: 1,
				category: 'Complete',
				subData: [
					{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Mandatory',
						typeImage: 'assets/images/orangeLearn.svg',
						complete_courses: 5,
						total_courses: 5,
						completed: '05/05',
						percComplete: 100,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: '',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Recommended',
						typeImage: 'assets/images/tickLearn.svg',
						complete_courses: 5,
						total_courses: 5,
						completed: '05/05',
						percComplete: 100,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: '',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Aspirational',
						typeImage: 'assets/images/mountLearn.svg',
						complete_courses: 5,
						total_courses: 5,
						completed: '05/05',
						percComplete: 100,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: '',
					}
				],
			},{
				id: 2,
				category: 'Current',
				subData: [
					{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Mandatory',
						typeImage: 'assets/images/orangeLearn.svg',
						complete_courses: 3,
						total_courses: 5,
						completed: '03/05',
						percComplete: 60,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'STAGE 4: Market Analysis',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Aspirational',
						typeImage: 'assets/images/mountLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'This course is about achieving market…',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Recommended',
						typeImage: 'assets/images/tickLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'This course is about achieving market…',
					}
				],
			},{
				id: 3,
				category: 'Upcoming',
				subData: [
					{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Aspirational',
						typeImage: 'assets/images/mountLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'ONLINE',
						type2Image: 'assets/images/online.svg',
						nextStep: 'This course is about achieving market…',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Recommended',
						typeImage: 'assets/images/tickLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'This course is about achieving market…',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Recommended',
						typeImage: 'assets/images/tickLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'Online',
						type2Image: 'assets/images/online.svg',
						nextStep: 'This course is about achieving market…',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Mandatory',
						typeImage: 'assets/images/orangeLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'This course is about achieving market…',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Aspirational',
						typeImage: 'assets/images/mountLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'This course is about achieving market…',
					},{
						title: 'Sales & Marketing',
						desc: 'As someone who transitioned into UX from a non-design career with actionable things beginners can do to land their first job in this amazing industry.',
						date: '26 Jan 2018',
						type: 'Mandatory',
						typeImage: 'assets/images/orangeLearn.svg',
						complete_courses: 0,
						total_courses: 5,
						completed: '00/05',
						percComplete: 2,
						type2: 'CLASSROOM',
						type2Image: 'assets/images/classroom.svg',
						nextStep: 'This course is about achieving market…',
					}
				],
			}
		]

    }

    getLocalList(){
        return this.list;
	}

	getLocalSubList(){
		return this.subList;
	}

	getLearnData(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
          //       if(err){
          //           // this.toastr.error('Please check server connection','Server Error!');
          //           this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					// this.toastr.clear());
          //       }
            });
        });
    }

	setcourseDetailList(paramData){
		this.courseDetailList = paramData;
	}

	getcourseDetailList(){
		return this.courseDetailList;
	}
	addCourseFeedback(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
          //       if(err){
          //           // this.toastr.error('Please check server connection','Server Error!');
          //           this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					// this.toastr.clear());
          //       }
            });
        });
    }

    removeCourseFeedback(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
          //       if(err){
          //           // this.toastr.error('Please check server connection','Server Error!');
          //           this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					// this.toastr.clear());
          //       }
            });
        });
	}

	selfEnrolCourse(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
          //       if(err){
          //           // this.toastr.error('Please check server connection','Server Error!');
          //           this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					// this.toastr.clear());
          //       }
            });
        });
    }

	setWorkflowData(data) {
		this.workFlowData = data;
	}

	getWorkflowData(){
		return this.workFlowData;
	}

	getLearningPathwayCoursesList(url,learnData) {
		return new Promise(resolve => {
            this.http.post(url, learnData)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
          //       if(err){
          //           // this.toastr.error('Please check server connection','Server Error!');
          //           this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
					// this.toastr.clear());
          //       }
            });
        });
  }
  /********* new api services start *********/
  /********* new api services end *********/
}
