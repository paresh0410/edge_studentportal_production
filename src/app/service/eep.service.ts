import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EEPService {
  workflow: any = [];
  cocProfileData: any;
  coursecontent: any = [];
  participantdata: any = [];
  activityData: any;
  currentstepdata: any = [];
  constructor(private http: HttpClient, private toastr: ToastrService) { }

  setActivityData(data) {
    this.activityData = data;
  }

  getActivityData() {
    return this.activityData;
  }

  get(url, param) {
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            // if (err) {
            //   // this.toastr.error('Please check server connection', 'Server Error!');
            //   this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
            //   this.toastr.clear());
            // }
          });
    });
  }
  updateProfileData(url, param) {
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            // if (err) {
            //   // this.toastr.error('Please check server connection', 'Server Error!');
            //   this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
            //   this.toastr.clear());
            // }
          });
    });
  }
}
