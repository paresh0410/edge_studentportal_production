import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";
import { EventsService } from '../service/events.service';
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { EMPTY, NEVER } from "rxjs";
@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(public toastr: ToastrService, public events: EventsService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem("token");

    if (token) {
      request = request.clone({
        headers: request.headers.set("Authorization", "Bearer " + token),
      });
    }

    if (!request.headers.has("Content-Type")) {
      request = request.clone({
        headers: request.headers.set("Content-Type", "application/json"),
      });
    }
    if (!request.headers.has("Access-Control-Allow-Origin")) {
      request = request.clone({
        headers: request.headers.set("Access-Control-Allow-Origin", "*"),
      });
    }
    if (!request.headers.has("Access-Control-Allow-Methods")) {
      request = request.clone({
        headers: request.headers.set(
          "Access-Control-Allow-Methods",
          "GET,PUT,POST,DELETE"
        ),
      });
    }
    if (!request.headers.has("Access-Control-Allow-Headers")) {
      request = request.clone({
        headers: request.headers.set(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        ),
      });
    }
    if (!request.headers.has("Access-Control-Allow-Headers")) {
      request = request.clone({
        headers: request.headers.set(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        ),
      });
    }
    const customeHeadersData = this.makeCustomHeaderDataReady();
    if (customeHeadersData) {
      request = request.clone({
        headers: request.headers.set(
          "Custom-Header",
          JSON.stringify(customeHeadersData)
        ),
      });
    }
    // request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
    request = request.clone({
      headers: request.headers.set("Content-Type", "application/json"),
    });
    console.log("request--->>>", request);
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log("event--->>>", event);
          if (event.body.success == false) {
            console.log(event.body);
            // this.toastr.error(event.body.message,'Server Error!');
            localStorage.clear();
            window.open("#/login", "_self");

            console.log("Unauthorised access");
          }
          // this.errorDialogService.openDialog(event);
        }
        return event;
      }),
      catchError((err: HttpErrorResponse) => {
        console.log("err", err);
        let data = {};
        data = {
          reason: err && err.error.reason ? err.error.reason : "",
          status: err.status,
        };
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            // redirect to the login route
            // or show a modal
            this.events.publish('tokenExpired', {
              data: null,
              status: true,
            });
            console.log(err);
            // localStorage.clear();
            this.clearLocalStorage();
            this.checkAndSetDynamicLink();
            window.open("#/login-new", "_self");
            console.log("Unauthorised access");
            return NEVER;
          } else {
            return throwError(err);
          }
        }
      })
    );
  }

  makeCustomHeaderDataReady() {
    if (localStorage.getItem("userdetail")) {
      const currentUserData = JSON.parse(localStorage.getItem("userdetail"));
      if (currentUserData) {
        const fullName =
          currentUserData.firstname + " " + currentUserData.lastname;
        const customeHeadersData = {
          userId: currentUserData.id,
          userName: currentUserData.username,
          userEmail: currentUserData.email,
          fullName: fullName,
          roleId: currentUserData.roleId,
          roleName: currentUserData.roll,
          platform: "Webportal",
          userLangugageId:
            currentUserData && currentUserData["userLang"]
              ? currentUserData["userLang"].languageId
              : null,
          userLangugageName:
            currentUserData && currentUserData["userLang"]
              ? currentUserData["userLang"].languageName
              : null,
          // userLangugageId: null,
          // userLangugageName: null,
          platformId: 3,
        };
        return customeHeadersData;
      } else {
        return null;
      }
    }
  }

  checkAndSetDynamicLink() {
    let currentURL = window.location.hash;
    currentURL = String(currentURL).replace('#', '')
    if(String(currentURL).indexOf('dynamic-link-learn') !== -1) {
      localStorage.setItem('redirectURL', currentURL.replace('#', ''));
    }
    console.log('currentURL', currentURL);
  }

  clearLocalStorage(){
    localStorage.removeItem('userDetails');
    localStorage.removeItem('employeeId');
    localStorage.removeItem('userOtherDetails');
    localStorage.removeItem('roleName');
    localStorage.removeItem('token');
    localStorage.removeItem('userdetail');
  }
}
