/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// import './polyfills';

import { APP_BASE_HREF, CommonModule } from "@angular/common";
import {
  NgModule,
  ApplicationRef,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  RendererFactory2,
} from "@angular/core";
import { MatTabsModule } from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { CoreModule } from "./@core/core.module";
import { HttpModule } from "@angular/http";
// import { QRCodeModule } from 'angularx-qrcode';
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxFsModule } from "ngx-fs";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatNativeDateModule } from "@angular/material";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

// import { AppComponent } from './app.component';
import { AppComponent } from "./app-new.component";

import { AppRoutingModule } from "./app-routing.module";
import { ThemeModule } from "./@theme/theme.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { Ng2CarouselamosModule } from "ng2-carouselamos";
import { NgxPasswordToggleModule } from "ngx-password-toggle";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { DemoMaterialModule } from "./pages/material-module";
// import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
// import { MatFormFieldModule, MatSelectModule } from '@angular/material';
import "hammerjs";
import { LoginComponent } from "./pages/login/login.component";
import { LogoutComponent } from "./pages/logout/logout.component";
import { SignupComponent } from "./pages/signup/signup.component";
import { OtpComponent } from "./pages/otp/otp.component";
import { WelcomePageComponent } from "./pages/welcome-Page/welcome-page.component";
import { ForgotPasswordComponent } from "./pages/forgot-password/forgot-password.component";
import { ResetPwdComponent } from "./pages/reset-pwd/reset-pwd.component";
import { InterestsComponent } from "./pages/interests/interests.component";
import { ErrorDialogComponent } from "./error-dailog/errordialog.component";
//import { DocumentViewerComponent } from './pages/document-viewer/documentviewer.component';

import { webAPIService } from "./service/webAPIService";
import { LearnServiceProvider } from "./service/learn-service";
import { CourseDetailServiceProvider } from "./service/course-detail-service";
import { CourseServiceProvider } from "./service/course-service";
import { InterestsServiceProvider } from "./service/interests-service";
import { PulseServiceProvider } from "./service/pulse-service";
import { AuthServiceProvider } from "./service/auth-service";
import { UserChatService } from "./service/user-chat-service";
import { LogServices, LogEnum, AreaEnum } from "./service/logservice";
import { ErrorDialogService } from "./error-dailog/errordialog.service";
import { ProfileServiceProvider } from "./service/profile-service";
import { PathwayServiceProvider } from "./service/pathway-service";
import { PollServiceProvider } from "./service/poll-service";
import { SurveyServiceProvider } from "./service/survey-service";
import { FeedbackServiceProvider } from "./service/feedback-service";
import { CalendarServiceProvider } from "./service/calendar-service";
import { TeamDashboardServiceProvider } from "./service/team-dashboard.service";
import { DashboardServiceProvider } from "./service/dashboard.service";
import { UserService } from "./service/user.service";
import { TrainerAutomationServiceProvider } from "./service/trainer-automation.service";
import { NotificationServiceProvider } from "./service/notification.service";
import { TrainTheTrainerServiceProvider } from "./service/traine-the-trainer.service";
import { SettingsServiceProvider } from "./service/settings.service";
import { LearnSearchServiceProvider } from "./service/learn-search.service";
// import { DocumentViewerService } from './pages/document-viewer/documentviewer.service';
// import { SafePipeModule } from 'safe-pipe';
import { HttpConfigInterceptor } from "./interceptor/httpconfig.interceptor";
// import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from "../environments/environment";
// import { SafePipePipe } from './safe-pipe.pipe';
// import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { MsalModule, MsalInterceptor, MsalService } from "@azure/msal-angular";
import { MSAL_CONFIG } from "@azure/msal-angular/dist/msal.service";
import { StarRatingModule } from "angular-star-rating";
import { LogLevel } from "msal";
// import { CcCallsListComponent } from '../app/pages/cc-calls-list/cc-calls-list.component';
// import { TrainerTAevalutionfbComponent } from '../../../trainer-taevalutionfb/trainer-taevalutionfb.component';
import { OpenviduSessionModule } from "openvidu-angular";
import { IndexComponent } from "./index/index.component";
import { PrivacyComponent } from "./pages/privacy/privacy.component";
// import { LoginSignupComponent } from './pages/login-signup/login-signup.component';
import { LoginNewComponent } from "./pages/login-new/login-new.component";

// import { SafePipe } from './pipes/safe.pipe';
// import { NullvaluePipe } from './pipes/nullvalue.pipe';
// import { SafePipe } from './safe.pipe';
export function loggerCallback(logLevel, message, piiEnabled) {
  // console.log('client logging' + message);
}

export function msalconfig() {
  const isIE =
    window.navigator.userAgent.indexOf("MSIE ") > -1 ||
    window.navigator.userAgent.indexOf("Trident/") > -1;
  const isEdge = !isIE && !!window["StyleMedia"];

  return {
    clientID: "", // 'bcd88b9b-1ef1-4f07-a923-05245da7d5f6',
    authority: "", // 'https://login.microsoftonline.com/bflfinatics.onmicrosoft.com/',
    validateAuthority: true,
    redirectUri: window.location.origin, // 'http://localhost:4200/',
    // cacheLocation: (isIE || isEdge) ? 'sessionStorage' : 'localStorage',
    cacheLocation: "localStorage",
    storeAuthStateInCookie: isIE || isEdge,
    postLogoutRedirectUri: window.location.origin + "/#/login", // 'http://localhost:4200/#/login',
    navigateToLoginRequestUrl: true,
    popUp: false,
    consentScopes: [
      "user.read",
      "api://54b9f2a8-dc1f-4173-8955-f762e6fd0b37/access_as_user",
    ],
    unprotectedResources: ["https://www.microsoft.com/en-us/"],
    // protectedResourceMap: protectedResourceMap,
    logger: loggerCallback,
    correlationId: "1234",
    level: LogLevel.Info,
    piiLoggingEnabled: true,
  };
}
export const protectedResourceMap: [string, string[]][] = [
  [
    "https://buildtodoservice.azurewebsites.net/api/todolist",
    ["api://54b9f2a8-dc1f-4173-8955-f762e6fd0b37/access_as_user"],
  ],
  ["https://graph.microsoft.com/v1.0/me", ["user.read"]],
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    SignupComponent,
    PrivacyComponent,
    OtpComponent,
    WelcomePageComponent,
    ForgotPasswordComponent,
    ResetPwdComponent,
    //DocumentViewerComponent,
    InterestsComponent,
    ErrorDialogComponent,
    IndexComponent,
    LoginNewComponent,
    // WebinarMultiJitsiComponent,
    // LoginSignupComponent,
    // MadetoryquestionpipePipe,
    // SafePipe,
    // NullvaluePipe,
    // SafePipe,
    // CcCallsListComponent,
    // TrainerTAevalutionfbComponent
    // SafePipePipe,
  ],
  imports: [
    NgxSpinnerModule,
    // CommonModule,
    BrowserModule,
    MatTabsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    // QRCodeModule,
    Ng2CarouselamosModule,
    NgxPasswordToggleModule,
    NgxFsModule,
    NgbModule.forRoot(),
    // NbTabsetModule,
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    StarRatingModule.forRoot(),
    FormsModule,
    DemoMaterialModule,
    MatNativeDateModule,
    // MatSelectModule,
    // MatFormFieldModule,
    // NgxMatSelectSearchModule,
    ReactiveFormsModule,
    MsalModule,
    // MsalModule.forRoot(msalconfig()),
    // NativeScriptModule,
    //SafePipeModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: "toast-top-right",
      preventDuplicates: true,
      maxOpened: 1,
      autoDismiss: true,
      closeButton: true,
      progressBar: true,
    }),
  ],
  bootstrap: [AppComponent],
  entryComponents: [ErrorDialogComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: "/" },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },

    // ApplicationRef,
    MsalService,
    {
      provide: MSAL_CONFIG,
      useValue: msalconfig(),
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true,
    },
    webAPIService,
    LearnServiceProvider,
    CourseDetailServiceProvider,
    CourseServiceProvider,
    InterestsServiceProvider,
    PulseServiceProvider,
    AuthServiceProvider,
    UserChatService,
    LogServices,
    LogEnum,
    AreaEnum,
    ErrorDialogService,
    ProfileServiceProvider,
    PathwayServiceProvider,
    PollServiceProvider,
    SurveyServiceProvider,
    FeedbackServiceProvider,
    CalendarServiceProvider,
    TeamDashboardServiceProvider,
    DashboardServiceProvider,
    UserService,
    TrainerAutomationServiceProvider,
    NotificationServiceProvider,
    TrainTheTrainerServiceProvider,
    SettingsServiceProvider,
    LearnSearchServiceProvider,
    // DocumentViewerService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AppModule {}

// platformBrowserDynamic().bootstrapModule(AppModule);
