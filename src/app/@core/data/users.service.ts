
import { of as observableOf,  Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

class User {
  name: string;
  picture: string;
}

export class Contacts {
  user: User;
  type: string;
}

export class RecentUsers extends Contacts {
  time: number;
}

@Injectable()
export class UserService {

  	private time: Date = new Date;
	
	// private users = {
    	// nick: { name: 'Amanda Seyfried', picture: 'assets/images/Bitmap.png' },
    	// eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
		// jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
		// lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
		// alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
		// kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
	// };
	  
    private users = {
		name: '',
		picture: ''
	}
	public userDetails:any = {};
	public name = '';
	public picture = '../../../assets/images/user.png';
	private types = {
    	mobile: 'mobile',
		home: 'home',
		work: 'work',
	};
	private dataSource = new BehaviorSubject(this.picture);
	data = this.dataSource.asObservable();
	private contacts: Contacts[] = [
		// { user: this.users.nick, type: this.types.mobile },
		// { user: this.users.eva, type: this.types.home },
		// { user: this.users.jack, type: this.types.mobile },
		// { user: this.users.lee, type: this.types.mobile },
		// { user: this.users.alan, type: this.types.home },
		// { user: this.users.kate, type: this.types.work },
  	];
  
	private recentUsers: RecentUsers[]  = [
		// { user: this.users.alan, type: this.types.home, time: this.time.setHours(21, 12)},
		// { user: this.users.eva, type: this.types.home, time: this.time.setHours(17, 45)},
		// { user: this.users.nick, type: this.types.mobile, time: this.time.setHours(5, 29)},
		// { user: this.users.lee, type: this.types.mobile, time: this.time.setHours(11, 24)},
		// { user: this.users.jack, type: this.types.mobile, time: this.time.setHours(10, 45)},
		// { user: this.users.kate, type: this.types.work, time: this.time.setHours(9, 42)},
		// { user: this.users.kate, type: this.types.work, time: this.time.setHours(9, 31)},
		// { user: this.users.jack, type: this.types.mobile, time: this.time.setHours(8, 0)},
	];

	constructor(){
		if(localStorage.getItem('userDetails')){
			this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
		}
	}
  	getUsers() {
			if(localStorage.getItem('userDetails')){
				this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
				if(this.userDetails){
					// + ' ' + this.userDetails.lastname
					if(this.userDetails.firstname >= 15){
						this.name = this.userDetails.firstname.substring(0, 15) + '...';
					}
					else{
						this.name = this.userDetails.firstname;
					}
					if (this.userDetails.picture_url != null){
						this.picture = this.userDetails.picture_url;
					}
					else{
						this.picture = '../../../assets/images/user.png';
					}
				} 
				this.users.name = this.name;
				this.users.picture = this.picture;
				console.log('USER--->',this.users);
			}
    	return this.users;
  	}

  	getContacts(): Observable<Contacts[]> {
    	return observableOf(this.contacts);
  	}

  	getRecentUsers(): Observable<RecentUsers[]> {
    	return observableOf(this.recentUsers);
	  }
	updatedDataSelection(data){
		this.dataSource.next(data);
	}
}
