export class Language{

  constructor(private langId: number, private langName: string, private langCode: string){

  }


  public get languageId(): Number {
    return this.langId;
  }

  public get languageName(): String {
    return this.langName;
  }
  public get languageCode(): String {
    return this.langCode;
  }
}
