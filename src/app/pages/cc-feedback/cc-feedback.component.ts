import { Component, OnInit, ViewEncapsulation, Input, OnChanges } from '@angular/core';
import { CallCoachingService } from '../../service/call-coaching.service';
import { ENUM } from '../../service/enum';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'ngx-cc-feedback',
  templateUrl: './cc-feedback.component.html',
  styleUrls: ['./cc-feedback.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CcFeedbackComponent implements OnInit, OnChanges {
  @Input() fdata;
  @Input() calldetail;
  feedbacktemplateArr: any = [];
  employeeId: any;
  userDetails: any = [];
  activeRole: any;
  calls: any;
  userotherdetail: any;
  trainerId: any;
  subsections: any = [];


  role: any;
  constructor(public callservice: CallCoachingService,
    public datePipe: DatePipe, public Toastr: ToastrService) {
    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
    this.userotherdetail = JSON.parse(localStorage.getItem('userOtherDetails'));
    let uDetail = this.userotherdetail[1]
    for (let i = 0; i < uDetail.length; i++) {
      if (uDetail[i].roleId == 7) {
        this.trainerId = uDetail[i].fieldmasterId;
      }
    }
  }

  ngOnInit() {
    console.log(this.fdata);
    for (let i = 0; i < this.fdata.length; i++) {
      if (this.fdata[i].role === this.userDetails.roll) {
        this.calls = this.calldetail;
        this.role = this.userDetails.roleId;
        console.log(this.calls);
        console.log(this.feedbacktemplateArr);
        this.activeRole = i;
        // this.activateRole = true;
        this.selected(i, this.fdata[i]);
      }
      for (let j = 0; j < this.fdata[i].section.length; j++) {
        for (let k = 0; k < this.fdata[i].section[j].subsection.length; k++) {
          if (this.fdata[i].section[j].subsection[k].dataType == 'datetime') {
            this.fdata[i].section[j].subsection[k].value =
              new Date(this.fdata[i].section[j].subsection[k].value) ?  new Date(this.fdata[i].section[j].subsection[k].value) :
              new Date();
          }
        }
      }
    }

    this.feedbacktemplateArr = this.fdata;
    // // this.activeRole = 0;
    // this.subsections = this.feedbacktemplateArr[0].section;
    // if (this.feedbacktemplateArr.length == 1) {
    //   this.activateRole = true;
    // }
    console.log("Feedback contents -- ", this.subsections);
    // this.enableButtons = this.calldetail.dataFlag;
    // this.getfeedback(param);
  }
  // getfeedback(param) {
  //   const url = ENUM.domain + ENUM.url.cc_get_feedback;

  //   this.callservice.get(url, param).then(res => {
  //     console.log(res);
  //   }, err => {
  //     console.log(err);
  //   })
  // }
  activateRole: boolean;
  roleName: String;
  tabChanged(index, roleData) {
    console.log('Role Index--->', index);
    this.activeRole = index;
    this.subsections = roleData.section;
    console.log(this.subsections);

    if (roleData.roleId == this.role) {
      this.activateRole = true;
      this.roleName = roleData.role;
    } else {
      this.activateRole = false;
    }
  }
  selected(i, roleData) {
    this.activeRole = i;
    this.subsections = roleData.section;
    console.log(this.subsections);

    if (roleData.roleId == this.role) {
      this.activateRole = true;
      this.roleName = roleData.role;
    } else {
      this.activateRole = false;
    }
  }
  datetime(date) {

    console.log(date);
  }
  feedbackArrToPush: any = [];

  submit(data) {

    // if(this.calldetail.isEnable == 'Y'){
    //   console.log("Submitted data", data);
    //   if (data.length > 0) {
    //     for (let j = 0; j < data.length; j++) {
    //       let feedbackArrObjToPush: any = {};
    //       for (let i = 0; i < data[j].subsection.length; i++) {
    //         // console.log("Subsection ", data.subsection[i].subsection_name);
    //         // console.log("Subsection ", data.subsection[i].dataType);

    //         feedbackArrObjToPush = {
    //           role: this.roleName,
    //           section: data[j].section,
    //           subSection: data[j].subsection[i].subsection_name ? data[j].subsection[i].subsection_name : null,
    //           dataType: data[j].subsection[i].dataType ? data[j].subsection[i].dataType : null,
    //           roleId: data[j].subsection[i].roleId ? data[j].subsection[i].roleId : null,
    //           feedback: data[j].subsection[i].value ? data[j].subsection[i].value : null,
    //           ftId: data[j].subsection[i].ftId ? data[j].subsection[i].ftId : null,
    //         };
    //         this.feedbackArrToPush.push(feedbackArrObjToPush);
    //       }
    //     }
    //   }
    //   console.log('feedbackArrToPush', this.feedbackArrToPush);
    //   var allstr = this.getDataReadyForFeedbackCC(this.feedbackArrToPush);
    //   console.log('allstr', allstr)
    //   let param = {
    //     callId: this.calls.callId,
    //     userId: this.userDetails.id,
    //     roleId: this.userDetails.roleId,
    //     tId: this.userDetails.tenantId,
    //     allstr: allstr ? allstr : null,
    //   };
    //   console.log('param:', param);

    //   const url = ENUM.domain + ENUM.url.cc_add_feedback;

    //   this.callservice.get(url, param).then(res => {
    //     console.log(res);
    //     if (res['type'] == true) {
    //       // this.Toastr.success(res[data].msg);
    //       // window.history.back();
    //       this.Toastr.success('Feedback submitted successfully.');
    //     }
    //   }, err => {
    //     console.log(err);
    //   });
    // }
    // else{
    //   // this.Toastr.info(this.calldetail.msg,'Info');
    //   this.Toastr.info('Feedback will be submitted after call.','Info');
    // }

    console.log("Submitted data", data);
    if (data.length > 0) {
      this.feedbackArrToPush = []
      for (let j = 0; j < data.length; j++) {
        let feedbackArrObjToPush: any = {};
        for (let i = 0; i < data[j].subsection.length; i++) {
          // console.log("Subsection ", data.subsection[i].subsection_name);
          // console.log("Subsection ", data.subsection[i].dataType);

          feedbackArrObjToPush = {
            role: this.roleName,
            section: data[j].section,
            subSection: data[j].subsection[i].subsection_name ? data[j].subsection[i].subsection_name : null,
            dataType: data[j].subsection[i].dataType ? data[j].subsection[i].dataType : null,
            roleId: data[j].subsection[i].roleId ? data[j].subsection[i].roleId : null,
            feedback: data[j].subsection[i].value ? data[j].subsection[i].value : null,
            ftId: data[j].subsection[i].ftId ? data[j].subsection[i].ftId : null,
          };
          this.feedbackArrToPush.push(feedbackArrObjToPush);
        }
      }
    }
    console.log('feedbackArrToPush', this.feedbackArrToPush);
    var allstr = this.getDataReadyForFeedbackCC(this.feedbackArrToPush);
    console.log('allstr', allstr)
    let param = {
      callId: this.calls.callId,
      userId: this.userDetails.id,
      roleId: this.userDetails.roleId,
      tId: this.userDetails.tenantId,
      allstr: allstr ? allstr : null,
    };
    console.log('param:', param);

    const url = ENUM.domain + ENUM.url.cc_add_feedback;

    this.callservice.get(url, param).then(res => {
      console.log(res);
      if (res['type'] == true) {
        // this.Toastr.success(res[data].msg);
        // window.history.back();
        this.Toastr.success('Feedback submitted successfully.').onHidden.subscribe(()=>
        this.Toastr.clear());
      }
    }, err => {
      console.log(err);
    });
  }
  formatDate(date) {
    const t = new Date(date);
    // const formattedDateTime = this.datepipe.transform(t, 'yyyy-MM-dd h:mm');
    const year = t.getFullYear() + '-' + (t.getMonth() + 1) + '-' + t.getDate();
    const time = ' ' + t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();
    const formattedDateTime = year + time;
    return formattedDateTime;
  }
  getDataReadyForFeedbackCC(feedbackArr) {
    var section;
    var subSection;
    var roleId;
    var dataType;
    var feedback;
    var ftId;
    var Finalline;
    var entityId;

    for (let i = 0; i < feedbackArr.length; i++) {

      if (feedbackArr[i].subSection == null) {
        feedbackArr[i].subSection = '';
      }
      section = feedbackArr[i].section;
      subSection = feedbackArr[i].subSection;
      if(feedbackArr[i].dataType == 'datetime'){
        feedback = this.formatDate(feedbackArr[i].feedback);
      }else {
        feedback = feedbackArr[i].feedback;
      }
      dataType = feedbackArr[i].dataType;
      roleId = feedbackArr[i].roleId;
      ftId = feedbackArr[i].ftId;
      if (this.userDetails.roleId === 7) {
        // entityId = this.userDetails.referenceId;
        entityId = this.trainerId;
      } else {
        entityId = this.employeeId;
      }
      console.log('entityId', entityId);
      let line = section + "|" + subSection + "|" + feedback + "|" + dataType + "|" + ftId
        + "|" + entityId;
      if (i == 0) {
        Finalline = line;
      } else {
        Finalline += "#" + line;
      }

    }
    return Finalline;
    //console.log('Finalline',Finalline);
  }
  ngOnChanges(){
    console.log('data ===>', this.calldetail);
  }
}
