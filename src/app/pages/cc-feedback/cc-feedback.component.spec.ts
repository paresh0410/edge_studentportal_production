import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcFeedbackComponent } from './cc-feedback.component';

describe('CcFeedbackComponent', () => {
  let component: CcFeedbackComponent;
  let fixture: ComponentFixture<CcFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
