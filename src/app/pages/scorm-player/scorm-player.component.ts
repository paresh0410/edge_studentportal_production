import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
// import { FileService } from '../../service/file.service';
import xml2js from 'xml2js';
import { ENUM } from '../../service/enum';
// import { SqldbService } from '../../service/sqldb.service';
import { ScormService } from '../../service/scormservices.service';
import * as JSZip from 'jszip';
import * as AcrossTabs from '../../../assets/js/across-tabs.min.js';
import { Subscription } from 'rxjs/Subscription';
import { MultiLanguageService } from '../../service/multi-language.service';
@Component({
    selector: 'app-scorm-player',
    templateUrl: './scorm-player.component.html',
    styleUrls: ['./scorm-player.component.scss'],
})
export class ScormPlayerComponent implements OnInit, OnDestroy {

  @Input() scorm: any;
  @Output() scromInteraction? = new EventEmitter();
  @Output() scromActivityCompletion = new EventEmitter();
  languageChangeSubscriber = Subscription.EMPTY;
  parent ;
  manifesturl: string;
  loaded: boolean;
  isExist: boolean;
  scormInfo: any;
  downloading: boolean;
  scormDetails: any;
  isWorking: boolean = false;
  errormsg: any;
  packageId: any;
  userdetail: any;
  popupWindow: any;
  allTabs;
  openedTabs;
  closedTabs;
  scormStatus;
    // tslint:disable-next-line:max-line-length
    constructor(private scormservice: ScormService,
      public multiLanguageService: MultiLanguageService) {

        this.loaded = false;
        this.isExist = false;
        if (localStorage.getItem('userdetail')) {
            this.userdetail = JSON.parse(localStorage.getItem('userdetail'));
        }
        // this.languageChangeSubscriber = multiLanguageService.selectedactivityLanguage.
        // subscribe(selectedactivityLanguage => {
        //   if(selectedactivityLanguage){
        //    this.loaded = false;
        //    this.isExist = false;
        //    this.scorm = selectedactivityLanguage;
        //    this.deleteScromData();
        //    this.intializeData();
        //   }
        //  });
    }

    ngOnInit() {
       this.intializeData();
    }

    // intializeData(){

    //   if (this.scorm) {
    //     this.isWorking = true;
    //     this.scormInfo = this.scorm; // JSON.parse(atob(scorm));
    //     // this.scorm.url = scormInfo.scromUrl;
    //     this.scorm.downloadTime = new Date();
    //     // this.manifesturl = this.scorm.reference;
    //     const reference = decodeURIComponent(this.scorm.reference);
    //     let filename = this.scorm.reference.substring(this.scorm.reference.lastIndexOf('/') + 1);
    //     filename = decodeURIComponent(filename);
    //     let folderpath = this.scorm.reference.replace(/^.*\/\/[^\/]+/, '');
    //     folderpath = decodeURIComponent(folderpath.substring(1));
    //     this.getScormPackages(reference, filename, folderpath, this.scorm.fileinfo.mimetype);
    //     // this.manifesturl = 'http://127.0.0.1:8080/MyHome_SME/index_lms_html5.html';
    // }
    // }

    intializeData(){
      console.log('AcrossTabs', AcrossTabs);
      const _this = this;
      this.parent = new AcrossTabs.default.Parent({
        onHandshakeCallback: () => {
          console.log('onHandshakeCallback', event);
        },
        onChildCommunication: (event) => {
          console.log('onChildCommunication', event);
          this.scormStatus = event.msg;
          if (this.scromInteraction) {
            this.scromInteraction.emit(this.scormStatus);
          }
        },
        onPollingCallback: (event) => {
          console.log('onPollingCallback', event);
        },
        onChildDisconnect: (event) => {
          _this.deleteScormPackage();
          _this.scromActivityCompletion.emit();
          console.log('onChildDisconnect', event);
        }
      },);
      this.allTabs = this.parent.getAllTabs();
      this.openedTabs = this.parent.getOpenedTabs();
      this.closedTabs = this.parent.getClosedTabs();

        if (this.scorm) {
            this.isWorking = true;
            this.scormInfo = this.scorm; // JSON.parse(atob(scorm));
            // this.scorm.url = scormInfo.scromUrl;
            this.scorm.downloadTime = new Date();
            // this.manifesturl = this.scorm.reference;
            const reference = decodeURIComponent(this.scorm.reference);
            let filename = this.scorm.reference.substring(this.scorm.reference.lastIndexOf('/') + 1);
            filename = decodeURIComponent(filename);
            let folderpath = this.scorm.reference.replace(/^.*\/\/[^\/]+/, '');
            folderpath = decodeURIComponent(folderpath.substring(1));
            this.getScormPackages(reference, filename, folderpath, this.scorm.fileinfo.mimetype);
            // this.manifesturl = 'http://127.0.0.1:8080/MyHome_SME/index_lms_html5.html';
        }


    }
    getScormPackages(reference, filename,folderpath, documenttype) {
      if (this.userdetail) {
          const data = {
              filename: filename,
              key: folderpath,
              eid: this.userdetail.id,
              extension: documenttype,
              // scorm: btoa(JSON.stringify(unescape(encodeURIComponent(this.scorm)))),
              scorm: null, // btoa(JSON.stringify(this.scorm)),// this.encryptdata(this.scorm),
              url: reference,
          }
          const basicdata = {
              empId : this.scorm.empId || this.scorm.employeeId, // 37988,
              employeeId: this.scorm.empId || this.scorm.employeeId,
              usrId : this.scorm.usrId,
              activityId : this.scorm.activityId,
              contentId : this.scorm.contentId,
              enrolId : this.scorm.enrolId,
              attempt: this.scorm.attempt,
              activitycompletionid: this.scorm.activitycompletionid,
              moduleId: this.scorm.moduleId,
              courseId: this.scorm.courseId,
              tenantId: this.scorm.tenantId,
              completionstate: this.scorm.completionstate,
              completionstatus: this.scorm.completionstatus,
              roleId : this.userdetail.roleId,
              languageId: this.scorm.defaultlangId,
              // activityName: this.scorm.activityName,
              // name: this.scorm.name
          }
          this.scormservice.viewScormPackage(data).then( (result) => {
              this.isWorking = false;
              if (result.success === true) {
                  this.packageId = result.package;
                  this.manifesturl =  ENUM.domain + ENUM.url.viewScormPackage
                  + '?id=' + result.package
                  + '&' + 'scormId=' + result.scormId
                  + '&' + 'scorm=' + btoa(JSON.stringify(basicdata))
                  + '';
                  // Old Code
                  // this.popupWindow = window.open(this.manifesturl, '_blank', 'modal=yes,alwaysRaised=yes');


                  // this.initScormAPI(this.scorm.contentId);
                  // this.popupWindow.onunload = () => {
                  //   alert('Child window closed');
                  //   // debugger;
                  //   // this.scromActivityCompletion.emit(basicdata);
                  // };

                  // New Code
                  var config = {
                    // url: ' http://127.0.0.1:8081/child.html',
                    url: this.manifesturl,
                    windowName: 'Child - ' + 0,
                    // tslint:disable-next-line:max-line-length
                    windowFeatures: 'toolbar=no,location=yes,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=1000,height=600',
                  };
                  this.parent.openNewTab(config);

                  this.allTabs = this.parent.getAllTabs();
                  this.openedTabs = this.parent.getOpenedTabs();
                  this.closedTabs = this.parent.getClosedTabs();

                  console.log('this.allTabs', this.allTabs);
                  console.log('this.openedTabs', this.openedTabs);
                  console.log('this.closedTabs', this.closedTabs);

              //     this.popupWindow.onbeforeunload = function () {
              //       alert('Child window onbeforeunload');
              //   };
              //   this.popupWindow.onunload = function () {
              //     if (window.closed) {
              //       alert('Child window onunload');
              //    }else{
              //       alert("just refreshed");
              //    }
              //    if (this.popupWindow.closed) {
              //     alert('Child window onunload popupWindow');
              //  }else{
              //     alert("just refreshed popupWindow");
              //  }
              // };
              } else {
                  this.errormsg = 'Unable to fetch Scorm Package';
              }
          }).catch( (err) => {
              console.log(err);
          })
      }
  }
    // getScormPackages(reference, filename,folderpath, documenttype) {
    //     if (this.userdetail) {
    //         const data = {
    //             filename: filename,
    //             key: folderpath,
    //             eid: this.userdetail.id,
    //             extension: documenttype,
    //             // scorm: btoa(JSON.stringify(unescape(encodeURIComponent(this.scorm)))),
    //             scorm: null, // btoa(JSON.stringify(this.scorm)),// this.encryptdata(this.scorm),
    //             url: reference
    //         }
    //         const basicdata = {
    //             empId : this.scorm.empId, // 37988,
    //             employeeId: this.scorm.empId,
    //             usrId : this.scorm.usrId,
    //             activityId : this.scorm.activityId,
    //             contentId : this.scorm.contentId,
    //             enrolId : this.scorm.enrolId,
    //             attempt: this.scorm.attempt,
    //             activitycompletionid: this.scorm.activitycompletionid,
    //             moduleId: this.scorm.moduleId,
    //             courseId: this.scorm.courseId,
    //             tenantId: this.scorm.tenantId,
    //             completionstate: this.scorm.completionstate,
    //             completionstatus: this.scorm.completionstatus,
    //             roleId : this.userdetail.roleId,
    //             languageId: this.scorm.defaultlangId,
    //             // activityName: this.scorm.activityName,
    //             // name: this.scorm.name
    //         }
    //         this.scormservice.viewScormPackage(data).then( (result) => {
    //             this.isWorking = false;
    //             if (result.success === true) {
    //                 this.packageId = result.package;
    //                 this.manifesturl =  ENUM.domain + ENUM.url.viewScormPackage
    //                 + '?id=' + result.package
    //                 + '&' + 'scormId=' + result.scormId
    //                 + '&' + 'scorm=' + btoa(JSON.stringify(basicdata))
    //                 + '';
    //                 this.popupWindow = window.open(this.manifesturl, '_blank', 'modal=yes,alwaysRaised=yes');
    //                 // this.initScormAPI(this.scorm.contentId);
    //             } else {
    //                 this.errormsg = 'Unable to fetch Scorm Package';
    //             }
    //         }).catch( (err) => {
    //             console.log(err);
    //             this.isWorking = false;
    //         })
    //     }
    // }
deleteScormPackage() {
    if (this.packageId) {
        this.scormservice.deleteScormPackage(this.packageId).then( (result) => {
            console.log(result);
        }).catch( (err) => {
            console.log(err);
        });
    }
}
    parseXML(data, cb) {
        const parser = new xml2js.Parser(
            {
                trim: true,
                explicitArray: true
            });

        parser.parseString(data, function (err, result) {
            cb(result);
        });
    }
    setUrl(url) {
        this.manifesturl = window['Ionic'].WebView.convertFileSrc(url);
        console.log('manifest', this.manifesturl);
    }
    /**
     * Scorm API
     */
    tspInit(window, storage, prefix, _this, callback) {
        prefix = typeof prefix !== 'undefined' ? prefix : '';
        callback = typeof callback !== 'undefined' ? callback : console.log;

        window.API = {};

        window.scormStatus = {
            lesson_status: '',
            score_raw: 0,
            score_max: 100,
            score_min: 0,
            session_time: 0,
            detailed_answers: {},
            start_time: 0,
            total_time: 0
        };


        window.API.LMSInitialize = function () {
            console.log('LMSInitialize');
        };
        window.API.LMSTerminate = function () {
            console.log('LMSTerminate');
        };

        window.API.LMSGetValue = function (letname) {
            const _letname = letname;
            letname = prefix + letname;
            let ret = storage.getItem(letname);
            if (!ret || ret === null) {
                ret = _this.getScormValue(_letname);
            }
            if (ret == null && (letname.indexOf('_count', this.length - '_count'.length) !== -1)) {
                ret = 0;
                storage.setItem(letname, ret);
            }
            console.log('LMSGetValue', letname, '=', ret);
            return ret;
        };

        window.API.LMSSetValue = function (letname, letvalue) {
            const _letname = letname;
            letname = prefix + letname;

            let m = letname.match(/([0-9]+)\.cmi\.interactions\.([0-9]+)\.id/);
            if (m != null) {
                // tslint:disable-next-line:radix
                storage.setItem('{{scorm.contentId}}.cmi.interactions._count', parseInt(m[2]) + 1);
                // _this.scorm.scormTrack[_this.scorm.contentId + '.cmi.interactions._count'] = parseInt(m[2]) + 1;
                // tslint:disable-next-line:radix
                _this.scorm.scormTrack['cmi.interactions._count'] = parseInt(m[2]) + 1;
            }

            m = letname.match(/([0-9]+)\.cmi\.interactions\.([0-9]+)\.result/);
            if (m != null) {
                // tslint:disable-next-line:radix
                const key = storage.getItem(prefix + 'cmi.interactions.' + parseInt(m[2]) + '.id');
                // tslint:disable-next-line:radix
                _this.getScormValue('cmi.interactions.' + parseInt(m[2]) + '.id');
                window.scormStatus.detailed_answers[key] = letvalue;
            }
            // if (_letname === 'cmi.core.lesson_status') {
            //   window.scormStatus.lesson_status = letvalue;
            //   _this.scorm.scormTrack['cmi.core.lesson_status'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.core.lesson_status') {
                window.scormStatus.lesson_status = letvalue;
                _this.scorm.scormTrack['cmi.core.lesson_status'] = letvalue;
            }

            // if (_letname === 'cmi.core.score.raw') {
            //   window.scormStatus.score_raw = letvalue;
            //   _this.scorm.scormTrack['cmi.core.score.raw'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.core.score.raw') {
                window.scormStatus.score_raw = letvalue;
                _this.scorm.scormTrack['cmi.core.score.raw'] = letvalue;
            }

            // if (_letname === 'cmi.core.score.max') {
            //   window.scormStatus.score_max = letvalue;
            //   _this.scorm.scormTrack['cmi.core.score.max'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.core.score.max') {
                window.scormStatus.score_max = letvalue;
                _this.scorm.scormTrack['cmi.core.score.max'] = letvalue;
            }

            // if (_letname === 'cmi.core.score.min') {
            //   window.scormStatus.score_min = letvalue;
            //   _this.scorm.scormTrack['cmi.core.score.min'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.core.score.min') {
                window.scormStatus.score_min = letvalue;
                _this.scorm.scormTrack['cmi.core.score.min'] = letvalue;
            }

            // if (_letname === 'cmi.core.session_time') {
            //   window.scormStatus.session_time = letvalue;
            //   _this.scorm.scormTrack['cmi.core.session_time'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.core.session_time') {
                window.scormStatus.session_time = letvalue;
                _this.scorm.scormTrack['cmi.core.session_time'] = letvalue;
            }

            // if (_letname === 'x.start.time') {
            //   window.scormStatus.session_time = letvalue;
            //   _this.scorm.scormTrack['x.start.time'] = letvalue;
            // } else
            if (letname === prefix + 'x.start.time') {
                // window.scormStatus.session_time = letvalue;
                _this.scorm.scormTrack['x.start.time'] = letvalue;
            }

            // if (_letname === 'cmi.core.total_time') {
            //   window.scormStatus.total_time = letvalue;
            //   _this.scorm.scormTrack['cmi.core.total_time'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.core.total_time') {
                window.scormStatus.total_time = letvalue;
                _this.scorm.scormTrack['cmi.core.total_time'] = letvalue;
            }

            // if (_letname === 'cmi.suspend_data') {
            //   window.scormStatus.total_time = letvalue;
            //   _this.scorm.scormTrack['cmi.suspend_data'] = letvalue;
            // } else
            if (letname === prefix + 'cmi.suspend_data') {
                // window.scormStatus.total_time = letvalue;
                _this.scorm.scormTrack['cmi.suspend_data'] = letvalue;
            }

            storage.setItem(letname, letvalue);
            _this.scorm.scormTrack[_letname] = letvalue;
            console.log('LMSSetValue', letname, '=', letvalue);
        };

        window.API.LMSCommit = function () {
            console.log('LMSCommit');
            // saving to API
            callback(window.scormStatus);
            return true;
        };

        window.API.LMSFinish = function () {
            console.log('LMSFinish');
            _this.saveScormTracker(_this);
        };

        window.API.LMSGetLastError = function () {
            console.log('LMSGetLastError');
        };

        window.API.LMSGetErrorString = function () {
            console.log('LMSGetErrorString');
        };

        window.API.LMSGetDiagnostic = function () {
            console.log('LMSGetDiagnostic');
        };

    }

    initScormAPI(scormId) {
        const prefix = scormId + '.';
        this.tspInit(window, window.localStorage, prefix, this, (progress) => {
            // this will be called whenever student makes a progress in test.
            console.log(progress);
            console.log(this.scorm.scormTrack);
            this.saveScormTracker(this);
        });
    }
    getScormValue(item) {
        return this.scorm.scormTrack[item];
    }

    saveScormTracker(_this) {
        const trackList = _this.populateScormDetail(_this.scorm);
        try {
            this.scormservice.saveScormTrackList(trackList).then((result) => {
                console.log(result);
            }).catch((error) => {
                console.log(error);
            });
        } catch (e) {
            _this.scormservice.saveScormTrackList(trackList).then((result) => {
                console.log(result);
            }).catch((error) => {
                console.log(error);
            });
        }
    }
    populateScormDetail(scorm) {
        const list = [];
        if (scorm && scorm.scormTrack) {
            const track = scorm.scormTrack;
            // tslint:disable-next-line:forin
            for (const k in track) {
                const item = {
                    empId: scorm.empId !== null ? scorm.empId : 2,
                    scormid: scorm.contentId,
                    enrolId: scorm.enrolId !== null ? scorm.enrolId : 117,
                    // tslint:disable-next-line:radix
                    attempt: scorm.attempt === null ? 1 : parseInt(scorm.attempt) + 1,
                    element: null,
                    value: null
                };
                if (track.hasOwnProperty(k)) {
                    console.log('Key is ' + k + ', value is' + track[k]);
                    item.element = k;
                    item.value = track[k];
                }
                if (item.element) {
                    list.push(item);
                }
            }
        }
        return list;
    }

    ngOnDestroy() {
        // this.saveScormTracker(this);
       this.deleteScromData();
    }


    // deleteScromData(){
    //     if (this.popupWindow) {
    //       this.popupWindow.close();
    //   this.deleteScormPackage();
    //   }

    // }

    deleteScromData(){
      if(this.parent){
        this.parent.closeAllTabs();
        this.allTabs = this.parent.getAllTabs();
        this.openedTabs = this.parent.getOpenedTabs();
        this.closedTabs = this.parent.getClosedTabs();

        console.log('this.allTabs', this.allTabs);
        console.log('this.openedTabs', this.openedTabs);
        console.log('this.closedTabs', this.closedTabs);

        this.deleteScormPackage();
      }

    }
    encryptdata(value) {
        return this.Base64.encode(btoa(unescape(encodeURIComponent((JSON.stringify(this.scorm))))))
    }
    /**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/

Base64 = {

	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode : function (input) {
		let output = "";
		let chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		let i = 0;

		input = this._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}

		return output;
	},

	// public method for decoding
	decode : function (input) {
		let output = "";
		let chr1, chr2, chr3;
		let enc1, enc2, enc3, enc4;
		let i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = this._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		let utftext = "";

		for (let n = 0; n < string.length; n++) {

			let c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		let string = "";
		let i = 0;
		let c , c1 , c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				let c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}
}
