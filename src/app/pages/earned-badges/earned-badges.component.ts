import { Component, OnInit, Input } from '@angular/core';
import { EarnedBadgesViewerService } from './earned-badges.service';

@Component({
  selector: 'ngx-earned-badges',
  templateUrl: './earned-badges.component.html',
  styleUrls: ['./earned-badges.component.scss']
})
export class EarnedBadgesComponent implements OnInit {
  @Input('skeleton') skeleton: any;
  // skeleton : boolean = true;
  badges: any = [
    // { badgeId: 1, badgeName: "Master 1", path: "./assets/images/Master1.svg"},
    // { badgeId: 2, badgeName: "Master 2", path: "./assets/images/Master2.svg"},
    // { badgeId: 3, badgeName: "Master 3", path: "./assets/images/Master3.svg"},
    // { badgeId: 4, badgeName: "Intermediate", path: "./assets/images/Intermediate.svg"},
    // { badgeId: 5, badgeName: "Beginner", path: "./assets/images/Beginner.svg"},
  ]
  selectedItem: any;
  selectedIndex: any;

  constructor(private EBS: EarnedBadgesViewerService) {

  }

  ngOnInit() {
    console.log(this.skeleton);
    if (this.skeleton == false) {
      this.badges = this.EBS.getBadgesData();
      console.log('Badges===>', this.badges);
    }
  }

}
