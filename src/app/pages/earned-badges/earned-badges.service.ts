import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class EarnedBadgesViewerService {
  badgeDetails: any

  constructor() {}

  setBadgesData(Data) {
    this.badgeDetails = Data;
  }

  getBadgesData() {
    return this.badgeDetails;
  }
}
