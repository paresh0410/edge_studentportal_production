import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';

import { CommonModule } from '@angular/common';
// import { EarnedBadgesComponent } from './earned-badges.component';


@NgModule({
  imports: [
    CommonModule,
    Ng2CarouselamosModule
  ],
  declarations: [
    // EarnedBadgesComponent
  ],
  providers:[
     
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class EarnedBadgesModule {
}
