import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseServiceProvider } from '../../service/course-service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-mark-attendance',
  templateUrl: './mark-attendance.component.html',
  styleUrls: ['./mark-attendance.component.scss']
})
export class MarkAttendanceComponent implements OnInit {

	displaymsg:boolean = false;
	courseDataSummary:any;
	courseDataContent:any;
	courseTitle:any;
	trainerId:any=0;
	description:string;
	completionId: any;
	enrolId: any;

	constructor(public CSP: CourseServiceProvider, private router:Router, private routes:ActivatedRoute,
		private toastr: ToastrService) {

		this.courseDataContent = CSP.getDataContent();
		this.courseDataSummary = CSP.getDataSummary();
		this.courseTitle = this.courseDataSummary.courseTitle;
		if(localStorage.getItem("userdetail")){
			var userdetail = JSON.parse(localStorage.getItem("userdetail"));
		}
		if(localStorage.getItem("employeeId")){
			var employeeId = localStorage.getItem("employeeId");
		}
		var url = ENUM.domain + ENUM.url.getMarkedAttendance;
		var param ={
			cId: this.courseDataContent.courseId,
			mId: this.courseDataContent.moduleId,
			actId: this.courseDataContent.activityId,
			tId: userdetail.tenantId,
			empId: employeeId,
		}

		console.log('get mark attendance param', param);
		this.CSP.getMarkedAttendence(url, param)
		.then((result:any)=>{
			console.log('RESULT==>', result.data);
			this.trainerId = result.data[0].trainerId;
			this.completionId = result.data[0].completionId;
			this.enrolId = result.data[0].enrolId;
			if(result.data[0].status) {
				this.displaymsg = true;
			}
		}).catch(result =>{
			console.log("ServerResponseError :",  result);
		})

	}

	goBack(){
		window.history.back();
		// this.router.navigate(['../../../learn/course-detail'],{relativeTo:this.routes});
	}

  	ngOnInit() {
  	}

  	markAttendance(){
		var url = ENUM.domain + ENUM.url.markAttendance;
		if(localStorage.getItem("userdetail")){
			var userdetail = JSON.parse(localStorage.getItem("userdetail"));
		}
		if(localStorage.getItem("employeeId")){
			var employeeId = localStorage.getItem("employeeId");
		}
		const allstr = this.enrolId + '|' + employeeId + '|' +
						 userdetail.id + '|' + this.completionId;

		console.log('this.courseDataContent', this.courseDataContent);
		var param = {
			actionId: 1,
			iId: this.courseDataContent.courseId,
			modId: this.courseDataContent.moduleId,
			actId: this.courseDataContent.activityId,
			// learnerId: employeeId,
			allstr: allstr,
			trainId: this.trainerId,
			tId: userdetail.tenantId,
			userId: userdetail.id,
		};
		console.log('param', param);
// 		actionId: 1,
// iId: this.activityData.courseId,
// modId: this.activityData.moduleId,
// actId: this.activityData.activityId,
// learnerId: this.currentUserData.eid,
// trainId: this.attendanceCheckRes.trainerId,
// currentDate: null,
// currentTime: null,
// tId: this.currentUserData.tenantId,
// userId: this.currentUserData.id
		this.CSP.getMarkedAttendence(url,param)
		.then((result: any) => {
			if(result.type) {
				//this.toastr.success(result.data[0].msg,'Successful!');
				this.displaymsg = true;
			}else {
				this.toastr.warning('Something went wrong','Warning').onHidden.subscribe(()=>
				this.toastr.clear());
				this.displaymsg = false;
			}
		}).catch(result => {
			console.log("ServerResponseError :",  result);
		})
  	}

}
