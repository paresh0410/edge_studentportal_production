import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators,  } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { AuthServiceProvider } from '../../service/auth-service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-reset-pwd',
  templateUrl: './reset-pwd.component.html',
  styleUrls: ['./reset-pwd.component.scss']
})
export class ResetPwdComponent implements OnInit {

  password: string;
  confirm_pass: string;
  passWrong: boolean;

  welcomepagevar: boolean;

  resetPwdForm: FormGroup;
  submitted = false;


  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private ASP: AuthServiceProvider,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.welcomepagevar = false;
    this.resetPwdForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_pass : ['', [Validators.required, Validators.minLength(6)] ]
  });

  }

  get f() { return this.resetPwdForm.controls; }   // form fields getter for easy access

  onSubmit() {
    this.submitted = true;
    this.onInput(this.confirm_pass);
    // this.checkPassword();
    // stop here if form is invalid
    if (this.resetPwdForm.invalid) {
        return;
    }else {
      if (localStorage.getItem('ECN') && localStorage.getItem('tenantId')) {
        const ECN = localStorage.getItem('ECN');
        const tId = localStorage.getItem('tenantId');
        const param = {
          newPassword: this.password,
          ecn: ECN,
          tId: tId,
          type: 3,
        };
        this.ASP.resetPassword(param)
        .then((result: any) => {
          if (result.type === true){
            this.toastr.success(result.data1, 'Success!!').onHidden.subscribe(()=>
            this.toastr.clear());
            this.welcomepagevar = true;
            // this.router.navigate(["../otp"], { relativeTo: this.routes });
          }else {
            this.toastr.warning(result.data1, 'Error!!').onHidden.subscribe(()=>
            this.toastr.clear());
          }
        }).catch(result => {
          console.log('RESULT===>', result);
          this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          this.toastr.clear());
        })
      }else{
        this.router.navigate(["../forgotPass"], { relativeTo: this.routes });
        this.toastr.warning('Please enter your ECN again', 'Error!!').onHidden.subscribe(()=>
        this.toastr.clear());
      }
      // setTimeout(() => {
      //   this.router.navigate(['../login'],{relativeTo:this.routes});
      // }, 1000)
    }
  }

  goBack(){
    this.router.navigate(['../forgotPass'],{relativeTo:this.routes});
  }

  gotoLogin(){
    this.router.navigate(['../login-new'],{relativeTo:this.routes});
  }

checkPassword() {
  if(this.password === this.confirm_pass){
    return true;
  }else{
    return false;
  }
}
onInput(value) {
if(this.checkPassword()){
//if both passwords are same set errors to null
this.f.confirm_pass.setErrors(null);
this.passWrong = false;
console.log(this.passWrong);
}else{
//set error to confirm_pass manually
this.f.confirm_pass.setErrors([{'passwordMismatch': true}]);

this.passWrong = true;
console.log(this.passWrong);
}

}




}
