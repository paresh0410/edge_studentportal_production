import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcObservationComponent } from './cc-observation.component';

describe('CcObservationComponent', () => {
  let component: CcObservationComponent;
  let fixture: ComponentFixture<CcObservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
