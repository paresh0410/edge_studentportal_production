import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ENUM } from '../../service/enum';
import { CallCoachingService } from '../../service/call-coaching.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'ngx-cc-observation',
  templateUrl: './cc-observation.component.html',
  styleUrls: ['./cc-observation.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CcObservationComponent implements OnInit {
  @Input() obsdata;
  @Input() calldetail;
  // @Input() preAssessment;
  calls: any = [];
  obsquestion: any = [];
  employeeId: any;
  userDetails: any = [];
  constructor(public callservice: CallCoachingService, public spinner: NgxSpinnerService,
    public toast: ToastrService) {
    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
  }

  ngOnInit() {
    for (let i = 0; i < this.obsdata.length; i++) {
      if(this.obsdata[i].score == null) {
        this.obsdata[i].score = 0;
      }
    }
    this.obsquestion = this.obsdata;
    console.log('this.obsquestion',this.obsquestion);
    this.calls = this.calldetail;
    console.log(this.obsquestion);
  }
  onRateChange(count, index) {
    console.log('count.rating', count.rating);
    console.log('index', index);
    var count = count.rating;
    for (let i = 0; i < this.obsquestion.length; i++) {
      if ( i == index) {
        this.obsquestion[i].score = count;
      }
    }
  }
  submit(data) {
    // if(this.calls.isEnable == 'Y'){
    //   this.spinner.show();
    //   console.log('obs data', data);
    //   var allstr = this.getdatareadyforobs(data);
    //   console.log('obs allstr', allstr);
    //   let param = {
    //     callId: this.calls.callId,
    //     userId: this.userDetails.id,
    //     tId: this.userDetails.tenantId,
    //     allstr: allstr,
    //   };
    //   console.log(param);
    //   const url = ENUM.domain + ENUM.url.cc_add_observation;
    //   this.callservice.get(url, param).then(res => {
    //     console.log(res);
    //     this.spinner.hide();
    //     if (res['type'] == true) {
    //       // this.Toastr.success(res[data].msg);
    //       // window.history.back();
    //       this.toast.success('Observation submitted successfully.');
    //     }
    //   }, err => {
    //     this.spinner.hide();
    //     console.log(err);
    //   });
    // }
    // else{
    //   let message = 'Observation will be submitted after call';
    //   this.toast.info(message, 'Info');
    // }
      this.spinner.show();
      console.log('obs data', data);
      var allstr = this.getdatareadyforobs(data);
      console.log('obs allstr', allstr);
      let param = {
        callId: this.calls.callId,
        userId: this.userDetails.id,
        tId: this.userDetails.tenantId,
        allstr: allstr,
      };
      console.log(param);
      const url = ENUM.domain + ENUM.url.cc_add_observation;
      this.callservice.get(url, param).then(res => {
        console.log(res);
        this.spinner.hide();
        if (res['type'] == true) {
          // this.Toastr.success(res[data].msg);
          // window.history.back();
          this.toast.success('Observation submitted successfully.');
        }
      }, err => {
        this.spinner.hide();
        console.log(err);
      });


  }
  getdatareadyforobs(obserdata) {
    console.log('obserdata',obserdata);
    var areaId;
    var areaName;
    var compId;
    var compName;
    var otId;
    var spectrId;
    var spectrName;
    var value;
    var Finalline;
    for (let i = 0; i < obserdata.length; i++) {
      spectrId = obserdata[i].spectrIds;
      spectrName = obserdata[i].spectrNames;
      compId = obserdata[i].compIds;
      compName = obserdata[i].compNames;
      areaId = obserdata[i].evalAreaIds;
      areaName = obserdata[i].evalAreaNames;
      value = obserdata[i].score;
      otId = obserdata[i].otId;
      let line = spectrId + "|" + spectrName + "|" + compId + "|" + compName + "|" + areaId + "|" + areaName
        + "|" + value + "|" + otId;
      if (i == 0) {
        Finalline = line;
      } else {
        Finalline += "#" + line;
      }
    }
    return Finalline;
  }
}
