import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowLearnComponent } from './workflow-learn.component';

describe('WorkflowLearnComponent', () => {
  let component: WorkflowLearnComponent;
  let fixture: ComponentFixture<WorkflowLearnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowLearnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowLearnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
