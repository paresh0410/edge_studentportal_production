import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LearnServiceProvider } from '../../service/learn-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ENUM } from '../../service/enum';
import { LogServices, LogEnum } from '../../service/logservice';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
// import { environment  } from '../../../environments/environment';
import { feature } from '../../../environments/feature-environment';
@Component({
  selector: 'ngx-workflow-learn',
  templateUrl: './workflow-learn.component.html',
  styleUrls: ['./workflow-learn.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WorkflowLearnComponent implements OnInit {
  learnList = [
    {
      compStatus: 'CURRENT',
      list: [
        {
          'approvalStatus': null,
          'approverId': null,
          'cat_id': 1,
          'category': 'demoedit  category 1234678',
          'compStatus': 'CURRENT',
          'completed': '03 / 04',
          'completed_courses': 3,
          'courseDate': 'NaN  NaN',
          'courseDate1': 'March 7',
          'courseDesc':'This is demo course1',
          'courseMon': 'March',
          'courseMonth': '01 March 2019',
          'courseTitle': 'Demo Course23',
          'courseType': 'MANDATORY',
          'courseType2': 'Online',
          'cpoints': 110,
          'earnpoints':268,
          'endDate': '08 Mar 2019',
          'enrolId': 36,
          'id':40,
          'isBookmark':0,
          'isDislike': 0,
          'isLike': 1,
          'nextStage':"",
          'noDislikes':0,
          'noLikes':2,
          'noOfmodules':2,
          'percComplete': 75,
          'startDate': '2019-03-07T08:04:33.000Z',
          'tags': 'DC3,Demo3,Course3',
          'total_courses': 4,
          'type1Image' : 'assets/images/orangeLearn.svg',
          'type2Image': 'assets/images/online.svg',
          'typeImage': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/cat2.png',
        },
        {
          'approvalStatus': null,
          'approverId': null,
          'cat_id': 1,
          'category': 'demoedit  category 12346782',
          'compStatus': 'CURRENT',
          'completed': '03 / 04',
          'completed_courses': 3,
          'courseDate': 'NaN  NaN',
          'courseDate1': 'March 7',
          'courseDesc': 'This is demo course12',
          'courseMon': 'March',
          'courseMonth': '01 March 2019',
          'courseTitle': 'Demo Course23',
          'courseType': 'MANDATORY',
          'courseType2': 'Online',
          'cpoints': 110,
          'earnpoints':268,
          'endDate': '08 Mar 2019',
          'enrolId': 36,
          'id':40,
          'isBookmark':0,
          'isDislike': 0,
          'isLike': 1,
          'nextStage':"",
          'noDislikes':0,
          'noLikes':2,
          'noOfmodules':2,
          'percComplete': 75,
          'startDate': '2019-03-07T08:04:33.000Z',
          'tags': 'DC3,Demo3,Course3',
          'total_courses': 4,
          'type1Image' : 'assets/images/orangeLearn.svg',
          'type2Image': 'assets/images/online.svg',
          'typeImage': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/cat2.png',
        },
        {
          'approvalStatus': null,
          'approverId': null,
          'cat_id': 1,
          'category': 'demoedit  category 123467821233',
          'compStatus': 'CURRENT',
          'completed': '03 / 04',
          'completed_courses': 3,
          'courseDate': 'NaN  NaN',
          'courseDate1': 'March 7',
          'courseDesc': 'This is demo course12',
          'courseMon': 'March',
          'courseMonth': '01 March 2019',
          'courseTitle': 'Demo Course23',
          'courseType': 'MANDATORY',
          'courseType2': 'Online',
          'cpoints': 110,
          'earnpoints':268,
          'endDate': '08 Mar 2019',
          'enrolId': 36,
          'id':40,
          'isBookmark':0,
          'isDislike': 0,
          'isLike': 1,
          'nextStage':"",
          'noDislikes':0,
          'noLikes':2,
          'noOfmodules':2,
          'percComplete': 75,
          'startDate': '2019-03-07T08:04:33.000Z',
          'tags': 'DC3,Demo3,Course3',
          'total_courses': 4,
          'type1Image' : 'assets/images/orangeLearn.svg',
          'type2Image': 'assets/images/online.svg',
          'typeImage': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/cat2.png',
        },
      ],
    },
  ];

  public monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  arrayDataList;

  workflowData: any = [];
  employeeId: any;
  userDetails: any;

  constructor(public LSP: LearnServiceProvider, private router: Router,
    private toast: ToastrService,
    private routes: ActivatedRoute) {

    // console.log ("Data in array ===>", this.learnList[0].list);
    // this.arrayDataList = this.learnList[0].list;
    if (localStorage.getItem('userDetails')) {
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    }
    if (localStorage.getItem('employeeId')) {
      this.employeeId = localStorage.getItem('employeeId');
    }
   }

  ngOnInit() {
    this.workflowData = this.LSP.getWorkflowData();
    console.log("workflow data from service :====>", this.workflowData);
    // this.arrayDataList = this.workflowData.courseList;
    this.fetchLearningPathwayUpdatedCourses();
  }

  fetchLearningPathwayUpdatedCourses() {
    const learningPathwayParams = {
      eId : this.employeeId,
      tId : this.userDetails.tenantId,
      uId : this.userDetails.id,
      wfId : this.workflowData.workflowid
    };

    this.fetchCourseList(learningPathwayParams, (result) => {
      if (result.type === true) {
        this.workflowData = [];
        if (result.data && result.data.length !=0){
          this.workflowData = result.data[0];
          this.makeCourseDataReady(this.workflowData);
          this.workflowData = this.getFormattedCourseData(this.workflowData);
          this.arrayDataList = this.workflowData.courseList;
          console.log('Workflow courses list ', this.workflowData);
        } else {
          this.toast.warning('No course available in the Workflow', 'Warning!');
          window.history.back();
        }
      } else {
        console.log('err ', result);
      }
    });
  }

  fetchCourseList(params: any, cb) {
    // this.LSP.getLearningPathwayCoursesList(params).then(
    //   res => {
    //     if (res.type === true) {
    //       console.log('Learning pathway updated Course list ', res);
    //       cb(res);
    //     } else {
    //       console.log('err ', res);
    //     }
    //   },
    //   err => {
    //     console.log(err);
    //   }
    // );
    const url = ENUM.domain + ENUM.url.list_all_learning_pathway_courses;
    this.LSP.getLearningPathwayCoursesList(url, params)
      .then(result => {
        // console.log('DATA--->', result);
        cb(result);
      })
      .catch(result => {
        console.log('ServerResponseError :', result);
      });
  }

  goToCourseDetails(passData, index) {
    console.log('DATA--->',passData);
    console.log('Index--->',index);
    this.LSP.setcourseDetailList(passData);
    if(feature.activity.standardui){
      this.router.navigate(['../learn/course-detail'], { relativeTo: this.routes });
    }
    if(feature.activity.modernui) {
    this.router.navigate(['../module-activity'], { relativeTo: this.routes });
    }
  }
  back() {
    window.history.back();
  }
  // togglelike(item) {
  //   console.log('ITEM--->', item);
  //   if (!item.isLike) {
  //     item.isLike = true;
  //     item.noLikes = item.noLikes + 1;
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 1,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId
  //     };
  //     var url = ENUM.domain + ENUM.url.addCourseFeedback;
  //     this.LSP.addCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //     if (item.isLike) {
  //       if (item.isDislike) {
  //         if (item.noDislikes > 0) {
  //           item.noDislikes = item.noDislikes - 1;
  //         }
  //         this.toggledislike(item);
  //         item.isDislike = false;
  //       }
  //     }
  //   } else {
  //     item.isLike = false;
  //     if (item.noLikes > 0) {
  //       item.noLikes = item.noLikes - 1;
  //     }
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 2,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId
  //     };
  //     var url = ENUM.domain + ENUM.url.removeFeedback;
  //     this.LSP.removeCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //   }
  // }
  // toggledislike(item) {
  //   console.log('ITEM--->', item);
  //   if (!item.isDislike) {
  //     item.isDislike = true;
  //     item.noDislikes = item.noDislikes + 1;
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 2,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId
  //     };
  //     var url = ENUM.domain + ENUM.url.addFeedback;
  //     this.LSP.addCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //     if (item.isDislike) {
  //       if (item.isLike) {
  //         if (item.noLikes > 0) {
  //           item.noLikes = item.noLikes - 1;
  //         }
  //         this.togglelike(item);
  //         item.isLike = false;
  //       }
  //     }
  //   } else {
  //     item.isDislike = false;
  //     if (item.noDislikes > 0) {
  //       item.noDislikes = item.noDislikes - 1;
  //     }
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 1,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId
  //     };
  //     var url = ENUM.domain + ENUM.url.removeFeedback;
  //     this.LSP.removeCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //   }
  // }
  // enrolInCourse(data) {
  //   console.log('EnrolCourseData', data);
  //   var params = {
  //     enrolId: data.enrolId
  //   };
  //   var url = ENUM.domain + ENUM.url.selfEnrol;
  //   this.LSP.selfEnrolCourse(url, params)
  //     .then((result: any) => {
  //       console.log('DATA--->', result);
  //       if (result.type == true) {
  //         this.toastr.success('Enrol Request Sent Successfully', 'Success!');
  //       } else {
  //         this.toastr.error(
  //           'Please try again after sometime',
  //           'Error Occured!'
  //         );
  //       }
  //     })
  //     .catch(result => {
  //       console.log('ServerResponseError :', result);
  //     });
  // }
  submitCourseFeedback(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId:userDetails.id,
      eId:  this.employeeId,
      tId: userDetails.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.addCourseFeedback;
      this.LSP.addCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }

  removeCourseFeedback(courseData, feedbackType) {
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.userDetails.id,
      eId: this.employeeId,
      tId: userDetails.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.removeCourseFeedback;
      this.LSP.removeCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }
  makeCourseDataReady(workflowData) {
    if (workflowData) {
      if (workflowData.courseList) {
        for (let i = 0; i < workflowData.courseList.length; i++) {
          workflowData.courseList[i]['begin'] = true;
          workflowData.courseList[i]['wait'] = false;
          workflowData.courseList[i]['completed'] = false;
          if (workflowData.courseList[i].previousCourseid !== 0 && workflowData.courseList[i].previousCourseid !== null &&
            workflowData.courseList[i].previousCourseid !== undefined) {
            for (let j = 0; j < workflowData.courseList.length; j++) {
              if (workflowData.courseList[j].id === workflowData.courseList[i].previousCourseid) {
                const dependentActIndex = j + 1;
                // workflowData.courseList[i]['dependentActName'] = 'Activity ' + dependentActIndex;
                workflowData.courseList[i]['dependentCourseName'] = workflowData.courseList[j].courseTitle;
                if (workflowData.courseList[j].percComplete === 100) {
                  if (workflowData.courseList[i].cascade_course_flag === 1) {
                    workflowData.courseList[i].wait = false;
                    workflowData.courseList[i].begin = true;
                    workflowData.courseList[i].completed = false;
                  } else {
                    workflowData.courseList[i].wait = true;
                    workflowData.courseList[i].begin = false;
                    workflowData.courseList[i].completed = false;
                  }
                } else {
                  workflowData.courseList[i].wait = true;
                  workflowData.courseList[i].begin = false;
                  workflowData.courseList[i].completed = false;
                }
              }
            }
          } else if (workflowData.courseList[i].percComplete === 100) {
            workflowData.courseList[i].completed = true;
            workflowData.courseList[i].wait = false;
            workflowData.courseList[i].begin = false;
          } else {
            if (workflowData.courseList[i].cascade_course_flag === 1) {
              workflowData.courseList[i].wait = false;
              workflowData.courseList[i].begin = true;
              workflowData.courseList[i].completed = false;
            } else {
              workflowData.courseList[i].wait = true;
              workflowData.courseList[i].begin = false;
              workflowData.courseList[i].completed = false;
            }
            // workflowData.courseList[i].wait = true;
            // workflowData.courseList[i].begin = false;
            // workflowData.courseList[i].completed = false;
          }
        }
      }
    }
  }

  getFormattedCourseData(DataCourseType) {
    // for (let i = 0; i < DataCourseType.length; i++) {
      let temporary = DataCourseType;
      for (let j = 0; j < temporary.courseList.length; j++) {
        temporary.courseList[j].courseDate = this.formatDate(
          temporary.courseList[j].courseDate);
        if (!temporary.courseList[j].earnpoints) {
          temporary.courseList[j].earnpoints = 0;
        }
        if (!temporary.courseList[j].cpoints) {
          temporary.courseList[j].cpoints = 0;
        }
        temporary.courseList[j].endDate = this.formatDate(temporary.courseList[j].endDate);
        temporary.courseList[j].completed = this.formatCompData(
          temporary.courseList[j].completed_courses,
          temporary.courseList[j].total_courses,
        );
        if (temporary.courseList[j].workflowid) {
          temporary.courseList[j].type1Image = 'assets/images/purpleIcon.png';
        } else if (temporary.courseList[j].courseType == 'MANDATORY') {
          temporary.courseList[j].type1Image = 'assets/images/orangeLearn.svg';
        } else if (temporary.courseList[j].courseType == 'RECOMMENDED') {
          temporary.courseList[j].type1Image = 'assets/images/mountLearn.svg';
        } else {
          // if(temporary.courseList[j].courseType == 'ASPIRATIONAL'){
          temporary.courseList[j].type1Image = 'assets/images/tickLearn.svg';
        }
        if (temporary.courseList[j].courseType2 == 'Online') {
          temporary.courseList[j].type2Image = 'assets/images/online.svg';
        } else if (temporary.courseList[j].courseType2 == 'Classroom') {
          temporary.courseList[j].type2Image = 'assets/images/classroom.svg';
        }
        // if (temporary.courseList[j].percComplete === 0) {
        //   temporary.courseList[j].percComplete = 0;
        // }
      }
    // }
    return DataCourseType;
  }

  ProgressInWhole(val){
    if (val) {
      return Math.round(val);
    }
    else{
      return 0;
    }
  }


  formatDate(date) {
    var d = new Date(date),
    month = '' + d.getMonth(),
    day = '' + d.getDate(),
    year = d.getFullYear();
    month = this.monthNames[month];
    if (day.length < 2) day = '0' + day;
    var dateString = [day, month, year].join(' ');
    return dateString;
  }

  formatCompData(ccvar, tcvar) {
    ccvar = '' + ccvar;
    tcvar = '' + tcvar;
    if (ccvar.length < 2) ccvar = '0' + ccvar;
    if (tcvar.length < 2) tcvar = '0' + tcvar;
    var retString = ccvar + ' / ' + tcvar;
    return retString;
  }
  bindBackgroundImage(img) {
    if(img === null || img === '' || img === 'assets/images/courseicon.jpg' ) {
      return 'url(' + 'assets/images/open-book-leaf.jpg' + ')';
    } else {
      return 'url(' + img + ')';
    }
  }
}
