import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepCocComponent } from './eep-coc.component';

describe('EepCocComponent', () => {
  let component: EepCocComponent;
  let fixture: ComponentFixture<EepCocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepCocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepCocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
