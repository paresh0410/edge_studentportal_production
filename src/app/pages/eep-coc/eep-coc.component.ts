import { Component, Input, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { EEPService } from '../../service/eep.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EepStepComponent } from '../eep-step/eep-step.component';
import { ENUM } from '../../service/enum';
@Component({
  selector: 'ngx-eep-coc',
  templateUrl: './eep-coc.component.html',
  styleUrls: ['./eep-coc.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EepCocComponent implements OnInit {
  @Output() public passData = new EventEmitter();
  @Input() response_data;
  trainerCodeOfConduct: any = [];
  selectedProgram: any;
  programs: any = [];
  userDetails: any;
  tenantId: any;
  employeeId: any;
  userId: any;
  wfresonseId: any;
  stepValue: any;
  agreeAndDisAgreeButtonStatus: boolean = false;
  workflowdetail: any;
  stepsData: any = [];
  selectedIndex: any;
  selectedProgramId: any;
  public flagData = 3;
  constructor(private eepservice: EEPService,
    public spinner: NgxSpinnerService,
    public eeeComponent: EepStepComponent, private toastr: ToastrService) {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userId = this.userDetails.id;
    this.tenantId = this.userDetails.tenantId;
    this.employeeId = localStorage.getItem('employeeId');
    this.workflowdetail = this.eepservice.workflow;
    this.getAllPrograms();
    this.getCocData();
    this.getAllSteps();
    this.stepValue = eeeComponent.stepsData;
  }

  ngOnInit() {
  }
  getAllPrograms() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.eep_workflow_program;
    const param = {
      'tId': this.tenantId,
    }
    this.eepservice.get(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT program Success===>', result);
      console.log('result.data[0]', result.data[0]);
      this.programs = result.data;
      console.log('programs', this.programs);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT program Error===>', result);
    })
  }

  getCocData() {
    this.trainerCodeOfConduct = [];
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.eep_codeofconduct;
    const param = {
      'wfId': this.workflowdetail.id,
      'empId': this.employeeId,
      'tId': this.tenantId,
    };
    this.eepservice.get(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT COC Success===>', result);
      console.log('result.data', result.data[0]);
      if (result.data[0].responseType === 1) {
        this.flagData = 0;
      } else if (result.data[0].responseType === 0) {
        this.flagData = 2;
      }
      var imageExts = new Array('.png', '.jpeg', '.jpg' );
      var fileExts = new Array('.pdf');

      this.wfresonseId = result.data[0].wfrId;
      const fooCocName = result.data[0].secName.split('|');
      const fooCocDesc = result.data[0].secDesc.split('|');
      const fooConRef = result.data[0].contentRef.split('|');
  
      for (let i = 0; i < fooCocName.length; i++) {
        const fooObj = {
          secName: fooCocName[i],
          secDesc: fooCocDesc[i],
          contentRef: fooConRef[i] == 0 ? null : fooConRef[i],
          fileExt: fooConRef[i].substring(fooConRef[i].lastIndexOf('.'))
        };
        if (imageExts.indexOf(fooObj.fileExt.toLowerCase()) < 0) {
        } else {
          fooObj['mimeType'] = 'image';
        }
        if (fileExts.indexOf(fooObj.fileExt.toLowerCase()) < 0) {
        } else {
          fooObj['mimeType'] = 'application';
        }
        this.trainerCodeOfConduct.push(fooObj);
      }

      if (result.data[0].programId == null) {
        this.selectedProgram = '';
      } else {
        // this.selectedProgram = result.data[0].programId;
        this.selectedProgramId = result.data[0].programId;
        this.selectedProgram = result.data[0].programName;
      }
      console.log('this.trainerCodeOfConduct', this.trainerCodeOfConduct);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });
  }

  getAllSteps() {
    const param = {
      wfId: this.workflowdetail.id,
      tId: this.tenantId,
      empId: this.employeeId,
    };

    const url = ENUM.domain + ENUM.url.eep_workflowstep;

    this.eepservice.get(url, param).then((result: any) => {
      this.spinner.hide();
      this.stepsData = result.data;

      if (this.stepsData[0].status == 3) {
        this.agreeAndDisAgreeButtonStatus = true;
      } else {
        this.agreeAndDisAgreeButtonStatus = false;
      }
      console.log('Steps Value', this.stepsData);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT tttNomination Error===>', result);
    });
  }

  agreeDisagreeCOC(program, status) {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.eep_insert_workflow_responce;
    const param = {

      'wfId': this.workflowdetail.id,
      'empId': this.employeeId,
      'pgmId': program,
      'resType': status,
      'tId': this.tenantId,
      'userId': this.userId,
      'stepId': 1,
    };
    console.log('param', param);

    this.eepservice.get(url, param).then((result: any) => {
      if (result.type == true) {
        this.getCocData();
        this.spinner.hide();
        this.toastr.success(result.data[0].msg, "Success!").onHidden.subscribe(()=>
        this.toastr.clear());
        this.agreeAndDisAgreeButtonStatus = false;
        if (result.data[0].action === 2) {
          this.selectedIndex = 1;
          let dataPassed = {
            selectedIndex: 1,
            disabled: false,
          }
          this.flagData = 0;
          this.passData.emit(dataPassed);
        } else {
          let dataPassed = {
            disabled: true,
          }
          this.passData.emit(dataPassed);
          this.flagData = 2;
        }

        // selectTab(index: number): void {
        // this.selectedIndex = 1;
        // this.passData.emit(this.selectedIndex);
        // }
      } else {
        this.spinner.hide();
        this.toastr.warning(result.data[0].msg, "error!").onHidden.subscribe(()=>
        this.toastr.clear());
      }
      console.log('Insert Result', result);
    }).catch(result => {
      this.spinner.hide();
      console.log('Insert Result Error===>', result);
    });

  }
}
