import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CourseTypeCardViewerService {
  courseDetails: any = {
    mandatoryData: {},
    openData: {},
  };

  constructor() {}

  setLearnData(mandatoryData, openData) {
    this.courseDetails.mandatoryData = mandatoryData;
    this.courseDetails.openData = openData;
  }

  getLearnData() {
    return this.courseDetails;
  }
}
