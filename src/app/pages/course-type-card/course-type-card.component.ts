import { Component, OnInit, Input, ViewEncapsulation, OnChanges, EventEmitter, Output } from '@angular/core';
import { CourseTypeCardViewerService } from './course-type-card.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';

@Component({
  selector: 'ngx-course-type-card',
  templateUrl: './course-type-card.component.html',
  styleUrls: ['./course-type-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CourseTypeCardComponent implements OnInit, OnChanges {

  // @Input('learndata') learnerData: any;
  @Input('dashboardPrograms') dashboardPrograms: any;
  @Input('skeleton') skeleton: any;
  @Output() showPopup = new EventEmitter<object>();
  // skeleton: boolean = true;
  // title: string = "darshana";
  speed = 5000;
  mandatoryData: any = { online: 3, classroom: 4 };
  openData: any = { online: 0, classroom: 0 };
  courseCard: any = [
    // {
    // 	courseType: "Mandatory",
    // 	ongoingCourses: 12,
    // 	sub: [
    // 		{
    // 			name: 'Classsroom',
    // 			number: 2
    // 		}, {
    // 			name: 'Online',
    // 			number: 10
    // 		}
    // 	]
    // },
    // {
    //   courseType: "Recommended",
    //   ongoingCourses: 0,
    //   sub: [
    //     {
    //       name: 'Classsroom',
    //       number: 0
    //     },{
    //       name: 'Online',
    //       number: 0
    //     }
    //   ]
    // },
    // {
    // 	courseType: "Open",
    // 	ongoingCourses: 12,
    // 	sub: [
    // 		{
    // 			name: 'Classsroom',
    // 			number: 2
    // 		}, {
    // 			name: 'Online',
    // 			number: 10
    // 		}
    // 	]
    // }
  ];
  programsIcon = [];
  dontShowProgram = true;
  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 500,
      number: 1
    },
    {
      width: 768,
      number: 1
    },
    {
      width: 1024,
      number: 1
    }
  ];
  infinite = false;
  currentIndex = 0;
  constructor(private CTC: CourseTypeCardViewerService, private router: Router) {
  }


  ngOnInit() {
    console.log("skeleton", this.skeleton);
    if(this.skeleton == false){
      this.courseCard = this.CTC.getLearnData();
      this.mandatoryData = this.courseCard.mandatoryData;
      this.openData = this.courseCard.openData;
    }
    // for(var i=0;i<this.courseCard.length;i++){
    // 	var temp:any = this.courseCard[i];
    // 	for(var j=0;j<temp.sub.length;j++){
    // 		temp.sub[0].number = lData.mandatoryData.classroom;
    // 		temp.sub[1].number = lData.mandatoryData.online;
    // 	}
    // }
    console.log('mandatoryData===>', this.mandatoryData);
    console.log('openData===>', this.openData);
    // console.log('learnerData===>', this.learnerData);
  }
  goToOpenMandatory() {
    // this.router.navigate(['/pages/learn/', { courseType: 'Mandtory' }]);
    // this.router.navigate(['/pages/learn/'], { queryParams: { 'courseType': 'MANDATORY' } });
    this.router.navigate(['/pages/learning/course']);
    // const temp ={
    //  index:1,
    // }
    // this.LC.tabChanged(temp);
  }
  goToOpenTab() {
    // this.router.navigate(['/pages/learn/', { courseType: 'Mandtory' }]);
    // this.router.navigate(['/pages/learn/'], { queryParams: { 'courseType': 'OPEN' } });
    this.router.navigate(['/pages/learning/self']);
    // const temp ={
    //  index:1,
    // }
    // this.LC.tabChanged(temp);
  }

  ngOnChanges(){
    this.makeSliderDataReady();
  }
  makeSliderDataReady() {
    if(this.dashboardPrograms) {
      if (this.dashboardPrograms['TTT Workflows'].length !== 0) {
        const object =  {
          identifier: 'TTT',
          programName: 'Trainer Program',
          programIcon: 'fas fa-users',
        }
        this.programsIcon.push(object);
      }
      if (this.dashboardPrograms['Tools'] && this.dashboardPrograms['Tools'].length != 0 && this.dashboardPrograms['Tools'][0].cnt != 0) {
        //   console.log('tools data ==>', this.tools[0]);
        //   this.toolsIcon = true;
        // }
        const object =  {
          identifier: 'Tools',
          programName: 'Tools',
          programIcon: 'fas fa-toolbox',
        };
        this.programsIcon.push(object);
      }
      if(this.dashboardPrograms['CC Data'][1].cnt != 0){
        const object =  {
          identifier: 'Call_Coaching',
          programName: 'Call Coaching',
          programIcon: 'fas fa-headset icon',
        };
        this.programsIcon.push(object);
      }
      if (this.dashboardPrograms['EEP Data'].length !== 0) {
        const object =  {
          identifier: 'EEP',
          programName: 'EEP',
          programIcon: 'fas fa-user-graduate',
        };
        this.programsIcon.push(object);
      }
      if(this.programsIcon.length === 0) {
        this.dontShowProgram = true;
      }else {
        this.dontShowProgram = false;
      }
    }
    // if (this.proccData[1].cnt != 0) {
    //   // this.callIcon = true;
    // }
    // if (this.tools[0].cnt != 0) {
    //   console.log('tools data ==>', this.tools[0]);
    //   this.toolsIcon = true;
    // }
    // if (this.Wdata.length != 0) {
    //   this.tttworkFlowIcon = true;
    // }
    // if (this.eepdata.length != 0) {
    //   this.eepIcon = true;
    // }
    // if (this.callIcon || this.tttworkFlowIcon || this.eepIcon || this.toolsIcon) {
    //   this.dontShowSide = true;
    // }
    // console.log('this.callIcon ==>', this.callIcon);
    // console.log('this.tttworkFlowIcon ==>', this.tttworkFlowIcon);
    // console.log('this.eepIcon ==>', this.eepIcon);
    // console.log('this.toolsIcon ==>', this.toolsIcon);
    // console.log('this.dontShowSide ==>', this.dontShowSide);
    // this.cdf.detectChanges();
  }

  passDataToParent(item){
    console.log('Data on clicked ==>', item);
    this.showPopup.emit(item);
  }

}
