import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
// import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { CommonModule } from '@angular/common';
// import { CourseTypeCardComponent } from './course-type-card.component'
// import { DashboardModule } from './dashboard/dashboard.module';



@NgModule({
  imports: [
    CommonModule,
    // NgxHmCarouselModule
  ],
  declarations: [
    // CourseTypeCardComponent
  ],
  providers:[

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class CourseTypeCardModule {
}
