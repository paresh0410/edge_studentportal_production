import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseTypeCardComponent } from './course-type-card.component';

describe('CourseTypeCardComponent', () => {
  let component: CourseTypeCardComponent;
  let fixture: ComponentFixture<CourseTypeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseTypeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseTypeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
