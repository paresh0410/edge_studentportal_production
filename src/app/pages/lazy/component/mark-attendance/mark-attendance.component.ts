import { Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
// import { Router, ActivatedRoute } from '@angular/router';
import { CourseServiceProvider } from '../../../../service/course-service';
import { ENUM } from '../../../../service/enum';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-mark-attendance',
  templateUrl: './mark-attendance.component.html',
  styleUrls: ['./mark-attendance.component.scss'],
})
export class MarkAttendanceComponent implements OnInit, OnChanges {


  @Input() courseDataContentInput: any = {};
  // @Input() courseDataSummaryInput: any = {};
  @Output() componentData = new EventEmitter();

  displaymsg: boolean = false;
  courseDataSummary: any;
  courseDataContent: any;
  courseTitle: any;
  trainerId: any = 0;
  description: string;
  completionId: any;
  enrolId: any;

  constructor(
    public CSP: CourseServiceProvider,
    // private router: Router,
    // private routes: ActivatedRoute,
    private toastr: ToastrService,
  ) {
    // this.courseDataContent = CSP.getDataContent();
    // this.courseDataSummary = CSP.getDataSummary();

  }

  goBack() {
    // window.history.back();
    // this.router.navigate(['../../../learn/course-detail'],{relativeTo:this.routes});
  }

  ngOnInit() {}

  markAttendance() {
    const url = ENUM.domain + ENUM.url.markAttendance;
    // if (localStorage.getItem('userdetail')) {
    //   let userdetail = JSON.parse(localStorage.getItem('userdetail'));
    // }
    // if (localStorage.getItem('employeeId')) {
    //   let employeeId = localStorage.getItem('employeeId');
    // }
    const event = {
      'loaderFlag' : true,
      'errorFlag': false,
      'activityResponseData': null,
      'showComponent': 'mark-attendance',
    };
    // this.spinner.show();
    this.componentData.emit(event);
    const allstr =
      this.enrolId +
      '|' +
      this.employeeId +
      '|' +
      this.userdetail.id +
      '|' +
      this.completionId;

    console.log('this.courseDataContent', this.courseDataContent);
    const param = {
      actionId: 1,
      iId: this.courseDataContent.courseId,
      modId: this.courseDataContent.moduleId,
      actId: this.courseDataContent.activityId,
      // learnerId: employeeId,
      allstr: allstr,
      trainId: this.trainerId,
      tId: this.userdetail.tenantId,
      userId: this.userdetail.id,
    };
    console.log('param', param);
    // 		actionId: 1,
    // iId: this.activityData.courseId,
    // modId: this.activityData.moduleId,
    // actId: this.activityData.activityId,
    // learnerId: this.currentUserData.eid,
    // trainId: this.attendanceCheckRes.trainerId,
    // currentDate: null,
    // currentTime: null,
    // tId: this.currentUserData.tenantId,
    // userId: this.currentUserData.id
    this.CSP.getMarkedAttendence(url, param)
      .then((result: any) => {
        if (result.type) {
          //this.toastr.success(result.data[0].msg,'Successful!');
          this.displaymsg = true;
          const event = {
            'loaderFlag' : false,
            'errorFlag': false,
            'activityResponseData': result,
            'showComponent': 'mark-attendance',
          };
          if(result && result['data'] && result['data'].length !=0){
            event['responseData'] =  result['data'][0];
          }else {
            event['responseData'] =  null;
          }
          // this.spinner.show();
          this.componentData.emit(event);
          // this.spinner.show();
          // this.componentData.emit(result);
        } else {
          const event = {
            'loaderFlag' : false,
            'errorFlag': false,
            'activityResponseData': result,
            'showComponent': 'mark-attendance',
            'responseData': null,
          };
          // if(result && result['data'] && result['data'].length !=0){
          //   event['responseData'] =  result['data'][0];
          // }else {
          //   event['responseData'] =  null;
          // }
          // this.spinner.show();
          this.componentData.emit(event);
          this.toastr
            .warning('Something went wrong', 'Warning')
            .onHidden.subscribe(() => this.toastr.clear());
          this.displaymsg = false;
        }
      })
      .catch((result) => {
        const event = {
          'loaderFlag' : false,
          'errorFlag': false,
          'activityResponseData': result,
          'showComponent': 'mark-attendance',
          'responseData': null,
        };
        // if(result && result['data'] && result['data'].length !=0){
        //   event['responseData'] =  result['data'][0];
        // }else {
        //   event['responseData'] =  null;
        // }
        // this.spinner.show();
        this.componentData.emit(event);
        this.toastr
        .warning('Something went wrong', 'Warning')
        .onHidden.subscribe(() => this.toastr.clear());
        this.displaymsg = false;
        console.log('ServerResponseError :', result);
      });
  }
  userdetail: any;
  employeeId: any;
  getAttendanceData(){
    this.courseTitle = this.courseDataContent.courseName;
    if (localStorage.getItem('userdetail')) {
       this.userdetail = JSON.parse(localStorage.getItem('userdetail'));
    }
    if (localStorage.getItem('employeeId')) {
      this.employeeId = localStorage.getItem('employeeId');
    }
    const url = ENUM.domain + ENUM.url.getMarkedAttendance;
    const param = {
      cId: this.courseDataContent.courseId,
      mId: this.courseDataContent.moduleId,
      actId: this.courseDataContent.activityId,
      tId: this.userdetail.tenantId,
      empId: this.employeeId,
    };

    console.log('get mark attendance param', param);
    this.CSP.getMarkedAttendence(url, param)
      .then((result: any) => {
        console.log('RESULT==>', result.data);
        this.trainerId = result.data[0].trainerId;
        this.completionId = result.data[0].completionId;
        this.enrolId = result.data[0].enrolId;
        if (result.data[0].status) {
          this.displaymsg = true;
        }
      })
      .catch((result) => {
        console.log('ServerResponseError :', result);
      });
  }

  ngOnChanges(){
    if(this.courseDataContentInput){
      this.courseDataContent = this.courseDataContentInput;
      // this.courseDataSummary = this.courseDataSummaryInput;
      // if (localStorage.getItem('userdetail')) {
      //   this.userdetail = JSON.parse(localStorage.getItem('userdetail'));
      //   this.tenantId = this.userdetail.tenantId;
      // }
      this.getAttendanceData();
    }
  }
}
