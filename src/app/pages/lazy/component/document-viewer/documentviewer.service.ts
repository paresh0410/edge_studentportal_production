import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENUM} from '../../../../service/enum';


@Injectable()
export class DocumentViewerService {
document:any;
provider:any = "google";
    constructor(private http: HttpClient) {

    }
setDocument(document, provider){
  this.document = document;
  this.provider = provider;
}

getDocument(){
  return {document:this.document, provider:this.provider};
}
getPdfLink(link) { // s3 bucket link
  const url = ENUM.domain + ENUM.url.getPdfUrl + link;
  return new Promise(resolve => {
          this.http.get(url)
          // .map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve(null);
          });
      });
}
deletePdfLink(link) { // server pdf link
  const url = ENUM.domain + ENUM.url.deletePdfUrl + link;
  return new Promise(resolve => {
          this.http.get(url)
          // .map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve(null);
          });
      });
}
}
