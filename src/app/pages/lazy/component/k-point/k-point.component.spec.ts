import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KPointComponent } from './k-point.component';

describe('KPointComponent', () => {
  let component: KPointComponent;
  let fixture: ComponentFixture<KPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KPointComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
