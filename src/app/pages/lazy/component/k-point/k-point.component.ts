// tslint:disable-next-line:max-line-length
import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  AfterViewInit,
  OnDestroy,
  OnChanges,
  Renderer2,
  ChangeDetectorRef,
  EventEmitter,
  ViewEncapsulation,
} from "@angular/core";
// import { currentActivityData } from '../../../../service/ik-point.service';
import $ from "jquery";
// import $ from '../../../assets/js/kpointplayer';
// import { CourseServiceProvider } from '../../../../service/course-service';
import { KPointService } from "../k-point/k-point.service";
// import { UserService } from '../../../../service/user.service';
declare var kPoint;
declare var $kPointQuery;
var kpointprivate;
import { MultiLanguageService } from "../../../../service/multi-language.service";
import { Subscription } from "rxjs/Subscription";
var _this;
@Component({
  selector: "app-k-point",
  templateUrl: "./k-point.component.html",
  styleUrls: ["./k-point.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class KPointComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  kPointObj: any;
  kpplayerflag: boolean = false;
  @ViewChild("kpplayer") kpplayer: any;
  // @Input() currentActivityData: currentActivityData;
  @Input() currentActivityData: any;
  // @Output() saveEvent = new EventEmitter<any>();
  @Output() callActivityCompletion = new EventEmitter();
  passData = {
    status: null,
    duration: "",
    currentTime: "",
  };

  kPoint = {
    host: "",
    id: "",
    src: "",
    url: "",
    player: "kPlayer",
    params: {
      "add-widgets": ["hello"],
      "add-config": {
        hello: {
          enabled: 1,
        },
      },
      // 'xt': xauth_token
      offset: 0,
    },
  };

  currentProgress: any = 0;

  activityCompRes: any;
  kpointPlayer = null;
  languageChangeSubscriber = Subscription.EMPTY;
  player: any;
  alreadyAdded = false;
  // saveActCompWorking: any = false;

  constructor(
    private renderer: Renderer2,
    public multiLanguageService: MultiLanguageService,
    public cdf: ChangeDetectorRef,
    private kpointservice: KPointService
  ) {
    // this.currentActivityData = this.kpointservice.getActivity();

    // this.languageChangeSubscriber =
    //   multiLanguageService.selectedactivityLanguage.subscribe((selectedactivityLanguage) => {
    //   //do what ever needs doing when data changes
    //   if(selectedactivityLanguage){
    //     this.kpplayerflag = false;
    //     this.cdf.markForCheck();
    //     this.currentActivityData = selectedactivityLanguage;
    //     this.kpplayerflag = true;
    //     this.cdf.markForCheck();
    //     setTimeout( () => {
    //       // this.initializeKpoint();

    //       this.makeKPointDataReady(this.currentActivityData);
    //       this.kpointservice.setActivity(this.currentActivityData);
    //       this.kpointPlayer.loadVideoById(this.kPoint.id);
    //     }, 500);
    //   }

    // });
    ["", "webkit", "moz", "ms"].forEach((prefix) =>
      document.addEventListener(
        prefix + "fullscreenchange",
        () => {
          if (!window.screenTop && !window.screenY) {
            this.resizeKpoint(false);
            console.log("not fullscreen");
          } else {
            // this.windowFullScreen = true;
            this.resizeKpoint(true);
            console.log("fullscreen");
          }
        },
        false
      )
    );
  }

  ngOnInit() {
    this.kpplayerflag = true;
    console.log("kPoint - ", this.currentActivityData);

    this.kPointObj = this.kPoint;
    _this = this;
    // console.log(this.createAuthToken());
  }
  ngAfterViewInit() {
    // const param = {
    // 	"email":"test.bhavesh@bajajfinserv.in",
    // 	"displayname":"BhaveshTandel",
    // 	"userId":519,
    // 	"account_number":"23X519"
    // }
    this.initializeKpoint();
  }
  kpointConfigure(token, offset = null) {
    // let offset = 0;
    // if (offset === 0) {
    // 	this.currentActivityData.contentwatchedtime = 0;
    // }
    // if ((this.currentActivityData.contentwatchedtime / this.currentActivityData.contenttime * 100) < 97) {
    // 	offset = this.currentActivityData.contentwatchedtime;
    // }
    offset = this.currentActivityData.contentwatchedtime;
    let player = null;
    player = kPoint.Player(this.kpplayer.nativeElement, {
      kvideoId: this.kPointObj.id,
      videoHost: this.kPointObj.host,
      params: {
        autoplay: false,
        xt: token,
        // 'offset': offset,
        hide: "search, toc, logo",
        resume: true,
      },
    });
    this.kpointPlayer = player;
    player.addEventListener(player.events.loaddata, () => {
      console.log(player.events.loaddata);
      // if (this.currentActivityData.contentwatchedtime) {
      //   this.setProgress(this.currentActivityData.contentwatchedtime);
      // }
      this.setPlayerDuration(player);
    });
    player.addEventListener(player.events.onStateChange, () => {
      // console.log(player.events.onStateChange);
      // console.log('getCurrentTime - ' , player.getCurrentTime());
      // console.log('getDuration - ' , player.getDuration());
      // console.log('getPlayState - ' , player.getPlayState());
      const playstate = player.getPlayState();
      console.log("State ", playstate);
      if (playstate === player.playStates.REPLAYING) {
        offset = 0;
      }
      this.resizeKpoint(document["fullscreenElement"]);
      // document.querySelector('.kresizable')['style']['paddingBottom'] = '49%';
      // padding-bottom: 56.25%;
      this.getProgress(player);
      this.setWatchedData(player);
      // if (playstate == player.playStates.ENDED) {
      // const info = player.info;
      // const param = {
      // 	DOMAIN_NAME: info.videoHost,
      // 	KVTOKEN: info.viewToken,
      // 	XT: token
      // }
      // this.kpointservice.getKpointConsumption(param).then((result) => {
      // 	if (result) {
      // 		console.log(result)
      // 	}
      // 	this.getProgress(player);
      // 	this.setWatchedData(player);
      // }).catch((error) => {
      // 	console.log(error);
      // })
      // }
    });
    player.addEventListener(player.events.timeUpdate, () => {
      // console.log(player.events.timeUpdate);
    });
    player.addEventListener(player.events.ready, () => {
      // console.log(player.events.ready);
    });
    player.addEventListener(player.events.started, () => {
      console.log("player.events.started", player.events.started);
      player.seekTo(offset);
      // var ms = this.currentActivityData.contentwatchedtime * 1000;
      // player.seekTo(ms);
    });
    player.addEventListener(player.events.error, () => {
      // console.log(player.events.error);
    });
    player.addEventListener(player.events.reinit, () => {
      // console.log(player.events.reinit);
    });

    kpointprivate = player;
    // this.renderer.listen(player, 'loaddata', () => {
    //   console.log('kpoint loaded');
    // });
    // this.renderer.listen(player, 'play', () => {
    //   console.log('kpoint play');
    // });
    // this.renderer.listen(player, 'playing', () => {
    //   console.log('kpoint playing');
    // });
    // this.renderer.listen(player, 'ended', () => {
    //   console.log('kpoint ended');
    // });
    // this.renderer.listen(player, 'pause', () => {
    //   console.log('kpoint pause');
    // });
  }

  getProgress(player) {
    this.currentProgress = player.getCurrentTime();
  }

  setPlayerDuration(player) {
    this.currentActivityData.contenttime = player.getDuration();
  }

  // resizeKpoint(fullScreen){
  //   const element = document.querySelector('.kresizable');
  //   if(fullScreen && element){
  //     element['style']['paddingBottom'] = '56.25%';
  //   }else {
  //     element['style']['paddingBottom'] = '47.5%';
  //   }

  // }
  resizeKpoint(fullScreen) {
    setTimeout(() => {
      const ratio = this.getAspectRation(fullScreen);
      if (ratio) {
        const element = document.querySelector(".kresizable");
        if (element) {
          element["style"]["paddingBottom"] = ratio + "%";
        }
      }
    });
  }
  getAspectRation(fullScreen) {
    let parentElementSize = null;
    if (fullScreen) {
      parentElementSize = document["fullscreenElement"];
    } else {
      parentElementSize = document.querySelector("#fullDiv");
    }
    if (parentElementSize) {
      const number =
        (Number(parentElementSize["offsetHeight"]) /
          Number(parentElementSize["offsetWidth"])) *
        100;
      return number;
    }
    return null;
    //  const number =  (Number(parentElementSize['offsetHeight'])
    //   / Number(parentElementSize['offsetWidth'])) * 100;
    //   return number;
  }
  makeKPointDataReady(currentActivityData) {
    const kpointData = currentActivityData;
    const kPointUrl = currentActivityData.reference;
    if (kPointUrl.split("/").length > 0) {
      this.kPoint.host = kPointUrl.split("/")[2];
      this.kPoint.id = kPointUrl.split("/")[4];
      this.kPoint.src = kPointUrl;
      this.kPoint.url =
        kPointUrl.split("/")[0] + "//" + kPointUrl.split("/")[2];
    }
  }

  setWatchedData(player) {
    if (this.currentProgress === 0 && player) {
      this.makePassDataReady(
        null,
        player.getDuration(),
        player.getCurrentTime()
      );
      this.saveKPointActivity(
        null,
        player.getDuration(),
        player.getCurrentTime()
      );
      // this.saveEvent.emit(this.passData);
    } else if (player.getDuration() === player.getCurrentTime()) {
      this.makePassDataReady(
        "Y",
        player.getDuration(),
        player.getCurrentTime()
      );
      // this.saveEvent.emit(this.passData);
      this.saveKPointActivity(
        "Y",
        player.getDuration(),
        player.getCurrentTime()
      );
    } else {
      this.makePassDataReady(
        null,
        player.getDuration(),
        player.getCurrentTime()
      );
      // this.saveEvent.emit(this.passData);
      this.saveKPointActivity(
        null,
        player.getDuration(),
        player.getCurrentTime()
      );
    }
  }

  makePassDataReady(status, duration, currentTime) {
    this.passData = {
      status: status,
      duration: duration,
      currentTime: currentTime,
    };
  }

  saveKPointActivity(status, duration, currentTime) {
    // let activity = this.kpointservice.getActivity();
    this.currentActivityData = this.kpointservice.getActivity();
    const activity = this.currentActivityData;
    if (activity.completionstatus === "Y") {
      activity.completionstatus = "Y";
    } else if (status === "Y") {
      activity.completionstatus = "Y";
    } else {
      activity.completionstatus = "UP";
    }
    activity.contenttime = duration;
    activity.contentwatchedtime = currentTime;
    // this.CSP.saveActivityCompletion(activity);
    // if (this.saveActCompWorking === false) {

    // }
    this.saveKpointActivityCompletionData(activity);
  }

  saveKpointActivityCompletionData(activityData) {
    this.callActivityCompletion.emit(activityData);
    // this.saveActCompWorking = true;
    // this.CSP.saveActivityCompletion(activityData).then(res => {
    //   this.saveActCompWorking = false;
    //   if (res['type'] === true) {
    // 	  this.activityCompRes = res;
    //     if (this.currentActivityData.activitycompletionid === undefined || !this.currentActivityData.activitycompletionid) {
    //       this.currentActivityData.activitycompletionid = res['data'][0][0].lastid;
    //       this.kpointservice.setActivity(this.currentActivityData);
    //     }
    //     console.log('Activity completion res', res);
    //   }
    // }, err => {
    //   this.saveActCompWorking = false;
    //   console.log(err);
    // });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.languageChangeSubscriber.unsubscribe();
    if (kpointprivate) {
      this.setWatchedData(kpointprivate);
    }
  }

  ngOnChanges() {
    if (this.currentActivityData) {
      // this.playerService.setActivity(this.currentActivityData);
      this.makeKPointDataReady(this.currentActivityData);
      this.kpointservice.setActivity(this.currentActivityData);
    }
  }

  initializeKpoint() {
    try {
      if (localStorage.hasOwnProperty("userDetails")) {
        const user = JSON.parse(localStorage.getItem("userDetails"));
        if (user) {
          const param = {
            "email":user.email,
            // email: "shubham.sharma@bajajfinserv.in",
            // email: 'ashish.addala@bajajfinserv.in',
            "displayname":user.firstname + ' ' + user.lastname,
            // displayname: "shubham" + " " + "sharma",
            // displayname: 'ashish' + " " + 'addala'
            // "userId":519,
            // "account_number":"23X519"
          };
          this.kpointservice
            .getCourseData(param)
            .then((result) => {
              this.kpointConfigure(result["token"], null);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      }
    } catch (e) {}
  }
}
