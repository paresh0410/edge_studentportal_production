import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend  } from '@angular/common/http';
import { ENUM } from '../../../../service/enum';
@Injectable()
export class KPointService {

    duration:number;
    activity:any;
    constructor(private http: HttpClient, private handler: HttpBackend) {
      this.http = new HttpClient(handler);
    }

    public setActivity(activity){
      this.activity = activity;
    }
    public getActivity(){
      return this.activity;
    }
    getCourseData(param){
      const url = ENUM.domain + ENUM.url.getKpointToken;
      return new Promise(resolve => {
          this.http.post(url, param)
          // .map(res => res.json())
          .subscribe(data => {
              resolve(data);
          },
          err => {
              resolve(err);
          });
      });
  }

  getKpointConsumption(param){
    let url = ENUM.url.getCurrentViewershipDetail;
    // 'https://#DOMAIN_NAME/api/v1/xapi/view/#KVTOKEN?xt=#XT'
    url = url.replace('#DOMAIN_NAME', param.DOMAIN_NAME);
    url = url.replace('#KVTOKEN', param.KVTOKEN);
    url = url.replace('#XT', param.XT);
    return new Promise(resolve => {
        this.http.get(url)
        // .map(res => res.json())
        .subscribe(data => {
            resolve(data);
        },
        err => {
            resolve(err);
        });
    });
}

}
