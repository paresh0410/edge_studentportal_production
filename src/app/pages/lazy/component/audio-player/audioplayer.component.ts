import { Component, HostBinding, Input, OnDestroy, ChangeDetectorRef, Output, EventEmitter,OnChanges,
   Renderer2, ViewChild, AfterViewInit } from '@angular/core';
//import { PlayerService, Track } from '../../@core/utils/player.service';
import { AudioPlayerService, Track } from './audioplayer.service';
import { CourseServiceProvider } from '../../../../service/course-service';
import { MultiLanguageService } from '../../../../service/multi-language.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'ngx-audioplayer',
  templateUrl: './audioplayer.component.html',
  styleUrls: ['./audioplayer.component.scss'],
  //encapsulation: ViewEncapsulation.None
})

export class AudioPlayerComponent implements OnDestroy, AfterViewInit, OnChanges {
  @Input()
  @HostBinding('class.collapsed')
  collapsed: boolean;

  @Input() autoplay: boolean = false;
  // @Input() saveActCompWorking: boolean = false;
  @Input() currentActivityData: any = {};
  @Output() callActivityCompletion = new EventEmitter();

  track: Track;
  player: HTMLAudioElement;
  shuffle: boolean;
  @ViewChild('duration')
  durationplayer: any;
  duration : any = {
    value : 0
  };

  // currentActivityData: any;
  activityCompRes: any;
  languageChangeSubscriber = Subscription.EMPTY;
  // saveActCompWorking: any = false;

  constructor(private playerService: AudioPlayerService, public cdf: ChangeDetectorRef,
    public multiLanguageService: MultiLanguageService,
    public courseService: CourseServiceProvider, public renderer: Renderer2) {

    //this.track = this.audioList;
    //this.playerService.setTrack();
    //this.track = this.playerService.getTrack();

    //this.saveAudioActivity();
    // this.currentActivityData = this.playerService.getActivity();
    this.track = this.playerService.random();//this.playerService.getTrack(0);//;
    this.createPlayer();
    // this.languageChangeSubscriber = multiLanguageService.selectedactivityLanguage
    // .subscribe(selectedactivityLanguage => {
    //   //do what ever needs doing when data changes
    //   console.log('selectedLanguage', selectedactivityLanguage);
    //   if(selectedactivityLanguage){
    //     this.currentActivityData = selectedactivityLanguage;
    //     this.track = this.playerService.random();//this.playerService.getTrack(0);//;
    //     console.log('this.track', this.track);
    //     this.setTrack();
    //     this.startPlayerProgress();

    //   }
    // });

   //console.log('this.currentActivityData', this.currentActivityData);
  }

  ngOnDestroy() {
    this.languageChangeSubscriber.unsubscribe();
    if (this.playerService.fromTools === false) {
      this.saveAudioActivity(null);

    }

    this.player.pause();
    this.player.src = '';
    this.player.load();

  }

  prev() {
    if (!this.player.loop) {
      if (this.shuffle) {
        this.track = this.playerService.random();
      } else {
        this.track = this.playerService.prev();
      }
    }

    this.reload();
  }

  next() {
    if (!this.player.loop) {
      if (this.shuffle) {
        this.track = this.playerService.random();
      } else {
        this.track = this.playerService.next();
      }
    }

    this.reload();
  }

  playPause() {
    if (this.player.paused) {
      this.player.play();
    } else {
      this.player.pause();
    }
  }

  toggleShuffle() {
    this.shuffle = !this.shuffle;
  }

  toggleLoop() {
    this.player.loop = !this.player.loop;
    this.autoplay = this.player.loop;
  }

  setVolume(volume: number) {
    this.player.volume = volume / 100;
  }

  getVolume(): number {
    return this.player.volume * 100;
  }

  // ngAfterViewInit() {
  //   this.renderer.listen(this.audio.nativeElement, 'loadeddata', () => {
  //     console.log('audio loadeddata');
  //     const activity = this.activity;
  //     if (activity.contentwatchedtime) {
  //       this.setProgress(activity.contentwatchedtime);
  //     }
  //   });
  //   this.renderer.listen(this.audio.nativeElement, 'ended', () => {
  //     console.log('audio ended');
  //     this.makePassDataReady('Y', this.audio.nativeElement.duration, this.audio.nativeElement.currentTime);
  //     this.saveEvent.emit(this.passData);
  //     // this.saveAudioActivity('Y');
  //   });
  //   this.renderer.listen(this.audio.nativeElement, 'play', () => {
  //     console.log('audio play');
  //     this.makePassDataReady(null, this.audio.nativeElement.duration, this.audio.nativeElement.currentTime);
  //     this.saveEvent.emit(this.passData);
  //     // this.saveAudioActivity(null);
  //   });
  //   this.renderer.listen(this.audio.nativeElement, 'pause', () => {
  //     console.log('audio pause');
  //   });
  //   this.renderer.listen(this.audio.nativeElement, 'playing', () => {
  //     console.log('audio playing');
  //   });
  // }

  setProgress(duration: number) {
    this.player.currentTime = this.player.duration * duration / 100;
    console.log(this.player.currentTime);
    // this.cdf.detectChanges();
  }
  setPlayerProgress(duration: number) {
    this.player.currentTime = duration;//this.player.duration * duration / 100;
    console.log(this.player.currentTime);
    // this.cdf.detectChanges();
  }

  getPlayerProgress(): number {
    return this.player.currentTime;
  }
  getProgress(): number {
    // this.setProgressBar();
    if(this.player){
      return this.player.currentTime / this.player.duration * 100 || 0;
    }

  }

  private createPlayer() {
    this.player = new Audio();
    this.player.onended = () => this.next();
    this.setTrack();
  }

  private reload() {
    this.setTrack();
    if (this.autoplay) {
      this.player.play();
    }
  }

  private setTrack() {
      this.player.src = this.track.url;
      this.player.load();
  }

  ngAfterViewInit() {
    this.startPlayerProgress();
  }

  startPlayerProgress(){

    if (this.playerService.fromTools === false) {
      this.renderer.listen(this.player, 'loadeddata', () => {
        console.log('video loadeddata');
        // let activity = this.playerService.getActivity();
        let activity = this.currentActivityData;

        if (activity.contentwatchedtime) {
          this.setPlayerProgress(activity.contentwatchedtime);
        }
      });
      this.renderer.listen(this.player, 'ended', () => {
        console.log('video ended');
        this.saveAudioActivity('Y');
      });
      this.renderer.listen(this.player, 'play', () => {
        console.log('video play');
        this.saveAudioActivity(null);
      });
      this.renderer.listen(this.player, 'pause', () => {
        console.log('video pause')
      });
      this.renderer.listen(this.player, 'playing', () => {
        console.log('video playing');
        // this.cdf.detectChanges();
      });
      this.renderer.listen(this.player, 'timeupdate', () => {
        this.duration.value = this.getProgress();
        // this.cdf.detectChanges();
      });
    }
  }
  saveAudioActivity(status) {
    //this.playerService.setAudioDuration(this.player.duration);
    // this.currentActivityData = this.playerService.getActivity();
    const activity = this.currentActivityData;
    // let activity = this.playerService.getActivity();
    activity.contenttime = this.player.duration;
    activity.contentwatchedtime = this.player.currentTime;
    if (activity.completionstatus === 'Y') {
      activity.completionstatus = 'Y';
    } else if (status === 'Y') {
      activity.completionstatus = 'Y';
    } else {
      if (parseInt(activity.contenttime, 10) > parseInt(activity.contentwatchedtime, 10)) {
        activity.completionstatus = 'UP';
      } else {
        activity.completionstatus = 'Y';
      }
    }

    // this.courseService.saveActivityCompletion(activity);
    // if (this.saveActCompWorking === false) {

    // }
    this.saveAudioActivityCompletionData(activity);
  }

  saveAudioActivityCompletionData(activityData) {
    // this.saveActCompWorking = true;

    this.callActivityCompletion.emit(activityData);
    // this.courseService.saveActivityCompletion(activityData).then(res => {
    //   this.saveActCompWorking = false;
    //   if (res['type'] === true) {
    //     this.activityCompRes = res;
    //     if (this.currentActivityData.activitycompletionid === undefined ||
    //       !this.currentActivityData.activitycompletionid) {
    //       this.currentActivityData.activitycompletionid = res['data'][0][0].lastid;
    //       this.playerService.setActivity(this.currentActivityData);
    //     }
    //     console.log('Activity completion res', res);
    //   }
    // }, err => {
    //   this.saveActCompWorking = false;
    //   console.log(err);
    // });
  }

  ngOnChanges(){
    if(this.currentActivityData){
      this.playerService.setActivity(this.currentActivityData);
    }
  }
}
