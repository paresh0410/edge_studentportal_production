import { Component, OnInit, ChangeDetectorRef, OnDestroy, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseServiceProvider } from '../../../service/course-service';
import { ENUM } from '../../../service/enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
// import { UserService } from '../../../service/user.service';
import { AudioPlayerService, Track } from '../component/audio-player/audioplayer.service';
import { VideoPlayerService } from '../component/video-player/videoplayer.service';
// import { KPointService } from '../component/k-point/k-point.service';
import { DocumentViewerService } from '../component/document-viewer/documentviewer.service';
import { WebinarConfig } from '../../webinar-multiple/webinar-entity';
import { feature } from '../../../../environments/feature-environment';

import { MultiLanguageService } from '../../../service/multi-language.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'ngx-activity-container',
  templateUrl: './activity-container.component.html',
  styleUrls: ['./activity-container.component.scss']
})
export class ActivityContainerComponent implements OnInit, OnDestroy, OnChanges {

  @Output() componentData = new EventEmitter();
  @Input() courseDataContentInput: any = {};
  @Input() courseDataSummaryInput: any = {};
  featureConfig;
  // activityCompletionCalled = false;
  // componentEventData = {
  //   'loaderFlag' : false,
  //   'activityResponseData': null,
  // };

  trainerData: any;
  usersData: any;
  mimetype: any;
  courseDataSummary: any;
  courseDataContent: any;
  courseTitle: any;
  audioTrack: Track;
  audioTrackList: Track[] = [];
  courseDetail: any;
  url: any;
  showdiv: boolean = false;
  googleProvider = ['pdf', 'txt', 'doc', 'docx'];
  microsoftProvider = ['xlsx', 'odt', 'odp', 'ods', 'ppt', 'pptx'];
  provider: boolean = false;
  document: any;
  description: any;
  userdetail: any;
  completionstatus: any;
  courseFrom: any;
  voiceCall: boolean;
  videoCall: boolean;
  tenantId: number;
  windowFullScreen = false;
  documentURL: any =
    // tslint:disable-next-line: max-line-length
    'https://docs.google.com/gview?url=https://bhaveshedgetest.s3.amazonaws.com/EDGE-UserManual_v1-01.pdf&embedded=true';
  courseReadyFlag: boolean;
  activityName: any;
  config: WebinarConfig;
  isYoutube: boolean;
  scormStatus: any;
  languageChangeSubscriber = Subscription.EMPTY;
  constructor(
    public CSP: CourseServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public cdf: ChangeDetectorRef,
    public videoservice: VideoPlayerService,
    public audioservice: AudioPlayerService,
    public documentservice: DocumentViewerService,
    public toastr: ToastrService,
    public multiLanguageService: MultiLanguageService,
    // public kpointservice: KPointService,
    // public userService: UserService
  ) {
    this.completionstatus = 'UP';
    this.provider = false;
    this.showdiv = false;

    // this.componentEventData.loaderFlag = true;

    // this.spinner.show();
    // this.componentData.emit(true);
    this.courseReadyFlag = false;
    this.courseDetail = {
      fileinfo: {
        documenttype: null
      },
      reference: null,
      show: false
    };
    this.featureConfig = feature;

    this.languageChangeSubscriber = multiLanguageService.selectedactivityLanguage.subscribe(selectedactivityLanguage => {
      //do what ever needs doing when data changes
      console.log('selectedLanguage', selectedactivityLanguage);
      if (selectedactivityLanguage) {
        this.isYoutube = false;
        this.showdiv = false;
        // this.imageError.flag = false;
        this.cdf.markForCheck();
        this.courseDataContent = selectedactivityLanguage;
        setTimeout(() => {
          this.reIntializeData();
        }, 300);
      }
    });

  }

  ngOnInit() {

    // const event = {
    //   'loaderFlag': true,
    //   'responseData': null,
    // }
    // this.componentData.emit(event);
    // // this.courseDataContent = this.CSP.getDataContent();
    // // this.courseDataSummary = this.CSP.getDataSummary();
    // console.log('DATA--->', this.courseDataContent);
    // console.log('DATA--->', this.courseDataSummary);
    if(document['fullscreenElement']){
      this.windowFullScreen = true;
    }else {
      this.windowFullScreen = false;
    }
//     this.windowFullScreen = document['fullscreenEnabled'] || document['mozFullscreenEnabled '] ||
// document['webkitFullscreenEnabled '] || document['mswebkitFullscreenEnabled'];
console.log('windowFullScreen--->', this.windowFullScreen);
    ["", "webkit", "moz", "ms"].forEach(
      prefix => document.addEventListener(prefix + "fullscreenchange", () => {
        if (!window.screenTop && !window.screenY) {
          this.windowFullScreen = false;
          console.log('not fullscreen');
        } else {
          this.windowFullScreen = true;
          console.log('fullscreen');
        }
        this.resizeWindowIframe();
      }, false)
    );

  }
  // goBack() {
  //   // if (this.courseDataSummary.courseFrom === 1) {
  //   //   this.router.navigate(['../../../trainer-dashboard/trainer-details'], {
  //   //     relativeTo: this.routes
  //   //   });
  //   // } else if (this.courseDataSummary.courseFrom === 2) {
  //   //   this.router.navigate(['../../nomination'], {
  //   //     relativeTo: this.routes
  //   //   });
  //   // } else if (this.courseDataSummary.courseFrom === 3) {
  //   //   // this.router.navigate(['../../nomination'], {
  //   //   //   relativeTo: this.routes
  //   //   // });
  //   //   window.history.back();
  //   // } else {
  //   //   this.router.navigate(['../../../learn/course-detail'], {
  //   //     relativeTo: this.routes
  //   //   });
  //   // }
  //   // window.history.back();
  // }

  findprovider(filename) {
    if (!filename) return null;
    let fileformat = this.getFileFormat(filename);
    let provider = 'google';
    if (this.googleProvider.indexOf(fileformat) >= 0) {
      provider = 'google';
    } else if (this.microsoftProvider.indexOf(fileformat) >= 0) {
      provider = 'microsoft';
    }
    return provider;
  }

  getFileFormat(filename) {
    if (!filename) return null;
    return filename.split('.').pop();
  }

  saveKPointComp(event) {
    const activity = this.courseDetail;
    // const activity = this.playerService.getActivity();
    if (activity.completionstatus === 'Y') {
      activity.completionstatus = 'Y';
    } else if (event.status === 'Y') {
      activity.completionstatus = 'Y';
    } else {
      activity.completionstatus = 'UP';
    }
    activity.contenttime = event.duration;
    activity.contentwatchedtime = event.currentTime;

    // if (this.saveActCompWorking === false) {
    // 	this.saveActivityCompletionData(activity);
    // }
  }

  ngOnDestroy(): void {
    this.languageChangeSubscriber.unsubscribe();
  }

  // this is for get trainer data and learnerdata
  gettrainersData(data) {
    // console.log('gettrainersData', data);
    const params = {
      cId: data.courseId,
      mId: data.moduleId,
      tId: data.tenantId,
    };
    const url = ENUM.domain + ENUM.url.getTrianerList;
    this.CSP.gettrianers(url, params)
      .then((result: any) => {

        if (result.type == true) {
          this.usersData = result['data'][0];
          this.config = {
            voice: this.voiceCall,
            video: this.videoCall,
            creator: this.userdetail.roleId === 7 ? true : false,
            areaId: null,
            instanceId: null,
            tenantId: null,
            type: null,
            recording: false,
            allowCalls: true,
            startDate: '',
            EndDate: '',
          };
          this.courseReadyFlag = true;
          // this.spinner.hide();
          // this.componentEventData.loaderFlag = false;
          // this.spinner.show();
          // this.componentData.emit(this.componentEventData);
          const event = {
            'loaderFlag': false,
            'errorFlag': false,
            'activityResponseData': null,
            'showComponent': 'activity-container',
          };
          // this.spinner.show();
          this.componentData.emit(event);

          console.log(' this.trainerData --->', this.trainerData);
          // this.toastr.success('Enrol Request Sent Successfully', 'Success!');

        } else {
          // this.toastr.error(
          //   'Please try again after sometime',
          //   'Error Occured!'
          // );
          this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(() =>
            this.toastr.clear());
        }
      })
      .catch(result => {
        const event = {
          'loaderFlag': false,
          'errorFlag': true,
          'activityResponseData': null,
          'showComponent': 'activity-container',
        };
        // this.spinner.show();
        this.componentData.emit(event);
        this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(() =>
          this.toastr.clear());
        console.log('ServerResponseError :', result);
      });
  }
  getUsersData(data) {
    console.log('getUsersList', data);
    const params = {
      cId: data.courseId,
      aId: 2,
      tId: this.tenantId,
    };
    const url = ENUM.domain + ENUM.url.getUsersList;
    this.CSP.getUsers(url, params)
      .then((result: any) => {
        console.log('DATA--->', result);
        if (result.type == true) {
          this.usersData = result['data'][0];
          this.config = {
            voice: this.voiceCall,
            video: this.videoCall,
            creator: this.userdetail.roleId === 7 ? true : false,
            areaId: null,
            instanceId: null,
            tenantId: null,
            type: null,
            recording: this.userdetail.roleId === 7 ? true : false,
            allowCalls: true,
            startDate: '',
            EndDate: '',
          };
          this.courseReadyFlag = true;
          // this.spinner.hide();
          const event = {
            'loaderFlag': false,
            'activityResponseData': null,
            'errorFlag': false,
            'showComponent': 'activity-container',
          };
          // this.spinner.show();
          this.componentData.emit(event);
          // this.toastr.success('Enrol Request Sent Successfully', 'Success!');
        } else {
          // this.toastr.error(
          //   'Please try again after sometime',
          //   'Error Occured!'
          // );
          const event = {
            'loaderFlag': false,
            'activityResponseData': null,
            'errorFlag': true,
            'showComponent': 'activity-container',
          };
          // this.spinner.show();
          this.componentData.emit(event);
          this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(() =>
            this.toastr.clear());
        }
      })
      .catch(result => {
        const event = {
          'loaderFlag': false,
          'activityResponseData': null,
          'errorFlag': true,
          'showComponent': 'activity-container',
        };
        // this.spinner.show();
        this.componentData.emit(event);
        this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(() =>
          this.toastr.clear());
        console.log('ServerResponseError :', result);
      });
  }

  disableCompletionCall = false;

  webinarActivityCompletion(event) {
    // console.log('this.courseFrom', this.courseFrom)
    // console.log('$event', $event);
    // if ($event) {
    //   this.courseDetail.completionstatus = 'Y';
    //   // this.CSP.saveActivityCompletion(this.courseDetail);
    //   this.saveActivityCompletionData(this.courseDetail);
    // }

    if (event && !this.disableCompletionCall) {
      this.disableCompletionCall = true;
      if (event.webinarInformation.isCreator === true) {
        const eventOutput = {
          'loaderFlag': true,
          'responseData': null,
          'errorFlag': false,
          'showComponent': 'activity-container',
        };
        this.componentData.emit(eventOutput);
        if (this.courseDataContent) {
          let params = {
            courseId: this.courseDataContent.courseId,
            moduleId: this.courseDataContent.moduleId,
            activityId: this.courseDataContent.activityId,
            tId: this.tenantId,
          };
          params['userStr'] = this.getJoinedDataForWebinar(event.calledParticipantsList);
          if (event.type === 'callInvitation') {
            params['isCompleted'] = 0;
          }
          if (event.type === 'callEnded') {
            params['isCompleted'] = 1;
          }
          const url = ENUM.domain + ENUM.url.saveActivityCompletionWebinar;
          this.CSP.callPostApi(url, params).then(res => {
            if (res['type'] === true) {
              // this.activityCompRes = res;
              // if (this.courseDetail.activitycompletionid === undefined || !this.courseDetail.activitycompletionid) {
              //   this.courseDetail.activitycompletionid = res['data'][0][0].lastid;
              // }
              const event = {
                'loaderFlag': false,
                'responseData': res['data'][0][0],
                'errorFlag': false,
                'showComponent': 'activity-container',
              };
              if (params['isCompleted'] == 1) {
                event['showComponent'] = 'activity-container';
              }
              this.componentData.emit(event);
              this.disableCompletionCall = false;
              console.log('Activity completion res', res);
            } else {
              this.disableCompletionCall = false;
            }
          }).catch(result => {
            const event = {
              'loaderFlag': false,
              'responseData': null,
              'errorFlag': true,
              'showComponent': 'activity-container',
            };
            // if (params['isCompleted'] == 1) {
            //   event['showComponent'] = 'activity-container';
            // }
            this.componentData.emit(event);
            // this.toastr.warning('Something went wrong please try again later', 'Warning!');
            this.disableCompletionCall = false;
            console.log('ServerResponseError :', result);
          });
        }
      }
    }
  }


  getJoinedDataForWebinar(calledParticipantsList) {
    const string = calledParticipantsList.map(function (elem) {
      return elem.id + '#' + elem.enrolId;
    }).join('|');
    return string;
  }
  activityCompRes: any;
  saveActCompWorking = false;
  saveActivityCompletionData(activityData) {


    if (!this.saveActCompWorking) {
      if (Number(this.userdetail['roleId']) === 8) {
        const event = {
          // 'loaderFlag': true,
          'responseData': null,
          'errorFlag': false,
          'showComponent': 'activity-container',
        };
        // if (activityData.completionstatus === 'Y'){
        //   event['loaderFlag'] = true;
        // }else {
        //   // 'showComponent': 'activity-container',
        //   event['loaderFlag'] = false;
        // }
        event['loaderFlag'] = false;
        this.componentData.emit(event);
        this.saveActCompWorking = true;
        this.CSP.saveActivityCompletion(activityData).then(res => {
          this.saveActCompWorking = false;
          if (res['type'] === true) {
            const eventPass = {
              'loaderFlag': false,
              // 'responseData': res,
              'errorFlag': false,
            };
            if (res && res['data'] && res['data'].length != 0 && res['data'][0].length != 0) {
              if (activityData.completionstatus === 'Y') {
                eventPass['showComponent'] = 'next-activity';
                eventPass['responseData'] = res['data'][0][0];
              }
            } else {
              // 'showComponent': 'activity-container',
              eventPass['showComponent'] = 'activity-container';
              eventPass['responseData'] = null;
            }
            this.componentData.emit(eventPass);
            this.activityCompRes = res;

            if (this.courseDetail.activitycompletionid === undefined || !this.courseDetail.activitycompletionid) {
              this.courseDetail.activitycompletionid = res['data'][0][0].lastid;
            }
            console.log('Activity completion res', res);
          }
        }, err => {
          const eventPass = {
            'loaderFlag': false,
            'responseData': null,
            'errorFlag': true,
            'showComponent': 'activity-container',
          };
          this.componentData.emit(eventPass);
          this.saveActCompWorking = false;
          console.log(err);
        });
      } else {
        const eventPass = {
          'loaderFlag': false,
          'responseData': null,
          'errorFlag': false,
          'showComponent': 'activity-container',
        };
        this.componentData.emit(eventPass);
        // params['tempId'] = false;
      }
    } else {

    }


  }
  open_browser(course) {
    console.log(course);
    this.url = course.reference;
    if (this.url) {
      this.courseDetail.completionstatus = 'Y';
      this.saveActivityCompletionData(this.courseDetail);
      window.open(this.url, '_blank');
    } else {
      this.toastr.warning(
        'Please try again after sometime',
        'Error Occured!'
      ).onHidden.subscribe(() =>
        this.toastr.clear());
    }
  }
  downloadContent() {
    this.courseDetail.completionstatus = 'Y';
    this.saveActivityCompletionData(this.courseDetail);
  }
  fnYoutubeId(url) {
    const VID_REGEX =
      /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    if (url.match(VID_REGEX)) {
      this.youtubeId = url.match(VID_REGEX)[1];
    }
  }
  fnYoutubeVideo(url) {
    if (this.isURL(url)) {
      const list = ['youtu.be', 'www.youtube.com'];
      this.isYoutube = false;
      const hostname = new URL(url).hostname;
      const index = list.indexOf(hostname);
      return index >= 0 ? true : false;
    }

  }

  isURL(str) {
    try {
      new URL(str);
    } catch (_) {
      return false;
    }
    return true;
  }
  /**
   * Youtube Player
   */
  player: YT.Player;
  public youtubeId: string = '';

  playerReady = false;
  savePlayer(player) {
    this.resizeWindowIframe();
    this.player = player;
    player.setSize('100%', 500);
    console.log('player instance', player);
  }
  onStateChange(event) {
    console.log('player state', event.data);
    const player = event.target;
    this.courseDetail.contenttime = player.getDuration();
    this.courseDetail.contentwatchedtime = player.getCurrentTime()
    if (this.courseDetail.completionstatus == 'Y') {
      this.courseDetail.completionstatus == 'Y';
    } else if (parseInt(player.getDuration()) == parseInt(player.getCurrentTime())) {
      this.courseDetail.completionstatus = 'Y';
    } else {
      this.courseDetail.completionstatus == 'UP';
    }
    // this.CSP.saveActivityCompletion(this.courseDetail);
    this.saveActivityCompletionData(this.courseDetail);
  }

  ngOnChanges() {
    if (this.courseDataContentInput && this.courseDataSummaryInput) {
      this.courseDataContent = this.courseDataContentInput;
      this.courseDataSummary = this.courseDataSummaryInput;
      if (localStorage.getItem('userdetail')) {
        this.userdetail = JSON.parse(localStorage.getItem('userdetail'));
        this.tenantId = this.userdetail.tenantId;
      }
      this.reIntializeData();
    }
    console.log('On Changes')
  }

  reIntializeData() {
    this.resetDependancyContainer();
    if (this.courseDataContent) {
      this.courseTitle = this.courseDataContent.courseName;
      this.courseDetail = this.courseDataContent;
    }
    if (!this.courseDetail.completionstatus) {
      this.courseDetail.completionstatus = this.completionstatus;
    }
    if (this.courseDetail.fileinfo.documenttype === 'video') {
      this.videoservice.setVideoTrack(this.courseDetail);
      if (
        !this.courseDetail.completionstatus &&
        this.courseDetail.completionstatus !== 'Y'
      ) {
        this.courseDetail.completionstatus = this.completionstatus;
      }

      this.videoservice.setActivity(this.courseDetail);
    } else if (this.courseDetail.fileinfo.documenttype === 'audio') {
      this.audioTrack = {
        name: this.courseDetail.activityName,
        artist: this.courseDetail.activityName,
        url: this.courseDetail.reference,
        cover: 'assets/images/cover1.jpg',
      };
      this.audioTrackList = [];
      this.audioTrackList.push(this.audioTrack);
      this.audioservice.setTrack(this.audioTrackList);
      if (
        !this.courseDetail.completionstatus &&
        this.courseDetail.completionstatus !== 'Y'
      ) {
        this.courseDetail.completionstatus = this.completionstatus;
      }
    } else if (this.courseDetail.fileinfo.documenttype === 'image') {

      // this.courseDetail.completionstatus = 'Y';
      // this.CSP.saveActivityCompletion(this.courseDetail);

    } else if (
      this.courseDetail.fileinfo.documenttype === 'application' &&
      this.courseDetail.fileinfo.mimetype === 'application/pdf'
    ) {
      let provider = this.findprovider(this.courseDetail.reference);
      this.documentservice.setDocument(
        this.courseDetail.reference,
        provider
      );
      this.provider = true;
      // this.courseDetail.completionstatus = 'Y';
      // this.saveActivityCompletionData(this.courseDetail);
    } else if (this.courseDetail.fileinfo.documenttype === 'kpoint') {

    } else if (this.courseDataContent.activityTypeId === 11) {
      if (this.courseDataSummary['courseFrom'] === undefined) {
        this.voiceCall = false;
        this.videoCall = false;
        this.gettrainersData(this.courseDetail);
        this.courseFrom = 'learner';


      } else {
        this.voiceCall = true;
        this.videoCall = true;
        this.courseFrom = 'trainer';
        this.getUsersData(this.courseDetail);
      }


    }
    if ((this.courseDataContent.reference &&
      this.courseDataContent.formatId == 6 || this.courseDataContent.formatId == 8)
      || this.courseDataContent.mimeType == 'application/x-msdownload') {
      this.isYoutube = this.fnYoutubeVideo(this.courseDetail.reference);
      this.fnYoutubeId(this.courseDetail.reference);

    }

    this.showdiv = true;

    // if (this.courseDataSummary['courseFrom'] == 2 || this.courseDataSummary['courseFrom'] == 3 ) {


    //   let params = {
    //     suptId: this.courseDataContent.supertypeId,
    //     subtId: this.courseDataContent.activityTypeId,
    //     aId: this.courseDataContent.activityId,
    //     uId: this.userdetail.userId,
    //     eId: null,
    //     // tempId: true,
    //   };
    //   if (Number(this.userdetail['roleId']) === 8){
    //     params['tempId'] = true;
    //   }else {
    //     params['tempId'] = false;
    //   }
    //   var url = ENUM.domain + ENUM.url.getModuleActivity;
    //   this.CSP.getCourseData(url, params)
    //     .then((result: any) => {
    //       if (result.type) {
    //         console.log('RESULT-->', result);
    //         this.courseDetail = result.data;
    //         if(params['tempId']){
    //           this.courseDetail['enrolId'] =  this.courseDataContent.enrolId;
    //         }
    //         // console.log()
    //         //	this.courseReadyFlag = true;
    //         if (!this.courseDetail.completionstatus) {
    //           this.courseDetail.completionstatus = this.completionstatus;
    //         }
    //         if (this.courseDetail.fileinfo.documenttype === 'video') {
    //           this.videoservice.setVideoTrack(this.courseDetail);
    //           if (
    //             !this.courseDetail.completionstatus &&
    //             this.courseDetail.completionstatus !== 'Y'
    //           ) {
    //             this.courseDetail.completionstatus = this.completionstatus;
    //           }
    //           //this.courseDetail.contenttime = this.videoservice.getVideoDuration();
    //           this.videoservice.setActivity(this.courseDetail);
    //         } else if (this.courseDetail.fileinfo.documenttype === 'audio') {
    //           this.audioTrack = {
    //             name: this.courseDetail.name,
    //             artist: this.courseDetail.name,
    //             url: this.courseDetail.reference,
    //             cover: 'assets/images/cover1.jpg'
    //           };
    //           this.audioTrackList.push(this.audioTrack);
    //           this.audioservice.setTrack(this.audioTrackList);
    //           if (
    //             !this.courseDetail.completionstatus &&
    //             this.courseDetail.completionstatus !== 'Y'
    //           ) {
    //             this.courseDetail.completionstatus = this.completionstatus;
    //           }
    //           // this.audioservice.setActivity(this.courseDetail);
    //           //this.courseDetail.contenttime = this.audioservice.getAudioDuration();
    //         } else if (this.courseDetail.fileinfo.documenttype === 'image') {
    //           this.courseDetail.completionstatus = 'Y';
    //           // this.CSP.saveActivityCompletion(this.courseDetail);
    //           this.saveActivityCompletionData(this.courseDetail);
    //         } else if (
    //           this.courseDetail.fileinfo.documenttype === 'application' &&
    //           this.courseDetail.fileinfo.mimetype !== 'application/zip'
    //         ) {
    //           let provider = this.findprovider(this.courseDetail.reference);
    //           this.documentservice.setDocument(
    //             this.courseDetail.reference,
    //             provider
    //           );
    //           this.provider = true;
    //           this.courseDetail.completionstatus = 'Y';
    //           // this.CSP.saveActivityCompletion(this.courseDetail);
    //           this.saveActivityCompletionData(this.courseDetail);
    //           //this.cdf.detectChanges();
    //         } else if (this.courseDetail.fileinfo.documenttype === 'kpoint') {
    //           // this.kpointservice.setActivity(this.courseDetail);
    //         } else if (this.courseDataContent.activityTypeId === 11) {
    //           if (this.courseDataSummary['courseFrom'] === undefined) {
    //             this.voiceCall = false;
    //             this.videoCall = false;
    //             this.gettrainersData(this.courseDetail);
    //             this.courseFrom = 'learner';


    //           } else {
    //             this.voiceCall = true;
    //             this.videoCall = true;
    //             this.courseFrom = 'trainer';
    //             this.getUsersData(this.courseDetail);
    //           }


    //         }
    //         this.description = this.courseDetail.description;
    //         this.showdiv = true;
    //         // this.cdf.detectChanges();
    //         // console.log('DATA===>',temp.data);
    //         // this.spinner.hide();
    //         // this.CSP.saveActivityCompletion(this.courseDetail);
    //       //  this.componentEventData.loaderFlag = false;
    //       const event = {
    //         'loaderFlag' : false,
    //         'activityResponseData': null,
    //       };
    //       // this.spinner.show();
    //       this.componentData.emit(event);
    //             // this.spinner.show();
    //       //  this.componentData.emit(this.componentEventData);
    //       } else {
    //         // this.spinner.hide();
    //         // this.componentEventData.loaderFlag = false;
    //         // this.spinner.show();
    //         // this.componentData.emit(this.componentEventData);

    //         const event = {
    //           'loaderFlag' : false,
    //           'activityResponseData': null,
    //         };
    //         // this.spinner.show();
    //         this.componentData.emit(event);

    //         this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
    //         this.toastr.clear());
    //         // this.toastr.warning(result.data, 'Server Warning!');
    //         // this.goBack();
    //       }
    //     })
    //     .catch(result => {
    //       const event = {
    //         'loaderFlag' : false,
    //         'activityResponseData': null,
    //       };
    //       // this.spinner.show();
    //       this.componentData.emit(event);
    //       this.courseDetail = null;
    //       this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
    //       this.toastr.clear());
    //       console.log('ServerResponseError :', result);
    //     });
    // } else {
    //   this.courseTitle = this.courseDataSummary.courseTitle;
    //   let param ;
    //   param = {
    //     suptId: this.courseDataContent.supertypeId,
    //     subtId: this.courseDataContent.activityTypeId,
    //     aId: this.courseDataContent.activityId,
    //     uId: this.userdetail.userId,
    //     eId: this.courseDataContent.enrolId ? this.courseDataContent.enrolId : null,
    //     tempId : this.courseDataContent.enrolId ? false : true,
    //   };
    //   if (this.courseDataSummary['courseFrom'] == 1){
    //     if(Number(this.userdetail['roleId']) === 8){
    //       param['tempId'] = true;
    //     }else {
    //       param['tempId'] = false;
    //     }
    //   }
    //   var url = ENUM.domain + ENUM.url.getModuleActivity;
    //   if (this.courseDataContent.activityTypeId === 11 && this.courseDataSummary['courseFrom'] == 1) {
    //       this.voiceCall = true;
    //       this.videoCall = true;
    //       this.courseFrom = 'trainer';
    //       this.getUsersData(this.courseDataContent);
    //       this.description = this.courseDataContent.cDescription;
    //       this.showdiv = true;
    //    } else {
    //   this.CSP.getCourseData(url, param)
    //     .then((result: any) => {
    //       if (result.type) {
    //         console.log('RESULT-->', result);
    //         this.courseDetail = result.data;
    //         //	this.courseReadyFlag = true;
    //         if (this.courseDetail.reference && this.courseDetail.formatId == 8) {
    //           this.isYoutube = this.fnYoutubeVideo(this.courseDetail.reference);
    //           this.fnYoutubeId(this.courseDetail.reference);
    //         }
    //         if (!this.courseDetail.completionstatus) {
    //           this.courseDetail.completionstatus = this.completionstatus;
    //         }
    //         if (this.courseDetail.fileinfo.documenttype === 'video') {
    //           this.videoservice.setVideoTrack(this.courseDetail);
    //           if (
    //             !this.courseDetail.completionstatus &&
    //             this.courseDetail.completionstatus !== 'Y'
    //           ) {
    //             this.courseDetail.completionstatus = this.completionstatus;
    //           }
    //           //this.courseDetail.contenttime = this.videoservice.getVideoDuration();
    //           this.videoservice.setActivity(this.courseDetail);
    //         } else if (this.courseDetail.fileinfo.documenttype === 'audio') {
    //           this.audioTrack = {
    //             name: this.courseDetail.name,
    //             artist: this.courseDetail.name,
    //             url: this.courseDetail.reference,
    //             cover: 'assets/images/cover1.jpg'
    //           };
    //           this.audioTrackList.push(this.audioTrack);
    //           this.audioservice.setTrack(this.audioTrackList);
    //           if (
    //             !this.courseDetail.completionstatus &&
    //             this.courseDetail.completionstatus !== 'Y'
    //           ) {
    //             this.courseDetail.completionstatus = this.completionstatus;
    //           }
    //           this.audioservice.setActivity(this.courseDetail);
    //           //this.courseDetail.contenttime = this.audioservice.getAudioDuration();
    //         } else if (this.courseDetail.fileinfo.documenttype === 'image') {
    //           this.courseDetail.completionstatus = 'Y';
    //           // this.CSP.saveActivityCompletion(this.courseDetail);
    //           this.saveActivityCompletionData(this.courseDetail);
    //         } else if (
    //           this.courseDetail.fileinfo.documenttype === 'application' &&
    //           this.courseDetail.fileinfo.mimetype == 'application/pdf'
    //         ) {
    //           let provider = this.findprovider(this.courseDetail.reference);
    //           this.documentservice.setDocument(
    //             this.courseDetail.reference,
    //             provider
    //           );
    //           this.provider = true;
    //           this.courseDetail.completionstatus = 'Y';
    //           // this.CSP.saveActivityCompletion(this.courseDetail);
    //           this.saveActivityCompletionData(this.courseDetail);
    //           // .cdf.detectChanges();
    //         } else if (this.courseDetail.fileinfo.documenttype === 'kpoint') {
    //           // this.kpointservice.setActivity(this.courseDetail);
    //         } else if (this.courseDataContent.activityTypeId === 11) {
    //           if (this.courseDataSummary['courseFrom'] === undefined) {
    //             this.voiceCall = false;
    //             this.videoCall = false;
    //             this.gettrainersData(this.courseDetail);
    //             this.courseFrom = 'learner';
    //           } else {
    //             this.voiceCall = true;
    //             this.videoCall = true;
    //             this.courseFrom = 'trainer';
    //             this.getUsersData(this.courseDetail);
    //           }
    //         }
    //         this.description = this.courseDetail.description;
    //         this.showdiv = true;
    //         // this.cdf.detectChanges();
    //         // console.log('DATA===>',temp.data);
    //         // this.spinner.hide();
    //         // this.CSP.saveActivityCompletion(this.courseDetail);
    //         const event = {
    //           'loaderFlag' : false,
    //           'activityResponseData': null,
    //         };
    //         // this.spinner.show();
    //         this.componentData.emit(event);
    //       } else {
    //         // this.spinner.hide();
    //         const event = {
    //           'loaderFlag' : false,
    //           'activityResponseData': null,
    //         };
    //         // this.spinner.show();
    //         this.componentData.emit(event);
    //         this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
    //         this.toastr.clear());
    //         // this.goBack();
    //       }
    //     })
    //     .catch(result => {
    //       this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
    //       this.toastr.clear());
    //       const event = {
    //         'loaderFlag' : false,
    //         'activityResponseData': null,
    //       };
    //       // this.spinner.show();
    //       this.componentData.emit(event);
    //       // this.spinner.hide();
    //       this.courseDetail = null;
    //       console.log('ServerResponseError :', result);
    //     });
    //   }
    // }
    console.log('course=============================>>>>>>>>>>>>>', this.courseTitle);
  }

  // updateModuleActivityList(responseData, activityData){
  //   for(let index = 0; this.cur)
  // }

  getUpdatedActivity() {
    const event = {
      'loaderFlag': true,
      'responseData': null,
      'errorFlag': false,
      'showComponent': 'activity-container',
    };
    this.componentData.emit(event);
    const params = {
      activityId: this.courseDataContent.activityId,
      moduleId: this.courseDataContent.moduleId,
      courseId: this.courseDataContent.courseId,
      enrolId: this.courseDataContent.enrolId,
    };
    const url = ENUM.domain + ENUM.url.getUpdatedActivityData;
    this.CSP.callPostApi(url, params).then(res => {
      if (res['type'] === true) {
        const event = {
          'loaderFlag': false,
          'responseData': res['data']['updatedActivityData'],
          'errorFlag': false,
          'showComponent': 'activity-container',
        };

        this.componentData.emit(event);
        // this.disableCompletionCall = false;
        console.log('Activity completion res', res);
      } else {
        const event = {
          'loaderFlag': false,
          'responseData': null,
          'errorFlag': false,
          'showComponent': 'activity-container',
        };

        this.componentData.emit(event);
      }
    }).catch(result => {
      const event = {
        'loaderFlag': false,
        'responseData': null,
        'errorFlag': true,
        'showComponent': 'activity-container',
      };
      // if (params['isCompleted'] == 1) {
      //   event['showComponent'] = 'activity-container';
      // }
      this.componentData.emit(event);
    });
  }

  resizeWindowIframe() {
    setTimeout(() => {
      const node = document.querySelector('youtube-player > iframe') as HTMLIFrameElement;
      if (node) {
        node.height =
          String(document.querySelector('#fullDiv').clientHeight - 10);
        this.playerReady = true;
      }
    }, 100);
  }

  getScormStatus(event) {
    this.scormStatus = event;
  }

  imageLoaded(event) {
    console.log("Event", event);
    if (event && event.target) {
      const x = event.srcElement.x;
      const y = event.srcElement.y;
      if ((x !== 0) && (y !== 0)) {
        const width = event.srcElement.width;
        const height = event.srcElement.height;
        const portrait = height > width ? true : false;
        console.log('Loaded: ', width, height, 'portrait: ', portrait);
        if (this.courseDetail.completionstatus !== 'Y') {
          this.courseDetail.completionstatus = 'Y';
          this.saveActivityCompletionData(this.courseDetail);
        }
      }
    }
  }
  errorHandler(event) {
    console.log(event);
    if (event && event['type']) {
      // this.imageError.flag = true;
      this.sendContentLoadErrorMessage('image', 'Image file corrupted or not accessible. Please Contact site administrator');
    }
    // if(event.type )
    // event.target.src = "https://cdn.browshot.com/static/images/not-found.png";
  }
  dependencyConatiner = {
    displayFlag: false,
    message: '',
  }
  sendContentLoadErrorMessage(type, content) {
    switch (type) {
      case 'pdf':
        if ('file_corrupt') {
          this.dependencyConatiner.message = 'PDF file is corrupted. Please Contact site administrator.';
        } else {
          this.dependencyConatiner.message = 'Unable to generate PDF link. Please try again later.';
        }
        this.dependencyConatiner.displayFlag = true;
        break;
      case 'image':
        this.dependencyConatiner.displayFlag = true;
        this.dependencyConatiner.message = content;
        // this.setDependencyConatinerDataReadyNew();
        break;
      default:
        break;
    }

  }

  resetDependancyContainer() {
    this.dependencyConatiner = {
      displayFlag: false,
      message: '',
    };
  }
}

