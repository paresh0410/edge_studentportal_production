import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  Input,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { LearnServiceProvider } from "../../../service/learn-service";
import { CourseDetailServiceProvider } from "../../../service/course-detail-service";
import { CourseServiceProvider } from "../../../service/course-service";
import { ENUM } from "../../../service/enum";
import { NgxSpinnerService } from "ngx-spinner";
import { MultiLanguageService } from "../../../service/multi-language.service";
// import { MENU_ITEMS } from '../../pages-menu';
// import { StarRatingComponent } from 'ng-starrating';
@Component({
  selector: "ngx-module-activity-container",
  templateUrl: "./module-activity-container.component.html",
  styleUrls: ["./module-activity-container.component.scss"],
})
export class ModuleActivityContainerComponent implements OnInit, OnDestroy {
  // menu = MENU_ITEMS;
  full: any;
  register: boolean = true;
  showPopup: boolean = false;
  quizIntro: boolean = false;
  quizPass: boolean = false;
  share: boolean = false;
  rating: boolean = false;
  ratingText: boolean = false;
  round: any = {};
  outerStroke = "#ffc107";
  innerColor = "#e83e8c";
  // modules: any = [
  //   {
  //     name: 'Lorem Ipsum is simply dummy text of the printin',
  //     mStartdate: '12 jan 2021',
  //     actCount: '3',
  //     disable: false,
  //     show: false,
  //     activities: [
  //       {
  //         actName: 'Activity Name',
  //         actDate: '12-dec-2020',
  //         actCompleted: true,
  //         actEnable: true,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //     ]
  //   },
  //   {
  //     name: 'Lorem Ipsum is simply dummy text of the printin',
  //     mStartdate: '12 jan 2021',
  //     actCount: '5',
  //     disable: true,
  //     show: false,
  //     activities: [
  //       {
  //         actName: 'Activity Name',
  //         actDate: '12-dec-2020',
  //         actCompleted: true,
  //         actEnable: true,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //     ]
  //   },
  //   {
  //     name: 'Lorem Ipsum is simply dummy text of the printin',
  //     mStartdate: '12 jan 2021',
  //     actCount: '5',
  //     disable: false,
  //     show: false,
  //     activities: [
  //       {
  //         actName: 'Activity Name',
  //         actDate: '12-dec-2020',
  //         actCompleted: true,
  //         actEnable: true,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //     ]
  //   },
  //   {
  //     name: 'Lorem Ipsum is simply dummy text of the printin',
  //     mStartdate: '12 jan 2021',
  //     actCount: '5',
  //     disable: false,
  //     show: false,
  //     activities: [
  //       {
  //         actName: 'Activity Name',
  //         actDate: '12-dec-2020',
  //         actCompleted: true,
  //         actEnable: true,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //     ]
  //   },
  //   {
  //     name: 'Lorem Ipsum is simply dummy text of the printin',
  //     mStartdate: '12 jan 2021',
  //     actCount: '8',
  //     disable: false,
  //     show: false,
  //     activities: [
  //       {
  //         actName: 'Activity Name',
  //         actDate: '12-dec-2020',
  //         actCompleted: true,
  //         actEnable: true,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //       {
  //         actName: 'Activity Name',
  //         actCompleted: false,
  //         actEnable: false,
  //       },
  //     ]
  //   },
  // ];

  private body = document.querySelector("body");

  // New Variables
  courseDetailSummary: any = [];
  isServiceDisabled: any;
  userdetails: any;
  moduleList: any = [];
  employeeId = null;

  //  Activity Data Variable
  checkResponse: any;
  disabledGoToCourse = false;
  passKey = "";
  showConatinerSpinner = false;
  showComponent = "";

  passWrong: boolean = false;
  submitted: boolean = false;
  password: string = "";
  showDescri: boolean = false;

  noDataFound = {
    title: "",
    description: "",
    showImage: true,
  };

  @Input("activityModuleList") activityModuleList = {
    showPassedList: false,
    listData: [],
    currentCourseDetails: null,
  };
  defaultLangugae = {
    languageId: null,
    languageName: "Select Language",
    // languageCode : null,
  };
  // languageChangeSubscriber = null;
  windowFullScreen = false;
  constructor(
    public toastr: ToastrService,
    public LSP: LearnServiceProvider,
    public CDSP: CourseDetailServiceProvider,
    public cdf: ChangeDetectorRef,
    public spinner: NgxSpinnerService,
    public multiLanguageService: MultiLanguageService,
    public CSP: CourseServiceProvider
  ) {
    this.userdetails = JSON.parse(localStorage.getItem("userdetail"));
    this.employeeId = localStorage.getItem("employeeId");
    const themeData = JSON.parse(localStorage.getItem("theme"));
    this.innerColor = themeData["pageBackground"];
    this.outerStroke = themeData["button"];
    // this.languageChangeSubscriber = multiLanguageService.selectedactivityLanguage;
    // this.languageChangeSubscriber.subscribe(selectedactivityLanguage => {
    //   //do what ever needs doing when data changes
    //   console.log('selectedLanguage', selectedactivityLanguage);
    // });
  }

  ngOnInit() {
    // this.full = document.getElementById('fullDiv');
    // console.log('full', this.full);

    // Get Course Information
    this.isServiceDisabled = this.LSP.subject.subscribe(() => {
      console.log("Data changed ===> ", this.activityModuleList);
      if (this.activityModuleList.showPassedList) {
        this.moduleList = this.activityModuleList.listData;
        this.courseDetailSummary = this.activityModuleList.currentCourseDetails;
        this.makeModuleActivityDataReady();
        this.goToFirstAcitivity();
      } else {
        this.getCourseDetails();
      }
    });


    ["", "webkit", "moz", "ms"].forEach(
      prefix => document.addEventListener(prefix + "fullscreenchange", () => {
        if (!window.screenTop && !window.screenY) {
          this.windowFullScreen = false;
          console.log('not fullscreen');
        } else {
          this.windowFullScreen = true;
          console.log('fullscreen');
        }
      }, false)
    );
  }

  showdiv(i) {
    // console.log('data', data);
    // if (this.modules[i].activities.length > 1) {
    //   for (let k = 0; k < this.modules.length; k++) {
    //     if (k !== i) {
    //       this.modules[k].show = false;
    //     }
    //   }
    // }
    // this.modules[i].show = !this.modules[i].show;

    // if (this.moduleList[i].isDisable == "Y") {
    //   let date = "";
    //   if (this.moduleList[i].mStartDate) {
    //     date = this.moduleList[i].mStartDate;
    //   } else {
    //     date = "NA";
    //   }
    //   this.noDataFound.title =
    //     this.moduleList[i].moduleName + " will start from " + date;
    //   this.noDataFound.description = "";
    //   this.noDataFound.showImage = false;
    //   this.showComponent = "noDataFound";
    //   return null;
    // }

    if (this.moduleList[i].isDisable == "Y") {
      this.resetBeginContainerData();
    }
    if (!this.setBeginContainerData(this.moduleList[i], "module")) {
      if (this.moduleList[i].list.length > 0) {
        for (let k = 0; k < this.moduleList.length; k++) {
          if (k !== i) {
            this.moduleList[k].show = false;
          }
        }
      }
      this.moduleList[i].show = !this.moduleList[i].show;
    } else {
      this.showBeginContainer = true;
    }
  }

  goToFirstAcitivity() {
    if (
      this.moduleList[0].isDisable === "Y" ||
      this.moduleList[0].list[0].completed
    ) {
      this.currentModuleIndex = 0;
      this.currentActIndex = 0;
      this.currentActivityData = this.moduleList[0].list[0];
      this.showdiv(0);
      this.goToAutoNext(true);
    } else if (
      (this.moduleList[0].list[0].wait ||
        this.moduleList[0].list[0].byTrainer == 1) &&
      this.autoPlayFlag
    ) {
      this.currentModuleIndex = 0;
      this.currentActIndex = 0;
      this.currentActivityData = this.moduleList[0].list[0];
      this.showdiv(0);
      this.goToAutoNext(true);
    } else {
      this.showdiv(0);
      // this.goToCourse(this.moduleList[0].list[0], 0 , 0);
      this.showBeginBox(this.moduleList[0].list[0], 0, 0);
      // this.checkIfNextExist();
      // this.checkIfPreviousExist();
    }
  }

  fullScreen() {
    this.full = document.getElementById("fullDiv");
    if (this.full.requestFullscreen) {
      this.full.requestFullscreen();
    } else if (this.full.mozRequestFullScreen) {
      this.full.mozRequestFullScreen();
    } else if (this.full.webkitRequestFullscreen) {
      this.full.webkitRequestFullscreen();
    } else if (this.full.msRequestFullscreen) {
      this.full.msRequestFullscreen();
    }
  }

  // modals: any = {
  //   1: 'share',
  //   2: 'rating',
  // }

  // pop-up
  openPop(pop) {
    // for(let item in this.modals) {
    //   if(item === pop) {
    //   }
    // }
    console.log("pop ====", pop);
    this.showPopup = true;

    switch (pop) {
      case "share":
        this.share = true;
        break;

      case "rating":
        this.rating = true;
        break;

      case "quizPass":
        this.quizPass = true;
        break;

      case "quizIntro":
        this.quizIntro = true;
        break;

      default:
        break;
    }
    this.body.style.overflow = "hidden";
    this.cdf.markForCheck();
  }

  closePopup() {
    this.showPopup = false;
    this.share = false;
    this.rating = false;
    this.quizPass = false;
    this.quizIntro = false;
    setTimeout(() => {
      this.body.style.overflow = "inherit";
    }, 1000);
  }

  // rating
  onRate(rate) {
    console.log("rate", rate);
    if (rate.newValue > 0) {
      this.ratingText = true;
    }
  }

  ngOnDestroy() {
    if (this.isServiceDisabled) {
      this.isServiceDisabled.unsubscribe();
    }
    // if(this.languageChangeSubscriber){
    //   this.languageChangeSubscriber.unsubscribe();
    // }
  }

  getCourseDetails() {
    this.spinner.show();
    this.courseDetailSummary = this.LSP.getcourseDetailList();
    console.log("this.courseDetailSummary", this.courseDetailSummary);
    // this.userdetails = JSON.parse(localStorage.getItem('userdetails'));
    // this.employeeId = localStorage.getItem('employeeId');
    // this.activeRole = localStorage.getItem('roleName');
    // console.log('this.activeRole:', this.activeRole);
    // console.log('userdetails course detail page:', this.CDSPuserdetails);
    const param = {
      courseId: this.courseDetailSummary.id,
      tId: this.userdetails.tenantId,
      eId: this.courseDetailSummary.enrolId,
    };
    const url = ENUM.domain + ENUM.url.getLearnModules;
    this.CDSP.getCourseDetailsData(url, param)
      .then((result) => {
        this.spinner.hide();
        // let temp = result;
        // console.log('fetched course modules:', result);
        // this.courseDetailTabArray = temp.data;
        if (result && result["data"] && result["data"].length !== 0) {
          this.moduleList = result["data"];
          console.log("Response Module List ===>", this.moduleList);
          this.makeModuleActivityDataReady();
          this.goToFirstAcitivity();
          this.autoPlayFlag = result["autoPlayFlag"] ? true : false;
          // this.autoPlayFlag = false;
        } else {
          // this.toastr.

          this.toastr
            .warning("No Data Available", "Warning!")
            .onHidden.subscribe(() => this.toastr.clear());
          window.history.back();
          console.log("No Data in course", result);
        }
      })
      .catch((result) => {
        this.spinner.hide();
        this.toastr
          .warning("Something went wrong", "Warning!")
          .onHidden.subscribe(() => this.toastr.clear());
      });
    // console.log('SummaryDATA--->', this.courseDetailSummary);
  }

  makeModuleActivityDataReady() {
    for (let i = 0; i < this.moduleList.length; i++) {
      // console.log(fish[i]);
      // if (i === 0) {
      //   this.moduleList[i]['show'] = true;
      // }else {
      //     this.moduleList[i]['show'] = false;
      // }
      this.moduleList[i]["show"] = false;
      this.moduleList[i].list.forEach((element) => {
        element = this.finddependetactivity(element);
      });
    }
    console.log("Final Module List ===>", this.moduleList);
  }
  finddependetactivity(detail) {
    detail["begin"] = true;
    detail["wait"] = false;
    detail["completed"] = false;
    detail["pending"] = false;
    if (detail.wCompleted === 1) {
      detail.completed = true;
      detail.wait = false;
      detail.begin = false;
      detail.pending = false;
    } else if (detail.dependentActId !== 0) {
      detail["dependentActName"] = detail.dependentActName;
      if (detail.dependentActCompStatus === 1) {
        detail.wait = false;
        detail.begin = true;
        detail.completed = false;
        detail.pending = false;
      } else {
        detail.wait = true;
        detail.begin = false;
        detail.completed = false;
        detail.pending = false;
      }
    }
    detail["actIcon"] = this.bindActivityIcon(detail);
    detail["actFormatName"] = this.bindActivityFormatName(detail);
    // detail["showBeginContainer"] = this.bindBeginContainer(detail);
    return detail;
    // console.log('Data ==>', detail);
    // if (detail.wCompleted == 1) {
    //   return 3;
    // } else if (detail.wCompleted == 0) {
    //   if (detail.dependentActId == 0) {
    //     if (detail.byTrainer == 1) {
    //       return 2;
    //     } else {
    //       return 1;
    //     }
    //   } else if (detail.dependentActId != 0) {
    //     if (detail.dependentActCompStatus == 0) {
    //       return 4;
    //     } else if (detail.dependentActCompStatus == 1) {
    //       return 1;
    //     }
    //   } if (detail.byTrainer == 1) {
    //     return 2;
    //   }
    // }
    // if(detail.dependentActId == 0){
    //   if(detail.wCompleted == 0 ){
    //     return 1;
    //   }
    // } else if(detail.wCompleted == 0 && detail.byTrainer == 1){
    //   return 2;
    // } else if(detail.wCompleted == 1){
    //   return 3;
    // } else if( detail.dependentActId != 0){
    //   if( detail.wCompleted == 0 && detail.dependentActCompStatus == 1){
    //     return 1;
    //   } else if(detail.wCompleted == 0 && detail.dependentActCompStatus == 0){
    //     return 4
    //   }
    // }
  }

  // currentActivityClicked(){

  // }

  currentActivityData: any;

  // goToCourse(data, moduleIndex , activityIndex) {
  //   this.currentModuleIndex = moduleIndex;
  //   this.currentActIndex = activityIndex;
  //   if (data['wait']){
  //     this.noDataFound.title = 'Complete activity " '
  //     + data.dependentActName + '" from module "' +  data.dependentModName + '" to unlock';
  //     this.noDataFound.description = '';
  //     this.noDataFound.showImage = false;
  //     this.showComponent = 'noDataFound';
  //     return null;
  //   }
  //   if(data.byTrainer == 1){
  //     if(data.completed){
  //       this.noDataFound.title = 'Attendance is marked by trainer';
  //     }else{
  //       this.noDataFound.title = 'Attendance will be marked by trainer';
  //     }

  //     this.noDataFound.description = '';
  //     this.noDataFound.showImage = false;
  //     this.showComponent = 'noDataFound';
  //     return null;
  //   }
  //   // if(data.completed){
  //   //   let dateCompleted;
  //   //   if(data.completedDate){
  //   //     const date = new Date(data.completedDate);
  //   //     dateCompleted = date.getDate() + date.getMonth() +  date.getFullYear();
  //   //   }else {
  //   //     dateCompleted = 'NA';
  //   //   }
  //   //   this.noDataFound.title = 'Completed on ' + dateCompleted;
  //   //   this.noDataFound.description = '';
  //   //   this.noDataFound.showImage = false;
  //   //   this.showComponent = 'noDataFound';
  //   //   return null;
  //   // }
  //   this.disabledGoToCourse = true;
  //   this.showComponent = '';
  //   // this.spinner.show();
  //   setTimeout(() =>{
  //                     this.showConatinerSpinner = true;
  //                     this.cdf.markForCheck();
  //                   }, 200);
  //   this.showArrow = true;

  //   this.currentActivityData = data;
  //   // this.CSP.setCourse(this.courseDetailSummary, data);
  //   console.log("DATA---->", data);
  //   if (data.activity_type === 'Quiz') {
  //     let param = {
  //       qId: data.quizId,
  //       tId: this.userdetails.tenantId,
  //       uId: this.employeeId,
  //       cId: data.courseId,
  //       mId: data.moduleId,
  //       aId: data.activityId,
  //       enId : data.enrolId,
  //     };
  //     const url = ENUM.domain + ENUM.url.checkQuizUser;
  //     this.CDSP.getCourseDetailsData(url, param)
  //       .then(result => {
  //         var temp: any = result;
  //         if (temp.data !== null || temp.data !== undefined) {
  //           if (temp.data.length > 0) {
  //             console.log("CHECk QUIZ FOR USER---->", temp.data[0]);
  //             this.checkResponse = temp.data[0];
  //             if (temp.data[0].flag == "true") {
  //               console.log("PassCheck===>", temp.data[0]);
  //               if (temp.data[0].qPassword == 0) {
  //                 if (this.checkResponse.instructions) {
  //                   this.disabledGoToCourse = false;

  //                   setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //                   this.openPop('quizIntro');
  //                   // this.spinner.hide();
  //                   // this.displayInstructions = true;
  //                 } else {
  //                   // this.displayInstructions = false;
  //                   this.disabledGoToCourse = false;
  //                   // this.getSelectedActData(data, () => {

  //                   //    this.cdf.markForCheck();
  //                   // });

  //                   setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);
  //                   this.showComponent = 'quiz';
  //                   this.showArrow = false;
  //                   // this.spinner.hide();
  //                   // this.router.navigate(["quiz"], { relativeTo: this.routes });
  //                 }
  //               } else {
  //                 this.disabledGoToCourse = false;
  //                 setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //                 // this.spinner.hide();
  //                 this.openPop('quizPass');
  //                 // this.passCheck = true;
  //                 this.passKey = temp.data[0].passWord;
  //               }
  //             } else {
  //               this.disabledGoToCourse = false;
  //               setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //               this.noDataFound.title = 'Warning';
  //               this.noDataFound.showImage = false;
  //               this.noDataFound.description = temp.data[0].msg;
  //               this.showComponent = 'noDataFound';
  //                this.cdf.markForCheck();
  //               const message = temp.data[0].msg + ' ' + temp.data[0].nextAttemptDate;
  //               this.toastr.warning(message, "Warning!").onHidden.subscribe(()=>
  //               this.toastr.clear());
  //             }
  //           } else {
  //             this.disabledGoToCourse = false;
  //             // this.spinner.hide();
  //             setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //             this.toastr.warning(
  //               "Something went wrong please try again later",
  //               "Error!"
  //             ).onHidden.subscribe(()=>
  //             this.toastr.clear());
  //           }
  //         } else {
  //           // this.spinner.hide();
  //           this.disabledGoToCourse = false;
  //           setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //           this.noDataFound.title = 'Warning';
  //           this.noDataFound.description = '';
  //           this.noDataFound.showImage = true;
  //           this.showComponent = 'noDataFound';
  //           this.noDataFound.description = 'Something went wrong please try again later';
  //            this.cdf.markForCheck();
  //           this.toastr.warning(
  //             "Something went wrong please try again later",
  //             "Error!"
  //           ).onHidden.subscribe(()=>
  //           this.toastr.clear());
  //         }
  //       })
  //       .catch(result => {
  //         // this.spinner.hide();
  //         setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //         this.disabledGoToCourse = false;

  //         this.noDataFound.title = 'Warning';
  //         this.noDataFound.showImage = true;
  //         this.showComponent = 'noDataFound';
  //         this.noDataFound.description = 'Something went wrong please try again later';
  //         console.log("ServerResponseError :", result);
  //         this.toastr.warning(
  //           "Something went wrong please try again later",
  //           "Error!"
  //         ).onHidden.subscribe(()=>
  //         this.toastr.clear());
  //         // this.spinner.hide();
  //       });
  //   }else if (data.activity_type === 'Feedback')  {
  //     this.disabledGoToCourse = false;
  //     let params = {
  //       fId: data.feedbackId,
  //       tId: this.userdetails.tenantId,
  //       eId: this.employeeId,
  //       cId: data.courseId,
  //       mId: data.moduleId,
  //       aId: data.activityId,
  //       enId : data.enrolId,
  //     };
  //     const url = ENUM.domain + ENUM.url.checkFeedbackUser;
  //     this.CDSP.getCourseDetailsData(url, params)
  //       .then(result => {
  //         var temp: any = result;
  //         if (temp.data !== null || temp.data !== undefined) {
  //           if (temp.data.length > 0) {
  //             console.log("CHECk FEEDBACK FOR USER---->", temp.data[0]);
  //             if (temp.data[0].flag == "true") {
  //               // this.CDSP.setFeedbackSummary(data);
  //               this.disabledGoToCourse = false;
  //               // this.spinner.hide();
  //               // this.router.navigate(["feedback"], { relativeTo: this.routes });
  //               // this.showComponent = 'feedback';
  //               this.getSelectedActData(data, () => {
  //                 setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);
  //                 this.showComponent = 'feedback';

  //               });
  //             } else {
  //               this.disabledGoToCourse = false;
  //               setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //               this.noDataFound.title = 'Warning';
  //               this.noDataFound.showImage = false;
  //               this.noDataFound.description = temp.data[0].msg;
  //               this.showComponent = 'noDataFound';
  //                this.cdf.markForCheck();
  //               // this.spinner.hide();
  //               this.toastr.warning(temp.data[0].msg, "Warning!").onHidden.subscribe(()=>
  //               this.toastr.clear());
  //             }
  //           } else {

  //             this.noDataFound.title = 'Warning';
  //             this.noDataFound.showImage = true;
  //             this.noDataFound.description = 'Something went wrong please try again later';
  //             this.showComponent = 'noDataFound';
  //             this.disabledGoToCourse = false;
  //             setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //             // this.spinner.hide();
  //             this.toastr.warning(
  //               "Something went wrong please try again later",
  //               "Error!"
  //             ).onHidden.subscribe(()=>
  //             this.toastr.clear());
  //           }
  //         } else {

  //           this.noDataFound.title = 'Warning';
  //           this.noDataFound.showImage = true;
  //           this.noDataFound.description = 'Something went wrong please try again later';
  //           this.showComponent = 'noDataFound';
  //           this.disabledGoToCourse = false;
  //           setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //           this.toastr.warning(
  //             "Something went wrong please try again later",
  //             "Error!"
  //           ).onHidden.subscribe(()=>
  //           this.toastr.clear());
  //         }
  //       })
  //       .catch(result => {

  //         this.noDataFound.title = 'Warning';
  //         this.noDataFound.showImage = true;
  //         this.noDataFound.description = 'Something went wrong please try again later';
  //         this.showComponent = 'noDataFound';
  //         this.disabledGoToCourse = false;
  //         setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //         // this.spinner.hide();
  //         console.log("ServerResponseError :", result);
  //         this.toastr.warning(
  //           "Something went wrong please try again later",
  //           "Error!"
  //         ).onHidden.subscribe(()=>
  //         this.toastr.clear());
  //         // this.spinner.hide();
  //       });
  //   } else if (data.activity_type === 'Attendance') {
  //     this.disabledGoToCourse = false;
  //     // this.spinner.hide();
  //     this.getSelectedActData(data, () => {
  //       setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);
  //       this.showComponent = 'mark-attendance';

  //     });
  //     // this.showConatinerSpinner = false;
  //     //  this.cdf.markForCheck();

  //     // this.router.navigate(["mark-attendance"], { relativeTo: this.routes });
  //   }else if(data.activity_type === 'Webinar'){
  //     this.disabledGoToCourse = false;
  //     // this.spinner.hide();
  //     setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //     this.showComponent = 'course';
  //     // this.router.navigate(['course'], { relativeTo: this.routes });
  //   } else {
  //     this.disabledGoToCourse = false;
  //     // this.showConatinerSpinner = false;
  //       //  this.cdf.markForCheck();
  //     // this.spinner.hide();
  //     if (data && data['reference'] && data['reference'] !== ''){
  //       // this.router.navigate(['course'], { relativeTo: this.routes });
  //       this.getSelectedActData(data, () => {
  //         setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);
  //         this.showComponent = 'course';

  //       });
  //     }else{
  //       setTimeout(() =>{
  //                     this.showConatinerSpinner = false;
  //                     this.cdf.markForCheck();
  //                   }, 200);

  //       this.noDataFound.title = 'Warning';
  //       this.noDataFound.showImage = true;
  //       this.noDataFound.description = 'No Data Available in this activity';
  //       this.showComponent = 'noDataFound';
  //        this.cdf.markForCheck();
  //       this.toastr.warning(
  //         'No Data Available in this activity',
  //         'Warning!'
  //       ).onHidden.subscribe(()=>
  //       this.toastr.clear());
  //     }
  //   }
  // }

  onSubmitInstruction() {
    // this.router.navigate(["quiz"], { relativeTo: this.routes });
    this.showComponent = "quiz";
    this.cdf.markForCheck();
    this.closePopup();
  }

  onSubmitPassword() {
    this.submitted = true;
    // this.serviceButtonDisabled = true;
    if (this.password === this.passKey) {
      // this.passCheck = false;
      this.closePopup();
      if (this.checkResponse.instructions) {
        // this.serviceButtonDisabled = true;
        this.openPop("quizIntro");
        // this.displayInstructions = true;
      } else {
        // this.serviceButtonDisabled = true;
        this.showComponent = "quiz";
        this.cdf.markForCheck();
        // this.displayInstructions = false;
        // this.router.navigate(["quiz"], { relativeTo: this.routes });
      }
    } else {
      this.passWrong = true;
    }
  }

  showArrow = true;
  performActionOnChildComponentData(event) {
    if (event) {
      setTimeout(() => {
        this.showConatinerSpinner = event.loaderFlag;
        this.cdf.markForCheck();
      }, 200);

      if (event && event.responseData) {
        this.updateModuleActivityList(
          event.responseData,
          this.currentActivityData
        );
      }
      console.log("Event ==>", event);
      //  this.cdf.markForCheck();
      if (event["showComponent"] && event["showComponent"] === "quiz") {
        this.showArrow = false;
      } else {
        this.showArrow = true;
      }
    }
  }

  // Update Activity Module list after activity completion
  updateModuleActivityList(responseData, currentActivity) {
    for (let index = 0; index < this.moduleList.length; index++) {
      for (
        let indexj = 0;
        indexj < this.moduleList[index].list.length;
        indexj++
      ) {
        if (
          this.moduleList[index].list[indexj].activityId ===
          currentActivity.activityId
        ) {
          this.moduleList[index].list[indexj].completedDate =
            responseData.compDate;
          this.moduleList[index].list[indexj]["completedStatus"] =
            responseData.compStatus;
          this.moduleList[index].list[indexj]["activitycompletionid"] =
            responseData.lastid;
          this.moduleList[index].list[indexj]["complitionlangId"] =
            responseData.complitionlangId;
          // this.moduleList[index].list[indexj]["multiLanguageList"] =
          //   currentActivity.multiLanguageList;
          // if(responseData.compStatus === 'Y'){
          //   this.currentActivityData.wCompleted = 1;
          // }
          // this.moduleList[index].list[indexj].wCompleted = responseData['compStatus'] === 'Y' ? 1 : 0;
          this.moduleList[index].list[indexj].wCompleted =
            responseData["compStatus"] === "Y" ? 1 : 0;
          this.moduleList[index].isModuleCompFlag =
            responseData["mcompflag"];
          this.currentActivityData["complitionlangId"] =
            responseData.complitionlangId;
          // this.currentActivityData.completedDate =
          // responseData.compDate;
        }
        if (
          this.moduleList[index].list[indexj].dependentActId ===
            currentActivity.activityId &&
          responseData["compStatus"] === "Y"
        ) {
          this.moduleList[index].list[indexj].dependentActCompStatus = 1;
        }
        this.moduleList[index].list[indexj] = this.finddependetactivity(
          this.moduleList[index].list[indexj]
        );
      }
    }
    this.courseDetailSummary.percComplete = responseData.perc;
    console.log("updateModuleActivityList ==>", this.moduleList);
    console.log("currentActivity ==>", this.currentActivityData);
    // this.checkIfNextExist();
    // this.checkIfPreviousExist();
    // this.goToAutoNext();
  }

  getSelectedActData(activty, cb) {
    let params = {};
    if (
      this.courseDetailSummary["courseFrom"] == 2 ||
      this.courseDetailSummary["courseFrom"] == 3
    ) {
      params = {
        suptId: activty.supertypeId,
        subtId: activty.activityTypeId,
        aId: activty.activityId,
        uId: this.userdetails.userId,
        eId: null,
        enId: this.courseDetailSummary.enrolId
          ? this.courseDetailSummary.enrolId
          : null,
        // tempId: true,
      };
      if (Number(this.userdetails["roleId"]) === 8) {
        params["tempId"] = true;
      } else {
        params["tempId"] = false;
      }
    } else {
      params = {
        suptId: activty.supertypeId,
        subtId: activty.activityTypeId,
        aId: activty.activityId,
        uId: this.userdetails.userId,
        eId: activty.enrolId ? activty.enrolId : null,
        enId: this.courseDetailSummary.enrolId
          ? this.courseDetailSummary.enrolId
          : null,
        tempId: activty.enrolId ? false : true,
      };
      if (this.courseDetailSummary["courseFrom"] == 1) {
        if (Number(this.userdetails["roleId"]) === 8) {
          params["tempId"] = true;
        } else {
          params["tempId"] = false;
        }
      }
    }
    // this.load.presentLoading('Please wait...');
    const url = ENUM.domain + ENUM.url.getModuleActivity;
    this.CSP.getCourseData(url, params)
      .then((result: any) => {
        if (result.type === true) {
          console.log("RESULT-->", result);
          if (
            this.courseDetailSummary["courseFrom"] == 2 ||
            this.courseDetailSummary["courseFrom"] == 3
          ) {
            if (params["tempId"]) {
              result.data["enrolId"] = this.courseDetailSummary.enrolId;
            }
          }
          // this.currentActivityData = result.data;
          cb(result.data, false);
        } else {
          // this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          // this.toastr.clear());
          cb(null, true);
        }
      })
      .catch((result) => {
        // this.spinner.show();
        cb(null, true);
        // this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
        // this.toastr.clear());
      });
  }
  currentActIndex = 0;
  currentModuleIndex = 0;
  // lastAcitivityReachFlag = false;
  goToAutoNext(firstTimeFlag) {
    this.showComponent = "";
    this.setNoDataFoundContainer(null, false);
    this.resetBeginContainerData();
    if (this.autoPlayFlag) {
      this.skipToNextActivityAuto();
    } else {
      if(firstTimeFlag){
        this.skipToNextActivityFirstTime();
      }else {
        this.skipToNextActivity();
      }
    }

    // this.checkIfNextExist();
    // this.checkIfPreviousExist();
  }

  goToPrevious() {
    if (this.currentModuleIndex === 0 && 0 === this.currentActIndex) {
      // this.lastAcitivityReachFlag = true;
      return null;
    }

    this.showComponent = "";
    this.setNoDataFoundContainer(null, false);
    this.resetBeginContainerData();
    if (this.autoPlayFlag) {
      this.skipToPreviousActivityAuto();
    } else {
      this.skipToPreviousActivity();
    }

    // this.checkIfNextExist();
    // this.checkIfPreviousExist();
    // this.goToCourse(this.currentActivityData , this.currentModuleIndex, this.currentActIndex);
  }


  // Check Next Activity Exist
  nextExist = false;
  previousExist = false;
  checkIfNextExist() {
    this.nextExist = false;
    if (
      this.currentModuleIndex === this.moduleList.length - 1 &&
      this.moduleList[this.currentModuleIndex].list.length - 1 ===
        this.currentActIndex
    ) {
      // this.lastAcitivityReachFlag = true;
      return null;
    }
    let currentModuleIndex = this.currentModuleIndex;
    for (let index = 0; index < this.moduleList.length; index++) {
      let breakFlag = false;
      if (this.moduleList[index].isDisable === "N") {
        for (
          let indexj = 0;
          indexj < this.moduleList[index].list.length;
          indexj++
        ) {
          // if(this.moduleList[index].list[indexj].activityId === this.currentActivityData.activityId){
          //   this.currentActIndex = indexj;
          //   this.currentModuleIndex = index;
          // }
          if (this.currentModuleIndex < index) {
            currentModuleIndex = 0;
            if (
              currentModuleIndex <= indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1
              )
            ) {
              // if(this.currentModuleIndex !== index){
              //   moduleIndexChangeFlag = true;
              //  }
              // noModuleActivityFoundDisplay = false;
              //   this.currentActIndex = indexj;
              //  this.currentModuleIndex = index;

              //   // this.currentActIndex = 0;
              //   this.currentActivityData = this.moduleList[index].list[indexj];
              this.nextExist = true;
              breakFlag = true;
              break;
            }
          } else if (currentModuleIndex === index) {
            if (
              currentModuleIndex < indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1
              )
            ) {
              this.nextExist = true;
              breakFlag = true;
              break;
            }
          }
        }
        // this.moduleList[index].list[indexj] = this.finddependetactivity(this.moduleList[index].list[indexj]);
      }
      if (breakFlag || index === this.moduleList.length) {
        break;
      }
    }
  }

  // Check Previous Activity Exist
  checkIfPreviousExist() {
    this.previousExist = false;
    if (this.currentModuleIndex === 0 && 0 === this.currentActIndex) {
      // this.lastAcitivityReachFlag = true;
      return null;
    }
    const moduleLength = this.moduleList.length - 1;
    // let noModuleActivityFoundDisplay = true;
    // let moduleIndexChangeFlag = false;
    let currentActIndex = this.currentActIndex;
    for (let index = moduleLength; index >= 0; index--) {
      let breakFlag = false;
      if (this.moduleList[index].isDisable === "N") {
        for (
          let indexj = this.moduleList[index].list.length - 1;
          indexj >= 0;
          indexj--
        ) {
          if (this.currentModuleIndex > index) {
            // noModuleActivityFoundDisplay = false;
            currentActIndex = this.moduleList[index].list.length - 1;
            if (
              currentActIndex >= indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1
              )
            ) {
              this.previousExist = true;
              breakFlag = true;
              break;
            }
          } else if (this.currentModuleIndex === index) {
            if (
              currentActIndex > indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1
              )
            ) {
              this.previousExist = true;
              breakFlag = true;
              break;
            }
          }

          // this.moduleList[index].list[indexj] = this.finddependetactivity(this.moduleList[index].list[indexj]);
        }
      }
      if (breakFlag || index === 0) {
        break;
      }
    }

    // this.goToCourse(this.currentActivityData , this.currentModuleIndex, this.currentActIndex);
  }

  goBack() {
    window.history.back();
  }

  // Change Activity Language
  changeActivityLanguage(languageData, flag) {
    // console.log('Change Activity Language =>', languageData);
    let multiContentActivity = null;
    if (
      this.currentActivityData &&
      Array.isArray(this.currentActivityData["multiLanguageList"])
    ) {
      for (
        let index = 0;
        index < this.currentActivityData["multiLanguageList"].length;
        index++
      ) {
        const element = this.currentActivityData["multiLanguageList"][index];
        if (
          Number(element["languageId"]) === Number(languageData["languageId"])
        ) {
          element["selected"] = true;
          multiContentActivity = element;
        } else {
          element["selected"] = false;
        }
      }
    }
    this.setActivityData(multiContentActivity, flag);
    // this.multiLanguageService.updateActivityLanguage(this.currentActivityData);
    // console.log('Change Activity Language =>', languageData);
  }

  // Get Multilanguage data at start
  setActivityDataAtStart() {
    // let multiContentActivity = null;
    // for (
    //   let index = 0;
    //   index < this.currentActivityData["multiLanguageList"].length;
    //   index++
    // ) {
    //   const element = this.currentActivityData["multiLanguageList"][index];
    //   if (element["selected"]) {
    //     multiContentActivity = element;
    //     break;
    //   }
    // }
    if (
      this.currentActivityData["multiLanguageList"] &&
      this.currentActivityData["multiLanguageList"].length != 0
    ) {
      this.currentActivityData["multiLanguageList"][0]["selected"] = true;
      this.setActivityData(
        this.currentActivityData["multiLanguageList"][0],
        false
      );
    }
  }

  // Set MultiLanguage activity Data
  setActivityData(multiContentActivity, flag) {
    this.currentActivityData["formatId"] = multiContentActivity["formatId"];

    // this.currentActivityData['defaultlangId'] = multiContentActivity['languageId'];
    this.currentActivityData["languageName"] =
      multiContentActivity["languageName"];
    this.currentActivityData["mimeType"] = multiContentActivity["mimeType"];
    this.currentActivityData["reference"] = multiContentActivity["reference"];
    this.currentActivityData["referenceType"] =
      multiContentActivity["referenceType"];
    this.currentActivityData["fileinfo"] = multiContentActivity["fileinfo"];

    this.currentActivityData["defaultlangId"] =
      multiContentActivity["languageId"];
      //new change
      this.currentActivityData['isDeleted'] = multiContentActivity["isDeleted"]
      this.currentActivityData['isDeletedMsg'] = multiContentActivity["isDeletedMsg"]
      // this.currentActivityData['icon'] = this.bindActivityContainerIcon(multiContentActivity)
      //end change
    if (
      !(
        Number(this.currentActivityData.complitionlangId) ===
        Number(multiContentActivity["languageId"])
      )
    ) {
      this.currentActivityData["contentwatchedtime"] = 0;
    }
    setTimeout(() => {
      if (
        !(
          Number(this.currentActivityData.complitionlangId) ===
          Number(multiContentActivity["languageId"])
        )
      ) {
        this.currentActivityData["contentwatchedtime"] = 0;
      }
      this.showComponent = "course";
    }, 300);
    if (flag) {
      this.multiLanguageService.updateActivityLanguage(
        this.currentActivityData
      );
    }
    console.log("currentActivity",this.currentActivityData)
  }

  showMore() {
    console.log(this.courseDetailSummary.courseDesc.length)
    this.showDescri = !this.showDescri;
  }

  // Show Auto Begin Container Based on auto flag
  showBeginContainer = false;
  currentClickedActivityData = null;
  autoPlayFlag = false;
  showBeginBox(data, moduleIndex, activityIndex) {
    this.currentModuleIndex = moduleIndex;
    this.currentActIndex = activityIndex;
    this.currentClickedActivityData = data;
    this.showConatinerSpinner = true;

    this.showComponent = "";
    console.log("data", data);
    this.resetBeginContainerData();
    // this.setBeginContainerData(data);
    // this.showBeginContainer = true;
    this.getSelectedActData(data, (activity, error) => {
      if (error) {
        this.currentActivityData = null;
        setTimeout(() => {
          this.showConatinerSpinner = false;
          this.cdf.markForCheck();
        }, 300);
        this.setNoDataFoundContainer("noActivityFound", true);
      } else {
        this.setNoDataFoundContainer(null, false);
        this.currentActivityData = activity;
        this.currentActivityData["dependentActName"] = data["dependentActName"];
        this.currentActivityData["dependentModName"] = data["dependentModName"];
        this.currentActivityData["completed"] = data.completed;
        this.currentActivityData["wait"] = data.wait;
        this.currentActivityData["begin"] = data.begin;
        this.currentActivityData["begin"] = data.begin;
        this.currentActivityData["completedDate"] = data.completedDate;
        this.currentActivityData["actIcon"] = data.actIcon;
        this.currentActivityData["activityTypeId"] = data.activityTypeId;
        this.currentActivityData["activity_type"] = data.activity_type;
        this.currentActivityData["byTrainer"] = data.byTrainer;
        this.currentActivityData["byLearner"] = data.byLearner;
        this.currentActivityData["dependentModId"] = data.dependentModId;
        this.currentActivityData["dependentActId"] = data.dependentActId;
        this.currentActivityData["supertypeId"] = data.supertypeId;
        this.currentActivityData["actFormatName"] = data["actFormatName"];
        this.currentActivityData[
          "showBeginContainer"
        ] = this.bindBeginContainer(this.currentActivityData);
        // this.currentActivityData = this.finddependetactivity(activity);
        if (data.wait || data.byTrainer == 1) {
          this.setBeginContainerData(activity, "activity");
          this.showBeginContainer = true;
          setTimeout(() => {
            this.showConatinerSpinner = false;
            this.cdf.markForCheck();
          }, 300);
        } else {
          if (this.currentActivityData["showBeginContainer"]) {
            this.showConatinerSpinner = false;
            this.setBeginContainerData(activity, "activity");
            this.showBeginContainer = true;
            setTimeout(() => {
              this.showConatinerSpinner = false;
              this.cdf.markForCheck();
            }, 300);
          } else {
            this.setActivityDataAtStart();
            this.showBeginContainer = false;
            setTimeout(() => {
              this.showConatinerSpinner = false;
              this.cdf.markForCheck();
            }, 300);
            this.startActivity();
          }
        }
      }
    });
  }

  // Begin Container Data
  beginContainer = {
    waitState: false,
    completedState: true,
    name: "",
    descrition: "",
    message: "",
    goToButton: false,
    beginButton: true,
    icon: "",
  };
  setBeginContainerData(data, type) {
    // this.showComponent = "";
    if (type === "activity") {
      this.beginContainer.name = this.currentActivityData.activityName;
      this.beginContainer.descrition = data.description;
      this.beginContainer.goToButton = data.wait;
      this.beginContainer.completedState = data.completed;
      this.beginContainer.waitState =
        data.wait || (data.begin && data.byTrainer == 1 && !data.completed);
      if (data.begin && data.byTrainer != 1) {
        this.beginContainer.beginButton = true;
      } else if (data.completed) {
        this.beginContainer.beginButton = true;
      } else {
        this.beginContainer.beginButton = false;
      }
      let message = "";
      if (data["wait"]) {
        message =
          'Complete activity " ' +
          data.dependentActName +
          '" from module "' +
          data.dependentModName +
          '" to unlock';
        this.beginContainer.icon = "lock";
      } else if (data.byTrainer == 1) {
        if (data.completed) {
          message = "Attendance is marked by trainer";
        } else {
          message = "Attendance will be marked by trainer";
        }
        this.beginContainer.icon = "lock";
      } else if (data.completed) {
        let dateCompleted;
        const monthShortNames = [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec",
        ];
        const date = new Date(data.completedDate);
        if (data.completedDate) {
          this.beginContainer.icon = "complete";
          dateCompleted =
            date.getDate() +
            "-" +
            monthShortNames[date.getMonth()] +
            "- " +
            date.getFullYear();
        } else {
          dateCompleted = "NA";
        }
        if(data['activityTypeId'] == 6 || data['activityTypeId'] == 9){
          this.beginContainer.beginButton = false;
        }
        message = "Completed on " + dateCompleted;
      } else {
        this.beginContainer.icon = data["actIcon"];
      }
      this.beginContainer.message = message;
    } else if (type === "module") {
      if (data.isDisable == "Y") {
        let date = "";
        if (data.mStartDate) {
          date = data.mStartDate;
        } else {
          date = "NA";
        }
        this.beginContainer.name = data.moduleName;
        this.beginContainer.descrition = "";
        this.beginContainer.goToButton = false;
        this.beginContainer.completedState = false;
        this.beginContainer.waitState = true;
        this.beginContainer.icon = "lock";
        this.beginContainer.message =
          data.moduleName + " will start from " + date;
        this.beginContainer.icon = "lock";
      }else {
        this.beginContainer.waitState = false;
      }

    } else if (type === "quiz") {
      this.beginContainer.name = data.moduleName;
      this.beginContainer.descrition = "";
      this.beginContainer.goToButton = false;
      this.beginContainer.completedState = false;
      this.beginContainer.waitState = true;
      this.beginContainer.icon =  this.currentActivityData["actIcon"];
      this.beginContainer.message = data;
    } else if (type === "feedback") {
      this.beginContainer.name = this.currentActivityData.activityName;
      this.beginContainer.descrition = "";
      this.beginContainer.goToButton = false;
      this.beginContainer.completedState = false;
      this.beginContainer.waitState = true;
      this.beginContainer.icon =  this.currentActivityData["actIcon"];
      this.beginContainer.message = data;
    } else if (type === "noModuleActivityFound") {
      this.beginContainer.name = "No Module/Sessions or Activity to display";
      this.beginContainer.descrition = "";
      this.beginContainer.goToButton = false;
      this.beginContainer.completedState = false;
      this.beginContainer.waitState = true;
      this.beginContainer.icon = "warning";
      this.beginContainer.message = "";
    }
    return this.beginContainer.waitState;
  }

  resetBeginContainerData() {
    this.showBeginContainer = false;
    this.beginContainer = {
      waitState: false,
      completedState: true,
      name: "",
      descrition: "",
      message: "",
      goToButton: false,
      beginButton: true,
      icon: "",
    };
  }

  // Set No Data found container
  setNoDataFoundContainer(message, displayFlag) {
    if (displayFlag) {
      this.noDataFound.showImage = true;
      this.noDataFound.description =
        "Something went wrong please try again later";
      this.showComponent = "noDataFound";
    } else {
      this.noDataFound.showImage = false;
      this.noDataFound.description = "";
      this.showComponent = "";
    }
  }

  // Set Activity Data for container
  startActivity() {
    if (this.disabledGoToCourse) {
      return null;
    }
    this.showComponent = "";
    // this.spinner.show();
    setTimeout(() => {
      this.showConatinerSpinner = true;
      this.cdf.markForCheck();
    }, 200);
    this.showArrow = true;
    this.setNoDataFoundContainer(null, false);
    this.resetBeginContainerData();
    if (this.currentActivityData.activity_type === "Quiz") {
      let param = {
        qId: this.currentActivityData.quizId,
        tId: this.userdetails.tenantId,
        uId: this.employeeId,
        cId: this.currentActivityData.courseId,
        mId: this.currentActivityData.moduleId,
        aId: this.currentActivityData.activityId,
        enId: this.currentActivityData.enrolId,
      };
      const url = ENUM.domain + ENUM.url.checkQuizUser;
      this.CDSP.getCourseDetailsData(url, param)
        .then((result) => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log("CHECk QUIZ FOR USER---->", temp.data[0]);
              this.checkResponse = temp.data[0];
              if (temp.data[0].flag == "true") {
                console.log("PassCheck===>", temp.data[0]);
                if (temp.data[0].qPassword == 0) {
                  if (this.checkResponse.instructions) {
                    this.disabledGoToCourse = false;

                    setTimeout(() => {
                      this.showConatinerSpinner = false;
                      this.cdf.markForCheck();
                    }, 200);

                    this.openPop("quizIntro");
                  } else {
                    this.disabledGoToCourse = false;
                    setTimeout(() => {
                      this.showConatinerSpinner = false;
                      this.cdf.markForCheck();
                    }, 200);
                    this.showComponent = "quiz";
                    this.showArrow = false;
                  }
                } else {
                  this.disabledGoToCourse = false;
                  setTimeout(() => {
                    this.showConatinerSpinner = false;
                    this.cdf.markForCheck();
                  }, 200);
                  this.openPop("quizPass");
                  this.passKey = temp.data[0].passWord;
                }
              } else {
                this.disabledGoToCourse = false;
                setTimeout(() => {
                  this.showConatinerSpinner = false;
                  this.cdf.markForCheck();
                }, 200);

                // Quiz Next Attempt warning
                const message =
                  temp.data[0].msg + " " + temp.data[0].nextAttemptDate;
                // this.setNoDataFoundContainer(obj, false);
                this.toastr
                  .warning(message, "Warning!")
                  .onHidden.subscribe(() => this.toastr.clear());
                this.showBeginContainer = this.setBeginContainerData(
                  message,
                  "quiz"
                );
              }
            } else {
              this.disabledGoToCourse = false;
              setTimeout(() => {
                this.showConatinerSpinner = false;
                this.cdf.markForCheck();
              }, 200);
              this.setNoDataFoundContainer("quizError", true);
              this.toastr
                .warning(
                  "Something went wrong please try again later",
                  "Error!"
                )
                .onHidden.subscribe(() => this.toastr.clear());
            }
          } else {
            // this.spinner.hide();
            this.disabledGoToCourse = false;
            setTimeout(() => {
              this.showConatinerSpinner = false;
              this.cdf.markForCheck();
            }, 200);

            this.setNoDataFoundContainer("quizError", true);
            this.toastr
              .warning("Something went wrong please try again later", "Error!")
              .onHidden.subscribe(() => this.toastr.clear());
          }
        })
        .catch((result) => {
          // this.spinner.hide();
          setTimeout(() => {
            this.showConatinerSpinner = false;
            this.cdf.markForCheck();
          }, 200);

          this.disabledGoToCourse = false;

          this.setNoDataFoundContainer("quizError", true);
          console.log("ServerResponseError :", result);
          this.toastr
            .warning("Something went wrong please try again later", "Error!")
            .onHidden.subscribe(() => this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (this.currentActivityData.activity_type === "Feedback") {
      this.disabledGoToCourse = false;
      let params = {
        fId: this.currentActivityData.feedbackId,
        tId: this.userdetails.tenantId,
        eId: this.employeeId,
        cId: this.currentActivityData.courseId,
        mId: this.currentActivityData.moduleId,
        aId: this.currentActivityData.activityId,
        enId: this.currentActivityData.enrolId,
      };
      const url = ENUM.domain + ENUM.url.checkFeedbackUser;
      this.CDSP.getCourseDetailsData(url, params)
        .then((result) => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log("CHECk FEEDBACK FOR USER---->", temp.data[0]);
              if (temp.data[0].flag == "true") {
                this.disabledGoToCourse = false;
                setTimeout(() => {
                  this.showConatinerSpinner = false;
                  this.showComponent = "feedback";
                  this.cdf.markForCheck();
                }, 200);
              } else {
                this.disabledGoToCourse = false;
                setTimeout(() => {
                  this.showConatinerSpinner = false;
                  this.cdf.markForCheck();
                }, 200);

                this.showBeginContainer = this.setBeginContainerData(
                  temp.data[0].msg,
                  "feedback"
                );
                // this.spinner.hide();
                this.toastr
                  .warning(temp.data[0].msg, "Warning!")
                  .onHidden.subscribe(() => this.toastr.clear());
              }
            } else {
              this.setNoDataFoundContainer("feedbackError", true);
              this.disabledGoToCourse = false;
              setTimeout(() => {
                this.showConatinerSpinner = false;
                this.cdf.markForCheck();
              }, 200);
              this.toastr
                .warning(
                  "Something went wrong please try again later",
                  "Error!"
                )
                .onHidden.subscribe(() => this.toastr.clear());
            }
          } else {
            this.setNoDataFoundContainer("feedbackError", true);
            this.disabledGoToCourse = false;
            setTimeout(() => {
              this.showConatinerSpinner = false;
              this.cdf.markForCheck();
            }, 200);

            this.toastr
              .warning("Something went wrong please try again later", "Error!")
              .onHidden.subscribe(() => this.toastr.clear());
          }
        })
        .catch((result) => {
          this.setNoDataFoundContainer("feedbackError", true);
          this.disabledGoToCourse = false;
          setTimeout(() => {
            this.showConatinerSpinner = false;
            this.cdf.markForCheck();
          }, 200);

          // this.spinner.hide();
          console.log("ServerResponseError :", result);
          this.toastr
            .warning("Something went wrong please try again later", "Error!")
            .onHidden.subscribe(() => this.toastr.clear());
        });
    } else if (this.currentActivityData.activity_type === "Attendance") {
      this.disabledGoToCourse = false;
      setTimeout(() => {
        this.showConatinerSpinner = false;
        this.cdf.markForCheck();
      }, 200);
      this.showComponent = "mark-attendance";
    } else if (this.currentActivityData.activity_type === "Webinar") {
      this.disabledGoToCourse = false;
      setTimeout(() => {
        this.showConatinerSpinner = false;
        this.cdf.markForCheck();
      }, 200);
      this.showComponent = "course";
    } else {
      this.disabledGoToCourse = false;
      this.showComponent = "course";
      if (
        this.currentActivityData &&
        this.currentActivityData["reference"] &&
        this.currentActivityData["reference"] !== ""
      ) {
        // this.router.navigate(['course'], { relativeTo: this.routes });
        setTimeout(() => {
          this.showConatinerSpinner = false;
          this.cdf.markForCheck();
        }, 200);
        this.showComponent = "course";
      } else {
        this.setNoDataFoundContainer("activityDataError", true);
        setTimeout(() => {
          this.showConatinerSpinner = false;
          this.cdf.markForCheck();
        }, 200);
        this.toastr
          .warning("No Data Available in this activity", "Warning!")
          .onHidden.subscribe(() => this.toastr.clear());
      }
    }
  }

  // //icon new change
  // bindActivityContainerIcon(detail) {
  //   // if (detail.activity_type === 'Quiz') {

  //    if (detail && detail.formatId) {
  //     switch (Number(detail.formatId)) {
  //       case 1: // video
  //         return "video";
  //       case 2: // Audio
  //         return "audio";
  //       case 3: // PDF
  //         return "pdf";
  //       case 4: // K-point
  //         return "kpoint";
  //       case 5: // Scrom
  //         return "scrom";
  //       case 6: // Youtube
  //         return "youtube";
  //       case 7: // Image
  //         return "image";
  //       case 8: // External link
  //         return "url";
  //       case 9: // Practice file
  //         return "practice_file";
  //       case 10: // PPT
  //         return "ppt";
  //       case 11: // Excel
  //         return "excel";
  //       case 12: // Word
  //         return "word";
  //       default:
  //         return "";
  //     }
  //   } else {
  //     return "";
  //   }

  // }
  // // end new change

  // Activity Icons Binding
  bindActivityIcon(detail) {
    // if (detail.activity_type === 'Quiz') {
    if (detail.activityTypeId == 5) {
      return "quiz";
      // } else if ( detail.activity_type  === 'Feedback') {
    } else if (detail.activityTypeId == 6) {
      return "feedback";
    }else if (detail.activityTypeId == 9) {
      return "attendance";
    } else if (detail.activityTypeId == 11) {
      return "webinar";
      // } else if ( detail.activity_type === 'Attendance') {
    }  else if (detail.activityTypeId == 1 || detail.activityTypeId == 2) {
      switch (Number(detail.formatId)) {
        case 1: // video
          return "video";
        case 2: // Audio
          return "audio";
        case 3: // PDF
          return "pdf";
        case 4: // K-point
          return "kpoint";
        case 5: // Scrom
          return "scrom";
        case 6: // Youtube
          return "youtube";
        case 7: // Image
          return "image";
        case 8: // External link
          return "url";
        case 9: // Practice file
          return "practice_file";
        case 10: // PPT
          return "ppt";
        case 11: // Excel
          return "excel";
        case 12: // Word
          return "word";
        default:
          return "";
      }
    } else {
      return "";
    }

  }

  // Activity Format Name Binding
  bindActivityFormatName(detail) {
    // if (detail.activity_type === 'Quiz') {
    if (detail.activityTypeId == 5) {
      return "Quiz";
      // } else if ( detail.activity_type  === 'Feedback') {
    } else if (detail.activityTypeId == 6) {
      return "Feedback";
    } else if (detail.activityTypeId == 11) {
      return "Webinar";
      // } else if ( detail.activity_type === 'Attendance') {
    } else if (detail.activityTypeId == 9) {
      return "Attendance";
    } else if (detail.activityTypeId == 1 || detail.activityTypeId == 2) {
      switch (Number(detail.formatId)) {
        case 1: // video
          return "Video";
        case 2: // Audio
          return "Audio";
        case 3: // PDF
          return "Pdf";
        case 4: // K-point
          return "Kpoint";
        case 5: // Scrom
          return "Scrom";
        case 6: // Youtube
          return "Youtube";
        case 7: // Image
          return "Image";
        case 8: // External link
          return "URL";
        case 9: // Practice file
          return "Practice File";
        case 10: // PPT
          return "PPT";
        case 11: // Excel
          return "Excel";
        case 12: // Word
          return "Word";
        default:
          return "";
      }
    } else {
      return "";
    }

  }
  // Show Begin Container
  bindBeginContainer(detail) {
    if (this.autoPlayFlag) {
      return false;
    } else {
      if (detail.activityTypeId == 5) {
        // return 'quiz';
        return true;
      } else if (detail.activityTypeId == 6) {
        // return 'feedback';
        return true;
      } else if (detail.activityTypeId == 11) {
        // return 'webinar';
        return false;
      } else if (detail.activityTypeId == 9) {
        // return 'attendance';
        return false;
      } else if (detail.activityTypeId == 1 || detail.activityTypeId == 2) {
        switch (Number(detail.formatId)) {
          case 1: // video
            return true;
          case 2: // Audio
            return true;
          case 3: // PDF
            return true;
          case 4: // K-point
            return true;
          case 5: // Scrom
            return true;
          case 6: // Youtube
            return true;
          case 7: // Image
            return true;
          case 8: // External link
            return true;
          case 9: // Practice file
            return true;
          case 10: // PPT
            return true;
          case 11: // Excel
            return true;
          case 12: // Word
            return true;
        }
        return true;
        // return 'video';
      } else {
        return false;
      }
    }
  }


  getActivityModuleData(tempCurrentModIndex, tempCurrentActIndex) {
    // if (
    //   String(
    //     this.moduleList[tempCurrentModIndex].isDisable
    //   ).toLowerCase() === "y"
    // ) {
    //   tempCurrentModIndex = tempCurrentModIndex + 1;
    //   tempCurrentActIndex = 0;
    // } else
    // if (
    //   this.moduleList[tempCurrentModIndex].list.length - 1 ===
    //   tempCurrentActIndex
    // ) {
    //   tempCurrentModIndex = tempCurrentModIndex+ 1;
    //   tempCurrentActIndex = 0;
    // } else {
    //   tempCurrentActIndex = tempCurrentActIndex + 1;
    // }

    const nextActivity = this.getNextActivityData(
      tempCurrentModIndex,
      tempCurrentActIndex,
    );
    if (nextActivity) {
      // this.resetDependencyConatiner();
      this.showBeginBox(nextActivity, tempCurrentModIndex, tempCurrentActIndex);
    } else {
      // console.log('No activity available ...');
      this.setBeginContainerData(null, "noModuleActivityFound");
    }
  }

  // Skip To next activity
  skipToNextActivity() {
    this.resetBeginContainerData();
    let tempCurrentModIndex = this.currentModuleIndex;
    let tempCurrentActIndex = this.currentActIndex;
    let nextModuleFound = false;
    if (this.moduleList.length > 0) {
      for (
        let index = tempCurrentModIndex;
        index < this.moduleList.length;
        index++
      ) {
        if (String(this.moduleList[index].isDisable).toLowerCase() === "y") {
          // tempCurrentModIndex = index;
        } else {
          tempCurrentModIndex = index;
          if (tempCurrentModIndex !== this.currentModuleIndex) {
            tempCurrentActIndex = 0;
          } else {
            if (
              this.moduleList[tempCurrentModIndex].list.length - 1 ===
              tempCurrentActIndex
            ) {
              tempCurrentModIndex = tempCurrentModIndex + 1;
              tempCurrentActIndex = 0;
            } else {
              tempCurrentActIndex = tempCurrentActIndex + 1;
            }
          }
          nextModuleFound = true;
          break;
        }
      }
      if (nextModuleFound) {
        if(this.currentModuleIndex != tempCurrentModIndex){
          this.showdiv(tempCurrentModIndex);
        }
        this.getActivityModuleData(tempCurrentModIndex, tempCurrentActIndex);
      } else {
        if (this.currentModuleIndex + 1 < this.moduleList.length - 1) {
          this.currentModuleIndex = this.currentModuleIndex + 1;
          this.currentActIndex = 0;
          this.setBeginContainerData(
            this.moduleList[this.currentModuleIndex],
            "module"
          );
        } else {
          this.setBeginContainerData(null, "noModuleActivityFound");
        }
        this.showBeginContainer = true;
      }
    } else {
      // console.log('No module available ...');

      this.setBeginContainerData(null, "noModuleActivityFound");
      this.showBeginContainer = true;
    }
  }

  skipToNextActivityFirstTime() {
    this.resetBeginContainerData();
    let tempCurrentModIndex = this.currentModuleIndex;
    let tempCurrentActIndex = this.currentActIndex;
    let nextModuleFound = false;
    let moduleIndexChangeFlag = false;
    let noModuleActivityFoundDisplay = true;
    if (this.moduleList.length > 0) {
      for (
        let index = tempCurrentModIndex;
        index < this.moduleList.length;
        index++
      ) {
        let breakFlag = false;
        if (this.moduleList[index].isDisable === "N") {
          nextModuleFound = true;
          for (
            let indexj = 0;
            indexj < this.moduleList[index].list.length;
            indexj++
          ) {
            if (
              this.moduleList[index].list[indexj].activityId ===
              this.currentActivityData.activityId
            ) {
              tempCurrentActIndex = indexj;
              tempCurrentModIndex = index;
            }
            if (tempCurrentModIndex < index) {
              tempCurrentActIndex = 0;
              if (
                tempCurrentActIndex <= indexj &&
                !(
                  this.moduleList[index].list[indexj]["completed"]
                )
              ) {
                if (tempCurrentModIndex !== index) {
                  moduleIndexChangeFlag = true;
                }
                noModuleActivityFoundDisplay = false;
                tempCurrentActIndex = indexj;
                tempCurrentModIndex = index;
                // this.currentActivityData = this.moduleList[index].list[indexj];
                breakFlag = true;
                break;
              }
            } else if (tempCurrentModIndex === index) {
              if (
                tempCurrentActIndex < indexj &&
                !(
                  this.moduleList[index].list[indexj]["completed"]
                )
              ) {
                if (tempCurrentModIndex !== index) {
                  moduleIndexChangeFlag = true;
                }
                noModuleActivityFoundDisplay = false;
                tempCurrentActIndex = indexj;
                tempCurrentModIndex = index;
                // this.currentActivityData = this.moduleList[index].list[indexj];
                breakFlag = true;
                break;
              }
            }
          }
          // this.moduleList[index].list[indexj] = this.finddependetactivity(this.moduleList[index].list[indexj]);
        }
        if (breakFlag || index === this.moduleList.length) {
          break;
        }
      }
      // if (nextModuleFound) {
      //   this.getActivityModuleData(tempCurrentModIndex, tempCurrentActIndex);
      // } else {
      //   if (this.currentModuleIndex + 1 < this.moduleList.length - 1) {
      //     this.currentModuleIndex = this.currentModuleIndex + 1;
      //     this.currentActIndex = 0;
      //     this.setBeginContainerData(
      //       this.moduleList[this.currentModuleIndex],
      //       "module"
      //     );
      //   } else {
      //     this.setBeginContainerData(null, "noModuleActivityFound");
      //   }
      if(noModuleActivityFoundDisplay){
        if(!nextModuleFound){
          if (this.currentModuleIndex + 1 < this.moduleList.length - 1) {
                this.currentModuleIndex = this.currentModuleIndex + 1;
                this.currentActIndex = 0;
                this.setBeginContainerData(
                  this.moduleList[this.currentModuleIndex],
                  "module"
                );
          }else {
            this.setBeginContainerData(null, "noModuleActivityFound");
            this.showBeginContainer = true;
          }
        }else {
          if (moduleIndexChangeFlag) {
            this.showdiv(tempCurrentModIndex);
          }
          this.getActivityModuleData(tempCurrentModIndex, tempCurrentActIndex);
        }
      }else {
        if (moduleIndexChangeFlag) {
          this.showdiv(tempCurrentModIndex);
        }
        this.getActivityModuleData(tempCurrentModIndex, tempCurrentActIndex);
      }
        this.showBeginContainer = true;
    } else {
      // console.log('No module available ...');
      this.showBeginContainer = true;
      this.setBeginContainerData(null, "noModuleActivityFound");
    }
  }
   // Skip To next activity auto
  skipToNextActivityAuto() {
    let noModuleActivityFoundDisplay = true;
    let moduleIndexChangeFlag = false;
    if (
      this.currentModuleIndex === this.moduleList.length - 1 &&
      this.moduleList[this.currentModuleIndex].list.length - 1 ===
        this.currentActIndex
    ) {
      // this.lastAcitivityReachFlag = true;
      return null;
    }
    // this.showComponent = "";
    // this.setNoDataFoundContainer(null, false);
    // this.resetBeginContainerData();
    // let noModuleActivityFoundDisplay = true;
    // let moduleIndexChangeFlag = false;

    for (let index = 0; index < this.moduleList.length; index++) {
      let breakFlag = false;
      if (this.moduleList[index].isDisable === "N") {
        for (
          let indexj = 0;
          indexj < this.moduleList[index].list.length;
          indexj++
        ) {
          if (
            this.moduleList[index].list[indexj].activityId ===
            this.currentActivityData.activityId
          ) {
            this.currentActIndex = indexj;
            this.currentModuleIndex = index;
          }
          if (this.currentModuleIndex < index) {
            this.currentActIndex = 0;
            if (
              this.currentActIndex <= indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1 ||
                this.moduleList[index].list[indexj]["completed"]
              )
            ) {
              if (this.currentModuleIndex !== index) {
                moduleIndexChangeFlag = true;
              }
              noModuleActivityFoundDisplay = false;
              this.currentActIndex = indexj;
              this.currentModuleIndex = index;

              // this.currentActIndex = 0;
              this.currentActivityData = this.moduleList[index].list[indexj];
              breakFlag = true;
              break;
            }
          } else if (this.currentModuleIndex === index) {
            if (
              this.currentActIndex < indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1 ||
                this.moduleList[index].list[indexj]["completed"]
              )
            ) {
              if (this.currentModuleIndex !== index) {
                moduleIndexChangeFlag = true;
              }
              noModuleActivityFoundDisplay = false;
              this.currentActIndex = indexj;
              this.currentModuleIndex = index;

              // this.currentActIndex = 0;
              this.currentActivityData = this.moduleList[index].list[indexj];

              breakFlag = true;
              break;
            }
          }
        }
        // this.moduleList[index].list[indexj] = this.finddependetactivity(this.moduleList[index].list[indexj]);
      }
      if (breakFlag || index === this.moduleList.length) {
        break;
      }
    }
    if (noModuleActivityFoundDisplay) {
      // this.noDataFound.title = "No Module Activity to display";
      // this.noDataFound.description = "";
      // this.noDataFound.showImage = false;
      // this.showComponent = "noDataFound";
      this.setBeginContainerData(null, "noModuleActivityFound");
      this.showBeginContainer = true;
      // this.currentModuleIndex = this.moduleList.length - 1;
      // this.currentActIndex =
      //   this.moduleList[this.currentModuleIndex].list.length - 1;
      // this.currentActivityData = this.moduleList[this.currentModuleIndex].list[
      //   this.currentActIndex
      // ];
      // if (this.moduleList[this.currentModuleIndex].isDisable == "Y") {
      //   let date = "";
      //   if (this.moduleList[this.currentModuleIndex].mStartDate) {
      //     date = this.moduleList[this.currentModuleIndex].mStartDate;
      //   } else {
      //     date = "NA";
      //   }
      //   this.noDataFound.title =
      //     this.moduleList[this.currentModuleIndex].moduleName +
      //     " will start from " +
      //     date;
      //   this.noDataFound.description = "";
      //   this.noDataFound.showImage = false;
      //   this.showComponent = "noDataFound";
      //   return null;
      // }
      // this.showBeginBox(
      //   this.currentActivityData,
      //   this.currentModuleIndex,
      //   this.currentActIndex,
      // );
      // this.showdiv(this.currentModuleIndex);
      // this.moduleList[this.currentModuleIndex].show = true;
    } else {
      if (moduleIndexChangeFlag) {
        this.showdiv(this.currentModuleIndex);
      }
      // this.goToCourse(this.currentActivityData, this.currentModuleIndex, this.currentActIndex);
      this.showBeginBox(
        this.currentActivityData,
        this.currentModuleIndex,
        this.currentActIndex
      );
    }
  }

  // Skip To previous activity
  skipToPreviousActivity() {
    this.resetBeginContainerData();
    let tempCurrentModIndex = this.currentModuleIndex;
    let tempCurrentActIndex = this.currentActIndex;
    let nextModuleFound = false;
    // if (this.moduleList.length > 0) {
    //   if (
    //     String(
    //       this.moduleList[this.currentModuleIndex].isDisable,
    //     ).toLowerCase() === "y"
    //   ) {
    //     tempCurrentModIndex = this.currentModuleIndex - 1;
    //     tempCurrentActIndex =
    //       this.moduleList[this.currentModuleIndex].length - 1;
    //   } else if (0 === this.currentActIndex) {
    //     tempCurrentModIndex = this.currentModuleIndex - 1;
    //     tempCurrentActIndex =
    //       this.moduleList[this.currentModuleIndex].length - 1;
    //   } else {
    //     tempCurrentActIndex = this.currentActIndex - 1;
    //   }
    //   const nextActivity = this.getNextActivityData(
    //     tempCurrentModIndex,
    //     tempCurrentActIndex,
    //   );
    //   if (nextActivity) {
    //     // this.resetDependencyConatiner();
    //     this.showBeginBox(
    //       nextActivity,
    //       tempCurrentModIndex,
    //       tempCurrentActIndex
    //     );
    //   } else {
    //     // console.log('No activity available ...');
    //     this.setBeginContainerData(null, "noModuleActivityFound");
    //   }
    // } else {
    //   // console.log('No module available ...');
    //   this.setBeginContainerData(null, "noModuleActivityFound");
    // }

    if (this.moduleList.length > 0) {
      for (let index = tempCurrentModIndex; index >= 0; index--) {
        if (String(this.moduleList[index].isDisable).toLowerCase() === "y") {
          // tempCurrentModIndex = index;
        } else {
          tempCurrentModIndex = index;
          if (tempCurrentModIndex !== this.currentModuleIndex) {
            tempCurrentActIndex =
              this.moduleList[tempCurrentModIndex].list.length - 1;
          } else {
            if (tempCurrentActIndex === 0) {
              tempCurrentModIndex = tempCurrentModIndex - 1;
              tempCurrentActIndex =
                this.moduleList[tempCurrentModIndex].list.length - 1;
            } else {
              tempCurrentActIndex = tempCurrentActIndex - 1;
            }
          }
          nextModuleFound = true;
          break;
        }
      }
      if (nextModuleFound) {
        if(this.currentModuleIndex != tempCurrentModIndex){
          this.showdiv(tempCurrentModIndex);
        }
        this.getActivityModuleData(tempCurrentModIndex, tempCurrentActIndex);
      } else {
        if (this.currentModuleIndex - 1 > 0) {
          this.currentModuleIndex = this.currentModuleIndex - 1;
          this.currentActIndex =
            this.moduleList[this.currentModuleIndex].list.length - 1;
          this.setBeginContainerData(
            this.moduleList[this.currentModuleIndex],
            "module"
          );
        } else {
          this.setBeginContainerData(null, "noModuleActivityFound");
        }
        this.showBeginContainer = true;
      }
    } else {
      // console.log('No module available ...');
      this.setBeginContainerData(null, "noModuleActivityFound");
      this.showBeginContainer = true;
    }
  }

   // Skip To previous activity auto
  skipToPreviousActivityAuto() {
    const moduleLength = this.moduleList.length - 1;
    let noModuleActivityFoundDisplay = true;
    let moduleIndexChangeFlag = false;
    for (let index = moduleLength; index >= 0; index--) {
      let breakFlag = false;
      if (this.moduleList[index].isDisable === "N") {
        for (
          let indexj = this.moduleList[index].list.length - 1;
          indexj >= 0;
          indexj--
        ) {
          if (this.currentModuleIndex > index) {
            noModuleActivityFoundDisplay = false;
            this.currentActIndex = this.moduleList[index].list.length - 1;
            if (
              this.currentActIndex >= indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1 ||
                this.moduleList[index].list[indexj]["completed"]
              )
            ) {
              if (this.currentModuleIndex !== index) {
                moduleIndexChangeFlag = true;
              }
              this.currentActIndex = indexj;
              this.currentModuleIndex = index;

              // this.currentActIndex = 0;
              this.currentActivityData = this.moduleList[index].list[indexj];
              breakFlag = true;
              break;
            }
          } else if (this.currentModuleIndex === index) {
            if (
              this.currentActIndex > indexj &&
              !(
                this.moduleList[index].list[indexj]["wait"] ||
                this.moduleList[index].list[indexj].byTrainer === 1 ||
                this.moduleList[index].list[indexj]["completed"]
              )
            ) {
              noModuleActivityFoundDisplay = false;
              if (this.currentModuleIndex !== index) {
                moduleIndexChangeFlag = true;
              }
              this.currentActIndex = indexj;
              this.currentModuleIndex = index;

              // this.currentActIndex = 0;
              this.currentActivityData = this.moduleList[index].list[indexj];
              breakFlag = true;
              break;
            }
          }

          // this.moduleList[index].list[indexj] = this.finddependetactivity(this.moduleList[index].list[indexj]);
        }
      }
      if (breakFlag || index === 0) {
        break;
      }
    }
    if (noModuleActivityFoundDisplay) {
      this.setBeginContainerData(null, "noModuleActivityFound");
      this.showBeginContainer = true;
      if (moduleIndexChangeFlag) {
        this.showdiv(this.currentModuleIndex);
      }
      this.moduleList[this.currentModuleIndex].show = true;
    } else {
      if (moduleIndexChangeFlag) {
        this.showdiv(this.currentModuleIndex);
      }
      // this.goToCourse(this.currentActivityData, this.currentModuleIndex, this.currentActIndex);
      this.showBeginBox(
        this.currentActivityData,
        this.currentModuleIndex,
        this.currentActIndex
      );
    }
  }

  getNextActivityData(nextModIndex, nextActIndex) {
    if (this.moduleList[nextModIndex]) {
      if (
        this.moduleList[nextModIndex].list &&
        this.moduleList[nextModIndex].list.length > 0
      ) {
        return this.moduleList[nextModIndex].list[nextActIndex];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  goToDependantActivity() {
    this.resetBeginContainerData();
    this.setNoDataFoundContainer(null, false);
    const depModId = this.currentActivityData.dependentModId;
    const depActId = this.currentActivityData.dependentActId;
    const tempCurrentModIndex = this.checkModIfExists(
      depModId,
      this.moduleList
    );
    const actList = this.moduleList[tempCurrentModIndex].list;
    const tempCurrentActIndex = this.checkActIfExists(depActId, actList);
    if (tempCurrentModIndex !== -1 && tempCurrentActIndex !== -1) {
      const nextActivity = this.getNextActivityData(
        tempCurrentModIndex,
        tempCurrentActIndex
      );
      if (nextActivity) {
        // this.getActivityData(tempCurrentActIndex, nextActivity, tempCurrentModIndex);
        if(tempCurrentModIndex != this.currentModuleIndex){
          if(this.currentModuleIndex != tempCurrentModIndex){
            this.showdiv(tempCurrentModIndex);
          }
        }
        this.showBeginBox(
          nextActivity,
          tempCurrentModIndex,
          tempCurrentActIndex
        );
      } else {
        // console.log('No activity available ...');
        this.setBeginContainerData(null, "noModuleActivityFound");
        this.showBeginContainer = true;
      }
    } else {
      // console.log('No module/activity available ...');
      this.setBeginContainerData(null, "noModuleActivityFound");
      this.showBeginContainer = true;
    }
  }

  checkModIfExists(id, array) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].moduleId == id) {
        return i;
      }
    }
    return -1;
  }

  checkActIfExists(id, array) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].activityId == id) {
        return i;
      }
    }
    return -1;
  }

  perfornContainerAction(action) {
    if (action) {
      switch (action) {
        case "dependant":
          this.goToDependantActivity();
          break;
      }
    }
  }
}
