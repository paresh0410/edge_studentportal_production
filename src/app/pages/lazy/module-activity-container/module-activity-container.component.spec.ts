import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleActivityContainerComponent } from './module-activity-container.component';

describe('ModuleActivityContainerComponent', () => {
  let component: ModuleActivityContainerComponent;
  let fixture: ComponentFixture<ModuleActivityContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleActivityContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleActivityContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
