import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleActivityContainerComponent } from './module-activity-container/module-activity-container.component';

const routes: Routes = [
  {path: '', component: ModuleActivityContainerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule { }
