import { NgModule , CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LazyRoutingModule } from './lazy-routing.module';
import { MatVideoModule } from 'mat-video';
import { StarRatingModule } from 'angular-star-rating';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { SortablejsModule } from 'angular-sortablejs';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { CommonComponentSharingModule } from '../common-component-sharing/common-component-sharing.module';
import { ImageViewerModuleCommonModule } from '../common-component-sharing/image-viewer-module-common.module';

import { ActivityContainerComponent } from './activity-container/activity-container.component';
import { ModuleActivityContainerComponent } from './module-activity-container/module-activity-container.component';
// import { RatingModule } from 'ng-starrating';
import { AudioPlayerComponent } from './component/audio-player/audioplayer.component';
import { KPointComponent } from './component/k-point/k-point.component';
import { ScormPlayerComponent } from './component/scorm-player/scorm-player.component';
// import { NoDataFoundComponent } from './component/no-data-found/no-data-found.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { QuizComponent } from './quiz-container/quiz/quiz.component';
import { QuizReviewComponent } from './/quiz-container/quiz-review/quiz-review.component';
import { QuizContainerComponent } from './quiz-container/quiz-container.component';


import { TimingPipe, RemoveHtmlPipe, SafePipe } from './pipes';

import { PipeModule } from '../../pipes/pipes.module' ;
import { MarkAttendanceComponent } from './component/mark-attendance/mark-attendance.component';
import { VideoPlayerComponent } from './component/video-player/videoplayer.component';
import { DocumentViewerComponent } from './component/document-viewer/documentviewer.component';
// import { WebinarMultiComponent } from '../webinar-multiple/webinar-multi.component';


import { DocumentViewerService } from './component/document-viewer/documentviewer.service';
import { VideoPlayerService } from './component/./video-player/videoplayer.service';
import { AudioPlayerService } from './component/./audio-player/audioplayer.service';
import { KPointService } from './component/k-point/k-point.service';
import { ScormService } from '../../service/scormservices.service';
import { WebinarServiceProvider } from '../../service/webinar.service';
import { TruncateModule } from 'ng2-truncate';


const PIPES = [
    TimingPipe,
    RemoveHtmlPipe,
    // MandetoryquestionpipePipe,
    SafePipe,
];

const COMPONENTS = [
    ModuleActivityContainerComponent,
    ActivityContainerComponent,
    AudioPlayerComponent,
    KPointComponent,
    ScormPlayerComponent,
    VideoPlayerComponent,
    DocumentViewerComponent,
    // NoDataFoundComponent,
    MarkAttendanceComponent,
    FeedbackComponent,
    // WebinarMultiComponent,
    QuizContainerComponent,
    QuizComponent,
    QuizReviewComponent,
];

@NgModule({
  declarations: [
    ...COMPONENTS,
    ...PIPES,
  ],
  imports: [
    CommonModule,
    PipeModule.forRoot(),
    LazyRoutingModule,
    NgCircleProgressModule.forRoot({}),
    // RatingModule,
    FormsModule,
    NgxYoutubePlayerModule,
    MatVideoModule,
    StarRatingModule,
    SortablejsModule,
    CommonComponentSharingModule.forRoot(),
    ImageViewerModuleCommonModule.forRoot(),
    TruncateModule,
    MatProgressSpinnerModule,
  ],
  providers:[
    VideoPlayerService,
    AudioPlayerService,
    KPointService,
    ScormService,
    DocumentViewerService,
    WebinarServiceProvider,
 ],
 exports: [
   ...COMPONENTS,
 ],
 schemas: [
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
]
})


export class LazyModule { }
