import { Component, OnInit, EventEmitter, ViewEncapsulation } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { Observable, of } from 'rxjs';
import { PulseServiceProvider } from '../../service/pulse-service';
import { ENUM } from '../../service/enum' ;
import { ToastrService } from 'ngx-toastr';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { NgxSpinnerService } from 'ngx-spinner'

export interface PeriodicElement {
	name: string;
	position: number;
	weight: number;
	symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
	{ position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
	{ position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
	{ position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
	{ position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
	{ position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
	{ position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
	{ position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
	{ position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
	{ position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
	{ position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

@Component({
	selector: 'ngx-pulse',
	templateUrl: './pulse.component.html',
	styleUrls: ['./pulse.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PulseComponent implements OnInit {

	public content:any ={
		// pName: '',
		// tName: '',
		// fileUpload: '',
	}
	modal: boolean;

	displayedColumns: string[] = ['select', 'position', 'name', 'weight', 'symbol'];
	dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
	selection = new SelectionModel<PeriodicElement>(true, []);

	options: UploaderOptions;
	formData: FormData;
	files: UploadFile[];
	uploadInput: EventEmitter<UploadInput>;
	humanizeBytes: Function;
	dragOver: boolean;

	//trending hashtag Array//
	name: String;
	noOfPost: number;

	hashtagArray = [
		// { name: "Design", noOfPost: 144 },
		// { name: "Sales", noOfPost: 144 },
		// { name: "Positivity", noOfPost: 144 },
		// { name: "JohnDoe", noOfPost: 144 },
		// { name: "Design", noOfPost: 144 },
		// { name: "Sales", noOfPost: 144 },
		// { name: "Positivity", noOfPost: 144 },
		// { name: "JohnDoe", noOfPost: 144 },
		// { name: "Design", noOfPost: 144 },
		// { name: "Sales", noOfPost: 144 },
		// { name: "Positivity", noOfPost: 144 },
		// { name: "JohnDoe", noOfPost: 144 },
		// { name: "Design", noOfPost: 144 },
	];
	//  trending hashtag array ends  //

	//  posts box array  //
	title: string;
	timePassed: string;
	attachment: string;
	tags: string;
	like: boolean;
	dislike: boolean;
	isBookmark: boolean;
	share: boolean;

	MyPosts = [
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/cover2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera1.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera3.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/cover2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera1.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	// 	{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera3.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false }
	];

	//post box array end//

	tagName: string;
	onClick: boolean;
	chipset: boolean;

	similarPosts = [
		// { tagName: "Design", noOfPost: "250", title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/cover2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
		// { tagName: "Design", noOfPost: "250", title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera3.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
		// { tagName: "Design", noOfPost: "250", title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, isBookmark: false },
	];

	//autocomplete user array//

	autocomplete = ["John Doe", "John Doppler", "John Dollar"];


	showchipset() {
		this.chipset = true;
	}

	hidechipset() {
		this.chipset = false;
	}

	constructor(public PSP: PulseServiceProvider, private toastr: ToastrService, private spinner: NgxSpinnerService) {
		// maxSize: 5000,
		this.options = { concurrency: 1, maxUploads: 1 };
    	this.files = []; // local uploading files array
    	this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
		this.humanizeBytes = humanizeBytes;
		this.getData();
		var parameter = {
			tId: 1
		}
		var url = ENUM.domain + ENUM.url.getTrendingTags;
		this.PSP.getTrendingTags(url,parameter)
  		.then(result=>{
			var temp:any = result
			if(temp.data != undefined || temp.data != null){
				this.hashtagArray = temp.data;
			}else{}
		}).catch(result =>{
			// this.spinner.hide();
		  	console.log("ServerResponseError :",  result);
		})

	}

	getData(){
		this.spinner.show();
		var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		var params = {
			uId: userDetails.id,
			tId: userDetails.tenantId
		}
		var url = ENUM.domain + ENUM.url.getPulse;
		this.PSP.getPulseData(url,params)
  		.then(result=>{
			var temp:any = result
			if(temp.data != undefined || temp.data != null){
				this.MyPosts = temp.data;
				for(var i = 0; i < this.MyPosts.length; i++){
					var TagArray = this.MyPosts[i].tags.split(',');
					// console.log('POSTS--->',TagArray);
					this.MyPosts[i].tags = TagArray.join('  |  ');
					// if(this.MyPosts[i].attachment == undefined || this.MyPosts[i].attachment == null || this.MyPosts[i].attachment == ''){
						this.MyPosts[i].attachment = './assets/images/camera2.jpg';
                    // }
                    if(this.MyPosts[i].isLike == 1){
                        this.MyPosts[i].isLike = true;
                    }else{
                        this.MyPosts[i].isLike = false;
                    }
                    if(this.MyPosts[i].isDislike == 1){
                        this.MyPosts[i].isDislike = true;
                    }else{
                        this.MyPosts[i].isDislike = false;
					}
					if(this.MyPosts[i].isBookmark == 1){
                        this.MyPosts[i].isBookmark = true;
                    }else{
                        this.MyPosts[i].isBookmark = false;
					}
                }
				console.log('MyPosts',this.MyPosts);
				this.spinner.hide();
			}else{
				this.spinner.hide();
			}
		}).catch(result =>{
			this.spinner.hide();
		  	console.log("ServerResponseError :",  result);
		})
	}

	/** Whether the number of selected elements matches the total number of rows. */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/** Selects all rows if they are not all selected; otherwise clear selection. */
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => this.selection.select(row));
	}

	ngOnInit() {
		this.modal = false;
		this.onClick = false;
		this.chipset = false;

	}

	transform(value: string): Observable<object> {
		const item = { display: `#${value}`, value: `#${value}` };
		return of(item);
	}


	similarItems(data) {
        var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		var param = {
			tags: data.rawname,
			tId: userDetails.tenantId,
			uId: userDetails.id
		}
		var url = ENUM.domain + ENUM.url.getTrendingTagsData;
		this.PSP.getTrendingTagsData(url,param)
  		.then(result=>{
			var temp:any = result
			if(temp.data != undefined || temp.data != null){
				if(temp.data.length > 0){
					this.similarPosts = temp.data;
					for(var i = 0; i < this.similarPosts.length; i++){
						// if(this.similarPosts[i].attachment == undefined || this.similarPosts[i].attachment == null || this.similarPosts[i].attachment == ''){
							this.similarPosts[i].attachment = './assets/images/camera2.jpg';
						// }
						this.similarPosts[i].rawname = data.rawname;
						this.similarPosts[i].trending = data.trending;
					}
					this.onClick = true;
				}else{
					this.toastr.warning('No Posts Available','Warning!').onHidden.subscribe(()=>
					this.toastr.clear());
				}
			}
		}).catch(result =>{
			// this.spinner.hide();
		  	console.log("ServerResponseError :",  result);
		})
	}

	backToPrevious() {
		this.onClick = false;
	}

	togglelike(item) {
        console.log('ITEM--->',item);
		if (!item.isLike) {
            item.isLike = true;
            item.noLikes = item.noLikes + 1;
            var userDetails = JSON.parse(localStorage.getItem('userDetails'));
            var params = {
                "pId" : item.tagId,
                "fType" : 1,
                "uId" : userDetails.id,
                "eId" : userDetails.id,
                "tId" : userDetails.tenantId,
            }
            var url = ENUM.domain + ENUM.url.addFeedback;
		    this.PSP.addFeedback(url,params)
  		    .then(result=>{
			    console.log('DATA--->',result);
		    }).catch(result =>{
			    console.log("ServerResponseError :",  result);
	  	    })
			if (item.isLike) {
                if(item.isDislike){
                    if(item.noDislikes > 0){
                        item.noDislikes = item.noDislikes - 1;
					}
					this.toggledislike(item);
					item.isDislike = false;
                }
			}
		} else {
            item.isLike = false;
            if(item.noLikes > 0){
                item.noLikes = item.noLikes - 1;
            }
            var userDetails = JSON.parse(localStorage.getItem('userDetails'));
            var params = {
                "pId" : item.tagId,
                "fType" : 1,
                "uId" : userDetails.id,
                "eId" : userDetails.id,
                "tId" : userDetails.tenantId,
            }
            var url = ENUM.domain + ENUM.url.removeFeedback;
		    this.PSP.removeFeedback(url,params)
  		    .then(result=>{
			    console.log('DATA--->',result);
		    }).catch(result =>{
			    console.log("ServerResponseError :",  result);
	  	    })
		}
	}

	toggledislike(item) {
        console.log('ITEM--->',item);
		if (!item.isDislike) {
            item.isDislike = true;
            item.noDislikes = item.noDislikes + 1;
            var userDetails = JSON.parse(localStorage.getItem('userDetails'));
            var params = {
                "pId" : item.tagId,
                "fType" : 2,
                "uId" : userDetails.id,
                "eId" : userDetails.id,
                "tId" : userDetails.tenantId,
            }
            var url = ENUM.domain + ENUM.url.addFeedback;
		    this.PSP.addFeedback(url,params)
  		    .then(result=>{
			    console.log('DATA--->',result);
		    }).catch(result =>{
			    console.log("ServerResponseError :",  result);
	  	    })
			if (item.isDislike) {
                if(item.isLike){
                    if(item.noLikes > 0){
                        item.noLikes = item.noLikes - 1;
					}
					this.togglelike(item);
					item.isLike = false;
                }
			}
		} else {
            item.isDislike = false;
            if(item.noDislikes > 0){
                item.noDislikes = item.noDislikes - 1;
            }
            var userDetails = JSON.parse(localStorage.getItem('userDetails'));
            var params = {
                "pId" : item.tagId,
                "fType" : 2,
                "uId" : userDetails.id,
                "eId" : userDetails.id,
                "tId" : userDetails.tenantId,
            }
            var url = ENUM.domain + ENUM.url.removeFeedback;
		    this.PSP.removeFeedback(url,params)
  		    .then(result=>{
			    console.log('DATA--->',result);
		    }).catch(result =>{
			    console.log("ServerResponseError :",  result);
	  	    })
		}
	}


	togglesave(item) {
        console.log('ITEM--->',item);
		if (!item.isBookmark) {
            item.isBookmark = true;
            var userDetails = JSON.parse(localStorage.getItem('userDetails'));
            var params = {
                "pId" : item.tagId,
                "fType" : 3,
                "uId" : userDetails.id,
                "eId" : userDetails.id,
                "tId" : userDetails.tenantId,
            }
            var url = ENUM.domain + ENUM.url.addFeedback;
		    this.PSP.addFeedback(url,params)
  		    .then(result=>{
			    console.log('DATA--->',result);
		    }).catch(result =>{
			    console.log("ServerResponseError :",  result);
	  	    })
		} else {
            item.isBookmark = false;
            var userDetails = JSON.parse(localStorage.getItem('userDetails'));
            var params = {
                "pId" : item.tagId,
                "fType" : 3,
                "uId" : userDetails.id,
                "eId" : userDetails.id,
                "tId" : userDetails.tenantId,
            }
            var url = ENUM.domain + ENUM.url.removeFeedback;
		    this.PSP.removeFeedback(url,params)
  		    .then(result=>{
				console.log('DATA--->',result);
				this.getData();
		    }).catch(result =>{
			    console.log("ServerResponseError :",  result);
	  	    })
		}
	}

	close() {
		this.modal = false;
	}

	openPopupWindow() {
		this.modal = true;
	}

	submit(sData){
		var tagString = '';
		var tagArray = [];
		var pDa = this.formatDate();
		console.log('DATA',pDa);
		if(sData.tags != null || sData.tags != undefined){
			for(var i=0;i<sData.tags.length;i++){
				if(sData.tags[i].value.charAt(0) == '#'){
					sData.tags[i].value = sData.tags[i].value.slice(1);
					tagArray.push(sData.tags[i].value);
				}else{
					tagArray.push(sData.tags[i].value);
				}
			}
		}
		tagString = tagArray.join('|');
		console.log('TS--->',tagString);
		var contentType = "";
		var postData = "";
		var doingFileUpload = false;
		var file = this.files;
		const fd: any = new FormData();
		var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		var params = {
			eId: userDetails.id,
			uId: userDetails.id,
			pName: sData.pNa,
			postPicRef: '',
			pDate: pDa,
			reference: '',
			contentRefId: '',
			tId: userDetails.tenantId,
			tags: tagString,
		}
		var url = ENUM.domain + ENUM.url.pushPulse;
		// for(let i=0; i<file.length; i++){
		// 	fd.append('file', file[i]);
		// }
		fd.append('content', JSON.stringify(params));
		if(this.files.length > 0){
			fd.append('file', file);
		}

		this.PSP.getPulseData(url,fd)
  		.then(result=>{
			var temp: any = result;
			console.log('SuccessResult:-',result);
			if(temp.type == false){
				this.toastr.warning(temp.data,'Error!').onHidden.subscribe(()=>
				this.toastr.clear());
			}else{
				this.toastr.warning(temp.data,'Success!').onHidden.subscribe(()=>
				this.toastr.clear());
				this.getData();
				this.content = {}
			}
		}).catch(result =>{
			// this.spinner.hide();
		  	console.log("ServerResponseError :",  result);
	  	});
	}

	formatDate(){
		var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    	if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		var hours = '' + d.getHours()
    	var minutes = '' + d.getMinutes()
    	var seconds = '' + d.getSeconds();

    	if (hours.length < 2)
        	hours = "0" + hours;

    	if (minutes.length < 2)
        	minutes = "0" + minutes;

    	if (seconds.length < 2)
			seconds = "0" + seconds;

		var dateString = [year, month, day].join('-');
		var timeString = [hours, minutes, seconds].join('-');
		var saveString = dateString + ' ' + timeString

    	return saveString;
	}

	onUploadOutput(output: UploadOutput): void {
		switch (output.type) {
		  	case 'allAddedToQueue':
			  // uncomment this if you want to auto upload files when added
			  // const event: UploadInput = {
			  //   type: 'uploadAll',
			  //   url: '/upload',
			  //   method: 'POST',
			  //   data: { foo: 'bar' }
			  // };
			  // this.uploadInput.emit(event);
			break;

			case 'addedToQueue':
				if (typeof output.file !== 'undefined') {
					  	this.files.push(output.file);
				}
			break;
		  	case 'uploading':
				if (typeof output.file !== 'undefined') {
			  		// update current data in files array for uploading file
			  		const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
			  		this.files[index] = output.file;
				}
			break;
		  	case 'removed':
				// remove file from array when removed
				this.files = this.files.filter((file: UploadFile) => file !== output.file);
			break;
		  	case 'dragOver':
				this.dragOver = true;
			break;
		  	case 'dragOut':
		  	case 'drop':
				this.dragOver = false;
			break;
		  	case 'done':
				// The file is downloaded
			break;
		}
	}

	cancelUpload(id: string): void {
		this.uploadInput.emit({ type: 'cancel', id: id });
	}

	removeFile(id: string): void {
		this.uploadInput.emit({ type: 'remove', id: id });
	}

	removeAllFiles(): void {
		this.uploadInput.emit({ type: 'removeAll' });
	}

	removeLimitExceed(output){
		console.log('OUTPUT--->',output);
		if(output.target.files[0].size > 5000000){
			console.log('File is too large',output.target.files[0].size);
			let value:any;
			value = document.getElementById('inputPost');
			value.value = '';
			this.toastr.warning('Limit Exceeded','Error!').onHidden.subscribe(()=>
			this.toastr.clear());
		}
        // this.files = output.target.files[0];
        // console.log('FILES--->',this.files);
	}

}
