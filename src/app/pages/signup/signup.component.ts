import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthServiceProvider } from '../../service/auth-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'ngx-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  formData = {
    firstName: '',
    lastName: '',
    phone_no: '',
    email: '',
    password: '',
    confirm_pass: '',
  };
  passWrong = false;
  signUpForm: FormGroup;
  submitted = false;
  subDomain = '';
  hide = true;
  hide1 = true;
   domainBasedtenantId: any;
  // signUpForm = new FormControl('', [
  //   Validators.required,
  //   Validators.email,
  // ]);

  matcher = new MyErrorStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private ASP: AuthServiceProvider,
    public spinner: NgxSpinnerService,
    private router: Router,
    private routes: ActivatedRoute,
    public toastr: ToastrService,
  ) {
    const id = document.getElementById('nb-global-spinner');
    id.style.display = 'none';
  }

  ngOnInit() {
    this.passWrong = false;
    // this.getSubdomainURL();
    //form group and validations declared

    this.signUpForm = this.formBuilder.group({
      firstName: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z]+'),
        ],
      ],
      phone_no: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          // Validators.pattern('[6-9]d{9}$'),
        ],
      ],
      lastName: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z]+'),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$'),
        ],
      ],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_pass: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get f() {
    return this.signUpForm.controls;
  } //form fields getter for easy access

  onSubmit() {
    this.submitted = true;
    this.domainBasedtenantId = this.ASP.getDomainBasedTenant();
    // stop here if form is invalid
    if (this.signUpForm.invalid) {
      return;
    }
    // else{
    //   this.router.navigate(['../otp'],{relativeTo:this.routes});
    // }
    console.log('Submit form data ==>', this.formData);
    console.log('Subdomain ==>', this.subDomain);
    this.spinner.show();
    const param = {
      firstName: this.formData.firstName,
      lastName: this.formData.lastName,
      userName: this.formData.email,
      phone: this.formData.phone_no,
      emailId: this.formData.email,
      password: this.formData.password,
      subDomainName: this.subDomain,
      loginURL: window.location.origin,
      tenantId: this.domainBasedtenantId.tenantId,
      platform: 3,
    };
    this.ASP.signUp(param)
    .then(result => {
      this.spinner.hide();
      if(result['type']){
        if(result['alreadyExist']){
          this.toastr.warning(
            result['message'],
            'Warning',
          ).onHidden.subscribe(()=>
          this.toastr.clear());
        }else {
          this.toastr.info(
            result['message'],
            'Information',
          ).onHidden.subscribe(() =>
          this.toastr.clear());
        }
       this.goToLogin();
      }else {
        this.toastr.warning(
          'Unable to Signup',
          'Error'
        ).onHidden.subscribe(()=>
        this.toastr.clear());
      }

      console.log('Response ==>', result);
    })
    .catch(result => {
      this.spinner.hide();
      this.toastr.warning(
        'Unable to Signup',
        'Error'
      ).onHidden.subscribe(()=>
      this.toastr.clear());

      console.log('ServerResponseError :', result);
    });
  }

  checkPassword() {
    if (this.formData.password === this.formData.confirm_pass) {
      return true;
    } else {
      return false;
    }
  }
  onInput(value) {
    if (this.checkPassword()) {
      //if both passwords are same set errors to null
      this.f.confirm_pass.setErrors(null);
      this.passWrong = false;
    } else {
      //set error to confirm_pass manually
      this.f.confirm_pass.setErrors([{ passwordMismatch: true }]);
      this.passWrong = true;
    }
  }

  goToLogin() {
    this.router.navigate(['../login-new'], { relativeTo: this.routes });
  }

  // getSubdomainURL() {
  //   const hostName = window.location.hostname;
  //   if (hostName) {
  //     const splited = hostName.split('.');
  //     if (splited && splited.length !== 0) {
  //       this.subDomain = splited[0];
  //     }
  //   }
  //   // this.subDomain = hostName ? const splited = hostName.split('.') ? [0] : '';
  //   console.log('Subdomain ==>', this.subDomain);

  //   this.subDomain = 'testdomain';
  // }
}
