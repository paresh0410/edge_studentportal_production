import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from '../../../service/enum';
import { ToolsService } from '../../../service/tools.service';

@Component({
  selector: 'ngx-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {

  tools: any = [];
  section: any = [];
  userDetails: any;
  employeeId: any;
  parentSectionId: any = null;
  oldParentSecId: any;
  backPath: any = true;

  constructor(private router: Router,
              private routes: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private tools_service: ToolsService) {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
   }

  ngOnInit() {
    if (this.tools_service.fromContent === true) {
      this.parentSectionId = this.tools_service.parentSecId;
      console.log('from content this.parentSectionId', this.parentSectionId);
    }
    
    
    this.getSection(this.parentSectionId);
  }

noSectionFound: boolean = false;
getSection(id) {
  this.spinner.show();
  const url = ENUM.domain + ENUM.url.get_all_section;

  const param = {
    'parSecId': id,
    'empId': this.employeeId,
    'tId': this.userDetails.tenantId,
  };
  console.log('get section param:', param);
  this.tools_service.getSectionData(url, param)
    .then((result: any) => {
      this.spinner.hide();
      console.log('RESULT Sec===>', result);
    
      result.data[0].length === 0 ? this.parentSectionId = null :
      this.oldParentSecId = result.data[0][0].parentSectionId;
      console.log('oldParentSecId', this.oldParentSecId);

      result.data[1].length === 0 ? this.noSectionFound = true :
      this.noSectionFound = false,
      this.section = result.data[1];

     // this.section = result.data;
      console.log('Section data', this.section);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT', result);
    })
}

  gotoContent(data) {
    console.log('data', data);
    if (data.sectionContent == 1) {
      this.tools_service.parentSecId = data.id;
      this.oldParentSecId = data.parentSectionId;
      this.parentSectionId = data.id;

      this.getSection(this.parentSectionId);
    } else {
     
      this.tools_service.parentSecId = data.id;
      this.router.navigate(['../section-content'], { relativeTo: this.routes });
    }
    
  }

  back() {
    if (this.parentSectionId) {
      this.getSection(this.oldParentSecId);
    } else {
      this.router.navigate(['../pages/dashboard']);
    }
   
  }
}
