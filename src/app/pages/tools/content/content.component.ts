  import { Component, OnInit } from '@angular/core';
  import { NgxSpinnerService } from 'ngx-spinner';
  import { ENUM } from '../../../service/enum';
  import { ToolsService } from '../../../service/tools.service';
  import { Router, ActivatedRoute } from '@angular/router';



  @Component({
    selector: 'ngx-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss']
  })
  export class ContentComponent implements OnInit {

    content: any = [
      {
        id: 1,
        imgPath: 'assets/images/open-book-leaf.jpg',
        contentName: 'Content 1',
        contentDesc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt eum incidunt alias error, laboriosam ad nisi, nobis odit illum quas reiciendis consectetur amet ea. Explicabo quibusdam quasi error eos.',
      },
      {
        id: 2,
        imgPath: 'assets/images/open-book-leaf.jpg',
        contentName: 'Content 2',
        contentDesc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt eum incidunt alias error, laboriosam ad nisi, nobis odit illum quas reiciendis consectetur amet ea. Explicabo quibusdam quasi error eos.',
      },
      {
        id: 3,
        imgPath: 'assets/images/open-book-leaf.jpg',
        contentName: 'Content 3',
        contentDesc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt eum incidunt alias error, laboriosam ad nisi, nobis odit illum quas reiciendis consectetur amet ea. Explicabo quibusdam quasi error eos.',
      },
      {
        id: 4,
        imgPath: 'assets/images/open-book-leaf.jpg',
        contentName: 'Content 4',
        contentDesc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt eum incidunt alias error, laboriosam ad nisi, nobis odit illum quas reiciendis consectetur amet ea. Explicabo quibusdam quasi error eos.',
      },
      {
        id: 5,
        imgPath: 'assets/images/open-book-leaf.jpg',
        contentName: 'Content 5',
        contentDesc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt eum incidunt alias error, laboriosam ad nisi, nobis odit illum quas reiciendis consectetur amet ea. Explicabo quibusdam quasi error eos.',
      },
      {
        id: 6,
        imgPath: 'assets/images/open-book-leaf.jpg',
        contentName: 'Content 6',
        contentDesc: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti deserunt eum incidunt alias error, laboriosam ad nisi, nobis odit illum quas reiciendis consectetur amet ea. Explicabo quibusdam quasi error eos.',
      },
    ]
    userDetails: any;
    employeeId: any;
    noDataFound: any = false;
    parSecId: any = null;
    parentSectionId;
    constructor( private spinner: NgxSpinnerService, private tools_service: ToolsService,
      private router: Router,
      private routes: ActivatedRoute,
      ) {
        this.parentSectionId = this.tools_service.parentSecId;
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
    }

    ngOnInit() {
      this.parentSectionId = this.tools_service.parentSecId;
      this.getSectionContent();

    }

    getSectionContent() {
      this.spinner.show();
      let url = ENUM.domain + ENUM.url.get_all_section_content;

      const param = {
        'secId': this.parentSectionId,
        'empId': this.employeeId,
        'tId': this.userDetails.tenantId,
      };
      console.log('get content param', param);
      this.tools_service.getSectionContent(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('Content Sec===>', result);

        result.data[0].length === 0 ? this.noDataFound = true :
        this.parSecId = result['data'][0][0].parentSectionId;
        console.log('this.parSecId', this.parSecId);

        result.data[1].length === 0 ? this.noDataFound = true :
        this.noDataFound = false, this.content = result['data'][1];

        console.log(' this.allContent', this.content);
      }).catch(result => {
        this.spinner.hide();
        console.log('RESULT', result);
      })

    }

    content_details(data) {
      this.tools_service.contentData = data;
      this.router.navigate(['content-details'], { relativeTo: this.routes });
    }


    back(){
     // window.history.back();
     this.tools_service.fromContent = true;
     this.tools_service.parentSecId = this.parSecId;
    this.router.navigate(['../pages/section']);

    }

    // bindBackgroundImage(detail) {
    //   // detail.img = 'assets/images/open-book-leaf-2.jpg';
    //   // return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};

    //  if (detail.referenceType === 'video' || detail.mimeType === 'video/mp4') {
    //     detail.img = 'assets/images/activity_image/video.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else if (detail.contentType == 2) {
    //     detail.img = 'assets/images/activity_image/audio.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else if (detail.contentType == 3) {
    //     detail.img = 'assets/images/activity_image/pdf.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else if (detail.contentType == 6) {
    //     detail.img = 'assets/images/activity_image/video.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else if (detail.contentType == 7) {
    //     detail.img = 'assets/images/activity_image/image.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else if (detail.referenceType === 'application' && detail.mimeType === 'application/x-msdownload') {
    //     detail.img = 'assets/images/activity_image/url.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else if (detail.formatId == 9) {
    //     detail.img = 'assets/images/activity_image/practice_file.jpg';
    //     return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    //   } else {
    //     detail.img = 'assets/images/open-book-leaf-2.jpg';
    //     return { 'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')' };
    //   }
    // }

    bindBackgroundImage(detail) {
      if (detail.contentType === 1) {
      detail.img = 'assets/images/activity_image/video.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.contentType === 2){
      detail.img = 'assets/images/activity_image/audio.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.contentType === 3){
      detail.img = 'assets/images/activity_image/pdf.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }  else if ( detail.contentType === 5){
      detail.img = 'assets/images/activity_image/scrom.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.contentType === 6){
      detail.img = 'assets/images/activity_image/video.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.contentType === 7){
      detail.img = 'assets/images/activity_image/image.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.contentType === 8){
      detail.img = 'assets/images/activity_image/url.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.contentType === 9){
        detail.img = 'assets/images/activity_image/practice_file.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
        }
        else if (detail.contentType == 10) {
          detail.img = 'assets/images/activity_image/ppt.jpg';
          return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
        }
        else if (detail.contentType == 11) {
          detail.img = 'assets/images/activity_image/excel.jpg';
          return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
        }
        else if (detail.contentType == 12) {
          detail.img = 'assets/images/activity_image/word.jpg';
          return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
        } else {
      detail.img  = 'assets/images/activity1.jpg';
      return { 'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' +  detail.img + ')' };
      }
      }
  }
