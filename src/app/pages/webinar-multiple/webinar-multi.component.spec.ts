import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarMultiComponent } from './webinar-multi.component';

describe('WebinarMultiComponent', () => {
  let component: WebinarMultiComponent;
  let fixture: ComponentFixture<WebinarMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarMultiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
