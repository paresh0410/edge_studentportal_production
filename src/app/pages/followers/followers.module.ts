import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';

import { CommonModule } from '@angular/common';

// import { FollowersComponent } from './followers.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    // FollowersComponent
  ],
  providers:[
     
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class FollowersModule {
}
