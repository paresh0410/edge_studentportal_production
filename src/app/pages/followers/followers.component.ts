import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent implements OnInit {

followers = [
  {profilePic: "fas fa-user-circle" , name: "John Doe"},
  {profilePic: "fas fa-user-circle" , name: "John Doe"},
  {profilePic: "fas fa-user-circle" , name: "John Doe"},
  {profilePic: "fas fa-user-circle" , name: "John Doe"},
 ];


  constructor() { }

  ngOnInit() {
  }

}
