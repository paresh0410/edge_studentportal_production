import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'ngx-add-pathway',
	templateUrl: './add-pathway.component.html',
	styleUrls: ['./add-pathway.component.scss']
})
export class AddPathwayComponent implements OnInit {

	firstbtn: boolean;
	secondbtn: boolean;
	thirdbtn: boolean;

	btngrp = { firstbtn: true, secondbtn: false, thirdbtn: false };
	selectedCount: number = 0;


	isMandatory: string;
	isSelected: boolean;
	cardImg: string;
	content: string;
	stages: string;

	addpathway = [
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "Mandatory", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "Mandatory", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "Mandatory", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: "", cardImg: "./assets/images/kitten-corporate.png", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },

	];
	constructor() {

	}

	ngOnInit() {
	}

	radiofn(post) {
		this.selectedCount = 0;

		if (!post.isSelected) {
			post.isSelected = true;       //if radio button is false make it true

			return this.selectedCount = this.count();        // store count in variable if true
		} else {
			post.isSelected = false;
			return this.selectedCount = this.count();        // store count in variable if false
		}
	}


	activatefirst(btns) {
		if (!btns.firstbtn) {
			btns.firstbtn = true;
			btns.secondbtn = false;
			btns.thirdbtn = false;
		}
	}
	count() {
		var count = 0;
		this.addpathway.forEach(obj => {      //for each object in array of addpathway
			console.log(obj);
			if (obj.isSelected == true)            // if array element is selected
			{
				count++;                          //increment count
			}
		})
		return count;
	}

	activatesecond(btns) {
		if (!btns.secondbtn) {
			btns.firstbtn = false;
			btns.secondbtn = true;
			btns.thirdbtn = false;
		}
	}

	activatethird(btns) {
		if (!btns.thirdbtn) {
			btns.firstbtn = false;
			btns.secondbtn = false;
			btns.thirdbtn = true;
		}
	}



	togglesave(item) {
		if (!item.save) {
			item.save = true;
		} else {
			item.save = false;
		}
	}

}
