import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfilePageComponent } from "./profile-page/profile-page.component";
// import { SignupComponent } from './signup/signup.component';
// import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { NoDataFoundComponent } from './no-data-found/no-data-found.component';
import { InterestsComponent } from './interests/interests.component';
import { LearnComponent } from './learn/learn.component';
import { LearnSearchComponent } from './learn-search/learn-search.component'
import { CourseDetailsComponent } from './course-details/course-details.component';
import { PulseComponent } from './pulse/pulse.component';
import { AddPathwayComponent } from './add-pathway/add-pathway.component';
import { InsidePathwayComponent } from './inside-pathway/inside-pathway.component';
import { QuizComponent } from './quiz/quiz.component';
import { QuizReviewComponent } from './quiz-review/quiz-review.component'
import { IntermediateComponent } from './intermediate/intermediate.component';
import { CourseComponent } from './course/course.component';
import { ChatComponent } from './chat/chat.component';
import { FavouritesComponent } from './favourites/favourites.component';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { CreateBookmarkComponent } from './create-bookmark/create-bookmark.component';
import { TeamDashboardComponent } from './team-dashboard/team-dashboard.component';
import { TrainerDashboardComponent } from './trainer-dashboard/trainer-dashboard.component';
import { TaskManagerComponent } from './task-manager/task-manager.component';
import { CalendarComponent } from './calendar/calendar.component';
import { Level1Component } from './level1/level1.component';
import { Level2Component } from './level2/level2.component';
import { TrainerDetailsComponent } from './trainer-details/trainer-details.component';
import { SurveyComponent } from './survey/survey.component';
import { PollComponent } from './poll/poll.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { CoachingComponent } from './coaching/coaching.component';
import { AuthGuard } from '../service/auth.guard';
import { PartnerComponent } from '../pages/partner/partner.component';
// import { CallDetailComponent } from './call-detail/call-detail.component';
import { from } from 'rxjs';

import { SectionComponent } from './tools/section/section.component';
import { ContentComponent } from './tools/content/content.component';
import { TTTNominationComponent } from './ttt-nomination/ttt-nomination.component';
import { AttendanceDataComponent } from './attendance-data/attendance-data.component';
import { AssessmentDataComponent } from './assessment-data/assessment-data.component';
import { FeedbackDataComponent } from './feedback-data/feedback-data.component';
import { MarkAttendanceComponent } from './mark-attendance/mark-attendance.component';
import { TaTrainerDetailComponent } from './ta-trainer-detail/ta-trainer-detail.component';
import { TttBookSlotsComponent } from './ttt-book-slots/ttt-book-slots.component';
import { TttFeedbackActivityComponent } from './ta-trainer-detail/ttt-feedback-activity/ttt-feedback-activity.component';
import { TttWebinarActivityComponent } from './ta-trainer-detail/ttt-webinar-activity/ttt-webinar-activity.component';
import { TttBhApprovalComponent } from './ttt-bh-approval/ttt-bh-approval.component';
import { TrainerDashboardDetailsComponent } from './trainer-dashboard-details/trainer-dashboard-details.component';
import { BatchesDetailComponent } from './trainer-dashboard-details/batches-detail/batches-detail.component';
import { TrainerTAevalutionfbComponent } from './trainer-taevalutionfb/trainer-taevalutionfb.component';
import { TttFeedbackComponent } from '../pages/ttt-nomination/ttt-feedback/ttt-feedback.component';
import { EepStepComponent } from '../pages/eep-step/eep-step.component';
import { EepGbhApprovalComponent } from '../pages/eep-gbh-approval/eep-gbh-approval.component';
import { CcCallsListComponent } from '../pages/cc-calls-list/cc-calls-list.component';
import { CcTrainerNominationlistComponent } from '../pages/cc-trainer-nominationlist/cc-trainer-nominationlist.component';
import { PartnerNomineeCallComponent } from '../pages/partner-nominee-call/partner-nominee-call.component';
import { WorkflowLearnComponent } from './workflow-learn/workflow-learn.component';
import { ContentDisplayComponent } from './content-display/content-display.component';
// import { DynamicLinkLearnComponent } from './dynamic-link-learn/dynamic-link-learn.component';
// import { DynamicLinkModule } from './dynamic-link/dynamic-link.module';
// import { ModuleActivityComponent } from './module-activity-module/module-activity/module-activity/module-activity.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  },
  {
    path: 'NoData',
    component: NoDataFoundComponent
  },
  // {
  //   path: 'VideoPlayer',
  //   component: VideoPlayerComponent
  // },
  {
    path: 'profile',
    component: ProfilePageComponent,
  },
  {
    path: 'learn',
    component: LearnComponent,
  },
  {
    path: 'workflow_learn',
    component: WorkflowLearnComponent,
  },
  {
    path: 'learn-search/:search',
    canActivate: [AuthGuard],
    component: LearnSearchComponent,
  },
  // {
  //   path: 'dynamic-link-learn/:areaId/:instanceId/:indentifier',
  //   canActivate: [AuthGuard],
  //   component: DynamicLinkLearnComponent,
  // },
  {
    path: 'dynamic-link-learn/:areaId/:instanceId/:indentifier',
    canActivate: [AuthGuard],
    loadChildren: './dynamic-link/dynamic-link.module#DynamicLinkModule',
  },
  {
    path: 'course-detail',
    component: CourseDetailsComponent,
  },
  {
    path: 'pulse',
    component: PulseComponent,
  },
  {
    path: 'chat',
    component: ChatComponent,
  },
  {
    path: 'favourites',
    component: FavouritesComponent,
    // children:[
    //   {
    //     path : 'favourites/bookmark', component: BookmarkComponent
    //   }
    // ]
  },
  {
    path: 'trainer-dashboard',
    component: TrainerDashboardComponent,
  },
  {
    path: 'task-manager',
    component: TaskManagerComponent,
  },
  {
    path: 'calendar',
    component: CalendarComponent,
  },
  {
    path: 'call-coaching',
    component: CoachingComponent,
  },
  {
    path: 'team-dashboard',
    component: TeamDashboardComponent,
  },
  {
    path: 'partner',
    component: PartnerComponent,
  },
  {
    path: 'nomination',
    component: TTTNominationComponent,
  },
  {
    path: 'module-activity',
    loadChildren : './lazy/lazy.module#LazyModule',
    // loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
    // loadChildren: './module-activity-module/module-activity.module#ModuleActivityModule',
  },
  {
    path: 'learning',
    loadChildren : './learn-container/learn-container.module#LearnContainerModule'
    // loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
    // loadChildren: './module-activity-module/module-activity.module#ModuleActivityModule',
  },
  // {
  //   path: 'quiz-review',
  //   component: QuizReviewComponent,
  // },
  // {
  //   path: 'mark-attendance',
  //   component: MarkAttendanceComponent
  // },
  // {
  //   path: 'call-detail',
  //   component: CallDetailComponent,
  // },
  {
    path: '',
    component: IntermediateComponent,
    children: [
      { path: 'learn/course-detail', component: CourseDetailsComponent },
      { path: 'learn/course-detail/course', component: CourseComponent },
      { path: 'learn/course-detail/quiz', component: QuizComponent },
      { path: 'learn/course-detail/quiz/quiz-review', component: QuizReviewComponent },
      { path: 'learn/course-detail/feedback', component: FeedbackComponent },
      { path: 'learn/course-detail/mark-attendance', component: MarkAttendanceComponent },
      { path: 'learn-search/:search/course-detail', component: CourseDetailsComponent },
      { path: 'learn-search/:search/course-detail/course', component: CourseComponent },
      { path: 'learn-search/:search/course-detail/quiz', component: QuizComponent },
      { path: 'learn-search/:search/course-detail/quiz/quiz-review', component: QuizReviewComponent},
      { path: 'learn-search/:search/course-detail/feedback', component: FeedbackComponent },
      { path: 'learn-search/:search/course-detail/mark-attendance', component: MarkAttendanceComponent },
      { path: 'nomination/course', component: CourseComponent },
      { path: 'nomination/quiz', component: QuizComponent },
      { path: 'nomination/quiz/quiz-review', component: QuizReviewComponent },
      { path: 'nomination/feedback', component: FeedbackComponent },
      { path: 'nomination/mark-attendance', component: MarkAttendanceComponent },
      { path: 'profile/addpathway', component: AddPathwayComponent },
      { path: 'profile/insidePathway', component: InsidePathwayComponent },
      { path: 'favourites/bookmark', component: BookmarkComponent },
      { path: 'favourites/create-bookmark', component: CreateBookmarkComponent },
      { path: 'trainer-dashboard/level-1', component: Level1Component },
      { path: 'trainer-dashboard/level-1/level-2', component: Level2Component },
      { path: 'trainer-dashboard/trainer-details', component: TrainerDetailsComponent },
      { path: 'trainer-dashboard/trainer-details/course', component: CourseComponent },
      { path: 'trainer-dashboard/trainer-details/attendance', component: AttendanceDataComponent },
      { path: 'trainer-dashboard/trainer-details/assessment', component: AssessmentDataComponent },
      { path: 'trainer-dashboard/trainer-details/feedback', component: FeedbackDataComponent },
      { path: 'trainer-dashboard/trainer-details/TrainerTAevalutionfb', component: TrainerTAevalutionfbComponent },
      { path: 'trainer-dashboard/trainer-details/trainer-feedback', component: TttFeedbackComponent },
      { path: 'ttt-trainer-detail', component: TaTrainerDetailComponent },
      { path: 'dashboard/survey', component: SurveyComponent },
      { path: 'dashboard/poll', component: PollComponent },
      { path: 'dashboard/feedback', component: FeedbackComponent },
      { path: 'book-slot', component: TttBookSlotsComponent },
      { path: 'trainer-feedback', component: TttFeedbackActivityComponent },
      // { path: 'ttt-BH-approval', component: TttBhApprovalComponent },
      { path: 'trainer-dashboard/trainer-dashboard-detail', component: TrainerDashboardDetailsComponent },
      { path: 'trainer-dashboard/trainer-dashboard-detail/batch-details', component: BatchesDetailComponent },
      { path: 'trainer-webinar', component: TttWebinarActivityComponent },
      { path: 'ttt-BH-approval', canActivate: [AuthGuard], component: TttBhApprovalComponent },
      { path: 'eep-step', component: EepStepComponent },
      { path: 'eep-GBH-approval', canActivate: [AuthGuard], component: EepGbhApprovalComponent },
      { path: 'eep-step/quiz', component: QuizComponent },
      { path: 'eep-step/quiz/quiz-review', component: QuizReviewComponent },
      { path: 'call-coaching-list', component: CcCallsListComponent},
      { path: 'train-nomination',  component: CcTrainerNominationlistComponent},
      { path: 'call-coaching-list/course', component: CourseComponent},
      { path: 'PartnerNomineeCall', component:PartnerNomineeCallComponent},
      { path: 'section', component: SectionComponent},
      { path: 'section-content', component: ContentComponent },
      { path: 'section-content/content-details', component: ContentDisplayComponent}
    ],
  },
  // {
  //   path: 'addpathway', component: AddPathwayComponent
  // },
  // {
  //   path: 'insidePathway', component: InsidePathwayComponent
  // },
  // {
  //   path:'quiz', component: QuizComponent
  // },
  // {
  //   path: 'signup',
  //   component: SignupComponent,
  // },
  // {
  //   path: 'iot-dashboard',
  //   component: ECommerceComponent,
  // }, {
  //   path: 'ui-features',
  //   loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  // }, {
  //   path: 'modal-overlays',
  //   loadChildren: './modal-overlays/modal-overlays.module#ModalOverlaysModule',
  // }, {
  //   path: 'extra-components',
  //   loadChildren: './extra-components/extra-components.module#ExtraComponentsModule',
  // }, {
  //   path: 'bootstrap',
  //   loadChildren: './bootstrap/bootstrap.module#BootstrapModule',
  // }, {
  //   path: 'maps',
  //   loadChildren: './maps/maps.module#MapsModule',
  // }, {
  //   path: 'charts',
  //   loadChildren: './charts/charts.module#ChartsModule',
  // }, {
  //   path: 'editors',
  //   loadChildren: './editors/editors.module#EditorsModule',
  // }, {
  //   path: 'forms',
  //   loadChildren: './forms/forms.module#FormsModule',
  // }, {
  //   path: 'tables',
  //   loadChildren: './tables/tables.module#TablesModule',
  // }, {
  //   path: 'miscellaneous',
  //   loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  // },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  },],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
