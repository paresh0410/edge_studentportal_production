import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ENUM } from '../../../service/enum';
import { TrainTheTrainerServiceProvider } from '../../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CourseServiceProvider } from '../../../service/course-service';
import { CourseDetailServiceProvider } from '../../../service/course-detail-service';
import { LearnServiceProvider } from '../../../service/learn-service';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from "@angular/forms";

@Component({
  selector: 'ttt-content',
  templateUrl: './ttt-content.component.html',
  styleUrls: ['./ttt-content.component.scss']
})
export class TttContentComponent implements OnInit, OnChanges {
  @Input('step') step1;
  nomineeModules: any = [];
  ActiveTab: any;
  resetPwdForm: FormGroup;
  nomineeContent: any = [];
  userDetails: any = [];
  stepsdata: any = [];
  currentstepdata: any = [];
  courseDetailSummary: any = [];
  checkResponse: any;
  displayInstructions: boolean = false;
  passKey: string;
  workflowdetail: any = [];
  passWrong: boolean = false;
  passCheck: boolean = false;
  submitted: boolean = false;
  employeeId: any = [];
  password = '';
  @Input('stepId') stepId;
  constructor(public ttt: TrainTheTrainerServiceProvider,
    public spinner: NgxSpinnerService,
    public CSP: CourseServiceProvider,
    public CDSP: CourseDetailServiceProvider,
    public toastr: ToastrService,
    private router: Router,
    private routes: ActivatedRoute,
    public LSP: LearnServiceProvider,
    private formBuilder: FormBuilder
  ) {
    this.workflowdetail = this.ttt.workflow;
    console.log(this.workflowdetail);
    // this.ActiveTab = this.nomineeModules[0].id;
    // this.nomineeContent = this.nomineeModules[0].list;
    this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
  }

  ngOnInit() {

  }
  disabledActivityFlag = false;
  tabChanged(data) {
    console.log('DATA--->', data);
    this.ActiveTab = data.id;
    this.nomineeContent = this.nomineeModules[data.id].list;
    for (let i = 0; i < this.nomineeContent.length; i++) {
      const temp: any = i + 1;
      // this.courseDetailArray[i].img =
      //   "assets/images/open-book-leaf-2.jpg";
      this.nomineeContent[i].img = 'assets/images/open-book-leaf-2.jpg';
      // if(this.courseDetailArray[i].activity_type === 'Quiz'){
      //   this.courseDetailArray[i].img = 'assets/images/activity_image/quiz.jpg';
      // }
      // else {
      //   this.courseDetailArray[i].img = 'assets/images/open-book-leaf-2.jpg';
      // }
      // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
      this.nomineeContent[i].activityTempId = temp;
      const tempCourseI = this.nomineeContent[i];
      //var tempDate = new Date(this.courseDetailArray[i].completed);
      for (let j = 0; j < this.nomineeContent.length; j++) {
        const tempCourseJ = this.nomineeContent[j];
        //this.courseDetailArray[j].activityDuration = this.makeTimeReady(this.courseDetailArray[j].activityDuration.split('.')[0]);
        if (tempCourseI.dependentActId !== 0 || !tempCourseI.dependentActId) {
          if (tempCourseI.dependentActId === tempCourseJ.activityId) {
            if (tempCourseJ.wCompleted === 0) {
              tempCourseI.dependant = tempCourseJ.activityName;
              // break;
            }
          }
        }
      }
    }
    console.log(this.nomineeContent);
  }

  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToCourse(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}
  observermodule() {
    this.nomineeModules = [];
    this.nomineeContent = [];
    const data = {
      wfId: this.workflowdetail.id,
      empId: this.employeeId,
      tId: this.userDetails.tenantId,
    };
    console.log(data);
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.observercourse;
    this.ttt.get(url, data).then(res => {
      console.log(res);
      try {
        this.spinner.hide();
        this.nomineeModules = res['data'];
        console.log('this.nomineeModules', this.nomineeModules);
        this.moduledetail(this.nomineeModules);
      } catch (e) {
        console.log(e);
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  selectionbatch() {
    const data = {
      wfId: this.workflowdetail.id,
      empId: this.employeeId,
      tId: this.userDetails.tenantId,
    };
    console.log(data);
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.tttselectedbatch;
    this.ttt.get(url, data).then(res => {
      console.log(res);
      this.spinner.hide();
      try {
        this.nomineeModules = res['data'];
        this.moduledetail(this.nomineeModules);
      } catch (e) {
        console.log(e);
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  content() {
    const data = {
      actId: this.currentstepdata.activityId,
      tId: this.userDetails.tenantId,
      isActivityEnable: this.currentstepdata.isActivityEnable,
      isModuleEnable: this.currentstepdata.isModuleEnable,
      modId: this.currentstepdata.modId,
      enId : this.workflowdetail.enrolId,
    };
    this.spinner.show();
    console.log(data);
    const url = ENUM.domain + ENUM.url.getmodules;
    this.ttt.get(url, data).then(res => {
      console.log(res);
      try {
        this.nomineeModules = res['data'];
        if(this.nomineeModules){
        this.moduledetail(this.nomineeModules);
        }
      } catch (e) {
        console.log(e);
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  moduledetail(nominee) {
    if (this.nomineeModules.length > 0) {
      for (var i = 0; i < this.nomineeModules.length; i++) {
        var temp: any = i + 1;
        this.nomineeModules[i].id = i;
        // this.nomineeModules[i].moduleName = 'Module' + ' ' + temp;
      }
      this.ActiveTab = this.nomineeModules[0].id;
      this.nomineeContent = this.nomineeModules[0].list;
      try {
        for (var i = 0; i < this.nomineeContent.length; i++) {
          var temp: any = i + 1;
          this.nomineeContent[i].img =
            "assets/images/open-book-leaf-2.jpg";
          // this.nomineeContent[i].activityName = 'Activity' + ' ' + temp;
          this.nomineeContent[i].activityTempId = temp;
          var tempCourseI = this.nomineeContent[i];
          var tempDate = new Date(this.nomineeContent[i].completed);
          for (var j = 0; j < i; j++) {
            var tempCourseJ = this.nomineeContent[j];
            if (tempCourseI.dependentActId != 0) {
              if (tempCourseI.dependentActId == tempCourseJ.activityId) {
                if (tempCourseJ.wCompleted == 0) {
                  tempCourseI.dependant = tempCourseJ.activityName;
                  // break;
                } else {
                  tempCourseI.dependant = null;
                }
              } else {
                tempCourseI.dependant = null;
              }
            } else {
              tempCourseI.dependant = null;
            }
          }
        }
      } catch (e) {
        console.log(e);
      }
      console.log("CD===>", this.nomineeContent);
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    console.log('stepData' , this.step1);
    if(changes.stepId.previousValue !== changes.stepId.currentValue){
      this.resetPwdForm = this.formBuilder.group({
        password: ["", [Validators.required, Validators.minLength(6)]]
      });
      this.currentstepdata = this.step1;
      console.log(this.currentstepdata);

      if (this.stepId == 5) {
        this.observermodule();
      }else if (this.stepId == 9) {
        this.selectionbatch();
      } else {
        if (this.currentstepdata) {
          this.content();
        }
      }
    }
  }
  goToCourse(data) {
    this.CSP.setCourse(this.courseDetailSummary, data);
    console.log("DATA---->", data);
    if (data.activityTypeId == 5) {
      const userDetails = JSON.parse(localStorage.getItem("userDetails"));
      const employeeId = localStorage.getItem('employeeId');
      var param = {
        qId: data.quizId,
        tId: userDetails.tenantId,
        uId: employeeId,
        cId: data.courseId,
        mId: data.moduleId,
        aId: data.activityId,
        enId: this.workflowdetail.enrolId,
      };
      var url = ENUM.domain + ENUM.url.checkQuizUser;
      this.CDSP.getCourseDetailsData(url, param)
        .then(result => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log("CHECk QUIZ FOR USER---->", temp.data[0]);
              this.checkResponse = temp.data[0];
              if (temp.data[0].flag == "true") {
                console.log("PassCheck===>", temp.data[0]);
                if (temp.data[0].qPassword == 0) {
                  if (this.checkResponse.instructions) {
                    this.displayInstructions = true;
                  } else {
                    this.displayInstructions = false;
                    const courseDetailSummary = {
                      courseFrom: 2,
                    };
                    this.CSP.setCourse(courseDetailSummary, data);
                    this.router.navigate(["quiz"], { relativeTo: this.routes });
                  }
                } else {
                  this.passCheck = true;
                  this.passKey = temp.data[0].passWord;
                }
              } else {
                this.toastr.warning(temp.data[0].msg, "Warning!").onHidden.subscribe(()=>
                this.toastr.clear());
              }
            } else {
              // this.toastr.error(
              //   "Something went wrong please try again later",
              //   "Server Error!"
              // );
              this.toastr.warning("No Data Available", "Warning!").onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            // this.toastr.error(
            //   "Something went wrong please try again later",
            //   "Server Error!"
            // );
            this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
          // this.toastr.error(
          //   "Something went wrong please try again later",
          //   "Server Error!"
          // );
          this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (data.activityTypeId == 6) {
      var employeeId = localStorage.getItem("employeeId");
      var userDetails = JSON.parse(localStorage.getItem("userDetails"));
      var params = {
        // fId: data.feedbackId,
        // tId: userDetails.tenantId,
        // eId: employeeId,
        // enId : this.workflowdetail.enrolId,
        fId: data.feedbackId,
        tId: userDetails.tenantId,
        eId: employeeId,
        cId: data.courseId,
        mId: data.moduleId,
        aId: data.activityId,
        enId : data.enrolId,
      };
      var url = ENUM.domain + ENUM.url.checkFeedbackUser;
      this.CDSP.getCourseDetailsData(url, params)
        .then(result => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log("CHECk FEEDBACK FOR USER---->", temp.data[0]);
              if (temp.data[0].flag == "true") {
                this.CDSP.setFeedbackSummary(data);
                const courseDetailSummary = {
                  courseFrom: 2,
                };
                this.CSP.setCourse(courseDetailSummary, data);
                this.router.navigate(["feedback"], { relativeTo: this.routes });
              } else {
                this.toastr.warning(temp.data[0].msg, "Warning!").onHidden.subscribe(()=>
                this.toastr.clear());
              }
            } else {
              // this.toastr.error(
              //   "Something went wrong please try again later",
              //   "Server Error!"
              // );
              this.toastr.warning("No Data Available", "Warning!").onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            // this.toastr.error(
            //   "Something went wrong please try again later",
            //   "Server Error!"
            // );
            this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
          // this.toastr.error(
          //   "Something went wrong please try again later",
          //   "Server Error!"
          // );
          this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (data.activityTypeId == 9) {
      const courseDetailSummary = {
        courseFrom: 2,
      };
      this.CSP.setCourse(courseDetailSummary, data);
      this.router.navigate(["mark-attendance"], { relativeTo: this.routes });
    } else {
      const courseDetailSummary = {
        courseFrom: 2,
      };
      this.CSP.setCourse(courseDetailSummary, data);
      this.router.navigate(["course"], { relativeTo: this.routes });
    }
  }

  close() {
    this.passCheck = false;
  }

  closedisplayInstructions() {
    this.displayInstructions = false;
  }

  get f() {
    return this.resetPwdForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.resetPwdForm.invalid) {
      return;
    } else {
      console.log("EnteredPassword--->", this.resetPwdForm.value.password);
      console.log("PasswordKey--->", this.passKey);
      if (this.resetPwdForm.value.password == this.passKey) {
        this.passCheck = false;
        if (this.checkResponse.instructions) {
          this.displayInstructions = true;
        } else {
          this.displayInstructions = false;
          this.router.navigate(["quiz"], { relativeTo: this.routes });
        }
      } else {
        this.passWrong = true;
      }
    }
  }

  onSubmit1() {
    this.router.navigate(["quiz"], { relativeTo: this.routes });
  }

  bindBackgroundImage(detail) {
    // detail.img = 'assets/images/open-book-leaf-2.jpg';
    // return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};
    if (detail.activity_type === 'Quiz'){
      detail.img = 'assets/images/activity_image/quiz.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
    } else if ( detail.activity_type  === 'Feedback'){
      detail.img = 'assets/images/activity_image/feedback.jpg';
      return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
     } else if ( detail.referenceType  === 'video' || detail.mimeType  === 'video/mp4'){
        detail.img = 'assets/images/activity_image/video.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else if ( detail.referenceType  === 'audio' || detail.mimeType  === 'audio/mpeg'){
        detail.img = 'assets/images/activity_image/audio.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else if ( detail.referenceType  === 'application' && detail.mimeType  === 'application/zip'){
        detail.img = 'assets/images/activity_image/scrom.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      } else if ( detail.referenceType  === 'application' && detail.mimeType  === 'application/pdf'){
        detail.img = 'assets/images/activity_image/pdf.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else if ( detail.referenceType  === 'kpoint' || detail.mimeType  === 'embedded/kpoint'){
        detail.img = 'assets/images/activity_image/video.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else if ( detail.referenceType  === 'image'){
        detail.img = 'assets/images/activity_image/image.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else if ( detail.referenceType  === 'application' && detail.mimeType  === 'application/x-msdownload'){
        detail.img = 'assets/images/activity_image/url.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else if ( detail.formatId == 9){
        detail.img = 'assets/images/activity_image/practice_file.jpg';
        return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
      }else {
        detail.img= 'assets/images/open-book-leaf-2.jpg';
        return { 'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' +  detail.img + ')' };
    }
  }

}
