import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttContentComponent } from './ttt-content.component';

describe('TttContentComponent', () => {
  let component: TttContentComponent;
  let fixture: ComponentFixture<TttContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
