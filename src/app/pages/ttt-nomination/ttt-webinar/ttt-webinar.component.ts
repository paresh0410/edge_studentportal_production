import { Component, OnInit, ViewEncapsulation, Input } from "@angular/core";
import { ENUM } from "../../../service/enum";
import { TrainTheTrainerServiceProvider } from "../../../service/traine-the-trainer.service";
import { CourseServiceProvider } from "../../../service/course-service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Router } from '@angular/router';
import { TrainerAutomationServiceProvider } from "../../../service/trainer-automation.service";
import { WebinarConfig } from '../../../pages/webinar-multiple/webinar-entity';
import { feature } from '../../../../environments/feature-environment';

@Component({
  selector: "ngx-ttt-webinar",
  templateUrl: "./ttt-webinar.component.html",
  styleUrls: ["./ttt-webinar.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class TttWebinarComponent implements OnInit {
  @Input('step') step1;
  tenantId: number;
  activityData: any;
  voiceCall: boolean = true;
  videoCall: boolean = true;
  courseReadyFlag: boolean = false;
  showActivity: boolean = false;
  showwebinar: boolean = false;
  webinarlist: any =[] ;
  showlist: boolean = false;
  Nolist: boolean = false;
  playvdo: boolean = false;
  usersData: any;
  trainerworkflow: any = [];
  coursecontentdata: any = [];
  userList: any = [];
  currentstepdata: any = [];
  nomineeModules: any = [];
  nomineeContent: any = [];
  ActiveTab: any;
  userDetails: any;
  config: WebinarConfig = {
    voice: true,
    video: true,
    creator: false,
    areaId: 29,
    instanceId: null,
    tenantId: null,
    type: null,
    recording: true,
    allowCalls: true,
    startDate: '',
    EndDate: '',
  };
  userdetail: any = [];
  videotrack: any;
  rolename: any;
  disabledActivityFlag = false;
  featureConfig;
  constructor(
    private tttService: TrainTheTrainerServiceProvider,
    private CSP: CourseServiceProvider,
    public spinner: NgxSpinnerService,
    public router: Router, public TAServiceProvider: TrainerAutomationServiceProvider,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    // this.activityData = this.tttService.getActivityData();
    // console.log("WEBINARDATA===>", this.activityData);
    // this.gettrainersData(this.activityData);
    this.trainerworkflow = this.tttService.workflow;
    // this.trainerworkflow = this.TAServiceProvider.trainerworkflow;
    // console.log(this.trainerworkflow);
    this.coursecontentdata = this.tttService.coursecontent;
    // console.log(this.coursecontentdata);
    this.currentstepdata = this.step1;
    console.log('CurrentStepData===>', this.currentstepdata);
    if (localStorage.getItem('userDetails')) {
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.userdetail = JSON.parse(localStorage.getItem('userdetail'));
      this.tenantId = this.userDetails.tenantId;
      this.config.tenantId = this.tenantId;
    }
    this.featureConfig = feature;
    this.config = {
      voice: this.voiceCall,
      video: this.videoCall,
      creator: this.userdetail.roleId === 7 ? true : false,
      areaId: 2,
      instanceId: this.trainerworkflow.id,
      tenantId: this.tenantId,
      type: null,
      recording: true,
      allowCalls: true,
      startDate: '',
      EndDate: '',
    };
    if (this.currentstepdata) {
      this.content();
      this.getrecordinglist();
    }
  }

  content() {
    if (localStorage.getItem('userDetails')) {
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.tenantId = this.userDetails.tenantId;
      const data = {
        actId: this.currentstepdata.activityId,
        tId: this.userDetails.tenantId,
        isActivityEnable: this.currentstepdata.isActivityEnable,
        isModuleEnable: this.currentstepdata.isModuleEnable,
        modId: this.currentstepdata.modId
      };
      this.spinner.show();
      console.log(data);
      const url = ENUM.domain + ENUM.url.getmodules;
      this.tttService.get(url, data).then(res => {
        console.log(res);
        this.spinner.hide();
        this.nomineeModules = res['data'];
        if (this.nomineeModules.length > 0) {
          for (var i = 0; i < this.nomineeModules.length; i++) {
            var temp: any = i + 1;
            this.nomineeModules[i].id = i;
            // this.nomineeModules[i].moduleName = 'Module' + ' ' + temp;
          }
          this.ActiveTab = this.nomineeModules[0].id;
          this.nomineeContent = this.nomineeModules[0].list;
          for (var i = 0; i < this.nomineeContent.length; i++) {
            var temp: any = i + 1;
            this.nomineeContent[i].img =
              "assets/images/open-book-leaf-2.jpg";
            // this.nomineeContent[i].activityName = 'Activity' + ' ' + temp;
            this.nomineeContent[i].activityTempId = temp;
            var tempCourseI = this.nomineeContent[i];
            var tempDate = new Date(this.nomineeContent[i].completed);
            for (var j = 0; j < i; j++) {
              var tempCourseJ = this.nomineeContent[j];
              if (tempCourseI.dependentActId != 0) {
                if (tempCourseI.dependentActId == tempCourseJ.activityId) {
                  if (tempCourseJ.wCompleted == 0) {
                    tempCourseI.dependant = tempCourseJ.activityName;
                    // break;
                  } else {
                    tempCourseI.dependant = null;
                  }
                } else {
                  tempCourseI.dependant = null;
                }
              } else {
                tempCourseI.dependant = null;
              }
            }
          }
          console.log("CD===>", this.nomineeContent);
        }
      }, err => {
        this.spinner.hide();
        console.log(err);
      });

    }

  }

  getUsersData(data) {
    if (localStorage.getItem('userdetail') && localStorage.getItem('employeeId')) {
      const uD = JSON.parse(localStorage.getItem('userdetail'));
      const employeeId = localStorage.getItem('employeeId');
      const params = {
        wfId: this.trainerworkflow.id,
        // wfId: 1,
        empId: employeeId,
        tId: uD.tenantId,
      };
      const url = ENUM.domain + ENUM.url.getTrainerForWebinar;
      this.tttService
        .getTrianers(url, params)
        .then((result: any) => {
          console.log("DATA--->", result);
          if (result.type == true) {
            if (result.data.length > 0) {
              this.usersData = result['data'][0];
              this.userList = result['data'];
              this.usersData.courseId = this.trainerworkflow.id;          // Hardcoded workflowId is passed in courseId
              this.courseReadyFlag = true;
            } else {
              this.toastr.warning(
                "No user available.",
                "Error Occured!"
              ).onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            this.toastr.warning(
              "Please try again after sometime.",
              "Error Occured!"
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        });
    } else {
      this.toastr.warning(
        "Please login again",
        "Error Occured!"
      ).onHidden.subscribe(()=>
      this.toastr.clear());
    }
  }

  goToWebinar(data) {
    this.showActivity = true;
    this.showwebinar = true;
    this.activityData = data;
    this.getUsersData(this.nomineeModules);
  }
  // getrecording() {
  //   this.getrecordinglist();
  // }
  getrecordinglist() {
    const params = {
      // wfId: this.trainerworkflow.id,
      wfId: this.currentstepdata.activityId, // for demo
    };
    // const url = ENUM.domain + ENUM.url.webinar_recording;
    this.tttService.list(params).then(res => {
      console.log(res);
      if (res.type == true) {
        this.webinarlist = res.data;
        if (this.webinarlist.length !== 0) {
          // this.Nolist = true;
          // this.showlist = true;
          // this.showActivity = true;
          // this.showwebinar = false;
        } else {
          // this.showActivity = true;
          // this.showwebinar = false;
         this.Nolist = true;
        }
      }
    }, err => {
      console.log(err);
    })
  }
  play(item) {
    console.log(item);
    this.videotrack = item.link;
    this.playvdo = true;
    this.showlist = false;
  }
  goBack() {
    this.showActivity = false;
    this.showwebinar = false;
  }
  goBack1() {
    this.showlist = false;
    this.showActivity = false;
    this.Nolist = false;
  }
  goBack2() {
    this.playvdo = false;
    this.showlist = true;
  }
  changeCount(event) {
    console.log('ChnageCount', event);
  }


  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToWebinar(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}

}
