import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttWebinarComponent } from './ttt-webinar.component';

describe('TttWebinarComponent', () => {
  let component: TttWebinarComponent;
  let fixture: ComponentFixture<TttWebinarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttWebinarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttWebinarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
