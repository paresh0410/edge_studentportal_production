import { Component, OnInit, Output, EventEmitter, AfterViewInit, OnChanges, Input } from '@angular/core';
import { TrainTheTrainerServiceProvider } from './../../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from '../../../service/enum';
import { ImageCroppedEvent } from '../../../../../node_modules/ngx-image-cropper';
import { ToastrService } from 'ngx-toastr';
import { ProfileServiceProvider } from '../../../service/profile-service';
import { ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileFormComponent implements OnInit, AfterViewInit, OnChanges {
  @Output() public passData1 = new EventEmitter();
  @Input() response_data;
  @Input() tab_changed;
  employeeDetail: any = {};
  itemsList = [
    {
      id: '1',
      name: 'Yes',
      value: 1,
    },
    {
      id: '2',
      name: 'No',
      value: 0,
    }];
  chosenItem;
  cocProfileData: any;
  userDetails: any;
  tenantId: any;
  employeeId: any;
  userId: any;
  wfresonseId: any;
  profileForm: any = {
    'contact': '',
    'comment': '',
    'isTrained': 0,
    'picRef': '',
    'wfrId': '',
  };
  imageChangedEvent: any = "";
  croppedImage: any = "";
  showCropper = false;
  intialImage = false;
  selectImage = false;
  workflowdetail: any = [];
  selectedstep: any;
  showbtn = false;
  options: FormGroup;
  proData: any = [];
  submitted: any = false;

  constructor(private tttServiceProvider: TrainTheTrainerServiceProvider,
    public spinner: NgxSpinnerService,
    private toastr: ToastrService,
    public ProSP: ProfileServiceProvider, fb: FormBuilder
  ) {
    if (this.tttServiceProvider.cocProfileData) {
      this.cocProfileData = this.tttServiceProvider.cocProfileData;
      console.log('this.cocProfileData', this.cocProfileData);
    }
    this.workflowdetail = this.tttServiceProvider.workflow;
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userId = this.userDetails.id;
    this.tenantId = this.userDetails.tenantId;
    this.employeeId = localStorage.getItem('employeeId');
    console.log('employeeId', this.employeeId);
    console.log('this.userDetails', this.userDetails);
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });

  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.getCocData();
    this.getEmpData();
  }
  getCocData() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.getCocProfileData;
    const param = {
      'wfId': this.workflowdetail.id,
      'empId': this.employeeId,
      'tId': this.tenantId,
    };
    this.tttServiceProvider.getCocProfileData(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT COC Success===>', result);
      console.log('result.data', result.data[0]);
      this.proData = result.data[0];
      if (this.proData) {
        this.profileForm = {
          'contact': this.proData.contactNo,
          'comment': this.proData.comment,
          'isTrained': this.proData.isTrained,
          'picRef': this.proData.profilePicRef,
          'wfrId': this.proData.wfrId,
        };
        if (!this.profileForm.picRef) {
          this.profileForm.picRef = "assets/images/user.png";
        }
        console.log('this.profileForm', this.profileForm);
        console.log(this.profileForm.isTrained);
        this.chosenItem = this.profileForm.isTrained;
      } else {
        console.log('Please agree coc');
        this.profileForm = {};
      }
      this.wfresonseId = result.data[0].wfrId;
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });
  }


  changeProfile(event: any): void {
    // console.log(event);
    // console.log(event.target.files[0]);
    // this.employeeDetail.profilePic = (<FileReader>event.target);
    this.intialImage = true;
    this.selectImage = true;
    this.imageChangedEvent = event;
    console.log(this.imageChangedEvent);
  }

  updateProfile(profileData) {
    this.submitted = true;
    if(this.phone.invalid) {
      return;
    } else {
    // if(profileData.contact != '' && profileData.picRef!=''&& profileData.isTrained!=''){
    if (this.proData.responseType === 1) {
      console.log('profileData', profileData);
     // console.log('profileData', value);
      if(this.chosenItem != null){
        this.spinner.show();
        const url = ENUM.domain + ENUM.url.updateProfileData;
        const param = {
          'contNo': profileData.contact,
          'cmt': profileData.comment,
          'isTrained': profileData.isTrained,
          'picRef': profileData.picRef,
          'userId': this.userId,
          'wfrId': profileData.wfrId,
          'wfId': this.workflowdetail.id,
          'empId': this.employeeId,
          'tId': this.tenantId,
          'stepId': 1,
        };
        if (!param.picRef) {
          param.picRef = this.croppedImage;
        }
        console.log('param', param);
        this.tttServiceProvider.updateProfileData(url, param).then((result: any) => {
          this.spinner.hide();
          if (result.type == true) {
            this.toastr.success(result.data[0].msg, "Success!").onHidden.subscribe(()=>
            this.toastr.clear());
            console.log('RESULT Profile Success===>', result);
            this.selectedstep = '1';
            this.passData1.emit(this.selectedstep);
          } else {
            this.toastr.warning(result.data[0].msg, "Attention !").onHidden.subscribe(()=>
            this.toastr.clear());
            // this.toastr.warning( " Please fill all the fields",'Attention !');
          }
        }).catch(result => {
          this.spinner.hide();
          console.log('RESULT COC Error===>', result);
        });
      }else {
        this.toastr.warning( 'Please fill all the fields', 'Attention !').onHidden.subscribe(()=>
        this.toastr.clear());
      }
    } else if (this.proData.responseType == 0) {
      this.toastr.warning('You are no longer accessibleto TTT workflow ', "Error!").onHidden.subscribe(()=>
      this.toastr.clear());
    } else {
      this.toastr.info('Please accept terms & conditions first', "Info!").onHidden.subscribe(()=>
      this.toastr.clear());
    }
  // }
   }
  }
 phone = new FormControl('', [
    // Validators.required, Validators.pattern(("[6-9]\\d{9}"))
    Validators.required, Validators.pattern(("^[0-9]{10}$"))
  ]);
  getEmpData() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.getEmpData;
    const param = {
      'empId': this.employeeId,
      'tId': this.tenantId,
    };
    this.tttServiceProvider.getEmpData(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT COC Success===>', result);
      console.log('result.data', result.data[0]);
      const empData = result.data[0];
      if (empData) {
        this.employeeDetail = {
          ecn: empData.ecn,
          employeeName: empData.name,
          contact: empData.contact,
          email: empData.email,
          band: empData.band,
          business: empData.business,
          subDepartment: empData.subdept,
          reportingManager: empData.rmname,
        };
      }
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });
  }
  fileChangeEvent(event: any): void {
    this.intialImage = true;
    this.selectImage = true;
    this.imageChangedEvent = event;
    console.log(this.imageChangedEvent);
    // this.intialImage=true;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log('Crop Image ===>', this.croppedImage);
  }
  imageLoaded() {
    this.showCropper = true;
    console.log("Image loaded");
  }
  dissmissImageCropPopModal(index) {
    this.intialImage = false;
    this.selectImage = false;
    if (index == 1) {
      this.profileForm.picRef = this.croppedImage;
    }
  }


  // dissmissImageCropPopModal(index) {
  //   if (localStorage.getItem("userDetails")) {
  //     const userDetails = JSON.parse(localStorage.getItem("userDetails"));
  //     if (index == 1) {
  //       let url = ENUM.domain + ENUM.url.updateProfileData;
  //       let param = {
  //         uId: userDetails.id,
  //         base64: this.croppedImage,
  //         tId: userDetails.tenantId,
  //       };
  //       this.ProSP.uploadProfilePic(url, param).then((res: any) => {
  //         if (res.type) {
  //           this.toastr.success(
  //             "Profile picture updated successfully",
  //             "Success!"
  //           );
  //           this.selectImage = false;
  //         } else {
  //           this.toastr.error(
  //             "Cannot update profile picture. Please try again",
  //             "Error Occurred!"
  //           );
  //         }
  //       });
  //     } else if (index == 2) {
  //       let url = ENUM.domain + ENUM.url.updateProfileData;
  //       let param = {
  //         uId: userDetails.id,
  //         Location: null,
  //         tId: userDetails.tenantId,
  //       };
  //       this.ProSP.deleteProfilePic(url, param).then((res: any) => {
  //         if (res.type) {
  //           this.toastr.success(
  //             "Profile picture removed successfully",
  //             "Success!"
  //           );
  //           this.intialImage = false;
  //           this.selectImage = false;
  //         } else {
  //           this.toastr.error(
  //             "Cannot remove profile picture. Please try again",
  //             "Error Occurred!"
  //           );
  //         }
  //       });
  //     } else {
  //       if ( !this.profileForm.picRef ) {
  //         this.intialImage = false;
  //       }else {
  //         this.intialImage = false;
  //         this.profileForm.picRef = this.profileForm.picRef;
  //       }
  //       this.selectImage = false;
  //     }
  //   } else {
  //     this.toastr.warning("Please login again", "Warning Login!");
  //   }
  // }
  ngOnChanges() {
    console.log("Data ==>", this.response_data);
    if (this.response_data) {
      if (this.response_data.status === 1 && this.response_data.status === 0) {
        this.showbtn = false;
      } else {
        this.getCocData();
        this.showbtn = true;
      }
    }
  }
  onItemChange(item) {
    console.log(item);
    this.profileForm.isTrained = item.value;
    console.log(this.profileForm);
  }
}
