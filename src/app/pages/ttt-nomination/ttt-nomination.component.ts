import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from '@angular/core';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { TrainTheTrainerServiceProvider } from '../../service/traine-the-trainer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from '../../service/enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { CalendarServiceProvider } from '../../service/calendar-service';
import { webApi } from '../../service/webApi';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'ttt-nomination',
  templateUrl: './ttt-nomination.component.html',
  styleUrls: ['./ttt-nomination.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false }
    }
  ]
})
export class TTTNominationComponent implements OnInit {
  workflowStep: any = [];
  batchdetail: any = [];
  stepsData: any = [];
  userDetails: any = [];
  tenantId: any;
  employeeId: any;
  selectedIndex = 0;
  selectedstep = 1;
  userId: any;
  wfresonseId: any;
  workflowdetail: any = [];
  temp: any;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  disabledTab = true;
  count: any;
  certificateDownloadUrl: any;
  certificateContent: any;
  stepRejected = false;
  stepRejectedMessage = '';

  // Popup Variables
  // tttShowInstructionPopup: boolean = true;
  // currentStepIndexClicked: number;
  // currentStepClicked = {};



  constructor(
    private tttServices: TrainTheTrainerServiceProvider,
    private router: Router,
    private _formBuilder: FormBuilder,
    private routes: ActivatedRoute,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService,
    private CalSP: CalendarServiceProvider,
    private cdf: ChangeDetectorRef,
    public toastr: ToastrService,
  ) {
    try {
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
      this.userId = this.userDetails.id;
      this.tenantId = this.userDetails.tenantId;
      this.employeeId = localStorage.getItem('employeeId');
      console.log('employeeId', this.employeeId);
      console.log('this.userDetails', this.userDetails);
    } catch (err) {
      console.log(err);
    }
    this.workflowdetail = this.tttServices.workflow;
    this.getAllSteps(true);
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
   }

  componentDataPassedd(event) {
      // this.getAllSteps(true);
      if(event.resetStep){
        this.getAllSteps(false);
      }else{
        console.log('pass data : ', event);
        this.disabledTab = event.disabled;
        this.selectTab(event.selectedIndex);
      }



  }
  componentDataPass(event) {
    this.stetdetail('', event);
    this.getAllSteps(true);
  }

  selectTab(index: number): void {
    this.selectedIndex = index;
  }

  getAllSteps(loaderFlag) {
    if(loaderFlag) {
      this.spinner.show();
    }

    const param = {
      wfId: this.workflowdetail.id,
      tId: this.tenantId,
      empId: this.employeeId
    };
    const url = ENUM.domain + ENUM.url.getAllWorkFlowSteps;

    this.tttServices
      .getAllWorkFlowSteps(url, param)
      .then((result: any) => {
        if(loaderFlag) {
          this.spinner.hide();
        }

        this.stepsData = result.data;
        // for(const data of this.stepsData){

        //  Old

        // for (let i = 0; i < this.stepsData.length; i++) {
        //   if(this.stepsData[i].status == 0){
        //     this.stepRejected = true;
        //     this.stepRejectedMessage = this.stepsData[i]['rejectedMessage'];
        //     this.selectedstep = this.stepsData[i].stepId;
        //     this.temp = this.stepsData[i];
        //     break;
        //   }else {
        //     if (this.stepsData[i].status != 1) {
        //       this.selectedstep = this.stepsData[i].stepId;
        //       this.temp = this.stepsData[i];
        //       console.log(this.temp);
        //       // this.temp.emit(this.stepsData[ this.selectedstep]);
        //       break;
        //     }
        //     this.selectedstepindex = i;
        //   }

        //   console.log(this.selectedstep);
        // }

        for (let i = 0; i < this.stepsData.length; i++) {
          if(this.stepsData[i].status == 0){
            this.stepRejected = true;
            this.stepRejectedMessage = this.stepsData[i]['rejectedMessage'];
            this.selectedstep = this.stepsData[i].stepId;
            this.temp = this.stepsData[i];
            this.selectedstepindex = i;
            this.goToNextStep(i, this.stepsData[i]);
            break;
          }else {
            if (this.stepsData[i].status != 1) {
              this.selectedstep = this.stepsData[i].stepId;
              this.temp = this.stepsData[i];
              this.selectedstepindex = i;
              console.log(this.temp);
              this.goToNextStep(i, this.stepsData[i]);
              // this.temp.emit(this.stepsData[ this.selectedstep]);
              break;
            }
            this.selectedstepindex = i;
            if(i === this.stepsData.length - 1){
                this.goToNextStep(i - 1 , this.stepsData[i]);
            }


          }

          console.log(this.selectedstep);
        }

        console.log('TTT nomination', this.stepsData);
      })
      .catch(result => {
        this.spinner.hide();
        console.log('RESULT tttNomination Error===>', result);
      });
  }
  selectedstepindex: any;
  stetdetail(stepIndex, step) {
    this.temp = {};
    this.cdf.detectChanges();
    console.log('Old Step id ===>', step.stepId);
    console.log('Old Step id current ===>', this.selectedstep);
    this.selectedstep = null;
    // if (step.stepId == 10) {
    //   if (step.status == 1) {
    //     this.mockfeedback(step);
    //   }
    // } if (step.stepId == 11) {
    //   if (step.status == 1) {
    //     this.mockfeedback(step);
    //   }
    // } if (step.stepId == 14) {
    //   if (step.status == 1) {
    //     this.mockfeedback(step);
    //   }
    // }
    // setTimeout(() => {

    this.selectedstepindex = stepIndex;
    if (step && step['status'] === 0) {
      this.stepRejected = true;
      this.stepRejectedMessage = step['rejectedMessage'];
      this.selectedstep = step.stepId;
      // this.selectedstep = step.stepId;
      // this.cdf.detectChanges();
      // this.temp = step;
      return null;
    } else {
      this.stepRejected = false;
      this.stepRejectedMessage = '';
    }
    console.log(stepIndex, step);
    // this.selectedstep = step.stepId;
    this.temp = step;

    this.selectedstep = step.stepId;
    this.cdf.detectChanges();
    console.log('New Step id ===>', this.selectedstep);
    // if (step.stepId == 2) {
    //   if (step.status == 1) {
    //     this.toastr.success(
    //       'BH Approval is completed.',
    //       'Completed'
    //     );
    //   } else if (step.status == 2) {
    //     this.toastr.warning(
    //       'BH Approval is pending.',
    //       'Warning!!!'
    //     );
    //   } else if (step.status == 3) {
    //     this.toastr.warning(
    //       'Please complete first step and wait for the approval.',
    //       'Warning!!!'
    //     );
    //   }
    // }
    // this.temp.emit(step);
    console.log(this.temp);
    // console.log(temp);
    // if (step.stepId == 12) {
    //   if (step.status == 1) {
    //     // this.fetchPointsData();
    //     // this.clickToDownload();
    //   }
    // }
    if (step.stepId == 9) {
      // if (step.status == 1) {
        this.batchcontent(step);
      // }
    }
    // if (step.stepId == 10) {
    //   if (step.status == 1) {
    //     // this.mockfeedback(step);
    //   }
    // }
    if (step.stepId == 11) {
      if (step.status == 1) {
        // this.mockfeedback(step);
      }
      if (step.stepId == 9) {
        // if (step.status == 1) {
          this.batchcontent(step);
        // }
      }
      // this.cdf.detectChanges();
      // }, 100);
    }
    if(step.stepId == 4) {
      if (step.status == 2 || step.status == 1) {
        if (step.webStatus === 0){
          // this.checkForWebinarSlot();
        }
      }
    }

  }
  onVariableChangeProperty(item) {
    if (!item) return item;

    const json = JSON.stringify(item);
    const base64 = btoa(json);
    return JSON.parse(atob(base64));
  }
  batchcontent(stepData) {
    const data = {
      wfId: this.workflowdetail.id,
      empId: this.employeeId,
      tId: this.userDetails.tenantId
    };
    console.log(data);
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.tttselectedbatch;
    this.tttServices.get(url, data).then(
      res => {
        console.log(res);
        this.spinner.hide();
        try {


          if (res['data'] && res['data'].length > 0) {
            // this.batchdata = res.data[0].list[0];
            this.batchdetail = res['data'][0];
            if (stepData.status === 2 && this.batchdetail.self === 0) {
              this.batchdetail['msg'] = this.batchdetail.msg;
              // this.presenttoast(msg);
            } else if (stepData.status === 2 && this.batchdetail.self === 1) {
              // const msg = this.batchdata.msg;
              // this.presenttoast(msg);
              // this.goToCourses('SELF');
              this.batchdetail['defaultMsg'] = this.batchdetail.msg;
              // this.presentalert1(this.batchdata, 0, '');
            } else {
               this.batchdetail.startdate = this.formdate(
                  this.batchdetail.validFromDate
              );
                   this.batchdetail.enddate = this.formdate(
                  this.batchdetail.validToDate
                 );
              // this.batchdata.startdate = this.formdate(this.batchdata.validFromDate);
              // this.batchdata.enddate = this.formdate(this.batchdata.validToDate);
              // console.log(this.batchdata);
              // const msgdata = this.batchdata;
              // this.batchdata['defaultMsg'] = 'You have enrolled for ' + ''' + msgdata.fullname + ''' +' batch and it will start from ' + msgdata.startdate + ' at ' + msgdata.venueName + ' location.';
              // this.presentalert1(this.batchdata, 0, '');
            }
          }
        } catch (e) {
          console.log(e);
        }
      },
      err => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  mockfeedback(data) {
    this.count = [];
    this.spinner.show();
    const param = {
      empId: this.employeeId,
      tId: this.userDetails.tenantId,
      activityId: data.activityId
    };
    const url = ENUM.domain + ENUM.url.getfeedbackscore;
    this.tttServices.get(url, param).then(
      res => {
        console.log(res);
        if (res['type'] === true) {
          try {
            this.count = res['data'][0].cnt;
          } catch { }
        }
        this.spinner.hide();
      },
      err => {
        this.spinner.hide();
        console.log();
      }
    );
  }
  formdate(date) {
    if (date) {
      var formatted = this.datePipe.transform(date, 'dd-MMM-yyyy');
      return formatted;
    }
  }



  //@v.s.a // here we redict to book page to book evaluation call
  gotoCalender(action) {
    var dData = {
      actionType: action, // this is because it is for evaluation call
      instanceId: this.workflowdetail.id, //here we pass the workflowId
      tId: this.userDetails.tenantId,
    };
    this.CalSP.data = dData; // we asign  the data to servive data
    this.router.navigate(['../book-slot'], { relativeTo: this.routes }); // we route to the book-slot page
  }

  showWebinarActivity: boolean = false;
  checkForWebinarSlot() {
    if (localStorage.getItem('userdetail') && localStorage.getItem('employeeId')) {
      const uD = JSON.parse(localStorage.getItem('userdetail'));
      const employeeId = localStorage.getItem('employeeId');
      const params = {
        wfId: this.workflowdetail.id,
        // wfId: 1,
        empId: employeeId,
        tId: uD.tenantId,
      };
      const url = ENUM.domain + ENUM.url.getTrainerForWebinar;
      this.tttServices
        .getTrianers(url, params)
        .then((result: any) => {
          console.log('DATA--->', result);
          if (result.type == true) {
            if (result.data.length > 0) {
              this.showWebinarActivity = true;
            } else {
              this.showWebinarActivity = false;
            }
          } else {
            this.toastr.warning(
              'Please try again after sometime.',
              'Error Occured!'
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
    } else {
      this.toastr.warning(
        'Please login again',
        'Error Occured!'
      ).onHidden.subscribe(()=>
      this.toastr.clear());
    }
  }

  refreshSteps(){
    this.getAllSteps(false);
  }
  goToSelfLearn(){
    this.router.navigate(['/pages/learn/'], { queryParams: { 'courseType': 'OPEN' } });
  }

  // Popup Logic

  // popupShowHide(event){
  //   this.tttShowInstructionPopup = event;
  // }

  goToNextStep (stepIndex , step) {
    // this.currentStepIndexClicked = stepIndex;
    // this.currentStepClicked = step;
    this.stetdetail(stepIndex, step);
    // this.popupShowHide(true);
  }

  acceptedGoToNext() {
    // this.stetdetail(this.currentStepIndexClicked, this.currentStepClicked);
    // this.popupShowHide(false);
  }
}
