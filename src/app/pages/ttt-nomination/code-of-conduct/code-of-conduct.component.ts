import { Component, OnInit, Output, EventEmitter, ViewEncapsulation, OnChanges, Input } from '@angular/core';
import { TrainTheTrainerServiceProvider } from './../../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from '../../../service/enum';
import { TTTNominationComponent } from '../ttt-nomination.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'code-of-conduct',
  templateUrl: './code-of-conduct.component.html',
  styleUrls: ['./code-of-conduct.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CodeOfConductComponent implements OnInit, OnChanges {
  @Output() public passData = new EventEmitter();
  @Input() response_data;
  public flagData = 3;
  trainerCodeOfConduct: any = [];
  selectedProgram: any;
  response: any;
  programs: any = [
    // { value : 1, name: 'Program 1'},
    // { value : 2, name: 'Program 2'},
    // { value : 3, name: 'Program 3'},
    // { value : 4, name: 'Program 4"},
  ];
  userDetails: any;
  tenantId: any;
  employeeId: any;
  userId: any;
  wfresonseId: any;
  stepValue: any;
  agreeAndDisAgreeButtonStatus: boolean = false;
  workflowdetail: any;
  stepsData: any = [];
  selectedIndex: any;
  disableTab = false;
  isdisabled: boolean;

  constructor(
    private tttServiceProvider: TrainTheTrainerServiceProvider,
    public spinner: NgxSpinnerService,
    public tttComponent: TTTNominationComponent, private toastr: ToastrService,
  ) {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userId = this.userDetails.id;
    this.tenantId = this.userDetails.tenantId;
    this.employeeId = localStorage.getItem('employeeId');
    this.workflowdetail = this.tttServiceProvider.workflow;
    console.log('employeeId', this.employeeId);
    console.log('this.userDetails', this.userDetails);
    this.getAllPrograms();
    this.getCocData();
    this.getAllSteps();
    this.stepValue = tttComponent.stepsData;
  }

  ngOnInit() {
  }

  getAllPrograms() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.getTttPrograms;
    const param = {
      'tId': this.tenantId,
    }
    this.tttServiceProvider.getAllProrams(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT program Success===>', result);
      console.log('result.data[0]', result.data[0]);
      this.programs = result.data;
      if(this.programs && this.programs.length !== 0){
        this.response = this.programs[0].responseType;

      }
      console.log('programs', this.programs);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT program Error===>', result);
    })
  }
  selectedProgramId: any;
  getCocData() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.getCocProfileData;
    const param = {
      'wfId': this.workflowdetail.id,
      'empId': this.employeeId,
      'tId': this.tenantId,
    };
    this.tttServiceProvider.getCocProfileData(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT COC Success===>', result);
      console.log('result.data', result.data[0]);
      if(result.data[0].responseType === 1){
        this.flagData = 2;
        // this.isdisabled = true;
      } else if(result.data[0].responseType === 0){
        this.flagData = 0;
        // this.isdisabled = false;
      }
      if(result.data[0].responseType){
        this.disableTab = result.data[0].responseType;
        if (result.data[0].responseType != null){
          let dataPassed = {
            selectedIndex: 2,
            disabled: false,
            resetStep: false,
          }
          this.passData.emit(dataPassed);
        }
      }
      var imageExts = new Array('.png', '.jpeg', '.jpg');
      var fileExts = new Array('.pdf');

      this.wfresonseId = result.data[0].wfrId;
      const fooCocName = result.data[0].secName.split('|');
      const fooCocDesc = result.data[0].secDesc.split('|');
      const fooConRef = result.data[0].contentRef.split('|');
      for (let i = 0; i < fooCocName.length; i++) {
        const fooObj = {
          secName: fooCocName[i],
          secDesc: fooCocDesc[i],
          contentRef: fooConRef[i] == 0 ? null : fooConRef[i],
          fileExt: fooConRef[i].substring(fooConRef[i].lastIndexOf('.'))
        };
        if (imageExts.indexOf(fooObj.fileExt) < 0) {
        } else {
          fooObj['mimeType'] = 'image';
        }
        if (fileExts.indexOf(fooObj.fileExt) < 0) {
        } else {
          fooObj['mimeType'] = 'application';
        }
        this.trainerCodeOfConduct.push(fooObj);
      }

      this.selectedProgramId = result.data[0].programId;
      this.selectedProgram = result.data[0].programName;
      console.log('this.trainerCodeOfConduct', this.trainerCodeOfConduct);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });
  }

  agreeDisagreeCOC(program, status) {
    console.log("enters",status)
    // if(status == 1){
    //   // this.isdisabled = true
    //   this.flagData = 2;
    // }else{
    //   // this.isdisabled = false
    //   this.flagData = 0;
    // }
    if (this.response_data.status === 3) {
      const url = ENUM.domain + ENUM.url.insertWorflowIntroResponse;
      const param = {
        'wfId': this.workflowdetail.id,
        'empId': this.employeeId,
        'pgmId': program,
        'resType': status ? 1 : 0,
        'tId': this.tenantId,
        'userId': this.userId,
        'stepId': 1,
      };
      console.log('param', param);

      this.tttServiceProvider.insertWorflowIntroResponse(url, param).then((result: any) => {
        this.spinner.hide();
        if (result.type == true) {
          this.toastr.success(result.data[0].msg, "Success!").onHidden.subscribe(()=>
          this.toastr.clear());
          this.agreeAndDisAgreeButtonStatus = false;
          // selectTab(index: number): void {
          if (result.data[0].action === 1) {
            this.selectedIndex = 1;
            let dataPassed = {
              selectedIndex: 1,
              disabled: false,
              resetStep: false,
            }
            this.flagData = 2;
            this.passData.emit(dataPassed);
          } else {
            let dataPassed = {
              disabled: true,
              selectedIndex:2,
              resetStep: true,
            }
            this.passData.emit(dataPassed);
            this.flagData = 0;
          }
          // }
        } else {
          this.toastr.warning(result.data[0].msg, "Error!").onHidden.subscribe(()=>
          this.toastr.clear());
        }
        console.log('Insert Result', result);
      }).catch(result => {
        this.spinner.hide();
        console.log('Insert Result Error===>', result);
      });

    }


  }


  getAllSteps() {
    const param = {
      wfId: this.workflowdetail.id,
      tId: this.tenantId,
      empId: this.employeeId,
    };


    const url = ENUM.domain + ENUM.url.getAllWorkFlowSteps;

    this.tttServiceProvider.getAllWorkFlowSteps(url, param).then((result: any) => {
      this.spinner.hide();
      this.stepsData = result.data;

      // console.log('Status step value', this.stepsData[0].status);
      // console.log('Status step value', this.stepsData[0].stepId);
      // console.log('Status step value', this.stepsData[0].displayName);
      if (this.stepsData[0].status == 3) {
        this.agreeAndDisAgreeButtonStatus = true;
      } else {
        this.agreeAndDisAgreeButtonStatus = false;
      }
      // if (this.stepsData[0].status == null) {
      //   this.agreeAndDisAgreeButtonStatus = true;
      // }

      console.log('Steps Value', this.stepsData);
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT tttNomination Error===>', result);
    });



  }

  ngOnChanges() {
    console.log("Data ==>", this.response_data);
    // if (this.response_data.status == 2) {
    //   this.flagData = 2;
    // } else if (this.response_data.status == 0){
    //   this.flagData = 0;
    // }
  }

  safeHtmlBind(){

  }
}
