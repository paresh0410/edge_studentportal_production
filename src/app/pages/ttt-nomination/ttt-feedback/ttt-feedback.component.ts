import { Component, OnInit, ViewEncapsulation, Input, OnChanges, SimpleChanges,EventEmitter} from "@angular/core";
import { TrainTheTrainerServiceProvider } from "../../../service/traine-the-trainer.service";
import { ENUM } from "../../../service/enum";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "ttt-feedback",
  templateUrl: "./ttt-feedback.component.html",
  styleUrls: ["./ttt-feedback.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TttFeedbackComponent implements OnInit, OnChanges {

  @Input('step') step1;
  @Input('stepId') stepId;
  feedback: any = [];
  average:any;

  workflowdetail: any = [];
  userDetails: any = [];
  employeeId: any;
  stepsdata: any = [];
  currentstepdata: any = [];
  constructor(public ttt: TrainTheTrainerServiceProvider, private spinner: NgxSpinnerService) {
    this.workflowdetail = this.ttt.workflow;
    this.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    this.employeeId = localStorage.getItem("employeeId");
    // if (this.workflowdetail) {
    //   this.fbbyttrainer();
    // }
  }

  ngOnInit() {
    console.log(this.stepsdata);
    console.log(this.currentstepdata);
    // this.fbbyttrainer();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    console.log('stepData' , this.step1);
    if(changes.stepId.previousValue !== changes.stepId.currentValue){
      this.currentstepdata = this.step1;
      this.fbbyttrainer();
    }
  }
  back() {
    window.history.back();
  }


  fbbyttrainer() {
    if(this.currentstepdata && this.currentstepdata != {}){
      this.spinner.show();
      const data = {
        wfId: this.workflowdetail.id,
        actId : this.currentstepdata.activityId,
        empId: this.employeeId,
        tId: this.userDetails.tenantId,
        stepId: this.stepId,
      };
      const url = ENUM.domain + ENUM.url.evalution_fb_for_learner;

      this.ttt.get(url, data).then(
        res => {
          this.spinner.hide();
          try {
            console.log(res);
            if (res["type"] == true) {
              try {
                this.feedback = res["data"];
                if(res["avg"].length !=0){
                  this.average =   res['avg'][0];
                }
                for (let i = 0; i < this.feedback.length; i++) {
                  this.feedback[i].id = i + 1;
                  if(this.feedback[i].value == null){
                    this.feedback[i].value = 1;
                  }
                }
              } catch { }
            }
          } catch (e) {
            console.log(e);
          }
        },
        err => {
          this.spinner.hide();
          console.log(err);
        }
      );
    }

  }
}
