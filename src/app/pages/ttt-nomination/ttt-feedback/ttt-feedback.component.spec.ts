import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttFeedbackComponent } from './ttt-feedback.component';

describe('TttFeedbackComponent', () => {
  let component: TttFeedbackComponent;
  let fixture: ComponentFixture<TttFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
