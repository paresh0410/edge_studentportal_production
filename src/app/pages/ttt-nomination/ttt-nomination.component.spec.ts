import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TTTNominationComponent } from './ttt-nomination.component';

describe('TTTNominationComponent', () => {
  let component: TTTNominationComponent;
  let fixture: ComponentFixture<TTTNominationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TTTNominationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TTTNominationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
