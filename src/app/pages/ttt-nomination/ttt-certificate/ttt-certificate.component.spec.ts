import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttCertificateComponent } from './ttt-certificate.component';

describe('TttCertificateComponent', () => {
  let component: TttCertificateComponent;
  let fixture: ComponentFixture<TttCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
