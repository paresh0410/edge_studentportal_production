import { Component, OnInit, Input, OnChanges, ViewEncapsulation } from '@angular/core';
import { TrainTheTrainerServiceProvider } from './../../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from '../../../service/enum';
@Component({
  selector: 'ngx-ttt-certificate',
  templateUrl: './ttt-certificate.component.html',
  styleUrls: ['./ttt-certificate.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TttCertificateComponent implements OnInit, OnChanges {
  @Input() currentStepData;
  currentStageData;
  certificateDownloadUrl: string;
  employeeId;
  workflowdetail;
  certificateContent;
  showTable = false;
  showTotal = false;
  showDownloadCertificate = false;
  certTab: any;
  userDetails: any = [];
  constructor(private tttServiceProvider: TrainTheTrainerServiceProvider, public spinner: NgxSpinnerService) {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.employeeId = localStorage.getItem('employeeId');
    this.workflowdetail = this.tttServiceProvider.workflow;
    console.log('employeeId', this.employeeId);
    console.log('this.userDetails', this.userDetails);
  }

  ngOnInit() {
  }
  ngOnChanges() {
    this.currentStageData = this.currentStepData;
    this.fetchPointsData();
  }

  fetchPointsData() {
    const data = {
      tId: this.userDetails.tenantId,
      wfId: this.workflowdetail.id,
      empId: this.employeeId,
    };
    console.log(data);
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.getTTTCertificate;
    this.tttServiceProvider.getStepPoints(url, data).then(res => {
      console.log(res);
      this.certificateContent = res;
      if (this.certificateContent['alltab'].length !== 0) {
        this.showTable = true;
      }
      if (this.certificateContent['certTab'] !== null) {
        this.certTab = this.certificateContent['certTab'];
        if (this.certTab.certContent === 1 && this.certTab.isWfComp === 1) {
          this.showDownloadCertificate = true;
          this.clickToDownload();
        }
      }
      if (this.certificateContent['totalTab'] !== null) {
        let data = this.certificateContent['totalTab'];
        if (data.points !== 0) {
          this.showTotal = true;
        }
      }
      this.spinner.hide();

    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  clickToDownload() {
    let url = ENUM.domain + ENUM.url.getTTTCertificateContent + '?';
    const param = {
      eId: this.employeeId,
      wfId: this.workflowdetail.id,
      tId: this.userDetails.tenantId,
      points: this.certificateContent['totalTab'].points,
      type: 'web',
    };
    // const param = {
    //   uId: 11,
    //   wfId: 1,
    //   tId: 1,
    //   type: "web"
    // };
    const String =
      "empId=" +
      param.eId +
      "&wfId=" +
      param.wfId +
      "&tId=" +
      param.tId +
      "&points=" +
      param.points +
      "&type=web";
    url = url + String;
    // console.log("URL===>", url);
    // location.href = url;
    this.certificateDownloadUrl = url;
    // this.tttServices.getCertificateFromUrl(url)
    // .then((res: any)=>{
    // 	// this.blob = new Blob([res], {type: 'application/pdf'});
    // 	console.log('Certificate Downloaded for Course ', data.id);
    // })
  }

}
