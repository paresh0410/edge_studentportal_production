import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PollServiceProvider } from '../../service/poll-service';
import { SurveyServiceProvider } from '../../service/survey-service';
import { DashboardServiceProvider } from '../../service/dashboard.service';
import { ENUM } from '../../service/enum';
import { CourseTypeCardViewerService } from '../course-type-card/course-type-card.service';
// import { leaderboardViewerService } from '../leader-board/leader-board.service';
import { EarnedBadgesViewerService } from '../earned-badges/earned-badges.service';
import { EarnedCertificateViewerService } from '../certificates/certificates.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TrainTheTrainerServiceProvider } from '../../service/traine-the-trainer.service';
import { DatePipe } from '@angular/common';
import { EEPService } from '../../service/eep.service';
import { CallCoachingService } from '../../service/call-coaching.service';
import { parse } from 'querystring';
import { BrandDetailsService } from '../../service/brand-details.service';
import { ToolsService } from '../../service/tools.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit {
  userName = 'Amanda';
  currentBrandData: any;
  path: 'assets/images/surveyIcon.png';
  sSum: any;
  survey: any = [];
  pSum: any;
  poll: any = [];
  selectedItem: any;
  selectedIndex: any;
  learnerDetails: any;
  Wdata: any = [];
  callCoaching: any = [];
  eepdata: any = [];
  ccData: any = [];
  tools: any = [];
  hideback: boolean = false;
  ttee: any = [];
  setdata: any;
  title: any;
  callIcon: boolean = false;
  tttworkFlowIcon: boolean = false;
  eepIcon: boolean = false;
  toolsIcon: boolean = false;
  dontShowSide: boolean = false;
  tenantData;
  tenantName: string;
  newsdeptlist: any = [];
  newstenlist: any = [];
  newslist: any = [];
  newslistFlag: boolean;
  /**
   * API data loading
   */
  isSurvey: boolean = false;
  isLeaderboard: boolean = false;
  isEarnBadge: boolean = false;
  isCourseType: boolean = false;
  isPoll: boolean = false;
  isCertificate: boolean = false;
  employeeId: any;
  userDetails: any;
  programObject: any;
  goTOTeamDashboard() {
    this.router.navigate(['team-dashboard'], { relativeTo: this.routes });
  }
  userRankData: any;
  gotoSurvey(data) {
    this.SSP.setPollSummary(data);
    this.router.navigate(['survey'], { relativeTo: this.routes });
  }

  gotoPoll(data) {
    this.PSP.setPollSummary(data);
    this.router.navigate(['poll'], { relativeTo: this.routes });
  }

  scoreCard = [
    { points: '<strong class="showGreen">12</strong>', status1: 'Performers' },
    {
      points: '<strong class="showRed">20</strong>',
      status1: 'Need Attention'
    },
    { points: '<strong class="showLightRed">32</strong>', status1: 'Reporting' }
  ];

  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private PSP: PollServiceProvider,
    private SSP: SurveyServiceProvider,
    private DSP: DashboardServiceProvider, private datePipe: DatePipe,
    private CTC: CourseTypeCardViewerService,
    // private LBS: leaderboardViewerService,
    private tools_service: ToolsService,
    private EBS: EarnedBadgesViewerService, private ttt: TrainTheTrainerServiceProvider,
    private EEP: EEPService, private callservice: CallCoachingService,
    private ECS: EarnedCertificateViewerService, private cdf: ChangeDetectorRef,
    public brandService: BrandDetailsService,
  ) { }

  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.dashboarddetail();
    this.surveylist();
    this.polllist();

    this.newsAndAnnouncement(this.userDetails, 0);
  }


  dashboarddetail() {
    const param1 = {
      empId: this.employeeId,
      tenantId: this.userDetails.tenantId,
    };

    const url4 = ENUM.domain + ENUM.url.getLearnerDetails;
    this.DSP.getLearnerDetails(url4, param1)
      .then((result: any) => {
        this.learnerDetails = result;
        if (this.learnerDetails) {
          const manData = this.learnerDetails.formattedData.mandatoryCourses || [];
          const opData = this.learnerDetails.formattedData.openCourses || [];
          const lbData = this.learnerDetails.formattedData.leaderbord || [];
          const bData = this.learnerDetails.formattedData.badges || [];
          const cData = this.learnerDetails.formattedData.certificates || [];
          this.Wdata = this.learnerDetails.formattedData.workflow || [];
          this.eepdata = this.learnerDetails.formattedData.EEPworkflow || [];
          for (let i = 0; i < this.Wdata.length; i++) {
            this.Wdata[i].sdate = this.formatDate(this.Wdata[i].startDate);
            this.Wdata[i].edate = this.formatDate(this.Wdata[i].endDate);
          }
          for (let i = 0; i < this.eepdata.length; i++) {
            this.eepdata[i].sdate = this.formatDate(this.eepdata[i].startDate);
          }
          this.ccData = this.learnerDetails.formattedData.coaching || [];
          this.newslist = this.learnerDetails.formattedData.News_announcement || [];
          this.tools = this.learnerDetails.formattedData.Tools || [];
          this.userRankData = this.learnerDetails.formattedData.userRank[0];
          console.log(this.userRankData);
          console.log("learnerDetails--->", this.learnerDetails);
          // console.log("tools--->", this.tools[0].cnt);
          console.log("NEWSLIST", this.newslist);

          this.programObject = {
            'TTT Workflows': this.Wdata,
            'EEP Data': this.eepdata,
            'CC Data' : this.ccData,
            'Tools': this.tools,
          };


          // this.checkSideMenuExist();
          // if (this.newslist.length != 0) {
          //   this.newslistFlag = true;
          // }
          // else {
          //   this.newslistFlag = false;
          // }

          if (this.newslist.length !== 0 && this.newslist[0]['newsExist'] !== 0 ) {
            this.newslistFlag = true;
          }
          else {
            this.newslistFlag = false;
          }

          // console.log('opData--->',opData);
          this.CTC.setLearnData(manData, opData);
          this.isCourseType = true;
          // this.LBS.setleaderboardData(lbData, this.userRankData);
          // this.LBS.setleaderboardData(lbData.);
          this.EBS.setBadgesData(bData);
          this.isEarnBadge = true;
          this.ECS.setCertificateData(cData);
          this.isCertificate = true;
          // this.show_carousal = true;
          this.cdf.detectChanges();
        } else {
          this.isCourseType = true;
          this.isEarnBadge = true;
          this.isCertificate = true;
        }
      })
      .catch(result => {
        this.isCourseType = true;
        this.isEarnBadge = true;
        this.isCertificate = true;
        console.log("RESULT===>", result);
      });
  }

  polllist() {
    const param1 = {
      EmpId: +this.employeeId,
      tId: this.userDetails.tenantId,
    };
    const url1 = ENUM.domain + ENUM.url.getPollUser;
    this.PSP.getPoll(url1, param1)
      .then((result: any) => {
        console.log('Poll Data===>', result);
        if (result.type == true) {
          this.poll = result.data;
          this.pSum = result.data[0];
          this.isPoll = true;
        }
        else {
          this.isPoll = true;
          console.log('no data found');
        }
      })
      .catch(result => {
        // console.log('RESULT===>',result);
        this.isPoll = true;
      });

  }

  surveylist() {
    const param1 = {
      EmpId: +this.employeeId,
      tId: this.userDetails.tenantId,
    };
    const url3 = ENUM.domain + ENUM.url.getSurveyUser;
    this.PSP.getPoll(url3, param1)
      .then((result: any) => {
        console.log('SURVEY===>', result);
        if (result.type == true) {
          this.survey = result.data;
          for (let i = 0; i < this.survey.length; i++) {
            this.survey[i].path = './assets/images/surveyIcon.png';
          }
          this.sSum = result.data[0];
          this.isSurvey = true;
        } else {
          this.isSurvey = true;
          console.log('No data found');
        }
      })
      .catch(result => {
        // console.log('RESULT===>',result);
        this.isSurvey = true;
      });
  }

  formatDate(date) {
    let d = new Date(date);
    let formatted = this.datePipe.transform(d, "dd-MMM-yyyy");
    return formatted;
  }
  gotoworkflow(data) {
    if (this.setdata === "ttt") {
      this.ttt.workflow = data;
      console.log("NOMINATION WORKFLOW ==>", this.ttt.workflow);
      this.router.navigate(['../nomination'], { relativeTo: this.routes });
    } else {
      this.EEP.workflow = data;
      console.log("NOMINATION WORKFLOW ==>", this.EEP.workflow);
      this.router.navigate(['../eep-step'], { relativeTo: this.routes });
    }
  }

  // gotoeepworkflow(data) {
  //   this.EEP.workflow = data;
  //   console.log("NOMINATION WORKFLOW ==>", this.EEP.workflow);
  //   this.router.navigate(['../eep-step'], { relativeTo: this.routes });
  // }
  // gotocc() {
  //   this.callservice.frompage = 'learner';
  //   this.router.navigate(['../call-coaching-list'], { relativeTo: this.routes });
  // }
  openNav() {
    document.getElementById('mySidenav').style.width = '120px';
  }
  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }
  gotopopup(data) {
    console.log('Popup data', data);
    // this.setdata = data;
    // if (this.setdata === 'ttt') {
    //   this.ttee = this.Wdata;
    //   this.title = 'TTT Workflow';
    // } else if (this.setdata === 'eep') {
    //   this.ttee = this.eepdata;
    //   this.title = 'EEP Workflow';
    // } else {
    //   this.router.navigate(['../section'], { relativeTo: this.routes });
    // }
    // console.log('ttee', this.ttee);
    //
    switch(data.identifier){
      case 'TTT': this.ttee = this.Wdata;
                  this.setdata = 'ttt';
                  this.title = 'TTT Programs';
                  break;
      case 'Tools': this.router.navigate(['../section'], { relativeTo: this.routes });
                    this.tools_service.fromContent = false;
                   break;
      case 'Call_Coaching': this.callservice.frompage = 'learner';
                           this.router.navigate(['../call-coaching-list'], { relativeTo: this.routes });
                           break;
      case 'EEP': this.ttee = this.eepdata;
                  this.setdata = 'eep';
                  this.title = 'EEP Workflow';
                  break;
    }
    this.hideback = true;
  }
  closepop() {
    this.hideback = false;
    this.ttee = [];
  }
  // checkSideMenuExist() {
  //   if (this.ccData[1].cnt != 0) {
  //     this.callIcon = true;
  //   }
  //   if (this.tools[0].cnt != 0) {
  //     console.log('tools data ==>', this.tools[0]);
  //     this.toolsIcon = true;
  //   }
  //   if (this.Wdata.length != 0) {
  //     this.tttworkFlowIcon = true;
  //   }
  //   if (this.eepdata.length != 0) {
  //     this.eepIcon = true;
  //   }
  //   if (this.callIcon || this.tttworkFlowIcon || this.eepIcon || this.toolsIcon) {
  //     this.dontShowSide = true;
  //   }
  //   console.log('this.callIcon ==>', this.callIcon);
  //   console.log('this.tttworkFlowIcon ==>', this.tttworkFlowIcon);
  //   console.log('this.eepIcon ==>', this.eepIcon);
  //   console.log('this.toolsIcon ==>', this.toolsIcon);
  //   console.log('this.dontShowSide ==>', this.dontShowSide);
  //   this.cdf.detectChanges();
  // }
  newsAndAnnouncement(userDetails, index) {
    this.tenantName = userDetails.tenantName;
  }
  tabChanged(event) {
    console.log(event.index);
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.newsAndAnnouncement(userDetails, 1);
    // if(event.index)
  }
}
