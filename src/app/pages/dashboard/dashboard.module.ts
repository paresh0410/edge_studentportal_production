import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { NgxContentLoadingModule } from 'ngx-content-loading';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { DashboardComponent } from './dashboard.component';
import { CourseTypeCardComponent } from '../course-type-card/course-type-card.component';
import { SkillMeterComponent } from '../skill-meter/skill-meter.component';
import { LeaderBoardComponent } from '../leader-board/leader-board.component';
import { EarnedBadgesComponent } from '../earned-badges/earned-badges.component';
import { CertificatesComponent } from '../certificates/certificates.component';
import { MyPathwaysComponent } from '../my-pathways/my-pathways.component';
import { MyActivityComponent } from '../my-activity/my-activity.component';
import { FollowersComponent } from '../followers/followers.component';

import { DemoMaterialModule } from '../material-module';
import { OpenviduSessionModule } from 'openvidu-angular';
import { ReadMoreComponent } from '../../component/read-more/read-more.component';
// import { CourseTypeCardModule } from '../course-type-card/course-type-card.module';
// import { SkillMeterModule } from '../skill-meter/skill-meter.module';
// import { LeaderBoardModule } from '../leader-board/leader-board.module';
// import { EarnedBadgesModule } from '../earned-badges/earned-badges.module';
// import { CertificatesModule } from '../certificates/certificates.module';
// import { MyPathwaysModule} from '../my-pathways/my-pathways.module';
// import { MyActivityModule } from '../my-activity/my-activity.module';
// import { FollowersModule } from '../followers/followers.module';
import { from } from 'rxjs';
// import { BarRatingModule } from "ngx-bar-rating";


@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    Ng2CarouselamosModule,
    NgxContentLoadingModule,
    NgxHmCarouselModule,
    // OpenviduSessionModule
    // CourseTypeCardModule,
    // SkillMeterModule,
    // LeaderBoardModule,
    // EarnedBadgesModule,
    // CertificatesModule,
    // MyPathwaysModule,
    // MyActivityModule,
    // FollowersModule,

    // BarRatingModule
  ],
  declarations: [
    DashboardComponent,
    CourseTypeCardComponent,
    SkillMeterComponent,
    LeaderBoardComponent,
    EarnedBadgesComponent,
    CertificatesComponent,
    MyPathwaysComponent,
    MyActivityComponent,
    FollowersComponent,
    ReadMoreComponent,
  ],
  providers:[

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class DashboardModule {
}
