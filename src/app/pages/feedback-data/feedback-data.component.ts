import { Component, OnInit, ViewEncapsulation , ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { TrainerAutomationServiceProvider } from '../../service/trainer-automation.service';
import { ENUM } from '../../service/enum';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'feedback-data',
  templateUrl: './feedback-data.component.html',
  styleUrls: ['./feedback-data.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FeedbackDataComponent implements OnInit {
  Feedback_Data: any = [
    { participants: "Unmesh", Objective: 3, Content: 4, Trainer: 3, Overall: 4 },
    { participants: "Ashish", Objective: 2, Content: 5, Trainer: 4, Overall: 3 },
    { participants: "Aditya", Objective: 5, Content: 2, Trainer: 2, Overall: 4 },
    { participants: "Niket", Objective: 4, Content: 6, Trainer: 1, Overall: 2 },
    { participants: "Dinesh", Objective: 6, Content: 7, Trainer: 5, Overall: 1 },
    { participants: "Vivek", Objective: 2, Content: 3, Trainer: 6, Overall: 2 },
    ];

    displayedColumns3: string[] = ['participants', 'Objective', 'Content', 'Trainer', 'Overall'];
    feedback = new MatTableDataSource(this.Feedback_Data);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    Object = Object;
    feedbackData:any={};
    userDetails:any;
    tenantId:any;
  constructor(private TAServiceProvider : TrainerAutomationServiceProvider,private router: Router, private routes: ActivatedRoute) {
    if(this.TAServiceProvider.feedbackData){
      this.feedbackData = this.TAServiceProvider.feedbackData;
      console.log('this.feedbackData',this.TAServiceProvider.feedbackData);
    } 
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.tenantId = this.TAServiceProvider.tenantId;
    this.getFeedbackDataByFeed();
  }

  ngOnInit() {
  }

  noFeedbackData:boolean = false;
  objKeys:any=[];
  participants:any;
  getFeedbackDataByFeed(){
    let url = ENUM.domain + ENUM.url.getFeedbackData;
    let param = { 
      "reqFrom": "WEB",
      "feedId": this.feedbackData.feedbackId,
      "tId": this.tenantId,
      "courseId":this.feedbackData.courseId,
      "moduleId":this.feedbackData.moduleId
    }
    this.TAServiceProvider.getFeedbackData(url, param)
    .then((result:any)=>{
      console.log('RESULT Feedback data===>',result);
      if(result.data.length == 0){
        this.noFeedbackData = true;
      }else{
        this.objKeys = Object.keys(result.data[0]);
        console.log('objKeys',this.objKeys);
        this.noFeedbackData = false;
        this.participants = result.data;
      }
		}).catch(result=>{
			console.log('RESULT Error participant===>',result);
		})
  }

  goBack(){
    this.router.navigate(['../'], { relativeTo: this.routes });
  }
  makefeedback(question) {
    if(question){
      var fb = question.replace(/<[^>]+>/g, '');
      fb=fb.replace(/&nbsp;/g, '');
      if(fb.length > 15){
        return (fb.substring(0,15) + '...');
      }
      else{
        return fb;
      }
    }
  }
}
