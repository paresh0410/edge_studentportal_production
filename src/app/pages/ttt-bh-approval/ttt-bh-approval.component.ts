import { Component, OnInit } from '@angular/core';
import { ENUM } from '../../service/enum';
import { TrainTheTrainerServiceProvider } from '../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'ngx-ttt-bh-approval',
  templateUrl: './ttt-bh-approval.component.html',
  styleUrls: ['./ttt-bh-approval.component.scss']
})
export class TttBhApprovalComponent implements OnInit {

  nominees: any = [];
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: null
  //   },
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: 1
  //   },
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: null
  //   },
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: 2
  //   },
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: 1
  //   },
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: 2
  //   },
  //   {
  //     id:1,
  //     employeeName: 'Unmesh',
  //     department: 'Sales',
  //     status: 2
  //   }
  // ]
  employeeId:any;
  userDetails:any;
  listShow:boolean =false;
  constructor(public ttt: TrainTheTrainerServiceProvider,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService,) {
      this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
     }

  ngOnInit() {
    this.dcontent();
  }
  /*this function is for get all BH aprovalemp list*/
  dcontent() {
    try{
      const data = {
        empId: this.employeeId,
        tId: this.userDetails.tenantId,
      };
      this.spinner.show();
      console.log(data);
      const url = ENUM.domain + ENUM.url.get_BH_aproval_list;
      this.ttt.getAllAprocallist(url, data).then(res => {
        console.log(res);
        this.spinner.hide();
        try {
          this.nominees = res['data'];
          console.log('this.nominees',this.nominees);
      //    this.listShow = t
          // this.moduledetail(this.nomineeModules);
        } catch (e) {
          console.log(e);
          this.nominees=[];
        }
      }, err => {
        this.spinner.hide();
        console.log(err);
      });
    }catch(e)
    {
      console.log(e);
    }

  }

// this function is for update the status
  updateStatus(item,sts) {
    const datae = {
      appId:item.BHId,
      appStat:sts,
      appCmt:null,
      actId:0,
      userId:this.userDetails.id,
      wfId:item.workflowId,
      empId:item.empId,
      tId:this.userDetails.tenantId,
    };
    this.spinner.show();
    console.log(datae);
    const url = ENUM.domain + ENUM.url.ttt_update_BH_aproval;
    this.ttt.updateAprovalStatus(url, datae).then(res => {
      console.log(res);
      this.spinner.hide();
      try {
        var result =res['data'];
        console.log('result===>',result)
        this.toastr.success(result.msg, 'Success!').onHidden.subscribe(()=>
        this.toastr.clear());
        this.dcontent();
      } catch (e) {
        console.log(e);
        console.log(res['data']);
        // this.toastr.warning(res['data'], 'Server Warning!');
        this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
        this.toastr.clear());
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
      // this.toastr.warning(err, 'Server Warning!');
      this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
      this.toastr.clear());
    });
  }
}
