import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttBhApprovalComponent } from './ttt-bh-approval.component';

describe('TttBhApprovalComponent', () => {
  let component: TttBhApprovalComponent;
  let fixture: ComponentFixture<TttBhApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttBhApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttBhApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
