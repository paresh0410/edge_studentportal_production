import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { TrainerAutomationServiceProvider } from "../../service/trainer-automation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ENUM } from "../../service/enum";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: "ngx-level2",
  templateUrl: "./level2.component.html",
  styleUrls: ["./level2.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class Level2Component implements OnInit {
  level2Details: any = [ ];

  // noAssessData: boolean;
  noFeedData: boolean;
  // noFeedbackModule:boolean;
  level2data:any =[];
  objKeys: any = [];
  section:any =[];
  level1_data: any = [];
  userDetail: any = [];
  // assessmentData: any = [];
  Object = Object;
  avg_feedback: any;
  avg_Assess: any;
  programData: any;
  trainerId:any;
  displayDataOf:string;
  currentlyOpenedItemIndex = 1;
  constructor(
    private trainer_service: TrainerAutomationServiceProvider,
    private spinner: NgxSpinnerService,
    private router: Router,
    private routes: ActivatedRoute,
    private toaster: ToastrService,
  ) {
    this.level1_data = this.trainer_service.level1_Data;
    this.displayDataOf = this.trainer_service.show_data_on_level2;
    // console.log("Level 1 data >>", this.level1_data);
    // console.log("OBJ", Object.keys);
    // this.avg_feedback = this.level1_data.avg_feed;
    // this.avg_Assess = this.level1_data.avg_assess;
    this.userDetail = JSON.parse(localStorage.getItem("userdetail"));
    let userOtherDetails = JSON.parse(
      localStorage.getItem("userOtherDetails")
    );
    console.log("userOtherDetails", userOtherDetails);
    let userOthersDetails = userOtherDetails[1];

    for (let i = 0; i < userOthersDetails.length; i++) {
      if (userOthersDetails[i].roleId == 7) {
        this.trainerId = userOthersDetails[i].fieldmasterId;
      }
    }
    console.log("trainerId", this.trainerId);
    this.programData = this.level1_data.programName;
    // console.log("avg Feedback", this.avg_feedback);
    // console.log("avg assess", this.avg_Assess);
  }

  ngOnInit() {
    // this.getFeedbackData();
    // this.getFeedbackSection();
    // this.getAssessmentData();
    this.getEmployeeList();

  }

  // getFeedbackSection(){
  //   let url = ENUM.domain + ENUM.url.getFeedbackSection;
  //   let param = {
  //     'pgmId': this.level1_data.programId,
  //     'trnId': this.trainerId,
  //     'tId' : this.userDetail.tenantId,
  //   }
  //   this.trainer_service.getFeedbackSection(url, param)
  //     .then((result: any) => {
  //       this.spinner.hide();
  //       console.log('Level 1 getFeedbackSection ==>', result);
  //       if (result.data.length == 0) {
  //         // this.noAssessData = true;
  //         this.noFeedbackModule = true;
  //       } else {
  //         this.sectionArray = result.data;
  //         this.ActiveTab = this.sectionArray[0];
  //         this.activeTabIndex = 0;
  //         this.getFeedbackData(this.ActiveTab)
  //         console.log('Level 1 getFeedbackSection ==>',  this.sectionArray);
  //       }

  //     }).catch(result => {
  //       this.spinner.hide();
  //       console.log('Level 1 data Assessment scores  ==>', result);
  //     })
  // }

  // getFeedbackData(data) {
  //   this.spinner.show();
  //   let url = ENUM.domain + ENUM.url.getLevel2FeedbackData;
  //   let param = {
  //     trnId: this.trainerId,
  //     corsId: this.level1_data.course_id,
  //     'modeType': 'web',
  //     'secId':data.secId,
  //     // this.level1_data.courseId
  //   };
  //   this.trainer_service
  //     .getLevel2Feedback(url, param)
  //     .then((result: any) => {
  //       this.spinner.hide();
  //       console.log("Level 2 data feedback ===>", result);
  //       // this.level1Details = result.data;
  //       if (result.data.length == 0) {
  //         this.noFeedData = true;
  //       } else {
  //         this.objKeys = Object.keys(result.data[0]);
  //         console.log("objKeys", this.objKeys);
  //         this.noFeedData = false;
  //         this.level2Details = result.data;
  //         console.log("Level 2 details: ", this.level2Details);
  //       }
  //     })
  //     .catch(result => {
  //       this.spinner.hide();
  //       console.log("Level 2 data feedback ===>", result);
  //     });
  // }

  // getAssessmentData() {
  //   this.spinner.show();
  //   let url = ENUM.domain + ENUM.url.getLevel2AssessData;
  //   let param = {
  //     crsId: this.level1_data.course_id,
  //     tId: this.userDetail.tId,
  //     trnId: this.trainerId,
  //   };

  //   this.trainer_service
  //     .getLevel2Assess(url, param)
  //     .then((result: any) => {
  //       this.spinner.hide();
  //       console.log("Level 2 data Assessment scores ==>", result);
  //       // this.assessmentData = result.data;
  //       if (result.data.length == 0) {
  //         this.noAssessData = true;
  //       } else {
  //         // this.objKeys = Object.keys(result.data[0]);
  //         // console.log('objKeys',this.objKeys);
  //         this.noAssessData = false;
  //         this.assessmentData = result.data;
  //         console.log("Level 2 details: ", this.assessmentData);
  //       }
  //     })
  //     .catch(result => {
  //       this.spinner.hide();
  //       console.log("Level 2 data Assessment scores  ==>", result);
  //     });
  // }

  back() {
    this.router.navigate(["../"], { relativeTo: this.routes });
  }

  // tabChanged(data,index){
  //   this.ActiveTab = data;
  //   this.activeTabIndex = index;
  //   this.getFeedbackData(this.ActiveTab);
  // }

  getEmployeeList(){
    this.spinner.show();
      let url = ENUM.domain + ENUM.url.getLevel2CourseList;
      let param = {
        'trainId': this.trainerId,
        'cId':  this.level1_data.courseId,
        'tId': this.userDetail.tenantId,
        'iId': this.level1_data.instanceId,
        'flag': this.displayDataOf,
      };
      console.log('Params ==>',param);
      this.trainer_service.getLevel2(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('Employee List ===>', result);
        // this.level1data = result;
        // this.level1data = result;
        if(result.type){
          if(result.data.length != 0) {
            this.level2data = result.data;
            this.noFeedData = true;
          }
          else{
            // this.toaster.error('Error', 'No data found.');
          }
        }

        // this.level1Details = result.data;

      }).catch(result => {
        this.spinner.hide();
        console.log('Level 2 Eomployee List ===>', result);
        // this.toaster.error('Error', 'No Data Found');
      })
      this.spinner.show();
  }

  setOpened(item,itemIndex) {
    // this.feedback_section = this.level1data[itemIndex].sectionlist;
    // this.questionObjectKeys =
    // this.objKeys = Object.keys(result.data[0]);
    let url = ENUM.domain + ENUM.url.getLevel2QuestionList;
    let param = {
      // "pgmId": this.program_Data.id,
      'cId': this.level1_data.courseId,
      // "trainId": this.trainerId,
      "tId": this.userDetail.tenantId,
      'flag': this.displayDataOf,
      "iId": this.level1_data.instanceId,
      'empId': item.EmployeeId,
      'trainId': this.trainerId,
    }
    this.trainer_service.getLevel2(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('Level 1 data Question List ==>', result);
        // this.feedback_section = this.level1data[itemIndex].sectionlist;
        if(result.type){
          if(result.data.length != 0) {
            this.section = result.data;
          }
          else{
            // this.toaster.error('Error', 'No Data Found');
            this.section = [];
          }
        }

        this.currentlyOpenedItemIndex = itemIndex;
      }).catch(result => {
        this.spinner.hide();
        console.log('Level 1 data Assessment scores  ==>', result);
      })

  }

  setClosed(itemIndex) {
    if(this.currentlyOpenedItemIndex === itemIndex) {
      this.currentlyOpenedItemIndex = -1;
    }
  }
}
