import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthServiceProvider } from "../../service/auth-service";
import { ToastrService } from 'ngx-toastr';
import { BrandDetailsService } from '../../service/brand-details.service';
@Component({
  selector: "ngx-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent implements OnInit {
  email: string;

  forgotPwdForm: FormGroup;
  submitted = false;
  stepone: boolean = false;
  currentBrandData:any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private ASP: AuthServiceProvider,
    private toastr: ToastrService,
    public brandService: BrandDetailsService,
  ) {
    const id = document.getElementById("nb-global-spinner");
    id.style.display = "none";
  }

  ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.forgotPwdForm = this.formBuilder.group({
      email: ["", [Validators.required]]
    });
  }

  get f() {
    return this.forgotPwdForm.controls;
  } //form fields getter for easy access

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgotPwdForm.invalid) {
      return;
    } else {
      // console.log('ECN', this.email);
      const param = {
        ecn: this.email,
      };
      this.ASP.getToken(param)
      .then((result: any) => {
        if(result.type === true){
          localStorage.setItem('ECN', this.email);
          this.toastr.success(result.data1, 'Success!!').onHidden.subscribe(()=>
          this.toastr.clear());
          this.router.navigate(["../otp"], { relativeTo: this.routes });
        }else{
          this.toastr.warning(result.data1, 'Error!!').onHidden.subscribe(()=>
          this.toastr.clear());
        }
      }).catch(result=>{
        console.log('RESULT===>',result);
        this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
      this.toastr.clear());
			})
    }
  }
  // onSubmit(){
  //   this.router.navigate(["../otp"], { relativeTo: this.routes });
  // }


  goBack() {
    this.router.navigate(["../login-new"], { relativeTo: this.routes });
  }

  // checkPassword() {
  //   if(this.password !== this.confirm_password){
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }
  // onInput(value) {
  // if(this.checkPassword()){
  // //if both passwords are same set errors to null
  // this.f.confirm_password.setErrors(null);
  // this.passWrong = false;
  // }else{
  // //set error to confirm_pass manually
  // this.f.confirm_password.setErrors([{'passwordMatch': true}]);
  // this.passWrong = true;
  // }

  // }
}
