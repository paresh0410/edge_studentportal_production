import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { TrainerAutomationServiceProvider } from '../../service/trainer-automation.service';
import { ENUM } from '../../service/enum';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseServiceProvider } from '../../service/course-service';
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'attendance-data',
  templateUrl: './attendance-data.component.html',
  styleUrls: ['./attendance-data.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AttendanceDataComponent implements OnInit {

  Attendance_Data: any = [
    // { ecnNo: 111, name: "Unmesh", designation: "Manager", time: "9:20 pm", attended: false },
    // { ecnNo: 111, name: "Aditya", designation: "Developer", time: "9:20 pm", attended: false },
    // { ecnNo: 111, name: "Ashish", designation: "Manager", time: "9:20 pm", attended: false },
    // { ecnNo: 111, name: "Bhavesh", designation: "Developer", time: "9:20 pm", attended: false },
    // { ecnNo: 111, name: "Dinesh", designation: "Developer", time: "9:20 pm", attended: false },
    // { ecnNo: 111, name: "Niket", designation: "Developer",  time: "9:20 pm", attended: false },
  ];
  displayedColumns3: string[] = ['employeeId', 'name', 'designation', 'datetime', 'attStatus'];
  assessments = new MatTableDataSource(this.Attendance_Data);
  attendanceData: any = {};
  userDetails: any;
  tenantId: any;
  markAll: boolean = false;
  finalData: any;
  dataToUpadte: any = [];
  employeeId;
  empStr: string = '';
  searchString:string = '';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private TAServiceProvider: TrainerAutomationServiceProvider,
    private router: Router, public CSP: CourseServiceProvider,
    private routes: ActivatedRoute, private toast: ToastrService,
    private spinner: NgxSpinnerService) {
    this.TAServiceProvider.dataFromAttFeedAss = true;
    if (this.TAServiceProvider.attendanceData) {
      this.attendanceData = this.TAServiceProvider.attendanceData;
      console.log('this.TAServiceProvider.attendanceData', this.TAServiceProvider.attendanceData);
    }
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.tenantId = this.userDetails.tenantId;
    // this.getAllParticipantsAttendance();
    console.log('USHA PARTICIPANTS');
  }

  ngOnInit() {
    this.assessments.paginator = this.paginator;
    this.getAllParticipantsAttendance();
  }
  selectedAll = false;
  // markAllAttendance(selectionFlag) {
  //   console.log('nfckjnaknfs', selectionFlag);
  //   if (selectionFlag === 1) {
  //     this.selectedAll = true;
  //     this.Attendance_Data.forEach(element => {
  //       if (element.attStatus === 'N') {
  //         element.attStatus = 'Y';
  //       } else {
  //         element.attStatus = 'Y';
  //       }
  //     });
  //   } else if (selectionFlag === 0) {
  //     this.selectedAll = false;
  //     this.Attendance_Data.forEach(element => {
  //       if (element.attStatus === 'Y') {
  //         element.attStatus = 'N';
  //       } else {
  //         element.attStatus = 'N';
  //       }
  //     });
  //   }

  //   console.log('participant attendance', this.Attendance_Data);
  // }

  noParticipants: boolean = false;
  getAllParticipantsAttendance() {
    this.spinner.show();
    let url = ENUM.domain + ENUM.url.getAllParticipantAttendance;
    let param = {
      "reqFrom": "WEB",
      "participantCourseId": this.attendanceData.courseId,
      "moduleId": this.attendanceData.moduleId,
      "activityId": this.attendanceData.activityId,
      "trainId": this.TAServiceProvider.trainerId,
      "tId": this.tenantId,
      "lmt": null,
      "pNo": null,
      "string": this.searchString,

    }
    console.log('param', param);
    this.TAServiceProvider.getParticipantAttendance(url, param)
      .then((result: any) => {
        console.log('RESULT Success===>', result);
        if (result.data.length == 0) {
          this.noParticipants = true;
        } else {
          this.noParticipants = false;
          // for(let i=0;i<result.data;i++){
          //   this.Attendance_Data = this.formatTime()
          // }
          this.Attendance_Data = result.data;
          this.finalData = this.Attendance_Data;
          this.Attendance_Data.forEach((element) => {
            // console.log('Single Data ===>', element);
            Object.assign(element, { isSelected: false });
            // console.log('Updated Single Data ===>', element);
            if (element.datetime) {
              element.datetime = this.convertDateTime(element.datetime);
            }
          });
        }
        this.spinner.hide();
      }).catch(result => {
        console.log('RESULT Error===>', result);
        this.spinner.hide();
      });
  }

    convertDateTime(data) {
      return new Date(data.toLocaleString());
    }

  // insertDeleteAttendance(data,index){
  //   console.log('data',data);
  //   let url = ENUM.domain + ENUM.url.insertDeleteAttendance;
  //   let param = {
  //     "actionId": data.attStatus == 'N' ? 1 : 2,
  //     "iId": this.attendanceData.courseId,
  //     "modId": this.attendanceData.moduleId,
  //     "actId": this.attendanceData.activityId,
  //     "learnerId": data.employeeId,
  //     "trainId": this.TAServiceProvider.trainerId,
  //     "tId": this.tenantId,
  //     "userId": this.userDetails.id
  //   }
  //   console.log('param',param);
  //   this.TAServiceProvider.insertDeleteAttendance(url, param)
  //   .then((result:any)=>{
  //     console.log('RESULT Mark Success===>',result);
  //     if(result.type == true){
  //       if(this.Attendance_Data[index].attStatus == 'N'){
  //         this.Attendance_Data[index].attStatus = 'Y';
  //         this.Attendance_Data[index].datetime = result.data[0].datetime;
  //        // this.CSP.saveActivityCompletion(this.courseDetail);
  //       }
  //       else{
  //         this.Attendance_Data[index].attStatus = "N";
  //         this.Attendance_Data[index].datetime = null;
  //       }
  //     }else{

  //     }
  // 	}).catch(result=>{
  // 		console.log('RESULT Mark Error===>',result);
  // 	})


  // }


  markattendance(index) {
    if (this.Attendance_Data[index].attStatus == 'N') {
      this.Attendance_Data[index].attStatus = 'Y';
    }
    else {
      this.Attendance_Data[index].attStatus = "N";
    }
  }

  goBack() {
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  onClicked(value) {
    console.log('Clicked ===>');
    this.dataToUpadte = [];
    this.finalData.forEach((element) => {
      element.isSelected = !value;
    });
    console.log("Data to send ===>", this.finalData);
  }

  onClickedSingle(item, value) {
    this.finalData.forEach((element) => {
      // console.log('Single Data ===>', element);
      // Object.assign(element, {isSelected: false});
      // console.log('Updated Single Data ===>', element);
      if (element['employeeId'] == item.id) {
        element.isSelected = value;
      }
    });
    console.log("Data to send ===>", this.finalData);
  }

  submitFuction(value) {
    this.spinner.show();
    this.dataToUpadte = [];
    console.log('value', value);
    // this.selectedAll = true;
    console.log('Single Data ===>', this.finalData);
    this.markAll = false;
    this.finalData.forEach((element) => {
      if (value == 1) {
        if (element['attStatus'] == 'N') {
          let data = element;
          // data['attStatus'] = 'Y';
          this.dataToUpadte.push(data);
        }
      }
      if (value == 2) {
        if (element['attStatus'] == 'Y') {
          let data = element;
          // data['attStatus'] = 'Y';
          this.dataToUpadte.push(data);
        }
      }
    });
    console.log('Data To Send Data ===>', this.dataToUpadte);
    if (this.dataToUpadte.length == 0){
      this.toast.warning('Please Select Employees');
      this.spinner.hide();
    }else{
      this.empbind(this.dataToUpadte, response => {
        console.log('response', response);
        if (response) {
          console.log("Pipe Seperated Id ===>", response);
          let param = {
            // 'enrolId' :
            'actionId': value,
            'iId': this.attendanceData.courseId,
            'modId': this.attendanceData.moduleId,
            'actId': this.attendanceData.activityId,
            'allstr': response,
            'trainId': this.TAServiceProvider.trainerId,
            'tId': this.tenantId,
            'userId': this.userDetails.id,
          };
          console.log('param', param);
          let url = ENUM.domain + ENUM.url.insertDeleteAttendance;
          this.TAServiceProvider.insertDeleteAttendance(url, param).then(res => {
            console.log(res);
            this.toast.success('Data Updated Successfully');

            if (res['type'] === true) {
              this.getAllParticipantsAttendance();
            }
          }, err => {
            console.log(err);
            // this.toast.warning('Something went wrong.');
            this.spinner.hide();
          });
        }else {
          // this.toast.warning('Something went wrong.');
          this.spinner.hide();
        }
      });
    }
    // let url = ENUM.domain + ENUM.url.insertDeleteAttendance;

    //   console.log('param',param);
    //   this.TAServiceProvider.insertDeleteAttendance(url, param)
    // console.log("Data to send ===>", this.dataToUpadte);
  }
  empbind(data: any, cb) {
    console.log('bindDate', data);
    let finalStr = '';
    for (let i = 0; i < data.length; i++) {
      if (data[i].isSelected === true) {
        const str = data[i].enrolId + '|' + data[i].employeeId + '|' + data[i].userid + '|' + data[i].completionId;
        // if (finalStr === '') {
        //   finalStr = str;
        // } else {
        // }
        finalStr === '' ? finalStr = str : finalStr += '#' + str;
      }
    }
    console.log('finalStr', finalStr);
    cb(finalStr);
  }

  searchlist(searchString){
    console.log(searchString);
    this.searchString = searchString;
    this.getAllParticipantsAttendance();
  }
  clearstring(){
    this.searchString = '';
    this.getAllParticipantsAttendance();
  }

  searchData(event){
    if(event === ''){
      this.searchString = '';
      this.getAllParticipantsAttendance();
    }
  }
}
