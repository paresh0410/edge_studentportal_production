import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { DemoMaterialModule } from '../material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from '../../pipes/pipes.module' ;


import { WebinarComponent } from '../webinar/webinar.component';
import { WebinarMultiComponent } from '../webinar-multiple/webinar-multi.component';
import { ChatComponent } from '../chat/chat.component';
import { UserVideoComponent } from '../webinar/user-video.component';
import { OpenViduVideoComponent } from '../webinar/ov-video.component';
import { ModalViewerComponent } from '../../component/modal-viewer/modal-viewer.component';
import { ToasterComponent } from '../../component/toaster/toaster.component';
import { CourseCardComponent } from '../../component/course-card/course-card.component';
import { ActivityCardComponent } from '../../component/activity-card/activity-card.component';
import { WorkflowCardComponent } from '../../component/workflow-card/workflow-card.component';
import { JitsiComponent } from '../../component/jitsi/jitsi.component';
import { WebinarMultiJitsiComponent } from '../webinar-multi-jitsi/webinar-multi-jitsi.component';
import { ImageViewerContainerComponent } from '../../component/image-viewer-container/image-viewer-container.component';
import { ImageViewerModule } from '../../component/image-viewer/image-viewer.module';
import { NoDataFoundComponent } from '../lazy/component/no-data-found/no-data-found.component';

const COMPONENTS = [
  WebinarComponent,
  WebinarMultiComponent,
  ModalViewerComponent,
  ToasterComponent,
  UserVideoComponent,
  OpenViduVideoComponent,
  ChatComponent,
  CourseCardComponent,
  ActivityCardComponent,
  WorkflowCardComponent,
  JitsiComponent,
  WebinarMultiJitsiComponent,
  ImageViewerContainerComponent,
  NoDataFoundComponent,
];
@NgModule({
  declarations: [
    ... COMPONENTS,
  ],
  imports: [
    CommonModule,
    FormsModule,
    FilterPipeModule,
    PipeModule.forRoot(),
    ReactiveFormsModule,
    DemoMaterialModule,
    Ng2SearchPipeModule,
    ImageViewerModule,
  ],
  exports:[
    ...COMPONENTS,
   ]
})
export class CommonComponentSharingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CommonComponentSharingModule,
    };
  }
}
