import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageViewerModule } from '../../component/image-viewer/image-viewer.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ImageViewerModule,
  ]
})
export class ImageViewerModuleCommonModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ImageViewerModule,
    };
  }
}
