import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators  } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { AuthServiceProvider } from '../../service/auth-service';
import { ToastrService } from 'ngx-toastr';
import { BrandDetailsService } from '../../service/brand-details.service';
@Component({
  selector: 'ngx-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss'],
})
export class OtpComponent implements OnInit {
  otpForm: FormGroup;
  firstDigit: number;
  secondDigit: number;
  thirdDigit: number;
  fourthDigit: number;
  otpValue: string = '';
  otpIntValue: number;
  submitted= false;
  timeout: string;
  nxtpagevariable: boolean;
  resendTokenRes: any;
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    private ASP: AuthServiceProvider,
    private toastr: ToastrService,
    public brandService: BrandDetailsService,
    ) {

    let id = document.getElementById('nb-global-spinner');
    id.style.display = 'none';
  }
  currentBrandData: any;
  ngOnInit() {

    this.nxtpagevariable = false;
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.timeout = "1:52";

    this.otpForm = this.formBuilder.group({
      firstDigit: ['', Validators.required],
      secondDigit: ['',  Validators.required],
      thirdDigit: ['',  Validators.required],
      fourthDigit: ['', Validators.required],
    });
  }

  get f() { return this.otpForm.controls; }   //form fields getter for easy access

  gotothispage(){
    this.otpIntValue = parseInt (this.otpForm.value.firstDigit + this.otpForm.value.secondDigit + this.otpForm.value.thirdDigit + this.otpForm.value.fourthDigit);
    //       console.log("Form Data =========>" , this.otpIntValue);
    console.log('OTP===>', this.otpIntValue);

      if (localStorage.getItem('ECN')){
        const ECN = localStorage.getItem('ECN');
        const param = {
          ecn: ECN,
          otp: this.otpIntValue,
        };
        this.ASP.verifyToken(param)
        .then((result: any) => {
          if (result.type === true){
            localStorage.setItem('tenantId', result.tId);
            this.toastr.success(result.data1, 'Success!!').onHidden.subscribe(()=>
            this.toastr.clear());
            this.nxtpagevariable = true;
            setTimeout(() => {
              this.router.navigate(['../resetPass'], {relativeTo: this.routes});
            }, 1000);
          }else {
            this.toastr.warning(result.data1, 'Error!!').onHidden.subscribe(()=>
            this.toastr.clear());
            this.otpForm.reset();
          }
        }).catch(result => {
          console.log('RESULT===>', result);
          this.otpForm.reset();
          this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
          this.toastr.clear());
        })
      }else{
        this.router.navigate(["../forgotPass"], { relativeTo: this.routes });
        this.toastr.warning('Please enter your ECN again', 'Error!!').onHidden.subscribe(()=>
        this.toastr.clear());
        this.otpForm.reset();
      }
  }

//   gotothispage(){
//       // console.log("Form Data =========>" , this.otpForm.value);
// // tslint:disable-next-line: max-line-length
// // tslint:disable-next-line: radix
//       this.otpIntValue = parseInt (this.otpForm.value.firstDigit + this.otpForm.value.secondDigit + this.otpForm.value.thirdDigit + this.otpForm.value.fourthDigit);
//       console.log("Form Data =========>" , this.otpIntValue);
//     }

  onSubmit() {
  this.submitted = true;

    // stop here if form is invalid
    if (this.otpForm.invalid) {
        return;
    }else {

    }
}
    resendToken(){

      const ECN = localStorage.getItem('ECN');
      const data = {
        ecn: ECN,
        // tId :
      };
      console.log(data);
      this.ASP.resendtoken(data).then(res => {
        console.log(res);
        try {
          console.log(res);
          if (res['type'] === true) {
            if (res['data']['type'] === 'success') {
              this.resendTokenRes = res['data1'];
              this.toastr.success('Token resend successfully!').onHidden.subscribe(()=>
              this.toastr.clear());
              this.otpForm.reset();
              // this.presentAlert('Token resend successfully!');
            } else {
              this.toastr.warning(res['data']['message']).onHidden.subscribe(()=>
              this.toastr.clear());
              // this.presentAlert(res.data.message);
            }
          } else {
            this.toastr.warning(res['data1']).onHidden.subscribe(()=>
            this.toastr.clear());
            // this.presentAlert(res.data1);
            this.otpForm.reset();
          }
        } catch {
          console.log('error');
        }
      }, err => {
        // this.load.dismiss();
        console.log(err);
      });
    // } else {
    //   console.log('error');
  }

// keytab(event, maxLength) {
//   let nextInput = event.srcElement.nextElementSibling; // get the sibling element

//   const target = event.target || event.srcElement;
//   const id = target.id;
//   console.log(maxLength); // prints undefined
//   console.log(event.target.value.length);

//   if(event.target.value.length < maxLength && event.target.value != '')  // check the maxLength from here
//       return;
//   else
//       nextInput.focus();   // focus if not null
// }

// keytab(event, maxLength) {

//   this.otpValue = this.otpValue + event.target.value;
//   this.otpIntValue = parseInt(this.otpValue);
  // console.log('OTP-VALUE===>', this.otpValue);
  // const container = document.getElementsByClassName('container')[0];
  // const target = event.target || event.srcElement;
  // const myLength = target.value.length;
  // if (myLength >= maxLength) {
  //   let next = target;
  //   const next2 = next.parentElement;
  //   while (next = next2.nextElementSibling) {
  //       if (next == null)
  //           break;
  //       if (next.tagName.toLowerCase() === 'input') {
  //           next.focus();
  //           break;
  //       }
  //   }
  // }else if (myLength === 0) {
  //   let previous = target;
  //   while (previous = previous.previousElementSibling) {
  //       if (previous == null)
  //           break;
  //       if (previous.tagName.toLowerCase() === 'input') {
  //           previous.focus();
  //           break;
  //       }
  //   }
  // }

// }

}
