import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { PartnerService } from '../../service/partner.service';
import { ENUM } from '../../service/enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'ngx-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PartnerComponent implements OnInit {
  userDetails: any = [];
  list: boolean = false;
  constructor(public partner: PartnerService, public spinner: NgxSpinnerService,
    private datePipe: DatePipe, private router: Router, private routes: ActivatedRoute) {
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
  }

  ngOnInit() {
    this.nomineelist();
  }
  nomineelist() {
    this.spinner.show();
    const param = {
      partnerId: this.userDetails.referenceId,
      tId: this.userDetails.tenantId,
    };
    const url = ENUM.domain + ENUM.url.partner_nominee_list;
    this.partner.get(url, param).then(res => {
      console.log(res);
      this.spinner.hide();
      if (res['type'] === true) {
        const length = res['data'].length;
        for (let i = 0; i < length; i++) {
          res['data'][i].date = this.formdate(res['data'][i].nomDate);
        }
        this.nomineelist = res['data'];
        this.list = true;
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }
  formdate(date) {
    if (date) {
      const formatted = this.datePipe.transform(date, "dd-MMM-yyyy");
      return formatted;
    }
  }

  viewCall(nominee){
    this.partner.nominee = nominee;
    this.router.navigate(['../PartnerNomineeCall'], { relativeTo: this.routes });
  }
}
