import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { leaderboardViewerService } from '../leader-board/leader-board.service';
import { ENUM } from '../../service/enum';

@Component({
  selector: 'ngx-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class LeaderBoardComponent implements OnInit {

  userRankData = {
    empname: '',
    picture: '',
    points: 0,
    rank: '',
    userid: 0,
    pos: ''
  }
  displayedColumns: string[] = ['rank', 'profileImage', 'name', 'levels'];
  isLeaderBoard: boolean = false;
  skeletonarray = [0, 1, 2, 3, 4];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private router: Router, private routes: ActivatedRoute,
    private LBS: leaderboardViewerService) {

    let url = ENUM.domain + ENUM.url.getLearnerRankDetails;
    const employeeId = localStorage.getItem('employeeId');
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param1 = {
      empId: employeeId,
      tenantId: userDetails.tenantId,
    };
    this.LBS.getDashboardDetails(url, param1).then((result: any) => {
      console.log("Get Dashboard Rank Data ==>", result);
      this.LBS.setleaderboardData(result.formattedData.leaderbord, result.formattedData.userRank);
      console.log("Get Dashboard leaderbord ==>", result.formattedData.leaderbord);
      console.log("Get Dashboard rANK ==>", result.formattedData.userRank);
	  this.array2 = result.formattedData.leaderbord;
	  if (result.formattedData.userRank && result.formattedData.userRank.length > 0) {
		this.userRankData = result.formattedData.userRank[0];
    }
    this.makeRankDataReady(this.userRankData);
    this.makeLeaderboardDataReady(this.array2);

      this.isLeaderBoard = true;
    }).catch(result => {
      console.log("RESULT===>", result);
      this.isLeaderBoard = true;
    });
  }

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
  }

  array2: any = [
    // { rank: 1, profileImage: '../../../assets/user-avtars/1.jpg', name: "Sudheesh Ladasdsadas", levels: 10 },
    // { rank: 2, profileImage: '../../../assets/user-avtars/2.jpg', name: "Sudheesh Ladasdsadas", levels: 30 },
    // { rank: 3, profileImage: '../../../assets/user-avtars/3.jpg', name: "Sudheesh Ladasdsadas", levels: 140 },
    // { rank: 4, profileImage: '../../../assets/user-avtars/4.jpg', name: "Sudheesh Ladasdsadas", levels: 40 },
    // { rank: 5, profileImage: '../../../assets/user-avtars/5.jpg', name: "Sudheesh asdsadasd", levels: 240 },
    // { rank: 6, profileImage: '../../../assets/user-avtars/6.jpg', name: "Sudheesh asdsada", levels: 10 },
    // { rank: 7, profileImage: '../../../assets/user-avtars/7.jpg', name: "Sudheesh asdsadasd", levels: 20 },
    // { rank: 4, profileImage: '../../../assets/user-avtars/8.jpg', name: "Sudheesh asdasdasd", levels: 30 },
    // { rank: 6, profileImage: '../../../assets/user-avtars/9.jpg', name: "Sudheesh asdsad", levels: 120 },
    // { rank: 7, profileImage: '../../../assets/user-avtars/1.jpg', name: "Sudheeshs asdsadsad", levels: 110 },
    // { rank: 8, profileImage: '../../../assets/user-avtars/2.jpg', name: "Sudheesh", levels: 20 },
    // { rank: 9, profileImage: '../../../assets/user-avtars/3.jpg', name: "Sudheesh", levels: 34 },
    // { rank: 10, profileImage: '../../../assets/user-avtars/4.jpg', name: "Amanda", levels: 23 },
  ];

  ngAfterViewInit() {

	// this.array2 = this.LBS.getleaderboardData();
	// if (this.array2.currentUserData) {
	// 	this.userRankData = this.array2.currentUserData;
	// }

    console.log("RESULT 2===>", this.array2);
    // this.userRankData = this.array2.currentUserData;
    console.log("RESULT 4===>", this.userRankData);
    // this.makeRankDataReady(this.userRankData);
    // for (var i = 0; i < this.array2.length; i++) {
    // 	// if (this.array2[i].picture == 0) {
    // 	// 	this.array2[i].picture = '../../../assets/user-avtars/2.jpg'
    // 	// }
    // }
    // console.log('LeaderBoardData==>', this.array2);
    // this.makeLeaderboardDataReady(this.array2);
    // console.log("Rank Data after call" , this.array2);
  }
  // makeRankDataReady(rankData) {
  // 	if (rankData) {
  // 	  const rank = rankData.rank;
  // 	  // const lastone = rank.toString().split('').pop();
  // 	  // if (lastone == 1) {
  // 	  //   rankData.pos = 'st';
  // 	  // } else if (lastone == 2) {
  // 	  //   rankData.pos = 'nd';
  // 	  // } else if (lastone == 3) {
  // 	  //   rankData.pos = 'rd';
  // 	  // } else {
  // 	  //   rankData.pos = 'th';
  // 	  // }
  // 	  rankData.pos = this.ordinal_suffix_of(rank);
  // 	  console.log('rankData ', rankData);
  // 	} else {
  // 	  const defaultUserRankData = {
  // 		empname: '',
  // 		picture: '',
  // 		points: 0,
  // 		rank: 'NA',
  // 		userid: 0,
  // 		pos: ''
  // 	  }
  // 	  this.userRankData = defaultUserRankData;
  // 	  console.log('Default rankData ', this.userRankData);
  // 	}
  //   }
  makeRankDataReady(rankData) {
    if (rankData && rankData.points > 0) {
      const rank = rankData.rank;
      rankData.pos = this.ordinal_suffix_of(rank);
      console.log('rankData ', rankData);
    } else {
      let defaultUserRankData;
      if (rankData) {
        defaultUserRankData = {
          empname: rankData.empname,
          picture: rankData.picture,
          points: 0,
          rank: 'NA',
          userid: rankData.userid,
          pos: rankData.pos
        };
      } else {
        defaultUserRankData = {
          empname: '',
          picture: '',
          points: 0,
          rank: 'NA',
          userid: 0,
          pos: ''
        };
      }

      this.userRankData = defaultUserRankData;
      console.log('Default rankData ', this.userRankData);
    }
  }



  makeLeaderboardDataReady(leaderboardData) {
    if (this.userRankData.points > 0) {
      this.array2 = leaderboardData;
    } else {
      this.array2 = leaderboardData.slice(0, 5);
    }
    this.array2.forEach(element => {
      if (element.userId === this.userRankData.userid) {
    	element['status'] = true;
      } else {
    	element['status'] = false;
      }
    });
  }

  ordinal_suffix_of(i) {
    const j = i % 10;
    const k = i % 100;
    if (j === 1 && k !== 11) {
      // return i + 'st';
      return 'st';
    }
    if (j === 2 && k !== 12) {
      // return i + 'nd';
      return 'nd';
    }
    if (j === 3 && k !== 13) {
      // return i + 'rd';
      return 'rd';
    }
    // return i + 'th';
    return 'th';
  }
}


