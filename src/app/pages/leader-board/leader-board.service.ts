import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class leaderboardViewerService {

    leaderboard:any=[];
    currentUserData:any;
    constructor(private http: HttpClient,  private toastr: ToastrService) { }



    getDashboardDetails(url,param){
        return new Promise(resolve => {
            this.http.post(url, param)
            // .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            },
            err => {
                resolve(err);
                if(err){
                  this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
                  this.toastr.clear());
                    // this.toastr.error('Please check server connection','Server Error!');
                }
            });
        });
    }

    setleaderboardData(boardData , currentUserData){
        this.leaderboard = boardData;
        this.leaderboard.currentUserData = currentUserData;
    }

    getleaderboardData(){
        return this.leaderboard;
    }

}
