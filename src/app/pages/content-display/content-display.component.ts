import { Component, OnInit } from '@angular/core';
import { ToolsService } from '../../service/tools.service';
import { AudioPlayerService } from './../audio-player/audioplayer.service';
import { VideoPlayerService } from '../video-player/videoplayer.service';
import {  DomSanitizer } from '@angular/platform-browser';
import { DocumentViewerService } from '../document-viewer/documentviewer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-content-display',
  templateUrl: './content-display.component.html',
  styleUrls: ['./content-display.component.scss']
})
export class ContentDisplayComponent implements OnInit {
  contentData: any = {};
  pdfFile: any = 'https://scalified.com/valid-document-url.pdf';
  videoData: any = {};
  youtube;
  youtubeurl;


  constructor(private toolService: ToolsService,
     private audioService: AudioPlayerService,
     private videoService: VideoPlayerService,
     private documentservice: DocumentViewerService,
     private router: Router,
      private routes: ActivatedRoute,
     private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.contentData = this.toolService.contentData;

    console.log(this.contentData);
    if(Object.keys(this.contentData).length !== 0){
      if (this.contentData.contentType === 2 ) {
        const audioTrackList = [];
        const audioTrack = {
          name: this.contentData.contentName,
          artist: this.contentData.contentName,
          url: this.contentData.assetRef,
          cover: 'assets/images/cover1.jpg',

        };

        audioTrackList.push(audioTrack);
        this.audioService.setTrack(audioTrackList);
        this.audioService.fromTools = true;
      }
      if (this.contentData.contentType === 1) {
        this.videoService.fromTools = true;
        this.videoData = {
          reference : this.contentData.assetRef,
          name : this.contentData.assetName,
        }
        this.videoService.setVideoTrack(this.videoData);
      }

      if (this.contentData.contentType === 3) {
        this.videoService.fromTools = true;
        // this.contentData.assetRef = 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/Policy_Corner-E1andE3.pdf';
        // this.contentData.assetRef = this.sanitizer.bypassSecurityTrustResourceUrl(this.contentData.assetRef);
        let provider = 'google';
        this.documentservice.setDocument(
          this.contentData.assetRef,
          provider
        );
      }

      if (this.contentData.contentType === 6) {
        console.log('this.contentData.assetRef', this.contentData.assetRef);
        this.youtubeconfigure(this.contentData.assetRef);

        // this.youtube =  this.sanitizer.bypassSecurityTrustResourceUrl(this.contentData.assetRef);

      }
      if(this.contentData.contentType === 8){
        console.log('this.contentData.assetRef', this.contentData.assetRef);
        if (this.contentData.assetRef !== '') {
          const strWindowFeatures = 'location=yes,height=570,width=520,scrollbars=yes,status=yes';
          window.open(this.contentData.assetRef, '_blank', strWindowFeatures);
        }

      }
      
      
    }

   }

   goBack() {
     window.history.back();
   }

   youtubeconfigure(link) {
    // this.youtubeurl = link;
    console.log("youtubeurl", link);
    try {
      if (!link) {
        alert("Please Enter url");
      } else {
        const videoid = this.populateYoutubeObj(link);

        const url =
          "https://www.youtube.com/embed/" + videoid + "?autoplay=1&showinfo=0";
        this.youtubeurl = this.transform(url);

      }

    } catch (e) {
      console.log(e);
    }
  }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }


  populateYoutubeObj(url) {
    try {
      if (url) {
        let video_id = "";
        if (url.includes("embed")) {
          const urlsplit = url.split("/");
          video_id = urlsplit[urlsplit.length - 1];
        } else {
          video_id = url.split("v=")[1];
          const ampersandPosition = video_id.indexOf("&");
          if (ampersandPosition !== -1) {
            video_id = video_id.substring(0, ampersandPosition);
          }
        }
        return video_id;
      } else {
        return null;
      }
    } catch (e) {
      console.log(e);
      return null;
    }
  }

}
