import { NgModule , CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LearnContainerRoutingModule } from './learn-container-routing.module';
import { StarRatingModule } from 'angular-star-rating';
import { NgCircleProgressModule } from 'ng-circle-progress';

import { CommonComponentSharingModule } from '../common-component-sharing/common-component-sharing.module';

// import { NoDataFoundComponent } from './component/no-data-found/no-data-found.component';


// import { TimingPipe, RemoveHtmlPipe, SafePipe } from './pipes';

import { PipeModule } from '../../pipes/pipes.module';

import { LearnContainerComponent } from './learn-container.component';
import { CatalogueComponent } from './catalogue/catalogue.component' ;
import { LearnCoursesComponent } from './learn-courses/learn-courses.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { LearningPathwayComponent } from './learning-pathway/learning-pathway.component';

import { ComponentModule } from '../../component/component.module';
import { TabsModule } from 'ngx-tabset';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';

import { ThemeModule } from '../../@theme/theme.module';
import { LearnSearchContainerComponent } from './learn-search-container/learn-search-container.component';

const PIPES = [
    // TimingPipe,
    // RemoveHtmlPipe,
    // MandetoryquestionpipePipe,
    // SafePipe,
];

const COMPONENTS = [
    // NoDataFoundComponent,
    LearnContainerComponent,
    CatalogueComponent,
    LearnCoursesComponent,
    CourseDetailsComponent,
    LearningPathwayComponent,
    LearnSearchContainerComponent,
];

@NgModule({
  declarations: [
    ...COMPONENTS,
    ...PIPES,
  ],
  imports: [
    CommonModule,
    PipeModule.forRoot(),
    LearnContainerRoutingModule,
    NgCircleProgressModule.forRoot({}),
    // RatingModule,
    FormsModule,
    StarRatingModule,
    CommonComponentSharingModule.forRoot(),
    ComponentModule,
    TabsModule.forRoot(),
    InfiniteScrollModule,
    Ng2CarouselamosModule,
    ThemeModule.forRoot(),
  ],
  providers: [],
  exports: [
    ...COMPONENTS,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ],
})


export class LearnContainerModule { }
