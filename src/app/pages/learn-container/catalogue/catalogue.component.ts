import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ViewRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CourseCard } from '../../../models/course-card.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LearnServiceProvider } from '../../../service/learn-service';
import { CatalogueService } from '../../../service/learn-container/catalogue.service';
import { catalogConfig } from '../../../models/config';
import { ToasterService } from '../../../service/learn-container/toaster.service';
import { AuthServiceProvider } from '../../../service/auth-service';
import { RoutingStateService } from '../../../service/routing-state.service';
import { EventsService } from '../../../service/events.service';

@Component({
  selector: 'ngx-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // encapsulation: ViewEncapsulation.None,
})
export class CatalogueComponent implements OnInit, AfterViewInit {

  catalogConfig: CourseCard = catalogConfig;
  categories: any = [];

  currentUserData: any;
  currentSelectedMenu: any;
  currentPageNoCatalogue: any;
  searchTermCatalogue: any = '';
  nextSearchedCatalogueList: any;
  totalCataloguePageCount: any;
  totalCatalogueCount: any;
  searchedCatalogueList: any = [];
  notFound: any;
  infiniteScroll: any;
  searching: any;
  working: any;
  infiniteScrollDisabled: boolean = false;
  catalogueParams: any;
  loading: boolean = false;
  isLoading$: boolean = false;

  array = [];
  sum = 100;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  dataLimit = 10;

  noDataFoundContainer = {
    image: 'assets/images/no-data.svg',
    title: 'Categories and Courses not available',
    discription: 'Categories and Courses you are looking for are not available this time, please try after some time.',
  };

  tenantDetails: any;
  tenantId: any;

  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToasterService,
    public learnService: LearnServiceProvider,
    public catalogueService: CatalogueService,
    private cdf: ChangeDetectorRef,
    public ASP: AuthServiceProvider,
    public routingStateService: RoutingStateService,
    public events: EventsService,
  ) {  }

  ngOnInit() {
    this.getRoutingState();
    this.setDefaultValues();
    this.getUserData();
    this.routes.queryParams.subscribe(queryParams => {
        // do something with the query params
    });
    this.routes.params.subscribe(routeParams => {
        // this.currentSelectedMenu = routeParams.search;
        this.searchTermCatalogue = routeParams.search ? routeParams.search : '';

        // this.publishEvent(this.searchTermCatalogue);

        this.loading = true;
        this.fetchCatalogueCoursesForFirstPage(1, () => {
          console.log('Courses and categories fetched...');
          setTimeout(() => {
            this.calculateContainerHeight();
          }, 500);
          this.loading = false;
          this.detectChangesAfterViewChange();
        });
    });
    this.currentSelectedMenu = 0;
    this.catalogueService.currentCatId = 0;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.calculateContainerHeight();
    }, 500);
  }

  publishEvent(params) {
    this.events.publish('learn:search', {
      data: params,
      status: true,
    });
  }

  getRoutingState() {
    this.routingStateService.loadRouting();
    const routeHistory = this.routingStateService.getHistory();
    console.log('routeHistory...', routeHistory);
    const previousRoute = this.routingStateService.getPreviousUrl();
    console.log('previousRoute...', previousRoute);
  }

  setDefaultValues() {
    this.currentSelectedMenu = '';
    // this.currentUserData = {};
    this.currentPageNoCatalogue = 0;
    this.searchTermCatalogue = '';
    this.nextSearchedCatalogueList = [];
    this.totalCataloguePageCount = 0;
    this.totalCatalogueCount = 0;
    this.searchedCatalogueList = [];
    this.notFound = false;
    this.searching = false;
    this.working = false;
    this.catalogueParams = {};
    this.infiniteScrollDisabled = false;
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
      this.currentUserData['eid'] = JSON.parse(localStorage.getItem('employeeId'));
    } else {
      console.log('User not logged in...');
      this.tenantDetails =  this.ASP.getDomainBasedTenant();
      console.log('tenant details --->', this.tenantDetails);
      this.tenantId = this.tenantDetails.tenantId  ? this.tenantDetails.tenantId : 1;
    }
  }

  detectChangesAfterViewChange() {
    setTimeout(() => {
      if (this.cdf && !(this.cdf as ViewRef).destroyed) {
        this.cdf.detectChanges();
      }
    }, 100);
  }

  /********* fetch courses and subcategories by categories start *********/
  fetchCatalogueCoursesForFirstPage(currentPageNo, cb) {
    this.currentPageNoCatalogue = currentPageNo;
    this.catalogueParams = {
      pageno: this.currentPageNoCatalogue,
      dataLimit: this.dataLimit,
      searchString: this.searchTermCatalogue,
      empId: this.currentUserData ? this.currentUserData.eid : null,
      tId: this.currentUserData ? this.currentUserData.tenantId : this.tenantId,
    };
    this.fetchCatelogueCourseList(this.catalogueParams, result => {
      if (result.type === true) {
        const pageCount = result.count[0].courseCount;
        if (this.currentPageNoCatalogue !== 1) {
          this.nextSearchedCatalogueList = result.categoryList;
          this.totalCataloguePageCount = result.count[0].pageCount;
          this.totalCatalogueCount = result.count[0].courseCount;
          // this.totalCataloguePageCount = result.count[0].courseCount;
          // const totalPageCount = result.count[0].courseCount;
          // const totalListCount = result.categoryList.length;
          // this.totalCatalogueCount = (totalListCount * totalPageCount);
          this.searchedCatalogueList = this.searchedCatalogueList.concat(this.nextSearchedCatalogueList);
        } else {
          this.notFound = true;
          this.totalCataloguePageCount = result.count[0].pageCount;
          this.totalCatalogueCount = result.count[0].courseCount;
          this.searchedCatalogueList = result.categoryList;
          // this.totalCataloguePageCount = result.count[0].courseCount;
          // const totalPageCount = result.count[0].courseCount;
          // const totalListCount = result.categoryList.length;
          // this.totalCatalogueCount = (totalListCount * totalPageCount);
        }
        if (this.searchedCatalogueList.length > 0) {
          this.currentPageNoCatalogue++;
        }
        cb(true);
      } else {
        console.log('err ', result);
        cb(false);
      }
    });
  }

  fetchCatelogueCourseList(params: any, cb) {
    this.catalogueService.getCatalogueCourses(this.catalogueParams).then(
      res => {
        if (res.type === true) {
          console.log(this.catalogueParams.pageno, ' page category list ', res);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.searching = false;
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
        cb(res);
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.searching = false;
        this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
      }
    );
  }

  onScrollDownCatalogue (ev) {
    console.log('scrolled down!!', ev);

    this.direction = 'down';

    // setTimeout(() => {
      if (this.currentPageNoCatalogue > this.totalCataloguePageCount) {
        this.infiniteScrollDisabled = true;
        console.log('All courses fetched....');
      } else {
        if (!this.working) {
          this.working = true;
          this.fetchCatalogueCoursesForFirstPage(this.currentPageNoCatalogue , () => {
            setTimeout(() => {
              this.calculateContainerHeight();
            }, 500);
            this.working = false;
            console.log('Done');
            this.infiniteScrollDisabled = false;
          });
        }
      }
    // }, 500);
  }

  onScrollUpCatalogue(ev) {
    console.log('scrolled up!', ev);
    this.direction = 'up';
  }
  /********* fetch courses and subcategories by categories end *********/

  goToCategory(cat, subCat) {
    console.log('Category : ', cat, 'Subcategory : ', subCat);
    const mainCat = {
      categoryId: 0,
      categoryName: 'catalogue',
      visible: true,
      searchStr: this.searchTermCatalogue,
      filters: [],
      afterSearch: false,
    };
    let catId = 0;

    // this.catalogueService.addToBreadcrumbs(mainCat);
    // if (subCat) {
    //   const parentCat = this.makeBraedCrumbDataReady(cat);
    //   this.catalogueService.addToBreadcrumbs(parentCat);
    //   const childCat = this.makeBraedCrumbDataReady(subCat);
    //   this.catalogueService.addToBreadcrumbs(childCat);
    //   catId = childCat.categoryId;
    // } else {
    //   const parentCat = this.makeBraedCrumbDataReady(cat);
    //   this.catalogueService.addToBreadcrumbs(parentCat);
    //   catId = parentCat.categoryId;
    // }

    const breadcrumbs = {
      id: 1,
      subBreadCrumbs: [],
    };
    breadcrumbs.subBreadCrumbs.push(mainCat);
    if (subCat) {
      const parentCat = this.makeBraedCrumbDataReady(cat, true);
      breadcrumbs.subBreadCrumbs.push(parentCat);
      const childCat = this.makeBraedCrumbDataReady(subCat, true);
      breadcrumbs.subBreadCrumbs.push(childCat);
      catId = childCat.categoryId;
    } else {
      const parentCat = this.makeBraedCrumbDataReady(cat, false);
      breadcrumbs.subBreadCrumbs.push(parentCat);
      catId = parentCat.categoryId;
    }
    this.catalogueService.addSubBreadcrumbs(breadcrumbs);

    this.router.navigate(['./category/' + catId], { relativeTo: this.routes });
    // this.router.navigate(['./courses/my-pathways'], { relativeTo: this.routes });
  }

  makeBraedCrumbDataReady(data, directFlag) {
    const path = {
      categoryId: data.categoryId,
      categoryName: data.categoryName,
      visible: true,
      searchStr: '',
      filters: [],
      afterSearch: false,
      direct: directFlag,
    };
    return path;
  }

  performAction(event, i, tabData) {
    if (event) {
        switch (event.type) {
            case 'goToCourse': this.goToModules(event.cardData, i, tabData);
                break;
            case 'goToWorflow': this.goToModules(event.cardData, i, tabData);
                break;
            case 'wishlist': this.wishlistCourse(event.cardData, event.liked);
                break;
            case 'buy': this.buyCourse(event.cardData);
                break;
        }
    }
  }

  /********* go to course datils or workflow details start *********/
  goToModules(currentCourse, currentIndex, tabData) {
    console.log('Selected course : ', currentCourse, 'from tab : ', tabData);
    if (currentCourse.isWorkFlow == '1') {
      this.goToPathway(this.currentSelectedMenu, currentCourse);
    } else {
      this.goToCourse(this.currentSelectedMenu, currentCourse);
    }
  }

  goToCourse(selectedMenu, currentCourse) {
    const courseId = currentCourse.courseId;
    this.router.navigate(['./course/' + courseId], { relativeTo: this.routes });
  }

  goToPathway(selectedMenu, currentCourse) {
    // this.router.navigate(['./pathway/' + selectedMenu], { relativeTo: this.routes });
    const wfId = selectedMenu == 0 ? 0 : currentCourse.workflowid;
    this.router.navigate(['../pathway/details/' + wfId], { relativeTo: this.routes });
  }

  /********* go to course datils or workflow details end *********/

  /********* wishlist course start *********/
  wishlistCourse(courseData, wishlistType) {
    console.log('Course wishlist clicked : ', courseData);
    if (this.currentUserData) {
      if (wishlistType) {
        if (wishlistType === 0) {
          wishlistType = 1;
        } else {
          wishlistType = 0;
        }
      } else {
        wishlistType = 1;
      }
      const param = {
        cId: courseData.courseId,
        aId: 2,
        check: wishlistType,
        uId: this.currentUserData.id,
        eId: this.currentUserData.eid,
        tId: this.currentUserData.tenantId,
      };
      // this.load.presentLoading('Please wait....');
      this.catalogueService.addCourseToWishlist(param).then(
        res => {
          if (res.type === true) {
            console.log(' Course feedback add res ', res);
            // this.load.dismiss();
            this.updateCourseWishlistData(courseData, wishlistType);
          } else {
            // this.load.dismiss();
            console.log('err ', res);
            this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
          }
        },
        err => {
          // this.load.dismiss();
          console.log(err);
          this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
        },
      );
    } else {
      this.toastr.prsentToast('Please sign in to add course to wishlist', 'Sign In!', 'warning');
    }
  }

  updateCourseWishlistData(courseData, wishlistType) {
    // if (courseData.wishList === 0) {
    //   courseData.wishList = 1;
    // } else {
    //   courseData.wishList = 0;
    // }
    courseData.wishList = wishlistType;
  }
  /********* wishlist course end *********/

  /********* buy course start *********/
  buyCourse(courseData) {
    console.log('Course buy clicked : ', courseData);
    this.goToCourse(this.currentSelectedMenu, courseData);
  }
  /********* buy course end *********/

  calculateContainerHeight() {
    // const tempContentHeight = document.getElementsByTagName('ion-content')[1].clientHeight;
    // const tempCalenderHeight = document.getElementsByTagName('ion-calendar')[0].clientHeight;
    // const tempContainerHeight = tempContentHeight - tempCalenderHeight;
    // document.getElementById("search-results-catalogue").style.height = tempContainerHeight + 'px';
    let B = document.body,
        H = document.documentElement,
        height, tempContainerHeight;

    if (typeof document['height'] !== 'undefined') {
        height = document['height'] // For webkit browsers
    } else {
        height = Math.max( B.scrollHeight, B.offsetHeight,H.clientHeight, H.scrollHeight, H.offsetHeight );
    }
    console.log('Document height is : ', height);
    const header = document.getElementsByTagName('ngx-header');
    let tempHeaderHeight = 0;
    if(header && header.length !=0){
      tempHeaderHeight = header[0]['clientHeight'];
   }
    // const tempHeaderHeight = document.getElementsByTagName('ngx-header')[0].clientHeight;
    // const tempFooterHeight = document.getElementsByTagName('ngx-footer')[0].clientHeight;
    // tempContainerHeight = height - tempHeaderHeight - tempFooterHeight;
    tempContainerHeight = height - tempHeaderHeight;
    // document.getElementById("search-res-cat").style.height = tempContainerHeight + 'px';
    // document.getElementById("search-res-cat").style.overflowY = 'scroll';
    // document.getElementById("search-res-cat").style.overflowX = 'hidden';
    const searchRes = document.getElementById("search-res-cat");
    if(searchRes){
      searchRes.style.height = tempContainerHeight + 'px';
      searchRes.style.overflowY = 'scroll';
      searchRes.style.overflowX = 'hidden';
    }
  }
}
