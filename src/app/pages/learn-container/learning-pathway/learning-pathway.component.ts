import { Component, OnInit } from '@angular/core';
import { LearnServiceProvider } from '../../../service/learn-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../../../service/enum';
import { LearnService } from '../../../service/learn-container/learn.service';
import { ToasterService } from '../../../service/learn-container/toaster.service';

@Component({
  selector: 'ngx-learning-pathway',
  templateUrl: './learning-pathway.component.html',
  styleUrls: ['./learning-pathway.component.scss'],
})
export class LearningPathwayComponent implements OnInit {

  coupon: boolean = true;
  preview: any = []
  courseList: any = [];

  // arrayDataList: any;
  public monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  selectedMenuForPathway: any;
  currentUserData: any;
  workflowData: any;

  loading: boolean = false;
  noData: boolean = false;

  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToasterService,
    public learnServiceProvider: LearnServiceProvider,
    public learnService: LearnService,
  ) { }

  ngOnInit() {
    this.getUserData();
    this.routes.queryParams.subscribe(queryParams => {
        // do something with the query params
    });
    this.routes.params.subscribe(routeParams => {
        this.selectedMenuForPathway = routeParams.id;
        this.fetchCourseDataWithModules();
    });
    // this.getWorkflowDataFromService();
  }

  showCoupon() {
    this.coupon = !this.coupon;
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
      this.currentUserData['eid'] = JSON.parse(localStorage.getItem('employeeId'));
    } else {
      console.log('User not logged in...');
    }
  }

  // getWorkflowDataFromService() {
  //   this.workflowData = this.learnServiceProvider.getWorkflowData();
  //   console.log("workflow data from service :====>", this.workflowData);
  //   if (this.workflowData) {
  //     this.fetchLearningPathwayUpdatedCourses();
  //   }
  // }

  /******** get courses from workflow start *********/
  fetchCourseDataWithModules() {
    // this.spinner.show();
    this.loading = true;
    const param = {
      wfId : this.selectedMenuForPathway,
      empId : this.currentUserData ? this.currentUserData.eid : 'null',
      uId : this.currentUserData ? this.currentUserData.id : 'null',
      tId: this.currentUserData ? this.currentUserData.tenantId : 1,
    };
    this.learnService.getPathwayDetails(param).then(
      res => {
        this.loading = false;
        if (res.type === true) {
          // console.log('Course details res : ', res);
          this.workflowData = res.courseData;
          // this.workflowData = this.getFormattedCourseData(this.workflowData);
          const courseList = this.workflowData.courseData;
          if (courseList.length > 0) {
            this.workflowData.courseData = this.makeCourseDataReady(courseList);
            this.workflowData.perComplete = this.calculatePathwayProgress(courseList);
          } else {
            this.noData = true;
          }
          console.log('Formatted workflow details res : ', this.workflowData);
          // cb(res);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.loading = false;
        this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
      }
    );
  }

  makeCourseDataReady(courseList) {
    // if (workflowData) {
      // if (workflowData.courseList) {
        for (let i = 0; i < courseList.length; i++) {
          courseList[i]['begin'] = true;
          courseList[i]['wait'] = false;
          courseList[i]['completed'] = false;
          if (courseList[i].previousCourseid !== 0 && courseList[i].previousCourseid !== null &&
            courseList[i].previousCourseid !== undefined) {
            for (let j = 0; j < courseList.length; j++) {
              if (courseList[j].id === courseList[i].previousCourseid) {
                const dependentActIndex = j + 1;
                // courseList[i]['dependentActName'] = 'Activity ' + dependentActIndex;
                courseList[i]['dependentCourseName'] = courseList[j].courseTitle;
                if (courseList[j].percComplete === 100) {
                  if (courseList[i].cascade_course_flag === 1) {
                    courseList[i].wait = false;
                    courseList[i].begin = true;
                    courseList[i].completed = false;
                  } else {
                    courseList[i].wait = true;
                    courseList[i].begin = false;
                    courseList[i].completed = false;
                  }
                } else {
                  courseList[i].wait = true;
                  courseList[i].begin = false;
                  courseList[i].completed = false;
                }
              }
            }
          } else if (courseList[i].percComplete === 100) {
            courseList[i].completed = true;
            courseList[i].wait = false;
            courseList[i].begin = false;
          } else {
            if (courseList[i].cascade_course_flag === 1) {
              courseList[i].wait = false;
              courseList[i].begin = true;
              courseList[i].completed = false;
            } else {
              courseList[i].wait = true;
              courseList[i].begin = false;
              courseList[i].completed = false;
            }
          }
        }
      // }
    // }
    return courseList;
  }

  calculatePathwayProgress(courseList) {
    let tempProgress = 0;
    const courseLen = courseList.length;
    for (let i = 0; i < courseLen; i++) {
      tempProgress += courseList[i].percComplete;
    }
    const progress = (tempProgress / courseLen);
    return progress;
  }

  // getFormattedCourseData(DataCourseType) {
  //   // for (let i = 0; i < DataCourseType.length; i++) {
  //     let temporary = DataCourseType;
  //     for (let j = 0; j < temporary.courseList.length; j++) {
  //       temporary.courseList[j].courseDate = this.formatDate(
  //         temporary.courseList[j].courseDate);
  //       if (!temporary.courseList[j].earnpoints) {
  //         temporary.courseList[j].earnpoints = 0;
  //       }
  //       if (!temporary.courseList[j].cpoints) {
  //         temporary.courseList[j].cpoints = 0;
  //       }
  //       temporary.courseList[j].endDate = this.formatDate(temporary.courseList[j].endDate);
  //       temporary.courseList[j].completed = this.formatCompData(
  //         temporary.courseList[j].completed_courses,
  //         temporary.courseList[j].total_courses,
  //       );
  //       if (temporary.courseList[j].workflowid) {
  //         temporary.courseList[j].type1Image = 'assets/images/purpleIcon.png';
  //       } else if (temporary.courseList[j].courseType == 'MANDATORY') {
  //         temporary.courseList[j].type1Image = 'assets/images/orangeLearn.svg';
  //       } else if (temporary.courseList[j].courseType == 'RECOMMENDED') {
  //         temporary.courseList[j].type1Image = 'assets/images/mountLearn.svg';
  //       } else {
  //         // if(temporary.courseList[j].courseType == 'ASPIRATIONAL'){
  //         temporary.courseList[j].type1Image = 'assets/images/tickLearn.svg';
  //       }
  //       if (temporary.courseList[j].courseType2 == 'Online') {
  //         temporary.courseList[j].type2Image = 'assets/images/online.svg';
  //       } else if (temporary.courseList[j].courseType2 == 'Classroom') {
  //         temporary.courseList[j].type2Image = 'assets/images/classroom.svg';
  //       }
  //       // if (temporary.courseList[j].percComplete === 0) {
  //       //   temporary.courseList[j].percComplete = 0;
  //       // }
  //     }
  //   // }
  //   return DataCourseType;
  // }

  ProgressInWhole(val){
    if (val) {
      return Math.round(val);
    }
    else{
      return 0;
    }
  }

  formatDate(date) {
    var d = new Date(date),
    month = '' + d.getMonth(),
    day = '' + d.getDate(),
    year = d.getFullYear();
    month = this.monthNames[month];
    if (day.length < 2) day = '0' + day;
    var dateString = [day, month, year].join(' ');
    return dateString;
  }

  formatCompData(ccvar, tcvar) {
    ccvar = '' + ccvar;
    tcvar = '' + tcvar;
    if (ccvar.length < 2) ccvar = '0' + ccvar;
    if (tcvar.length < 2) tcvar = '0' + tcvar;
    var retString = ccvar + ' / ' + tcvar;
    return retString;
  }

  bindBackgroundImage(img) {
    if (img === null || img === '' || img === 'assets/images/courseicon.jpg') {
      // return 'url(' + 'assets/images/card.jpg' + ')';
      return 'assets/images/card.jpg';
    } else {
      return img;
    }
  }
  /******** get courses from workflow end ********/

  performActionOnCourse(actionType, event, cardData, i) {
    if (actionType) {
        switch (actionType) {
            case 'goToCourse':
                this.goToModules(cardData, i);
                break;
            case 'likeDislike':
                this.submitCourseFeedback(cardData, event);
                break;
        }
    }
  }

  /********* go to course datils or workflow details start *********/
  goToModules(currentCourse, currentIndex) {
    console.log('Selected course : ', currentCourse);
    if (currentCourse.noOfmodules > 0) {
      this.learnServiceProvider.setcourseDetailList(currentCourse);
      this.router.navigate(['../../../../module-activity'], { relativeTo: this.routes });
    } else {
      console.log('No modules avalable');
      this.toastr.prsentToast('No modules avalable', 'Warning!', 'warning');
    }
  }
  /********* go to course datils or workflow details end *********/

  /********* submit course feedback start *********/
  submitCourseFeedback(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        // courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          // courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        // courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        // courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          // courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        // courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.id,
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.submitUserCoursesFeedback(param).then(
      res => {
        if (res.type === true) {
          console.log(' Course feedback add res ', res);
          // this.load.dismiss();
          this.updateCourseFeedbackData(courseData, feedbackType);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
      },
    );
  }

  removeCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.id,
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.removeUserCoursesFeedback(param).then(
      res => {
        if (res.type === true) {
          console.log(' Course feedback remove res ', res);
          // this.load.dismiss();
          this.updateCourseFeedbackData(courseData, feedbackType);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
      },
    );
  }

  updateCourseFeedbackData(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        courseData.isLike = 1;
      } else {
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        courseData.isDislike = 1;
      } else {
        courseData.isDislike = 0;
      }
    }
  }
  /********* submit course feedback end *********/

  goBack() {
    window.history.back();
  }

}
