import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningPathwayComponent } from './learning-pathway.component';

describe('LearningPathwayComponent', () => {
  let component: LearningPathwayComponent;
  let fixture: ComponentFixture<LearningPathwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearningPathwayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningPathwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
