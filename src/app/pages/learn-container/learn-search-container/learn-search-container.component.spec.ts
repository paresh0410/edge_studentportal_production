import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnSearchContainerComponent } from './learn-search-container.component';

describe('LearnSearchContainerComponent', () => {
  let component: LearnSearchContainerComponent;
  let fixture: ComponentFixture<LearnSearchContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnSearchContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnSearchContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
