import { Component, OnInit,ViewRef, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { courseConfig, pathwayConfig, openConfig } from '../../../models/config';
import { CourseCard } from '../../../models/course-card.model';
import { LearnService } from '../../../service/learn-container/learn.service';
import { ToasterService } from '../../../service/learn-container/toaster.service';
import { EventsService } from '../../../service/events.service';
import { Subscription, fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { LearnServiceProvider } from '../../../service/learn-service';

@Component({
  selector: 'ngx-learn-search-container',
  templateUrl: './learn-search-container.component.html',
  styleUrls: ['./learn-search-container.component.scss']
})
export class LearnSearchContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  dataLimit = 20;
  loading = true;
  searchTerm = '';
  searchedCourseList: any = [];
  throttle = 20;
  scrollDistance = 2;
  scrollUpDistance = 2;
  direction = '';
  working = false;
  nextSearchedCourseList: any = [];
  searchParams: any;
  currentPageNo: any = 1;
  // notFound: any = false;
  totalPageCount: any;
  totalCoursesCount: any;
  differentTypeCoursesCount: any;
  totalRecordCount: number;
  searchString: any = '';
  pageSearchData: any;
  currentUserData = null;
  courseConfig: CourseCard = courseConfig;
  pathwayConfig: CourseCard = pathwayConfig;
  openConfig: CourseCard = openConfig;
  noDataFoundContainer = {
    image: 'assets/images/no-data.svg',
    title: 'Courses not available',
    discription: 'Courses you are looking for are not available this time, please try after some time.',
  };


  backBtnEvent: Subscription;
  backBtnEventCalled: boolean = false;

  constructor(private router: Router,
    private cdf: ChangeDetectorRef,
    private toastr: ToasterService,
    public learnService: LearnService,
    public learnServiceProvider: LearnServiceProvider,
    public events: EventsService,
    private routes: ActivatedRoute) {
      this.routes.params.pipe(debounceTime(700)).subscribe(params => {
        console.log('search', params['search']);
        this.getUserData();
        this.setDefaultValues();
        if(params['search']){
          this.searchTerm = params['search'];
        }else {
          this.searchTerm = "";
        }
        this.initComponentData();
      });

  }

  ngOnInit() {
    this.getUserData();
  }

  initComponentData(){
    if(this.currentUserData && this.currentUserData['roleId'] == 8){
      this.getIntialDataForCourse();
    }else {
      this.loading = false;
      this.noDataFoundContainer = {
        image: 'assets/images/no-data.svg',
        title: '',
        discription: 'This Feature is not available for this role.',
      }
    }

  }
  getIntialDataForCourse(){
    this.currentPageNo = 1;
    this.searchedCourseList = [];
    this.detectChangesAfterViewChange();
    this.loading = true;
    this.fetchCoursesForFirstPage(this.currentPageNo, (result) => {
      console.log('Fetch first page of courses for string : ', this.searchTerm, result);
      this.loading = false;
      this.detectChangesAfterViewChange();
      setTimeout(() => {
        this.calculateContainerHeight();
      }, 500);
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.calculateContainerHeight();
    }, 500);
  }
  onScrollUp(ev) {
    console.log('scrolled up!', ev);
    this.direction = 'up';
  }

  onScrollDown($event){
    if (this.currentPageNo > this.totalPageCount) {
      // this.infiniteScroll.disabled = true;
      // event.target.disabled = true;
    } else {
      // this.currentPageNo++;
      if (!this.working) {
        this.working = true;
        // if (this.networkservice.getOnlineStatus()) {
          this.fetchCoursesForFirstPage(this.currentPageNo, () => {
            this.working = false;
            this.calculateContainerHeight();
            console.log('Done');
          });
      }
    }
  }


  fetchCoursesForFirstPage(currentPageNo, cb) {
    // this.searchedCourseList = [];
    this.currentPageNo = currentPageNo;
    this.searchParams = {
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
      uId: this.currentUserData.id,
      pageLimit: this.dataLimit,
      pageNo: this.currentPageNo,
      searchString: this.searchTerm,
    };
    this.fetchCourseList(this.searchParams, result => {
      if (result.type === true) {
        // this.makeDifferentTypesOfCoursesCountReady(this.differentTypeCoursesCount);
        if (this.currentPageNo !== 1) {
          this.nextSearchedCourseList = result.data;
          this.totalPageCount = result.lastPage;
          this.totalCoursesCount = result.rowsTotalCount;
          this.differentTypeCoursesCount = result.diffCourseCount;
          this.searchedCourseList = this.searchedCourseList.concat(this.nextSearchedCourseList);
        } else {
          // this.notFound = true;
          this.totalPageCount = result.lastPage;
          this.searchedCourseList = result.data;
          this.totalCoursesCount = result.rowsTotalCount;
          this.differentTypeCoursesCount = result.diffCourseCount;
        }
        // if (this.load.presentLoading) {
        //   this.load.dismiss();
        // }
        if (this.searchedCourseList.length > 0) {
          this.currentPageNo++;
        }
        cb(true);
      } else {
        console.log('err ', result);
        cb(false);
      }
    });
  }

  fetchCourseList(params: any, cb) {
    // if (this.currentPageNo === 1) {
    //   this.load.presentLoading('Please wait....');
    // }
    this.learnService.getAllLearnCourseListBySearchFilter(this.searchParams).then(
      res => {
        if (res.type === true) {
          console.log(this.searchParams.pageNo, ' page Course list ', res);
          // this.load.dismiss();
          // this.totalPageCount = res.lastPage;
          // this.searchedCourseList = res.data;
          cb(res);
          // this.currentPageNo++;
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.loading = false;
          this.working = false;
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.loading = false;
        this.working = false;
        this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
      }
    );
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
      this.currentUserData['eid'] = JSON.parse(localStorage.getItem('employeeId'));
    } else {
      // console.log('User not logged in...');
      // this.tenantDetails =  this.ASP.getDomainBasedTenant();
      // console.log('tenant details --->', this.tenantDetails);
      // this.tenantId = this.tenantDetails.tenantId  ? this.tenantDetails.tenantId : 1;
    }
  }

  setDefaultValues() {
    this.currentPageNo = 0;
    this.searchTerm = '';
    this.nextSearchedCourseList = [];
    this.totalPageCount = 0;
    this.totalCoursesCount = 0;
    this.differentTypeCoursesCount = '';
    this.searchedCourseList = [];
    // this.notFound = false;
    this.loading = true;
    this.working = false;
    this.noDataFoundContainer = {
      image: 'assets/images/no-data.svg',
      title: 'Courses not available',
      discription: 'Courses you are looking for are not available this time, please try after some time.',
    };
  }

  calculateContainerHeight() {
    let B = document.body,
        H = document.documentElement,
        height, tempContainerHeight;

    if (typeof document['height'] !== 'undefined') {
        height = document['height'] // For webkit browsers
    } else {
        height = Math.max( B.scrollHeight, B.offsetHeight,H.clientHeight, H.scrollHeight, H.offsetHeight );
    }
    console.log('Document height is : ', height);
    const header = document.getElementsByTagName('ngx-header');
    let tempContentHeight = 0;
    if(header  && header.length !=0){
      tempContentHeight = header[0]['clientHeight'];
   }
    // const tempContentHeight = document.getElementsByTagName('ngx-header')[0].clientHeight;
    tempContainerHeight = height - tempContentHeight - 24;
    const searchRes = document.getElementById("search-res");
    if(searchRes){
      searchRes.style.height = tempContainerHeight + 'px';
      searchRes.style.overflowY = 'scroll';
      searchRes.style.overflowX = 'hidden';
    }

  }

  detectChangesAfterViewChange() {
    setTimeout(() => {
      if (this.cdf && !(this.cdf as ViewRef).destroyed) {
        this.cdf.detectChanges();
      }
    }, 100);
  }



  ngOnDestroy() {
    console.log('Destroyed');
    if(localStorage.getItem("searchTriggredFrom") != null) {
      localStorage.removeItem("searchTriggredFrom");
   }
  }


  performAction(event) {
    if (event) {
        switch (event.type) {
            case 'goToCourse': this.goToModules(event.cardData);
                break;
            case 'goToWorflow': this.goToModules(event.cardData);
                break;
            case 'goToOpenCourse': this.goToOpenCourseDetails(event.cardData);
                break;
            case 'enrolSelf': this.enrolSelfCourses(event.cardData);
                break;
            case 'likeDislike': this.submitCourseFeedback(event.cardData, event.liked);
                break;
            // case 'share': this.shareCourse(event.cardData);
            //     break;
            // case 'wishlist': this.wishlistCourse(event.cardData, event.liked);
            //     break;
            // case 'buy': this.buyCourse(event.cardData);
            //     break;
        }
    }
  }

  /********* go to course datils or workflow details start *********/
  goToModules(currentCourse) {
    // console.log('Selected course : ', currentCourse, 'from tab : ', tabData);
    // if (this.currentSelectedMenu == 0) {
    //   this.goToCourse();
    // } else if (this.currentSelectedMenu == 1) {
    //   this.goToCourse();
    // } else if (this.currentSelectedMenu == 2) {
    //   this.goToPathway();
    // }
    if (currentCourse.isWorkFlow == '1') {
      this.goToPathway(currentCourse);
    } else {
      if (currentCourse.noOfmodules > 0) {
        this.learnServiceProvider.setcourseDetailList(currentCourse);
        // this.router.navigate(['../../module-activity'], { relativeTo: this.routes });
        this.router.navigate(['/pages/module-activity']);
      } else {
        console.log('No modules avalable');
        this.toastr.prsentToast('No modules avalable in this course', 'Not available!', 'warning');
      }
    }
  }

  goToCourse(currentCourse) {
    // this.router.navigate(['../course/' + selectedMenu], { relativeTo: this.routes });
    const courseId = currentCourse.courseId;
    this.router.navigate(['./course/' + courseId], { relativeTo: this.routes });
  }

  goToPathway(currentCourse) {
    const wfId = currentCourse.workflowid;
    // this.router.navigate(['../pathway/details/' + wfId], { relativeTo: this.routes });
    this.router.navigate(['pages/learning/pathway/details/' + wfId]);
  }

  goToOpenCourseDetails(currentCourse) {
    const courseId = currentCourse.id;
    this.router.navigate(['./course/' + courseId], { relativeTo: this.routes });
  }

  /********* go to course datils or workflow details end *********/

  /********* enrol self courses start *********/
  enrolSelfCourses(courseData) {
    const param = {
      enrolId: courseData.enrolId,
      ttt_workflowId: courseData.ttt_workflowId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.selfEnrolUserCourses(param).then(
      res => {
        if (res.type === true) {
          console.log('Enrol Course res ', res);
          this.toastr.prsentToast('Course enroled successfully', 'Success!', 'success');
          // this.load.dismiss();
          // this.fetchCoursesForFirstPage();
          this.fetchCoursesForFirstPage(1, () => {
            setTimeout(() => {
              this.calculateContainerHeight();
            }, 500);
            console.log('Course List updated after enrolment...');
          });
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to enrol course at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to enrol course at this time', 'Failed!', 'error');
      }
    );
  }
  /********* enrol self courses end *********/

  /********* submit course feedback start *********/
  submitCourseFeedback(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        // courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          // courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        // courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        // courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          // courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        // courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.id,
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.submitUserCoursesFeedback(param).then(
      res => {
        if (res.type === true) {
          console.log(' Course feedback add res ', res);
          // this.load.dismiss();
          this.updateCourseFeedbackData(courseData, feedbackType);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
      },
    );
  }

  removeCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.id,
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.removeUserCoursesFeedback(param).then(
      res => {
        if (res.type === true) {
          console.log(' Course feedback remove res ', res);
          // this.load.dismiss();
          this.updateCourseFeedbackData(courseData, feedbackType);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
      },
    );
  }

  updateCourseFeedbackData(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        courseData.isLike = 1;
      } else {
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        courseData.isDislike = 1;
      } else {
        courseData.isDislike = 0;
      }
    }
  }

}
