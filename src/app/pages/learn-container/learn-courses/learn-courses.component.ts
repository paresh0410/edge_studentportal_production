import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewRef, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { CourseCard } from '../../../models/course-card.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, pipe, of, Subscription, fromEvent } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { LearnServiceProvider } from '../../../service/learn-service';
import { catalogConfig, openConfig, wishConfig, courseConfig, pathwayConfig, learnTabs } from '../../../models/config';
import { retry, map, catchError } from 'rxjs/operators';
import { CatalogueService } from '../../../service/learn-container/catalogue.service';
import { LearnService } from '../../../service/learn-container/learn.service';
import { ToasterService } from '../../../service/learn-container/toaster.service';
import { Location } from '@angular/common';
import { AuthServiceProvider } from '../../../service/auth-service';
import { EventsService } from '../../../service/events.service';

@Component({
  selector: 'ngx-learn-courses',
  templateUrl: './learn-courses.component.html',
  styleUrls: ['./learn-courses.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class LearnCoursesComponent implements OnInit, AfterViewInit, OnDestroy {

  // catalogConfig: CourseCard = catalogConfig;
  // wishConfig: CourseCard = wishConfig;
  // courseConfig: CourseCard = courseConfig;
  // pathwayConfig: CourseCard = pathwayConfig;
  // openConfig: CourseCard = openConfig;

  mainTabsData: any = learnTabs;

  catalogCourses: any = [];
  whishlistedCourses: any = [];
  myCourses: any = [];

  loading: boolean = false;
  // isLoading$: Observable<boolean>;
  isLoading$: boolean = false;

  array = [];
  sum = 100;
  throttle = 20;
  scrollDistance = 2;
  scrollUpDistance = 2;
  direction = '';

  selectedTabsArray = [];
  currentSelectedMenu: any;
  currentUserData: any;

  currentPageNo: any;
  // searchParams: any;
  searchTerm: any = '';
  nextSearchedCourseList: any;
  totalPageCount: any;
  totalCoursesCount: any;
  differentTypeCoursesCount: any;
  searchedCourseList: any = [];
  notFound: any;
  infiniteScroll: any;
  searching: any;
  working: any;
  learnParams: any;
  selectedTabType: any = 'ALL';
  selectedSubTabType: any = 'CURRENT';
  selectedTabIndex: any = 0;
  infiniteScrollDisabled: boolean = false;
  selectedMainTabIndex: any = 0;
  selectedSubTabIndex: any = 0;
  selectedSubTabData: any;
  subCategory: any;
  selectedTabData: any;
  subTabsData: any;
  category: any;

  noDataFoundContainer = {
    image: 'assets/images/no-data.svg',
    title: 'Courses not available',
    discription: 'Courses you are looking for are not available this time, please try after some time.',
  };

  routingPath: any = [
    {
      id: 0,
      type: 'catalogue',
    },
    {
      id: 1,
      type: 'course',
    },
    {
      id: 2,
      type: 'pathway',
    },
    {
      id: 3,
      type: 'self',
    },
    {
      id: 4,
      type: 'wishlist',
    },
  ];

  customClass: any;
  breadcrumbsList: any;
  catalogueParams: any;
  filtersData: any;
  categoryId: any;
  subCategoryId: any;
  sortStr: any;
  priceStr: any;
  levelStr: any;
  tagStr: any;
  dataLimit = 20;
  isWorking: any = false;

  appliedFilterData:any = [];
  tenantDetails: any;
  tenantId: any;

  backBtnEvent: Subscription;
  backBtnEventCalled: boolean = false;

  todaysDate = new Date();
  currentMonth = this.todaysDate.getMonth() + 1;
  nextMonth = this.currentMonth + 1;
  previousMonth = this.currentMonth - 1;
  currentYear = this.todaysDate.getFullYear();

  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToasterService,
    public learnServiceProvider: LearnServiceProvider,
    public catalogueService: CatalogueService,
    public learnService: LearnService,
    private cdf: ChangeDetectorRef,
    private location: Location,
    public ASP: AuthServiceProvider,
    public events: EventsService,
  ) {
    this.subscribeEvent();
  }

  ngOnInit() {
    this.addBackButtonEventListner();
    this.setDefaultValues();
    this.getUserData();
    // this.routes.queryParams.subscribe(queryParams => {
    //     // do something with the query params
    // });
    this.routes.params.subscribe(routeParams => {
        // this.currentSelectedMenu = routeParams.id;
        this.setDefaultValues();
        this.getUserData();
        this.makePageDataReady(routeParams);
        // const breadcrumbsData = this.catalogueService.breadCrumbs;
        // console.log('Current breadcrumbs data : ', breadcrumbsData);
        // this.breadcrumbsList = this.catalogueService.breadCrumbs;
        setTimeout(() => {
          this.calculateContainerHeight();
        }, 500);
    });

    console.log('All tabs new : ', this.mainTabsData);
    console.log('Selected tabs : ', this.selectedTabsArray);
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.calculateContainerHeight();
    }, 500);
  }

  setDefaultValues() {
    this.selectedTabsArray = [];
    this.currentSelectedMenu = '';
    // this.currentUserData = {};
    this.currentPageNo = 0;
    this.searchTerm = '';
    this.nextSearchedCourseList = [];
    this.totalPageCount = 0;
    this.totalCoursesCount = 0;
    this.differentTypeCoursesCount = '';
    this.searchedCourseList = [];
    this.notFound = false;
    this.searching = false;
    this.working = false;
    this.learnParams = {};
    this.selectedTabType = 'ALL';
    this.selectedSubTabType = 'CURRENT';
    this.selectedTabIndex = 0;
    this.infiniteScrollDisabled = false;
    this.selectedMainTabIndex = 0;
    this.selectedSubTabIndex = 0;
    this.selectedSubTabData = {};
    this.subCategory = '';
    this.selectedTabData = {};
    this.subTabsData = {};
    this.category = '';
    this.customClass =  '';
    this.breadcrumbsList = [];
    this.filtersData = [];
    this.categoryId = 0;
    this.sortStr = '';
    this.priceStr = '';
    this.levelStr = '';
    this.tagStr = '';
    this.catalogueParams = {
      catId: 0,
      pageno: 1,
      dataLimit: 10,
      searchString: '',
      priceData: '',
      levels: '',
      tags: '',
      empId: null,
      tId: 1,
    };
    this.appliedFilterData = [];
  }

  subscribeEvent() {
    this.events.subscribe('breadcrumbs:update', (result: any) => {
      console.log('Updated bredcrumbs data : ', result);
      if (result.status && result.data) {
        const updatebrdcrmbs = result.data;
        this.breadcrumbsList = updatebrdcrmbs.subBreadCrumbs;
        this.catalogueService.updateBreadcrumbs(this.breadcrumbsList);
        console.log('Current breadcrumbs data : ', this.breadcrumbsList);
      }
    });
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
      this.currentUserData['eid'] = JSON.parse(localStorage.getItem('employeeId'));
    } else {
      console.log('User not logged in...');
      this.tenantDetails =  this.ASP.getDomainBasedTenant();
      console.log('tenant details --->', this.tenantDetails);
      this.tenantId = this.tenantDetails.tenantId  ? this.tenantDetails.tenantId : 1;
    }
  }

  /************* make data ready for selected route start ***************/
  makePageDataReady(routeParams) {
    console.log('Current route is : ', this.router.url, ' and params are : ', routeParams);
    const str = this.router.url;
    const n = str.search('catalogue');
    if (n !== -1) {
      this.currentSelectedMenu = 0;
      // this.categoryId = Number(routeParams.id);
      // this.catalogueService.currentCatId = this.categoryId;
      this.searchTerm = routeParams.search ? routeParams.search : '';
    } else {
      if (routeParams.id) {
        const routeItem = this.routingPath.filter(function(item) {
          if (routeParams.id == item.type || routeParams.id == item.id) {
            return item;
          }
        });
        this.currentSelectedMenu = routeItem[0].id;
      } else {
        this.currentSelectedMenu = 0;
      }
      if (routeParams.search) {
        this.searchTerm = routeParams.search;
      }
    }

    if (this.currentSelectedMenu == 0) {
      this.customClass = 'sub-inner';
      this.categoryId = Number(routeParams.id);
      this.catalogueService.currentCatId = this.categoryId;

      this.breadcrumbsList = this.catalogueService.getCurrentPageBreadcrumbs();
      console.log('Current breadcrumbs data : ', this.breadcrumbsList);
      this.backBtnEventCalled = false;

      this.makeFiltersReadyIfApplied();

    } else {
      this.customClass = 'learn-section';
      this.makePageParamsReady(routeParams);
    }

  }

  makePageParamsReady(routeParams) {
    this.selectedMainTabIndex = this.currentSelectedMenu;
    // this.loadCourses(routeParams.id);
    if (this.currentSelectedMenu == 0) {
      this.selectedTabsArray = this.mainTabsData[0].subTabs;
    } else if (this.currentSelectedMenu == 1) {
      this.selectedTabsArray = this.mainTabsData[1].subTabs;
      // this.fetchTempList();
    } else if (this.currentSelectedMenu == 2) {
      this.selectedTabsArray = this.mainTabsData[2].subTabs;
      // this.fetchTempList2();
    } else if (this.currentSelectedMenu == 3) {
      this.selectedTabsArray = this.mainTabsData[3].subTabs;
      // this.fetchTempList2();
    } else if (this.currentSelectedMenu == 4) {
      this.selectedTabsArray = this.mainTabsData[4].subTabs;
      // this.fetchTempList2();
    }
    // this.subTabSelected(0);
    if (routeParams && routeParams.id && this.currentSelectedMenu) {
      this.getMainTabData(this.currentSelectedMenu, this.mainTabsData[this.currentSelectedMenu]);
    } else {
      this.getMainTabData(0, this.mainTabsData[0]);
    }
  }

  /************* make data ready for selected route end ***************/

  /************* for breadcrumbs start ***************/
  addBackButtonEventListner() {
    // let _this = this;
    // window.addEventListener('popstate', function(event) {
    //   // The popstate event is fired each time when the current history entry changes.
    //   console.log('Back button pressed!', event);
    //   // if (_this.currentSelectedMenu == 0) {
    //   //   _this.catalogueService.resetBreadcrumbs();
    //   // }
    //   if (!_this.backBtnEventCalled) {
    //     _this.backBtnEventCalled = true;
    //     // _this.catalogueService.removeSubBreadcrumbs();
    //     _this.checkIfParentCatOrSubCat();
    //     _this.removeBackbuttonEventListner();
    //   }
    // }, false);

    // this.backBtnEvent = this.location.subscribe(
    //   ( (value:PopStateEvent) => {
    //     console.log("locaton OnNext")
    //     console.log(value);
    //     if (!this.backBtnEventCalled) {
    //       this.backBtnEventCalled = true;
    //       // this.catalogueService.removeSubBreadcrumbs();
    //       this.checkIfParentCatOrSubCat();
    //       // this.removeBackbuttonEventListner();
    //     }
    //   }),
    //   ( ex => {
    //     console.log("Error occured postate event")
    //     console.log(ex);
    //   }),
    // );

    this.backBtnEvent = fromEvent(window, 'popstate').subscribe(e => {
      console.log("locaton OnNext : ", e);
      if (!this.backBtnEventCalled) {
        this.backBtnEventCalled = true;
        this.checkIfParentCatOrSubCat();
        // this.removeBackbuttonEventListner();
      }
    });
  }

  removeBackbuttonEventListner() {
    // window.removeEventListener('popstate', function(event) {
    //   // The popstate event is fired each time when the current history entry changes.
    //   console.log('Back button pressed event removed!', event);
    // }, false);
    if (this.backBtnEvent) {
      this.backBtnEvent.unsubscribe();
    }
  }

  checkIfParentCatOrSubCat() {
    const breadcrumbsData = this.catalogueService.breadCrumbs1;
    if (breadcrumbsData.length > 0) {
      const subBreadcrumbsData = breadcrumbsData[breadcrumbsData.length - 1].subBreadCrumbs;
      if (subBreadcrumbsData.length > 2) {
        // this.catalogueService.removeSubBreadcrumbs();
        const currentcat = subBreadcrumbsData[subBreadcrumbsData.length - 1];
        // const currentcat = subBreadcrumbsData[subBreadcrumbsData.length - 1].categoryId;
        if (currentcat.categoryId == this.categoryId) {
          if (this.searchTerm == '') {
            if (currentcat.direct) {
              this.catalogueService.removeBreadcrumbs();
            } else {
              this.catalogueService.removeSubBreadcrumbs();
            }
          } else {
            if (currentcat.afterSearch) {
              // this.catalogueService.updateSubBreadcrumbsOptions(2, []);
              // this.catalogueService.updateSubBreadcrumbsOptions(3, false);
              console.log('need to remove applied filters');
            } else {
              console.log('no need to remove applied filters');
            }
          }
        } else {
          console.log('No need to remove subbreadcrumbs');
        }
      } else {
        const currentcat = subBreadcrumbsData[subBreadcrumbsData.length - 1];
        // const currentcat = subBreadcrumbsData[subBreadcrumbsData.length - 1].categoryId;
        if (currentcat.categoryId == this.categoryId) {
          if (this.searchTerm == '') {
            this.catalogueService.removeBreadcrumbs();
          } else {
            if (currentcat.afterSearch) {
              // this.catalogueService.updateSubBreadcrumbsOptions(2, []);
              // this.catalogueService.updateSubBreadcrumbsOptions(3, false);
              console.log('need to remove applied filters');
            } else {
              console.log('no need to remove applied filters');
            }
          }
        } else {
          console.log('No need to remove breadcrumbs');
        }
      }
    }
  }

  goToPath(path) {
    console.log('Current selected breadcrumbs : ', path);
    if (path.categoryId == 0) {
      // this.catalogueService.resetBreadcrumbs();
      // window.history.back();
      // this.router.navigate(['../' + this.categoryId], { relativeTo: this.routes });
      this.router.navigate(['../pages/learning/catalogue']);
    } else {
      this.categoryId = path.categoryId;
      // this.catalogueService.removeAllAfterIndexFromBreadcrumbs(path);
      this.catalogueService.removeAllAfterIndexFromSubBreadcrumbs(path);
      this.fetchCatalogueCoursesForFirstPage(1, () => {
        console.log('Filters and Courses fetched by categories ...');
        this.loading = false;
      });
      // const updatedBreadCrumbs = this.catalogueService.getUpdatedBreadCrumbs(path);
      // console.log('Updated breacrumbs are : ', updatedBreadCrumbs);
      // if (updatedBreadCrumbs) {
      //   const breadcrumbs = {
      //     id: 1,
      //     subBreadCrumbs: updatedBreadCrumbs,
      //   };
      //   this.catalogueService.addSubBreadcrumbs(breadcrumbs);
      //   this.router.navigate(['../' + this.categoryId], { relativeTo: this.routes });
      // } else {
      //   // window.history.back();
      // }
    }
  }

  makeFiltersReadyIfApplied() {
    if (this.breadcrumbsList.length > 0) {
      const subBreadcrubms = this.breadcrumbsList[this.breadcrumbsList.length - 1];
      // const filters = this.breadcrumbsList[this.breadcrumbsList.length - 1].filters;
      // if (!subBreadcrubms.afterSearch) {
        this.filtersData = subBreadcrubms.filters;
        const appliedFiltersStr = this.catalogueService.makeFilterDataReady(this.filtersData);
        if (appliedFiltersStr) {
          this.sortStr = appliedFiltersStr.sort;
          this.priceStr = appliedFiltersStr.price;
          this.levelStr = appliedFiltersStr.level;
          this.tagStr = appliedFiltersStr.tag;
          this.appliedFilterData = this.filtersData;
        }
      // }
    }

    this.loading = true;
    this.fetchCatalogueCoursesForFirstPage(1, () => {
      console.log('Filters and Courses fetched by categories ...');
      this.loading = false;
    });
  }
  /************* for breadcrumbs end ***************/

  /********************** fetch courses and filters by category start **********************/
  fetchCatalogueCoursesForFirstPage(currentPageNo, cb) {
    this.currentPageNo = currentPageNo;
    this.learnParams = {
      catId: this.categoryId,
      pageno: this.currentPageNo,
      dataLimit: this.dataLimit,
      searchString: this.searchTerm,
      sort: this.sortStr,
      priceData: this.priceStr,
      levels: this.levelStr,
      tags: this.tagStr,
      empId: this.currentUserData ? this.currentUserData.eid : 'null',
      tId: this.currentUserData ? this.currentUserData.tenantId : this.tenantId,
    };
    this.getCatalogueCoursesByCat(this.learnParams, result => {
      if (result.type === true) {
        if (this.currentPageNo !== 1) {
          this.nextSearchedCourseList = result.courseData;
          this.totalPageCount = result.count[0].pageCount;
          this.totalCoursesCount = result.count[0].courseCount;
          this.searchedCourseList = this.searchedCourseList.concat(this.nextSearchedCourseList);
        } else {
          this.notFound = true;
          this.searchedCourseList = result.courseData;
          this.totalPageCount = result.count[0].pageCount;
          this.totalCoursesCount = result.count[0].courseCount;
          if (this.appliedFilterData.length > 0) {
            this.filtersData = this.appliedFilterData;
            if (this.searchTerm != '') {
              this.catalogueService.updateSubBreadcrumbsOptions(3, true);
            }
          } else {
            this.filtersData = result.filterdata;
            this.catalogueService.updateSubBreadcrumbsOptions(3, false);
          }
          this.catalogueService.updateSubBreadcrumbsOptions(2, this.filtersData);
          // if (this.searchTerm != '') {
          //   this.catalogueService.updateSubBreadcrumbsOptions(3, true);
          // }
        }
        // if (this.load.presentLoading) {
        //   this.load.dismiss();
        // }
        if (this.searchedCourseList.length > 0) {
          this.currentPageNo++;
        }
        cb(true);
      } else {
        console.log('err ', result);
        cb(false);
      }
    });
  }

  getCatalogueCoursesByCat(params: any, cb) {
    // if (this.currentPageNo === 1) {
    //   this.load.presentLoading('Please wait....');
    // }
    this.catalogueService.getCatalogueCoursesByCategories(this.learnParams).then(
      res => {
        if (res.type === true) {
          console.log(this.learnParams.pageNo, ' page Course list ', res);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.searching = false;
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
        cb(res);
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.searching = false;
        this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
      }
    );
  }

  applyFilter(event) {
    console.log('Applied filter data : ', event);
    if (event) {
      switch (event.type) {
          case 'apply': this.getCoursesByFilters(event.filter, event.updatedFilterData);
              break;
          case 'clear': this.clearAppliedFilters(event.filter);
              break;
          case 'subCat': this.goToSubCat(event.filter);
              break;
      }
    }
  }

  getCoursesByFilters(filter, updatedFilterData) {
    if (!this.loading) {
      this.sortStr = filter.sort,
      this.priceStr = filter.price,
      this.levelStr = filter.level,
      this.tagStr = filter.tag,
      this.appliedFilterData = updatedFilterData;
      this.loading = true;
      this.fetchCatalogueCoursesForFirstPage(1, () => {
        console.log('Filters and Courses fetched by applied filters ...');
        this.loading = false;
      });
    } else {
      console.log('Already fetching ...');
    }
  }

  clearAppliedFilters(filter) {
    if (!this.loading) {
      if (this.appliedFilterData.length > 0) {
        this.categoryId = filter.id;
        this.appliedFilterData = [];
        this.sortStr = '';
        this.priceStr = '';
        this.levelStr = '';
        this.tagStr = '';
        this.loading = true;
        this.fetchCatalogueCoursesForFirstPage(1, () => {
          console.log('Filters and Courses fetched by subcategories ...');
          this.loading = false;
        });
      } else {
        console.log('No existing filter is applied ...');
      }
    } else {
      console.log('Already fetching ...');
    }
  }

  goToSubCat(filter) {
    if (!this.loading) {
      this.categoryId = filter.id;
      this.searchTerm = '';
      this.appliedFilterData = [];
      this.sortStr = '';
      this.priceStr = '';
      this.levelStr = '';
      this.tagStr = '';

      const parentCat = this.makeBraedCrumbDataReady(filter);
      // this.catalogueService.addToBreadcrumbs(parentCat);
      this.catalogueService.updateSubBreadcrumbs(parentCat);

      this.loading = true;
      this.fetchCatalogueCoursesForFirstPage(1, () => {
        console.log('Filters and Courses fetched by subcategories ...');
        this.loading = false;
      });

      // this.router.navigate(['../' + this.categoryId], { relativeTo: this.routes });

    } else {
      console.log('Already fetching ...');
    }
  }

  makeBraedCrumbDataReady(data) {
    const path = {
      categoryId: data.id,
      categoryName: data.name,
      visible: true,
      searchStr: '',
      filters: [],
      afterSearch: false,
    };
    return path;
  }
  /********************** fetch courses and filters by category start **********************/
  detectChangesAfterViewChange() {
    setTimeout(() => {
      if (this.cdf && !(this.cdf as ViewRef).destroyed) {
        this.cdf.detectChanges();
      }
    }, 100);
  }
  /********* fetch courses and workflows start *********/
  subTabSelected(selectedTabIndex) {
    this.loading = true;
    const selectedTabData = this.selectedTabsArray[selectedTabIndex];
    console.log('Current selected tab : ', selectedTabIndex, selectedTabData);
    this.getSubTabData(selectedTabIndex, selectedTabData);
    // this.detectChangesAfterViewChange();
  }

  getMainTabData(currentIndex, currentTab) {
    this.loading = true;

    this.selectedMainTabIndex = currentIndex;
    this.selectedTabData = currentTab;
    this.selectedTabType = currentTab.type;
    this.subTabsData = currentTab.subTabs;
    this.selectedSubTabType = currentTab.subTabs[0].subType;
    this.category = String(currentTab.type).toLowerCase();
    this.subCategory = String(currentTab.subTabs[0].subType).toLowerCase();

    this.selectedSubTabIndex = 0;
    this.getSubTabData(this.selectedSubTabIndex, currentTab.subTabs[0]);
  }

  getSubTabData(currentIndex, currentSubTab) {
    this.selectedSubTabIndex = currentIndex;
    this.selectedSubTabType = currentSubTab.subType;
    this.selectedSubTabData = currentSubTab;
    this.subCategory = String(currentSubTab.subType).toLowerCase();
    // if (this.checkCurrentTabData()) {
    //   this.searchedCourseList = [];
    //   const mainTab = this.selectedMainTabIndex;
    //   const subTab = this.selectedSubTabIndex;
    //   this.searchedCourseList = this.mainTabsData[mainTab].subTabs[
    //     subTab
    //   ].courseList;
    //   this.currentPageNo =
    //     this.mainTabsData[mainTab].subTabs[subTab].currentPage + 1;
    //   this.totalPageCount = this.mainTabsData[mainTab].subTabs[
    //     subTab
    //   ].totalRows;
    //   this.totalCoursesCount = this.mainTabsData[mainTab].subTabs[
    //     subTab
    //   ].rowsTotalCount;
    //   this.differentTypeCoursesCount = this.mainTabsData[mainTab].subTabs[
    //     subTab
    //   ].differentTypeCoursesCount;
    //   // this.makeDifferentTypesOfCoursesCountReady(this.differentTypeCoursesCount);
    //   this.loading = false;
    // } else {
      this.fetchCoursesForFirstPage(1, () => {
        setTimeout(() => {
          this.calculateContainerHeight();
        }, 500);
        console.log('Courses fetched...');
        this.loading = false;
      });
    // }
  }

  makeCourseDataReady(courseList) {
    const tempData = {
      cPointLabel: 'Points',
      cLevleLabel: 'Begginer',
      cCoursePrice: 400,
      cAuthorName: 'Author Name',
      cCourseDiscount: 10,
      cOriginalPrice: 500,
      cDiscountPrice: 400,
      cisFav: 0,
    };
    if (courseList && courseList.length > 0) {
      for (let i = 0; i < courseList.length; i++) {
        const course = courseList[i];
        const formattedCourse = Object.assign(course, tempData);
        courseList[i] = formattedCourse;
      }
      return courseList;
    }
  }

  fetchCoursesForFirstPage(currentPageNo, cb) {
    this.currentPageNo = currentPageNo;
    this.learnParams = {
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
      uId: this.currentUserData.id,
      subType: this.selectedTabType === 'SELF' ? 'OPEN' : this.selectedSubTabType,
      // type:  this.selectedTabType === 'ALL' ? 'ALL' : 'SELF',
      // type: this.selectedTabType,
      // subType: 'CURRENT',
      type: 'ALL',
      pageLimit: this.dataLimit,
      pageNo: this.currentPageNo,
      searchString: this.searchTerm,
    };
    const param = this.learnParams;
    this.fetchCourseList(this.learnParams, this.currentSelectedMenu, res => {
      const result = res['data'];
      if (result.type === true && res['type'] === this.currentSelectedMenu)  {
        if (this.currentPageNo !== 1) {
          // const tempCourseList = result.data;
          // this.nextSearchedCourseList = this.makeCourseDataReady(tempCourseList);
          this.nextSearchedCourseList = result.courseData;
          // this.totalPageCount = result.lastPage;
          // this.totalCoursesCount = result.rowsTotalCount;
          this.totalPageCount = result.count[0].pageCount;
          this.totalCoursesCount = result.count[0].courseCount;
          // this.differentTypeCoursesCount = result.diffCourseCount;
          this.searchedCourseList = this.searchedCourseList.concat(this.nextSearchedCourseList);
          console.log(' this.searchedCourseList elsse', currentPageNo);
          console.log(' this.searchedCourseList elsse', this.currentPageNo);
          console.log(' this.searchedCourseList elsse',this.searchedCourseList);
        } else {
          this.notFound = true;
          // const tempCourseList = result.data;
          // this.searchedCourseList = this.makeCourseDataReady(tempCourseList);
          this.searchedCourseList = result.courseData;
          // this.totalPageCount = result.lastPage;
          // this.totalCoursesCount = result.rowsTotalCount;
          this.totalPageCount = result.count[0].pageCount;
          this.totalCoursesCount = result.count[0].courseCount;
          console.log(' this.searchedCourseList',this.searchedCourseList);
          // this.differentTypeCoursesCount = result.diffCourseCount;
        }
        // this.selectedTabsArray[this.selectedTabIndex].courseList = this.searchedCourseList;
        console.log(' this.searchedCourseList',this.searchedCourseList);
        this.updateCourseCountInSubTabArray(this.selectedMainTabIndex,result.count[0]);
        this.saveCurrentTabData(
          this.searchedCourseList,
          this.currentPageNo,
          this.totalPageCount,
          this.totalCoursesCount,
          this.differentTypeCoursesCount,
        );
        // if (this.load.presentLoading) {
        //   this.load.dismiss();
        // }
        if (this.searchedCourseList.length > 0) {
          this.currentPageNo++;
        }
        cb(true);
      } else {
        console.log('err ', result);
        cb(false);
      }
    });
    // this.infiniteScroll.disabled = false;
  }

  // checkIfDataToBebind(res, param){
  //   if (res['type'] === this.currentSelectedMenu){
  //     return true;
  //   }else {
  //     if(this.selectedTabType === param['subType']){
  //       return false;
  //     }else {
  //       return true;
  //     }
  //   }

  // }

  fetchCourseList(params: any, type: any, cb) {
    // if (this.currentPageNo === 1) {
    //   this.load.presentLoading('Please wait....');
    // }
    this.learnService.getLearnCourseList(this.learnParams, type).then(
      res => {
        if (res['data'].type === true) {
          console.log(this.learnParams.pageNo, ' page Course list ', res);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.searching = false;
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
        cb(res);
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.searching = false;
        this.toastr.prsentToast('Unable to get results at this time.', 'Failed!', 'error');
      }
    );
  }

  checkCurrentTabData() {
    const mainTab = this.selectedMainTabIndex;
    const subTab = this.selectedSubTabIndex;
    if (this.mainTabsData[mainTab].subTabs[subTab].courseList.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  saveCurrentTabData(courseList, currentPage, totalRows, totalCoursesCount, differentTypeCoursesCount) {
    const mainTab = this.selectedMainTabIndex;
    const subTab = this.selectedSubTabIndex;
    this.mainTabsData[mainTab].subTabs[subTab].courseList = courseList;
    this.mainTabsData[mainTab].subTabs[subTab].currentPage = currentPage;
    this.mainTabsData[mainTab].subTabs[subTab].totalRows = totalRows;
    this.mainTabsData[mainTab].subTabs[
      subTab
    ].rowsTotalCount = totalCoursesCount;
    this.mainTabsData[mainTab].subTabs[
      subTab
    ].differentTypeCoursesCount = differentTypeCoursesCount;
  }
  /********* fetch courses and workflows end *********/

  /**************** infinite scroll start **************/
  onScrollDown (ev) {
    console.log('scrolled down!!', ev);
    this.direction = 'down';
    // setTimeout(() => {
      if (this.currentPageNo > this.totalPageCount) {
        this.infiniteScrollDisabled = true;
      } else {
        if (!this.working) {
          this.working = true;
          if (this.currentSelectedMenu == 0) {
            this.fetchCatalogueCoursesForFirstPage(this.currentPageNo, () => {
              this.working = false;
              console.log('Done');
              this.infiniteScrollDisabled = false;
            });
          } else {
            this.fetchCoursesForFirstPage(this.currentPageNo, () => {
              this.working = false;
              console.log('Done');
              setTimeout(() => {
                this.calculateContainerHeight();
              }, 500);
              this.infiniteScrollDisabled = false;
            });
          }
        }
      }
    // }, 500);
  }

  onScrollUp(ev) {
    console.log('scrolled up!', ev);
    this.direction = 'up';
  }
/**************** infinite scroll end **************/

  performAction(event, i, tabData) {
    if (event) {
        switch (event.type) {
            case 'goToCourse': this.goToModules(event.cardData, i, tabData);
                break;
            case 'goToWorflow': this.goToModules(event.cardData, i, tabData);
                break;
            case 'goToOpenCourse': this.goToOpenCourseDetails(event.cardData);
                break;
            case 'enrolSelf': this.enrolSelfCourses(event.cardData);
                break;
            case 'likeDislike': this.submitCourseFeedback(event.cardData, event.liked);
                break;
            case 'share': this.shareCourse(event.cardData);
                break;
            case 'wishlist': this.wishlistCourse(event.cardData, event.liked);
                break;
            case 'buy': this.buyCourse(event.cardData);
                break;
        }
    }
  }

  /********* go to course datils or workflow details start *********/
  goToModules(currentCourse, currentIndex, tabData) {
    console.log('Selected course : ', currentCourse, 'from tab : ', tabData);
    // if (this.currentSelectedMenu == 0) {
    //   this.goToCourse();
    // } else if (this.currentSelectedMenu == 1) {
    //   this.goToCourse();
    // } else if (this.currentSelectedMenu == 2) {
    //   this.goToPathway();
    // }
    if (currentCourse.isWorkFlow == '1') {
      if (this.currentSelectedMenu == 0) {
        this.goToPathway(this.currentSelectedMenu, currentCourse);
      } else {
        this.goToPathway(this.currentSelectedMenu, currentCourse);
        // this.learnService.setWorkflowData(currentCourse);
        // this.router.navigate(['../../workflow_learn'], { relativeTo: this.routes });
      }
    } else {
      if (this.currentSelectedMenu == 0) {
        this.goToCourse(this.currentSelectedMenu, currentCourse);
      } else {
        // this.moduleService.course = currentCourse;
        if (currentCourse.noOfmodules > 0) {
          this.learnServiceProvider.setcourseDetailList(currentCourse);
          // this.router.navigate(['../../module-activity'], { relativeTo: this.routes });
          this.router.navigate(['/pages/module-activity']);
        } else {
          console.log('No modules avalable');
          this.toastr.prsentToast('No modules avalable in this course', 'Not available!', 'warning');
        }
      }
    }
  }

  goToCourse(selectedMenu, currentCourse) {
    // this.router.navigate(['../course/' + selectedMenu], { relativeTo: this.routes });
    const courseId = currentCourse.courseId;
    this.router.navigate(['./course/' + courseId], { relativeTo: this.routes });
  }

  goToPathway(selectedMenu, currentCourse) {
    const wfId = selectedMenu == 0 ? 0 : currentCourse.workflowid;
    // this.router.navigate(['../pathway/details/' + wfId], { relativeTo: this.routes });
    this.router.navigate(['pages/learning/pathway/details/' + wfId]);
  }

  goToOpenCourseDetails(currentCourse) {
    const courseId = currentCourse.id;
    this.router.navigate(['./course/' + courseId], { relativeTo: this.routes });
  }

  /********* go to course datils or workflow details end *********/

  /********* enrol self courses start *********/
  enrolSelfCourses(courseData) {
    const param = {
      enrolId: courseData.enrolId,
      ttt_workflowId: courseData.ttt_workflowId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.selfEnrolUserCourses(param).then(
      res => {
        if (res.type === true) {
          console.log(' Enrol Course res ', res);
          this.toastr.prsentToast('Course enroled successfully', 'Success!', 'success');
          // this.load.dismiss();
          // this.fetchCoursesForFirstPage();
          this.fetchCoursesForFirstPage(1, () => {
            setTimeout(() => {
              this.calculateContainerHeight();
            }, 500);
            console.log('Course List updated after enrolment...');
          });
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to enrol course at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to enrol course at this time', 'Failed!', 'error');
      }
    );
  }
  /********* enrol self courses end *********/

  /********* submit course feedback start *********/
  submitCourseFeedback(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        // courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          // courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        // courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        // courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          // courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        // courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.id,
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.submitUserCoursesFeedback(param).then(
      res => {
        if (res.type === true) {
          console.log(' Course feedback add res ', res);
          // this.load.dismiss();
          this.updateCourseFeedbackData(courseData, feedbackType);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
      },
    );
  }

  removeCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.id,
      eId: this.currentUserData.eid,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.removeUserCoursesFeedback(param).then(
      res => {
        if (res.type === true) {
          console.log(' Course feedback remove res ', res);
          // this.load.dismiss();
          this.updateCourseFeedbackData(courseData, feedbackType);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log(err);
        this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
      },
    );
  }

  updateCourseFeedbackData(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        courseData.isLike = 1;
      } else {
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        courseData.isDislike = 1;
      } else {
        courseData.isDislike = 0;
      }
    }
  }
  /********* submit course feedback end *********/

  /********* share course start *********/
  shareCourse(courseData) {
    console.log('Course share clicked : ', courseData);
  }
  /********* share course end *********/

  /********* wishlist course start *********/
  wishlistCourse(courseData, wishlistType) {
    console.log('Course wishlist clicked : ', courseData);
    if (this.currentUserData) {
      if (wishlistType) {
        if (wishlistType === 0) {
          wishlistType = 1;
        } else {
          wishlistType = 0;
        }
      } else {
        wishlistType = 1;
      }
      const param = {
        cId: courseData.courseId,
        aId: 2,
        check: wishlistType,
        uId: this.currentUserData.id,
        eId: this.currentUserData.eid,
        tId: this.currentUserData.tenantId,
      };
      // this.load.presentLoading('Please wait....');
      this.catalogueService.addCourseToWishlist(param).then(
        res => {
          if (res.type === true) {
            console.log(' Course feedback add res ', res);
            // this.load.dismiss();
            this.updateCourseWishlistData(courseData, wishlistType);
          } else {
            // this.load.dismiss();
            console.log('err ', res);
            this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
          }
        },
        err => {
          // this.load.dismiss();
          console.log(err);
          this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
        },
      );
    } else {
      this.toastr.prsentToast('Please sign in to add course to wishlist', 'Sign In!', 'warning');
    }
  }

  updateCourseWishlistData(courseData, wishlistType) {
    if (this.currentSelectedMenu == 4 && wishlistType == 0) {
      this.loading = true;
      this.fetchCoursesForFirstPage(1, () => {
        setTimeout(() => {
          this.calculateContainerHeight();
        }, 500);
        console.log('Fetch updated wishlisted courses ...');
        this.loading = false;
      });
    } else {
      // if (courseData.wishList === 0) {
      //   courseData.wishList = 1;
      // } else {
      //   courseData.wishList = 0;
      // }
      courseData.wishList = wishlistType;
    }
  }
  /********* wishlist course end *********/

  /********* buy course start *********/
  buyCourse(courseData) {
    console.log('Course buy clicked : ', courseData);
    this.goToCourse(this.currentSelectedMenu, courseData);
  }
  /********* buy course end *********/

  // onScroll() {
  //   this.isLoading$ = true;
  //   this.fetchCourses();
  // }

  /********** some demo start ********/
  // tempList: any = [];
  // // private tempList: Observable<any[]>;
  // fetchTempList() {
  //   // this.learnService.getTempList().then(
  //   //   res => {
  //   //     // console.log('res ', res);
  //   //     this.tempList = res.results;
  //   //     // this.tempList = res;
  //   //     // if (this.tempList && this.tempList.length > 0) {
  //   //       this.tempList = this.tempList.pipe(
  //   //         // lazyArray(),
  //   //         lazyArray(0, 1),
  //   //       );
  //   //     // }
  //   //     // this.makeDataReady();
  //   //     console.log('templist : ', this.tempList);
  //   //   },
  //   //   err => {
  //   //     console.log(err);
  //   //   },
  //   // );

  //   //Create a function to accept Observable instance
  //   const calculationFun = pipe(
  //     lazyArray(0, 1),
  //   );
  //   //Instantiate response Observable
  //   const calculation$ = calculationFun(
  //     this.learnService.getTempUsersList()
  //   );
  //   //Subscribe the Observable instance
  //   calculation$.subscribe(
  //     result => {
  //       console.log(result);
  //     }
  //   );

  //   // this.learnService.getTempUsersList().pipe(
  //   //   // retry(3),
  //   //   // map(books => {
  //   //   //   if (books.results.length < 5) {  //It will throw error in console
  //   //   //     throw new Error('Not enough books');
  //   //   //   }
  //   //   //   return books.results;
  //   //   // }),
  //   //   // catchError(err => {
  //   //   //   console.error(err);
  //   //   //   return of([]);
  //   //   // }),
  //   //   // lazyArray(0, 1),
  //   // )
  //   // .subscribe(books => {
  //   //   this.tempList = books;
  //   //   console.log('templist : ', this.tempList);
  //   // });
  // }

  // async fetchTempList2() {
  //   // this.tempList = [];
  //   await this.learnService.getTempList().then(
  //     res => {
  //       // console.log('res ', res);
  //       // let tempList1 = [];
  //       this.tempList = res.results;
  //       this.tempList = this.tempList.pipe(
  //         // lazyArray(),
  //         lazyArray(0, 1),
  //       );
  //       console.log('templist : ', this.tempList);
  //     },
  //     err => {
  //       console.log(err);
  //     },
  //   );
  // }

  // async makeDataReady() {
  //   setTimeout(() => {
  //     // response.pipe(process.stdout);
  //     this.tempList = this.tempList.pipe(
  //       lazyArray(0, 1),
  //     );
  //   }, 2000);
  // }
  /********** some demo end ********/

  calculateContainerHeight() {
    let B = document.body,
        H = document.documentElement,
        height, tempContainerHeight;

    if (typeof document['height'] !== 'undefined') {
        height = document['height'] // For webkit browsers
    } else {
        height = Math.max( B.scrollHeight, B.offsetHeight,H.clientHeight, H.scrollHeight, H.offsetHeight );
    }
    console.log('Document height is : ', height);

    if (this.currentSelectedMenu != 0 && this.currentSelectedMenu != 3 && this.currentSelectedMenu != 4) {
      const header = document.getElementsByTagName('ngx-header');
      const tabsets = document.getElementsByClassName('nav-tabset');
      let tempContentHeight = 0;
      let tempTabsetHeight = 0;
      if(header && header.length !=0){
         tempContentHeight = header[0]['clientHeight'];
      }
      if(tabsets && tabsets.length !=0){
        tempTabsetHeight = tabsets[0]['clientHeight'];
     }
      // const tempTabsetHeight = document.getElementsByClassName('nav-tabset')[0].clientHeight;
      tempContainerHeight = height - tempContentHeight - tempTabsetHeight - 24;
    } else if (this.currentSelectedMenu == 0) {
      const header = document.getElementsByTagName('ngx-header');
      const breadcrumbs = document.getElementsByClassName('breadcrumbs');
      let tempContentHeight = 0;
      let tempBreadcrumbsHeight = 0;
      if(header && header.length !=0){
        tempContentHeight = header[0]['clientHeight'];
     }
     if(breadcrumbs &&  breadcrumbs.length !=0){
      tempBreadcrumbsHeight = breadcrumbs[0]['clientHeight'];
    }
      // const tempContentHeight = document.getElementsByTagName('ngx-header')[0].clientHeight;
      // const tempBreadcrumbsHeight = document.getElementsByClassName('breadcrumbs')[0].clientHeight;
      tempContainerHeight = height - tempContentHeight - tempBreadcrumbsHeight - 24;
    } else {
      const header = document.getElementsByTagName('ngx-header');
      let tempContentHeight = 0;
      if(header  && header.length !=0){
        tempContentHeight = header[0]['clientHeight'];
     }
      // const tempContentHeight = document.getElementsByTagName('ngx-header')[0].clientHeight;
      tempContainerHeight = height - tempContentHeight - 24;
    }
    const searchRes = document.getElementById("search-res");
    if(searchRes){
      searchRes.style.height = tempContainerHeight + 'px';
      searchRes.style.overflowY = 'scroll';
      searchRes.style.overflowX = 'hidden';
    }

  }

  ngOnDestroy() {
    // this.catalogueService.resetBreadcrumbs();
    this.removeBackbuttonEventListner();
  }

  updateCourseCountInSubTabArray(mainTabIndex, data){
    console.log('mainTabIndex ==>', mainTabIndex);
    console.log('data ==>', data);
    console.log('mainDataTab ==>', this.mainTabsData[mainTabIndex]);
    if(this.mainTabsData[mainTabIndex] &&
      this.mainTabsData[mainTabIndex]['subTabs'] &&
      this.mainTabsData[mainTabIndex]['subTabs'].length !== 0){
        const subTabArray = this.mainTabsData[mainTabIndex]['subTabs'];
        for (let index = 0; index < subTabArray.length; index++) {
          const element = subTabArray[index];
          if(element['subType'] === 'CURRENT') {
            element['totalCourseCount'] = data['crntCount'] ? data['crntCount'] : 0;
          }
          if(element['subType'] === 'UPCOMING') {
            element['totalCourseCount'] = data['upCount'] ?  data['upCount'] : 0;
          }
          if(element['subType'] === 'COMPLETED') {
            element['totalCourseCount'] = data['compCount'] ? data['compCount'] : 0;
          }
          element['preparedTabName'] = element['name'] + ' ( ' + element['totalCourseCount'] + ' )';
        }
    }
  }

  getMonthText(courseData) {
    if (courseData.year == this.currentYear) {
        if (courseData.month == this.previousMonth) {
            return 'Previous month';
        } else if (courseData.month == this.currentMonth) {
            return 'This month';
        } else if (courseData.month == this.nextMonth) {
            return 'Next month';
        } else {
            return courseData.monthname + ' ' + courseData.year;
        }
    } else {
        return courseData.monthname + ' ' + courseData.year;
    }
  }

  logData(data){
    console.log('data', data)
  }
}

