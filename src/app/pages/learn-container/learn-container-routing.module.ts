import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LearnContainerComponent } from './learn-container.component';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { LearningPathwayComponent } from './learning-pathway/learning-pathway.component';
import { LearnCoursesComponent } from './learn-courses/learn-courses.component';
import { LearnSearchContainerComponent } from './learn-search-container/learn-search-container.component';

const routes: Routes = [
  {
    path: '',
    component: LearnContainerComponent,
    children: [
      {
        path: 'learn-search-container',
        component: LearnSearchContainerComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: 'learn-search-container/:search',
        component: LearnSearchContainerComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'catelog',
        path: 'catalogue',
        component: CatalogueComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'catelog',
        path: 'catalogue/:search',
        component: CatalogueComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'catelog',
        path: 'catalogue/:search/category/:id',
        component: LearnCoursesComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'course',
        path: 'catalogue/:search/category/:id/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'course',
        path: 'catalogue/:search/category/:id/:search/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'catelog',
        path: 'catalogue/category/:id',
        component: LearnCoursesComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'catelog',
        path: 'catalogue/category/:id/:search',
        component: LearnCoursesComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'course',
        path: 'catalogue/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'course',
        path: 'catalogue/:search/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'course',
        path: 'catalogue/category/:id/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'course',
        path: 'catalogue/category/:id/:search/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        // path: 'pathway',
        path: 'catalogue/pathway/:id',
        component: LearningPathwayComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: ':id',
        component: LearnCoursesComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: ':id/:search',
        component: LearnCoursesComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: ':id/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: ':id/:search/course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      // {
      //   path: 'course',
      //   component: LearnCoursesComponent,
      //   runGuardsAndResolvers: 'always',
      // },
      // {
      //   path: 'course/:search',
      //   component: LearnCoursesComponent,
      //   runGuardsAndResolvers: 'always',
      // },
      {
        path: 'course/:id',
        component: CourseDetailsComponent,
        runGuardsAndResolvers: 'always',
      },
      // {
      //   path: 'pathway',
      //   component: LearnCoursesComponent,
      //   runGuardsAndResolvers: 'always',
      // },
      // {
      //   path: 'pathway/:search',
      //   component: LearnCoursesComponent,
      //   runGuardsAndResolvers: 'always',
      // },
      {
        path: 'pathway/details/:id',
        component: LearningPathwayComponent,
        runGuardsAndResolvers: 'always',
      },

      // {
      //   path: 'self',
      //   component: LearnCoursesComponent,
      //   runGuardsAndResolvers: 'always',
      // },
      // {
      //   path: 'self/:search',
      //   component: LearnCoursesComponent,
      //   runGuardsAndResolvers: 'always',
      // },
    ],
  },
  // {
  //   path: 'learn-container/course',
  //   component: CourseDetailsComponent,
  // },
  // {
  //   path: 'learn-container/pathway',
  //   component: LearningPathwayComponent,
  // },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    // RouterModule.forRoot(routes, {
    //   onSameUrlNavigation: 'reload',
    // }),
  ],
  exports: [
    RouterModule,
  ],
})
export class LearnContainerRoutingModule { }
