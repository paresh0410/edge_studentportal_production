import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { LearnServiceProvider } from '../../../service/learn-service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { ENUM } from '../../../service/enum';
import { CourseDetailServiceProvider } from '../../../service/course-detail-service';
import { CatalogueService } from '../../../service/learn-container/catalogue.service';
import { ToasterService } from '../../../service/learn-container/toaster.service';
import { AuthServiceProvider } from '../../../service/auth-service';
import { LearnService } from '../../../service/learn-container/learn.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare var Razorpay: any;

@Component({
  selector: 'ngx-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss'],
})
export class CourseDetailsComponent implements OnInit, OnDestroy {

  coupon: boolean = true;
  preview: any = [];
  modules: any = [];

  selectedMenuForCourse: any;
  currentUserData: any;
  courseData: any;
  moduleList: any;

  loading: boolean = false;
  noData: boolean = false;

  noDataFoundContainer = {
    image: 'assets/images/no-data.svg',
    title: 'Course details not available',
    discription: 'Course details you are looking for are not available this time, please try after some time.',
  };

  tenantDetails: any;
  tenantId: any;

  orderCreationData: any;
  orderConfigOptions: any;

  userDetails = {
    recipientName: '',
    recipientEmail: '',
    recipientPhone: '',
    amount: null,
  };

  data = {
    razorpay_payment_id: null,
    razorpay_order_id: null,
    razorpay_signature: null,
  };

  paymentStatus = 0;
  transactionCompleted: boolean = false;
  courseEnroled: boolean = false;
  updateCalled: boolean = false;
  hideSkeleton: any = false;

  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToasterService,
    public learnServiceProvider: LearnServiceProvider,
    public learnService: LearnService,
    public moduleService: CourseDetailServiceProvider,
    public catalogueService: CatalogueService,
    private cdf: ChangeDetectorRef,
    public ASP: AuthServiceProvider,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.routes.queryParams.subscribe(queryParams => {
        // do something with the query params
    });
    this.routes.params.subscribe(routeParams => {
        this.selectedMenuForCourse = routeParams.id;
        this.getUserData();
        this.fetchCourseDataWithModules();
    });
    // this.getCourseDataFromService();
  }

  showCoupon() {
    this.coupon = !this.coupon;
  }

  getUserData() {
    if (localStorage.getItem('userdetail')) {
      this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
      this.currentUserData['eid'] = JSON.parse(localStorage.getItem('employeeId'));
    } else {
      console.log('User not logged in...');
      this.tenantDetails =  this.ASP.getDomainBasedTenant();
      console.log('tenant details --->', this.tenantDetails);
      this.tenantId = this.tenantDetails.tenantId  ? this.tenantDetails.tenantId : 1;
    }
  }

  /********** get course details start **********/
  // getCourseDataFromService() {
  //   this.courseData = this.learnService.getWorkflowData();
  //   console.log("course data from service :====>", this.courseData);
  //   if (this.courseData) {
  //     this.fetchCourseDataWithModules();
  //   }
  // }

  fetchCourseDataWithModules() {
    // this.spinner.show();
    this.loading = true;
    const param = {
      courseId: this.selectedMenuForCourse,
      uId: this.currentUserData ? this.currentUserData.id : null,
      empId:  this.currentUserData ? this.currentUserData.eid : null,
      tId: this.currentUserData ? this.currentUserData.tenantId : this.tenantId,
    };
    this.catalogueService.getCatalogueCoursesDetails(param).then(
      res => {
        this.loading = false;
        if (res.type === true) {
          console.log('Course details res : ', res);
          this.courseData = res.courseData;
          if (this.courseData.moduleData.length == 0) {
            this.noData = true;
          }
          this.hideSkeleton = true;
          // cb(res);
        } else {
          // this.load.dismiss();
          console.log('err ', res);
          this.toastr.prsentToast('Unable to get results at this time.', 'Warning!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log('err ', err);
        this.loading = false;
        this.toastr.prsentToast('Unable to get results at this time.', 'Failed!', 'error');
      }
    );
  }
  /********** get course details end **********/

  /********* wishlist course start *********/
  wishlistCourse(courseData, wishlistType) {
    console.log('Course wishlist clicked : ', courseData);
    if (this.currentUserData) {
      if (wishlistType) {
        if (wishlistType === 0) {
          wishlistType = 1;
        } else {
          wishlistType = 0;
        }
      } else {
        wishlistType = 1;
      }
      const param = {
        cId: courseData.courseId,
        aId: 2,
        check: wishlistType,
        uId: this.currentUserData.id,
        eId: this.currentUserData.eid,
        tId: this.currentUserData.tenantId,
      };
      // this.load.presentLoading('Please wait....');
      this.catalogueService.addCourseToWishlist(param).then(
        res => {
          console.log(' Course feedback add res ', res);
          if (res.type === true) {
            // this.load.dismiss();
            this.updateCourseWishlistData(courseData, wishlistType);
          } else {
            // this.load.dismiss();
            this.toastr.prsentToast('Unable to update at this time', 'Warning!', 'warning');
          }
        },
        err => {
          // this.load.dismiss();
          console.log(err);
          this.toastr.prsentToast('Unable to update at this time', 'Failed!', 'error');
        },
      );
    } else {
      this.toastr.prsentToast('Please sign in to add course to wishlist', 'Sign In!', 'warning');
    }
  }

  updateCourseWishlistData(courseData, wishlistType) {
    // if (courseData.wishList === 0) {
    //   courseData.wishList = 1;
    // } else {
    //   courseData.wishList = 0;
    // }
    courseData.wishlisted = wishlistType;
    this.courseData.wishlisted = wishlistType;
  }
  /********* wishlist course end *********/

  /********* buy course start *********/
  generateItemStr(courseData) {
    let str = '';
    // return str = 2 + '%hash%' + courseData.courseId + '%hash%' + courseData.cOriginalPrice + '%hash%' + courseData.cDiscountPrice;
    return str = 2 + '%hash%' + courseData.courseId + '%hash%' + 0 + '%hash%' + 0;
  }

  buyCourse(courseData) {
    console.log('Course buy clicked : ', courseData);
    if (this.currentUserData) {
      if (courseData.isFree == 0) {
        const param = {
          receiptid: null,
          amount: courseData.cDiscountPrice == 'NA' ? 1 : courseData.cDiscountPrice,
          currency: 'INR',
          itemStr: this.generateItemStr(courseData),
        };
        // this.load.presentLoading('Please wait....');
        this.catalogueService.createPurchaseOrder(param).then(
          res => {
            console.log('Course purchase res ', res);
            if (res.type === true) {
              this.orderCreationData = res.orderDetails[0];
              this.orderConfigOptions = res.orderConfig;
              this.openRazorPay(this.orderCreationData);
              // this.load.dismiss();
            } else {
              // this.load.dismiss();
              this.toastr.prsentToast('Unable to process your request at this time', 'Warning!', 'warning');
            }
          },
          err => {
            // this.load.dismiss();
            console.log('Course purchase err ', err);
            this.toastr.prsentToast('Unable to process your request at this time', 'Failed!', 'error');
          },
        );
      } else {
        const param = {
          enrolmodeId: 6,
          instanceId: courseData.courseId,
          areaId: 2,
          uId: this.currentUserData.id,
          empId: this.currentUserData.eid,
          tId: this.currentUserData.tenantId,
        };
        this.spinner.show();
        // this.load.presentLoading('Please wait....');
        this.catalogueService.enrolFreeCourses(param).then(
          res => {
            console.log('Course enrol res ', res);
            const enrolRes = res.result[0];
            if (res.type === true) {
              this.spinner.hide();
              // this.load.dismiss();
              if (enrolRes.flag == 1) {
                this.toastr.prsentToast('Thank you for purchasing this course', 'Enrolled successfully!', 'success');
                this.courseEnroled = true;
                // this.fetchCourseDataWithModules();
                this.goBack();
              } else {
                this.toastr.prsentToast('Unable to process your request at this time', 'Warning!', 'warning');
              }
            } else {
              // this.load.dismiss();
              this.toastr.prsentToast('Unable to process your request at this time', 'Warning!', 'warning');

            }

          },
          err => {
            // this.load.dismiss();
            this.spinner.hide();
            console.log('Course enrol err ', err);
            this.toastr.prsentToast('Unable to process your request at this time', 'Failed!', 'error');
          },
        );
      }
    } else {
      // this.toastr.prsentToast('Please sign in to enrol this course', 'Sign In error!', 'warning');
      this.goToLogin();
    }
  }

  goToLogin() {
    const returnurl = window.location.hash;
    if (returnurl && returnurl !== '/' && returnurl !== '#/') {
      if (localStorage.hasOwnProperty('redirectURL') && localStorage.getItem('redirectURL') !== '/logout') {

      } else {
        localStorage.setItem('redirectURL', returnurl.replace('#', ''));
        this.router.navigate(['..' + returnurl.replace('#', '')], { relativeTo: this.routes });
      }
    }
    this.router.navigate(['../login-new']);
  }

  openRazorPay(res) {
    const _this = this;
    const options = {
      key: this.orderConfigOptions.key, // Enter the Key ID generated from the Dashboard
      amount: this.orderConfigOptions.amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
      currency: this.orderConfigOptions.currency,
      name: this.orderConfigOptions.name,
      description: this.orderConfigOptions.description,
      image: this.orderConfigOptions.image,
      order_id: res.orderkey, //Pass the `id` obtained in the response of Step 1
      handler: (response) => {
        console.log('response.razorpay_payment_id :', response.razorpay_payment_id);
        console.log('response.razorpay_order_id :', response.razorpay_order_id);
        console.log('response.razorpay_signature :', response.razorpay_signature);
        if (response !== null) {
          this.data = {
            razorpay_payment_id: response.razorpay_payment_id,
            razorpay_order_id: response.razorpay_order_id,
            razorpay_signature: response.razorpay_signature,
          };
          this.paymentStatus = 1;
          this.transactionCompleted = true;
          this.updatePaymentDetails();
        } else {
          this.toastr.prsentToast('Unable to complete transaction at this time', 'Payment failed!', 'error');
        }
      },
      prefill: {
        name: '',//this.currentUserData.firstname + ' ' + this.currentUserData.lastname,
        email: '',//this.currentUserData.email !== '' ? this.currentUserData.email : 'abc@xyz.com',
        contact: '',//this.currentUserData.phone ? this.currentUserData.phone : 9876543210,
      },
      notes: this.orderConfigOptions.notes,
      theme: this.orderConfigOptions.theme,
      modal: {
        backdropclose: false,
        escape: false,
        ondismiss: function() {
          console.log('Checkout form closed');
          _this.paymentStatus = 0;
          _this.transactionCompleted = true;
          _this.updatePaymentDetails();
          _this.updateCalled = true;
        },
      },
    };
    const razorpay = new Razorpay(options);
    razorpay.open();
  }

  updatePaymentDetails() {
    if (!this.updateCalled) {
      if(this.paymentStatus == 1){
        this.spinner.show();
      }
      const param = {
        orderkey: this.orderCreationData.orderkey,
        orderId: this.orderCreationData.orderId,
        isSuccess: this.paymentStatus,
      };
      // this.load.presentLoading('Please wait....');
      this.catalogueService.updatePurchaseOrder(param).then(
        res => {
          console.log('update Course purchase details res ', res);
          const orderRes = res.transactionData[0];
          if (res.type === true) {
            // this.load.dismiss();
            if(this.paymentStatus == 1){
              this.spinner.hide();
            }
            if (this.transactionCompleted) {
              if (orderRes.status == 1) {
                this.toastr.prsentToast('Thank you for purchasing this course', 'Payment successfull!', 'success');
                this.courseEnroled = true;
                // this.fetchCourseDataWithModules();
                // this.goBack();
                this.router.navigate(['pages/learning/course']);
              } else {
                this.toastr.prsentToast('Unable to purchase this course at this time', 'Payment failed!', 'warning');
              }
              this.transactionCompleted = false;
            }
          } else {
            // this.load.dismiss();
            this.toastr.prsentToast('Unable to complete your transaction at this time', 'Payment failed!', 'warning');
          }
        },
        err => {
          // this.load.dismiss();
          console.log('update Course purchase details ', err);
          this.toastr.prsentToast('Unable to update and get purchase details at this time', 'Failed!', 'error');
        },
      );
    } else {
      console.log('Already called.....');
    }
  }
  /********* buy course end *********/

  /********* enrol self courses start *********/
  enrolSelfCourses(courseData) {
    const param = {
      enrolId: courseData.enrolId,
      ttt_workflowId: courseData.ttt_workflowId,
    };
    // this.load.presentLoading('Please wait....');
    this.learnService.selfEnrolUserCourses(param).then(
      res => {
        if (res.type === true) {
          console.log(' Enrol Course res ', res);
          const enrolSelRes = res.data[0];
          // this.load.dismiss();
          if (enrolSelRes[0].approvalFlag == 1 && enrolSelRes[0].enrolId == 0) {
            this.toastr.prsentToast('Course enrol request sent successfully', 'Success!', 'success');
          } else {
            this.courseEnroled = true;
            this.toastr.prsentToast('Course enroled successfully', 'Success!', 'success');
          }
          // this.fetchCourseDataWithModules();
          this.goBack();
        } else {
          // this.load.dismiss();
          console.log('enrolSelfCourses err ', res);
          this.toastr.prsentToast('Unable to enrol course at this time', 'Failed!', 'warning');
        }
      },
      err => {
        // this.load.dismiss();
        console.log('enrolSelfCourses err ',err);
        this.toastr.prsentToast('Unable to enrol course at this time', 'Failed!', 'error');
      }
    );
  }
  /********* enrol self courses end *********/

  goToLearn(enrolMode) {
    console.log('Enrol mode is : ', enrolMode);
    // if (enrolMode) {
    //   this.router.navigate(['pages/learning/course']);
    // } else {
    //   this.router.navigate(['pages/learning/course']);
    // }
    this.router.navigate(['pages/learning/course']);
  }

  showdiv(i, data) {
    console.log('data', data);
    if (this.modules[i].activities.length > 1) {
      for (let k = 0; k < this.modules.length; k++) {
        if (k !== i) {
          this.modules[k].show = false;
        }
      }
    }
    this.modules[i].show = !this.modules[i].show;
  }

  bindBackgroundImage(img) {
    if (img === null || img === '' || img === 'assets/images/courseicon.jpg') {
      // return 'url(' + 'assets/images/card.jpg' + ')';
      return 'assets/images/card.jpg';
    } else {
      return img;
    }
  }

  goBack() {
    window.history.back();
  }

  ngOnDestroy() {
    if (this.orderCreationData && this.paymentStatus == 0) {
      this.updatePaymentDetails();
    }
  }
}
