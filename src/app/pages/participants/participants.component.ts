import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { TrainerAutomationServiceProvider } from '../../service/trainer-automation.service';
import { ENUM } from '../../service/enum';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ParticipantsComponent implements OnInit {

  participants: any = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['name', 'employeeId', 'mobile', 'email', 'designation'];
  trainerCourseId: any;
  trainerCourseName: any;
  trainerCourseDescription: any;
  batchCourseId: any;
  batchCourseName: any;
  batchCourseDescription: any;
  userDetails: any;
  participant = new MatTableDataSource(this.participants);
  tenantId: any;
  batchdata: any = [];
  constructor(private TAServiceProvider: TrainerAutomationServiceProvider, public spinner: NgxSpinnerService) {

    console.log('USHA PARTICIPANTS');
    this.batchdata = this.TAServiceProvider.batchData;
    // if (this.TAServiceProvider.batchData) {
    //   this.trainerCourseId = this.TAServiceProvider.batchData.trainerCourseId;
    //   this.trainerCourseName = this.TAServiceProvider.batchData.trainerCourseName;
    //   this.trainerCourseDescription = this.TAServiceProvider.batchData.trainerCourseDescription;
    //   this.batchCourseId = this.TAServiceProvider.batchData.batchCourseId;
    //   this.batchCourseName = this.TAServiceProvider.batchData.batchCourseName;
    //   this.batchCourseDescription = this.TAServiceProvider.batchData.batchCourseDescription;
    // }

    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));

    this.tenantId = this.userDetails.tenantId;
    this.getAllParticipants();
  }

  ngOnInit() {
    this.participants.paginator = this.paginator;

  }

  noParticipants: boolean = false;
  getAllParticipants() {
    this.spinner.show();
    let url = ENUM.domain + ENUM.url.getAllUshaParticipants;
    let param = {
      'reqFrom': 'WEB',
      'participantCourseId': this.batchdata.batchId,
      'tId': this.tenantId,
      'lmt': null,
      'pNo': null,
      'flag': 1,
      'actId': null,
      'wfId': null,
      'stepId': null,
    }
    this.TAServiceProvider.getParticipant(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('RESULT Success participant===>', result);
        if (result.data.length == 0) {
          this.noParticipants = true;
        } else {
          this.noParticipants = false;
          this.participants = result.data;
        }

      }).catch(result => {
        this.spinner.hide();
        console.log('RESULT Error participant===>', result);
      })
  }

}
