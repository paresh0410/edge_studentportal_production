import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ngx-my-activity',
  templateUrl: './my-activity.component.html',
  styleUrls: ['./my-activity.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MyActivityComponent implements OnInit {



  activities = [
    { icon: "fas fa-thumbs-up", time:Date() , statement: "You liked the post <strong class= 'highlight'> 'Buisness Development' </strong>", post: "./assets/images/open-book-leaf.jpg" },
    { icon: "fas fa-share-alt", time:Date(), statement: 'You recommended the post <strong class= "highlight"> "Buisness Development" </strong> to <strong class= "highlight"> Sonakshi Sharma </strong>', post: "./assets/images/open-book-leaf.jpg" },
    { icon: "fas fa-thumbs-up", time:Date(), statement: 'You liked the post <strong class= "highlight"> "Buisness Development" </strong>', post: "./assets/images/open-book-leaf.jpg" },

  ]

  constructor() { }

  ngOnInit() {
  }

}
