import { Component, OnInit, ViewEncapsulation , ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { TrainerAutomationServiceProvider } from '../../service/trainer-automation.service';
import { ENUM } from '../../service/enum';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'assessment-data',
  templateUrl: './assessment-data.component.html',
  styleUrls: ['./assessment-data.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AssessmentDataComponent implements OnInit {
  Assesment_Data: any = [
    { participants: "Unmesh", question1: "Option X",question2: "Option A",question3: "Option X",question4: "Option X",question5: "Option X", question6: 'Option X', question7: "Option X", question8: "Option X", question9: "Option X", question10: "Option X"},
    { participants: "Ashish", question1: "Option y",question2: "Option B",question3: "Option Y",question4: "Option X",question5: "Option X", question6: 'Option X', question7: "Option X", question8: "Option X", question9: "Option X", question10: "Option X" },
    { participants: "Aditya", question1: "Option Z",question2: "Option C",question3: "Option Z",question4: "Option X",question5: "Option X", question6: 'Option X', question7: "Option X", question8: "Option X", question9: "Option X", question10: "Option X" },
    { participants: "Niket", question1: "Option A",question2: "Option X",question3: "Option A",question4: "Option X",question5: "Option X", question6: 'Option X', question7: "Option X", question8: "Option X", question9: "Option X", question10: "Option X" },
    { participants: "Dinesh", question1: "Option B",question2: "Option Y",question3: "Option B",question4: "Option X",question5: "Option X", question6: 'Option X', question7: "Option X", question8: "Option X", question9: "Option X", question10: "Option X" },
    { participants: "Vivek", question1: "Option C",question2: "Option Z",question3: "Option C",question4: "Option X",question5: "Option X", question6: 'Option X', question7: "Option X", question8: "Option X", question9: "Option X", question10: "Option X" },
    ];
    displayedColumns2: string[] = ['participants', 'question1', 'question2', 'question3', 'question4', 'question5', 'question6', 'question7', 'question8', 'question9', 'question10'];
    assessments = new MatTableDataSource(this.Assesment_Data);

    @ViewChild(MatPaginator) paginator: MatPaginator;

    Object = Object;
    assessmentData:any={};
    userDetails:any;
    tenantId:any;
constructor(private TAServiceProvider : TrainerAutomationServiceProvider,private router: Router, private routes: ActivatedRoute) { 
  if(this.TAServiceProvider.assessmentData){
    this.assessmentData = this.TAServiceProvider.assessmentData;
    console.log('this.assessmentData',this.TAServiceProvider.assessmentData);
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
  }

    this.tenantId = this.TAServiceProvider.tenantId;  
    this.getAssessmentDataByQuiz();
  }

  ngOnInit() {
    this.assessments.paginator = this.paginator;

  }

  noAssessmentData:boolean = false;
  objKeys:any=[];
  participants:any;
  getAssessmentDataByQuiz(){
    let url = ENUM.domain + ENUM.url.getAssessmentData;
    let param = { 
      "reqFrom": "WEB",
      "quizIds": this.assessmentData.quizId,
      "tId": this.tenantId,
      "courseId":this.assessmentData.courseId,
      "moduleId":this.assessmentData.moduleId
    }
    this.TAServiceProvider.getAssessmentData(url, param)
    .then((result:any)=>{
      console.log('RESULT Assessment data===>',result);
      if(result.data.length == 0){
        this.noAssessmentData = true;
      }else{
        this.objKeys = Object.keys(result.data[0]);
        console.log('objKeys',this.objKeys);
        this.noAssessmentData = false;
        this.participants = result.data;
      }
		}).catch(result=>{
			console.log('RESULT Error participant===>',result);
		})
  }

  goBack(){
    this.router.navigate(['../'], { relativeTo: this.routes });
  }
  makeAssementData(question) {
    if(question){
      var fb = question.replace(/<[^>]+>/g, '');
      fb=fb.replace(/&nbsp;/g, '');
      if(fb.length > 15){
        return (fb.substring(0,15) + '...');
      }
      else{
        return fb;
      }
    }
  }
}

