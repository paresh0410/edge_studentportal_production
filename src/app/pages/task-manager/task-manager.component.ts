import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.scss']
})
export class TaskManagerComponent implements OnInit {
  todaysDate = new Date();

  days = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

  weekArray = [];
  
  toDoList: any = [
    {
      id:1,
      title: "Main",
      thingsToDo: [
        { 
          listId: 1,
          workId:1,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
          
        },
        { 
          listId: 1,
          workId:2,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 1,
          workId:3,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 1,
          workId:4,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
      ],    
    },
    {
      id:2,
      title: "Starred",
      thingsToDo: [
        { 
          listId: 2,
          workId:1,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 2,
          workId:2,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 2,
          workId:3,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 2,
          workId:4,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
      ],    
    },
    {
      id:3,
      title: "If I have time",
      thingsToDo: [
        { 
          listId: 3,
          workId:1,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 3,
          workId:2,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 3,
          workId:3,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
        { 
          listId: 3,
          workId:4,
          work: "Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
          completedStatus: false
        },
      ],     
    }
  ];

  constructor() {

    var tempDay = this.todaysDate.getDay();
    var tempDate = this.todaysDate.getDate();

    /************Week Start Date************/
    var weekStartDate = tempDate - tempDay;
    var weekStartMonth = this.todaysDate.getMonth();
    var Month = this.months[weekStartMonth];
    var weekStartYear = this.todaysDate.getFullYear();
    var wSFormat = weekStartDate + '-' + Month + '-' + weekStartYear;
    var WSD = new Date(wSFormat);

    /************Week End Date************/
    var diff = 6 - tempDay;
    var weekEndDate = tempDate + diff;
    var wEFormat = weekEndDate + '-' + Month + '-' + weekStartYear;
    var WED = new Date(wEFormat);

    var day = this.days[tempDay];
    console.log('DAY===>',WED);

  }

  ngOnInit() {
  }

  cutFromTheList(typeIndex, listItem, listindex){

    if(this.toDoList[typeIndex].thingsToDo[listindex].completedStatus == false){
      this.toDoList[typeIndex].thingsToDo[listindex].completedStatus = true;
      console.log("Completed status--> ",this.toDoList[typeIndex].thingsToDo[listindex].completedStatus);
    }
    else{
      this.toDoList[typeIndex].thingsToDo[listindex].completedStatus = false;
      console.log("Completed status--> ",this.toDoList[typeIndex].thingsToDo[listindex].completedStatus);    
    }
  }

}
