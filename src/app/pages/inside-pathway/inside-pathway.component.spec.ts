import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsidePathwayComponent } from './inside-pathway.component';

describe('InsidePathwayComponent', () => {
  let component: InsidePathwayComponent;
  let fixture: ComponentFixture<InsidePathwayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsidePathwayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsidePathwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
