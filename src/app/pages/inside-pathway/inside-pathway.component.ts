import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-inside-pathway',
  templateUrl: './inside-pathway.component.html',
  styleUrls: ['./inside-pathway.component.scss']
})
export class InsidePathwayComponent implements OnInit {

  isMandatory:boolean;
  isSelected:boolean;
  cardImg:string;
  content:string;
  stages:string;

  addpathway=[
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    
  ];

  togglesave(item){
    if(!item.save){
      item.save = true;
    }else{
      item.save = false;
    }
  }

  constructor(private router:Router, private routes:ActivatedRoute) { }

  ngOnInit() {
  }

  goBack(){
    this.router.navigate(['../../profile'],{relativeTo:this.routes})
  }

}
