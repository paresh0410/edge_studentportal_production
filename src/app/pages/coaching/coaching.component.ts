import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
declare var window: any;
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
declare var window: any;
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UserChatService } from '../../service/user-chat-service';
//import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../../service/enum';
// import { CallDetailService } from '../../call-detail.service';
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import 'rxjs/operator/toPromise';
import 'rxjs/operator/takeUntil';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ngx-coaching',
  templateUrl: './coaching.component.html',
  styleUrls: ['./coaching.component.scss']
})
export class CoachingComponent implements OnInit {
  myInnerHeight: any = window.innerHeight - 160;
  chatboxHeight: any = this.myInnerHeight - 150;
  _videoworking: boolean = false;
  modalInfo:any = {
    title: 'Send Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Yes',
    event_second: 'No'
  };
  visibleEndModal=false;
  modalEndMessage:any = {
    title: 'Meeting is ended',
    subtitle: '',
    message: 'Your meeting in ended',
    event_first: 'Ok',
    event_second: ''
  };
  visibleEndedModal=false;
  modalEndedMessage:any = {
    title: 'End Meeting',
    subtitle: '',
    message: 'Do you want to end meeting?',
    event_first: 'Yes',
    event_second: 'Cancel'
  };
  visibleRejectModal=false;
  rejectJoinMeeting:string;
  modalRejectMessage:any = {
    title: 'Invitation Rejected',
    subtitle: '',
    message: 'User reject a call.',
    event_first: 'Ok',
    event_second: ''
  };
  visibleSendModal=false;
  senderJoinMeeting:string;
  modalSendInvitation:any = {
    title: 'Send Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Ok',
    event_second: 'Cancel'
  };
  visibleReceiveModal=false;
  modalReceiveInvitation:any = {
    title: 'Receive Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Accept',
    event_second: 'Reject'
  };
  visibleConnectingModal=false;
  modalConnecting:any = {
    title: '',
    subtitle: '',
    message: 'Connecting...',
    event_first: '',
    event_second: 'Cancel'
  };
  visibleModal:boolean = false;
  userDetail: any = {
    id: 2,
    profilePic: "./assets/images/eva.png",
    name: "Steve",
    message: 'Test which is a new approach to have all solutions',
    datetime: Date(),
    messageType: "text",
  };
  frienduserDetail: any = {
    id: 4,
    profilePic: "./assets/images/jack.png",
    name: "John",
    message: 'Test which is a new approach to have all solutions',
    datetime: Date(),
    messageType: 'text',
  };
  profilePic: string;
  name: string;
  messagePeview: string;
  lastMsgTime: string;
  pendingMsgs: string;
  activateChat: boolean;
  message: string = '';
  messages: any = [];
  replyMessage = "";

  prevIndex: number = 0;
  userdetail: any = {};
  searchCtrl = new FormControl();
  filteredUsers: Observable<any[]>;

  users: Observable<any[]>
  isLoading = false;
  myForm: FormGroup;
  chatUser: any = {};
  clientHeight: any = 0;
  spinner: any = false;
  chatInit: any = false;
  UserId: number;
  FriendUserId: number;
  public data: any = {
    username: '',
    chatlist: [],
    friendSocketId: null,
    selectedFriendId: null,
    selectedFriendName: null,
    messages: [],
    invite: false,
    inviteuserid: 0
  };
  webinarinfo: any = {
    SenderId : 0,
    ReceiverId : 0,
    SenderSocketId : '',
    RecevierSocketId : '',
    SenderUsername : '',
    ReceiverUsername : ''
  };
  userId:number;
  callId:number;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  empId:any;
  coachId:any;
  bbbInfo:any;
  bbburl:any;
  constructor(public chatService: UserChatService, public cdf: ChangeDetectorRef, private fb: FormBuilder, private sanitizer: DomSanitizer) {
      // this.empId = this.calldetailservice.empId;
      // this.coachId = this.calldetailservice.coachId;
      // this.callId = this.calldetailservice.callId;
      // this.UserId = this.calldetailservice.empId; //this.userDetail.id;
      // this.coachId = this.calldetailservice.coachId; // this.frienduserDetail.id;
      this.callId = 10;
      this.bbburl = null;
      if(localStorage.getItem('userDetails')){
        const userDetail = JSON.parse(localStorage.getItem('userDetails'));
        this.UserId = userDetail.id;
      }
     }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;// + this.myScrollContainer.nativeElement.scrollTop;
      }, 10)

    } catch (err) {
      console.log(err);
    }
  }

  ngOnInit() {
    // this.UserId = this.empId; // this.userDetail.id;
    
    if(this.UserId == 2){
      this.UserId = 2;
      this.coachId = 4;
        this.webinarinfo= {
          SenderId : this.UserId,
          ReceiverId : this.coachId,
          SenderSocketId : '',
          RecevierSocketId : '',
          SenderUsername : 'Bhavesh-Tandel',
          ReceiverUsername : 'Aditya-Sawant'
        };
    }else if(this.UserId == 4){
      this.UserId = 4;
      this.coachId = 2;
      this.webinarinfo= {
        SenderId : this.UserId,
        ReceiverId : this.coachId,
        SenderSocketId : '',
        RecevierSocketId : '',
        SenderUsername : 'Aditya-Sawant',
        ReceiverUsername : 'Bhavesh-Tandel'
      };
    }
    this.FriendUserId = this.coachId; // this.frienduserDetail.id;
    this.checkUserFriendMap(this.UserId, this.FriendUserId);
    this.connectSocket(this.UserId);
    this.selectFriendToChat(this.FriendUserId)
    // this.getChatList(this.UserId);
    this.incommingMessage();
    this.usercomming();
    this.ReceiveWebinar();
    this.Connecting();
    this.ApprovalReceive();
    this.EndedMeetingReceive();
    // this.invitationcomming();
    // this.userapprovalcomming();
  }
  connectSocket(UserId) {
    this.chatService.connectSocketServer(UserId);
  }

  incommingMessage() {

    /*
    * This eventt will display the new incmoing message
    */
    this.chatService.socketOn('add-message-response', (response) => {
      //this.$apply(() => {
      // if (response && response.fromUserId == this.frienduserDetail.id) {
      //   response.friendProfilePic = "./assets/images/jack.png";
      //   this.data.messages.push(response);
      //   this.scrollToBottom();
      // }
      response.friendProfilePic = "./assets/images/jack.png";
        this.data.messages.push(response);
        //this.chatService.scrollToBottom();
        this.scrollToBottom();
      try {
        this.cdf.detectChanges();
      } catch (e) {
        console.log(e);
      }
    });

  }

  selectFriendToChat(friendId) {
    /*
    * Highlighting the selected user from the chat list
    */
    this.chatInit = true;
    this.spinner = true;
    this.messages = [];
    this.data.selectedFriendName = this.frienduserDetail['name'];
    this.data.selectedFriendId = this.frienduserDetail.id;
    // this.chatUser = friendData[0];
    // this.data.chatlist[this.prevIndex].pendingMsgs = 0;
    /**
    * This HTTP call will fetch chat between two users
    */
    if (this.chatUser.online === undefined) {
      this.data.invite = true;
      this.data.inviteuserid = friendId;
      this.data.messages = [];
      // this.cdf.detectChanges();
    } else {
      // if((this.chatUser.accepted === 0 && this.chatUser.rejected === 0 && this.chatUser.invited === 0) || (this.chatUser.accepted === 0 && this.chatUser.rejected === 0 && this.chatUser.invited === 1)){
      // this.data.invite = true;
      // this.data.inviteuserid = 0;
      // }else{
      // this.data.invite = false;
      // this.data.inviteuserid = 0;
      // }
      //userdetail = this.data;
    }
    this.chatService.getMessages(this.UserId, this.FriendUserId).then((response: any) => {
      //this.$apply(() => {
      if (response.messages.length > 0) {
        this.data.messages = response.messages[0];
      }
      this.scrollToBottom();
      this.spinner = false;
      this.cdf.detectChanges();
      //});
    }).catch((error) => {
      console.log(error);
      this.spinner = false;
      alert('Unexpected Error, Contact your Site Admin.');
    });
  }
  usercomming() {
    this.chatService.socketOn('user-comming-chat-list-response', (response) => {
      // if(this.data.chatlist){
      // this.data.chatlist.forEach((user,key)=>{
      // if(response.userinfo.length > 0){
      // if(user.userid == response.userinfo[0].userid){
      // this.data.chatlist[key].socketid = response.userinfo[0].socketid;
      // this.toastr.success( response.userinfo[0].username + ' ' + 'is online');
      // }
      // }
      // })
      // }
      if (response.userinfo.length > 0) {
        if (response.userinfo[0].userid === this.FriendUserId) {
          this.data.friendSocketId = response.userinfo[0].socketid;
          this.webinarinfo.RecevierSocketId = response.userinfo[0].socketid;
        }
      }
    })
  }


  sendChatMessage(event, value) {

    //if (event.keyCode === 13) {
    let fromUserId = null;
    let toSocketId = null;
    /* Fetching the selected User from the chat list starts */
    let selectedFriendId = this.webinarinfo.ReceiverId;//this.data.selectedFriendId;
    if (selectedFriendId === null) {
    return null;
    }
    
    /* Fetching the selected User from the chat list ends */
    /* Emmiting socket event to server with Message, starts */
    // if (this.friendData.length > 0) {
    fromUserId = this.webinarinfo.SenderId;
    toSocketId = this.data['friendSocketId'];
    let messagePacket = {
    message: value,//document.querySelector('#message').value,
    fromUserId: fromUserId,
    toUserId: selectedFriendId,
    toSocketId: toSocketId,
    userProfilePic: "./assets/images/eva.png",
    friendId: selectedFriendId
    };
    this.data.messages.push(messagePacket);
    // this.chatService.socketEmit(`add-message`, messagePacket);
    this.chatService.socketEmit(`add-message`, messagePacket);
    this.message = '';
    this.scrollToBottom();
    //document.querySelector('#message').value = '';
    //this.chatService.scrollToBottom();
    // } else {
    // alert('Unexpected Error Occured,Please contact Admin');
    // }
    /* Emmiting socket event to server with Message, ends */
    //}
    }

  checkUserFriendMap(userId, friendId) {
    let param = {
      userId: userId,
      friendId: friendId
    }
    this.chatService.insertDirectUserFriendMap(param).then((res) => {
      console.log(res);
    }).catch((err) => {
      console.log(err);
    })
  }


  /**
   * Start 
   * @Author Bhavesh Tandel
   * Video Call Method
   */

   /**
    * Open Invitation Modal POPUP
    * @method OpenInviationPopup
    * @param coachId
    * @param trainerId
    */
  OpenInviationPopup() {
    console.log('Open Modal Up');
    this.visibleSendModal = true;
      this.visibleReceiveModal = false;
      this.visibleConnectingModal = false;
  }
    /**
   * Send invitaion to receiver
   * @method SendInviation
   * @param coachId
   * @param trainerId
   * @event Accepted
   * @event Rejected
   */
  SendInviation(event) {
    if(event){
      const user = {
        callId : this.callId,
        senderId : this.UserId,
        receiverId : this.coachId,
        senderName : this.webinarinfo.SenderUsername,
        receiverName : this.webinarinfo.ReceiverUsername
      }
      this.chatService.socketEmit(`send-webinar-call`, user);
    }
  }
  /**
   * @method Connecting
   * @param flag 
   */
  Connecting() {
    console.log('Connecting');
    this.chatService.socketOn('connecting-webinar-call', (response)=>{
      console.log(response);
      this.visibleSendModal = false;
      this.visibleReceiveModal = false;
      this.visibleConnectingModal = true;
      this.senderJoinMeeting = response.bbbInfo.start;
      this.bbbInfo = response.bbbInfo;
      setTimeout( ()=>{
        // this.openWebinarWindow(response.bbbInfo.start);
      },3000);
    });
  }
  /**
   * Receive Invitation from Sender
   * @method ReceiveInvitation
   * 
   */
  ReceiveWebinar(){
    this.chatService.socketOn('receive-webinar-call', (response) => {
      console.log(response);
      if(response.bbbInfo){
        this.bbbInfo = response.bbbInfo;
      }
      this.openReceiveModalPopUp();
    })
  }
  openReceiveModalPopUp(){
    console.log('Receive Invitation');
    this.visibleSendModal = false;
      this.visibleReceiveModal = true;
      this.visibleConnectingModal = false;
  }
  ReceiveInvitation(event) {
    
  }
  AcceptInvitation(event){
    if(event === 'true'){
      this.bbbInfo.receiverId = this.coachId;
      this.bbbInfo.senderId = this.webinarinfo.SenderId;
      // this.bbbInfo.senderName = this.webinarinfo.SenderUsername,
      // this.bbbInfo.receiverName = this.webinarinfo.ReceiverUsername
      this.bbbInfo.name = this.webinarinfo.SenderUsername;
      this.bbbInfo.status = true;
      this.chatService.socketEmit(`receiver-accept-request-webinar-call`, this.bbbInfo);
    }else if(event === 'false'){
      this.bbbInfo.receiverId = this.coachId;
      this.bbbInfo.name = this.webinarinfo.ReceiverUsername;
      this.bbbInfo.status = false;
      this.chatService.socketEmit(`receiver-accept-request-webinar-call`, this.bbbInfo);
      //this.chatService.socketEmit(`receiver-accept-request-webinar-call`, this.bbbInfo);
    }
    this.CloseModal(true);
  }

  /**
   * @method StartMeeting
   */
  StartMeeting() {

  }

  /**
   * @method JoinMeeting
   */
  JoinMeeting(event) {

    var obj = {
      meetingID: this.bbbInfo.meetingID,
      fullName: this.webinarinfo.ReceiverUsername,
      password: this.bbbInfo.moderatorPW,
      senderId : this.UserId,
      receiverId : this.coachId,
      senderName : this.webinarinfo.SenderUsername,
      receiverName : this.webinarinfo.ReceiverUsername
    }
    this.chatService.socketEmit('accept-request-webinar-call', obj);
  }

  CloseModal($event) {
    console.log('Close Event');
    if(this.visibleConnectingModal){
      this.visibleConnectingModal = false;
    }
    if(this.visibleSendModal){
      this.visibleSendModal = false;
    }
    if(this.visibleReceiveModal){
      this.visibleReceiveModal = false;
    }
    if(this.visibleRejectModal){
      this.visibleRejectModal = false;
    }
    if(this.visibleEndedModal){
      this.visibleEndedModal = false;
    }
    this.visibleEndModal = false;
  }
  openRejectModalPopup(){
    this.CloseModal(false);
    this.visibleRejectModal = true;
  }
  ApprovalReceive(){
    this.chatService.socketOn('sender-receive-approval', (data)=>{
      console.log('Socket - sender-receive-approval', data);
      this.CloseModal(null);
      var senderUrl = this.senderJoinMeeting;
      if(data.status == true){
        this.openWebinarWindow(senderUrl);
      }else{
       console.log('User Reject your call!'); 
       this.openRejectModalPopup();
      }
      
    })
    this.chatService.socketOn('join-receiver', (data)=>{
      if(data.bbbInfo){
        this.bbbInfo.start = data.bbbInfo.start;
        this.openWebinarWindow(data.bbbInfo.start);
      }
    })
  }
  EndedMeeting(){
    if(this.bbbInfo){
      var obj = {
        meetingID: this.bbbInfo.meetingID
      }
      this.chatService.socketEmit('end-webinar-call', obj);
    }
  }
  EndedMeetingReceive(){
    this.chatService.socketOn('end-webinar-msg', (data)=>{
      this.CloseModal(true);
      this.bbbInfo = null;
      this._videoworking = false;
      this.visibleEndModal = true;
    })
  }
  
  /**
   * End 
   * @Author Bhavesh Tandel
   * Video Call Method
   */

openWebinarWindow(url) {
  this.bbburl = this.transform(url);
  this._videoworking = true;
  console.log(this.bbburl);
  //this.cdf.detectChanges();
    // const newwindow = window.open(url,'name','height=300,width=300, location=0');
    // if (window.focus) {newwindow.focus()}
    // return false;
}
endVideo(){
  this.visibleEndedModal = true;
}
encodeURI(url) {
  if (UriBuilder.parse(url).isRelative()) {
      url = cleanPath(document.baseURI + '/' + url);
  }
  return encodeURIComponent(url);
}
transform(url): any {
  // url = "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg";
  // url = "https://angular.io/api/platform-browser/DomSanitizer#bypassSecurityTrustUrl";
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnDestroy() {
    this.EndedMeeting();
  }
}
