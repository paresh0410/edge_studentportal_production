import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbTabsetModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';


//import { CoachingComponent } from './coaching.component';
// import { CoachingComponent } from './coaching.component';
// import { ModalViewerComponent } from '../../component/modal-viewer/modal-viewer.component';

@NgModule({
    imports: [
        CommonModule,
        NbTabsetModule,
        FormsModule
    ],

    declarations: [
        //CoachingComponent,
        // CoachingComponent,
        // ModalViewerComponent
    ],

    providers: [ 
        
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
     ]
      
})
      
export class CoachingModule{
      
      }