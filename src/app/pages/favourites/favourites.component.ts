import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { group } from '@angular/animations';

@Component({
  selector: 'ngx-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {

  firstbtn:boolean;
  secondbtn:boolean;
  thirdbtn:boolean;

  btngrp={firstbtn:true, secondbtn:false, thirdbtn:false};

 
  showItemsInFolder:boolean;

  isMandatory:boolean;
  isSelected:boolean;
  cardImg:string;
  content:string;
  stages:string;

  bookmarkitems=[
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    
  ];

  name:String;
  noOfPost:number;

  recentBookmarks=[
    { name: "Design", timeB: "42 mins"},
    { name: "Sales", timeB: "42 mins"},
    { name: "Positivity", timeB: "42 mins"},
    { name: "JohnDoe", timeB: "42 mins"},
    { name: "Design", timeB: "42 mins"},
    { name: "Sales", timeB: "42 mins"},
    { name: "Positivity", timeB: "42 mins"},
    { name: "JohnDoe", timeB: "42 mins"},
    { name: "Design",  timeB: "42 mins"},
    { name: "Sales", timeB: "42 mins"},
    { name: "Positivity", timeB: "42 mins"},
    { name: "JohnDoe", timeB: "42 mins"},
    { name: "Design", timeB: "42 mins"},
  ];
  
  folderICo:string;
  folderName:string;
  noOfItems:string;

  bookmarkFolder=[
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},

  ];


  recentTags=[
    { name: "Design", timeT: "42 mins"},
    { name: "Sales", timeT: "42 mins"},
    { name: "Positivity", timeT: "42 mins"},
    { name: "JohnDoe", timeT: "42 mins"},
    { name: "Design", timeT: "42 mins"},
    { name: "Sales", timeT: "42 mins"},
    { name: "Positivity", timeT: "42 mins"},
    { name: "JohnDoe", timeT: "42 mins"},
    { name: "Design", timeT: "42 mins"},
    { name: "Sales", timeT: "42 mins"},
    { name: "Positivity", timeT: "42 mins"},
    { name: "JohnDoe", timeT: "42 mins"},
    { name: "Design", timeT: "42 mins"},
  ];

  MyTags=[
    { title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/cover2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, save: false },
		{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera1.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, save: false },
		{ title: "How Design Helps in Business", timePassed: "52m", attachment: "./assets/images/camera2.jpg", tags: "Design   |   Business   |   Entrepreneurship   |   Corporate", like: false, dislike: false, share: false, save: false },
  ];


  recentResources=[
    { name: "Design", timeR: "42 mins"},
    { name: "Sales",  timeR: "42 mins"},
    { name: "Positivity",  timeR: "42 mins"},
    { name: "JohnDoe", timeR: "42 mins"},
    { name: "Design", timeR: "42 mins"},
    { name: "Sales", timeR: "42 mins"},
    { name: "Positivity", timeR: "42 mins"},
    { name: "JohnDoe", timeR: "42 mins"},
    { name: "Design", timeR: "42 mins"},
    { name: "Sales", timeR: "42 mins"},
    { name: "Positivity", timeR: "42 mins"},
    { name: "JohnDoe", timeR: "42 mins"},
    { name: "Design", timeR: "42 mins"},
  ];

  selectedCount:number;

  radiofn(post){
    this.selectedCount=0;
  
      if(!post.isSelected){       
      post.isSelected = true;       //if radio button is false make it true
      
      return  this.selectedCount=this.count();        // store count in variable if true
      }else{
      post.isSelected = false;
      return  this.selectedCount=this.count();        // store count in variable if false
      }   
    }

    count(){
      var count=0;
      this.bookmarkitems.forEach(obj => {      //for each object in array of addpathway
        console.log(obj);
        if(obj.isSelected==true)            // if array element is selected
        {
          count++;                          //increment count
        }
      })
      return  count;
    }

  createBOOKMARK(){
    // this.createbookmark=true;
    this.router.navigate(['create-bookmark'],{relativeTo:this.routes})
  }

  viewbookmark(){
    this.router.navigate(['bookmark'],{relativeTo:this.routes})    
  }


  togglesave(item){
    if(!item.save){
      item.save = true;
    }else{
      item.save = false;
    }
  }

  
	togglelike(item) {
		if (!item.like) {
			item.like = true;
			if (item.like) {
				item.dislike = false;
			}
		} else {
			item.like = false;
		}
	}

	toggledislike(item) {
		if (!item.dislike) {
			item.dislike = true;
			if (item.dislike) {
				item.like = false;
			}
		} else {
			item.dislike = false;
		}
	}


 chips = [
    {
      group: [
        {
          tagname: 'Tag 1',
          checked: true,
        },
        {
          tagname: 'Tag 2',
          checked: false,
        },
        {
          tagname: 'Tag 3',
          checked: false,
        }
      ]
    },{
      group: [
        {
          tagname: 'Tag 4',
          checked: false,
        },
        {
          tagname: 'Tag 5',
          checked: false,
        },
        {
          tagname: 'Tag 6',
          checked: false,
        }
      ]
    },{
      group: [
        {
          tagname: 'Tag 7',
          checked: false,
        },
        {
          tagname: 'Tag 8',
          checked: false,
        },
        {
          tagname: 'Tag 9',
          checked: false,
        }
      ]
    }
  ];

  constructor(private router:Router, private routes:ActivatedRoute) { }

 

  selectTag(chipIndex, groupIndex){
    if(this.chips[chipIndex].group[groupIndex].checked==false)
    {
    this.chips[chipIndex].group[groupIndex].checked= true; 
    } 
    else{
     this.chips[chipIndex].group[groupIndex].checked= false;
    }
  }



showFolderItems(){
  this.showItemsInFolder=true;
}


  activatefirst(btns){
    if(!btns.firstbtn){
      btns.firstbtn=true;
      btns.secondbtn=false;
      btns.thirdbtn=false;
    }
  }

  activatesecond(btns){
    if(!btns.secondbtn){
      btns.firstbtn=false;
      btns.secondbtn=true;
      btns.thirdbtn=false;
    }
  }

  activatethird(btns){
    if(!btns.thirdbtn){
      btns.firstbtn=false;
      btns.secondbtn=false;
      btns.thirdbtn=true;
    }
  }
  ngOnInit() {
    this.showItemsInFolder=false;
    // this.prevIndex = 0;
   }

}