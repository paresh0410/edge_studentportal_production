import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MsalService } from '@azure/msal-angular';
import { AuthServiceProvider } from '../../service/auth-service';

@Component({
	selector: 'ngx-logout',
	template: 'logout',
	// styleUrls: ['./login.component.scss']
})
export class LogoutComponent {

	constructor(private router: Router, private routes: ActivatedRoute,
		private authService: MsalService, private ASP: AuthServiceProvider) {
		const logoutData = this.make_logout_data_ready();
		this.ASP.userLogout(logoutData);
		if (this.authService.getUser()) {
				// this.authService.logout();
				localStorage.clear();
				localStorage.setItem('msal.logoutflag', 'true');
				this.router.navigate(['../login-new'], { relativeTo: this.routes });
		} else {
			if (localStorage.getItem('remember_me_details') != null) {
				let remember_me_details = JSON.parse(localStorage.getItem('remember_me_details'));
				if(remember_me_details.remember_me === true){
					localStorage.removeItem('userDetails');
					localStorage.removeItem('employeeId');
					localStorage.removeItem('userOtherDetails');
					localStorage.removeItem('roleName');
					localStorage.removeItem('token');
					localStorage.removeItem('userdetail');
				}
			}
			else{
				localStorage.clear();
			}
			localStorage.setItem('msal.logoutflag', 'true');

			this.router.navigate(['../login-new'], { relativeTo: this.routes });
		}
	}

	  make_logout_data_ready() {
		const userDetails = JSON.parse(localStorage.getItem('userDetails'));
		console.log("userDetails ==>", userDetails);
		const logoutData = {
		  type: 3,
		  userId: userDetails.id,
		  actDate: null,
		  tenantId: userDetails.tenantId
		}
		return logoutData;
	  }
}

