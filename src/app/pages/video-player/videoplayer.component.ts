import {
  Component,
  HostBinding,
  Input,
  OnDestroy,
  ChangeDetectorRef,
  Renderer2,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  AfterViewInit,
} from "@angular/core";
//import { PlayerService, Track } from '../../@core/utils/player.service';
import { VideoPlayerService, VideoTrack } from "./videoplayer.service";
import { CourseServiceProvider } from "../../service/course-service";
import { MultiLanguageService } from '../../service/multi-language.service';
import { Subscription } from 'rxjs/Subscription';
//import {MatVideoComponent} from 'mat-video/mat-video';
@Component({
  selector: "ngx-videoplayer",
  templateUrl: "./videoplayer.component.html",
  styleUrls: ["./videoplayer.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class VideoPlayerComponent  implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  @HostBinding("class.collapsed")
  collapsed: boolean;
  @ViewChild("vid") video: any; //HTMLVideoElement;

  version = "1.1.1"; //VERSION.full;
  appversion: string = "2.3.2"; //(<any>buildInfo)['version'];

  ngclass = ""; //'mat-video-responsive';

  videoTrack: VideoTrack;

  currentProgress: any = 0;

  currentActivityData: any;
  activityCompRes: any;

  saveActCompWorking: any = false;
  languageChangeSubscriber = Subscription.EMPTY;
  videoReady = false;
  constructor(
    private playerService: VideoPlayerService,
    public cdf: ChangeDetectorRef,
    private renderer: Renderer2,
    public couserService: CourseServiceProvider,
    public multiLanguageService: MultiLanguageService,
  ) {
    this.currentActivityData = this.playerService.getActivity();
    // this.languageChangeSubscriber = multiLanguageService.selectedactivityLanguage
    // .subscribe(selectedactivityLanguage => {
    //   //do what ever needs doing when data changes
    //   this.videoReady = false;
    //   this.cdf.markForCheck();
    //   console.log('selectedLanguage', selectedactivityLanguage);
    //   if(selectedactivityLanguage){
    //     this.currentActivityData = selectedactivityLanguage;
    //     this.reIntializeData();

    //   }
    //   setTimeout(() =>{
    //     this.videoReady = true;
    //     this.cdf.markForCheck();
    //   },)
    // });
  }

  ngOnInit() {
    this.videoTrack = this.playerService.getVideoTrack();
    this.setProgress(this.videoTrack.currentTime);
    //this.setPlayerDuration();

    //this.video.addEventListener('ended', () => console.log('video ended'));
    // this.reIntializeData();
  }
  ngAfterViewInit() {
    this.reIntializeData();
  }
  setProgress(time) {
    this.video.video.nativeElement.currentTime = time;
  }
  getProgress() {
    this.currentProgress = this.video.video.nativeElement.currentTime;
  }
  setPlayerDuration() {
    this.playerService.setVideoDuration(
      this.video.video.nativeElement.duration,
    );
  }
  ngOnDestroy() {
    this.languageChangeSubscriber.unsubscribe();
    if (this.playerService.fromTools === false) {
      this.saveAudioActivity(null);
      this.getProgress();
    }
  }

  saveAudioActivity(status) {
    // this.playerService.setAudioDuration(this.player.duration);
    this.currentActivityData = this.playerService.getActivity();
    const activity = this.currentActivityData;
    activity.contenttime = this.video.video.nativeElement.duration;
    activity.contentwatchedtime = this.video.video.nativeElement.currentTime;
    // let activity = this.playerService.getActivity();
    if (activity.completionstatus === "Y") {
      activity.completionstatus = "Y";
    } else if (status === "Y") {
      activity.completionstatus = "Y";
    } else {
      // parseInt
      if (
        parseInt(activity.contenttime, 10) >
        parseInt(activity.contentwatchedtime, 10)
      ) {
        activity.completionstatus = "UP";
      } else {
        activity.completionstatus = "Y";
      }
    }

    // this.couserService.saveActivityCompletion(activity);
    // this.playerService.setActivity(activity);
    if (this.saveActCompWorking === false) {
      this.saveVideoActivityCompletionData(activity);
    }
  }

  saveVideoActivityCompletionData(activityData) {
    if (!this.saveActCompWorking) {
    let userdetail;
    if (localStorage.getItem("userdetail")) {
      userdetail = JSON.parse(localStorage.getItem("userdetail"));
      if (Number(userdetail["roleId"]) === 8 ||  Number(userdetail['roleId']) === 10) {
        this.saveActCompWorking = true;
        this.couserService.saveActivityCompletion(activityData).then(
          (res) => {
            this.saveActCompWorking = false;
            if (res["type"] === true) {
              this.activityCompRes = res;
              if (
                this.currentActivityData.activitycompletionid === undefined ||
                !this.currentActivityData.activitycompletionid
              ) {
                this.currentActivityData.activitycompletionid =
                  res["data"][0][0].lastid;
                this.playerService.setActivity(this.currentActivityData);
              }
              console.log("Activity completion res", res);
            }
          },
          (err) => {
            this.saveActCompWorking = false;
            console.log(err);
          }
        );
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  }

  setVideoTrack(request){
    let track = {
      src : 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/mov_bbb_%281%29.mp4',
      title : 'NASA Rocket Launch',
      width : 600,
      height : 337.5,
      autoplay : false,
      preload : true,
      loop : false,
      quality : true,
      download : false,
      fullscreen : true,
      keyboard : true,
      color : 'primary',
      spinner : 'spin',
      poster : '../assets/images/videoplay.png',
      currentTime:0
    };
    track['src'] = request.reference;
    track['title'] = request.name;
    this.videoTrack = track;
  }

  reIntializeData() {
    this.setVideoTrack(this.currentActivityData);
    this.setProgress(this.videoTrack.currentTime);
    if (this.playerService.fromTools === false) {
      this.renderer.listen(this.video.video.nativeElement, "loadeddata", () => {
        console.log("video loadeddata");
        let activity = this.playerService.getActivity();
        if (activity.contentwatchedtime) {
          this.setProgress(activity.contentwatchedtime);
        }
      });
      this.renderer.listen(this.video.video.nativeElement, "ended", () => {
        console.log("video ended");
        this.saveAudioActivity("Y");
      });
      this.renderer.listen(this.video.video.nativeElement, "play", () => {
        console.log("video play");
        this.saveAudioActivity(null);
      });
      this.renderer.listen(this.video.video.nativeElement, "pause", () => {
        console.log("video pause");
      });
      this.renderer.listen(this.video.video.nativeElement, "playing", () => {
        console.log("video playing");
      });
    }
  }
}
