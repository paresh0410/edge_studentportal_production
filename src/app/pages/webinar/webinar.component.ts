import { WebinarServiceProvider } from '../../service/webinar.service';
import { LearnServiceProvider } from '../../service/learn-service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';
import {OpenviduSessionComponent,
  UserModel, OpenViduLayout, OpenViduLayoutOptions, OvSettings} from 'openvidu-angular';
import { OpenVidu, Publisher, Session, StreamEvent, StreamManager, Subscriber } from 'openvidu-browser';

import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ViewEncapsulation
} from '@angular/core';
declare var window: any;
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
declare var window: any;
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
//import { ToastrService } from 'ngx-toastr';
//import { ENUM } from '../../service/enum';
// import { CallDetailService } from '../../call-detail.service';
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import 'rxjs/operator/toPromise';
import 'rxjs/operator/takeUntil';
import { DomSanitizer } from '@angular/platform-browser';
import { WebinarConfig } from '../../pages/webinar-multiple/webinar-entity';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-webinar',
  templateUrl: './webinar.component.html',
  styleUrls: ['./webinar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebinarComponent implements OnInit {
  @Input() course: any;
  @Input() userData: any;
  @Input() videoCall: any;
  @Input() voiceCall: any;
  @Input() areaId: any;
  @Input() instanceId: any;
  @Input() config: WebinarConfig;
  @Output() callEnd = new EventEmitter();

  @ViewChild('ovSessionComponent')
  public ovSessionComponent: OpenviduSessionComponent;
  // trainerData: any;
  //  usersData: any;
  userdetail: any = {};
  myInnerHeight: any = window.innerHeight - 160;
  chatboxHeight: any = this.myInnerHeight - 150;
  _videoworking: boolean = false;
  modalInfo: any = {
    title: 'Send Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Yes',
    event_second: 'No'
  };
  visibleEndModal = false;
  modalEndMessage: any = {
    title: 'Meeting is ended',
    subtitle: '',
    message: 'Your meeting in ended',
    event_first: 'Ok',
    event_second: ''
  };
  visibleEndedModal = false;
  modalEndedMessage: any = {
    title: 'End Meeting',
    subtitle: '',
    message: 'Do you want to end meeting?',
    event_first: 'Yes',
    event_second: 'Cancel'
  };
  visibleRejectModal = false;
  rejectJoinMeeting: string;
  modalRejectMessage: any = {
    title: 'Invitation Rejected',
    subtitle: '',
    message: 'User reject a call.',
    event_first: 'Ok',
    event_second: ''
  };
  visibleSendModal = false;
  senderJoinMeeting: string;
  modalSendInvitation: any = {
    title: 'Send Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Ok',
    event_second: 'Cancel'
  };
  visibleReceiveModal = false;
  modalReceiveInvitation: any = {
    title: 'Receive Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Accept',
    event_second: 'Reject'
  };
  visibleConnectingModal = false;
  modalConnecting: any = {
    title: '',
    subtitle: '',
    message: 'Connecting...',
    event_first: '',
    event_second: 'Cancel'
  };
  visibleModal: boolean = false;
  userDetail: any = {
    id: 2,
    profilePic: './assets/images/eva.png',
    name: 'Steve',
    message: 'Test which is a new approach to have all solutions',
    datetime: Date(),
    messageType: 'text'
  };
  frienduserDetail: any = {
    id: 4,
    profilePic: './assets/images/jack.png',
    name: 'John',
    message: 'Test which is a new approach to have all solutions',
    datetime: Date(),
    messageType: 'text'
  };
  profilePic: string;
  name: string;
  messagePeview: string;
  lastMsgTime: string;
  pendingMsgs: string;
  activateChat: boolean;
  message: string = '';
  messages: any = [];
  replyMessage = '';

  prevIndex: number = 0;
  searchCtrl = new FormControl();
  filteredUsers: Observable<any[]>;

  users: Observable<any[]>;
  isLoading = false;
  myForm: FormGroup;
  chatUser: any = {};
  clientHeight: any = 0;
  spinner: any = false;
  chatInit: any = false;
  UserId: number;
  FriendUserId: number;
  public data: any = {
    username: '',
    chatlist: [],
    friendSocketId: null,
    selectedFriendId: null,
    selectedFriendName: null,
    messages: [],
    invite: false,
    inviteuserid: 0
  };
  webinarinfo: any = {
    SenderId: 0,
    ReceiverId: 0,
    SenderSocketId: '',
    RecevierSocketId: '',
    SenderUsername: '',
    ReceiverUsername: ''
  };
  userId: number;
  callId: number;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChild('popupscrollMe') private popupScrollContainer: ElementRef;
  empId: any;
  coachId: any;
  bbbInfo: any;
  bbburl: any;
  openViduRequest: object = {
    clientData: null,
    userid: this.UserId,
    sessionName: null,
    tokenOptions: {
      serverData: null,
      role: null
    }
  }
  openViduWebinarInfo:any = {
    sessionName: null,
    tokens: [],
    myUserName: null
  }
  session: boolean = false;
  ovSession: Session;
  ovLocalUsers: UserModel[];
  ovLayout: OpenViduLayout;
  ovLayoutOptions: OpenViduLayoutOptions;
  ovSettings:OvSettings = {
    chat: false,
    autopublish: false,
    toolbarButtons: {
      audio: true,
      video: true,
      screenShare: true,
      fullscreen: false,
      exit: true,
    }
  };
  constructor(
    private toastr: ToastrService,
    public chatService: WebinarServiceProvider,
    public cdf: ChangeDetectorRef,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer
  ) { }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
         // + this.myScrollContainer.nativeElement.scrollTop;
         if (this._videoworking) {
          this.popupScrollContainer.nativeElement.scrollTop = this.popupScrollContainer.nativeElement.scrollHeight;
         } else {
          this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
      }, 10);
    } catch (err) {
      console.log(err);
    }
    // console.log(this.videoCallDiv.nativeElement.offsetWidth);
  }

  ngOnInit() {
    // this.UserId = this.empId; // this.userDetail.id;
    console.log('videocall', this.videoCall);
    console.log('voicecall', this.voiceCall);
    console.log('course', this.course);
    console.log('usersdata', this.userData);
    this.data.chatlist = this.userData;
    this.ovSettings = {
      chat: false,
      autopublish: true,
      toolbarButtons: {
        audio: this.voiceCall || true,
        video: this.videoCall || true,
        screenShare: false,
        fullscreen: true,
        exit: true
      }
    };
    // this.data.chatlist.active = false;
    // console.log('usersdata', this.userData);
    this.callId = 10;
    this.bbburl = null;
    if (localStorage.getItem('userDetails')) {
      const userDetails = JSON.parse(localStorage.getItem('userDetails'));
      try {
        this.userDetail = userDetails;
        this.UserId = this.userDetail.id;
      } catch (e) {
        console.log(e);
      }
    }
    // if(this.UserId == 2){
    //   this.UserId = 2;
    //   this.coachId = 4;
    //     this.webinarinfo= {
    //       SenderId : this.UserId,
    //       ReceiverId : this.coachId,
    //       SenderSocketId : '',
    //       RecevierSocketId : '',
    //       SenderUsername : 'Bhavesh-Tandel',
    //       ReceiverUsername : 'Aditya-Sawant'
    //     };
    // }else if(this.UserId == 4){
    //   this.UserId = 4;
    //   this.coachId = 2;
    //   this.webinarinfo= {
    //     SenderId : this.UserId,
    //     ReceiverId : this.coachId,
    //     SenderSocketId : '',
    //     RecevierSocketId : '',
    //     SenderUsername : 'Aditya-Sawant',
    //     ReceiverUsername : 'Bhavesh-Tandel'
    //   };
    // }

    this.connectSocket(this.UserId);
    // this.selectFriendToChat(this.FriendUserId)
    // this.getChatList(this.UserId);
    this.incommingMessage();
    this.usercomming();
    this.ReceiveWebinar();
    this.Connecting();
    this.ApprovalReceive();
    this.EndedMeetingReceive();
    if (this.userData.length > 0) {
      this.selectFriendToChat(this.userData[0]);
    }
    if (this.userData.length == 1) {
      this.activeUser(0, 0);
    } else {
      this.activeUser(0, 1);
    }

    // this.invitationcomming();
    // this.userapprovalcomming();
  }
  connectSocket(UserId) {
    this.chatService.connectSocketServer(UserId);
  }
  // Store Current Data
  currentUserIndex: number;
  activeUser(currentindex, previousindex) {
    this.currentUserIndex = currentindex;
    if (previousindex || previousindex == 0) {
      this.data.chatlist[previousindex].active = true;
    }
    if (currentindex || currentindex == 0) {
      this.data.chatlist[currentindex].active = false;
    }
    this.prevIndex = currentindex;
  }

  incommingMessage() {
    /*
     * This eventt will display the new incmoing message
     */
    this.chatService.socketOn('add-message-response', response => {
      response.friendProfilePic = '../../../assets/images/user.png';
      if (response.fromUserId === this.FriendUserId) {
        this.data.messages.push(response);
        this.scrollToBottom();
        try {
          this.cdf.detectChanges();
        } catch (e) {
          console.log(e);
        }
      } else {
        let isToastRunning = false;
        this.data.chatlist.forEach(element => {
          if (element.id === response.fromUserId) {
            isToastRunning = true;
            this.toastr.success( response.message, element.name).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        });
        if (!isToastRunning) {
          this.toastr.success( 'You received a message.' ).onHidden.subscribe(()=>
          this.toastr.clear());
        }
        // if (this.webinarinfo.ReceiverId == response.fromUserId) {
        //   this.toastr.success( this.webinarinfo.ReceiverUsername, response.message );
        // } else {
        //   this.toastr.success( 'You received a message.' );
        // }

      }
      //this.chatService.scrollToBottom();
    });
  }

  selectFriendToChat(friend) {
    /*
     * Highlighting the selected user from the chat list
     */
    this.chatInit = true;
    this.spinner = true;
    this.messages = [];
    this.frienduserDetail = friend;
    this.webinarinfo = {
      SenderId: this.UserId,
      ReceiverId: friend.id,
      // SenderSocketId : '',
      // RecevierSocketId : '',
      SenderUsername: this.userDetail.username,
      ReceiverUsername: friend.name
    };
    // this.coachId = friend.id;
    this.FriendUserId = friend.id; // this.frienduserDetail.id;
    if( this.FriendUserId && this.UserId){
      this.checkUserFriendMap(this.UserId, this.FriendUserId);
      this.data.selectedFriendName = this.frienduserDetail['name'];
      this.data.selectedFriendId = this.frienduserDetail.id;
    }

    // this.chatUser = friendData[0];
    // this.data.chatlist[this.prevIndex].pendingMsgs = 0;
    /**
     * This HTTP call will fetch chat between two users
     */
    if (this.chatUser.online === undefined) {
      this.data.invite = true;
      this.data.inviteuserid = this.frienduserDetail.id;
      this.data.messages = [];
      // this.cdf.detectChanges();
    } else {
      // if((this.chatUser.accepted === 0 && this.chatUser.rejected === 0 && this.chatUser.invited === 0) || (this.chatUser.accepted === 0 && this.chatUser.rejected === 0 && this.chatUser.invited === 1)){
      // this.data.invite = true;
      // this.data.inviteuserid = 0;
      // }else{
      // this.data.invite = false;
      // this.data.inviteuserid = 0;
      // }
      //userdetail = this.data;
    }
    this.chatService
      .getMessages(this.UserId, this.FriendUserId, this.areaId, this.instanceId)
      .then((response: any) => {
        //this.$apply(() => {
        if (response.messages.length > 0) {
          this.data.messages = response.messages[0];
        }
        this.scrollToBottom();
        this.spinner = false;
        this.cdf.detectChanges();
        //});
      })
      .catch(error => {
        console.log(error);
        this.spinner = false;
        alert('Unexpected Error, Contact your Site Admin.');
      });
  }
  usercomming() {
    this.chatService.socketOn('user-comming-chat-list-response', response => {
      // if(this.data.chatlist){
      // this.data.chatlist.forEach((user,key)=>{
      // if(response.userinfo.length > 0){
      // if(user.userid == response.userinfo[0].userid){
      // this.data.chatlist[key].socketid = response.userinfo[0].socketid;
      // this.toastr.success( response.userinfo[0].username + ' ' + 'is online');
      // }
      // }
      // })
      // }
      if (response.userinfo.length > 0) {
        if (response.userinfo[0].userid === this.FriendUserId) {
          this.data.friendSocketId = response.userinfo[0].socketid;
          this.webinarinfo.RecevierSocketId = response.userinfo[0].socketid;
        }
      }
    });
  }

  sendChatMessage(event, value) {
    //if (event.keyCode === 13) {
    let fromUserId = null;
    let toSocketId = null;
    /* Fetching the selected User from the chat list starts */
    let selectedFriendId = this.webinarinfo.ReceiverId; //this.data.selectedFriendId;
    if (selectedFriendId === null) {
      return null;
    }

    /* Fetching the selected User from the chat list ends */
    /* Emmiting socket event to server with Message, starts */
    // if (this.friendData.length > 0) {
    fromUserId = this.webinarinfo.SenderId;
    toSocketId = this.data['friendSocketId'];
    let messagePacket = {
      message: value, //document.querySelector('#message').value,
      fromUserId: fromUserId,
      toUserId: selectedFriendId,
      toSocketId: toSocketId,
      userProfilePic: '../../../assets/images/user.png',
      friendId: selectedFriendId,
      areaId: this.areaId,
      instanceId: this.instanceId
    };
    this.data.messages.push(messagePacket);
    // this.chatService.socketEmit(`add-message`, messagePacket);
    this.chatService.socketEmit(`add-message`, messagePacket);
    this.message = '';
    this.scrollToBottom();
    //document.querySelector('#message').value = '';
    //this.chatService.scrollToBottom();
    // } else {
    // alert('Unexpected Error Occured,Please contact Admin');
    // }
    /* Emmiting socket event to server with Message, ends */
    //}
  }

  checkUserFriendMap(userId, friendId) {
    let param = {
      userId: userId,
      friendId: friendId
    };
    this.chatService
      .insertDirectUserFriendMap(param)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  /**
   * Start
   * @Author Bhavesh Tandel
   * Video Call Method
   */

  /**
   * Open Invitation Modal POPUP
   * @method OpenInviationPopup
   * @param coachId
   * @param trainerId
   */
  OpenInviationPopup() {
    console.log('Open Modal Up');
    this.visibleSendModal = true;
    this.visibleReceiveModal = false;
    this.visibleConnectingModal = false;
  }
  /**
   * Send invitaion to receiver
   * @method SendInviation
   * @param coachId
   * @param trainerId
   * @event Accepted
   * @event Rejected
   */
  SendInviation(event) {
    if (event) {
      this.openViduRequest = {
        clientData: this.webinarinfo.SenderUsername,
        userid: this.UserId,
        sessionName: this.course.activityName,
        tokenOptions: {
          serverData: this.webinarinfo.SenderUsername,
          role: 'PUBLISHER'
        },
        isCreator: true,
        recording: this.config.recording,
        creator: this.config.creator
      }
      const user = {
        callId: this.callId,
        senderId: this.UserId,
        receiverId: this.FriendUserId, //this.coachId,
        senderName: this.webinarinfo.SenderUsername,
        receiverName: this.webinarinfo.ReceiverUsername,
        areaId: this.areaId,
        openVidu: this.openViduRequest,
        recording: this.config.recording,
        creator: this.config.creator,
        isCreator: this.config.creator
      };
      this.chatService.socketEmit(`send-webinar-call`, user);
    }
  }
  /**
   * @method Connecting
   * @param flag
   */
  Connecting() {
    console.log('Connecting');
    this.chatService.socketOn('connecting-webinar-call', response => {
      console.log(response);
      this.visibleSendModal = false;
      this.visibleReceiveModal = false;
      this.visibleConnectingModal = true;
      this.senderJoinMeeting = response.bbbInfo.start;
      // this.bbbInfo = response.bbbInfo;
      this.openViduWebinarInfo = {
        sessionName: response.bbbInfo.sessionName,
        tokens: [],
        myUserName: this.webinarinfo.SenderUsername
      }
      this.openViduWebinarInfo.tokens.push(response.bbbInfo.token);
      // this.openWebinarWindow(this.openViduWebinarInfo);

    });
  }
  /**
   * Receive Invitation from Sender
   * @method ReceiveInvitation
   *
   */
  ReceiveWebinar() {
    this.chatService.socketOn('receive-webinar-call', response => {
      console.log(response);
      if (response.bbbInfo) {
        this.bbbInfo = response.bbbInfo;
      }
      this.openReceiveModalPopUp();
    });
  }
  openReceiveModalPopUp() {
    console.log('Receive Invitation');
    this.visibleSendModal = false;
    this.visibleReceiveModal = true;
    this.visibleConnectingModal = false;
  }
  ReceiveInvitation(event) { }
  AcceptInvitation(event) {
    if (event === 'true') {
      // this.bbbInfo.receiverId = this.FriendUserId; //this.coachId;
      // this.bbbInfo.senderId = this.webinarinfo.SenderId;
      // // this.bbbInfo.senderName = this.webinarinfo.SenderUsername,
      // // this.bbbInfo.receiverName = this.webinarinfo.ReceiverUsername
      // this.bbbInfo.name = this.webinarinfo.SenderUsername;
      // this.bbbInfo.status = true;
      this.openViduRequest = {
        clientData: this.webinarinfo.SenderUsername,
        userid: this.UserId,
        sessionName: this.course.activityName,
        tokenOptions: {
          serverData: this.webinarinfo.SenderUsername,
          role: 'PUBLISHER'
        },
        status: true,
        receiverId: this.FriendUserId,
        senderId: this.webinarinfo.SenderId
      }
      this.chatService.socketEmit(
        `receiver-accept-request-webinar-call`,
        this.openViduRequest
      );
    } else if (event === 'false') {
      this.bbbInfo.receiverId = this.FriendUserId; // this.coachId;
      this.bbbInfo.name = this.webinarinfo.ReceiverUsername;
      this.bbbInfo.status = false;
      this.chatService.socketEmit(
        `receiver-accept-request-webinar-call`,
        this.bbbInfo
      );
      //this.chatService.socketEmit(`receiver-accept-request-webinar-call`, this.bbbInfo);
    }
    this.CloseModal(true);
  }

  /**
   * @method StartMeeting
   */
  StartMeeting() { }

  /**
   * @method JoinMeeting
   */
  JoinMeeting(event) {
    var obj = {
      meetingID: this.bbbInfo.meetingID,
      fullName: this.webinarinfo.ReceiverUsername,
      password: this.bbbInfo.moderatorPW,
      senderId: this.UserId,
      receiverId: this.FriendUserId, // this.coachId,
      senderName: this.webinarinfo.SenderUsername,
      receiverName: this.webinarinfo.ReceiverUsername
    };
    this.chatService.socketEmit('accept-request-webinar-call', obj);
  }

  CloseModal($event) {
    console.log('Close Event');
    if (this.visibleConnectingModal) {
      this.visibleConnectingModal = false;
    }
    if (this.visibleSendModal) {
      this.visibleSendModal = false;
    }
    if (this.visibleReceiveModal) {
      this.visibleReceiveModal = false;
    }
    if (this.visibleRejectModal) {
      this.visibleRejectModal = false;
    }
    if (this.visibleEndedModal) {
      this.visibleEndedModal = false;
    }
    this.visibleEndModal = false;
  }
  openRejectModalPopup() {
    this.CloseModal(false);
    this.visibleRejectModal = true;
  }
  ApprovalReceive() {
    this.chatService.socketOn('sender-receive-approval', data => {
      console.log('Socket - sender-receive-approval', data);
      this.CloseModal(null);
      // var senderUrl = this.senderJoinMeeting;
      if (data.status == true) {
        // this.openViduWebinarInfo.tokens.push(data.bbbInfo.token);
        this.openWebinarWindow(this.openViduWebinarInfo);
        setTimeout(() => {
          // this.openWebinarWindow(response.bbbInfo.start);
          this.chatService.socketEmit(`start-recording-webinar-call`, this.openViduWebinarInfo);
        }, 1000);
      } else {
        console.log('User Reject your call!');
        this.openRejectModalPopup();
      }
    });
    this.chatService.socketOn('join-receiver', data => {
      if (data.bbbInfo) {
        // this.bbbInfo.start = data.bbbInfo;
        const webinar = data.bbbInfo;
        this.openViduWebinarInfo = {
          sessionName: webinar.sessionName,
          tokens: [],
          myUserName: webinar.userName
        };
        this.openViduWebinarInfo.tokens.push(webinar.token);
        this.openWebinarWindow(this.openViduWebinarInfo);
      }
    });
  }
  EndedMeeting() {
    if (this.openViduWebinarInfo.tokens.length > 0) {
      var obj = {
        // meetingID: this.bbbInfo.meetingID
        sessionname: this.openViduWebinarInfo.sessionName,
        token: this.openViduWebinarInfo.tokens[0],
        userid: this.UserId,
        senderId: this.webinarinfo.SenderId,
        receiverId: this.webinarinfo.ReceiverId,
        senderName: this.webinarinfo.SenderUsername,
        receiverName: this.webinarinfo.ReceiverUsername,
      };
      this.chatService.socketEmit('end-webinar-call', obj);
      // if(this.videoCall && this.voiceCall)
      // {
      this.leaveSession();
      this.CloseModal(true);
      this.bbbInfo = {};
      this.session = false;
      this._videoworking = false;
      this.visibleEndModal = true;
      this.openViduWebinarInfo = {
        sessionName: null,
        tokens: [],
        myUserName: null
      }
      this.callEnd.emit(this.videoCall);
      // }
    }
  }
  EndedMeetingReceive() {
    this.chatService.socketOn('end-webinar-msg', result => {
      // this.ovSessionComponent.emitLeaveSessionEvent(null);
      if (result.user && (result.user.sessionName === this.openViduWebinarInfo.sessionName)) {
        this.toastr.success(null, result.user.msg).onHidden.subscribe(()=>
        this.toastr.clear());
      }
      // this.CloseModal(true);
      // this.bbbInfo = {};
      // this.session = false;
      // this._videoworking = false;
      // this.visibleEndModal = true;
      // this.openViduWebinarInfo = {
      //   sessionName: null,
      //   tokens: [],
      //   myUserName: null
      // }
    });
  }

  /**
   * End
   * @Author Bhavesh Tandel
   * Video Call Method
   */

  openWebinarWindow(value) {
    // this.bbburl = this.transform(url);
    this._videoworking = true;
    this.session = true;
    console.log(value);
    this.joinSession();
  }
  endVideo() {
    this.visibleEndedModal = true;
  }
  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
      url = cleanPath(document.baseURI + '/' + url);
    }
    return encodeURIComponent(url);
  }
  transform(url): any {
    // url = 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg';
    // url = 'https://angular.io/api/platform-browser/DomSanitizer#bypassSecurityTrustUrl';
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnDestroy() {
    this.handlerLeaveSessionEvent(null);
  }
  // Responsive Width
  // @ViewChild('videoCallDiv')
  // videoCallDiv: ElementRef;
  /**
   * @description Open Vidu Events
   * @method handlerJoinSessionEvent
   * @method handlerLeaveSessionEvent
   * @method handlerErrorEvent
   * @method myMethod
   */

  handlerJoinSessionEvent(event): void {
    this.myMethod();
  }

  handlerLeaveSessionEvent(event): void {
    this.EndedMeeting();
    this.openViduWebinarInfo.tokens = [];
    this.session = false;
    this.leaveSession();
    this.chatService.disconnect();
  }

  handlerErrorEvent(event): void {
    // Do something
    console.log('handlerErrorEvent', event);
  }

  myMethod() {

    this.ovSession = this.ovSessionComponent.getSession();
    this.ovLocalUsers = this.ovSessionComponent.getLocalUsers();
    this.ovLayout = this.ovSessionComponent.getOpenviduLayout();
    this.ovLayoutOptions = this.ovSessionComponent.getOpenviduLayoutOptions();

    this.ovSession.on('streamCreated', (event: StreamEvent) => {
      // Do something
      console.log('streamCreated', event);
    });

    this.ovSession.on('streamDestroyed', (event: StreamEvent) => {
      // Do something
      console.log('streamDestroyed', event);
    });
  }


  // OpenVidu objects
  OV: OpenVidu;
  ovsession: Session;
  publisher: StreamManager; // Local
  subscribers: StreamManager[] = []; // Remotes
  // Join form
  mySessionId: string;
  myUserName: string;

  // Main video of the page, will be 'publisher' or one of the 'subscribers',
  // updated by click event in UserVideoComponent children
  mainStreamManager: StreamManager;
  joinSession() {
    let token = null;
    if (this.openViduWebinarInfo.tokens.length > 0) {
      token = this.openViduWebinarInfo.tokens[0];
    }
    // --- 1) Get an OpenVidu object ---

    this.OV = new OpenVidu();

    // --- 2) Init a session ---

    this.ovsession = this.OV.initSession();

    // --- 3) Specify the actions when events take place in the session ---

    // On every new Stream received...
    this.ovsession.on('streamCreated', (event: StreamEvent) => {

      // Subscribe to the Stream to receive it. Second parameter is undefined
      // so OpenVidu doesn't create an HTML video by its own
      let subscriber: Subscriber = this.ovsession.subscribe(event.stream, undefined);
      this.subscribers.push(subscriber);
    });

    // On every Stream destroyed...
    this.ovsession.on('streamDestroyed', (event: StreamEvent) => {

      // Remove the stream from 'subscribers' array
      this.deleteSubscriber(event.stream.streamManager);
    });

    // --- 4) Connect to the session with a valid user token ---

    // 'getToken' method is simulating what your server-side should do.
    // 'token' parameter should be retrieved and returned by your own backend
    // this.getToken().then(token => {

      // First param is the token got from OpenVidu Server. Second param can be retrieved by every user on event
      // 'streamCreated' (property Stream.connection.data), and will be appended to DOM as the user's nickname
      this.ovsession.connect(token, { clientData: this.openViduWebinarInfo.myUserName })
        .then(() => {

          // --- 5) Get your own camera stream ---

          // Init a publisher passing undefined as targetElement (we don't want OpenVidu to insert a video
          // element: we will manage it on our own) and with the desired properties
          let publisher: Publisher = this.OV.initPublisher(undefined, {
            audioSource: undefined, // The source of audio. If undefined default microphone
            videoSource: undefined, // The source of video. If undefined default webcam
            publishAudio: true,     // Whether you want to start publishing with your audio unmuted or not
            publishVideo: true,     // Whether you want to start publishing with your video enabled or not
            resolution: '640x480',  // The resolution of your video
            frameRate: 30,          // The frame rate of your video
            insertMode: 'APPEND',   // How the video is inserted in the target element 'video-container'
            mirror: false           // Whether to mirror your local video or not
          });

          // --- 6) Publish your stream ---

          this.ovsession.publish(publisher);

          // Set the main video in the page to display our webcam and store our Publisher
          this.mainStreamManager = publisher;
          this.publisher = publisher;
        })
        .catch(error => {
          console.log('There was an error connecting to the session:', error.code, error.message);
        });
    // });
  }

  leaveSession() {

    // --- 7) Leave the session by calling 'disconnect' method over the Session object ---

    if (this.ovsession) { this.ovsession.disconnect(); };

    // Empty all properties...
    this.subscribers = [];
    delete this.publisher;
    delete this.session;
    delete this.OV;
    this.generateParticipantInfo();
  }
  private generateParticipantInfo() {
    // Random user nickname and sessionId
    // this.mySessionId = 'SessionA';
    // this.myUserName = 'Participant' + Math.floor(Math.random() * 100);
  }

  private deleteSubscriber(streamManager: StreamManager): void {
    let index = this.subscribers.indexOf(streamManager, 0);
    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  }

  updateMainStreamManager(streamManager: StreamManager) {
    this.mainStreamManager = streamManager;
  }
  @HostListener('window:beforeunload')
  beforeunloadHandler() {
    // On window closed leave session
    this.leaveSession();
  }

}
