import { Component, OnInit } from '@angular/core';
import { PollServiceProvider } from '../../service/poll-service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {

	pollSummary:any;
	pollTitle:string;
	showResult: any = false;
	submitpollArray: any[];
  	questions=[
    //   { id:1,
    //     qTypeId:2,
    //     question_no:1 ,
    //     questionsInTotal:20 ,
    //     question: "Which of the following is the core of an electromagnet ?",
	// 	options:[ "Hard Iron", "Soft Iron", "Rusted Iron", "None of the above"],
    //     answer:"Answer"
    //   },
  	];

		constructor(private PSP: PollServiceProvider, private Toastr: ToastrService,
			private router:Router, private routes:ActivatedRoute) { }

  	ngOnInit() {
		this.showResult = false;
    	this.pollSummary = this.PSP.getPollSummary();
		console.log('POLLSUMMARY===>',this.pollSummary);
		this.pollTitle = this.pollSummary.name;
		var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		var param = {
			tId: userDetails.tenantId,
			pId: this.pollSummary.id,
			wComp: this.pollSummary.wComp,
		}
		var url = ENUM.domain + ENUM.url.getPollQuestionAnswers;
		this.PSP.getPoll(url, param)
		.then((result:any)=>{
			console.log('RESULT===>',result);
			this.questions = result.data;
			for(var i=0;i<this.questions.length;i++){
				for(var j=0;j<this.questions[i].optionList.length;j++){
					this.questions[i].optionList[j].sFlag = false;
				}
			}
		}).catch(result=>{
			console.log('RESULT===>',result);
		})

	}

	selectOption(mainData,index){

		for(var i=0;i<this.questions.length;i++){
			for(var j=0;j<this.questions[i].optionList.length;j++){
				this.questions[i].optionList[j].sFlag = false;
			}
		}
		if(mainData.optionList[index].sFlag == true){
			mainData.optionList[index].sFlag = false;
		}else{
			mainData.optionList[index].sFlag = true;
		}
		console.log('MainData===>',mainData);
		console.log('Index',index);

	}

	submitPoll(){

		var employeeId = localStorage.getItem('employeeId');
		var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		var pollId = this.pollSummary.id;
		var quesId = this.questions[0].qId;
		for(var i=0;i<this.questions.length;i++){
			for(var j=0;j<this.questions[i].optionList.length;j++){
				if(this.questions[i].optionList[j].sFlag == true){
					var value = this.questions[i].optionList[j].optionText;
				}
			}
		}
		if(this.pollSummary.recordUsers == 2){
			var param = {
				empId: employeeId,
				pId: pollId,
				qId: quesId,
				val: value,
				tId: userDetails.tenantId,
				uId: userDetails.id
			}
			var url = ENUM.domain + ENUM.url.submitPollAnswers;
			this.PSP.submitPoll(url, param)
			.then((result:any)=>{
				console.log('RESULT===>',result);
				if(result.type === true){
					this.Toastr.success('Poll Submitted Successfully','Success!').onHidden.subscribe(()=>
					this.Toastr.clear());
					this.submitpollArray = result.data;
					if(result.pollResult.length > 0){
						this.showResult = true;
						this.questions = result.pollResult;
					  }

					//
				}else{
					this.Toastr.warning('Please resubmit poll','Error Occured!').onHidden.subscribe(()=>
					this.Toastr.clear());
				}
			}).catch(result=>{
				console.log('RESULT===>',result);
				this.Toastr.warning('Please resubmit poll','Error Occured!').onHidden.subscribe(()=>
				this.Toastr.clear());
			})
		}else{
			var params = {
				pId: pollId,
				qId: quesId,
				val: value,
				tId: userDetails.tenantId,
			}
			var url = ENUM.domain + ENUM.url.submitPollAnswers;
			this.PSP.submitPoll(url, params)
			.then((result:any)=>{
				console.log('RESULT===>',result);
				if(result.type == true){
					this.Toastr.success('Poll Submitted Successfully','Success!').onHidden.subscribe(()=>
					this.Toastr.clear());
					this.submitpollArray = result.data;
					if(result.pollResult.length > 0){
					  this.showResult = true;
					  this.questions = result.pollResult;
					}

				}else{
					this.Toastr.warning('Please resubmit poll','Error Occured!').onHidden.subscribe(()=>
					this.Toastr.clear());
				}
			}).catch(result=>{
				console.log('RESULT===>',result);
				this.Toastr.warning('Please resubmit poll','Error Occured!').onHidden.subscribe(()=>
				this.Toastr.clear());
			})
		}
		// console.log('PARAM', param);
		// console.log('PARAMS', params);

	}

	goToDashboard(){
		this.router.navigate(['../../dashboard'],{relativeTo:this.routes});
	}

	back(){
		window.history.back();
	}
}
