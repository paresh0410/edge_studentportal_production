import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttBookSlotsComponent } from './ttt-book-slots.component';

describe('TttBookSlotsComponent', () => {
  let component: TttBookSlotsComponent;
  let fixture: ComponentFixture<TttBookSlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttBookSlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttBookSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
