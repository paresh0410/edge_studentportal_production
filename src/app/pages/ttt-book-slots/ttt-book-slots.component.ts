import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ENUM } from '../../../app/service/enum';
import { TrainTheTrainerServiceProvider } from '../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { CalendarServiceProvider } from '../../service/calendar-service';
import { ToastrService } from 'ngx-toastr';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
    parseInput: 'DD-MM-YYYY HH:mm:ss',
    datePickerInput: 'DD-MM-YYYY',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-ttt-book-slots',
  templateUrl: './ttt-book-slots.component.html',
  styleUrls: ['./ttt-book-slots.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class TttBookSlotsComponent implements OnInit {
  bookingDetails: any = [];
  listPreapred: any = [];
  userDetails: any;
  userId: any;
  employeeId: any;
  tenantId: any;
  confirmdata: any = [];
  checkInDate: any;
  // startTime: any;
  checkOutDate: any;
  // endTime: any;
  accomodationRequired = false;
  public monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  workflowdetail: any = [];
  batcheslist: any = [];
  abc: boolean = false;
  xyz: boolean = false;
  accomodation: boolean = false;
  constructor(
    private tttServiceProvider: TrainTheTrainerServiceProvider, private routes: ActivatedRoute, private router: Router,
    public spinner: NgxSpinnerService, public calsp: CalendarServiceProvider,
    private toast: ToastrService,
  ) {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userId = this.userDetails.id;
    this.tenantId = this.userDetails.tenantId;
    this.employeeId = localStorage.getItem('employeeId');
    console.log('employeeId', this.employeeId);
    console.log('this.userDetails', this.userDetails);
    this.workflowdetail = this.calsp.data;
    console.log(this.workflowdetail);
    if (this.workflowdetail.actionType === 'book') {
      this.getAllSlot();
    }

    if (this.workflowdetail.actionType === 'confirm batch') {
      this.batchlist();
    }
  }

  ngOnInit() {
    // this.getAllSlot();
  }

  getAllSlot() {
    this.spinner.show();
    this.bookingDetails = [];
    this.listPreapred = [];
    const param = {
      wfId: this.workflowdetail.instanceId,
      tId: this.tenantId,
    };

    const url = ENUM.domain + ENUM.url.workflowslotlist;
    this.tttServiceProvider
      .getWorkFlowslot(url, param)
      .then((result: any) => {
        this.spinner.hide();
        try {
          //  result.data.filter(item => {
          //   if (item.employeeId) {
          //   }
          //   else {
          //     return item;
          //   }
          // });
          // result.data = result.data.filter( item => item.includes( 'employeeId' ) );
          this.bookingDetails = result.data;
          result.data.forEach( (item) => {
            if (!item['employeeId']) {
              this.listPreapred.push(item);
            }
          });
          console.log ('this.bookingDetails', this.bookingDetails);
           this.bookingDetails = result.data;
          // for (let i = 0; i < this.bookingDetails.length; i++) {
          //   this.bookingDetails[i].callStartTime = this.settime(this.bookingDetails[i].callStartTime);
          //   this.bookingDetails[i].callEndTime = this.settime(this.bookingDetails[i].callEndTime);
          // }
          console.log ('this.bookingDetails', this.bookingDetails);
          this.abc = true;
          console.log(this.bookingDetails);
        } catch (e) {
          console.log(e);
        }
      })
      .catch(result => {
        this.spinner.hide();
        console.log('RESULT tttNomination Error===>', result);
      });
  }
  settime(date) {
    var d = new Date(date.toLocaleString());
    return d;
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + d.getMonth(),
      day = '' + d.getDate(),
      year = d.getFullYear();

    month = this.monthNames[month];
    if (day.length < 2) day = '0' + day;

    var dateString = [day, month, year].join(' ');

    return dateString;
  }
  bookevent(item) {
    this.spinner.show();
    const bookdata = {
      weId: item.evalId,
      wfId: item.workflowId,
      trainerId: item.trainerId,
      uId: this.userId,
      empId: this.employeeId,
      tId: this.tenantId,
    };
    console.log(bookdata);
    const url = ENUM.domain + ENUM.url.BookSlot;
    this.tttServiceProvider
      .bookWorkFlowslot(url, bookdata)
      .then((result: any) => {
        this.spinner.hide();
        if (result.type == true) {
          if (result.data[0].isChanged == 0) {
            this.toast.warning(result.data[0].msg, 'Warning').onHidden.subscribe(()=>
            this.toast.clear());
            this.getAllSlot();
          } else {
            this.toast.success(result.data[0].msg, 'Success').onHidden.subscribe(()=>
            this.toast.clear());
            setTimeout (() => {
              window.history.back();
            }, 2000);
          }
          // this.router.navigate(['../calendar'], { relativeTo: this.routes });
        }
      })
      .catch(result => {
        this.spinner.hide();
        this.toast.warning('Something went wrong. please try again later.', 'error').onHidden.subscribe(()=>
        this.toast.clear());
        console.log('RESULT tttNomination Error===>', result);
      });
  }
  batchlist() {
    this.spinner.show();
    const data = {
      wfId: this.workflowdetail.instanceId,
      tId: this.tenantId,
      empId: this.employeeId,
    };
    const url = ENUM.domain + ENUM.url.ttt_batch_list;
    this.tttServiceProvider.get(url, data).then(
      res => {
        this.spinner.hide();
        console.log(res);
        try {
          if (res['type'] == true) {
            try {
              this.batcheslist = res['data'];
              this.xyz = true;
              for (let i = 0; i < this.batcheslist.length; i++) {
                this.batcheslist[i].id = i + 1;
                this.batcheslist[i].startDate = this.formatDate(
                  this.batcheslist[i].startDate
                );
                this.batcheslist[i].endDate = this.formatDate(
                  this.batcheslist[i].endDate
                );
              }
            } catch { }
          }
        } catch { }
      },
      err => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }
  confirmbatch(event) {
    // this.spinner.show();
    this.confirmdata = {
      batchId: event.courseId,
      batchStartDate: event.startDate,
      batchEndDate: event.endDate,
      nomEmpId: this.employeeId,
      eventId: this.workflowdetail.eventId ? this.workflowdetail.eventId: event.eventId,
      userId: this.userId,
      tId: this.tenantId,
      wfId: this.workflowdetail.instanceId,
    };
    this.accomodation = true;
  }
  confirm() {
    this.spinner.show();
    let value;
    if (this.accomodationRequired) {
      value = 1;
    } else {
      value = 0;
    }
    this.confirmdata.isAccoReq = value;
    if (this.accomodationRequired) {
      if(this.checkInDate !== undefined){
        this.confirmdata.checkInDate =  this.getDate(this.checkInDate[0]);
        this.confirmdata.checkInTime =  this.getTime(this.checkInDate[0]);
        this.confirmdata.checkOutDate =  this.getDate(this.checkInDate[1]);
        this.confirmdata.checkOutTime =   this.getTime(this.checkInDate[1]);

        this.confirmdata.checkInDate =  this.getDate(this.checkInDate);
        this.confirmdata.checkInTime =  this.getTime(this.checkInDate);
        this.confirmdata.checkOutDate =  this.getDate(this.checkOutDate);
        this.confirmdata.checkOutTime =   this.getTime(this.checkOutDate);

        this.callConfirmBatch();
      }else{
        this.toast.warning('Check in date is required', 'Warning').onHidden.subscribe(()=>
        this.toast.clear());
      }

    }else {
      this.confirmdata.checkInDate =  '';
      this.confirmdata.checkInTime =  '';
      this.confirmdata.checkOutDate =  '';
      this.confirmdata.checkOutTime =   '';
      this.callConfirmBatch();
    }

    console.log(this.confirmdata);

    console.log(this.confirmdata);

  }

  callConfirmBatch(){
    const url = ENUM.domain + ENUM.url.confirm_batch;
    this.tttServiceProvider.get(url, this.confirmdata).then(res => {
      console.log(res);
      this.spinner.hide();
      try {
        if (res['type'] == true) {
          this.accomodation = false;
          this.confirmdata.actionType = '';
          window.history.back();
          // this.router.navigate(['../calendar'], { relativeTo: this.routes });
        }
      } catch (e) {
        console.log(e);
      }
    }, err => {
      this.accomodation = false;
      this.spinner.hide();
      console.log(err);
    });
  }

  closemodel() {
    this.accomodation = false;
  }
  back() {
    window.history.back();
  }

  getDate(date) {
    const currentDate = new Date(date);
    const extractDate = currentDate.getDate();
    const month = currentDate.getMonth(); //Be careful! January is 0 not 1
    const year = currentDate.getFullYear();
    const dateString = extractDate + '-' + (month + 1) + '-' + year;
    return dateString;
    }
    getTime(date) {
    const currentDate = new Date(date);
    const hour = currentDate.getDate();
    const minute = currentDate.getMonth(); //Be careful! January is 0 not 1
    const second = currentDate.getSeconds();
    const timeString = hour + ':' + minute + ':' + second;
    return timeString;
    }
}
