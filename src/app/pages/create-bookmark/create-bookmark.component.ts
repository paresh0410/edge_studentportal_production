import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-create-bookmark',
  templateUrl: './create-bookmark.component.html',
  styleUrls: ['./create-bookmark.component.scss']
})
export class CreateBookmarkComponent implements OnInit {

  isMandatory:boolean;
  isSelected:boolean;
  cardImg:string;
  content:string;
  stages:string;

  bookmarkitems=[
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    
  ];

  name:String;
  time:String;

  hashtagArray=[
    { name: "Design", time: "42 mins "},
    { name: "Sales", time: "42 mins "},
    { name: "Positivity", time: "42 mins "},
    { name: "JohnDoe",time: "42 mins"},
    { name: "Design", time: "42 mins "},
    { name: "Sales",time: "42 mins "},
    { name: "Positivity", time: "42 mins "},
    { name: "JohnDoe",time: "42 mins "},
    { name: "Design", time: "42 mins "},
    { name: "Sales", time: "42 mins "},
    { name: "Positivity", time: "42 mins "},
    { name: "JohnDoe", time: "42 mins "},
    { name: "Design", time: "42 mins "},
  ];
  

  
  selectedCount:number;

  radiofn(post){
    this.selectedCount=0;
  
      if(!post.isSelected){       
      post.isSelected = true;       //if radio button is false make it true
      
      return  this.selectedCount=this.count();        // store count in variable if true
      }else{
      post.isSelected = false;
      return  this.selectedCount=this.count();        // store count in variable if false
      }   
    }

    count(){
      var count=0;
      this.bookmarkitems.forEach(obj => {      //for each object in array of addpathway
        console.log(obj);
        if(obj.isSelected==true)            // if array element is selected
        {
          count++;                          //increment count
        }
      })
      return  count;
    }

    
  togglesave(item){
    if(!item.save){
      item.save = true;
    }else{
      item.save = false;
    }
  }



  constructor() { }

  ngOnInit() {
  }

}
