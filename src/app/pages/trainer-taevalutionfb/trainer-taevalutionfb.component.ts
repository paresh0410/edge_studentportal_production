import { Component, OnInit } from '@angular/core';
import { TrainerAutomationServiceProvider } from '../../service/trainer-automation.service';
import { TrainTheTrainerServiceProvider } from '../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../../service/enum';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'ngx-trainer-taevalutionfb',
  templateUrl: './trainer-taevalutionfb.component.html',
  styleUrls: ['./trainer-taevalutionfb.component.scss']
})
export class TrainerTAevalutionfbComponent implements OnInit {
  userDetails: any = [];
  trainerbatch: any = [];
  batchCourseId: any = [];
  batchCourse: any;
  tenantId: any;
  noParticipants: boolean = false;
  giveFeedback: boolean;
  participants: any = [];
  participantemp: any = [];
  questions: any = [];
  title: any;
  showSubmit = true;
  isPublished = false;
  mandatoryQuesString: any = [];
  constructor(private tttService: TrainTheTrainerServiceProvider, public TAServiceProvider: TrainerAutomationServiceProvider,
    public spinner: NgxSpinnerService, private Toastr: ToastrService, private sanitizer: DomSanitizer) {
    this.userDetails = JSON.parse(localStorage.getItem("userdetail"));
    this.trainerbatch = this.TAServiceProvider.feedbackData;
    console.log(this.trainerbatch);
    // if (this.trainerbatch.stepId === 10) {
    //   this.title = 'Mock';
    // } else if (this.trainerbatch.stepId === 11) {
    //   this.title = 'Assessment';
    // } else if (this.trainerbatch.stepId === 14) {
    //   this.title = 'Mock';
    // } else {
    //   this.title = 'Viva Feedback';
    // }
    this.title = 'Evaluate';
    this.batchCourseId = this.TAServiceProvider.batchData.batchId;
    // this.coursecontentdata = this.tttService.coursecontent;
    // console.log(this.coursecontentdata);
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.batchCourse = this.TAServiceProvider.batchData;
    if(this.batchCourse.workflowId == null || this.batchCourse.workflowId == undefined){
      this.showSubmit = true;
    }else{
      this.showSubmit = false;
    }
    this.tenantId = this.userDetails.tenantId;
    this.getparticipants();
  }

  ngOnInit() {
    console.log("NIKHIL");
  }

  checkEvaluatedOrNot(item) {
    this.spinner.show();
    let url = ENUM.domain + ENUM.url.check_fb_evaluated_or_not;
    let param = {
      'wfId': this.trainerbatch.wfId,
      'stepId': this.trainerbatch.stepId,
      'empId': item.empId
    }
    this.TAServiceProvider.getParticipant(url, param)
      .then((result: any) => {
        this.spinner.hide();
        if(result.type == true) {
          if (result.data && result.data.isPublished == 'Y') {
            this.Toastr.warning(result.data.msg, 'Warning');
          } else {
            this.viewFeedback(item);
          }
        }

      }).catch(result => {
        this.spinner.hide();
        console.log('RESULT Error participant===>', result);
      })
  }

  getparticipants() {
    this.spinner.show();
    let url = ENUM.domain + ENUM.url.getAllUshaParticipants;
    let param = {
      "reqFrom": "WEB",
      "participantCourseId": this.batchCourseId,
      "tId": this.tenantId,
      "lmt": null,
      "pNo": null,
      "flag": 2,
      "actId": this.trainerbatch.activityId ? this.trainerbatch.activityId : 0,
      'wfId': this.trainerbatch.wfId,
      'stepId': this.trainerbatch.stepId,
    }
    this.TAServiceProvider.getParticipant(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('RESULT Success participant===>', result);
        if (result.data.length == 0) {
          this.noParticipants = true;
        } else {
          this.noParticipants = false;
          this.participants = result.data;
        }

      }).catch(result => {
        this.spinner.hide();
        console.log('RESULT Error participant===>', result);
      })
  }

  viewFeedback(element) {
    this.participantemp = element;
    console.log(this.participantemp);
    this.giveFeedback = true;
    this.fbquestion();
  }
  goback() {
    console.log("feedback Data >>");
    this.giveFeedback = false;
  }
  backtobatch(){
    window.history.back();
  }
  fbquestion() {
    var param = {
      fId: this.trainerbatch.feedbackId,
      cId: this.trainerbatch.courseId,
      mId: this.trainerbatch.moduleId,
      actId: this.trainerbatch.activityId,
      // fId: this.coursecontentdata.,
      tId: this.userDetails.tenantId,
      empId: this.participantemp.empId,
    };
    var url = ENUM.domain + ENUM.url.getEvalutionfbquestion;
    this.TAServiceProvider
      .get(url, param)
      .then((result: any) => {
        this.questions = result.data;
        for (var i = 0; i < this.questions.length; i++) {
          this.questions[i].qId = i + 1;
          if ( this.questions[i].questionTypeId == 4){
            this.questions[i]['sRateAns'] = 0;
          }
          if (this.questions[i].questionTypeId === 2 || this.questions[i].questionTypeId === 3) {
              for (var j = 0; j < this.questions[i].optionList.length; j++) {
                  if (this.questions[i].value) {
                      if (this.questions[i].value === this.questions[i].optionList[j].option) {
                          this.questions[i].optionList[j].sFlag = true;
                      } else {
                          this.questions[i].optionList[j].sFlag = false;
                      }
                  } else {
                      this.questions[i].optionList[j].sFlag = false;
                  }
              }
          } else if (this.questions[i].questionTypeId === 1) {
              this.questions[i].questionPlaceholder = "Minimum " + this.questions[i].optionList[0].minLength + " and maximum " + this.questions[i].optionList[0].maxLength + " characters";
              this.questions[i].minLen = parseInt(this.questions[i].optionList[0].minLength);
              this.questions[i].maxLen = parseInt(this.questions[i].optionList[0].maxLength);
              if (this.questions[i].value) {
                  this.questions[i].optionList[0]['sAns'] = this.questions[i].value;
              } else {
                  this.questions[i].optionList[0]['sAns'] = '';
              }
          } else {
              var temp = parseInt(this.questions[i].optionList[0].noStar);
              var tempRatingObj = {};
              var tempRatingArr = [];
              for (var j = 0; j < temp; j++) {
                  tempRatingObj = {
                      option: j + 1
                  };
                  tempRatingArr.push(tempRatingObj);
              }
              this.questions[i].rating = tempRatingArr;
              this.questions[i]['rate'] = parseInt(this.questions[i].value);
              if (this.questions[i].rate) {
                  this.questions[i].sRateAns = this.questions[i].rate;
              }
              else { // this.feedbacklist[i].sRateAns = 0;              
              }
             }
          }
        // for (var i = 0; i < this.questions.length; i++) {
        //   this.questions[i].qId = i + 1;
        //   if (
        //     this.questions[i].questionTypeId == 2 ||
        //     this.questions[i].questionTypeId == 3
        //   ) {
        //     for (var j = 0; j < this.questions[i].optionList.length; j++) {

        //       this.questions[i].optionList[j].sFlag = false;
        //     }
        //   } else if (this.questions[i].questionTypeId == 1) {
        //     this.questions[i].questionPlaceholder =
        //       "Minimum " +
        //       this.questions[i].optionList[0].minLength +
        //       " and maximum " +
        //       this.questions[i].optionList[0].maxLength +
        //       " characters";
        //     this.questions[i].minLen = parseInt(
        //       this.questions[i].optionList[0].minLength
        //     );
        //     this.questions[i].maxLen = parseInt(
        //       this.questions[i].optionList[0].maxLength
        //     );
        //     this.questions[i].optionList[0].sAns = "";
        //   } else {
        //     var temp = parseInt(this.questions[i].optionList[0].noStar);
        //     var tempRatingObj = {};
        //     var tempRatingArr = [];
        //     for (var j = 0; j < temp; j++) {
        //       tempRatingObj = {
        //         option: j + 1
        //       };
        //       tempRatingArr.push(tempRatingObj);
        //     }
        //     this.questions[i].rating = tempRatingArr;
        //     this.questions[i].optionList[0].sRateAns = 0;
        //   }
        // }


        // this.saveFeedbackCompletion("UP");
        console.log("RESULT===>", this.questions);
      })
      .catch(result => {
        console.log("RESULT===>", result);
      });
  }
  selectOption(mainData, index) {
    if (mainData.questionTypeId != 1) {
      for (var j = 0; j < mainData.optionList.length; j++) {
        mainData.optionList[j].sFlag = false;
      }
      if (mainData.optionList[index].sFlag == true) {
        mainData.optionList[index].sFlag = false;
      } else {
        mainData.optionList[index].sFlag = true;
      }
      // console.log('MainData===>',mainData);
      // console.log('Index',index);
    }
  }

  onRateChange(count, index) {
    var count = count.rating;
    for (let i = 0; i < this.questions.length; i++) {
      if (i == index) {
        console.log(this.questions[i]);
        this.questions[i].sRateAns = count;
      }
    }
  }

  submitFeedback(type, action) {
    const mandatFlag = this.checkForMandatoryQues();
    if (mandatFlag) {
    // var employeeId = localStorage.getItem("employeeId");
    // var userDetails = JSON.parse(localStorage.getItem("userDetails"));
    // var feedId = this.summary.feedbackId;
    // var courseId = this.summary.courseId;
    // var moduleId = this.summary.moduleId;
    var quesId = [];
    var value = [];
    for (var i = 0; i < this.questions.length; i++) {
      if (this.questions[i].optionList) {
        quesId.push(this.questions[i].questionId);
        for (var j = 0; j < this.questions[i].optionList.length; j++) {
          if (
            this.questions[i].questionTypeId === 2 ||
            this.questions[i].questionTypeId === 3
          ) {
            if (this.questions[i].optionList[j].sFlag == true) {
              value.push(this.questions[i].optionList[j].option);
            } else {
            }
          } else if (this.questions[i].optionList[j].sAns) {
            value.push(this.questions[i].optionList[j].sAns);
          } else {
            value.push(this.questions[i].sRateAns);
          }
        }
      }
      // if (this.questions[i].rating) {
      //   quesId.push(this.questions[i].questionId);
      //   for (var j = 0; j < this.questions[i].rating.length; j++) {
      //     if (this.questions[i].rating[j].sRateAns != 0) {
      //       value.push(this.questions[i].rating[j].sRateAns);
      //     }
      //   }
      // }

    }
    var valueStr = value.join("|");
    var quesIdStr = quesId.join("|");
    var param = {
      actId: this.trainerbatch.activityId,
      empId: this.participantemp.empId,
      fId: this.trainerbatch.feedbackId,
      qId: quesIdStr,
      val: valueStr,
      valLen: value.length,
      tId: this.userDetails.tenantId,
      uId: this.userDetails.id,
      cId: this.trainerbatch.courseId,
      mId: this.trainerbatch.moduleId,
      isPublish: action,
      compId: this.participantemp.compId,
    };
    console.log("QUESTIONANSWER===>", param);
    var url = ENUM.domain + ENUM.url.submitTAevalutionfb;
    this.TAServiceProvider
      .get(url, param)
      .then((result: any) => {
        console.log("RESULT===>", result);
        if (result.type == true) {
          if(type == 1){
            this.Toastr.success('Feedback Submitted Successfully', 'Success!');
          }
          if (type == 2 && action == 0) {
            this.Toastr.success('Feedback question saved successfully', 'Success!');
          }
          if (type == 2 && action == 1) {
            this.Toastr.success('Feedback published successfully', 'Success!');
          }
          // this.saveFeedbackCompletion("Y");
          this.giveFeedback = false;
          this.getparticipants();
        } else {
          this.Toastr.warning("Please resubmit ", "Error Occured!");
        }
      })
      .catch(result => {
        console.log("RESULT===>", result);
        this.Toastr.warning("Please resubmit ", "Error Occured!");
      });
    }
else {
      const mandatString =
        "Question(s) " + this.mandatoryQuesString + " are mandatory";
      this.Toastr.warning(mandatString, "Cannot submit feedback");
    }
  }
  //////////////////check mendatory question/////////////
  checkForMandatoryQues() {
    this.mandatoryQuesString = null;
    console.log('MandatoryQues===>', this.questions);
    for (let i = 0; i < this.questions.length; i++) {
      let count = 0;
      if (this.questions[i].isMandatory === 1) {
        let temp = this.questions[i];
        for (let j = 0; j < temp.optionList.length; j++) {
          if (temp.questionTypeId === 2 || temp.questionTypeId === 3) {
            if (!temp.optionList[j].sFlag) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else if (temp.questionTypeId === 1) {
            if (temp.optionList[j].sAns === '') {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else {
            if (!temp.sRateAns) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          }
        }
      }
    }
    if (this.mandatoryQuesString) {
      return false;
    } else {
      return true;
    }
    // console.log('UnansweredQues===>', this.mandatoryQuesString);
  }
  makeFeedbackQuestionReady(question): SafeHtml {
    if(question.isMandatory == 1){
      let questionFinal = question.fName + ' <b class="mandatoryQues">*</b>';
      return this.sanitizer.bypassSecurityTrustHtml(questionFinal);
    }else {
      return this.sanitizer.bypassSecurityTrustHtml(question.fName);
    }
  }
}
