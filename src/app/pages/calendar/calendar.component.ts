import {
  Component,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  ViewChild,
  ViewEncapsulation,
  TemplateRef,
  OnInit
} from "@angular/core";
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from "date-fns";
import { MatTabChangeEvent } from "@angular/material";
import { Subject, from } from "rxjs";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute } from "@angular/router";

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from "angular-calendar";
import { CalendarServiceProvider } from "../../service/calendar-service";
import { ENUM } from "../../service/enum";
import { NgxSpinnerService } from "ngx-spinner";
import { DatePipe } from "@angular/common";
const colors: any = {
  red: {
    primary: "#2be0f3",
    secondary: "#f9ccad"
  },
  blue: {
    primary: "#bcdd11",
    secondary: "#bae7f7"
  },
  yellow: {
    primary: "#ff82ab",
    secondary: "#deacfd"
  }
};
@Component({
  selector: "ngx-calendar",
  templateUrl: "./calendar.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./calendar.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class CalendarComponent implements OnInit {
  @ViewChild("modalContent") modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;
  openevent: any;
  CalendarView = CalendarView;
  eventlist: any = [];
  viewDate: Date = new Date();
  eventsArray = [];
  modalData: {
    action: string;
    event: CalendarEvent;
  };

  // actions: CalendarEventAction[] = [
  //   {
  //     label: '<i class="fas fa-edit"></i>',
  //     onClick: ({ event }: { event: CalendarEvent }): void => {
  //       this.handleEvent('Edited', event);
  //     }
  //   },
  //   {
  //     label: '<i class="fa fa-fw fa-times"></i>',
  //     onClick: ({ event }: { event: CalendarEvent }): void => {
  //       this.events = this.events.filter(iEvent => iEvent !== event);
  //       this.handleEvent('Deleted', event);
  //     }
  //   }
  // ];

  dateObj = new Date();
  month = this.dateObj.getMonth();
  year = this.dateObj.getFullYear();
  date = this.dateObj.getDate();
  storeDateFormat = this.date + "" + this.month + "" + this.year;

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  myTasks: any = [
    // 	{
    // 		id: 1,
    // 		taskName: "Task 1",
    // 		activityName: "Activity Name",
    // 		dayToComplete: "Today",
    // 		completedStatus: true
    // 	},
    // 	{
    // 		id: 2,
    // 		taskName: "Task 2",
    // 		activityName: "Activity Name",
    // 		dayToComplete: "Tomorrow",
    // 		completedStatus: true
    // 	},
    // 	{
    // 		id: 3,
    // 		taskName: "Task 3",
    // 		activityName: "Activity Name",
    // 		dayToComplete: "Today",
    // 		completedStatus: false
    // 	},
    // 	{
    // 		id: 4,
    // 		taskName: "Task 4",
    // 		activityName: "Activity Name",
    // 		dayToComplete: "Tomorrow",
    // 		completedStatus: true
    // 	},
    // 	{
    // 		id: 5,
    // 		taskName: "Task 5",
    // 		activityName: "Activity Name",
    // 		dayToComplete: "Tomorrow",
    // 		completedStatus: false
    // 	},
    // 	{
    // 		id: 6,
    // 		taskName: "Task 6",
    // 		activityName: "Activity Name",
    // 		dayToComplete: "Today",
    // 		completedStatus: false
    // 	},
  ];
  trainerId: any;
  activeDayIsOpen: boolean = true;
  userdetail: any;
  employeeId: any;
  eventsDummy: any = [];
  constructor(
    private modal: NgbModal,
    private CalSP: CalendarServiceProvider,
    public cdf: ChangeDetectorRef,
    private Spinner: NgxSpinnerService,
    private datePipe: DatePipe, private routes: ActivatedRoute,
    private Toastr: ToastrService,
    private router: Router
  ) {
    this.employeeId = localStorage.getItem("employeeId");
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    console.log(date);
    console.log(this.events,"list of events")
    var startDate1 = new Date(
      date.getFullYear(),
      date.getMonth(),
      1
    );
    var endDate1 = new Date(
      date.getFullYear(),
      date.getMonth() + 1,
      0
    );
    if (new Date(date) >= startDate1 &&
      new Date(date) <= endDate1
    ) {
      this.viewDate = date;
      this.closeOpenMonthViewDay();
    }
    this.eventlist = events;
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        // this.activeDayIsOpen = false;;
        this.openevent = true;
      } else {
        this.openevent = false;
        // this.activeDayIsOpen = true;
      }
      this.cdf.detectChanges();
    }
  }

  showAddEventbtn: boolean = false;

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    console.log("tabChangeEvent => ", tabChangeEvent);
    console.log("index => ", tabChangeEvent.index);
    if (tabChangeEvent.index == 0) {
      this.showAddEventbtn = true;
    }
  };

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: "lg" });
  }
  sample(event){
    console.log(event,"bjvhcghcfgxfg")
  }


  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    // this.activeDayIsOpen = false;
    this.eventlist = [];
    console.log(this.viewDate);
    if (this.viewDate) {
      this.listofevent(this.events, this.viewDate);
    }
  }


  ngOnInit() {
    this.showAddEventbtn = true;
    this.Spinner.show();
    this.employeeId = localStorage.getItem("employeeId");
    if (localStorage.getItem("userdetail")) {
      this.userdetail = JSON.parse(localStorage.getItem("userdetail"));
      console.log(this.userdetail);
    }
    if (this.userdetail.roleId == 7) {
      if (localStorage.getItem("userOtherDetails")) {
        let userOtherDetails = JSON.parse(
          localStorage.getItem("userOtherDetails")
        );
        console.log("userOtherDetails", userOtherDetails);
        let userOthersDetails = userOtherDetails[1];

        for (let i = 0; i < userOthersDetails.length; i++) {
          if (userOthersDetails[i].roleId == 7) {
            this.trainerId = userOthersDetails[i].fieldmasterId;
          }
        }
        console.log("trainerId", this.trainerId);
      }
    }
    var param = {
      uId: this.userdetail.id,
      roleId: this.userdetail.roleId,
      tId: this.userdetail.tenantId
    };
    var url = ENUM.domain + ENUM.url.getCalendarEvents;
    this.CalSP.getEvents(url, param)
      .then((result: any) => {
        console.log(result);
        var eventArr = result.data;
        for (var i = 0; i < eventArr.length; i++) {
          eventArr[i].start = new Date(eventArr[i].eventDate);
          eventArr[i].title = eventArr[i].eventDesc;
        }
        this.events = eventArr;
        this.Spinner.hide();
        var todaydate = new Date();
        // this.eventlist = eventArr;
        this.listofevent(this.events, todaydate);
        // var nDate = (new Date()).getDate();
        // var nMonth = (new Date()).getMonth();
        // var nYear = (new Date()).getFullYear();
        // for(var i=0;i<this.events.length;i++){
        // 	var sDate = (this.events[i].start).getDate();
        // 	var sMonth = (this.events[i].start).getMonth();
        // 	var sYear = (this.events[i].start).getFullYear();
        // 	if(nDate == sDate && nMonth == sMonth && nYear == sYear){
        // 		this.eventsArray.push(this.events[i]);
        // 	}
        // }
        // console.log('RESULT===>', this.eventsArray);
        this.cdf.detectChanges();
      })
      .catch(result => {
        console.log("ServerResponseError :", result);
        this.Spinner.hide();
      });
  }
  getCalendarEvents(item){
    this.events = [];
    var param = {
      uId: this.userdetail.id,
      roleId: this.userdetail.roleId,
      tId: this.userdetail.tenantId
    };
    var url = ENUM.domain + ENUM.url.getCalendarEvents;
    this.CalSP.getEvents(url, param)
      .then((result: any) => {
        console.log(result);
        var eventArr = result.data;
        for (var i = 0; i < eventArr.length; i++) {
          eventArr[i].start = new Date(eventArr[i].eventDate);
          eventArr[i].title = eventArr[i].eventDesc;
        }
        this.events = eventArr;
        this.eventsDummy = eventArr
        this.Spinner.hide();
        var todaydate = new Date();
        ///NEW CHANGE
        var event1:any = []
        for (let i = 0; i < this.events.length; i++) {
          if (this.events[i]['instanceId'] == item.instanceId) {
            event1.push(this.events[i])
          }
        }
        var param = {
          date: item.start,
          events: event1
        }
        console.log("hhhvh", param)
        this.dayClicked(param)
        // END OF NEW CHANGE



        // this.eventlist = eventArr;
        // this.dayClicked()
        // this.listofevent(this.events, todaydate);  //UNCOMMENT IF REQUIRED
        // var nDate = (new Date()).getDate();
        // var nMonth = (new Date()).getMonth();
        // var nYear = (new Date()).getFullYear();
        // for(var i=0;i<this.events.length;i++){
        // 	var sDate = (this.events[i].start).getDate();
        // 	var sMonth = (this.events[i].start).getMonth();
        // 	var sYear = (this.events[i].start).getFullYear();
        // 	if(nDate == sDate && nMonth == sMonth && nYear == sYear){
        // 		this.eventsArray.push(this.events[i]);
        // 	}
        // }
        // console.log('RESULT===>', this.eventsArray);
        this.cdf.detectChanges();
      })
      .catch(result => {
        console.log("ServerResponseError :", result);
        this.Spinner.hide();
      });
  }

  listofevent(eventsfordisaply, todaydate) {
    this.eventlist = []
    //console.log(eventsfordisaply,"event for display");
    var currentdate = new Date(todaydate);
    var startDate = new Date(
      currentdate.getFullYear(),
      currentdate.getMonth(),
      1
    );
    var endDate = new Date(
      currentdate.getFullYear(),
      currentdate.getMonth() + 1,
      0
    );
    for (let i = 0; i < eventsfordisaply.length; i++) {
      if (
        // this.formdate(eventsfordisaply[i].eventDate) ==
        // this.formdate(currentdate)
        new Date(eventsfordisaply[i].eventDate) >= startDate &&
        new Date(eventsfordisaply[i].eventDate) <= endDate
      ) {
        // this.eventlist = eventsfordisaply;
        this.eventlist.push(eventsfordisaply[i]);
        // console.log(this.eventlist,"final list")
      }
      this.cdf.detectChanges();
    }
  }
  formdate(date) {
    if (date) {
      var formatted = this.datePipe.transform(date, "dd-MMM-yyyy");
      return formatted;
    }
  }
  enrolToCourseTA(event, i) {
    console.log(event);
    if (event.actionType === "enrol") {
      this.enrolaproval(event);
    }
    if (event.actionType === "book" || event.actionType === "confirm batch") {
      this.CalSP.data = event;
      this.router.navigate(["../book-slot"], { relativeTo: this.routes });
    }
    if (event.actionType === "confirm") {
      this.confirmwebinar(event, i);
    } if (event.actionType === "unenrol") {
      this.unenroltrainer(event);
    }
  }
  enrolaproval(event) {
    this.Spinner.show();
    console.log("event", event);
    var data = {
      maId: 10,
      caId: event.areaId,
      cId: event.courseId,
      iId: event.instanceId,
      uTId: this.trainerId,
      tId: this.userdetail.tenantId,
      roleId: 7,
      userId: event.userId,
      reqNomId:event.reqForNomId,
    };
    console.log("data", data);
    var url = ENUM.domain + ENUM.url.enrolTrainerToBatch;
    this.CalSP.enroltrainer(url, data).then((result: any) => {
      console.log(result);
      this.Spinner.hide();
      if ((result.type = true)) {
        event.actionType = null;
        this.Toastr.success(result.data[0].msg).onHidden.subscribe(()=>
        this.Toastr.clear());
        this.getCalendarEvents(event)
        var events = this.events
        console.log(this.eventsDummy,"ecfcffffcc")

        // this.listofevent(this.events, new Date());
        // this.dayClicked({date,events}:{date:Date;events: CalendarEvent[] }})


      }
    }, err => {
      this.Spinner.hide();
      // this.Toastr.error('Please check the server', 'Error');
      this.Toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
      this.Toastr.clear());


      console.log(err);
    });
  }
  unenroltrainer(event) {
    this.Spinner.show();
    var event1 :any = []
    console.log("event", event);
    var data = {
      // maId: 10,
      // caId: event.areaId,
      // cId : event.courseId,
      iId: event.instanceId,
      trainId: this.trainerId,
      tenantId: this.userdetail.tenantId,
      roleId: 7,
      // userId: event.userId,
      reqNomId:event.reqForNomId,
    };
    console.log("data", data);
    var url = ENUM.domain + ENUM.url.unerolTrainerBatch;
    this.CalSP.enroltrainer(url, data).then((result: any) => {
      console.log(result);
      this.Spinner.hide();
      if ((result.type = true)) {
        if (result.data[0].isallow == 0) {
          this.Toastr.warning(result.data[0].msg).onHidden.subscribe(()=>
          this.Toastr.clear());
        } else {
          this.Toastr.success(result.data[0].msg).onHidden.subscribe(()=>
          this.Toastr.clear());
          this.getCalendarEvents(event)
          console.log(this.eventsDummy,"eventsDummy")
          // this.listofevent(this.events, new Date());
          // this.dayClicked(param)
        }
      }
    }, err => {
      this.Spinner.hide();
      // this.Toastr.error('Please check the server', 'Error');
      this.Toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
      this.Toastr.clear());
      console.log(err);
    });
  }
  confirmwebinar(event, i) {
    this.Spinner.show();
    const data = {
      eventId: event.eventId,
      empId: this.employeeId,
      wfId: event.instanceId,
      userId: this.userdetail.id,
      eventStartDate: this.formdate(event.eventDate),
      tId: this.userdetail.tenantId,
    };
    const url = ENUM.domain + ENUM.url.tttconfirmwebinar;
    this.CalSP.getdata(url, data).then(res => {
      console.log(res);
      this.Spinner.hide();
      try {
        if (res['type'] == true) {
          try {
            this.eventlist[i].actionType = "confirmed";
            // event.actionType = "confirmed";
            this.cdf.detectChanges();
          } catch (e) {
            console.log(e);
          }
        }
      } catch (e) {
        console.log(e);
      }
    }, err => {
      this.Spinner.hide();
      this.Toastr.warning('Unable to confirm slot at this time', 'Warning!').onHidden.subscribe(()=>
      this.Toastr.clear());
      // this.Toastr.error('Please check the server', 'Error');
      console.log(err);
    });
  }
}
