import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicLinkLearnComponent } from './dynamic-link-learn.component';

describe('DynamicLinkLearnComponent', () => {
  let component: DynamicLinkLearnComponent;
  let fixture: ComponentFixture<DynamicLinkLearnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicLinkLearnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicLinkLearnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
