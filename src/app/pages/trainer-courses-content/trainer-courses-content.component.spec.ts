import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerCoursesContentComponent } from './trainer-courses-content.component';

describe('TrainerCoursesContentComponent', () => {
  let component: TrainerCoursesContentComponent;
  let fixture: ComponentFixture<TrainerCoursesContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerCoursesContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerCoursesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
