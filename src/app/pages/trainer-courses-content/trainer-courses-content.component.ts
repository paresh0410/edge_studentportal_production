import { Component, OnInit, ViewEncapsulation, ViewChild } from "@angular/core";
import { ENUM } from "../../service/enum";
import { TrainerAutomationServiceProvider } from "../../service/trainer-automation.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CourseServiceProvider } from "../../service/course-service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: "trainer-course-content",
  templateUrl: "./trainer-courses-content.component.html",
  styleUrls: ["./trainer-courses-content.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class TrainerCoursesContentComponent implements OnInit {
  contentDataArray: any = [];

  contentArray: any = [];

  trainerCourseId: any;
  trainerCourseName: any;
  trainerCourseDescription: any;
  batchCourseId: any;
  batchCourseName: any;
  batchCourseDescription: any;
  userDetails: any;
  batchdata: any = [];
  ActiveTab: any;
  tenantId: any;

  constructor(
    private TAServiceProvider: TrainerAutomationServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    private CSP: CourseServiceProvider,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService,
  ) {
    if (this.TAServiceProvider.lastActiveTabTrainer) {
      this.ActiveTab = this.TAServiceProvider.lastActiveTabTrainerIndex;
      this.contentDataArray = this.TAServiceProvider.lastActiveTabTrainerContentData;
    }

    this.batchdata = this.TAServiceProvider.batchData;

    console.log("Batch Data ===>", this.batchdata);
    // if (this.TAServiceProvider.batchData) {
    // 	this.trainerCourseId = this.TAServiceProvider.batchData.trainerCourseId;
    // 	this.trainerCourseName = this.TAServiceProvider.batchData.trainerCourseName;
    // 	this.trainerCourseDescription = this.TAServiceProvider.batchData.trainerCourseDescription;
    // 	this.batchCourseId = this.TAServiceProvider.batchData.batchCourseId;
    // 	this.batchCourseName = this.TAServiceProvider.batchData.batchCourseName;
    // 	this.batchCourseDescription = this.TAServiceProvider.batchData.batchCourseDescription;
    // 	console.log("Trainer Details Data =====>" ,this.TAServiceProvider.batchData);
    // }

    this.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    console.log("this.userDetails", this.userDetails);
    this.tenantId = this.userDetails.tenantId;
    this.getCourseModuls();
  }

  ngOnInit() {}

  tabChanged(data) {
    console.log("DATA--->", data);
    this.ActiveTab = data.id;
    this.contentDataArray = this.contentArray[data.id].list;
    this.TAServiceProvider.lastActiveTabTrainer = true;
    this.TAServiceProvider.lastActiveTabTrainerIndex = data.id;
    this.TAServiceProvider.lastActiveTabTrainerContentData = this.contentArray[
      data.id
    ].list;
  }

  getCourseModuls() {
    this.spinner.show();
    let url = ENUM.domain + ENUM.url.getCourseModules;

    let param = {
      cid: this.batchdata.courseId,
      tId: this.tenantId,
    };
    this.TAServiceProvider.getCourseModules(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log("RESULT Course modules Success===>", result);
        this.contentArray = result.data;
        console.log("this.contentArray", this.contentArray);
        if (!this.TAServiceProvider.lastActiveTabTrainer) {
          if (this.contentArray && this.contentArray.length !== 0) {
            this.ActiveTab = this.contentArray[0].id;
            this.contentDataArray = this.contentArray[0].list;
          }
        }
      })
      .catch((result) => {
        this.spinner.hide();
        console.log("RESULT Course modules Error===>", result);
      });
  }

  goToCourse(data) {
    this.TAServiceProvider.dataFromAttFeedAss = false;
    if (data.activityTypeId == 5 || data.activityTypeId == 6) {
      if (this.userDetails.roleId != 8) {
        this.toastr.info("this feature is not available.", "Info");
        return null;
      }
    }
    console.log("activityData:", data);
    var courseDetailSummary = {
      courseTitle: this.batchdata ? this.batchdata.batchName : "",
      courseFrom: 1,
    };
    this.CSP.setCourse(courseDetailSummary, data);
    this.router.navigate(["course"], { relativeTo: this.routes });
  }
  bindBackgroundImage(detail) {
    // detail.img = 'assets/images/open-book-leaf-2.jpg';
    // return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};
    if (detail.activity_type === "Quiz") {
      detail.img = "assets/images/activity_image/quiz.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.activity_type === "Feedback") {
      detail.img = "assets/images/activity_image/feedback.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "video" ||
      detail.mimeType === "video/mp4"
    ) {
      detail.img = "assets/images/activity_image/video.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "audio" ||
      detail.mimeType === "audio/mpeg"
    ) {
      detail.img = "assets/images/activity_image/audio.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "application" &&
      detail.mimeType === "application/zip"
    ) {
      detail.img = "assets/images/activity_image/scrom.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "application" &&
      detail.mimeType === "application/pdf"
    ) {
      detail.img = "assets/images/activity_image/pdf.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "kpoint" ||
      detail.mimeType === "embedded/kpoint"
    ) {
      detail.img = "assets/images/activity_image/video.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.referenceType === "image") {
      detail.img = "assets/images/activity_image/image.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "application" &&
      detail.mimeType === "application/x-msdownload"
    ) {
      detail.img = "assets/images/activity_image/url.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.formatId == 9) {
      detail.img = "assets/images/activity_image/practice_file.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else {
      detail.img = "assets/images/open-book-leaf-2.jpg";
      return {
        "background-image":
          "linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(" +
          detail.img +
          ")",
      };
    }
  }
}
