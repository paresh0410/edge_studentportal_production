import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { WelcomePageService } from '../../service/welcome-page.service';
import { BrandDetailsService } from '../../service/brand-details.service';
@Component({
	selector: 'ngx-welcome-page',
	templateUrl: './welcome-page.component.html',
	styleUrls: ['./welcome-page.component.scss']
})
export class WelcomePageComponent implements OnInit {

  currentBrandData: any;
  theme: any;
  constructor(private router: Router, private routes: ActivatedRoute,
    public brandService: BrandDetailsService,
    private welcomeService: WelcomePageService) {}

	ngOnInit() {
    this.currentBrandData = this.brandService.getCurrentBrandData();
		let id = document.getElementById('nb-global-spinner');
		id.style.display = 'none';
		let rName = null;
		if (localStorage.getItem('roleName')) {
			rName = localStorage.getItem('roleName');
		}
		if (rName === 'Learner' ) {
			setTimeout(() => {
				this.router.navigate(['../pages/dashboard'], { relativeTo: this.routes });
			}, 500);
		} else if (rName === 'Trainer') {
			setTimeout(() => {
				this.router.navigate(['../pages/trainer-dashboard'], { relativeTo: this.routes });
			}, 500);
		} else if (rName === 'Partner') {
			setTimeout(() => {
				this.router.navigate(['../pages/partner'], { relativeTo: this.routes });
			}, 500)
		} else if (rName === 'Manager') {
			setTimeout(() => {
				this.router.navigate(['../pages/team-dashboard'], { relativeTo: this.routes });
			}, 500)
		}
		else if (rName === 'Pre-onboard') {
			setTimeout(() => {
				this.router.navigate(['../pages/learn'], { relativeTo: this.routes });
			}, 500)
		}
	}

	themebind() {
		const themejson = localStorage.getItem('theme');
		if (themejson) this.theme = JSON.parse(themejson);
		if (this.theme && this.theme.logo) {
		  return this.theme.logo;
		} else {
		  return this.currentBrandData.dashboard_logo;
		}
	  }
	  errorHandler(event) {
		console.debug(event);
		event.target.src = this.currentBrandData.dashboard_logo;
	 }
}
