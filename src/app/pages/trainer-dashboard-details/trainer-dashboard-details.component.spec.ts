import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerDashboardDetailsComponent } from './trainer-dashboard-details.component';

describe('TrainerDashboardDetailsComponent', () => {
  let component: TrainerDashboardDetailsComponent;
  let fixture: ComponentFixture<TrainerDashboardDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerDashboardDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerDashboardDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
