import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-trainer-dashboard-details',
  templateUrl: './trainer-dashboard-details.component.html',
  styleUrls: ['./trainer-dashboard-details.component.scss']
})
export class TrainerDashboardDetailsComponent implements OnInit {

  trainerData: any = [
    {
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },
    {
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      id: 1,
      progName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },
  ];

  constructor(private router: Router, private routes: ActivatedRoute) { }

  ngOnInit() {
  }

  gotoBatchDetail(){
    this.router.navigate(['batch-details'], { relativeTo: this.routes });      
  }

}
