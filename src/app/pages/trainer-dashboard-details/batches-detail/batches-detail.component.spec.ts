import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchesDetailComponent } from './batches-detail.component';

describe('BatchesDetailComponent', () => {
  let component: BatchesDetailComponent;
  let fixture: ComponentFixture<BatchesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
