import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-batches-detail',
  templateUrl: './batches-detail.component.html',
  styleUrls: ['./batches-detail.component.scss']
})
export class BatchesDetailComponent implements OnInit {
  batchData: any = [
    {
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },
    {
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },{
      empId: 1,
      employeeName: "Program 1",
      batchcode: 2345,
      objFeedback: 7,
      avgContent: 6,
      avgTrainer: 3,
      avgOverall: 9,
      avgAssess: 8
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
