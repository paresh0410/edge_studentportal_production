import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { CourseDetailServiceProvider } from '../../service/course-detail-service';
import { ENUM } from '../../service/enum';
import { LogServices, LogEnum, AreaEnum } from '../../service/logservice';
import { CourseServiceProvider } from '../../service/course-service';
import { ToastrService } from 'ngx-toastr';
import { FeedbackServiceProvider } from '../../service/feedback-service';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'ngx-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class FeedbackComponent implements OnInit {
  courseDataSummary: any;
  courseDataContent: any;
  courseTitle: any;
  activityData: any;
  userdetail: any;
  summary: any;
  courseName: string;
  formIsValid: boolean = false;
  mandatoryQuesString: string = null;
  makeSubmitDisabled: boolean = false;
  questions = [];
  seperators: any;
  constructor(
    private CDSP: CourseDetailServiceProvider,
    private FSP: FeedbackServiceProvider,
    private Toastr: ToastrService,
    private router: Router,
    private routes: ActivatedRoute,
    private logservice: LogServices,
    public logenum: LogEnum,
    public areaenum: AreaEnum,
    public CSP: CourseServiceProvider,
    private sanitizer: DomSanitizer,
    private spinner: NgxSpinnerService,
    private cdf: ChangeDetectorRef,
  ) {
    this.courseDataContent = CSP.getDataContent();
    this.courseDataSummary = CSP.getDataSummary();
    if (localStorage.getItem('userdetail')) {
      this.userdetail = JSON.parse(localStorage.getItem('userdetail'));
    }
    this.courseTitle = this.courseDataSummary.courseTitle;
    //*****************************GET QUIZ CONTENT*************************************///
    let params = {
      suptId: this.courseDataContent.supertypeId,
      subtId: this.courseDataContent.activityTypeId,
      aId: this.courseDataContent.activityId,
      uId: this.userdetail.userId,
      eId: this.courseDataContent.enrolId,
      tempId: this.courseDataSummary.courseFrom == 2 || 3 ? true : false,
      enId: this.courseDataContent.enrolId,
    };
    if(this.courseDataSummary.courseFrom == 2 || 3) {
      params['eId'] = null;
      if (Number(this.userdetail['roleId']) === 8) {
        params['tempId'] = true;
      }else {
        params['tempId'] = false;
      }
    }
    var url = ENUM.domain + ENUM.url.getModuleActivity;
    this.CSP.getCourseData(url, params)
      .then((result: any) => {
        this.activityData = result.data;
        if(params['tempId']){
          this.activityData['enrolId'] =  this.courseDataContent.enrolId;
        }
        console.log('Activity Data ===>', this.activityData);
      })
      .catch(result => {
        console.log('ServerResponseError :', result);
      });
  }

  ngOnInit() {
    this.summary = this.CDSP.getFeedbackSummary();
    console.log('Feedback===>', this.summary);
    this.courseName = this.summary.courseName;
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    var param = {
      fId: this.summary.feedbackId,
      tId: userDetails.tenantId,
    };
    this.spinner.show();
    var url = ENUM.domain + ENUM.url.getFeedbackQuestions;
    this.FSP.getFeedback(url, param)
      .then((result: any) => {
        if (result){
          if (result['data'] && result['data'].length !== 0){
            this.questions = result['data'];
            for (var i = 0; i < this.questions.length; i++) {
              this.questions[i].qId = i + 1;
              if ( this.questions[i].questionTypeId == 4){
                this.questions[i]['sRateAns'] = 0;
              }
              if (
                this.questions[i].questionTypeId == 2 ||
                this.questions[i].questionTypeId == 3
              ) {
                for (var j = 0; j < this.questions[i].optionList.length; j++) {
                  this.questions[i].optionList[j].sFlag = false;
                }
              } else if (this.questions[i].questionTypeId == 1) {
                this.questions[i].questionPlaceholder =
                  'Minimum ' +
                  this.questions[i].optionList[0].minLength +
                  ' and maximum ' +
                  this.questions[i].optionList[0].maxLength +
                  ' characters';
                this.questions[i].minLen = parseInt(
                  this.questions[i].optionList[0].minLength
                );
                this.questions[i].maxLen = parseInt(
                  this.questions[i].optionList[0].maxLength
                );
                this.questions[i].optionList[0].sAns = '';
              } else {
                var temp = parseInt(this.questions[i].optionList[0].noStar);
                var tempRatingObj = {};
                var tempRatingArr = [];
                for (var j = 0; j < temp; j++) {
                  tempRatingObj = {
                    option: j + 1
                  };
                  tempRatingArr.push(tempRatingObj);
                }
                this.questions[i].rating = tempRatingArr;
                this.questions[i].optionList[0].sRateAns = 0;
              }
            }
            this.seperators = result['separetors'];
            this.spinner.hide();
            this.make_activity_comp_data_data(null);
            // this.saveFeedbackCompletion('UP');
            console.log('RESULT===>', this.questions);
            // this.Toastr.warning('Something went wrong', 'Warning');
            // window.history.back();
          }else {
            this.Toastr.warning('Feedback does not have any question', 'Warning').onHidden.subscribe(()=>
            this.Toastr.clear());
            window.history.back();
          }
        }else {
          this.Toastr.warning('Something went wrong', 'Warning').onHidden.subscribe(()=>
          this.Toastr.clear());
          window.history.back();
        }

      })
      .catch(result => {
        console.log('RESULT===>', result);
        this.Toastr.warning('Something went wrong', 'Warning').onHidden.subscribe(()=>
        this.Toastr.clear());
        window.history.back();
      });
  }

  make_activity_comp_data_data(status) {
      const activity = this.summary;
      // const activity = this.playerService.getActivity();
      if (activity.completionstatus === "Y") {
        activity.completionstatus = "Y";
      } else if (status === "Y") {
        activity.completionstatus = "Y";
      } else {
        activity.completionstatus = "UP";
      }

      // if (this.saveActCompWorking === false) {
      // 	this.saveActivityCompletionData(activity);
      // }

      this.saveFeedbackCompletion(activity.completionstatus);
    }

  selectOption(mainData, index) {
    if (mainData.questionTypeId != 1) {
      for (var j = 0; j < mainData.optionList.length; j++) {
        mainData.optionList[j].sFlag = false;
      }
      if (mainData.optionList[index].sFlag == true) {
        mainData.optionList[index].sFlag = false;
      } else {
        mainData.optionList[index].sFlag = true;
      }
      // console.log('MainData===>',mainData);
      // console.log('Index',index);
    }
  }

  selectOption1(mainData, index) {
    for (var j = 0; j < mainData.rating.length; j++) {
      mainData.rating[j].sFlag = false;
      mainData.rating[j].sRateAns = 0;
    }
    for (var i = 0; i <= index; i++) {
      mainData.rating[i].sFlag = true;
    }
    mainData.rating[index].sRateAns = index + 1;
    console.log('MainData===>', mainData);
    console.log('Index', index);
  }

  submitFeedback1() {

    this.makeSubmitDisabled = true;
    this.cdf.detectChanges();
    this.spinner.show();
    const mandatFlag = this.checkForMandatoryQues();
    if (mandatFlag) {
      var employeeId = localStorage.getItem('employeeId');
      var userDetails = JSON.parse(localStorage.getItem('userDetails'));
      // var feedId = this.summary.feedbackId;
      // var courseId = this.summary.courseId;
      // var moduleId = this.summary.moduleId;
      let feedId = 0;
      let courseId = 0;
      let moduleId = 0;
      let activityId = 0;
      let enrolId = 0;
      let actiCmpId = 0;
      if(this.summary){
        feedId = this.summary.feedbackId;
        courseId = this.summary.courseId;
        moduleId = this.summary.moduleId;
        activityId = this.summary.activityId;
        enrolId = this.summary.enrolId;
        actiCmpId = this.summary.activitycompletionid;
      } else {
        feedId = this.activityData.feedbackId ;
        courseId = this.activityData.courseId;
        moduleId = this.activityData.moduleId ;
        activityId = this.activityData.activityId;
        enrolId = this.activityData.enrolId;
        actiCmpId = this.activityData.activitycompletionid;
      }
      var quesId = [];
      var value = [];
      for (var i = 0; i < this.questions.length; i++) {
        if (this.questions[i].optionList) {
          quesId.push(this.questions[i].fqid);
          for (var j = 0; j < this.questions[i].optionList.length; j++) {
            if (this.questions[i].questionTypeId === 2 || this.questions[i].questionTypeId === 3) {
              if (this.questions[i].optionList[j].sFlag == true) {
                value.push(this.questions[i].optionList[j].option);
              } else { }
            } else if (this.questions[i].optionList[j].sAns) {
              value.push(this.questions[i].optionList[j].sAns);
            } else {
              // this is remove because we didn't get result if it wrong uncoment below code
              // and comment current code chnages by Vivek
              //	value.push(this.questions[i].optionList[j].sRateAns);
              value.push(this.questions[i].sRateAns);
            }
          }
        }
        // if (this.questions[i].rating) {
        //   quesId.push(this.questions[i].questionId);
        //   for (var j = 0; j < this.questions[i].rating.length; j++) {
        //     if (this.questions[i].rating[j].sRateAns != 0) {
        //       value.push(this.questions[i].rating[j].sRateAns);
        //     }
        //   }
        // }
      }
      // var valueStr = value.join('|');
      const valueStr = value.join(this.seperators['pipeValue']);


      // var quesIdStr = quesId.join('|');
      const quesIdStr = quesId.join(this.seperators['pipeValue']);


      // var param = {
      //   empId: employeeId,
      //   fId: feedId,
      //   qId: quesIdStr,
      //   val: valueStr,
      //   valLen: value.length,
      //   tId: userDetails.tenantId,
      //   uId: userDetails.id,
      //   cId: courseId,
      //   mId: moduleId,
      //   aId: this.activityData.activityId,
      //   enId: this.activityData.enrolId,
      //   actCompId: this.activityData.activitycompletionid ? this.activityData.activitycompletionid : 0
      // };
      // console.log('QUESTIONANSWER===>', param);
      // var url = ENUM.domain + ENUM.url.submitFeedbackAnswers;
      // this.FSP.submitFeedback(url, param)
      //   .then((result: any) => {
      //     console.log('RESULT===>', result);
      //     if (result.type == true) {
      //       this.spinner.hide();
      //       this.Toastr.success('Feedback Submitted Successfully', 'Success!');

      //       // this.saveFeedbackCompletion('Y');
      //       // this.make_activity_comp_data_data('Y');
      //       // this.router.navigate(['../../course-detail'], {
      //       // 	relativeTo: this.routes
      //       // });
      //       window.history.back();

      //     } else {
      //       this.spinner.hide();
      //       this.Toastr.error('Please resubmit Feedback', 'Error Occured!');
      //       this.makeSubmitDisabled = false;
      //       this.cdf.detectChanges();
      //     }
      //   })
      //   .catch(result => {
      //     this.spinner.hide();
      //     console.log('RESULT===>', result);
      //     this.Toastr.error('Please resubmit Feedback', 'Error Occured!');
      //     this.makeSubmitDisabled = false;
      //     this.cdf.detectChanges();
      //   });
      if (feedId !== 0 && courseId !== 0 && moduleId !== 0 && activityId !== 0 && enrolId !== 0) {
        var param = {
          empId: employeeId,
          fId: feedId,
          qId: quesIdStr,
          val: valueStr,
          valLen: value.length,
          tId: userDetails.tenantId,
          uId: userDetails.id,
          cId: courseId,
          mId: moduleId,
          aId: activityId,
          enId: enrolId,
          actCompId: actiCmpId,
          platform: 3,
        };
        console.log("QUESTIONANSWER===>", param);
        var url = ENUM.domain + ENUM.url.submitFeedbackAnswers;
        this.FSP.submitFeedback(url, param)
          .then((result: any) => {
            console.log("RESULT===>", result);
            if (result.type == true) {
              this.spinner.hide();
              this.Toastr.success("Feedback Submitted Successfully", "Success!").onHidden.subscribe(()=>
              this.Toastr.clear());

              // this.saveFeedbackCompletion('Y');
              // this.make_activity_comp_data_data('Y');
              // this.router.navigate(['../../course-detail'], {
              // 	relativeTo: this.routes
              // });
              window.history.back();
            } else {
              this.spinner.hide();
              // this.Toastr.error("Please resubmit Feedback", "Error Occured!");
              this.Toastr.warning ('Warning','Unable to submit your feedback at this time, please try again').onHidden.subscribe(()=>
              this.Toastr.clear());
              this.makeSubmitDisabled = false;
              this.cdf.detectChanges();
            }
          })
          .catch((result) => {
            this.spinner.hide();
            console.log("RESULT===>", result);
            // this.Toastr.error("Please resubmit Feedback", "Error Occured!");
            this.Toastr.warning ('Warning','Unable to submit your feedback at this time, please try again').onHidden.subscribe(()=>
            this.Toastr.clear());
            this.makeSubmitDisabled = false;
            this.cdf.detectChanges();
          });
        } else {
          this.spinner.hide();
          this.makeSubmitDisabled = false;
          this.cdf.detectChanges();
          this.Toastr.warning ('Warning','Unable to submit your feedback at this time, please try again').onHidden.subscribe(()=>
          this.Toastr.clear());
        }
    } else {
      this.spinner.hide();
      const mandatString = 'Question(s) ' + this.mandatoryQuesString + ' are mandatory';
      this.Toastr.warning(mandatString, 'Cannot submit feedback').onHidden.subscribe(()=>
      this.Toastr.clear());
      this.makeSubmitDisabled = false;
      this.cdf.detectChanges();
    }
  }

  saveFeedbackCompletion(feedBackStatus) {
    const activity = this.activityData;
    activity.completionstatus = feedBackStatus;
    this.CSP.saveActivityCompletion(activity).then(
      (res: any) => {
        if (res.type === true) {
          // this.quizCompRes = res;
          if (
            this.activityData.activitycompletionid === undefined ||
            !this.activityData.activitycompletionid
          ) {
            this.activityData.activitycompletionid = res.data[0][0].lastid;
          }
          console.log('Feedback completion res', res);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  onRateChange(count, index) {
    var count = count.rating;
    for (let i = 0; i < this.questions.length; i++) {
      if (i == index) {
        console.log(this.questions[i]);
        this.questions[i].sRateAns = count;
      }
    }
  }

  checkForMandatoryQues() {
    this.mandatoryQuesString = null;
    console.log('MandatoryQues===>', this.questions);
    for (let i = 0; i < this.questions.length; i++) {
      let count = 0;
      if (this.questions[i].isMandatory === 1) {
        let temp = this.questions[i];
        for (let j = 0; j < temp.optionList.length; j++) {
          if (temp.questionTypeId === 2 || temp.questionTypeId === 3) {
            if (!temp.optionList[j].sFlag) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else if (temp.questionTypeId === 1) {
            if (temp.optionList[j].sAns === '') {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else {
            if (temp.sRateAns === 0) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          }
        }
      }
    }
    if (this.mandatoryQuesString) {
      return false;
    } else {
      return true;
    }
    // console.log('UnansweredQues===>', this.mandatoryQuesString);
  }

  submitFeedback() {
    this.checkvalid();
    if (this.formIsValid) {
      this.resultFeedback();
    }
  }
  checkvalid() {
    let ansSubmitted = [];
    for (let i = 0; i < this.questions.length; i++) {
      const currentQuestion = this.questions[i];
      if (currentQuestion.questionTypeId === 1) {
        this.checkIfValidInputText(i);
        if (currentQuestion.isValid) {
          ansSubmitted.push(currentQuestion);
        }
      } else if (currentQuestion.questionTypeId === 2 || currentQuestion.questionTypeId === 3) {
        this.checkIfValidInputSelect(i);
        if (currentQuestion.isValid) {
          ansSubmitted.push(currentQuestion);
        }
      } else if (currentQuestion.questionTypeId === 4) {
        this.checkIfValidInputStar(i);
        if (currentQuestion.isValid) {
          ansSubmitted.push(currentQuestion);
        }
      }
    }
    if (ansSubmitted.length === this.questions.length) {
      this.formIsValid = true;
      // return true;
    } else {
      this.formIsValid = false;
      // return false;
    }
  }

  checkIfValidInputText(currentIndex) {
    let currentQuestion = this.questions[currentIndex];
    if (currentQuestion.optionList[0].sAns.length < currentQuestion.minLen) {
      currentQuestion['isValid'] = false;
      // currentQuestion['msg'] = 'Answer must at least' + ' ' + currentQuestion['minLen'] + ' ' + 'character.';
      currentQuestion['msg'] = 'Above question is mandatory.';
    } else {
      if (currentQuestion.isMandatory === 1) {
        if (!currentQuestion.optionList[0].sAns) {
          currentQuestion['isValid'] = false;
          currentQuestion['msg'] = 'Above question is mandatory';
        } else {
          currentQuestion['isValid'] = true;
        }
      } else {
        currentQuestion['isValid'] = true;
      }
    }
  }
  checkIfValidInputSelect(currentIndex) {
    let currentQuestion = this.questions[currentIndex];
    const selectedAnd = [];
    for (let j = 0; j < currentQuestion.optionList.length; j++) {
      if (currentQuestion.optionList[j].sFlag) {
        selectedAnd.push(currentQuestion.optionList[j]);
      }
    }
    if (currentQuestion.isMandatory === 1) {
      if (selectedAnd.length > 0) {
        currentQuestion['isValid'] = true;
      } else {
        currentQuestion['isValid'] = false;
        currentQuestion['msg'] = 'Above question is mandatory';
      }
    } else {
      currentQuestion['isValid'] = true;
    }
  }
  checkIfValidInputStar(currentIndex) {
    let currentQuestion = this.questions[currentIndex];
    // if (!currentQuestion.sRateAns) {
    //   currentQuestion['isValid'] = false;
    // } else {
    if (currentQuestion.isMandatory === 1) {
      if (!currentQuestion.sRateAns) {
        currentQuestion['isValid'] = false;
        currentQuestion['msg'] = 'Above question is mandatory';
      } else {
        currentQuestion['isValid'] = true;
      }
    } else {
    }
    currentQuestion['isValid'] = true;
  }

  resultFeedback() {
    var employeeId = localStorage.getItem('employeeId');
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    var feedId = this.summary.feedbackId;
    var courseId = this.summary.courseId;
    var moduleId = this.summary.moduleId;
    var quesId = [];
    var value = [];
    for (var i = 0; i < this.questions.length; i++) {
      if (this.questions[i].optionList) {
        quesId.push(this.questions[i].fqid);
        for (var j = 0; j < this.questions[i].optionList.length; j++) {
          if (this.questions[i].questionTypeId === 2 || this.questions[i].questionTypeId === 3) {
            if (this.questions[i].optionList[j].sFlag == true) {
              value.push(this.questions[i].optionList[j].option);
            } else { }
          } else if (this.questions[i].optionList[j].sAns) {
            value.push(this.questions[i].optionList[j].sAns);
          } else {
            value.push(this.questions[i].sRateAns);
          }
        }
      }
    }
    var valueStr = value.join('|');
    var quesIdStr = quesId.join('|');
    var param = {
      empId: employeeId,
      fId: feedId,
      qId: quesIdStr,
      val: valueStr,
      valLen: value.length,
      tId: userDetails.tenantId,
      uId: userDetails.id,
      cId: courseId,
      mId: moduleId,
      enId: this.activityData.enrolId,
    };
    console.log('QUESTIONANSWER===>', param);
    var url = ENUM.domain + ENUM.url.submitFeedbackAnswers;
    this.FSP.submitFeedback(url, param)
      .then((result: any) => {
        console.log('RESULT===>', result);
        if (result.type == true) {
          this.Toastr.success('Feedback Submitted Successfully', 'Success!').onHidden.subscribe(()=>
          this.Toastr.clear());
          // this.saveFeedbackCompletion('Y');
          // this.make_activity_comp_data_data('Y');
          window.history.back();
        } else {
          // this.Toastr.error('Please resubmit Feedback', 'Error Occured!');
          this.Toastr.warning('Please resubmit Feedback', 'Error Occured!').onHidden.subscribe(()=>
          this.Toastr.clear());
        }
      })
      .catch(result => {
        console.log('RESULT===>', result);
          // this.Toastr.error('Please resubmit Feedback', 'Error Occured!');
        this.Toastr.warning('Please resubmit Feedback', 'Error Occured!').onHidden.subscribe(()=>
        this.Toastr.clear());
      });
  }

  makeFeedbackQuestionReady(question): SafeHtml {
    if(question.isMandatory == 1){
      let questionFinal = question.fName + ' <b class="mandatoryQues">*</b>';
      return this.sanitizer.bypassSecurityTrustHtml(questionFinal);
    }else {
      return this.sanitizer.bypassSecurityTrustHtml(question.fName);
    }
  }
  goBack(){
    window.history.back();
  }
}
