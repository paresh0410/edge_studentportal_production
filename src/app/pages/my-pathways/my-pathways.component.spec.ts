import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPathwaysComponent } from './my-pathways.component';

describe('MyPathwaysComponent', () => {
  let component: MyPathwaysComponent;
  let fixture: ComponentFixture<MyPathwaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPathwaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPathwaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
