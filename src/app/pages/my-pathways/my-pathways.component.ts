import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-my-pathways',
  templateUrl: './my-pathways.component.html',
  styleUrls: ['./my-pathways.component.scss']
})
export class MyPathwaysComponent implements OnInit {

  createdPathway: number = 9;
  otherPathway: number = 12;

  constructor() { }

  ngOnInit() {
  }

}
