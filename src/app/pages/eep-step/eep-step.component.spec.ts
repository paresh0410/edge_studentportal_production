import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepStepComponent } from './eep-step.component';

describe('EepStepComponent', () => {
  let component: EepStepComponent;
  let fixture: ComponentFixture<EepStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
