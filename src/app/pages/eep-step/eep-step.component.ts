import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ENUM } from "../../service/enum";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from "@angular/common";
import { EEPService } from "../../service/eep.service";
@Component({
  selector: 'ngx-eep-step',
  templateUrl: './eep-step.component.html',
  styleUrls: ['./eep-step.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false }
    }]
})
export class EepStepComponent implements OnInit {
  workflowStep: any = [];
  batchdetail: any = [];
  stepsData: any = [];
  userDetails: any = [];
  tenantId: any;
  employeeId: any;
  selectedIndex = 0;
  selectedstep = 1;
  userId: any;
  wfresonseId: any;
  workflowdetail: any = [];
  temp: any = [];
  isLinear = false;
  disabledTab = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  show: boolean = false;
  isExpired: boolean = false;
  displayName: any;
  isFutureDate: boolean = false;
  startDate: any;
  endDate: any;
  constructor(private eepServices: EEPService,
    private router: Router, private _formBuilder: FormBuilder,
    private routes: ActivatedRoute, private datePipe: DatePipe,
    private spinner: NgxSpinnerService) {
    this.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    this.userId = this.userDetails.id;
    this.tenantId = this.userDetails.tenantId;
    this.employeeId = localStorage.getItem("employeeId");
    console.log("employeeId", this.employeeId);
    console.log("this.userDetails", this.userDetails);

    this.workflowdetail = this.eepServices.workflow;
    // this.getAllSteps();
  }

  ngOnInit() {
    this.getAllSteps();
  }

  goBack() {
    window.history.back();
  }

  getAllSteps() {
    this.spinner.show();
    const param = {
      wfId: this.workflowdetail.id,
      tId: this.tenantId,
      empId: this.employeeId
    };
    const url = ENUM.domain + ENUM.url.eep_workflowstep;

    this.eepServices.get(url, param)
      .then((result: any) => {
        this.spinner.hide();
        this.stepsData = result.data;
        console.log(this.stepsData[0]);
        // for(const data of this.stepsData){
        for (let i = 0; i < this.stepsData.length; i++) {
          if (this.stepsData[i].status != 1) {
            this.selectedstep = this.stepsData[i].stepId;
            this.temp = this.stepsData[i];
            console.log(this.temp);
            this.stetdetail(i,this.temp);
            // this.temp.emit(this.stepsData[ this.selectedstep]);
            break;
          }
          if(this.stepsData.length == (i+1))
          {
            var index =0 ;
            this.stetdetail(this.stepsData[index],this.stepsData[index]);
          }

          // this.displayName = this.temp.displayName
          // this.startDate = this.temp.startDate

          // if( this.temp.status == 0){
          //   this.show = true
          //   console.log(this.show)
          // }
          // else if(this.temp.isOpen == 1 &&  this.temp.isClose == 1){
          //   this.isExpired = true
          //   this.show = false
          //   console.log(this.show)

          // console.log(this.temp);
          // }
          // else if(this.temp.isOpen == 1 &&  this.temp.isClose == 0){
          //   this.show = true
          //   console.log(this.show)

          // }
      
          // else if(this.temp.isOpen == 0 &&  this.temp.isClose == 0){
          // this.show = false
          // this.isFutureDate = true
          // console.log(this.show)

          // }
          // else{
          //   this.show = true
          //   console.log(this.show)

      
          // }          
          console.log(this.selectedstep);
        }
        console.log("TTT nomination", this.stepsData);

      })
      .catch(result => {
        this.spinner.hide();
        console.log("RESULT tttNomination Error===>", result);
      });
  }

  stetdetail(stepIndex, step) {
    this.show = false
    this.isExpired = false
    this.isFutureDate = false
    console.log(stepIndex, step);
    this.selectedstep = step.stepId;
    this.temp = step;
    this.displayName = step.displayName
    this.startDate = step.startDate
    this.endDate = step.endDate
    if(step.status == 1 || step.status == 0){
      this.show = true
      console.log(this.show)

    }
    else if(step.isOpen == 1 &&  step.isClose == 1){
      this.isExpired = true
      this.show = false
    console.log(this.temp);
    }
    else if(step.isOpen == 1 &&  step.isClose == 0){
      this.show = true
      console.log(this.show)

    }

    else if(step.isOpen == 0 &&  step.isClose == 0){
    this.show = false
    this.isFutureDate = true
    console.log(this.show)

    }
    else{

    }
    

  }

  componentDataPassedd(event) {
    console.log("pass data : ", event);
    this.selectTab(event.selectedIndex);
    this.getAllSteps();
    this.disabledTab = event.disabled;
  }
  componentDataPass(event) {
    this.stetdetail('', event);
    this.getAllSteps();
  }
  selectTab(index: number): void {
    this.selectedIndex = index;
  }
  formdate(date) {
    if (date) {
      var formatted = this.datePipe.transform(date, "dd-MMM-yyyy");
      return formatted;
    }
  }
}
