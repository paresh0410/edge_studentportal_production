import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SurveyServiceProvider } from '../../service/survey-service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SurveyComponent implements OnInit {
  surveyTitle: any;
  surveySummary: any;
  count: any;
  mandatoryQuesString: string = null;
  formIsValid: boolean = false;
  questions = [];
  constructor(
    private SSP: SurveyServiceProvider,
    private Toastr: ToastrService,
    private router: Router,
    private routes: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.surveySummary = this.SSP.getPollSummary();
    console.log('SURVEYSUMMARY===>', this.surveySummary);
    this.surveyTitle = this.surveySummary.sName;
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    var param = {
      sId: this.surveySummary.sid,
      tId: userDetails.tenantId,
    };
    var url = ENUM.domain + ENUM.url.getSurveyQuestionAnswers;
    this.SSP.getSurvey(url, param)
      .then((result: any) => {
        console.log('RESULT===>', result);
        this.questions = result.data;
        for (var i = 0; i < this.questions.length; i++) {
          this.questions[i].qId = i + 1;
          if (
            this.questions[i].questionTypeId == 2 ||
            this.questions[i].questionTypeId == 3
          ) {
            for (var j = 0; j < this.questions[i].optionList.length; j++) {
              this.questions[i].optionList[j].sFlag = false;
            }
          } else if (this.questions[i].questionTypeId == 1) {
            this.questions[i].questionPlaceholder =
              'Minimum ' +
              this.questions[i].optionList[0].minLength +
              ' and maximum ' +
              this.questions[i].optionList[0].maxLength +
              ' characters';
            this.questions[i].minLen = parseInt(
              this.questions[i].optionList[0].minLength
            );
            this.questions[i].maxLen = parseInt(
              this.questions[i].optionList[0].maxLength
            );
            this.questions[i].optionList[0].sAns = '';
          } else {
            var temp = parseInt(this.questions[i].optionList[0].noStar);
            this.questions[i].rating = temp;
            this.questions[i].optionList[0].sRateAns = 0;
          }
        }
        console.log('RESULT===>', this.questions);
      })
      .catch(result => {
        console.log('RESULT===>', result);
      });
  }

  submitPoll() { }
  selectOption(mainData, index) {
    if (mainData.questionTypeId != 1) {
      for (var j = 0; j < mainData.optionList.length; j++) {
        mainData.optionList[j].sFlag = false;
      }
      if (mainData.optionList[index].sFlag == true) {
        mainData.optionList[index].sFlag = false;
      } else {
        mainData.optionList[index].sFlag = true;
      }
      // console.log('MainData===>',mainData);
      // console.log('Index',index);
    }
  }

  selectOption1(mainData, index) {
    for (var j = 0; j < mainData.rating.length; j++) {
      mainData.rating[j].sFlag = false;
      mainData.rating[j].sRateAns = 0;
    }
    for (var i = 0; i <= index; i++) {
      mainData.rating[i].sFlag = true;
    }
    mainData.rating[index].sRateAns = index + 1;
    console.log('MainData===>', mainData);
    console.log('Index', index);
  }

  submitSurvey1() {
    const mandatFlag = this.checkForMandatoryQues();
    if (mandatFlag) {
      var employeeId = localStorage.getItem('employeeId');
      var userDetails = JSON.parse(localStorage.getItem('userDetails'));
      var surveyId = this.surveySummary.sid;
      var quesId = [];
      var value = [];
      for (var i = 0; i < this.questions.length; i++) {
        if (this.questions[i].optionList) {
          quesId.push(this.questions[i].questionId);
          for (var j = 0; j < this.questions[i].optionList.length; j++) {
            if (this.questions[i].questionTypeId === 2 || this.questions[i].questionTypeId === 3) {
              if (this.questions[i].optionList[j].sFlag == true) {
                value.push(this.questions[i].optionList[j].option);
              } else { }
            } else if (this.questions[i].optionList[j].sAns) {
              value.push(this.questions[i].optionList[j].sAns);
            } else {
              value.push(this.questions[i].optionList[j].sRateAns);
            }
          }
        }
        // if (this.questions[i].rating) {
        //   quesId.push(this.questions[i].questionId);
        //   for (var j = 0; j < this.questions[i].rating.length; j++) {
        //     if (this.questions[i].rating[j].sRateAns != 0) {
        //       value.push(this.questions[i].rating[j].sRateAns);
        //     }
        //   }
        // }
      }
      var valueStr = value.join('|');
      var quesIdStr = quesId.join('|');
      if (this.surveySummary.recordUsers == 2) {
        var param = {
          empId: employeeId,
          sId: surveyId,
          qId: quesIdStr,
          val: valueStr,
          valLen: value.length,
          tId: userDetails.tenantId,
          uId: userDetails.id
        };
        console.log('QUESTIONANSWER===>', param);
        var url = ENUM.domain + ENUM.url.submitSurveyAnswers;
        this.SSP.submitSurvey(url, param)
          .then((result: any) => {
            console.log('RESULT===>', result);
            if (result.type == true) {
              this.Toastr.success('Survey Submitted Successfully', 'Success!').onHidden.subscribe(()=>
              this.Toastr.clear());
              this.router.navigate(['../../dashboard'], {
                relativeTo: this.routes
              });
            } else {
              this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
              this.Toastr.clear());
            }
          })
          .catch(result => {
            console.log('RESULT===>', result);
            this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
            this.Toastr.clear());
          });
      } else {
        var params = {
          sId: surveyId,
          qId: quesIdStr,
          val: valueStr,
          valLen: value.length,
          tId: userDetails.tenantId
        };
        console.log('QUESTIONANSWER===>', params);
        var url = ENUM.domain + ENUM.url.submitSurveyAnswers;
        this.SSP.submitSurvey(url, params)
          .then((result: any) => {
            console.log('Survey RESULT===>', result);
            if (result.type == true) {
              this.Toastr.success('Survey Submitted Successfully', 'Success!').onHidden.subscribe(()=>
              this.Toastr.clear());
              this.router.navigate(['/pages/dashboard']);
            } else {
              this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
              this.Toastr.clear());
            }
          })
          .catch(result => {
            console.log('RESULT===>', result);
            this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
            this.Toastr.clear());
          });
      }
    } else {
      const mandatString = 'Question(s) ' + this.mandatoryQuesString + ' are mandatory';
      this.Toastr.warning(mandatString, 'Cannot submit survey').onHidden.subscribe(()=>
      this.Toastr.clear());
    }
  }

  onRateChange(count, index) {
    this.count = count.rating;
    for (let i = 0; i < this.questions.length; i++) {
      if (i == index) {
        console.log(this.questions[i]);
        this.questions[i].sRateAns = this.count;
      }
    }
  }

  checkForMandatoryQues() {
    this.mandatoryQuesString = null;
    console.log('MandatoryQues===>', this.questions);
    for (let i = 0; i < this.questions.length; i++) {
      let count = 0;
      if (this.questions[i].isMandatory === 1) {
        let temp = this.questions[i];
        for (let j = 0; j < temp.optionList.length; j++) {
          if (temp.questionTypeId === 2 || temp.questionTypeId === 3) {
            if (!temp.optionList[j].sFlag) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else if (temp.questionTypeId === 1) {
            if (temp.optionList[j].sAns === '') {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else {
            if (temp.optionList[j].sRateAns === 0) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          }
        }
      }
    }
    if (this.mandatoryQuesString) {
      return false;
    } else {
      return true;
    }
    // console.log('UnansweredQues===>', this.mandatoryQuesString);
  }

  back() {
    window.history.back();
  }
  submitSurvey() {
    this.checkvalid();
    if (this.formIsValid) {
      this.resultsurvey();
    }
  }
  checkvalid() {
    let ansSubmitted = [];
    for (let i = 0; i < this.questions.length; i++) {
      const currentQuestion = this.questions[i];
      if (currentQuestion.questionTypeId === 1) {
        this.checkIfValidInputText(i);
        if (currentQuestion.isValid) {
          ansSubmitted.push(currentQuestion);
        }
      } else if (currentQuestion.questionTypeId === 2 || currentQuestion.questionTypeId === 3) {
        this.checkIfValidInputSelect(i);
        if (currentQuestion.isValid) {
          ansSubmitted.push(currentQuestion);
        }
      } else if (currentQuestion.questionTypeId === 4) {
        this.checkIfValidInputStar(i);
        if (currentQuestion.isValid) {
          ansSubmitted.push(currentQuestion);
        }
      }
    }
    if (ansSubmitted.length === this.questions.length) {
      this.formIsValid = true;
      // return true;
    } else {
      this.formIsValid = false;
      // return false;
    }
  }

  checkIfValidInputText(currentIndex) {
    let currentQuestion = this.questions[currentIndex];
    if (currentQuestion.optionList[0].sAns.length < currentQuestion.minLen) {
      currentQuestion['isValid'] = false;
      currentQuestion['msg'] = 'Answer must at least' + ' ' + currentQuestion['minLen'] + ' ' + 'character.';
    } else {
      if (currentQuestion.isMandatory === 1) {
        if (!currentQuestion.optionList[0].sAns) {
          currentQuestion['isValid'] = false;
          currentQuestion['msg'] = 'Above question is mandatory';
        } else {
          currentQuestion['isValid'] = true;
        }
      } else {
        currentQuestion['isValid'] = true;
      }
    }
  }
  checkIfValidInputSelect(currentIndex) {
    let currentQuestion = this.questions[currentIndex];
    const selectedAnd = [];
    for (let j = 0; j < currentQuestion.optionList.length; j++) {
      if (currentQuestion.optionList[j].sFlag) {
        selectedAnd.push(currentQuestion.optionList[j]);
      }
    }
    if (currentQuestion.isMandatory === 1) {
      if (selectedAnd.length > 0) {
        currentQuestion['isValid'] = true;
      } else {
        currentQuestion['isValid'] = false;
        currentQuestion['msg'] = 'Above question is mandatory';
      }
    } else {
      currentQuestion['isValid'] = true;
    }
  }
  checkIfValidInputStar(currentIndex) {
    let currentQuestion = this.questions[currentIndex];
    // if (!currentQuestion.sRateAns) {
    //   currentQuestion['isValid'] = false;
    // } else {
    if (currentQuestion.isMandatory === 1) {
      if (!currentQuestion.sRateAns) {
        currentQuestion['isValid'] = false;
        currentQuestion['msg'] = 'Above question is mandatory';
      } else {
        currentQuestion['isValid'] = true;
      }
    } else {
      currentQuestion['isValid'] = true;
    }
    // currentQuestion['isValid'] = true;
  }

  resultsurvey() {
    var employeeId = localStorage.getItem('employeeId');
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    var surveyId = this.surveySummary.sid;
    var quesId = [];
    var value = [];
    for (var i = 0; i < this.questions.length; i++) {
      if (this.questions[i].optionList) {
        quesId.push(this.questions[i].questionId);
        for (var j = 0; j < this.questions[i].optionList.length; j++) {
          if (this.questions[i].questionTypeId === 2 || this.questions[i].questionTypeId === 3) {
            if (this.questions[i].optionList[j].sFlag == true) {
              value.push(this.questions[i].optionList[j].option);
            } else { }
          } else if (this.questions[i].optionList[j].sAns) {
            value.push(this.questions[i].optionList[j].sAns);
          } else {
            value.push(this.questions[i].sRateAns);
          }
        }
      }
    }
    var valueStr = value.join('|');
    var quesIdStr = quesId.join('|');
    if (this.surveySummary.recordUsers == 2) {
      var param = {
        empId: employeeId,
        sId: surveyId,
        qId: quesIdStr,
        val: valueStr,
        valLen: value.length,
        tId: userDetails.tenantId,
        uId: userDetails.id,
      };
      console.log('QUESTIONANSWER===>', param);
      var url = ENUM.domain + ENUM.url.submitSurveyAnswers;
      this.SSP.submitSurvey(url, param)
        .then((result: any) => {
          console.log('RESULT===>', result);
          if (result.type == true) {
            this.Toastr.success('Survey Submitted Successfully', 'Success!').onHidden.subscribe(()=>
            this.Toastr.clear());
            this.router.navigate(['../../dashboard'], {
              relativeTo: this.routes
            });
          } else {
            this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
            this.Toastr.clear());
          }
        })
        .catch(result => {
          console.log('RESULT===>', result);
          this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
					this.Toastr.clear());
        });
    } else {
      var params = {
        sId: surveyId,
        qId: quesIdStr,
        val: valueStr,
        valLen: value.length,
        tId: userDetails.tenantId
      };
      console.log('QUESTIONANSWER===>', params);
      var url = ENUM.domain + ENUM.url.submitSurveyAnswers;
      this.SSP.submitSurvey(url, params)
        .then((result: any) => {
          console.log('Survey RESULT===>', result);
          if (result.type == true) {
            this.Toastr.success('Survey Submitted Successfully', 'Success!').onHidden.subscribe(()=>
            this.Toastr.clear());
            this.router.navigate(['/pages/dashboard']);
          } else {
            this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
            this.Toastr.clear());
          }
        })
        .catch(result => {
          console.log('RESULT===>', result);
          this.Toastr.warning('Please resubmit survey', 'Error Occured!').onHidden.subscribe(()=>
					this.Toastr.clear());
        });
    }
  }
}
