import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { TrainTheTrainerServiceProvider } from "../../../service/traine-the-trainer.service";
import { ENUM } from "../../../service/enum";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { TrainerAutomationServiceProvider } from "../../../service/trainer-automation.service";
@Component({
  selector: "course-content",
  templateUrl: "./course-content.component.html",
  styleUrls: ["./course-content.component.scss"]
})
export class CourseContentComponent implements OnInit {
  contentDataArray: any = [];
  contentArray: any = [];
  ActiveTab: any;
  disabledGoToCourse = false;

  trainingModule: any = [
    {
      moduleid: 1,
      moduleName: "Module 1",
      list: [
        {
          activityId: 7,
          activityName: "Activity 1",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 2,
          activityName: "Activity 2",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 3,
          activityName: "Activity 3",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 6,
          activityName: "Activity 4",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 1,
          activityName: "Activity 5",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        }
      ]
    },
    {
      moduleid: 2,
      moduleName: "Module 2",
      list: [
        {
          activityId: 7,
          activityName: "Activity 1",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 2,
          activityName: "Activity 2",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 3,
          activityName: "Activity 3",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 6,
          activityName: "Activity 4",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 1,
          activityName: "Activity 5",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        }
      ]
    },
    {
      moduleid: 3,
      moduleName: "Module 3",
      list: [
        {
          activityId: 7,
          activityName: "Activity 1",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 2,
          activityName: "Activity 2",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 3,
          activityName: "Activity 3",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 6,
          activityName: "Activity 4",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        },
        {
          activityId: 1,
          activityName: "Activity 5",
          img: "assets/images/activity1.jpg",
          modulename: "Demo Module",
          summary: "This is Demo Module ",
          bookmarked: false
        }
      ]
    }
  ];

  trainerCourseName = "Demo Course 1";
  trainerCourseDescription = "This is a demo course";
  userDetails: any = [];
  courseDetailTabArray: any = [];
  courseDetailSummary: any = [];
  courseDetailArray: any = [];
  trainerworkflow: any = [];
  constructor(
    public ttt: TrainTheTrainerServiceProvider,private spinner: NgxSpinnerService,
    private router: Router,public toastr: ToastrService,
    private routes: ActivatedRoute, private TAServiceProvider: TrainerAutomationServiceProvider,
  ) {
    this.ActiveTab = this.trainingModule[0].moduleid;
    this.contentDataArray = this.trainingModule[0].list;
    this.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    this.trainerworkflow = this.TAServiceProvider.trainerworkflow;
  }

  ngOnInit() {
    this.content();
  }

  tabChanged(data) {
    this.spinner.show();
    console.log("DATA--->", data);
    this.ActiveTab = data.id;
    this.courseDetailArray = this.courseDetailTabArray[data.id].list;
    for (var i = 0; i < this.courseDetailArray.length; i++) {
      var temp: any = i + 1;
      this.courseDetailArray[i].img = "assets/images/open-book-leaf-2.jpg";
      // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
    }
    this.spinner.hide();
  }

  content() {
    const param = {
      wfId: this.trainerworkflow.id,
      tId: this.userDetails.tenantId,
    };
    const url = ENUM.domain + ENUM.url.getcoursecontent;
    this.ttt.get(url, param).then(result => {
      // console.log("nikhil",result);
      var temp: any = result;
        this.courseDetailTabArray = temp.data;
        if (this.courseDetailTabArray.length > 0) {
          for (var i = 0; i < this.courseDetailTabArray.length; i++) {
            var temp: any = i + 1;
            this.courseDetailTabArray[i].id = i;
            // this.courseDetailTabArray[i].moduleName = 'Module' + ' ' + temp;
          }
          this.ActiveTab = this.courseDetailTabArray[0].id;
          this.courseDetailArray = this.courseDetailTabArray[0].list;
          for (var i = 0; i < this.courseDetailArray.length; i++) {
            var temp: any = i + 1;
            this.courseDetailArray[i].img =
              "assets/images/open-book-leaf-2.jpg";
            // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
            this.courseDetailArray[i].activityTempId = temp;
            var tempCourseI = this.courseDetailArray[i];
            var tempDate = new Date(this.courseDetailArray[i].completed);
            for (var j = 0; j < i; j++) {
              var tempCourseJ = this.courseDetailArray[j];
              if (tempCourseI.dependentActId != 0) {
                if (tempCourseI.dependentActId == tempCourseJ.activityId) {
                  if (tempCourseJ.wCompleted == 0) {
                    tempCourseI.dependant = tempCourseJ.activityName;
                    // break;
                  } else {
                    tempCourseI.dependant = null;
                  }
                } else {
                  tempCourseI.dependant = null;
                }
              } else {
                tempCourseI.dependant = null;
              }
            }
          }
          console.log("CD===>", this.courseDetailArray);
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.toastr.warning("No Data Available", "Warning!").onHidden.subscribe(()=>
          this.toastr.clear());
          // this.gotoLearn();
        }
      })
      .catch(result => {
        console.log("ServerResponseError :", result);
        this.spinner.hide();
      });
    }

  goToCourse(data) {
    console.log("activityData:", data);
    this.ttt.coursecontent = data;
    if (data.activityTypeId === 12) {
      this.router.navigate(['pages/trainer-feedback']);
    }else if (data.activityTypeId === 11) {
      this.ttt.setActivityData(data);
      this.router.navigate(['pages/trainer-webinar']);
    }
  }

  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToCourse(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}
}
