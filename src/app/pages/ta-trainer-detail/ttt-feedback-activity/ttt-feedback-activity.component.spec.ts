import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttFeedbackActivityComponent } from './ttt-feedback-activity.component';

describe('TttFeedbackActivityComponent', () => {
  let component: TttFeedbackActivityComponent;
  let fixture: ComponentFixture<TttFeedbackActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttFeedbackActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttFeedbackActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
