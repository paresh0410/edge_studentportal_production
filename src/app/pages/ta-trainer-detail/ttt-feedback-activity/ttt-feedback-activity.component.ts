import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ENUM } from "../../../service/enum";
import { TrainTheTrainerServiceProvider } from "../../../service/traine-the-trainer.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { TrainerAutomationServiceProvider } from "../../../service/trainer-automation.service";
import { EvaluationComponent } from "../../ttt-nomination/evaluation/evaluation.component";

@Component({
  selector: "ngx-ttt-feedback-activity",
  templateUrl: "./ttt-feedback-activity.component.html",
  styleUrls: ["./ttt-feedback-activity.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TttFeedbackActivityComponent implements OnInit {
  participants: any = [];
  mandatoryQuesString: string = null;
  feedback: any = [];

  giveFeedback: boolean;
  userDetails: any = [];
  questions: any = [];
  coursecontentdata: any = [];
  participantemp: any = [];
  trainerworkflow: any = [];
  fbtn: any;
  userotherdetail: any = [];
  trainerId: any;
  constructor(
    private tttService: TrainTheTrainerServiceProvider, public TAServiceProvider: TrainerAutomationServiceProvider,
    public spinner: NgxSpinnerService, private Toastr: ToastrService,
  ) {
    this.userDetails = JSON.parse(localStorage.getItem("userdetail"));
    this.userotherdetail = JSON.parse(localStorage.getItem('userOtherDetails'));
    let uDetail = this.userotherdetail[1]
    for (let i = 0; i <  uDetail.length; i++) {
      if (uDetail[i].roleId == 7) {
        this.trainerId =  uDetail[i].fieldmasterId;
      }
    }
    this.trainerworkflow = this.TAServiceProvider.trainerworkflow;
    console.log(this.trainerworkflow);
    this.coursecontentdata = this.tttService.coursecontent;
    console.log(this.coursecontentdata);
    if (this.coursecontentdata.stepId === 3) {
      this.fbtn = 'Evaluate';
    } else if (this.coursecontentdata.stepId === 7) {
      this.fbtn = 'Evaluate';
    } else if (this.coursecontentdata.stepId === 8) {
      this.fbtn = 'Evaluate';
    }else{
      this.fbtn = 'Evaluate';
    }
    this.getparticipants();
  }

  ngOnInit() { }

  viewFeedback(element) {
    this.participantemp = element;
    console.log(this.participantemp);
    this.giveFeedback = true;
    this.fbquestion();
  }
  Back() {
    window.history.back();
  }
  goback() {
    console.log("feedback Data >>");
    this.giveFeedback = false;
  }

  onRatingChange(event: any, i) {
    console.log("Event Object", event);

    for (var j = 0; j < this.feedback.length; j++) {
      if (j === i) {
        this.feedback[j].answer = event.rating;
      }
    }
  }

  getparticipants() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.nomineelistbytrainer;
    const param = {
      reqFrom: 'WEB',
      wfId: this.trainerworkflow.id,
      // trainerId: this.userDetails.referenceId,
      trainerId: this.trainerId,
      tId: this.userDetails.tenantId,
      pNo: null,
      fId: this.coursecontentdata.feedbackId,
      cId: this.coursecontentdata.courseId,
      mId: this.coursecontentdata.moduleId,
    };
    console.log('param', param);
    this.tttService
      .get(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log("Participant List under trainer===>", result);
        this.participants = result.data;
      })
      .catch(result => {
        this.spinner.hide();
        console.log("Participants list Error===>", result);
      });
  }
  //////////////////////fb question/////////////////////
  fbquestion() {
    var param = {
      fId: this.coursecontentdata.feedbackId,
      cId: this.coursecontentdata.courseId,
      mId: this.coursecontentdata.moduleId,
      actId: this.coursecontentdata.activityId,
      // fId: this.coursecontentdata.,
      tId: this.userDetails.tenantId,
      empId: this.participantemp.employeeId,
    };
    var url = ENUM.domain + ENUM.url.ttt_trainerfeedback;
    this.tttService
      .get(url, param)
      .then((result: any) => {
        this.questions = result.data;
        for (var i = 0; i < this.questions.length; i++) {
          this.questions[i].qId = i + 1;
          if ( this.questions[i].questionTypeId == 4){
            this.questions[i]['sRateAns'] = 0;
          }
          if (
            this.questions[i].questionTypeId == 2 ||
            this.questions[i].questionTypeId == 3
          ) {
            for (var j = 0; j < this.questions[i].optionList.length; j++) {
              this.questions[i].optionList[j].sFlag = false;
            }
          } else if (this.questions[i].questionTypeId == 1) {
            this.questions[i].questionPlaceholder =
              "Minimum " +
              this.questions[i].optionList[0].minLength +
              " and maximum " +
              this.questions[i].optionList[0].maxLength +
              " characters";
            this.questions[i].minLen = parseInt(
              this.questions[i].optionList[0].minLength
            );
            this.questions[i].maxLen = parseInt(
              this.questions[i].optionList[0].maxLength
            );
            this.questions[i].optionList[0].sAns = "";
          } else {
            var temp = parseInt(this.questions[i].optionList[0].noStar);
            var tempRatingObj = {};
            var tempRatingArr = [];
            for (var j = 0; j < temp; j++) {
              tempRatingObj = {
                option: j + 1
              };
              tempRatingArr.push(tempRatingObj);
            }
            this.questions[i].rating = tempRatingArr;
            this.questions[i].optionList[0].sRateAns = 0;
          }
        }
        // this.saveFeedbackCompletion("UP");
        console.log("RESULT===>", this.questions);
      })
      .catch(result => {
        console.log("RESULT===>", result);
      });
  }

  ///////////////Selection of ans/////////////////
  selectOption(mainData, index) {
    if (mainData.questionTypeId != 1) {
      for (var j = 0; j < mainData.optionList.length; j++) {
        mainData.optionList[j].sFlag = false;
      }
      if (mainData.optionList[index].sFlag == true) {
        mainData.optionList[index].sFlag = false;
      } else {
        mainData.optionList[index].sFlag = true;
      }
      // console.log('MainData===>',mainData);
      // console.log('Index',index);
    }
  }

  onRateChange(count, index) {
    var count = count.rating;
    for (let i = 0; i < this.questions.length; i++) {
      if (i == index) {
        console.log(this.questions[i]);
        this.questions[i].sRateAns = count;
      }
    }
  }

  /////////////submit fb////////////////////
  submitFeedback() {
    const mandatFlag = this.checkForMandatoryQues();
    if (mandatFlag) {
      // var employeeId = localStorage.getItem("employeeId");
      // var userDetails = JSON.parse(localStorage.getItem("userDetails"));
      // var feedId = this.summary.feedbackId;
      // var courseId = this.summary.courseId;
      // var moduleId = this.summary.moduleId;
      var quesId = [];
      var value = [];
      for (var i = 0; i < this.questions.length; i++) {
        if (this.questions[i].optionList) {
          quesId.push(this.questions[i].questionId);
          for (var j = 0; j < this.questions[i].optionList.length; j++) {
            if (
              this.questions[i].questionTypeId === 2 ||
              this.questions[i].questionTypeId === 3
            ) {
              if (this.questions[i].optionList[j].sFlag == true) {
                value.push(this.questions[i].optionList[j].option);
              } else {
              }
            } else if (this.questions[i].optionList[j].sAns) {
              value.push(this.questions[i].optionList[j].sAns);
            } else {
              value.push(this.questions[i].sRateAns);
            }
          }
        }
        // if (this.questions[i].rating) {
        //   quesId.push(this.questions[i].questionId);
        //   for (var j = 0; j < this.questions[i].rating.length; j++) {
        //     if (this.questions[i].rating[j].sRateAns != 0) {
        //       value.push(this.questions[i].rating[j].sRateAns);
        //     }
        //   }
        // }
      }
      var valueStr = value.join("|");
      var quesIdStr = quesId.join("|");
      var param = {
        wfId: this.trainerworkflow.id,
        stepId: this.coursecontentdata.stepId,
        actId: this.coursecontentdata.activityId,
        empId: this.participantemp.employeeId,
        fId: this.coursecontentdata.feedbackId,
        qId: quesIdStr,
        val: valueStr,
        valLen: value.length,
        tId: this.userDetails.tenantId,
        uId: this.userDetails.id,
        cId: this.coursecontentdata.courseId,
        mId: this.coursecontentdata.moduleId,
      };
      console.log("QUESTIONANSWER===>", param);
      var url = ENUM.domain + ENUM.url.trainer_fb_submit;
      this.tttService
        .get(url, param)
        .then((result: any) => {
          console.log("RESULT===>", result);
          if (result.type == true) {
            this.Toastr.success(" Submitted Successfully", "Success!");
            // this.saveFeedbackCompletion("Y");
            this.giveFeedback = false;
            this.getparticipants();
          } else {
            this.Toastr.warning("Please Resubmit  ", "Error Occured!");
          }
        })
        .catch(result => {
          console.log("RESULT===>", result);
          this.Toastr.warning("Please resubmit ", "Error Occured!");
        });
    } else {
      const mandatString =
        "Question(s) " + this.mandatoryQuesString + " are mandatory";
      this.Toastr.warning(mandatString, "Cannot submit ");
    }
  }
  //////////////////check mendatory question/////////////
  // checkForMandatoryQues() {
  //   this.mandatoryQuesString = null;
  //   console.log('MandatoryQues===>', this.questions);
  //   for (let i = 0; i < this.questions.length; i++) {
  //     let count = 0;
  //     if (this.questions[i].isMandatory === 1) {
  //       let temp = this.questions[i];
  //       for (let j = 0; j < temp.optionList.length; j++) {
  //         if (temp.questionTypeId === 2 || temp.questionTypeId === 3) {
  //           if (!temp.optionList[j].sFlag) {
  //             count++;
  //           }
  //           if (count === temp.optionList.length) {
  //             let unId = i + 1;
  //             if (this.mandatoryQuesString) {
  //               this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
  //             } else {
  //               this.mandatoryQuesString = '' + unId + '';
  //             }
  //           }
  //         } else if (temp.questionTypeId === 1) {
  //           if (temp.optionList[j].sAns === '') {
  //             count++;
  //           }
  //           if (count === temp.optionList.length) {
  //             let unId = i + 1;
  //             if (this.mandatoryQuesString) {
  //               this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
  //             } else {
  //               this.mandatoryQuesString = '' + unId + '';
  //             }
  //           }
  //         } else {
  //           if (!temp.sRateAns) {
  //             count++;
  //           }
  //           if (count === temp.optionList.length) {
  //             let unId = i + 1;
  //             if (this.mandatoryQuesString) {
  //               this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
  //             } else {
  //               this.mandatoryQuesString = '' + unId + '';
  //             }
  //           }
  //         }
  //       }
  //     }
  //   }
  //   if (this.mandatoryQuesString) {
  //     return false;
  //   } else {
  //     return true;
  //   }
  //   // console.log('UnansweredQues===>', this.mandatoryQuesString);
  // }
  checkForMandatoryQues() {
    this.mandatoryQuesString = null;
    console.log('MandatoryQues===>', this.questions);
    for (let i = 0; i < this.questions.length; i++) {
      let count = 0;
      if (this.questions[i].isMandatory === 1) {
        let temp = this.questions[i];
        for (let j = 0; j < temp.optionList.length; j++) {
          if (temp.questionTypeId === 2 || temp.questionTypeId === 3) {
            if (!temp.optionList[j].sFlag) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else if (temp.questionTypeId === 1) {
            if (temp.optionList[j].sAns === '') {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          } else {
            if (temp.sRateAns === 0) {
              count++;
            }
            if (count === temp.optionList.length) {
              let unId = i + 1;
              if (this.mandatoryQuesString) {
                this.mandatoryQuesString = this.mandatoryQuesString + ',' + ' ' + unId;
              } else {
                this.mandatoryQuesString = '' + unId + '';
              }
            }
          }
        }
      }
    }
    if (this.mandatoryQuesString) {
      return false;
    } else {
      return true;
    }
    // console.log('UnansweredQues===>', this.mandatoryQuesString);
  }
}
