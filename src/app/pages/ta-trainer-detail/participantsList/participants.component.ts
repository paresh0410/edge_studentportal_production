import { Component, OnInit, ViewEncapsulation, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { TrainTheTrainerServiceProvider } from '../../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENUM } from '../../../service/enum';
import { Router, ActivatedRoute } from '@angular/router';
import { TrainerAutomationServiceProvider } from '../../../service/trainer-automation.service';
@Component({
  selector: 'ttt-participants-list',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ParticipantsListComponent implements OnInit {
  participants: any = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    // 'User Profile',
    // 'Participant Name',
    // 'Ecn No.',
    // 'Evaluation Call',
    // 'Viva',
    // 'Mock 1',
    // 'Mock 2',
    // 'Assessment 1',
    // 'Assessment 2',
  ];
  //  'mobile',
    // 'email',
    // 'designation',
  trainerworkflow: any = [];
  userDetails: any = [];
  participant = new MatTableDataSource(this.participants);
  constructor(
    public ttt: TrainTheTrainerServiceProvider, private TAServiceProvider: TrainerAutomationServiceProvider,
    private routes: ActivatedRoute,
    private router: Router,
    public spinner: NgxSpinnerService,
    public cdf: ChangeDetectorRef,
  ) {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.trainerworkflow = this.TAServiceProvider.trainerworkflow;
  }

  ngOnInit() {
    this.participants.paginator = this.paginator;
    this.particpantlist();
  }
  particpantlist() {
    this.spinner.show();
    this.cdf.detectChanges();
    const data = {
      reqFrom: 'WEB',
      wfId: this.trainerworkflow.id,
      tId: this.userDetails.tenantId,
      pNo: null
    };
    const url = ENUM.domain + ENUM.url.nomineeList;

    this.ttt.nomineelist(url, data).then(
      res => {
        this.spinner.hide();
        this.cdf.detectChanges();
        console.log(res);
        try {
          if (res['type'] === true) {
            try {
              this.participants = res['data'];
              this.displayedColumns = res['header'].split(',');
              console.log('this.participants', this.participants);
              console.log('this.participants', this.displayedColumns);
            } catch { }
          }
          console.log(this.participants);
        } catch { }
      },
      err => {
        this.spinner.hide();
        console.log(err);
      }
    );
  }

  getClassList(element){
    const flag =  element == 'User Profile' || element == 'Participant Name' || element == 'Ecn No.';
    return ({
      'text-left': flag ,
      'text-right': !flag,
    });
  }
}
