import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-ta-trainer-detail',
  templateUrl: './ta-trainer-detail.component.html',
  styleUrls: ['./ta-trainer-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TaTrainerDetailComponent implements OnInit {

  constructor(public router: Router, public routes: ActivatedRoute) { }

  ngOnInit() {
    this.currentTabIndex = 0;
  }
  goBack(){
    // window.history.back();
    this.router.navigate(['../trainer-dashboard'], { relativeTo: this.routes });
  }

  currentTabIndex = null;
  tabChanged(event){
    if(event){
      this.currentTabIndex = event.index;
      console.log('Tab Changed ===>',event);
    }
  }
}
