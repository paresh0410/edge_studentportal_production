import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaTrainerDetailComponent } from './ta-trainer-detail.component';

describe('TaTrainerDetailComponent', () => {
  let component: TaTrainerDetailComponent;
  let fixture: ComponentFixture<TaTrainerDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaTrainerDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaTrainerDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
