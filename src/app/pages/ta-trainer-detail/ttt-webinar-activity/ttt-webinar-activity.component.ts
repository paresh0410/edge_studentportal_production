import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ENUM } from "../../../service/enum";
import { TrainTheTrainerServiceProvider } from "../../../service/traine-the-trainer.service";
import { CourseServiceProvider } from "../../../service/course-service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { Router, ActivatedRoute } from '@angular/router'
import { TrainerAutomationServiceProvider } from "../../../service/trainer-automation.service";
import { WebinarConfig } from '../../../pages/webinar-multiple/webinar-entity';
import { feature } from '../../../../environments/feature-environment';
@Component({
  selector: "ngx-ttt-webinar-activity",
  templateUrl: "./ttt-webinar-activity.component.html",
  styleUrls: ["./ttt-webinar-activity.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TttWebinarActivityComponent implements OnInit {
  activityData: any;
  voiceCall: boolean = true;
  videoCall: boolean = true;
  courseReadyFlag: boolean = false;
  usersData: any;
  userList: any = [];
  trainerworkflow: any = [];
  userDetail: any = {};
  tenantId: number;
  trainerId: any;
  description = '';
  featureConfig;
  config: WebinarConfig = {
    voice: true,
    video: true,
    creator: false,
    areaId: null,
    instanceId: null,
    tenantId: null,
    type: null,
    recording: true,
    allowCalls: true,
    startDate: '',
    EndDate: '',
  };
  constructor(
    private tttService: TrainTheTrainerServiceProvider,
    private CSP: CourseServiceProvider,
    public spinner: NgxSpinnerService,
    public router: Router, public TAServiceProvider: TrainerAutomationServiceProvider,
    private toastr: ToastrService,
    public routes: ActivatedRoute,
  ) {
    this.featureConfig = feature;
   }

  ngOnInit() {
    if (localStorage.getItem('userdetail')) {
      this.userDetail = JSON.parse(localStorage.getItem('userdetail'));
      this.tenantId = this.userDetail.tenantId;
      // this.config.tenantId = this.tenantId;
    }
    this.trainerworkflow = this.TAServiceProvider.trainerworkflow;
    this.activityData = this.tttService.getActivityData();
    console.log("WEBINARDATA===>", this.activityData);
    // this.gettrainersData(this.activityData);
    this.getUsersData(this.activityData);
  }

  getUsersData(data) {
    if (localStorage.getItem('userOtherDetails')) {
      const uD = JSON.parse(localStorage.getItem('userOtherDetails'));
      let uDetail = uD[1]
      for (let i = 0; i < uDetail.length; i++) {
        if (uDetail[i].roleId == 7) {
          this.trainerId = uDetail[i].fieldmasterId;
        }
      }
      const params = {
        wfId: this.trainerworkflow.id,
        traId: this.trainerId,
        tId: this.tenantId,
      };
      const url = ENUM.domain + ENUM.url.getUserForWebinar;
      this.tttService
        .getUsers(url, params)
        .then((result: any) => {
          console.log("DATA--->", result);
          if (result.type == true) {
            if (result.data.length > 0) {
              this.usersData = result['data'][0];
              this.userList = result['data'];
              this.usersData.courseId = this.trainerworkflow.id;          // Hardcoded workflowId is passed in courseId
              this.courseReadyFlag = true;
              this.config = {
                voice: true,
                video: true,
                creator: this.userDetail.roleId === 7 ? true : false,
                areaId: 29,
                instanceId: this.usersData.courseId || 0,
                tenantId: this.tenantId,
                type: null,
                recording: true,
                allowCalls: true,
                startDate: '',
                EndDate: '',
              };
              if (localStorage.getItem('userDetails')) {
                const userDetails = JSON.parse(localStorage.getItem('userDetails'));
                const url = ENUM.domain + ENUM.url.saveWorkflowCompletion;
                const Param = {
                  wfId: this.trainerworkflow.id,
                  empId: this.usersData.empId,
                  sId: 4,
                  aId: this.activityData.activityId,
                  sts: 1,
                  tId: userDetails.tenantId,
                  uId: userDetails.id,
                };
                this.tttService.saveCompletion(url, Param)
                  .then((result: any) => {
                    console.log('Workflow Completion Saved Successfully!!!');
                  }).catch(result => {
                    console.log("ServerResponseError :", result);
                  });
              } else {
                this.toastr.warning(
                  "Please login again",
                  "Error Occured!"
                ).onHidden.subscribe(() =>
                  this.toastr.clear());
              }
            } else {
              this.goBack();
              this.toastr.warning(
                "No user available for this workflow or trainer",
                "Error Occured!"
              ).onHidden.subscribe(() =>
                this.toastr.clear());
            }
          } else {
            this.toastr.warning(
              "Please try again after sometime",
              "Error Occured!"
            ).onHidden.subscribe(() =>
              this.toastr.clear());
          }
        })
        .catch(result => {
          console.log("ServerResponseError :", result);
        });
    } else {
      this.toastr.warning(
        "Please login again",
        "Error Occured!"
      ).onHidden.subscribe(() =>
        this.toastr.clear());
    }
  }

  changeCount(event) {
    console.log('event', event);
    if (event) {
      if (localStorage.getItem('userDetails')) {
        const userDetails = JSON.parse(localStorage.getItem('userDetails'));
        const url = ENUM.domain + ENUM.url.saveWorkflowCompletion;
        const Param = {
          wfId: this.trainerworkflow.id,
          userIds: event.webinarInformation.userids,
          sId: 4,
          aId: this.activityData.activityId,
          sts: 1,
          tId: userDetails.tenantId,
          uId: userDetails.id,
        };
        this.tttService.saveCompletion(url, Param)
          .then((result: any) => {
            console.log('Workflow Completion Saved Successfully!!!');
          }).catch(result => {
            console.log("ServerResponseError :", result);
          });
        this.activityData.completionstatus = 'Y';
        this.CSP.saveActivityCompletion(this.activityData);
      } else {
        this.toastr.warning(
          "Please login again",
          "Error Occured!"
        ).onHidden.subscribe(() =>
          this.toastr.clear());
      }
    }
  }

  goBack() {
    // this.router.navigate(['pages/ttt-trainer-detail']);
    // window.history.back();
    this.router.navigate(['../ttt-trainer-detail'], { relativeTo: this.routes });
  }

}
