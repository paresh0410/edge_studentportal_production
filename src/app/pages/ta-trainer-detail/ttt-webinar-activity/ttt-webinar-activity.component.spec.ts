import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TttWebinarActivityComponent } from './ttt-webinar-activity.component';

describe('TttFeedbackActivityComponent', () => {
  let component: TttWebinarActivityComponent;
  let fixture: ComponentFixture<TttWebinarActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TttWebinarActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TttWebinarActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
