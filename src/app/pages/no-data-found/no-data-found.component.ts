import { NbMenuService } from '@nebular/theme';
import { Component } from '@angular/core';

@Component({
  selector: 'ngx-no-data-found',
  styleUrls: ['./no-data-found.component.scss'],
  templateUrl: './no-data-found.component.html',
})
export class NoDataFoundComponent {

  constructor(private menuService: NbMenuService) {
  }

  goToHome() {
    this.menuService.navigateHome();
  }
}
