import { Component, OnInit, Input, ViewEncapsulation ,  } from '@angular/core';
import { CallCoachingService } from '../../service/call-coaching.service';
import { CourseServiceProvider } from '../../service/course-service';
import { ENUM } from '../../service/enum';
import { CourseDetailServiceProvider } from '../../service/course-detail-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
@Component({
  selector: 'ngx-cc-content',
  templateUrl: './cc-content.component.html',
  styleUrls: ['./cc-content.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CcContentComponent implements OnInit {
  @Input() callcontent: any;
  resetPwdForm: FormGroup;
  password: any;
  employeeId: any;
  userDetails: any;
  ActiveTab: any;
  courseDetailTabArray: any = [];
  courseDetailArray: any = [];
  courseDetailSummary: any = [];
  disabledGoToCourse = false;
  checkResponse: any;
  displayInstructions: boolean = false;
  passWrong: boolean = false;
  passCheck: boolean = false;
  submitted: boolean = false;
  passKey: string;
  activeRole: any;
  constructor(public callservice: CallCoachingService,  public CSP: CourseServiceProvider,
    public CDSP: CourseDetailServiceProvider,  private router: Router,
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private routes: ActivatedRoute) {

    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
    console.log('userDetails', this.userDetails);

    if (localStorage.getItem('roleName')) {
      this.activeRole = localStorage.getItem('roleName');
      console.log('this.activeRole', this.activeRole);
    }
  }

  ngOnInit() {
    this.resetPwdForm = this.formBuilder.group({
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
    this.courseDetailTabArray = this.callcontent;
    console.log('this.callcontent',this.callcontent);
    console.log('this.callcontent11',this.courseDetailTabArray);
    this.moduledetail();
  }
  tabChanged(data) {
    // this.spinner.show();
    console.log("DATA--->", data);
    this.ActiveTab = data.id;
    this.courseDetailArray = this.courseDetailTabArray[data.id].list;
    for (let i = 0; i < this.courseDetailArray.length; i++) {
      var temp: any = i + 1;
      this.courseDetailArray[i].img = "assets/images/open-book-leaf-2.jpg";
      // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
    }
    // this.spinner.hide();
  }
  moduledetail() {
    if (this.courseDetailTabArray.length > 0) {
      for (var i = 0; i < this.courseDetailTabArray.length; i++) {
        var temp: any = i + 1;
        this.courseDetailTabArray[i].id = i;
        // this.nomineeModules[i].moduleName = 'Module' + ' ' + temp;
      }
      this.ActiveTab = this.courseDetailTabArray[0].id;
      this.courseDetailArray = this.courseDetailTabArray[0].list;
      // try {
      //   for (var i = 0; i < this.courseDetailArray.length; i++) {
      //     var temp: any = i + 1;
      //     this.courseDetailArray[i].img =
      //       "assets/images/open-book-leaf-2.jpg";
      //     // this.nomineeContent[i].activityName = 'Activity' + ' ' + temp;
      //     this.courseDetailArray[i].activityTempId = temp;
      //     var tempCourseI = this.courseDetailArray[i];
      //     var tempDate = new Date(this.courseDetailArray[i].completed);
      //     for (var j = 0; j < i; j++) {
      //       var tempCourseJ = this.courseDetailArray[j];
      //       if (tempCourseI.dependentActId != 0) {
      //         if (tempCourseI.dependentActId == tempCourseJ.activityId) {
      //           if (tempCourseJ.wCompleted == 0) {
      //             tempCourseI.dependant = tempCourseJ.activityName;
      //             // break;
      //           } else {
      //             tempCourseI.dependant = null;
      //           }
      //         } else {
      //           tempCourseI.dependant = null;
      //         }
      //       } else {
      //         tempCourseI.dependant = null;
      //       }
      //     }
      //   }
      // } catch (e) {
      //   console.log(e);
      // }
      console.log("CD===>", this.courseDetailArray);
    }
  }
  goToCourse(data){
    this.CSP.setCourse(this.courseDetailSummary, data);
    console.log("DATA---->", data);

    if (data.activityTypeId == 5 || data.activityTypeId == 6){
      if(this.userDetails.roleId != 8){
        this.toastr.info('this feature is not available.' , 'Info');
        return null;
      }
    }
      if (data.activityTypeId == 5) {
        // const userDetails = JSON.parse(localStorage.getItem("userdetails"));
        const employeeId = localStorage.getItem('employeeId');
        var param = {
          qId: data.quizId,
          tId: this.userDetails.tenantId,
          uId: employeeId,
          cId: data.courseId,
          mId: data.moduleId,
          aId: data.activityId,
          enId : data.enrolId,
        };
        var url = ENUM.domain + ENUM.url.checkQuizUser;
        this.CDSP.getCourseDetailsData(url, param)
          .then(result => {
            var temp: any = result;
            if (temp.data !== null || temp.data !== undefined) {
              if (temp.data.length > 0) {
                console.log("CHECk QUIZ FOR USER---->", temp.data[0]);
                this.checkResponse = temp.data[0];
                if (temp.data[0].flag == "true") {
                  console.log("PassCheck===>", temp.data[0]);
                  if (temp.data[0].qPassword == 0) {
                    if (this.checkResponse.instructions) {
                      this.displayInstructions = true;
                    } else {
                      this.displayInstructions = false;
                      const courseDetailSummary = {
                        courseFrom: 3,
                      };
                      this.CSP.setCourse(courseDetailSummary, data);
                      // this.router.navigate(["quiz"], { relativeTo: this.routes });
                      this.router.navigate(['../learn/course-detail/quiz'], { relativeTo: this.routes });
                    }
                  } else {
                    this.passCheck = true;
                    this.passKey = temp.data[0].passWord;
                  }
                } else {
                  this.toastr.warning(temp.data[0].msg, "Warning!");
                }
              } else {
                // this.toastr.error(
                //   "Something went wrong please try again later",
                //   "Server Error!"
                // );
                this.toastr.warning('No Data available', 'Warning!');
              }
            } else {
              // this.toastr.error(
              //   "Something went wrong please try again later",
              //   "Server Error!"
              // );
              this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          })
          .catch(result => {
            console.log("ServerResponseError :", result);
            // this.toastr.error(
            //   "Something went wrong please try again later",
            //   "Server Error!"
            // );
            this.toastr.warning('Something went wrong please try again later', 'Warning!');
            // this.spinner.hide();
          });
      } else if (data.activityTypeId == 6) {
        var employeeId = localStorage.getItem("employeeId");
        // var userDetails = JSON.parse(localStorage.getItem("userDetails"));
        var params = {
          fId: data.feedbackId,
          tId: this.userDetails.tenantId,
          eId: employeeId,
          cId: data.courseId,
          mId: data.moduleId,
          aId: data.activityId,
          enId : data.enrolId,
        };
        var url = ENUM.domain + ENUM.url.checkFeedbackUser;
        this.CDSP.getCourseDetailsData(url, params)
          .then(result => {
            var temp: any = result;
            if (temp.data !== null || temp.data !== undefined) {
              if (temp.data.length > 0) {
                console.log('CHECk FEEDBACK FOR USER---->', temp.data[0]);
                if (temp.data[0].flag == 'true') {
                  this.CDSP.setFeedbackSummary(data);
                  const courseDetailSummary = {
                    courseFrom: 3,
                  };
                  this.CSP.setCourse(courseDetailSummary, data);
                  this.router.navigate(['../learn/course-detail/feedback'], { relativeTo: this.routes });
                } else {
                  this.toastr.warning(temp.data[0].msg, "Warning!");
                }
              } else {
                // this.toastr.error(
                //   "Something went wrong please try again later",
                //   "Server Error!"
                // );
                this.toastr.warning('No Data available', 'Warning!');
              }
            } else {
              // this.toastr.error(
              //   "Something went wrong please try again later",
              //   "Server Error!"
              // );
              this.toastr.warning('Something went wrong please try again later', 'Warning!');
            }
          })
          .catch(result => {
            console.log("ServerResponseError :", result);
            // this.toastr.error(
            //   "Something went wrong please try again later",
            //   "Server Error!"
            // );
            this.toastr.warning('Something went wrong please try again later', 'Warning!');
            // this.spinner.hide();
          });
      } else if (data.activityTypeId == 9) {
        const courseDetailSummary = {
          courseFrom: 3,
        };
        this.CSP.setCourse(courseDetailSummary, data);
        this.router.navigate(['mark-attendance'], { relativeTo: this.routes });
      } else {
        const courseDetailSummary = {
          courseFrom: 3,
        };
        this.CSP.setCourse(courseDetailSummary, data);
        this.router.navigate(['course'], { relativeTo: this.routes });
      }

  }

  // bindBackgroundImage(detail) {
  //   // detail.img = 'assets/images/open-book-leaf-2.jpg';
  //   // return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};
  //   if (detail.activity_type === 'Quiz'){
  //     detail.img = 'assets/images/activity_image/quiz.jpg';
  //     return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //   } else if ( detail.activity_type  === 'Feedback'){
  //     detail.img = 'assets/images/activity_image/feedback.jpg';
  //     return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //    } else if ( detail.referenceType  === 'video' || detail.mimeType  === 'video/mp4'){
  //       detail.img = 'assets/images/activity_image/video.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else if ( detail.referenceType  === 'audio' || detail.mimeType  === 'audio/mpeg'){
  //       detail.img = 'assets/images/activity_image/audio.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else if ( detail.referenceType  === 'application' && detail.mimeType  === 'application/zip'){
  //       detail.img = 'assets/images/activity_image/scrom.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     } else if ( detail.referenceType  === 'application' && detail.mimeType  === 'application/pdf'){
  //       detail.img = 'assets/images/activity_image/pdf.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else if ( detail.referenceType  === 'kpoint' || detail.mimeType  === 'embedded/kpoint'){
  //       detail.img = 'assets/images/activity_image/video.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else if ( detail.referenceType  === 'image'){
  //       detail.img = 'assets/images/activity_image/image.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else if ( detail.referenceType  === 'application' && detail.mimeType  === 'application/x-msdownload'){
  //       detail.img = 'assets/images/activity_image/url.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else if ( detail.formatId == 9){
  //       detail.img = 'assets/images/activity_image/practice_file.jpg';
  //       return {'background-image': 'url(' + detail.img + ')', 'background-position': 'left'};
  //     }else {
  //       detail.img = 'assets/images/open-book-leaf-2.jpg';
  //       return { 'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' +  detail.img + ')' };
  //   }
  // }

  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToCourse(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}

close() {
  this.passCheck = false;
}
closeInstrction() {
  this.displayInstructions = false;
}
onSubmit1() {
  // this.router.navigate(["quiz"], { relativeTo: this.routes });
  this.router.navigate(['../learn/course-detail/quiz'], { relativeTo: this.routes });
}
get f() {
  return this.resetPwdForm.controls;
}
onSubmit() {
  this.submitted = true;
  // this.disabledGoToCourse = true;
  if (this.resetPwdForm.invalid) {
    return;
  } else {
    console.log("EnteredPassword--->", this.resetPwdForm.value.password);
    console.log("PasswordKey--->", this.passKey);
    if (this.resetPwdForm.value.password == this.passKey) {
      this.passCheck = false;
      if (this.checkResponse.instructions) {
        // this.disabledGoToCourse = true;
        this.displayInstructions = true;
      } else {
        // this.disabledGoToCourse = true;
        this.displayInstructions = false;
        // this.router.navigate(["quiz"], { relativeTo: this.routes });
        this.router.navigate(['../learn/course-detail/quiz'], { relativeTo: this.routes });
      }
    } else {
      this.passWrong = true;
    }
  }
}
}
