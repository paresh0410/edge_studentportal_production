import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcContentComponent } from './cc-content.component';

describe('CcContentComponent', () => {
  let component: CcContentComponent;
  let fixture: ComponentFixture<CcContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
