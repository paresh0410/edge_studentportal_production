import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ENUM } from '../../service/enum';
import { ProfileServiceProvider } from '../../service/profile-service';
import { PathwayServiceProvider } from '../../service/pathway-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SampleLayoutComponent } from '../../@theme/layouts/sample/sample.layout';
import { ImageCroppedEvent } from '../../../../node_modules/ngx-image-cropper';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';
import { SettingsServiceProvider } from '../../service/settings.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../@core/data/users.service';
@Component({
  selector: 'ngx-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfilePageComponent implements OnInit {
  name: string;
  link: string;
  blob: any;
  band: string;
  designation: string;
  emailId: string;
  contact_no: number;
  followers: number;
  following: number;
  tags: number;
  profile_pic: string;
  coins: number;
  qr_code: string;
  public myQrCode: string = null;
  contentDiv: any = 0;
  noPathway: boolean = false;
  badgeLength: number = 0;
  certificateLength: number = 0;
  inChPassFlag: boolean = false;
  inChPass: string = '';
  rewardsData:any;
  profileDetails = {
    profile_pic: '',
    profile_img: '',
    coins: '',
    name: '',
    band: '',
    designation: '',
    email: '',
    phone: '',
    followers: '',
    following: '',
    tags: '',
    qr_code: ''
  };

  //activity array starts//

  posticon: string;
  fa_icon: string;
  message: string;
  postby: string;
  time: string;
  today: number = Date.now();

  roles: any = [
    // { id: 1, name: 'Learner', active: false },
    // { id: 2, name: 'Manager', active: false },
    // { id: 3, name: 'Trainer', active: false },
  ];
  activity;
  notifications;
  //activity array closed//

  //pathways array//

  pathwayImage: string;
  pathwayName: string;
  sections: number;
  items: number;
  createdOn: Date;

  pathwayArray = [
    {
      pathwayImage: './assets/images/eva.png',
      pathwayName: 'Create a new pathway',
      pathwayBody:
        'You can add mutiple items of your choice to enjoy your own pathways of learning'
    },
    {
      pathwayImage: './assets/images/eva.png',
      pathwayName: 'Pathway Name',
      sections: '0',
      items: '6',
      createdOn: '26 Jan 2018'
    },
    {
      pathwayImage: './assets/images/jack.png',
      pathwayName: 'Pathway Name',
      sections: '0',
      items: '6',
      createdOn: '26 Jan 2018'
    },
    {
      pathwayImage: './assets/images/kate.png',
      pathwayName: 'Pathway Name',
      sections: '0',
      items: '6',
      createdOn: '26 Jan 2018'
    },
    {
      pathwayImage: './assets/images/lee.png',
      pathwayName: 'Pathway Name',
      sections: '0',
      items: '6',
      createdOn: '26 Jan 2018'
    },
    {
      pathwayImage: './assets/images/kate.png',
      pathwayName: 'Pathway Name',
      sections: '0',
      items: '6',
      createdOn: '26 Jan 2018'
    },
    {
      pathwayImage: './assets/images/nick.png',
      pathwayName: 'Pathway Name',
      sections: '0',
      items: '6',
      createdOn: '26 Jan 2018'
    }
  ];

  // Badges array //

  badgesimg: string;
  badgeTitle: string;
  badgelevel: string;

  badgesArray = [];

  //badges array end//

  // Skills Array

  skills: boolean = false;
  skills1: boolean = true;

  // certificate array//

  certificateimg: string;
  greetings: string;
  message2: string;
  noofcourses: number;

  certificateArray = [];

  modal: boolean;
  syncSet: boolean;
  notify: boolean;
  privacyModal: boolean;

  // password settings//

  password: string;
  confirm_pass: String;
  passWrong: boolean;

  resetPwdForm: FormGroup;
  submitted = false;

  togglebtn = false;

  SynSettings() {
    if (!this.togglebtn) {
      this.togglebtn = true;
    } else {
      this.togglebtn = false;
    }
  }

  toggleState: boolean;
  sendNotification = [
    { statement: 'Send notification on email', toggleState: false },
    { statement: 'Send notification on phone', toggleState: false }
  ];

  privacyPref = [
    { viewValue: 'Followers only', value: 'Followers Only' },
    { viewValue: 'Everyone', value: 'Everyone' },
    { viewValue: 'Keep Hidden', value: 'Keep Hidden' }
  ];
  profileworking: boolean = false;
  badgeworking: boolean = false;
  certificateworking: boolean = false;
  rewardsworking: boolean = false;
  useractivdata: any;
  constructor(
    private router: Router,
    private routes: ActivatedRoute,
    private formBuilder: FormBuilder,
    private ProSP: ProfileServiceProvider,
    private PathSP: PathwayServiceProvider,
    private spinner: NgxSpinnerService,
    private SLC: SampleLayoutComponent,
    private SSP: SettingsServiceProvider,
    private toastr: ToastrService,
    public userService: UserService
  ) {
     this.modal = false;
    this.syncSet = false;
    this.notify = false;
    this.privacyModal = false;
    this.profileworking = false;
    this.badgeworking = false;
    this.rewardsworking = false;
    // this.profile_picAfterUpdate.emit('Umesh dada');
    // this.myQrCode  = 'Your QR code data string';
    let url1 = ENUM.domain + ENUM.url.getProfile;
    let userDetails = JSON.parse(localStorage.getItem('userDetails'));
    let param1 = {
      uId: userDetails.id
    };
    this.ProSP.getProfileData(url1, param1)
      .then(result => {
        let temp: any = result;
        console.log('RESULT OF PROFILE PAGE===>', temp.data);
        temp.data[0].name =
          temp.data[0].firstname + ' ' + temp.data[0].lastname;
        if ( temp.data[0].profile_img ) {
          temp.data[0].profile_pic = temp.data[0].profile_img;
        }else{
          temp.data[0].profile_pic = '../../../assets/images/user.png';
        }
        // followers: 1022, following: 198, tags: 57,
        if (
          temp.data[0].followers == null ||
          temp.data[0].followers == undefined
        ) {
          temp.data[0].followers = 0;
        }
        if (
          temp.data[0].following == null ||
          temp.data[0].following == undefined
        ) {
          temp.data[0].following = 0;
        }
        if (temp.data[0].tags == null || temp.data[0].tags == undefined) {
          temp.data[0].tags = 0;
        }
        if (temp.data[0].coins == null || temp.data[0].coins == undefined) {
          temp.data[0].coins = 0;
        }
        temp.data[0].qr_code = './assets/images/qr_code.png';
        this.profileDetails = temp.data[0];
        this.profileworking = true;
        // console.log('PD-->', this.profileDetails);
      })
      .catch(result => {
        this.profileworking = true;
        // this.spinner.hide();
        // console.log('ServerResponseError :', result);
      });

    // var url2 = ENUM.domain + ENUM.url.getPathway;
    // var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    // var param2 = {
    // 	uId: userDetails.id
    // }
    // this.PathSP.getPathwayData(url2,param2)
    // .then(result=>{
    // 	var temp:any = result;
    // 	console.log('PATHWAY===>',temp.data[0]);
    // 	if(temp.data != null || temp.data != undefined){
    // 		if(temp.data.length == 0){
    // 			this.noPathway = true;
    // 		}
    // 	}
    // }).catch(result =>{
    // 	// this.spinner.hide();
    //   	console.log('ServerResponseError :',  result);
    // })

    let url3 = ENUM.domain + ENUM.url.getBadges;
    // const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    let employeeId = JSON.parse(localStorage.getItem('employeeId'));
    let param3 = {
      uId: userDetails.id,
      eId: employeeId,
      tId: userDetails.tenantId
    };
    this.ProSP.getProfileData(url3, param3).then((res: any) => {
      let url = ENUM.domain + ENUM.url.getCertificates + '?';
      // console.log('Total Ref:=======>',res);
      if (res.data.badges && res.data.badges.length > 0) {
        this.badgesArray = res.data.badges;
      }
      if (res.data.certificates && res.data.certificates.length > 0) {
        this.certificateArray = res.data.certificates;
      }
      if (res.data.rewardsData && res.data.rewardsData.length > 0) {
        this.rewardsData = res.data.rewardsData[0];
      }
      // console.log('Rewards Data:=======>', this.rewardsData);
      for (let i = 0; i < this.certificateArray.length; i++) {
        if (this.certificateArray[i].certimg) {
          this.certificateArray[i].certificateimg = this.certificateArray[
            i
          ].certimg;
        } else {
          this.certificateArray[i].certificateimg =
            './assets/images/Certificate.png';
        }
        let String =
          'usrId=' +
          userDetails.id +
          '&corsId=' +
          this.certificateArray[i].id +
          '&tId=' +
          userDetails.tenantId +
          '&enrolId=' +
          this.certificateArray[i].enrolId +
          '&type=web';
        this.certificateArray[i].pdfUrl = url + String;
      }
      if (this.badgesArray.length > 0) {
        this.badgeLength = this.badgesArray.length;
      } else {
        this.badgeLength = 0;
      }
      if (this.certificateArray.length > 0) {
        this.certificateLength = this.certificateArray.length;
      } else {
        this.certificateLength = 0;
      }
      this.certificateworking = true;
      this.badgeworking = true;
      this.rewardsworking = true;
    });

    if (localStorage.getItem('userOtherDetails')) {
      let UOD = JSON.parse(localStorage.getItem('userOtherDetails'));
      let temp = UOD[3];
      for (let i = 0; i < temp.length; i++) {
        if (temp[i].status == true) {
          let MENU_ITEM = temp[i].menu;
          this.SLC.setSidebar(MENU_ITEM);
        }
      }
      this.roles = temp;
    }
  }

  ngOnInit() {
    this.resetPwdForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_pass: ['', [Validators.required, Validators.minLength(6)]]
    });


  }

  notifySetting() {
    this.notify = true;
  }

  privacy() {
    this.privacyModal = true;
  }

  activeUser(role, rolesIndex) {
    if(role && role['status']){
      return null;
    }
    this.useractivdata = JSON.parse(localStorage.getItem('userdetail'));
    this.useractivdata.roll = role.roleName;
    this.useractivdata.roleId = role.roleId;
    localStorage.setItem('userdetail', JSON.stringify(this.useractivdata));
    for (let i = 0; i < this.roles.length; i++) {
      this.roles[i].status = false;
    }
    this.roles[rolesIndex].status = true;
    localStorage.setItem('roleName', this.roles[rolesIndex].name);
    for (let i = 0; i < this.roles.length; i++) {
      if (this.roles[i].status == true) {
        let MENU_ITEM = this.roles[i].menu;
        this.link = this.roles[i].menu[0].link;
        this.SLC.setSidebar(MENU_ITEM);
      }
    }
    if (localStorage.getItem('userOtherDetails')) {
      let UOD = JSON.parse(localStorage.getItem('userOtherDetails'));
      UOD[3] = this.roles;
      UOD = JSON.stringify(UOD);
      localStorage.setItem('userOtherDetails', UOD);
    }
    let linkString = '../../' + this.link;
    this.router.navigate([linkString], { relativeTo: this.routes });
  }

  toggleBtnState(notify) {
    if (!notify.toggleState) {
      notify.toggleState = true;
      //   console.log(notify.toggleState);
    } else {
      notify.toggleState = false;
      //   console.log(notify.toggleState);
    }
  }

  get f() {
    return this.resetPwdForm.controls;
  } //  form fields getter for easy access

  onSubmit() {
    this.submitted = true;
    this.password_check(this.confirm_pass, this.password);
    // stop here if form is invalid
    if (this.resetPwdForm.invalid) {
      return;
    } else {

      // console.log('Old Password ==>', this.password);
      // console.log('New Password ==>', this.confirm_pass);
      if (localStorage.getItem('userDetails')) {
        const userDetails = JSON.parse(localStorage.getItem('userDetails'));
        const param = {
          ecn: userDetails.username,
          oldPassword: this.password,
          newPassword: this.confirm_pass,
          tId: userDetails.tenantId,
          type: 3,
        };
        // console.log('Parameters==>', param);
        const url = ENUM.domain + ENUM.url.changePassword;
        this.SSP.changePassword(url, param).then((res: any) => {
          console.log('RESULT CHANGE PASSWORD ==>', res);
          if (res.type) {
            this.inChPassFlag = false;
            this.passWrong = false;
            this.modal = false;
            this.resetPwdForm.reset();
            this.toastr.success(res.data1, 'Success!').onHidden.subscribe(()=>
            this.toastr.clear());
          } else {
              this.resetPwdForm.reset();
            this.inChPass = res.data1;
            this.inChPassFlag = true;
          }
        });
      }
    }
  }

  popUPModal() {
    this.modal = true;
  }

  syncSetting() {
    this.syncSet = true;
  }

  close() {
    this.modal = false;
    this.syncSet = false;
    this.notify = false;
    this.privacyModal = false;
    this.resetPwdForm.reset();
    this.inChPassFlag = false;
    this.passWrong = false;
  }

  checkPassword() {
    // if (this.password === this.confirm_pass) {
    return true;
    // } else {
    //   return false;
    // }
  }
  // onInput(value) {
  //   if (this.checkPassword()) {
  //     //  if both passwords are same set errors to null
  //     this.f.confirm_pass.setErrors(null);
  //     this.passWrong = false;
  //     //   console.log(this.passWrong);
  //   } else {
  //     //  set error to confirm_pass manually
  //     this.f.confirm_pass.setErrors([{ passwordMismatch: true }]);
  //     this.passWrong = true;
  //     //   console.log(this.passWrong);
  //   }
  // }

  password_check(val1 , val2){
    if (val1 === val2 ) {
      this.f.confirm_pass.setErrors([{ passwordmatched: true }]);
      this.passWrong = true;
        console.log( this.f.confirm_pass);
        console.log( this.f.password);
    }
    else{
      console.log( this.f.confirm_pass);
      console.log( this.f.password);
      this.f.confirm_pass.setErrors(null);
      this.passWrong = false;

    }
  }

  click(data, index) {
    let url = ENUM.domain + ENUM.url.getCertificates + '?';
    const userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      uId: userDetails.id,
      corsId: data.id,
      tId: userDetails.tenantId,
      enrolId: data.enrolId,
      type: 'web'
    };
    const String =
      'usrId=' +
      userDetails.id +
      '&corsId=' +
      data.id +
      '&tId=' +
      userDetails.tenantId +
      '&enrolId=' +
      data.enrolId +
      '&type=web';
    url = url + String;
    // console.log('URL===>', url);
    location.href = url;
    // this.ProSP.getCertificateData(url)
    // .then((res: any)=>{
    // 	// this.blob = new Blob([res], {type: 'application/pdf'});
    // 	console.log('Certificate Downloaded for Course ', data.id);
    // })
  }

  tabChanged(event) {
    // console.log('TABCHANGED!!!!!', event.index);
    this.contentDiv = event.index;
  }

  addPathway() {
    this.router.navigate(['addpathway'], { relativeTo: this.routes });
  }

  insidePathway() {
    this.router.navigate(['insidePathway'], { relativeTo: this.routes });
  }

  // Slider
  currentIndex = 0;
  speed = 5000;
  infinite = false;
  direction = 'right';
  directionToggle = true;
  autoplay = true;
  avatars = '1234567'.split('').map((x, i) => {
    const num = i;
    // const num = Math.floor(Math.random() * 1000);
    return {
      url: `https://picsum.photos/600/400/?${num}`,
      title: `${num}`
    };
  });

  // Slider
  currentIndex1 = 0;
  speed1 = 5000;
  infinite1 = true;
  direction1 = 'right';
  directionToggle1 = true;
  autoplay1 = true;

  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 500,
      number: 1
    },
    {
      width: 768,
      number: 3
    },
    {
      width: 1024,
      number: 5
    }
  ];
  // Slider 2
  breakpoint2: NgxHmCarouselBreakPointUp[] = [
    {
      width: 700,
      number: 1
    },
    {
      width: 1024,
      number: 3
    }
  ];
  //  Image Crop
  imageChangedEvent: any = '';
  croppedImage: any = '';
  showCropper = false;
  intialImage = false;
  selectImage = false;
  // importFile(event) {
  // 	if (event.target.files.length == 0) {
  // 		console.log('No file selected!');
  // 		return
  // 	}
  // 	let file: File = event.target.files[0];
  // 	console.log(file);
  // 	// after here 'file' can be accessed and used for further process
  // }
  fileChangeEvent(event: any): void {
    this.intialImage = true;
    this.selectImage = true;
    this.imageChangedEvent = event;
    console.log(this.imageChangedEvent);
    // this.intialImage=true;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log('Crop Image ===>', this.croppedImage);
  }
  imageLoaded() {
    this.showCropper = true;
    console.log('Image loaded');
  }
  dissmissImageCropPopModal(index) {
    if (localStorage.getItem('userDetails')) {
      let userDetails = JSON.parse(localStorage.getItem('userDetails'));
      if (index == 1) {
        let url = ENUM.domain + ENUM.url.saveProfileImg;
        let param = {
          uId: userDetails.id,
          base64: this.croppedImage,
          tId: userDetails.tenantId,
        };
        this.ProSP.uploadProfilePic(url, param).then((res: any) => {
          if (res.type) {
            this.toastr.success(
              'Profile picture updated successfully',
              'Success!',
            ).onHidden.subscribe(()=>
            this.toastr.clear());
            console.log("Res ==>", res);
            this.selectImage = false;
            userDetails.picture_url =  res.data1.Location;
            localStorage.setItem('userDetails', JSON.stringify(userDetails));
            let userdata = JSON.parse(localStorage.getItem('userdetail'));
            userdata.picture_url = res.data1.Location;
            localStorage.setItem('userdetail', JSON.stringify(userdata));
            this.userService.updatedDataSelection(res.data1.Location);
          } else {
            this.toastr.warning(
              'Cannot update profile picture. Please try again',
              'Error Occurred!'
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        });
      } else if (index == 2) {
        let url = ENUM.domain + ENUM.url.removeProfileImg;
        let param = {
          uId: userDetails.id,
          Location: null,
          tId: userDetails.tenantId,
        };
        this.ProSP.deleteProfilePic(url, param).then((res: any) => {
          if (res.type) {
            this.toastr.success(
              'Profile picture removed successfully',
              'Success!'
            ).onHidden.subscribe(()=>
            this.toastr.clear());
            this.intialImage = false;
            this.selectImage = false;
            userDetails.picture_url =  null;
            this.croppedImage = '../../../assets/images/user.png';
            localStorage.setItem('userDetails', JSON.stringify(userDetails));
            let userdata = JSON.parse(localStorage.getItem('userdetail'));
            userdata.picture_url = null;
            localStorage.setItem('userdetail', JSON.stringify(userdata));
            this.userService.updatedDataSelection(null);
          } else {
            this.toastr.warning(
              'Cannot remove profile picture. Please try again',
              'Error Occurred!'
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        });
      } else {
        if ( !this.profileDetails.profile_img ) {
          this.intialImage = false;
        }else {
          this.intialImage = false;
          this.profileDetails.profile_pic = this.profileDetails.profile_img;
          userDetails.picture_url =  this.profileDetails.profile_img;
          localStorage.setItem('userDetails', JSON.stringify(userDetails));
          let userdata = JSON.parse(localStorage.getItem('userdetail'));
          userdata.picture_url = this.profileDetails.profile_img;
          localStorage.setItem('userdetail', JSON.stringify(userdata));
          this.userService.updatedDataSelection(this.profileDetails.profile_img);
        }
        this.selectImage = false;
      }
    } else {
      this.toastr.warning('Please login again', 'Warning Login!').onHidden.subscribe(()=>
      this.toastr.clear());
    }
  }
}
