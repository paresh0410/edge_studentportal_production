import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.scss']
})
export class BookmarkComponent implements OnInit {


  isMandatory:boolean;
  isSelected:boolean;
  cardImg:string;
  content:string;
  stages:string;

  bookmarkitems=[
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected:false},
    { isMandatory:true, cardImg: "./assets/images/Orange_icon.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy", stages:"4 stages", isSelected:false},
    { isMandatory:false, cardImg: "./assets/images/kitten-corporate.png",description:"Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…",save: false, content: "How Design Helps Business in setting a great marketing strategy",  isSelected:false},
    
  ];

  name:String;
  time:string;

  recentlyViewed=[
    { name: "Design", time: "42 mins"},
    { name: "Sales", time: "42 mins"},
    { name: "Positivity", time: "42 mins"},
    { name: "JohnDoe", time: "42 mins"},
    { name: "Design", time: "42 mins"},
    { name: "Sales", time: "42 mins"},
    { name: "Positivity", time: "42 mins"},
    { name: "JohnDoe", time: "42 mins"},
    { name: "Design", time: "42 mins"},
    { name: "Sales", time: "42 mins"},
    { name: "Positivity", time: "42 mins"},
    { name: "JohnDoe", time: "42 mins"},
    { name: "Design", time: "42 mins"},
  ];
  
  folderICo:string;
  folderName:string;
  noOfItems:string;

  bookmarkFolder=[
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},
    {folderICo: './assets/images/folderico.svg', folderName:'Design Management', noOfItems:'4'},

  ];

  // selectedCount:number;

  // radiofn(post){
  //   this.selectedCount=0;
  
  //     if(!post.isSelected){       
  //     post.isSelected = true;       //if radio button is false make it true
      
  //     return  this.selectedCount=this.count();        // store count in variable if true
  //     }else{
  //     post.isSelected = false;
  //     return  this.selectedCount=this.count();        // store count in variable if false
  //     }   
  //   }

  //   count(){
  //     var count=0;
  //     this.bookmarkitems.forEach(obj => {      //for each object in array of addpathway
  //       console.log(obj);
  //       if(obj.isSelected==true)            // if array element is selected
  //       {
  //         count++;                          //increment count
  //       }
  //     })
  //     return  count;
  //   }


  createbookmark(){
    this.router.navigate(['../../favourites/create-bookmark'],{relativeTo:this.routes}) 
  }

  togglesave(item){
    if(!item.save){
      item.save = true;
    }else{
      item.save = false;
    }
  }


  goBACK(){
    this.router.navigate(['../../favourites'],{relativeTo:this.routes})    
  }

  constructor(private router:Router, private routes:ActivatedRoute) { }

  ngOnInit() {
  }

}
