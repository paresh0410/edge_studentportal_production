import { Component, OnInit, Input } from '@angular/core';
import { EarnedCertificateViewerService } from './certificates.service'
import { ENUM } from "../../service/enum";

@Component({
  selector: 'ngx-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.scss']
})
export class CertificatesComponent implements OnInit {
  @Input('skeleton') skeleton: any;
  certificates = [
    // { certificateId: 1, certiName: "Master 1", path: "./assets/images/Group12.png"},
  ];
  selectedItem: any;
  selectedIndex: any;

  constructor(private ECS: EarnedCertificateViewerService) {


  }

  ngOnInit() {
    console.log(this.skeleton);
    if (this.skeleton == false) {
      this.certificates = this.ECS.getCertificateData();
      console.log('Certi===>', this.certificates);
    }
  }

  clickToDownload(data) {
    let url = ENUM.domain + ENUM.url.getCertificates + "?";
    const userDetails = JSON.parse(localStorage.getItem("userDetails"));
    const param = {
      uId: userDetails.id,
      corsId: data.id,
      tId: userDetails.tenantId,
      enrolId: data.enrolId,
      type: "web"
    };
    const String =
      "usrId=" +
      userDetails.id +
      "&corsId=" +
      data.id +
      "&tId=" +
      userDetails.tenantId +
      "&enrolId=" +
      data.enrolId +
      "&type=web";
    url = url + String;
    // console.log("URL===>", url);
    location.href = url;
    // this.ProSP.getCertificateData(url)
    // .then((res: any)=>{
    // 	// this.blob = new Blob([res], {type: 'application/pdf'});
    // 	console.log('Certificate Downloaded for Course ', data.id);
    // })
  }

}
