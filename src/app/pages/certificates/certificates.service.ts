import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class EarnedCertificateViewerService {
  certificatesDetails: any;

  constructor() {}

  setCertificateData(Data) {
    this.certificatesDetails = Data;
  }

  getCertificateData() {
    return this.certificatesDetails;
  }
}
