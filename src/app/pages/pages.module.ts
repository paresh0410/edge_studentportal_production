import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { FormsModule } from '@angular/forms';
// import { PdfViewerModule } from 'ng2-pdf-viewer';
import { InlineSVGModule } from 'ng-inline-svg';
// import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
// import { DashboardModule } from './dashboard/dashboard.module';
// import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { TagInputModule } from 'ngx-chips';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { SortablejsModule } from 'angular-sortablejs';
// import { BrowserModule } from '@angular/platform-browser';
import { NgxUploaderModule } from 'ngx-uploader';
import { DashboardModule } from '../pages/dashboard/dashboard.module';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { MatVideoModule } from 'mat-video';
import { DocumentViewModule } from 'ngx-document-view';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatFormFieldModule, MatSelectModule } from '@angular/material';
import { HttpModule ,Http } from '@angular/http';
import { webAPIService } from '../service/webAPIService';
import { PassService } from '../service/passService';
import { SendFeedbackService } from '../service/send-feedback.service';
import { VideoPlayerService } from './video-player/videoplayer.service';
import { AudioPlayerService } from './audio-player/audioplayer.service';
import { KPointService } from './k-point/k-point.service';
import { KPointComponent } from './k-point/k-point.component';
import { ScormPlayerComponent } from './scorm-player/scorm-player.component';
import { ScormService } from '../service/scormservices.service';
import { WelcomePageService } from '../service/welcome-page.service';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { LoginComponent } from './login/login.component';
// import { SignupComponent } from './signup/signup.component';

import { MatNativeDateModule } from '@angular/material';
import { DemoMaterialModule } from './material-module';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { TrainerDashboardComponent } from './trainer-dashboard/trainer-dashboard.component';
import { TaskManagerComponent } from './task-manager/task-manager.component';
import { CalendarComponent } from './calendar/calendar.component';
import { Level1Component } from './level1/level1.component';
import { Level2Component } from './level2/level2.component';
import { TrainerDetailsComponent } from './trainer-details/trainer-details.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { IntermediateComponent } from './intermediate/intermediate.component';
import { LearnComponent } from './learn/learn.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { PulseComponent } from './pulse/pulse.component';
import { AddPathwayComponent } from './add-pathway/add-pathway.component';
import { InsidePathwayComponent } from './inside-pathway/inside-pathway.component';
import { QuizComponent } from './quiz/quiz.component';
import { QuizReviewComponent } from './quiz-review/quiz-review.component'
import { CourseComponent } from './course/course.component';

import { FavouritesComponent } from './favourites/favourites.component';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { CreateBookmarkComponent } from './create-bookmark/create-bookmark.component';
import { TeamDashboardComponent } from './team-dashboard/team-dashboard.component';
import { NoDataFoundComponent } from './no-data-found/no-data-found.component';
import { VideoPlayerComponent } from './video-player/videoplayer.component';
import { AudioPlayerComponent } from './audio-player/audioplayer.component';
import { DocumentViewerComponent } from './document-viewer/documentviewer.component';
import { DocumentViewerService } from './document-viewer/documentviewer.service';
import { CourseTypeCardViewerService } from './course-type-card/course-type-card.service';
import { leaderboardViewerService } from './leader-board/leader-board.service';
import { EarnedBadgesViewerService } from './earned-badges/earned-badges.service';
import { EarnedCertificateViewerService } from './certificates/certificates.service';
// import { CourseTypeCardComponent } from './course-type-card/course-type-card.component';
// import { SafePipeModule } from 'safe-pipe';
import { HttpConfigInterceptor} from '../interceptor/httpconfig.interceptor';
import { SafePipePipe } from '../safe-pipe.pipe';
import { SurveyComponent } from './survey/survey.component';
import { PollComponent } from './poll/poll.component';
import { FeedbackComponent } from './feedback/feedback.component';
// import { ModalViewerComponent } from '../component/modal-viewer/modal-viewer.component';
// import { ToasterComponent } from '../component/toaster/toaster.component';
// import { CoachingModule } from '../pages/coaching/coaching.module';
import { CoachingComponent } from '../pages/coaching/coaching.component';
// import { CallDetailComponent } from './call-detail/call-detail.component';
// import { ContentComponent } from './call-detail/content/content.component';
// import { CallDetailsChatComponent } from './call-detail/call-details-chat/call-details-chat.component';
// import { CallDetailFeedbackComponent } from './call-detail/call-detail-feedback/call-detail-feedback.component';
// import { CallDetailAssessmentComponent } from './call-detail/call-detail-assessment/call-detail-assessment.component';
// import { YTPlayerModule } from 'angular-youtube-player';
import { ParticipantsComponent } from './participants/participants.component';
import { AssessmentDataComponent } from './assessment-data/assessment-data.component';
import { FeedbackDataComponent } from './feedback-data/feedback-data.component';
import { AttendanceDataComponent } from './attendance-data/attendance-data.component';
import { TrainerCoursesContentComponent } from './trainer-courses-content/trainer-courses-content.component';

import { TrainerParticipantCourseContentComponent } from './trainer-participant-course-content/trainer-participant-course-content.component';
import { TTTNominationComponent } from './ttt-nomination/ttt-nomination.component';
import { CodeOfConductComponent } from './ttt-nomination/code-of-conduct/code-of-conduct.component';
import { ProfileFormComponent } from './ttt-nomination/profile-form/profile-form.component';
import { TttFeedbackComponent } from './ttt-nomination/ttt-feedback/ttt-feedback.component';
import { TttContentComponent } from './ttt-nomination/ttt-content/ttt-content.component';
import { EvaluationComponent } from './ttt-nomination/evaluation/evaluation.component';
import { TttWebinarComponent } from './ttt-nomination/ttt-webinar/ttt-webinar.component';
// import { LogoutComponent } from './logout/logout.component';
import { StarRatingModule } from 'angular-star-rating';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
import { MarkAttendanceComponent } from './mark-attendance/mark-attendance.component';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ImageCropperModule } from '../../../node_modules/ngx-image-cropper';
import { TaTrainerDetailComponent } from './ta-trainer-detail/ta-trainer-detail.component';
import { CourseContentComponent } from './ta-trainer-detail/course-content/course-content.component';
import { TttWebinarActivityComponent } from './ta-trainer-detail/ttt-webinar-activity/ttt-webinar-activity.component';
import { ParticipantsListComponent } from './ta-trainer-detail/participantsList/participants.component';
import { TttBookSlotsComponent } from './ttt-book-slots/ttt-book-slots.component';

// import { WebinarComponent } from './webinar/webinar.component';
// import { WebinarMultiComponent } from './webinar-multiple/webinar-multi.component';
import { WebinarServiceProvider } from '../service/webinar.service';
// import { ChatComponent } from './chat/chat.component';

import { LearnSearchComponent } from './learn-search/learn-search.component';
import { TttFeedbackActivityComponent } from './ta-trainer-detail/ttt-feedback-activity/ttt-feedback-activity.component';
import { TttBhApprovalComponent } from './ttt-bh-approval/ttt-bh-approval.component';
import { TrainerDashboardDetailsComponent } from './trainer-dashboard-details/trainer-dashboard-details.component';
import { BatchesDetailComponent } from './trainer-dashboard-details/batches-detail/batches-detail.component';
import { TrainerTAevalutionfbComponent } from './trainer-taevalutionfb/trainer-taevalutionfb.component';
import { TttCertificateComponent } from './ttt-nomination/ttt-certificate/ttt-certificate.component';
import { AuthGuard } from '../service/auth.guard';
import { OpenviduSessionModule, OpenviduSessionComponent } from 'openvidu-angular';
import { EepStepComponent } from './eep-step/eep-step.component';
import { EepCocComponent } from './eep-coc/eep-coc.component';
import { EepProfileComponent } from './eep-profile/eep-profile.component';
import { EeeModuleComponent } from './eee-module/eee-module.component';
import { EepGbhApprovalComponent } from './eep-gbh-approval/eep-gbh-approval.component';
import { CcCallsListComponent } from './cc-calls-list/cc-calls-list.component';
import { CcContentComponent } from './cc-content/cc-content.component';
import { CcFeedbackComponent } from './cc-feedback/cc-feedback.component';
import { CcTrainerNominationlistComponent } from './cc-trainer-nominationlist/cc-trainer-nominationlist.component';
import { CcAssessmentComponent } from './cc-assessment/cc-assessment.component';
import { CcObservationComponent } from './cc-observation/cc-observation.component';
const PAGES_COMPONENTS = [
  PagesComponent,
];
// import { UserVideoComponent } from '../pages/webinar/user-video.component';
// import { OpenViduVideoComponent } from '../pages/webinar/ov-video.component';
import { PartnerComponent } from './partner/partner.component';
import { PartnerNomineeCallComponent } from './partner-nominee-call/partner-nominee-call.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { TruncateModule } from 'ng2-truncate';
import { WorkflowLearnComponent } from './workflow-learn/workflow-learn.component';
// import { UpcomingBatchComponent } from './upcoming-batch/upcoming-batch.component';
import {MatTabsModule} from '@angular/material/tabs';
import { QuillModule } from 'ngx-quill';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import { SectionComponent } from './tools/section/section.component';
import { ContentComponent } from './tools/content/content.component';
import { ContentDisplayComponent } from './content-display/content-display.component';
import { SafePipeModule } from 'safe-pipe';
import { SafePipe } from '../safe.pipe';
import { NullvaluePipe } from './../pipes/nullvalue.pipe';
import { NgxContentLoadingModule } from 'ngx-content-loading';

import { DatePipe } from '@angular/common';
import { RemoveHtmlPipe } from '../pipes/remove-html.pipe';
import { DynamicLinkLearnComponent } from './dynamic-link-learn/dynamic-link-learn.component';

import { PipeModule } from '../pipes/pipes.module' ;
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
// import { ModuleActivityComponent } from './module-activity-module/module-activity/module-activity/module-activity.component';

import { CommonComponentSharingModule } from './common-component-sharing/common-component-sharing.module';
import { ImageViewerModuleCommonModule } from './common-component-sharing/image-viewer-module-common.module';
// import { LoginNewComponent } from './login-new/login-new.component';

import { ComponentModule } from '../component/component.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    PagesRoutingModule,
    PipeModule.forRoot(),
    StarRatingModule.forRoot(),
    ThemeModule,
    DocumentViewModule,
    // DashboardModule,
    // ECommerceModule,
    MiscellaneousModule,
    DemoMaterialModule,
    MatNativeDateModule,
    Ng2CarouselamosModule,
    TagInputModule,
    Ng2SearchPipeModule,
    DashboardModule,
    NgxUploaderModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    FormsModule,
    QuillModule.forRoot(),
    NgxPasswordToggleModule,
    MatVideoModule,
    FilterPipeModule,
    SortablejsModule,
    MatTabsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    CommonModule,
    NgxHmCarouselModule,
    ImageCropperModule,
    NgxSkeletonLoaderModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TruncateModule,
    SafePipeModule,
    // PdfViewerModule,
    // NgxExtendedPdfViewerModule,
    NgxContentLoadingModule,
    InlineSVGModule.forRoot(),
    NgxYoutubePlayerModule.forRoot(),

    // OpenviduSessionModule
    // OpenviduSessionModule
    CommonComponentSharingModule.forRoot(),
    ImageViewerModuleCommonModule.forRoot(),
    ComponentModule,
    InfiniteScrollModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    IntermediateComponent,
    // DashboardComponent,
    // WelcomePageComponent,
    // DashboardComponent,
    // WelcomePageComponent,
    NoDataFoundComponent,
    ProfilePageComponent,
    LearnComponent,
    CourseDetailsComponent,
    PulseComponent,
    AddPathwayComponent,
    InsidePathwayComponent,
    QuizComponent,
    QuizReviewComponent,
    CourseComponent,
    FavouritesComponent,
    BookmarkComponent,
    CreateBookmarkComponent,
    TeamDashboardComponent,
    VideoPlayerComponent,
    AudioPlayerComponent,
    DocumentViewerComponent,
    SafePipePipe,
    RemoveHtmlPipe,
    SafePipe,
    NullvaluePipe,
    TrainerDashboardComponent,
    TaskManagerComponent,
    CalendarComponent,
    Level1Component,
    Level2Component,
    TrainerDetailsComponent,
    SurveyComponent,
    PollComponent,
    FeedbackComponent,
    CoachingComponent,
    // ModalViewerComponent,
    // ToasterComponent,
    KPointComponent,
    ScormPlayerComponent,
    ParticipantsComponent,
    AssessmentDataComponent,
    FeedbackDataComponent,
    AttendanceDataComponent,
    TrainerCoursesContentComponent,
    TrainerParticipantCourseContentComponent,
    TTTNominationComponent,
    CodeOfConductComponent,
    ProfileFormComponent,
    TttFeedbackComponent,
    TttContentComponent,
    EvaluationComponent,
    TttWebinarComponent,
    // LogoutComponent
    MarkAttendanceComponent,
    TaTrainerDetailComponent,
    ParticipantsListComponent,
    CourseContentComponent,
    TttWebinarActivityComponent,
    TttBookSlotsComponent,
    // ChatComponent,
    //     WebinarComponent,
    // WebinarMultiComponent,
    // UserVideoComponent,
    // OpenViduVideoComponent,
    // ModalViewerComponent,
    LearnSearchComponent,
    TttFeedbackActivityComponent,
    TttBhApprovalComponent,
    TrainerDashboardDetailsComponent,
    BatchesDetailComponent,
    TrainerTAevalutionfbComponent,
    TttCertificateComponent,

    // OpenviduSessionComponent
    EepStepComponent,
    EepCocComponent,
    EepProfileComponent,
    EeeModuleComponent,
    EepGbhApprovalComponent,
    CcCallsListComponent,
    CcContentComponent,
    CcFeedbackComponent,
    CcTrainerNominationlistComponent,
    CcAssessmentComponent,
    CcObservationComponent,
    PartnerComponent,
    PartnerNomineeCallComponent,
    WorkflowLearnComponent,
    SectionComponent,
    ContentComponent,
    ContentDisplayComponent,
    DynamicLinkLearnComponent,
    // LoginNewComponent,
    // CourseCardComponent,
    // ActivityCardComponent,
    // WorkflowCardComponent,
    // ModuleActivityComponent,
    // UpcomingBatchComponent,
    // CallDetailComponent,
    // CallDetailComponent,
    // ContentComponent,
    // CallDetailsChatComponent,
    // CallDetailFeedbackComponent,
    // CallDetailAssessmentComponent
    // LoginComponent,
    // SignupComponent,
  ],
  providers:[
     webAPIService,
     PassService,
     VideoPlayerService,
     AudioPlayerService,
     KPointService,
     DocumentViewerService,
     CourseTypeCardViewerService,
     leaderboardViewerService,
     EarnedBadgesViewerService,
     EarnedCertificateViewerService,
     ScormService,
     WebinarServiceProvider,
     AuthGuard,
     SendFeedbackService,
     WelcomePageService,
     DatePipe,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA,
  ]
})
export class PagesModule {
}
