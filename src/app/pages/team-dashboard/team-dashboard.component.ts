import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ENUM } from '../../service/enum';
import { TeamDashboardServiceProvider } from '../../service/team-dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { CallCoachingService } from '../../service/call-coaching.service';

@Component({
	selector: 'ngx-team-dashboard',
	templateUrl: './team-dashboard.component.html',
	styleUrls: ['./team-dashboard.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class TeamDashboardComponent implements OnInit {

	noPending: number;
	APP_ELEMENT_DATA: ApprovalElement[];
	ELEMENT_DATA: PeriodicElement[];
	displayedColumns: string[] = ['iscoaching', 'empCode', 'empName', 'onlineCourse', 'classroomCourse', 'completion', 'points', 'grades', 'access'];
	displayedColumnsApprovals: string[] = ['empCode', 'empName', 'course', 'startDate', 'approval'];
	displayedColumnsCompletion: string[] = ['assessment', 'total', 'attempted', 'correct', 'percentage'];
	displayedColumnsCourses: string[] = ['courseName', 'total', 'completed', 'percentage'];
	dataSource: any;
	COR_ELEMENT_DATA: CoursesElement[];
	// dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
	dataSource1: any;
	dataSource2 = new MatTableDataSource<CompletionElement>(COMP_ELEMENT_DATA);
	dataSource3: any = [];
	// dataSource3 = new MatTableDataSource<CoursesElement>(COR_ELEMENT_DATA);
	person: any;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	username: any;
	modal: boolean = false;
	modal1: boolean = false;
	isloaded: boolean = false;
	noDataFound:boolean = false;
	scoreCard = [
		{ points: "<strong class='showGreen'>12</strong>", status1: "Performers" },
		{ points: "<strong class='showRed'>20</strong>", status1: "Need Attention" },
		{ points: "<strong class='showLightRed'>32</strong>", status1: "Reporting" },
	];

	coursesList = [
		{ isMandatory: false, cardImg: "./assets/images/kitten-corporate.png", description: "Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected: false },
		{ isMandatory: true, cardImg: "./assets/images/kitten-corporate.png", description: "Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: false, cardImg: "./assets/images/kitten-corporate.png", description: "Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected: false },
		{ isMandatory: false, cardImg: "./assets/images/kitten-corporate.png", description: "Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected: false },
		{ isMandatory: true, cardImg: "./assets/images/kitten-corporate.png", description: "Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", stages: "4 stages", isSelected: false },
		{ isMandatory: false, cardImg: "./assets/images/kitten-corporate.png", description: "Mauris non tempor quam, et lacinia saen. Renat accumsan eros eget libero postul…", save: false, content: "How Design Helps Business in setting a great marketing strategy", isSelected: false },

	];
	sampleArray: any = [];
	sampleArray1: any = [];
	public hideComp: boolean = false;
	userDetails: any = [];
	title: any;
	constructor(private router: Router, private routes: ActivatedRoute, public datePipe: DatePipe,
		public callservice: CallCoachingService,
		public TDSP: TeamDashboardServiceProvider, private Toastr: ToastrService) {


	}

	ngOnInit(){
		this.inntializeData();
	}

	inntializeData() {

		if (localStorage.getItem('employeeId')) {
			if (localStorage.getItem('employeeId')) {
				var employeeId = localStorage.getItem('employeeId');
			}
			if (localStorage.getItem('userDetails')) {
				this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
				this.username = this.userDetails.firstname + ' ' + this.userDetails.lastname;
			}
			var param = {
				managerId: employeeId,
				managerUname: this.userDetails.username,
				tId: this.userDetails.tenantId,
			};
			var url = ENUM.domain + ENUM.url.getEmployeeDetails;
			this.TDSP.getEmployeeDetails(url, param)
				.then((result: any) => {
					console.log('RESULTTEAMDASH123:-', result);

					if (result.type == true && result.data[0].length > 0) {
						var temp = result.data[0];
						for (var i = 0; i < temp.length; i++) {
							temp[i].empCode = temp[i].ECN;
							temp[i].empName = temp[i].FullName;
							temp[i].date = this.formdate(temp[i].access);
							temp[i].onlineCourse = temp[i].OnlineCompleted + ' / ' + temp[i].Online;
							temp[i].classroomCourse = temp[i].ClassroomCompleted + ' / ' + temp[i].Classroom;
							if (temp[i].courseCompeletedTotal != 0) {
								temp[i].completion = temp[i].courseCompeletedTotal * 100 / temp[i].courseTotal;
							} else {
								temp[i].completion = 0;
							}
						}
						this.sampleArray1 = temp;
						this.ELEMENT_DATA = this.sampleArray1;
						this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
						console.log('RESULTTEAMDASH:-', this.ELEMENT_DATA);
					}
					if (result.type == true && result.data[1].length > 0) {
						var temp = result.data[1];
						for (var i = 0; i < temp.length; i++) {
							temp[i].course = temp[i].fullname;
							temp[i].startDate = new Date(temp[i].enrolDate);
							if (temp[i].approvalStatus == "Approved") {
								temp[i].approval = true;
							} else if (temp[i].approvalStatus == "Pending") {
								temp[i].approval = null;
							} else {
								temp[i].approval = false;
							}
						}
						console.log('RESULT===>', temp);
						this.sampleArray = temp;
						this.APP_ELEMENT_DATA = this.sampleArray;
						this.dataSource1 = new MatTableDataSource<ApprovalElement>(this.APP_ELEMENT_DATA);
						var count = 0;
						for (var i = 0; i < this.sampleArray.length; i++) {
							if (this.sampleArray[i].approval == null) {
								count++;
							}
						}
						this.noPending = count;
					} else {
						this.APP_ELEMENT_DATA = this.sampleArray;
						this.dataSource1 = new MatTableDataSource<ApprovalElement>(this.APP_ELEMENT_DATA);
						var count = 0;
						for (var i = 0; i < this.sampleArray.length; i++) {
							if (this.sampleArray[i].approval == null) {
								count++;
							}
						}
						this.noPending = count;

					}
					this.isloaded = true;
				}).catch(result => {
					this.isloaded = true;
					this.noDataFound = true;
					console.log('RESULT===>', result);
				})
			// this.dataSource.sort = this.sort;
			// this.dataSource.paginator = this.paginator;
			// this.isloaded = true;
		}

	}
	formdate(date) {
		if (date) {
			var formatted = this.datePipe.transform(date, "dd-MMM-yyyy");
			return formatted;
		}
	}
	gotocall(element) {
		console.log('call data', element);
		this.callservice.employeedata = element;
		this.callservice.frompage = 'manager';
		this.router.navigate(['../call-coaching-list'], { relativeTo: this.routes });
	}


	hideCompCourse() {
		console.log('Hide Completed Courses');
		var element = document.getElementById('slider');
		element.classList.toggle('slideClose');
		if (this.hideComp == true) {
			this.hideComp = false;
		} else {
			this.hideComp = true;
		}
	}

	selectedCount: number;

	radiofn(post) {
		this.selectedCount = 0;

		if (!post.isSelected) {
			post.isSelected = true;       //if radio button is false make it true

			return this.selectedCount = this.count();        // store count in variable if true
		} else {
			post.isSelected = false;
			return this.selectedCount = this.count();        // store count in variable if false
		}
	}

	count() {
		var count = 0;
		this.coursesList.forEach(obj => {      //for each object in array of addpathway
			console.log(obj);
			if (obj.isSelected == true)            // if array element is selected
			{
				count++;                          //increment count
			}
		})
		return count;
	}

	giveApproval(data, status) {

		console.log('DATA', data);
		console.log('STATUS', status);
		var param = {
			enrolId: data.enrolId,
			sts: status
		};
		var url = ENUM.domain + ENUM.url.approveRejectCourse;
		this.TDSP.getEmployeeDetails(url, param)
			.then((result: any) => {
				if (result.type == true) {
					if (status == 1) {
						this.Toastr.success('Course Request Approved Successfully', 'Success!');
					} else {
						this.Toastr.success('Course Request Rejected Successfully', 'Success!');
					}
					this.inntializeData();
				} else {
					this.Toastr.warning('Please try again after sometime.', 'Error Occured!');
				}
			}).catch(result => {
				console.log('RESULT===>', result);
			})

	}

	togglesave(item) {
		if (!item.save) {
			item.save = true;
		} else {
			item.save = false;
		}
	}

	getCourseDetails(element, ctype) {
		if (ctype === 1) {
			this.title = "Online Courses";
		} else if (ctype === 4) {
			this.title = "Classroom Courses";
		}
		this.COR_ELEMENT_DATA = [];
		console.log(element);
		var url = ENUM.domain + ENUM.url.course_completion;
		const data = {
			empId: element.empId,
			tId: this.userDetails.tenantId,
			cTypeId: ctype,
		};
		this.TDSP.completiondetail(url, data).then(res => {
			console.log(res);
			if (res['type'] === true) {
				this.COR_ELEMENT_DATA = res['data'];
				this.modal1 = true;
				this.dataSource3 = new MatTableDataSource<CoursesElement>(this.COR_ELEMENT_DATA);
			} else {
				this.Toastr.warning('Please try again after sometime.', 'Error Occured!');
			}
		}, err => {
			this.Toastr.warning('Please try again after sometime.', 'Error Occured!');
			console.log(err);
		});
	}

	closeCourseModal() {
		this.modal1 = false;
	}

	getCompletionDetails() {
		this.modal = true;
	}

	closeCompletionModal() {
		this.modal = false;
	}

	goTODashboard() {
		this.router.navigate(['../../dashboard'], { relativeTo: this.routes })
	}

	getChangedValue(event) {
		this.APP_ELEMENT_DATA = [];
		console.log('EVENT---->', event);
		if (event == 'All') {
			this.APP_ELEMENT_DATA = this.sampleArray;
			this.dataSource1 = new MatTableDataSource<ApprovalElement>(this.APP_ELEMENT_DATA);
		} else if (event == 'Pending') {
			for (var i = 0; i < this.sampleArray.length; i++) {
				if (this.sampleArray[i].approval == null) {
					this.APP_ELEMENT_DATA.push(this.sampleArray[i]);
				}
			}
			this.dataSource1 = new MatTableDataSource<ApprovalElement>(this.APP_ELEMENT_DATA);
		} else if (event == 'Approved') {
			for (var i = 0; i < this.sampleArray.length; i++) {
				if (this.sampleArray[i].approval == true) {
					this.APP_ELEMENT_DATA.push(this.sampleArray[i]);
				}
			}
			this.dataSource1 = new MatTableDataSource<ApprovalElement>(this.APP_ELEMENT_DATA);
		} else if (event == 'Rejected') {
			for (var i = 0; i < this.sampleArray.length; i++) {
				if (this.sampleArray[i].approval == false) {
					this.APP_ELEMENT_DATA.push(this.sampleArray[i]);
				}
			}
			this.dataSource1 = new MatTableDataSource<ApprovalElement>(this.APP_ELEMENT_DATA);
		}
	}
	displayedColumns1: string[] = ['assessment', 'total', 'attempted', 'correct','percentage'];
	dataSource4 = new MatTableDataSource<CompletionElement>(COMP_ELEMENT_DATA);
}



export interface PeriodicElement {
	// profilePic: string;
	empCode: number;
	empName: string;
	onlineCourse: string;
	classroomCourse: number;
	completion: number;
	points: number;
	teamPos: number;
	overallPos: number;
	grades: number;
	access: string;
}

export interface ApprovalElement {
	empCode: number;
	empName: string;
	course: string;
	startDate: string;
	approval: Boolean
}

export interface CompletionElement {
	assessment: string;
	total: number;
	attempted: number;
	correct: number;
	percentage: number;
}

export interface CoursesElement {
	courseName: string;
	activitycount: number;
	activitycomplitioncount: number;
	percent: number;
}

const COMP_ELEMENT_DATA: CompletionElement[] = [
	{ assessment: 'Quiz-1', total: 20, attempted: 15, correct: 10, percentage: 50 },
	{ assessment: 'Quiz-2', total: 20, attempted: 18, correct: 15, percentage: 75 },
	{ assessment: 'Quiz-3', total: 20, attempted: 12, correct: 10, percentage: 50 },
	{ assessment: 'Quiz-4', total: 20, attempted: 16, correct: 8, percentage: 40 },
	{ assessment: 'Quiz-5', total: 20, attempted: 20, correct: 19, percentage: 95 },
]

// const COR_ELEMENT_DATA: CoursesElement[] = [
// 	{ courseName: 'Course-1', total: 10, completed: 6, percentage: 60 },
// 	{ courseName: 'Course-3', total: 5, completed: 4, percentage: 80 },
// 	{ courseName: 'Course-11', total: 4, completed: 1, percentage: 25 },
// 	{ courseName: 'Course-14', total: 10, completed: 5, percentage: 50 },
// 	{ courseName: 'Course-15', total: 20, completed: 15, percentage: 75 },
// 	{ courseName: 'Course-16', total: 25, completed: 25, percentage: 100 },
// ]

