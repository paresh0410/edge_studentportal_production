import { Component, Output, Input, OnDestroy,OnInit,
   ChangeDetectorRef,EventEmitter,ViewChild } from '@angular/core';
//import { PlayerService, Track } from '../../@core/utils/player.service';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser'
import {DocumentViewerService} from './documentviewer.service';
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import 'rxjs/operator/toPromise';
import 'rxjs/operator/takeUntil';
// import {
//   PDFProgressData,
//   PdfViewerComponent,
//   PDFDocumentProxy,
//   PDFSource,
// } from 'ng2-pdf-viewer';
import { ENUM } from '../../service/enum';
import { MultiLanguageService } from '../../service/multi-language.service';
declare var jQuery: any;
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'ngx-documentviewer',
  templateUrl: './documentviewer.component.html',
  styleUrls: ['./documentviewer.component.scss'],
  //encapsulation: ViewEncapsulation.None
})

export class DocumentViewerComponent implements OnInit, OnDestroy {
  @Input('document') documentref :any; //HTMLVideoElement;
  @ViewChild('iframe') iframe :any; //HTMLVideoElement;
  @Input() currentActivityData: any = {};
  document:any;
  provider:any;
  documenturl:any;
  activityCompletionCalled = false;
  @Output() loadError = new EventEmitter<any>();
  @Output() callActivityCompletion = new EventEmitter();
  ViewProviders:any = {
    /**
       * Google
       */
    google: 'google',
    /**
       * Microsoft
       */
    microsoft: 'microsoft',
};
/**
 * PDF Paramters
 */
  error: any;
  page = 1;
  rotation = 0;
  zoom = 1.0;
  originalSize = true;
  pdf: any;
  renderText = true;
  // progressData: PDFProgressData;
  isLoaded = false;
  stickToPage = true;
  showAll = true;
  autoresize = false;
  fitToPage = true;
  outline: any[];
  isOutlineShown = false;
  pdfQuery = '';
  documentflag: boolean;
  isWorking: boolean;
  pdfSrc: string; // | PDFSource | ArrayBuffer;
  pdfserverlink: string;
  languageChangeSubscriber = Subscription.EMPTY;
  PdfError = {
    flag : false,
    message : "Something Went Wrong",
  };
  constructor(private documentService: DocumentViewerService,
    public multiLanguageService: MultiLanguageService,
    public cdf : ChangeDetectorRef,private sanitizer: DomSanitizer) {
    this.documentflag = false;
    this.isWorking = false;
    this.isLoaded = false;
    // this.languageChangeSubscriber = multiLanguageService.selectedactivityLanguage
    // .subscribe(selectedactivityLanguage => {
    //   //do what ever needs doing when data changes
    //   this.documentflag = false;
    //   this.isWorking = false;
    //   this.isLoaded = false;
    //   console.log('selectedLanguage', selectedactivityLanguage);
    //   if(selectedactivityLanguage){
    //     this.documentflag = false;
    //     this.isWorking = false;
    //     this.isLoaded = false;
    //     this.intializeViewerData();

    //   }
    // });
    if (window.addEventListener) {
      window.addEventListener('message', this.receiveMessage.bind(this), false);
    } else {
      (<any>window).attachEvent('onmessage', this.receiveMessage.bind(this));
    }
  }
  ngOnInit(){
    this.intializeViewerData();
    // this.documenturl = this.getDocumentUrl(); //this.getDocumentUrl();
    // this.cdf.detectChanges();
    //this.iframe.nativeElement.src = this.getDocumentSanitizeUrl();
    //     this._provider = ViewProviders.google;
    //     this.providerChange = new EventEmitter();
    //     this._originSrc = ((this.iframe.nativeElement)).src;
    window.addEventListener('message', () => {
      this.receiveMessage();
   }, false);
  }

  intializeViewerData(){
    this.documentflag = true;
    let result:any = this.documentService.getDocument();
    this.document = result.document;
    this.provider = result.provider;
    let pdflink = this.documentref || this.document; // 'https://bflillumeproduction.s3.ap-south-1.amazonaws.com/web/101 GettingStarted.pdf'; // this.documentref || this.document;
    pdflink = encodeURI(pdflink);
    this.documentService.getPdfLink(pdflink).then((result: any) => {
      if(result['type']){
        console.log('Link ===>', result);
        // this.poll = result.data;
        // this.pSum = result.data[0];
        // this.isPoll = true;

        // this.PdfError['flag'] = false;
        //   this.PdfError['message'] = "";
        this.pdfserverlink = result.data;
        this.pdfSrc = this.convertURL(this.pdfserverlink);
        // this.isLoaded = true;
      }else {
        this.loadError.emit('file_corrupt');
      }
      this.isLoaded = true;

    // console.log('document url : - ', this.pdfSrc);
    })
    .catch(result => {
      console.log('RESULT===>',result);
      this.loadError.emit('pdf_link_generate_issue');
      // this.isLoaded = true;
    });
    // this.documenturl = this.getDocumentUrl(); //this.getDocumentUrl();
    // this.cdf.detectChanges();
    //this.iframe.nativeElement.src = this.getDocumentSanitizeUrl();
    //     this._provider = ViewProviders.google;
    //     this.providerChange = new EventEmitter();
    //     this._originSrc = ((this.iframe.nativeElement)).src;

  }
  // getDocumentSanitizeUrl(){
  //   //return this.sanitizer.bypassSecurityTrustUrl(this.getDocumentUrl());
  //   return this.sanitizer.bypassSecurityTrustResourceUrl(this.getDocumentUrl());
  // }
  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
        url = cleanPath(document.baseURI + '/' + url);
    }
    return encodeURIComponent(url);
};
convertURL (url) {
  return ENUM.domain + ENUM.url.pdfviewerurl + url;
}
/*getDocumentUrl() {
  if (this.provider === this.ViewProviders.google) {
      return ('https://docs.google.com/gview?url=' +
          encodeURI(this.document) +
          '&embedded=true');
  }
  else if (this.provider === this.ViewProviders.microsoft) {
      return ('https://view.officeapps.live.com/op/embed.aspx?src=' +
          encodeURI(this.document));
  }
  return null;
};
@ViewChild(PdfViewerComponent) private pdfComponent: PdfViewerComponent;

  openLocalFile() {
    jQuery('#file').trigger('click');
  }

  toggleOutline() {
    this.isOutlineShown = !this.isOutlineShown;
  }

  incrementPage(amount: number) {
    this.page += amount;
  }

  incrementZoom(amount: number) {
    this.zoom += amount;
  }

  rotate(angle: number) {
    this.rotation += angle;
  }

  /**
   * Render PDF preview on selecting file
   */
  onFileSelected() {
    const $pdf: any = document.querySelector('#file');

    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();

      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
      };

      reader.readAsArrayBuffer($pdf.files[0]);
    }
  }
/**
 * ng2-pdf-viewer events
 */

/**
   * Get pdf information after it's loaded
   * @param pdf
   */
  // afterLoadComplete(pdf: PDFDocumentProxy) {
  //   this.pdf = pdf;
  //   this.isLoaded = true;
  //   this.loadOutline();
  // }
    /**
   * Get outline
   */
  loadOutline() {
    this.pdf.getOutline().then((outline: any[]) => {
      this.outline = outline;
    });
  }

  /**
   * Handle error callback
   *
   * @param error
   */
  onError(error: any) {
    this.error = error; // set error

    if (error.name === 'PasswordException') {
      const password = prompt(
        'This document is password protected. Enter the password:'
      );

      if (password) {
        this.error = null;
        this.setPassword(password);
      }
    } else {
      alert('Error Occured - ' + error.name);
    }
  }
  setPassword(password: string) {
    let newSrc;
    /*if (this.pdfSrc instanceof ArrayBuffer) {
      newSrc = { data: this.pdfSrc };
    } else if (typeof this.pdfSrc === 'string') {
      newSrc = { url: this.pdfSrc };
    } else {
      newSrc = { ...this.pdfSrc };
    }
    newSrc.password = password;
    this.pdfSrc = newSrc;
    */
  }/**
   * Pdf loading progress callback
   * @param {PDFProgressData} progressData
   */
  // onProgress(progressData: PDFProgressData) {
  //   console.log(progressData);
  //   this.progressData = progressData;
  //   this.isLoaded = false;
  //   this.error = null; // clear error
  // }

  getInt(value: number): number {
    return Math.round(value);
  }

  /**
   * Navigate to destination
   * @param destination
   */
  navigateTo(destination: any) {
    // this.pdfComponent.pdfLinkService.navigateTo(destination);
  }

  /**
   * Scroll view
   */
  scrollToPage() {
    // this.pdfComponent.pdfViewer.scrollPageIntoView({
    //   pageNumber: 3,
    // });
  }

  /**
   * Page rendered callback, which is called when a page is rendered (called multiple times)
   *
   * @param {CustomEvent} e
   */
  pageRendered(e: CustomEvent) {
    console.log('(page-rendered)', e);
  }

  searchQueryChanged(newQuery: string) {
    if (newQuery !== this.pdfQuery) {
      this.pdfQuery = newQuery;
      // this.pdfComponent.pdfFindController.executeCommand('find', {
      //   query: this.pdfQuery,
      //   highlightAll: true,
      // });
    } else {
      // this.pdfComponent.pdfFindController.executeCommand('findagain', {
      //   query: this.pdfQuery,
      //   highlightAll: true,
      // });
    }
  }
  loadPDFWorkerSrc() {
    window['pdfWorkerSrc'] = '../../../assets/js/pdf.worker.min.js';
  }

  ngOnDestroy() {
    this.languageChangeSubscriber.unsubscribe();
    this.documentService.deletePdfLink(this.pdfserverlink).then((result: any) => {
      console.log('Link ===>', result);
    })
    .catch(result => {
      console.log('RESULT===>',result);
    });
  }

  receiveMessage: any = (event: any) =>  {
    if(event){
      const origin = event.origin;
      if (origin === '') {
        if (event.data !== null && typeof event.data === 'object') {
          console.log('Data' + event.data['']);
          if(event.data == 'PDF_loaded'){
            this.savePDFActivityCompletionData(this.currentActivityData);
          }
        } else {
          const RefObject = JSON.parse(event.data);
          console.log(RefObject);
          if(event.data == 'PDF_loaded'){
            this.savePDFActivityCompletionData(this.currentActivityData);
          }

          // this.RefValue = RefObject.value;
        }
      }else {
        console.log('Data' + event.data);
        if(event.data == 'PDF_loaded'){
          this.savePDFActivityCompletionData(this.currentActivityData);
        }

      }
    }

  }

  savePDFActivityCompletionData(currentActivityData) {
    if(!this.activityCompletionCalled){
      this.currentActivityData['completionstatus'] = "Y"
      this.activityCompletionCalled = true;
      console.log('callActivityCompletion');
      this.callActivityCompletion.emit(currentActivityData);
    }
  }
}
