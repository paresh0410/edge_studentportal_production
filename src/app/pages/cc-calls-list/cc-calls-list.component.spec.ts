import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcCallsListComponent } from './cc-calls-list.component';

describe('CcCallsListComponent', () => {
  let component: CcCallsListComponent;
  let fixture: ComponentFixture<CcCallsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcCallsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcCallsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
