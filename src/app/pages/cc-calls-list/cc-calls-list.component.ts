import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { CallCoachingService } from '../../service/call-coaching.service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';
import { WebinarConfig } from '../../pages/webinar-multiple/webinar-entity';
import { NgForm } from '@angular/forms';
import { get } from 'https';
import * as _moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
import { Router, ActivatedRoute } from "@angular/router";
import { feature } from '../../../environments/feature-environment';

const moment = (_moment as any).default ? (_moment as any).default : _moment;
// import * as Quill from 'quill';
export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@Component({
  selector: 'ngx-cc-calls-list',
  templateUrl: './cc-calls-list.component.html',
  styleUrls: ['./cc-calls-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }]
})
export class CcCallsListComponent implements OnInit {
  employeeId: any;
  userDetails: any = [];
  @ViewChild('tab1')
  tabRef: any;
  managerName: any;
  trainerName: any;
  employeeName: any;
  contentdata: any;
  isCallContent: boolean = false;
  isdata: boolean = false;
  assess: boolean = false;
  obs: boolean = false;
  not: boolean = false;
  selectedId: any;
  fdata: any;
  calls: any;
  // frompage: any;
  employeedata: any = [];
  assessmentdata: any = [];
  obsdata: any = [];
  userList: any = [];
  editorForm: NgForm;
  usersData: any;
  voiceCall: boolean = false;
  videoCall: boolean = false;
  activityData: any = {};
  config: WebinarConfig;
  callType: string = '';
  public editorOptions = {
    placeholder: 'insert content...',
  };
  notes: any;
  selected = 0;
  // Updated Changes
  selectedTabIndex = 2 ;
  onGoingCalls: any = [];
  pastCalls: any = [];
  upcomingCalls: any = [];
  isonGoingCalls: boolean = false;
  isPastCalls: boolean = false;
  isUpcomingCalls: boolean = false;
  intialNocallsFound: boolean = false;
  customCollapsedHeight: string = '40px';
  customExpandedHeight: string = '50px';

  // Call Validation Flag
  showContent: boolean = false;
  callValidationMessage: string = '';
  currentRole: string = '';


  courseReadyFlag: boolean = false;
  courseStartData: string = '';
  courseEndData: string = '';
  // quillConfig={
  //   // toolbar: '.toolbar',
  //   toolbar: {
  //     container: [
  //       ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  //       ['code-block'],
  //       [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  //       [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  //       //[{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  //       //[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  //       //[{ 'direction': 'rtl' }],                         // text direction

  //       //[{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  //       //[{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  //       //[{ 'font': [] }],
  //       //[{ 'align': [] }],

  //       ['clean'],                                         // remove formatting button

  //       ['link'],
  //       //['link', 'image', 'video']
  //       ['emoji'],
  //     ],
  //     handlers: {'emoji': function() {}}
  //   },
  //   autoLink: true,

  //   mention: {
  //     allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
  //     mentionDenotationChars: ["@", "#"],
  //     source: (searchTerm, renderList, mentionChar) => {
  //       let values;

  //     },
  //   },
  //   "emoji-toolbar": true,
  //   "emoji-textarea": true,
  //   "emoji-shortname": true,
  //   keyboard: {
  //     bindings: {
  //       // shiftEnter: {
  //       //   key: 13,
  //       //   shiftKey: true,
  //       //   handler: (range, context) => {
  //       //     // Handle shift+enter
  //       //     console.log("shift+enter")
  //       //   }
  //       // },
  //       enter:{
  //         key:13,
  //         handler: (range, context)=>{
  //           console.log("enter");
  //           return true;
  //         }
  //       }
  //     }
  //   }
  // }
  featureConfig ;
  tenantId: any;
  userotherdetail: any;
  trainerId: any;
  step = 1;
  constructor(
    public callservice: CallCoachingService,
    public cdf: ChangeDetectorRef,
    private toastr: ToastrService,
    private router: Router,
    private routes: ActivatedRoute,
  ) {
    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
    this.tenantId = this.userDetails.tenantId;
    this.voiceCall = true;
    this.videoCall = true;

    this.userotherdetail = JSON.parse(localStorage.getItem('userOtherDetails'));
    let uDetail = this.userotherdetail[1]
    for (let i = 0; i < uDetail.length; i++) {
      if (uDetail[i].roleId == 7) {
        this.trainerId = uDetail[i].fieldmasterId;
      }
    }
    this.featureConfig = feature;
  }

  ngOnInit() {
    this.employeedata = this.callservice.employeedata;
    this.currentRole = localStorage.getItem('roleName');
    // console.log(this.frompage);
    if (this.currentRole === 'Trainer') {
      this.trainercalllist();
    }
    if (this.currentRole === 'Learner') {
      this.coachinglist();
    }
    if (this.currentRole === 'Manager') {
      this.managercalllist();
    }
    // this.feedbackdata();
  }

  submit(data) {


      console.log('Editor text', data);
      const param = {
        cId: this.calls.callId,
        rolId: 7,
        // entId: this.userDetails.referenceId,
        entId: this.trainerId,
        msg: data,
        tId: this.userDetails.tenantId,
        userId: this.userDetails.userId,
      };
      const url = ENUM.domain + ENUM.url.cc_insert_call_note;
      this.callservice.get(url, param).then(
        res => {
          console.log('notes', res['data']);
          if (res['type'] === true) {
            try {
              console.log('note added.');
              this.toastr.success(
                'Successful',
                'Note added.',
              ).onHidden.subscribe(()=>
              this.toastr.clear());
              this.getNote(this.calls);
            } catch { }
          }
        },
        err => {
          console.log(err);
          console.log('note added.');
          this.toastr.success(
            'Please try again after sometime',
            'Error Occured!',
          ).onHidden.subscribe(()=>
          this.toastr.clear());
        },
      );


  }

  trainercalllist() {
    const url = ENUM.domain + ENUM.url.cc_trainer_call;
    const param = {
      trainId: this.trainerId,
      empId: this.employeedata.empId,
      tId: this.userDetails.tenantId,
      lmt: null,
      pNo: null,
      reqFrom: 'WEB',
      callType: null,
    };
    this.callservice.get(url, param).then(
      res => {
        console.log(res);
        if (res['type'] === true) {
          try {
            this.prepareCallList(res['data']);
            // this.calllist = res['data'];
            // this.managerName = this.calllist[0].managerName;
            // this.trainerName = this.calllist[0].trainerName;
            // this.employeeName = this.calllist[0].employeeName;
            // this.contenttdetail(this.calllist[0]);
            // this.feedbackdata(this.calllist[0]);
          } catch { }
        }
      },
      err => {
        console.log(err);
      },
    );
  }
  coachinglist() {
    const url = ENUM.domain + ENUM.url.get_nominee_list;
    const param = {
      empId: this.employeeId,
      tId: this.userDetails.tenantId,
      lmt: null,
      pNo: null,
      reqFrom: 'WEB',
      callType: null,
    };
    this.callservice.get(url, param).then(
      res => {
        console.log(res);
        if (res['type'] === true) {
          try {
            this.prepareCallList(res['data']);
            // this.calllist = res['data'];
            // this.managerName = this.calllist[0].managerName;
            // this.trainerName = this.calllist[0].trainerName;
            // this.employeeName = this.calllist[0].employeeName;
            // this.contenttdetail(this.calllist[0]);
            // this.feedbackdata(this.calllist[0]);
          } catch { }
        }
      },
      err => {
        console.log(err);
      },
    );
  }
  managercalllist() {
    const url = ENUM.domain + ENUM.url.cc_manager_call;
    const param = {
      managerId: this.employeeId,
      empId: this.employeedata.empId,
      // managerId: 5,
      // empId: 8,
      tId: this.userDetails.tenantId,
      lmt: null,
      pNo: null,
      reqFrom: 'WEB',
      callType: null,
    };
    console.log('managerParam:', param);
    this.callservice.get(url, param).then(
      res => {
        console.log(res);
        if (res['type'] === true) {
          try {
            this.prepareCallList(res['data']);

            // this.calllist = res['data'];
            // console.log('Call list ', this.calllist);
            // this.managerName = this.calllist[0].managerName;
            // this.trainerName = this.calllist[0].trainerName;
            // this.employeeName = this.calllist[0].employeeName;
            // this.contenttdetail(this.calllist[0]);
            // this.feedbackdata(this.calllist[0]);
          } catch { }
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  setdata(data) {
    this.managerName = data.managerName;
    this.trainerName = data.trainerName;
    this.employeeName = data.employeeName;
  }
  content(data) {
    console.log(data);
    if (data.length) {
      this.intialNocallsFound = false;
      this.contenttdetail(data[0], true);
    } else {
      this.intialNocallsFound = true;
      this.selectedId = '';
    }

  }
  contenttdetail(calldetail, slidechanged) {

    if(this.tabRef && slidechanged){
      this.tabRef.selectedIndex = 0;
    }



    console.log("Calldetail", calldetail);
    this.intialNocallsFound = false;
    this.isdata = false;
    this.calls = calldetail;
    // if(calldetail.isEnableC ){
    //   this.contentVisible = true;
    // }
    // this.contentVisible = calldetail.isEnableC;
    if(this.calls.flag === 3){
      this.callType = 'Future';
    }else if(this.calls.flag == 2){
      this.callType = 'OnGoing';
    }else{
      this.callType = 'Past';
    }
    this.courseStartData = calldetail.callStartDate;
    this.courseEndData = calldetail.callEndDate;
    this.feedbackdata(this.calls);
    this.getCallUserList(this.calls);

    if (this.userDetails.roleId === 7) {
      this.assessment(this.calls);

    }
    this.getNote(this.calls);
    if (this.userDetails.roleId === 5) {
      this.observationque(this.calls);
    }

    this.isCallContent = false;
    this.selectedId = calldetail.callId;
    this.setdata(calldetail);
    // this.managerName = calldetail.managerName;
    // this.trainerName = calldetail.trainerName;
    // this.employeeName = calldetail.employeeName;
    console.log(calldetail);
    this.contentdata = [];
    const data = {
      callId: calldetail.callId,
      tId: this.userDetails.tenantId,
    };
    console.log('con data', data);
    const url = ENUM.domain + ENUM.url.cc_course_module;

    this.callservice.get(url, data).then(
      res => {
        console.log(res);
        if (res['type'] === true) {
          try {
            this.contentdata = res['data'];
            console.log('this.contentdata', this.contentdata);
            this.isCallContent = true;
          } catch { }
        }
      },
      err => {
        console.log(err);
      },
    );

  }
  feedbackdata(data) {
    this.isdata = false;
    // const entityId123 =
    //   this.userDetails.roleId === 7
    //     ? this.userDetails.referenceId
    //     : this.employeeId;

    const entityId123 =
      this.userDetails.roleId === 7
        ? this.trainerId
        : this.employeeId;
    const data1 = {
      tId: this.userDetails.tenantId,
      roleId: this.userDetails.roleId,
      entityId: entityId123,
      callId: data.callId,
    };

    console.log('data1', data1);
    const url = ENUM.domain + ENUM.url.cc_get_feedback;

    this.callservice.get(url, data1).then(
      res => {
        console.log(res);
        if (res['type'] === true) {
          try {
            this.fdata = res['data'];
            this.isdata = true;
          } catch (e) {
            console.log(e);
          }
        }
      },
      err => {
        console.log(err);
      },
    );
  }

  assessment(data) {
    this.assess = false;
    const param = {
      tId: this.userDetails.tenantId,
      callId: data.callId,
    };
    const url = ENUM.domain + ENUM.url.cc_assement;
    this.callservice.get(url, param).then(
      res => {
        console.log(res['data']);
        if (res['type'] === true) {
          try {
            this.assessmentdata = res['data'];
            this.assess = true;
          } catch { }
        }
      },
      err => {
        console.log(err);
      },
    );
  }

  getNote(data) {
    // this.not = false;
    const param = {
      cId: data.callId,
      tId: this.userDetails.tenantId,
    };
    const url = ENUM.domain + ENUM.url.cc_get_call_note;
    this.callservice.get(url, param).then(
      res => {
        console.log('notes', res['data']);
        if (res['type'] === true) {
          try {
            if (res['data']) {
              this.notes = res['data'][0].note;
            }

            // this.not = true;
          } catch { }
        }
      },
      err => {
        console.log(err);
      },
    );
  }
  observationque(data) {
    this.obs = false;
    const param = {
      tId: this.userDetails.tenantId,
      callId: data.callId,
    };
    const url = ENUM.domain + ENUM.url.cc_observation;
    this.callservice.get(url, param).then(
      res => {
        console.log(res['data']);
        if (res['type'] === true) {
          try {
            this.obsdata = res['data'][0];
            console.log('this.obsdata', this.obsdata);
            this.obs = true;
          } catch (e) {
            console.log(e);
          }
        }
      },
      err => {
        console.log(err);
      },
    );
  }
  // Old Back
  // back() {
  //   window.history.back();
  // }

  // New Code
  back() {
    if (this.currentRole === 'Trainer') {
      this.router.navigate(['../train-nomination'], { relativeTo: this.routes });
    }
    if (this.currentRole === 'Learner') {
      this.router.navigate(['../dashboard'], { relativeTo: this.routes });
    }
    if (this.currentRole === 'Manager') {
      this.router.navigate(['../team-dashboard'], { relativeTo: this.routes });
    }
  }

  getCallUserList(data) {
    this.courseReadyFlag = false;
    const params = {
      callId: data.callId,
      roleId: this.userDetails.roleId,
      tId: this.userDetails.tenantId,
    };
    const url = ENUM.domain + ENUM.url.cc_get_userlist;
    this.callservice
      .get(url, params)
      .then((result: any) => {
        console.log('DATA--->', result);
        if (result.type === true) {
          if (result.data.length > 0) {
            this.usersData = result['data'][0];
            this.userList = result['data'];
            this.usersData.courseId = data.callId;
            this.activityData = {
              activityName: 'Call Id ' + ' ' + data.callId,
              instanceId: data.callId,
            };
            console.log(this.activityData);
            this.config = {
              voice: this.voiceCall,
              video: this.videoCall,
              creator: this.userDetails.roleId === 7 ? true : false,
              areaId: 21,
              instanceId: this.activityData.instanceId,
              tenantId: this.userDetails.tenantId,
              type: null,
              recording: true,
              allowCalls: data.flag === 1 ? false : true,
              startDate: data.callStartDate,
              EndDate: data.callEndDate,
            };

            console.log('Config ==>', this.config);
            this.courseReadyFlag = true;
          } else {
            // this.goBack();
            this.toastr.warning(
              'No user available for this call',
              'Error Occured!',
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        } else {
          // this.toastr.error(
          //   'Please try again after sometime',
          //   'Error Occured!',
          // );
      this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
      this.toastr.clear());

        }
      })
      .catch(result => {
        console.log('ServerResponseError :', result);
      });
  }
  prepareCallList(data) {
    console.log(data);
    if (data) {
      this.onGoingCalls = data.ongoing;
      if (this.onGoingCalls) {
        this.isonGoingCalls = true;
      }
      this.upcomingCalls = data.upcoming;
      if (this.upcomingCalls) {
        this.isUpcomingCalls = true;
      }
      this.pastCalls = data.past;
      if (this.pastCalls) {
        this.isPastCalls = true;
      }
      this.content(this.onGoingCalls);
    }
    // this.calllist = data;
    // console.log('Call List Data ===>', this.calllist);
    // for (let i = 0; i < this.calllist.length; i++) {
    //   this.calllist[i].calldatetime = new Date(
    //     this.calllist[i].calldatetime.toLocaleString()
    //   );
    // }

    // if (data) {
    //   if (data.ongoing.length != 0) {
    //     this.onGoingCalls = data.ongoing;
    //     this.setdata(this.onGoingCalls[0]);
    //     // this.managerName = this.onGoingCalls[0].managerName;
    //     // this.trainerName = this.onGoingCalls[0].trainerName;
    //     // this.employeeName = this.onGoingCalls[0].employeeName;
    //     this.contenttdetail(this.onGoingCalls[0]);
    //     this.isonGoingCalls = true;

    //   } else {
    //     this.intialNocallsFound = true;
    //   }
    //   if (data.upcoming.length != 0) {
    //     this.upcomingCalls = data.upcoming;
    //     this.isUpcomingCalls = true;
    //   }
    //   if (data.past.length != 0) {
    //     this.pastCalls = data.past;
    //     this.isPastCalls = true;
    //   }

    // }


  }
  changeCount(event) {
    console.log(event);
    this.contenttdetail(this.calls, false);
  }

  // Tabs Changes

  setStep(index: number) {
    if (index) {
      this.step = index;
      if (this.step == 1) {
        this.content(this.onGoingCalls);
      } if (this.step == 2) {
        this.content(this.upcomingCalls);
      } if (this.step == 3) {
        this.content(this.pastCalls);
      }
    }
  }

  onTabChanged(data){
    console.log('data ===>',data);
    if(data){
      switch(data.tab.textLabel){
        case 'Contact': this.getUpdatedCallStatus('webinar');
                        break;
        case 'Feedback': this.getUpdatedCallStatus('feedback');
                        break;
        case 'Assessment': this.getUpdatedCallStatus('assessment');
                        break;
        case 'Notepad':this.getUpdatedCallStatus('notepad');
                          break;
      }
    }
  }

  getUpdatedCallStatus(reqFromTab){
    const param = {
      reqFrom: reqFromTab,
      iId: this.calls.callId,
      flag: this.calls.flag,
    };
    const url = ENUM.domain + ENUM.url.cc_get_updated_call_status;
    this.callservice.get(url, param).then(
      res => {
        console.log(res['data']);
        if (res['type'] === true && res['data'].length !== 0 ) {
          try {
              if (reqFromTab == 'webinar'){
                if (this.calls.flag === 1) {
                  this.config.allowCalls = false;
                  this.showContent =  true ;
                  // this.callValidationMessage = res['data'][0].msg;
                } else {
                  this.showContent = res['data'][0].isEnable === 0 ? false : true ;
                  this.callValidationMessage = res['data'][0].msg;
                }
              }else{
                this.showContent = res['data'][0].isEnable === 0 ? false : true ;
                this.callValidationMessage = res['data'][0].msg;
              }

          } catch (e) {
            console.log(e);
          }
        }
      },
      err => {
        console.log(err);
      },
    );
  }
}
