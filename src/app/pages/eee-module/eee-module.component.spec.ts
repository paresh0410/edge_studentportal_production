import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EeeModuleComponent } from './eee-module.component';

describe('EeeModuleComponent', () => {
  let component: EeeModuleComponent;
  let fixture: ComponentFixture<EeeModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EeeModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EeeModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
