import { Component, OnInit, ViewEncapsulation, Input, ChangeDetectorRef } from '@angular/core';
import { ENUM } from '../../service/enum';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from "@angular/forms";
import { EEPService } from '../../service/eep.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CourseServiceProvider } from '../../service/course-service';
import { CourseDetailServiceProvider } from '../../service/course-detail-service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { LearnServiceProvider } from '../../service/learn-service';
@Component({
  selector: 'ngx-eee-module',
  templateUrl: './eee-module.component.html',
  styleUrls: ['./eee-module.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EeeModuleComponent implements OnInit {
  @Input('step') step1;
  disabledGoToCourse = false;
  nomineeModules: any = [];
  ActiveTab: any;
  password = '';
  resetPwdForm: FormGroup;
  nomineeContent: any = [];
  userDetails: any = [];
  stepsdata: any = [];
  currentstepdata: any = [];
  courseDetailSummary: any = [];
  checkResponse: any;
  displayInstructions: boolean = false;
  passKey: string;
  workflowdetail: any = [];
  passWrong: boolean = false;
  passCheck: boolean = false;
  submitted: boolean = false;
  employeeId: any = [];
  constructor(public eep: EEPService,
    public spinner: NgxSpinnerService,
    public CSP: CourseServiceProvider,
    public CDSP: CourseDetailServiceProvider,
    public toastr: ToastrService,
    private router: Router,
    private routes: ActivatedRoute,
    private cdf: ChangeDetectorRef,
    public LSP: LearnServiceProvider,
    private formBuilder: FormBuilder) {
    this.workflowdetail = this.eep.workflow;
    console.log(this.workflowdetail);
    // this.ActiveTab = this.nomineeModules[0].id;
    // this.nomineeContent = this.nomineeModules[0].list;
    this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
  }

  ngOnInit() {
    this.resetPwdForm = this.formBuilder.group({
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
    this.currentstepdata = this.step1;
    console.log(this.currentstepdata);
    this.content();
  }
  content() {
    const data = {
      wfId: this.workflowdetail.id,
      actId: this.currentstepdata.activityId,
      tId: this.userDetails.tenantId,
      empId: this.employeeId,
      enId: this.currentstepdata.enId,
    };
    this.spinner.show();
    console.log(data);
    const url = ENUM.domain + ENUM.url.eep_getmodules;
    this.eep.get(url, data).then(res => {
      console.log(res);
      this.spinner.hide();
      try {
        this.nomineeModules = res['data'];
        this.moduledetail(this.nomineeModules);
      } catch (e) {
        console.log(e);
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  moduledetail(nominee) {
    if (this.nomineeModules.length > 0) {
      for (var i = 0; i < this.nomineeModules.length; i++) {
        var temp: any = i + 1;
        this.nomineeModules[i].id = i;
        // this.nomineeModules[i].moduleName = 'Module' + ' ' + temp;
      }
      this.ActiveTab = this.nomineeModules[0].id;
      this.nomineeContent = this.nomineeModules[0].list;
      try {
        for (var i = 0; i < this.nomineeContent.length; i++) {
          var temp: any = i + 1;
          this.nomineeContent[i].img =
            "assets/images/activity_image/quiz.jpg";
          // this.nomineeContent[i].activityName = 'Activity' + ' ' + temp;
          this.nomineeContent[i].activityTempId = temp;
          var tempCourseI = this.nomineeContent[i];
          var tempDate = new Date(this.nomineeContent[i].completed);
          for (var j = 0; j < i; j++) {
            var tempCourseJ = this.nomineeContent[j];
            if (tempCourseI.dependentActId != 0) {
              if (tempCourseI.dependentActId == tempCourseJ.activityId) {
                if (tempCourseJ.wCompleted == 0) {
                  tempCourseI.dependant = tempCourseJ.activityName;
                  // break;
                } else {
                  tempCourseI.dependant = null;
                }
              } else {
                tempCourseI.dependant = null;
              }
            } else {
              tempCourseI.dependant = null;
            }
          }
        }
      } catch (e) {
        console.log(e);
      }
      console.log("CD===>", this.nomineeContent);
    }
  }

  goToCourse(data) {
    this.CSP.setCourse(this.courseDetailSummary, data);
    console.log("DATA---->", data);
    // if (data.activityTypeId == 5) {
    const userDetails = JSON.parse(localStorage.getItem("userDetails"));
    const employeeId = localStorage.getItem('employeeId');
    var param = {
      qId: data.quizId,
      tId: userDetails.tenantId,
      uId: employeeId,
      cId: data.courseId,
      mId: data.moduleId,
      aId: data.activityId,
      enId : data.enrolId,
    };
    var url = ENUM.domain + ENUM.url.checkQuizUser;
    this.CDSP.getCourseDetailsData(url, param)
      .then(result => {
        var temp: any = result;
        if (temp.data !== null || temp.data !== undefined) {
          if (temp.data.length > 0) {
            console.log("CHECk QUIZ FOR USER---->", temp.data[0]);
            this.checkResponse = temp.data[0];
            if (temp.data[0].flag == "true") {
              console.log("PassCheck===>", temp.data[0]);
              if (temp.data[0].qPassword == 0) {
                if (this.checkResponse.instructions) {
                  this.displayInstructions = true;
                } else {
                  this.displayInstructions = false;
                  const courseDetailSummary = {
                    courseFrom: 2,
                  };
                  this.CSP.setCourse(courseDetailSummary, data);
                  this.router.navigate(["quiz"], { relativeTo: this.routes });
                }
              } else {
                this.passCheck = true;
                this.passKey = temp.data[0].passWord;
              }
            } else {
              let message = temp.data[0].msg + ' ' + temp.data[0].nextAttemptDate;
              this.toastr.warning(message, "Warning!").onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            // this.toastr.error(
            //   "Something went wrong please try again later",
            //   "Server Error!"
            // );
            this.toastr.warning('No Data available', 'Warning!');
          }
        } else {
          // this.toastr.error(
          //   "Something went wrong please try again later",
          //   "Server Error!"
          // );
          this.toastr.warning('Something went wrong please try again later', 'Warning!');
        }
      })
      .catch(result => {
        console.log("ServerResponseError :", result);
        // this.toastr.error(
        //   "Something went wrong please try again later",
        //   "Server Error!"
        // );
        this.toastr.warning('Something went wrong please try again later', 'Warning!');
        // this.spinner.hide();
      });
  }
  close() {
    this.passCheck = false;
    this.displayInstructions = false;
    this.cdf.detectChanges();
  }

  onSubmit() {
    this.submitted = true;
    if (this.resetPwdForm.invalid) {
      return;
    } else {
      console.log("EnteredPassword--->", this.resetPwdForm.value.password);
      console.log("PasswordKey--->", this.passKey);
      if (this.resetPwdForm.value.password == this.passKey) {
        this.passCheck = false;
        if (this.checkResponse.instructions) {
          this.displayInstructions = true;
        } else {
          this.displayInstructions = false;
          this.router.navigate(["quiz"], { relativeTo: this.routes });
        }
      } else {
        this.passWrong = true;
      }
    }
  }
  onSubmit1() {
    this.router.navigate(["quiz"], { relativeTo: this.routes });
  }
  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToCourse(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}
}
