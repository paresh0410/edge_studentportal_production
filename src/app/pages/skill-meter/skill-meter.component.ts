import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-skill-meter',
  templateUrl: './skill-meter.component.html',
  styleUrls: ['./skill-meter.component.scss']
})
export class SkillMeterComponent implements OnInit {

  skills=[
    {skillName: "literature", percentage: 54},
    {skillName: "Graphic Designer", percentage: 80},
    {skillName: "Marketing", percentage: 25},
    {skillName: "Category 1", percentage: 40},
    {skillName: "Category 2", percentage: 92},
    {skillName: "Category 3", percentage: 6},

  ];

  constructor() { }

  ngOnInit() {
  }

}
