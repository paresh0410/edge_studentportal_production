import { NgModule , ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';

import { CommonModule } from '@angular/common';

// import { SkillMeterComponent } from './skill-meter.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    // SkillMeterComponent
  ],
  providers:[
     
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA 
  ]
})
export class SkillMeterModule {
}
