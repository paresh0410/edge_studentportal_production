import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonComponentSharingModule } from '../common-component-sharing/common-component-sharing.module';
import { LazyModule } from '../lazy/lazy.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../material-module';

import { DynamicLinkRoutingModule } from './dynamic-link-routing.module';

import { ImageViewerModuleCommonModule } from '../common-component-sharing/image-viewer-module-common.module';
import { DynamicLinkComponentComponent } from './dynamic-link-component/dynamic-link-component.component';

import { ComponentModule } from '../../component/component.module';

@NgModule({
  declarations: [DynamicLinkComponentComponent],
  imports: [
    CommonModule,
    DynamicLinkRoutingModule,
    CommonComponentSharingModule,
    LazyModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    ImageViewerModuleCommonModule.forRoot(),
    ComponentModule
  ]
})
export class DynamicLinkModule { }
