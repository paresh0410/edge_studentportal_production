import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicLinkComponentComponent } from './dynamic-link-component.component';

describe('DynamicLinkComponentComponent', () => {
  let component: DynamicLinkComponentComponent;
  let fixture: ComponentFixture<DynamicLinkComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicLinkComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicLinkComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
