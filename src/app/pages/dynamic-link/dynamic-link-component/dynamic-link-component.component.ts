import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ENUM } from '../../../service/enum';
import { CourseServiceProvider } from '../../../service/course-service';
import { CourseDetailServiceProvider } from '../../../service/course-detail-service';
import { LearnServiceProvider } from '../../../service/learn-service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { feature } from '../../../../environments/feature-environment';
import { catalogConfig, openConfig, wishConfig, courseConfig, pathwayConfig } from '../../../models/config';

@Component({
  selector: 'ngx-dynamic-link-component',
  templateUrl: './dynamic-link-component.component.html',
  styleUrls: ['./dynamic-link-component.component.scss']
})
export class DynamicLinkComponentComponent implements OnInit {

  currentUserData: any;
  queryParams: any;
  dataArray: any;
  flagData: any ;
  resetPwdForm: FormGroup;
  currentCourseData: any;
  activityModuleList = {
    'showPassedList' : false,
    'listData': [],
    'currentCourseDetails' : null,
  };
  featureConfig;

  courseCardConfig = courseConfig;
  pathwayCardConfig = pathwayConfig;
  openConfig = openConfig;
  constructor(private activatedRoute: ActivatedRoute, private route: Router,private spinner: NgxSpinnerService,
    public CSP: CourseServiceProvider, private router: Router,  private toastr: ToastrService,
    public CDSP: CourseDetailServiceProvider, public LSP: LearnServiceProvider,
    private formBuilder: FormBuilder) {
    this.featureConfig = feature;
    this.currentUserData = JSON.parse(localStorage.getItem('userdetail'));
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params =>{
      console.log('Nav params ===>', params);
      this.queryParams = params;
      this.fetchDynamicLinkData();
    } );
    this.resetPwdForm = this.formBuilder.group({
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }
  title = 'components-demo';
  demoCard = [
    {
      id: 404,
      earnpoints: 0,
      tags: 'ba',
      cpoints: 1,
      cat_id: 231,
      category: 'TTT 13_9',
      courseTitle: 'Observal Batch 13_9(TTT)',
      courseDesc: 'This batch is for testing ',
      startDate: '0000-00-00 00:00:00',
      endDate: 'NaN  NaN',
      approvalStatus: null,
      approverId: null,
      courseType: 'MANDATORY',
      typeImage: 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BannersCourses%28360x200%29-4.jpg',
      completed_courses: 0,
      total_courses: 2,
      percComplete: 0,
      courseType2: 'Classroom',
      enrolId: 688352,
      noOfmodules: 1,
      compStatus: 'CURRENT',
      nextStage: '',
      noLikes: 0,
      noDislikes: 0,
      isLike: 0,
      isDislike: 0,
      isBookmark: 0,
      workflowid: '',
      wname: '',
      wf_stdate: '',
      isWorkFlow: 0,
      perComplete: 0,
      workflowEarnedPoints: 0,
      workflowTotalPoints: 0,
      wf_enddate: '',
      ttt_workflowId: '',
      courseMonth: '01 undefined NaN',
      courseDate1: 'undefined NaN',
      courseDate: 'NaN  NaN',
      completed: '00 / 02',
      type1Image: 'assets/images/orangeLearn.svg',
      type2Image: 'assets/images/classroom.svg',
    },
    {
      id: 405,
      earnpoints: 0,
      tags: 'ba',
      cpoints: 1,
      cat_id: 231,
      category: 'TTT 13_9',
      courseTitle: 'Observal 123',
      courseDesc: 'Observal 123',
      startDate: '0000-00-00 00:00:00',
      endDate: 'NaN  NaN',
      approvalStatus: null,
      approverId: null,
      courseType: 'MANDATORY',
      typeImage: 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BannersCourses%28360x200%29-4.jpg',
      completed_courses: 0,
      total_courses: 2,
      percComplete: 0,
      courseType2: 'Classroom',
      enrolId: 688352,
      noOfmodules: 1,
      compStatus: 'CURRENT',
      nextStage: '',
      noLikes: 0,
      noDislikes: 0,
      isLike: 0,
      isDislike: 0,
      isBookmark: 0,
      workflowid: '',
      wname: '',
      wf_stdate: '',
      isWorkFlow: 0,
      perComplete: 0,
      workflowEarnedPoints: 0,
      workflowTotalPoints: 0,
      wf_enddate: '',
      ttt_workflowId: '',
      courseMonth: '01 undefined NaN',
      courseDate1: 'undefined NaN',
      courseDate: 'NaN  NaN',
      completed: '00 / 02',
      type1Image: 'assets/images/orangeLearn.svg',
      type2Image: 'assets/images/classroom.svg',
    },
    {
      id: 69,
      earnpoints: 126,
      tags: 'abc',
      cpoints: 110,
      cat_id: 11,
      category: 'test aditya',
      courseTitle: 'demo test bhut 1243',
      courseDesc: 'dskgjsdkb kfsd gkdfj',
      startDate: '2019-05-24 14:32:34',
      endDate: '01 Jan 1970',
      approvalStatus: null,
      approverId: null,
      courseType: 'OPEN',
      typeImage: 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BannersCourses%28360x200%29-4.jpg',
      completed_courses: 1,
      total_courses: 1,
      percComplete: 100,
      courseType2: 'Online',
      enrolId: 757,
      noOfmodules: 1,
      compStatus: 'COMPLETED',
      nextStage: '',
      noLikes: 0,
      noDislikes: 0,
      isLike: 0,
      isDislike: 0,
      isBookmark: 0,
      workflowid: '',
      wname: '',
      wf_stdate: '',
      isWorkFlow: 0,
      perComplete: 0,
      workflowEarnedPoints: 0,
      workflowTotalPoints: 0,
      wf_enddate: '',
      ttt_workflowId: '',
      courseMonth: '01 May 2019',
      courseMon: 'May',
      courseDate1: 'May 24',
      courseDate: 'NaN  NaN',
      completed: '01 / 01',
      type1Image: 'assets/images/orangeLearn.svg',
      type2Image: 'assets/images/online.svg',
    },
    {
      id: 69,
      earnpoints: 126,
      tags: 'abc',
      cpoints: 110,
      cat_id: 11,
      category: 'test aditya 123',
      courseTitle: 'demo test bhut 123432432',
      courseDesc: 'dskgjsdkb kfsd gkdfj',
      startDate: '2019-05-24 14:32:34',
      endDate: '01 Jan 1970',
      approvalStatus: 'Pending',
      approverId: null,
      courseType: 'OPEN',
      typeImage: 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/BannersCourses%28360x200%29-4.jpg',
      completed_courses: 1,
      total_courses: 1,
      percComplete: 100,
      courseType2: 'Online',
      enrolId: 757,
      noOfmodules: 1,
      compStatus: 'OPEN',
      nextStage: '',
      noLikes: 0,
      noDislikes: 0,
      isLike: 0,
      isDislike: 0,
      isBookmark: 0,
      workflowid: '',
      wname: '',
      wf_stdate: '',
      isWorkFlow: 0,
      perComplete: 0,
      workflowEarnedPoints: 0,
      workflowTotalPoints: 0,
      wf_enddate: '',
      ttt_workflowId: '',
      courseMonth: '01 May 2019',
      courseMon: 'May',
      courseDate1: 'May 24',
      courseDate: 'NaN  NaN',
      completed: '01 / 01',
      type1Image: 'assets/images/orangeLearn.svg',
      type2Image: 'assets/images/online.svg',
    },
    {
      id: 0,
      earnpoints: 0,
      tags: '',
      cpoints: 0,
      cat_id: 0,
      category: '',
      courseTitle: 'Done workflow testing_1',
      courseDesc: 'Done workflow testing_1 will show courses',
      startDate: '2019-08-14',
      endDate: 'NaN  NaN',
      approvalStatus: '',
      approverId: 0,
      courseType: 'MANDATORY',
      typeImage: null,
      completed_courses: 0,
      total_courses: 2,
      percComplete: 0,
      courseType2: 'Online',
      enrolId: 0,
      noOfmodules: 0,
      compStatus: 'COMPLETED',
      nextStage: '',
      noLikes: 0,
      noDislikes: 0,
      isLike: 0,
      isDislike: 0,
      isBookmark: 0,
      workflowid: '105',
      wname: 'Done workflow testing_1',
      wf_stdate: '2019-08-14',
      isWorkFlow: 1,
      perComplete: 100,
      workflowEarnedPoints: 4,
      workflowTotalPoints: 4,
      wf_enddate: '2019-09-03',
      ttt_workflowId: '',
      courseMonth: '01 August 2019',
      courseMon: 'August',
      courseDate1: 'August 14',
      courseDate: 'NaN  NaN',
      completed: '00 / 02',
      type1Image: 'assets/images/blue-cog-hi.png',
      type2Image: 'assets/images/online.svg',
    }
  ];
  activity = {
  moduleId: 489,
courseId: 404,
courseName: 'Observal Batch 13_9(TTT)',
cDescription: 'This batch is for testing ',
modulename: 'Home',
mStartDate: '10-Dec-2019 08:22 AM',
mEndDate: '31-Dec-2019 08:22 AM',
isDisable: 'N',
mSummary: 'This session is for testing ',
visible: 1,
reference: null,
referenceType: null,
mimeType: null,
formatId: null,
activityTitle: 'Activity 1221',
activityName: 'Feedback',
activityNo: 1221,
summary: 'This feedback is for the learner',
dependentActId: 0,
dependentActId1: 0,
activityDuration: '00:30:00.0',
modulePic: 'assets/images/course1.jpg',
courseOrder: 0,
tenantId: 1,
activityId: 1221,
supertypeId: 2,
activityTypeId: 6,
quizId: null,
feedbackId: 22,
activity_type: 'Feedback',
usermodified: 1,
points: null,
creditAllocId: null,
enrolId: 688352,
qpassword: 'not required',
wCompleted: 1,
completedDate: '2020-02-12T00:00:00.000Z',
byLearner: 0,
byTrainer: 0,
dependentActName: null,
dependentModId: null,
dependentModName: null,
dependentActCompStatus: 0,
fileinfo: '',
  };
  disabledActivityFlag = false;
  activityCardList = [];

  fetchDynamicLinkData(){
    const url = ENUM.domain + ENUM.url.getDynamicLinkData;
    const param = {
      empId: this.currentUserData.referenceId,
      userId: this.currentUserData.userId,
      areaId: this.queryParams.areaId,
      instanceId: this.queryParams.instanceId,
      roleId: this.currentUserData.roleId,
      tenantId: this.currentUserData.tenantId,
  };
    this.CSP.getCourseData(url, param).then((result: any) => {
      console.log('ServerResponseError :', result);
      if(result['data'] ){
        this.dataArray = result['data'];
        // if(result['msg'] && result['msg'].length !== 0){
        //   this.flagData = result['msg'][0];
        // }
        this.flagData = result['msg'];
        // this.flagData = result['']
        console.log('Card Data ==>', this.dataArray );
        console.log('Card Data ==>', this.flagData );
        if(this.queryParams['indentifier'] !== 'activity'){
          this.prepareCardData();
          this.makeCourseDataReadyForWF(result['data']);
        }else {
          if (result['data'].length !== 0){
            // this.tabChanged(result['data'][0]);
            const tempModRes = result['data'];

            // this.modulesList = [];
            if (tempModRes.length > 0 && tempModRes[0].list.length > 0) {
                this.currentCourseData = this.makeCourseDataReady(tempModRes[0].list[0]);
                // this.moduleService.courseData = this.currentCourseData;
                // this.makeModuleDataReady(tempModRes);
                this.activityModuleList.showPassedList = true;
                this.activityModuleList.listData = result['data'];
                this.activityModuleList.currentCourseDetails = this.makeCourseDataReady(tempModRes[0].list[0]);
            }

          }
        }
      }
      this.spinner.hide();
    })
    .catch(result => {
      this.spinner.hide();
      // this.courseDetail = null;
      console.log('ServerResponseError :', result);
    });
  }



  prepareCardData(){
    for (let index = 0; index < this.dataArray.length; index++) {
      if (this.dataArray[index].workflowid) {
        this.dataArray[index].type1Image = 'assets/images/blue-cog-hi.png';
      } else if (this.dataArray[index].courseType == 'MANDATORY') {
        this.dataArray[index].type1Image = 'assets/images/orangeLearn.svg';
      } else if (this.dataArray[index].courseType == 'RECOMMENDED') {
        this.dataArray[index].type1Image = 'assets/images/mountLearn.svg';
      } else {
        // if(this.dataArray[index].courseType == 'ASPIRATIONAL'){
          this.dataArray[index].type1Image = 'assets/images/tickLearn.svg';
      }
      if (this.dataArray[index].courseType2 == 'Online') {
        this.dataArray[index].type2Image = 'assets/images/online.svg';
      } else if (this.dataArray[index].courseType2 == 'Classroom') {
        this.dataArray[index].type2Image = 'assets/images/classroom.svg';
      }
      if (this.dataArray[index].courseType2 == 'Preonboarding') {
        this.dataArray[index].type2Image = 'assets/images/online.svg';
      }

    }

  }
  preformAction(event){
      if (event) {
        switch(event.type){
          // case 'goToCourse': this.goToCourseDetails(event.cardData);
          //                    break;
          case 'enrolSelf': this.enrolSelf(event.cardData);
                            break;
         case 'goToOpenCourse': this.goToOpenCourseDetails(event.cardData);
                            break;
          case 'likeDislike': this.likeDislike(event.cardData, event.liked);
                              break;
          // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
          //                     break;
          case 'goToCourse': this.navigateToCourseOrWorkflow(event.cardData);
          break;
        }
      }
  }

  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToActivityDetails(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}

navigateToCourseOrWorkflow(currentCourse){
  if (currentCourse.isWorkFlow == '1') {
    const wfId = currentCourse.workflowid;
    // this.router.navigate(['../pathway/details/' + wfId], { relativeTo: this.routes });
    this.router.navigate(['pages/learning/pathway/details/' + wfId]);
  } else {
      // this.goToCourse(this.currentSelectedMenu, currentCourse);
      this.goToCourseDetails(currentCourse);
  }
}

  goToCourseDetails(data){
    // console.log('DATA--->', passData);
    // console.log('Index--->', index);
   this.LSP.setcourseDetailList(data);
   if(this.featureConfig.activity.modernui){
    this.router.navigate(['../pages/module-activity'])
   }
   if(this.featureConfig.activity.standardui){
    this.router.navigate(['../pages/learn/course-detail']);
   }
  //  this.router.navigate(['../pages/learn/course-detail']);

  //  this.router.navigate(['../../../../learn/course-detail'], { relativeTo: this.activatedRoute });
  }

  goToOpenCourseDetails(currentCourse) {
    const courseId = currentCourse.id;
    this.router.navigate(['../pages/learning/self/course/' + courseId]);
  }
  enrolSelf(data){
    const params = {
      enrolId: data.enrolId,
      ttt_workflowId: data.ttt_workflowId,
    };
    const url = ENUM.domain + ENUM.url.selfEnrol;
    this.LSP.selfEnrolCourse(url, params)
      .then((result: any) => {
        console.log('DATA--->', result);
        if (result.type == true) {
          this.toastr.success('Enrol Request Sent Successfully', 'Success!').onHidden.subscribe(()=>
          this.toastr.clear());
          this.spinner.show();
          this.fetchDynamicLinkData();
        } else {
          this.toastr.warning(
            'Please try again after sometime',
            'Error Occured!'
          ).onHidden.subscribe(()=>
          this.toastr.clear());
        }
      })
      .catch(result => {
        console.log('ServerResponseError :', result);
      });
  }
  likeDislike(courseData, feedbackType){
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    // var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.userId,
      eId:  this.currentUserData.employeeId,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    const url = ENUM.domain + ENUM.url.addCourseFeedback;
      this.LSP.addCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }

  removeCourseFeedback(courseData, feedbackType) {
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.currentUserData.userId,
      eId: this.currentUserData.referenceId,
      tId: this.currentUserData.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.removeCourseFeedback;
      this.LSP.removeCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }

  goToWorkflowDetails(data){
    console.log('Go To workflow ===>', data);
    this.LSP.setWorkflowData(data);
    // this.router.navigate(['../../../../workflow_learn'], { relativeTo: this.activatedRoute });
    // this.router.navigate(['../pages/workflow_learn']);
    const wfId = data.workflowid;
    this.router.navigate(['../pages/learning/pathway/details/' + wfId]);
  }
  checkResponse: any;
  displayInstructions: boolean = false;
  passCheck: boolean = false;
  passKey: string;
  goToActivityDetails(data){
    console.log('Go To Activity Details ===>', data);
    this.disabledActivityFlag = true;
    this.spinner.show();
    this.CSP.setCourse(this.currentCourseData, data);
    // this.CSP.setCourse(this.courseDetailSummary, data);
    console.log('DATA---->', data);
    if (data.activityTypeId == 5) {
      // const userDetails = JSON.parse(localStorage.getItem("userDetails"));
      // const employeeId = localStorage.getItem('employeeId');
      var param = {
        qId: data.quizId,
        tId: this.currentUserData.tenantId,
        uId: this.currentUserData.userId,
        cId: data.courseId,
        mId: data.moduleId,
        aId: data.activityId,
        enId : data.enrolId,
      };
      var url = ENUM.domain + ENUM.url.checkQuizUser;
      this.CDSP.getCourseDetailsData(url, param)
        .then(result => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log('CHECk QUIZ FOR USER---->', temp.data[0]);
              this.checkResponse = temp.data[0];
              if (temp.data[0].flag == 'true') {
                console.log('PassCheck===>', temp.data[0]);
                if (temp.data[0].qPassword == 0) {
                  if (this.checkResponse.instructions) {
                    this.disabledActivityFlag = false;
                    this.spinner.hide();
                    this.displayInstructions = true;
                  } else {
                    this.displayInstructions = false;
                    this.disabledActivityFlag = false;
                    this.spinner.hide();
                    this.router.navigate(['../pages/learn/course-detail/quiz']);
                  }
                } else {
                  this.disabledActivityFlag = false;
                  this.spinner.hide();
                  this.passCheck = true;
                  this.passKey = temp.data[0].passWord;
                }
              } else {
                this.disabledActivityFlag = false;
                this.spinner.hide();
                this.disabledActivityFlag = false;
                let message = temp.data[0].msg + ' ' + temp.data[0].nextAttemptDate;
                this.toastr.warning(message, 'Warning!').onHidden.subscribe(()=>
                this.toastr.clear());
              }
            } else {
              this.disabledActivityFlag = false;
              this.spinner.hide();
              this.toastr.warning(
                'Something went wrong please try again later',
                'Error!'
              ).onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            this.spinner.hide();
            this.disabledActivityFlag = false;
            this.toastr.warning(
              'Something went wrong please try again later',
              'Error!'
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          this.spinner.hide();
          this.disabledActivityFlag = false;
          console.log('ServerResponseError :', result);
          this.toastr.warning(
            'Something went wrong please try again later',
            'Error!'
          ).onHidden.subscribe(()=>
          this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (data.activityTypeId == 6) {
      this.disabledActivityFlag = false;
      // var employeeId = localStorage.getItem('employeeId');
      // var userDetails = JSON.parse(localStorage.getItem('userDetails'));
      // var params = {
      //   fId: data.feedbackId,
      //   tId: userDetails.tenantId,
      //   eId: employeeId,
      // };
      // we pass this beacaues of right filteration eg : same feedback added to 2 courses if one course feedback
      // compleated then above expresion gives count =1 and
      //we cannot attempt feedback fpr second course we apply this filter
      var params = {
        fId: data.feedbackId,
        tId: this.currentUserData.tenantId,
        eId: this.currentUserData.referenceId,
        cId: data.courseId,
        mId: data.moduleId,
        aId: data.activityId,
        enId : data.enrolId,
      };
      var url = ENUM.domain + ENUM.url.checkFeedbackUser;
      this.CDSP.getCourseDetailsData(url, params)
        .then(result => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log('CHECk FEEDBACK FOR USER---->', temp.data[0]);
              if (temp.data[0].flag == 'true') {
                this.CDSP.setFeedbackSummary(data);
                this.disabledActivityFlag = false;
                this.spinner.hide();
                this.router.navigate(['../pages/learn/course-detail/feedback']);
              } else {
                this.disabledActivityFlag = false;
                this.spinner.hide();
                this.toastr.warning(temp.data[0].msg, 'Warning!').onHidden.subscribe(()=>
                this.toastr.clear());
              }
            } else {
              this.disabledActivityFlag = false;
              this.spinner.hide();
              this.toastr.warning(
                'Something went wrong please try again later',
                'Error!'
              ).onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            this.disabledActivityFlag = false;
            this.toastr.warning(
              'Something went wrong please try again later',
              'Error!'
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          this.disabledActivityFlag = false;
          this.spinner.hide();
          console.log('ServerResponseError :', result);
          this.toastr.warning(
            'Something went wrong please try again later',
            'Error!'
          ).onHidden.subscribe(()=>
          this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (data.activityTypeId == 9) {
      this.disabledActivityFlag = false;
      this.spinner.hide();
      this.router.navigate(['../pages/learn/course-detail/mark-attendance']);
    }else if(data.activityTypeId == 11){
      this.disabledActivityFlag = false;
      this.spinner.hide();
      this.router.navigate(['../pages/learn/course-detail/course']);
    }  else {
      this.disabledActivityFlag = false;
      this.spinner.hide();
      if (data && data['reference'] && data['reference'] !== ''){
        this.router.navigate(['../pages/learn/course-detail/course']);
      }else{
        this.toastr.warning(
          'No Data Available in this activity',
          'Warning!'
        ).onHidden.subscribe(()=>
        this.toastr.clear());
      }
    }


  }
  ActiveTab: any;
  tabChanged(data) {
    this.spinner.show();
    console.log('DATA--->', data);
    this.ActiveTab = data;
    this.activityCardList = data.list;
    for (let i = 0; i < this.activityCardList.length; i++) {
      const temp: any = i + 1;
      // this.courseDetailArray[i].img =
      //   "assets/images/open-book-leaf-2.jpg";
      this.activityCardList[i].img = 'assets/images/open-book-leaf-2.jpg';
      // if(this.courseDetailArray[i].activity_type === 'Quiz'){
      //   this.courseDetailArray[i].img = 'assets/images/activity_image/quiz.jpg';
      // }
      // else {
      //   this.courseDetailArray[i].img = 'assets/images/open-book-leaf-2.jpg';
      // }
      // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
      this.activityCardList[i].activityTempId = temp;
      const tempCourseI = this.activityCardList[i];
      //var tempDate = new Date(this.courseDetailArray[i].completed);
      for (let j = 0; j < this.activityCardList.length; j++) {
        const tempCourseJ = this.activityCardList[j];
        //this.courseDetailArray[j].activityDuration = this.makeTimeReady(this.courseDetailArray[j].activityDuration.split('.')[0]);
        if (tempCourseI.dependentActId !== 0 || !tempCourseI.dependentActId) {
          if (tempCourseI.dependentActId === tempCourseJ.activityId) {
            if (tempCourseJ.wCompleted === 0) {
              tempCourseI.dependant = tempCourseJ.activityName;
              // break;
            }
          }
        }
      }
    }
    this.spinner.hide();
  }
  goBack(){
    this.router.navigate(['../pages/dashboard']);
  }


  close() {
    this.passCheck = false;
  }
  closeInstrction() {
    this.displayInstructions = false;
  }
  get f() {
    return this.resetPwdForm.controls;
  }
  submitted: boolean = false;
  serviceButtonDisabled: boolean = false;
  passWrong: boolean = false;
  onSubmit() {
    this.submitted = true;
    this.serviceButtonDisabled = true;
    if (this.resetPwdForm.invalid) {
      return;
    } else {
      console.log('EnteredPassword--->', this.resetPwdForm.value.password);
      console.log('PasswordKey--->', this.passKey);
      if (this.resetPwdForm.value.password == this.passKey) {
        this.passCheck = false;
        if (this.checkResponse.instructions) {
          this.serviceButtonDisabled = true;
          this.displayInstructions = true;
        } else {
          this.serviceButtonDisabled = true;
          this.displayInstructions = false;
          this.router.navigate(['../pages/learn/course-detail/quiz']);
        }
      } else {
        this.passWrong = true;
      }
    }
  }

  onSubmit1() {
    // this.router.navigate(['quiz'], { relativeTo: this.activatedRoute });
    this.router.navigate(['../pages/learn/course-detail/quiz']);
  }

  makeCourseDataReady(tempCourseData) {
    const courseData = {
        approvalStatus: '',
        cat_id: '',
        category: '',
        compStatus: '',
        completed_courses: '',
        courseDesc: tempCourseData.cDescription,
        courseTitle: tempCourseData.courseName,
        courseType: tempCourseData.courseType,
        courseType2: '',
        cpoints: '',
        endDate: tempCourseData.endDate,
        enrolId: tempCourseData.enrolId,
        id: tempCourseData.courseId,
        isBookmark: '',
        isDislike: '',
        isLike: '',
        nextStage: '',
        noDislikes: '',
        noLikes: '',
        noOfmodules: '',
        percComplete: '',
        startDate: '',
        total_courses: '',
        typeImage: ''
    };
    return courseData;
}

makeCourseDataReadyForWF(courseData) {
  let courseList = courseData;
    if (courseList) {
      for (let i = 0; i < courseList.length; i++) {
        if(courseList[i].workflowid){
        courseList[i]['begin'] = true;
        courseList[i]['wait'] = false;
        courseList[i]['completed'] = false;
        if (courseList[i].previousCourseid !== 0 && courseList[i].previousCourseid !== null &&
          courseList[i].previousCourseid !== undefined) {
              courseList[i]['dependentCourseName'] = courseList[i].dependentCourseName;
              if (courseList[i].prevCourseCompStat == 'Y') {
                if (courseList[i].cascade_course_flag === 1) {
                  courseList[i].wait = false;
                  courseList[i].begin = true;
                  courseList[i].completed = false;
                } else {
                  courseList[i].wait = true;
                  courseList[i].begin = false;
                  courseList[i].completed = false;
                }
              } else {
                courseList[i].wait = true;
                courseList[i].begin = false;
                courseList[i].completed = false;
              }
        } else if (courseList[i].percComplete === 100) {
          courseList[i].completed = true;
          courseList[i].wait = false;
          courseList[i].begin = false;
        } else {
          if (courseList[i].cascade_course_flag === 1) {
            courseList[i].wait = false;
            courseList[i].begin = true;
            courseList[i].completed = false;
          } else {
            courseList[i].wait = true;
            courseList[i].begin = false;
            courseList[i].completed = false;
          }
        }
      }
    }
      this.dataArray = courseList;
      console.log('Formatted course data: ', courseList);
    }
}

}
