import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { ModuleActivityContainerComponent } from './module-activity-container/module-activity-container.component';
import { DynamicLinkComponentComponent } from './dynamic-link-component/dynamic-link-component.component';

const routes: Routes = [
  {path: '', component: DynamicLinkComponentComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DynamicLinkRoutingModule { }
