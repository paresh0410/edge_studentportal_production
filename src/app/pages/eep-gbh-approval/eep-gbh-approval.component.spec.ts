import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepGbhApprovalComponent } from './eep-gbh-approval.component';

describe('EepGbhApprovalComponent', () => {
  let component: EepGbhApprovalComponent;
  let fixture: ComponentFixture<EepGbhApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepGbhApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepGbhApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
