import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ENUM } from '../../service/enum';
import { TrainTheTrainerServiceProvider } from '../../service/traine-the-trainer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EEPService } from '../../service/eep.service';
@Component({
  selector: 'ngx-eep-gbh-approval',
  templateUrl: './eep-gbh-approval.component.html',
  styleUrls: ['./eep-gbh-approval.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EepGbhApprovalComponent implements OnInit {
  nominees: any = [];
  Comment:any = '';
  employeeId: any;
  userDetails: any;
  listShow: boolean = false;
  searchString: any;
  nomineelist: any = [];
  constructor(public eep: EEPService,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService) {
    this.employeeId = JSON.parse(localStorage.getItem('employeeId'));
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
  }


  ngOnInit() {
    console.log(this.Comment.length,"comment")
    this.dcontent();
  }
  dcontent() {
    try {
      const data = {
        empId: this.employeeId,
        tId: this.userDetails.tenantId,
      };
      this.spinner.show();
      console.log(data);
      const url = ENUM.domain + ENUM.url.get_GBH_aproval_list;
      this.eep.get(url, data).then(res => {
        console.log(res);
        this.spinner.hide();
        try {
          this.nominees = res['data'];
          if (this.searchString) {
            this.searchlist(this.searchString);
          }
          this.nomineelist = this.nominees;
          console.log('this.nominees', this.nominees);
          //    this.listShow = t
          // this.moduledetail(this.nomineeModules);
        } catch (e) {
          console.log(e);
          this.nominees = [];
        }
      }, err => {
        this.spinner.hide();
        console.log(err);
      });
    } catch (e) {
      console.log(e);
    }

  }

  // this function is for update the status
  updateStatus(item, sts) {
    if(item.isDisabled == 1){
      this.toastr.warning("Can Not access after end",'Warning!').onHidden.subscribe(()=>
      this.toastr.clear())
    }else if(!item.comment){
      this.toastr.warning("Please Enter your Comment",'Warning!').onHidden.subscribe(()=>
      this.toastr.clear())
    }
    else{
    const datae = {
      appId: item.GBHId,
      appStat: sts,
      appCmt: item.comment,
      actId: 0,
      userId: this.userDetails.id,
      wfId: item.workflowId,
      empId: item.empId,
      tId: this.userDetails.tenantId,
    };
    this.spinner.show();
    console.log(datae);
    const url = ENUM.domain + ENUM.url.eep_update_GBH_aproval;
    this.eep.get(url, datae).then(res => {
      console.log(res);
      this.spinner.hide();
      try {
        var result = res['data'];
        console.log('result===>', result);
        this.toastr.success(result[0].msg, 'Success!').onHidden.subscribe(()=>
        this.toastr.clear());
        this.dcontent();
      } catch (e) {
        console.log(e);
        console.log((res['data']));
        this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
        this.toastr.clear());
        // this.toastr.warning(res['data'], 'Server Warning!');

      }
    }, err => {
      this.spinner.hide();
      console.log(err);
      this.toastr.warning('Something went wrong please try again later', 'Warning!').onHidden.subscribe(()=>
      this.toastr.clear());
      // this.toastr.warning(err, 'Server Warning!');

    });
  }
}

  searchlist(data) {
    console.log(data);
    const val = data;
    const temp = this.nominees.filter(function (d) {
      return String(d.userName).toLowerCase().indexOf(val) !== -1 ||
        d.fullname.toLowerCase().indexOf(val) !== -1 ||
        d.workflowName.toLowerCase().indexOf(val) !== -1 ||d.phone.indexOf(val)!==-1||
        d.Business.toLowerCase().indexOf(val)!==-1||d.Band.toLowerCase().indexOf(val)!==-1||
        d.email.toLowerCase().toLowerCase().indexOf(val)!==-1||
        !val
    })
    this.nomineelist = temp;
  }
  // d.email.toLowerCase().toLowerCase().indexOf(val)!==-1||
  clearstring() {
    this.searchString = '';
    this.dcontent();
  }
}
