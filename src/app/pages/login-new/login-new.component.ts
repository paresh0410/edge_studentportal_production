import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  FormBuilder,
  // FormControl,
  FormGroup,
  // FormGroupDirective,
  // NgForm,
  Validators,
} from '@angular/forms';
// import { ErrorStateMatcher } from '@angular/material/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { routerNgProbeToken } from '@angular/router/src/router_module';
// import { ENUM } from '../../service/enum';
import { AuthServiceProvider } from '../../service/auth-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../service/user.service';
import { MsalService,
  // MsalConfig, MsalModule
 } from '@azure/msal-angular';
// import { MSAL_CONFIG } from '@azure/msal-angular/dist/msal.service';
// import { AppModule } from '../../app.module';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as CryptoJS from 'crypto-js';
// var filesystem = require('file-system');
// import * as filesystem from 'file-system/file-system';
// import { knownFolders, File, Folder } from "tns-core-modules/file-system";
// import * as fs from 'file-system';
// import * as s3Unzip from 's3-unzip';
// import * as JSZip from 'jszip';
import { BrandDetailsService } from '../../service/brand-details.service';
import { MultiLanguageService } from '../../service/multi-language.service';
import { DomSanitizer } from '@angular/platform-browser';
// import { DataSeparatorService } from '../../service/data-separator.service';

@Component({
  selector: 'ngx-login-new',
  templateUrl: './login-new.component.html',
  styleUrls: ['./login-new.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginNewComponent implements OnInit {

  usn: any;
  password: any;
  passWrong: boolean = false;
  FailMsg: string;
  signInForm: FormGroup;
  submitted = false;
  tenantList: any = [];
  tenant: any;
  errorMsg: any;
  isError: boolean = false;
  isLoginError: boolean = false;

  public type = 'password';
  public showPass = false;
  public remember_me = false;
  public goToWelcomePage = false;
  public responseTid: number;
  public isSso = false;
  hide = true;
  currentBrandData: any;

  // zip = new JSZip();
  config = {
    clientID: '2f22131a-e587-47db-9c39-437e25ed3757',
    authority: 'https://login.microsoftonline.com/bajajfinance.in/',
    validateAuthority: true,
    redirectUri: window.location.origin, // "http://localhost:4200/",
    cacheLocation: 'localStorage',
    postLogoutRedirectUri: window.location.origin + '/#/login', // "http://localhost:4200/#/login",
    navigateToLoginRequestUrl: true,
    popUp: false,
    consentScopes: [
      'user.read',
      'api://2f22131a-e587-47db-9c39-437e25ed3757/access_as_user'
    ],
    unprotectedResources: ['https://www.microsoft.com/en-us/'],
    // protectedResourceMap: protectedResourceMap,
    // logger: loggerCallback,
    correlationId: '1234',
    // level: LogLevel.Info,
    piiLoggingEnabled: true
  };
  // variables: any = {
  //   header : '--colorHeader',
  //   sidemenu: '--colorSidebar',
  //   commonText: '--colorText',
  //   icon: '--colorIcons',
  //   button: '--colorBgbtn',
  //   buttonText: '--colorBtnTxt',
  //   commonColor: '--colorCommon'
  // }
  variables: any = {
    button: '--colorBgbtn',
    commonColor: '--colorCommon',
    header: '--colorHeader',
    icon: '--colorIcons',
    pageBackground: '--colorBgPage',
    sidemenu: '--colorSidebar',
  };
   themes = document.querySelector('body');

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private routes: ActivatedRoute,
    public ASP: AuthServiceProvider,
    public spinner: NgxSpinnerService,
    public userservice: UserService,
    private authService: MsalService,
    private fb: FormBuilder,
    // public dataSeparatorService: DataSeparatorService,
    public brandService: BrandDetailsService,
    public multiLanguageService: MultiLanguageService,
    private _sanitizer: DomSanitizer,
  ) {
    // this.appmodule.msla
    this.fetchTenantList();
  }

  // FetchTenantList(){
  //   try {
  //     this.ASP.getTenantList()
  //       .then((result: any) => {
  //         this.tenantList = result.data;
  //         console.log('List Of tenantList ===>', this.tenantList);
  //         if (this.tenantList.length > 0) {
  //           this.tenant = this.tenantList[0];
  //           this.switchCompanyTab(this.tenant);
  //         }
  //         var id = document.getElementById('nb-global-spinner');
  //         id.style.display = 'none';
  //         this.spinner.spinnerObservable.next(false);
  //         this.isError = false;
  //       })
  //       .catch(e => {
  //         this.tenantList = [];
  //         this.errorMsg = 'Unable to fetch Tenant List';
  //         this.isLoginError = false;
  //         this.isError = true;
  //         var id = document.getElementById('nb-global-spinner');
  //         id.style.display = 'none';
  //         this.spinner.spinnerObservable.next(false);
  //         console.log(e);
  //       });
  //     // if (!this.authService.getUser()) {
  //     // 	this.ssologout();
  //     // 	localStorage.clear();
  //     // } else {
  //     // 	// this.loggedIn = true;
  //     // 	this.router.navigate(['../pages/dashboard'],{relativeTo:this.routes})
  //     // }

  //   } catch (e) {
  //     this.errorMsg = 'Unable to fetch Tenant List';
  //     this.isLoginError = false;
  //     this.isError = true;
  //     var id = document.getElementById('nb-global-spinner');
  //     id.style.display = 'none';
  //     this.spinner.spinnerObservable.next(false);
  //     console.log(e.message);
  //   }
  // }
  fetchTenantList(){
    try {
      var id = document.getElementById('nb-global-spinner');
      id.style.display = 'block';
      this.ASP.getTenantList()
        .then((result: any) => {
          if(result.data){
            this.tenantList = result.data;
            console.log('List Of tenantList ===>', this.tenantList);
            if (this.tenantList.length > 0) {
              this.tenant = this.tenantList[0];
              this.switchCompanyTab(this.tenant);
            }
            var id = document.getElementById('nb-global-spinner');
            id.style.display = 'none';
            this.spinner.hide();
          }else{
            setTimeout(()=>{
              this.fetchTenantList();
            }, 1500);
          }

        })
        .catch(e => {
          // var id = document.getElementById('nb-global-spinner');
          // id.style.display = 'block';
          // this.spinner.spinnerObservable.next(false);
          setTimeout(()=>{
            this.fetchTenantList();
          }, 1500);

          // this.tenantList = [];
          // var id = document.getElementById('nb-global-spinner');
          // id.style.display = 'none';
          // this.spinner.spinnerObservable.next(false);
          console.log(e);
        });
      // if (!this.authService.getUser()) {
      // 	this.ssologout();
      // 	localStorage.clear();
      // } else {
      // 	// this.loggedIn = true;
      // 	this.router.navigate(['../pages/dashboard'],{relativeTo:this.routes})
      // }

    } catch (e) {
      setTimeout(()=>{
        // var id = document.getElementById('nb-global-spinner');
        // id.style.display = 'block';
        this.fetchTenantList();
      }, 1500);
      // var id = document.getElementById('nb-global-spinner');
      // id.style.display = 'none';
      // this.spinner.spinnerObservable.next(false);
      console.log(e.message);
    }
  }

  ngOnInit() {
    //form group and validations declared
    if (localStorage.getItem('remember_me_details') != null) {
      // console.log("Local Storage =====>",JSON.parse(localStorage.getItem('remember_me_details')));
      let remember_me_details = JSON.parse(
        localStorage.getItem('remember_me_details')
      );
      if (remember_me_details.remember_me === true) {
        this.usn = remember_me_details.username;
        this.remember_me = remember_me_details.remember_me;
        let encryptKey = remember_me_details.encryption_key;
        // this.password = remember_me_details.password;
        var decrypted = this.get(encryptKey, remember_me_details.password);
        console.log('Decrypted ======>', decrypted);
        this.password = decrypted;
      }
      console.log('document ======>', this.themes);
    }

    this.signInForm = this.formBuilder.group({
      usn: [this.usn, Validators.required],
      password: [this.password, [Validators.required, Validators.minLength(6)]],
      remember_me: [this.remember_me]
    });

    // this.ssoLogin();

    console.log(this.routes.snapshot.queryParamMap.has('url'));
    console.log(this.routes.snapshot.queryParamMap.get('url'));
    console.log(this.routes.snapshot.queryParamMap.keys);
    // console.log
    this.currentBrandData = this.brandService.getCurrentBrandData();
    this.currentBrandData['loginImage'] = this.sanitizeImage(this.currentBrandData['loginImage']);
    // const teantData =  this.ASP.getDomainBasedTenant();
    // console.log('teantData', teantData);
  }



  encryptData(keys, value) {
    var key = CryptoJS.enc.Utf8.parse(keys);
    var iv = CryptoJS.enc.Utf8.parse(keys);
    var encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(value.toString()),
      key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }
    );

    return encrypted.toString();
  }

  get(keys, value) {
    var key = CryptoJS.enc.Utf8.parse(keys);
    var iv = CryptoJS.enc.Utf8.parse(keys);
    var decrypted = CryptoJS.AES.decrypt(value, key, {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  get f() {
    return this.signInForm.controls;
  } //form fields getter for easy access

  onSubmit() {
    // console.log("Remember Me Flag =====>",this.signInForm.value.remember_me);

    // this.usn =  this
    this.spinner.show();
    this.submitted = true;
    let tenant = null;
    localStorage.removeItem('ssoaccess');
    // if (this.isString(this.tenant)) {
    //   tenant = JSON.parse(this.tenant);
    // } else if (this.tenant) {
    //   tenant = this.tenant;
    // }
    // stop here if form is invalid
    if (this.signInForm.invalid && !tenant) {
      this.spinner.hide();
      return;
    } else {
      this.usn = this.signInForm.value.usn;
      this.password = this.signInForm.value.password;
      console.log('DATA--->');
      console.log('DATA--->', this.password);
      const teantData =  this.ASP.getDomainBasedTenant();
      console.log('DATA--->', teantData);
      var param = {
        // username: this.usn,
        email: this.usn,
        password: this.password,
        // tenantId: tenant.tenantId,
        sso: 0,
        type: 3,
        tenantId: teantData.tenantId,
      };
      // this.tenantList.forEach(element => {
      //     if(element.tenantId == tenant.tenantId) {
      //       // localStorage.setItem('tenantName', element.tenantName);
      //       console.log('tenantName', element.tenantName);
      //       localStorage.setItem('tenantData', JSON.stringify(element));
      //     }
      // });
      // var url =ENUM.domain + '' + ENUM.url.auth;
      console.log(' teantData',param);

      this.ASP.authUserData(param)
        .then(result => {
          var temp: any = result;
          console.log('Result--->', result);
          this.spinner.hide();
          if (temp.type == false) {
            // alert(temp.data1);
            this.isLoginError = true;
            if(temp.data.tenantId){
              this.responseTid = temp.data.tenantId;
            }

            // this.isSso = temp.isSso;

            if (temp.data1) {
              this.isSso = temp.isSso;
              // this.responseTid = res.data.tenantId;
              this.errorMsg = temp.data1;
              this.isError = true;
              // this.loginForm.reset();
              // this.loginForm.value.password = '';
              // this.loginForm.controls.password.reset();
            } else {
              // this.errorMsg = temp.data1;
              this.errorMsg = 'Something went wrong please try again.';
              this.isError = true;
              // this.presentalert(msg, 0);
            }
            this.clear();
          } else if (temp.type == true) {
            const themes = temp.theme;
            localStorage.setItem('theme', JSON.stringify(themes));
            for(let key in themes) {
              let value = themes[key];
              console.log('elemnet=======>>>', value);
              this.changeTheme(key,value);
            }
            this.passWrong = false;
            var stringUserDetail = JSON.stringify(temp.data);
            var stringUserOtherDetail = JSON.stringify(temp.otherdata);
            var userdetail = temp.data;
            localStorage.setItem('userDetails', stringUserDetail);
            localStorage.setItem('token', temp.token);
            localStorage.setItem('employeeId', temp.employeeId);

            this.remember_me = this.signInForm.value.remember_me;
            if (this.remember_me) {
              // var wordArray = '123456$#@$^@1ERF';
              // console.log("Word Array =======>" , wordArray);
              // wordArray
              let encryption_key = this.generateId(20);
              var encrypted = this.encryptData(encryption_key, this.password);
              // console.log('Encryption ==>' ,encrypted);
              // const password_encryption = AES.encrypt(this.password , 'key');

              let remember_me_data = {
                username: this.usn,
                password: encrypted,
                remember_me: this.remember_me,
                encryption_key: encryption_key,
                tenantId: temp.data.tenantId,
              };
              localStorage.setItem(
                'remember_me_details',
                JSON.stringify(remember_me_data),
              );
            } else {
              localStorage.removeItem('remember_me_details');
            }
            userdetail.userId = userdetail.id;
            var otherdata = temp.otherdata[3];
            console.log(otherdata);
            for (let i = 0; i < otherdata.length; i++) {
              // if (otherdata[i].fieldmasterId) {
              //   userdetail.referenceId = otherdata[i].fieldmasterId;
              //   userdetail.roll = otherdata[i].roleName;
              //   userdetail.roleId = otherdata[i].roleId;
              // } else {
              //   userdetail.roll = 'Learner';
              //   userdetail.roleId = 8;
              // }
              if (otherdata[i].isDefault == 1) {
                  userdetail.referenceId = otherdata[i].fieldmasterId;
                  userdetail.roll = otherdata[i].roleName;
                  userdetail.roleId = otherdata[i].roleId;
                  localStorage.setItem('roleName', otherdata[i].roleName);
              }
            }
            this.userservice.setUser(userdetail);
            localStorage.setItem('userdetail', JSON.stringify(userdetail));
            if(userdetail && userdetail['userLang']){
            this.multiLanguageService.updateLangugae(userdetail['userLang'], false);
            }
            localStorage.setItem('userOtherDetails', stringUserOtherDetail);
            if (localStorage.getItem('userOtherDetails')) {
              var UOD = JSON.parse(localStorage.getItem('userOtherDetails'));
              console.log('UOD', UOD);
              // var temp = UOD[3];
              var temp = UOD[3];
              for (let i = 0; i < temp.length; i++) {
                if (otherdata[i].isDefault === 1) {
                  userdetail.referenceId = otherdata[i].fieldmasterId;
                  userdetail.roll = otherdata[i].roleName;
                  userdetail.roleId = otherdata[i].roleId;
                }
              }
            }
            const redirectURL = localStorage.getItem('redirectURL') || '';
            if(redirectURL !== '/login-new' && redirectURL !== '') {
              this.router.navigate([redirectURL], { relativeTo: this.routes });
            } else {
              this.router.navigate(['../welcome'], { relativeTo: this.routes });
            }
          } else {
            this.passWrong = true;
            this.FailMsg = temp.data1;
            this.spinner.hide();
          }
        })
        .catch(result => {
          this.spinner.hide();
          console.log('ServerResponseError :', result);
        });
    }
    // if(this.usn=='admin@mobeserv.com' && this.password=='mobeserv'){
    //   this.router.navigate(['../welcome'],{relativeTo:this.routes});
    // }
    // }
  }

  forgotPass() {
    this.router.navigate(['../forgotPass'], { relativeTo: this.routes });
  }

  ssologin(item) {
    // this.authService.loginPopup(["user.read" ,"api://54b9f2a8-dc1f-4173-8955-f762e6fd0b37/access_as_user"]);
    // this.authService.loginPopup();
    this.tenantChange(item);
    localStorage.removeItem('ssoaccess');
    if (this.tenant.poup) {
      this.authService.loginPopup();
    } else {
      this.authService.loginRedirect();
    }
  }

  ssologout() {
    // this.authService.logout();
  }
  configure() {
    // platformBrowserDynamic([{ provide: MSAL_CONFIG, useValue: this.config }])
    // 	.bootstrapModule(AppModule)
    // 	.catch(err => {
    // 		console.log(err)
    // 	});
    console.log(this.authService.authority);
    // this.authService.authority = 'https://login.microsoftonline.com/bajajfinance.in/';
    this.authService.clientId = '29af2eb7-4f90-4c8a-8b56-a9ac27504103';
    console.log(this.authService.authority);

    // platformBrowserDynamic([
    // 	{
    // 	  provide: 'adalConfig',
    // 	  useValue: this.config as MsalConfig
    // 	}
    // ]).bootstrapModule(AppModule)
    // .catch(err =>
    // {
    // 	console.error(err)
    // });
  }
  tenantChange(item) {
    // item = JSON.parse(item);
    this.authService.authority = item.authority; // 'https://login.microsoftonline.com/bajajfinance.in/';
    this.authService.clientId = item.clientId; // '29af2eb7-4f90-4c8a-8b56-a9ac27504103';
    this.ASP.setTenant(item);
    localStorage.setItem('tenantinfo', JSON.stringify(item));
  }
  stringify(value) {
    return JSON.stringify(value);
  }
  isString(value) {
    return typeof value === 'string';
  }

  // Company Array
  company: any = [
    {
      companyName: 'Mobeserv',
      login: false
    },
    {
      companyName: 'BFL',
      login: true
    },
    {
      companyName: 'BFL 1',
      login: false
    }
  ];

  switchCompanyTab(item: any) {
    let tenantName = null;
    if (item.tab) {
      tenantName = item.tab.textLabel;
    } else {
      tenantName = this.tenant.tenantName;
    }

    if (this.tenantList.length > 0) {
      this.tenantList.forEach(element => {
        if (
          String(element.tenantName).toLowerCase() ===
          String(tenantName).toLowerCase()
        ) {
          this.tenant = element;
        }
      });
    }
    this.authService.authority = this.tenant.authority; // 'https://login.microsoftonline.com/bajajfinance.in/';
    this.authService.clientId = this.tenant.clientId; // '29af2eb7-4f90-4c8a-8b56-a9ac27504103';
    this.ASP.setTenant(this.tenant);
    this.clear();
  }

  repeatCssFunction() {
    return 'repeat(' + this.tenantList.length + ', 1fr)';
  }
  // dec2hex :: Integer -> String
  // i.e. 0-255 -> '00'-'ff'
  dec2hex(dec) {
    return ('0' + dec.toString(16)).substr(-2);
  }
  // generateId :: Integer -> String
  generateId(len) {
    let arr = new Uint8Array((len || 40) / 2);
    window.crypto.getRandomValues(arr);
    return Array.from(arr, this.dec2hex).join('');
  }
  clear() {
    this.usn = null;
    this.password = null;
  }
  // closeModal() {
  //   if(this.isLoginError){
  //     if (this.responseTid && this.isSso) {
  //       let item = null;
  //       this.tenantList.forEach(element => {
  //           if (element.tenantId === this.responseTid) {
  //             item = element;
  //           }
  //       });
  //       this.ssologin(item);
  //     }
  //     this.isError = false;
  //   }
  //   // else {
  //   //   this.fetchTenantList();
  //   // }

  //   // this.clear();
  // }
  closeModal() {
    if (this.responseTid && this.isSso) {
      let item = null;
      this.tenantList.forEach(element => {
          if (element.tenantId === this.responseTid) {
            item = element;
          }
      });
      this.ssologin(item);
    }
    this.isError = false;
    // this.clear();
  }
  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  changeTheme(colorName, colorValue) {
    // Object.keys(this.variables).forEach(function(val) {
      // console.log('themes=========>>>>>>>>>>>', themes[key]);
      for(let val in this.variables) {
        if(val === colorName) {
          if (colorName === 'header') {
            this.themes.style.setProperty(this.variables[val], colorValue);
            const headerTxt = this.getContrastYIQ(colorValue);
            this.themes.style.setProperty('--colorHeadertxt', headerTxt);
          } else if (colorName === 'sidemenu') {
            this.themes.style.setProperty(this.variables[val], colorValue);
            const sidebarTxt = this.getContrastYIQ(colorValue);
            this.themes.style.setProperty('--colorSidebartxt', sidebarTxt);
          } else if (colorName === 'button') {
            this.themes.style.setProperty(this.variables[val], colorValue);
            const btnTxt = this.getContrastYIQ(colorValue);
            this.themes.style.setProperty('--colorBtnTxt', btnTxt);
          } else {
            this.themes.style.setProperty(this.variables[val], colorValue);
          }
        }
    }
    // });
  }

  getContrastYIQ(hexcolor){
    hexcolor = hexcolor.replace("#", "");
    var r = parseInt(hexcolor.substr(0,2),16);
    var g = parseInt(hexcolor.substr(2,2),16);
    var b = parseInt(hexcolor.substr(4,2),16);
    var match = ((r*299)+(g*587)+(b*114))/1000;
    return (match >= 128) ? '#000000' : '#ffffff';
  }

  goToSignup(){
    this.router.navigate(['../signup'],{relativeTo:this.routes});
  }

  public sanitizeImage(image: string) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }
}
