import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LearnServiceProvider } from '../../service/learn-service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
// import { formatDate } from '@angular/common';
import { LogServices, LogEnum } from '../../service/logservice';
import { NbSidebarService } from '@nebular/theme';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';
import { LearnSearchServiceProvider } from '../../service/learn-search.service';
import { feature } from '../../../environments/feature-environment';

@Component({
  selector: 'ngx-learn-search',
  templateUrl: './learn-search.component.html',
  styleUrls: ['./learn-search.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LearnSearchComponent implements OnInit, OnDestroy {

  toggle() {
    this.sidebarService.toggle(false, 'menu-sidebar');
  }
  noDataFound = false;
  public tabArray: any = [];
  public CourseTypeTabArray: any = [];
  public CourseDateTabArray: any = [];
  public constTabArray: any = [];
  public view: number = 1;
  public viewPath: string = 'assets/images/calendar.svg';
  public cardArray: any = [];
  public enrolStatus: string = 'ENROL';
  public hideComp: boolean = true;
  isSearchSubscribe: any;
  public monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  public monthNames1 = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  public selectedIndex = 0;
  public areas = {};
  public userDetails: any;
  public employeeId: any;
  public prevTab: any;
  isWorking: boolean = false;
  featureConfig = feature;
  constructor(
    public LSP: LearnServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    public logService: LogServices,
    public logEnum: LogEnum,
    private sidebarService: NbSidebarService,
    private LSSP: LearnSearchServiceProvider,
    public cdf: ChangeDetectorRef,
  ) {
    this.areas = this.logService.getArea('learn');
    this.isWorking = false;
    this.spinner.show();
    if (localStorage.getItem('userDetails')) {
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    }
    if (localStorage.getItem('employeeId')) {
      this.employeeId = localStorage.getItem('employeeId');
    }
    var logAction = {
      action: this.logEnum.viewed,
      target: 'learn',
      instanceid: '',
      other:
        'Learn ' + this.logEnum.viewed + ' by ' + this.userDetails.username,
      crud: 'r',
      area: 'learn'
    };
    this.logService.saveLogged(logAction);
    // this.searchData();
  }
  searchData(){
    // const searchString = this.LSSP.getSearchString();
    // this.router.navigate([item.menuRoute, menuId], { relativeTo: this.routes });
    this.tabArray = [];
    this.CourseTypeTabArray = [];
    this.CourseDateTabArray = [];
    if (!this.isWorking) {
      this.cardArray = [];
      const searchString = this.LSSP.getSearchString() || this.routes.snapshot.params['search'];
      var param = {
        eId: this.employeeId,
        tId: this.userDetails.tenantId,
        uId: this.userDetails.id,
        SS: searchString,
      };
      this.isWorking = true;
      this.LSSP.getSearchedCourses(param)
        .then(result => {
          this.isWorking = false;
          var temp: any = result;
          this.CourseTypeTabArray = [];
          this.CourseDateTabArray = [];
          this.constTabArray = [];
          if (
            temp.data != undefined ||
            temp.data != null ||
            temp.data1 != undefined ||
            temp.data1 != null
          ) {
            if (temp.data.length > 0 && temp.data1.length) {
              // console.log('DATA1--->',temp.data1);
              this.CourseTypeTabArray = temp.data;
              this.CourseDateTabArray = temp.data1;
              this.tabArray = temp.data;
              //if(this.tabArray.length > 0){
              //this.prevTab = this.tabArray[0];
              //}
              this.cardArray = this.getFormattedCourseData(temp.data[0].list);
              console.log('DATA--->', this.CourseTypeTabArray);
              // this.cdf.detectChanges();
              // console.log('DATA--->',this.tabGroup.selectedIndex);
              this.spinner.hide();
              this.noDataFound = false;
            } else {

               this.cdf.detectChanges();
              this.toastr.warning('No Data Available', 'Warning!').onHidden.subscribe(()=>
              this.toastr.clear());
              this.spinner.hide();
            }
          }
        })
        .catch(result => {
          this.spinner.hide();
          console.log('ServerResponseError :', result);
        });
    }
  }
  getFormattedCourseData(DataCourseType) {
    for (let i = 0; i < DataCourseType.length; i++) {
      let temporary = DataCourseType[i];
      for (let j = 0; j < temporary.list.length; j++) {
        temporary.list[j].courseDate = this.formatDate(
          temporary.list[j].courseDate);
        if (!temporary.list[j].earnpoints) {
          temporary.list[j].earnpoints = 0;
        }
        if (!temporary.list[j].cpoints) {
          temporary.list[j].cpoints = 0;
        }
        temporary.list[j].endDate = this.formatDate(temporary.list[j].endDate);
        temporary.list[j].completed = this.formatCompData(
          temporary.list[j].completed_courses,
          temporary.list[j].total_courses,
        );
        if (temporary.list[j].courseType == 'MANDATORY') {
          temporary.list[j].type1Image = 'assets/images/orangeLearn.svg';
        } else if (temporary.list[j].courseType == 'RECOMMENDED') {
          temporary.list[j].type1Image = 'assets/images/mountLearn.svg';
        } else {
          // if(temporary.list[j].courseType == 'ASPIRATIONAL'){
          temporary.list[j].type1Image = 'assets/images/tickLearn.svg';
        }
        if (temporary.list[j].courseType2 == 'Online') {
          temporary.list[j].type2Image = 'assets/images/online.svg';
        } else if (temporary.list[j].courseType2 == 'Classroom') {
          temporary.list[j].type2Image = 'assets/images/classroom.svg';
        }
        // if (temporary.list[j].percComplete === 0) {
        //   temporary.list[j].percComplete = 0;
        // }
      }
    }
    return DataCourseType;
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + d.getMonth(),
      day = '' + d.getDate(),
      year = d.getFullYear();

    month = this.monthNames[month];
    if (day.length < 2) day = '0' + day;

    var dateString = [day, month, year].join(' ');

    return dateString;
  }

  formatCompData(ccvar, tcvar) {
    ccvar = '' + ccvar;
    tcvar = '' + tcvar;

    if (ccvar.length < 2) ccvar = '0' + ccvar;
    if (tcvar.length < 2) tcvar = '0' + tcvar;

    var retString = ccvar + ' / ' + tcvar;

    return retString;
  }

  ngOnInit() {
    this.isSearchSubscribe = this.LSSP.data.subscribe(data => {
      this.spinner.show();
      this.noDataFound = true;
      this.searchData();
    });
  }

  switchView() {
    this.spinner.show();
    console.log('VIEW---->', this.view);
    if (this.view == 1) {
      var monIndex = new Date().getMonth();
      var compString = this.monthNames[monIndex];
      this.view = 2;
      this.viewPath = 'assets/images/list.svg';
      this.tabArray = [];
      this.tabArray = this.CourseDateTabArray;
      for (var i = 0; i < this.tabArray.length; i++) {
        var cD = this.tabArray[i].CourseMon;
        for (var j = 0; j < this.monthNames1.length; j++) {
          if (cD == this.monthNames1[j]) {
            var index = j;
          }
        }
        var cDM = this.monthNames[index];
        var cDY = new Date(this.tabArray[i].CourseDate)
          .getFullYear()
          .toString()
          .substr(-2);
        this.tabArray[i].CourseDateMonth = cDM;
        this.tabArray[i].CourseDateYear = cDY;
        if (compString == this.tabArray[i].CourseDateMonth) {
          this.selectedIndex = i;
        } else {
          this.selectedIndex = 0;
        }
      }
      this.cardArray = [];
      this.cardArray = this.getFormattedCourseData(
        this.tabArray[this.selectedIndex].list,
      );
      console.log('TABARRAY===>', this.cardArray);
      this.spinner.hide();
    } else if (this.view == 2) {
      this.view = 1;
      this.viewPath = 'assets/images/calendar.svg';
      this.tabArray = [];
      this.tabArray = this.CourseTypeTabArray;
      this.cardArray = this.getFormattedCourseData(this.tabArray[0].list);
      // console.log('TabArray===>',this.tabArray);
      this.spinner.hide();
    }
  }

  tabChanged(event) {
    if(event){
      this.spinner.show();
      console.log('TABCHANGED!!!!!', event.index);
      this.cardArray = [];
      this.cardArray = this.getFormattedCourseData(
        this.tabArray[event.index].list,
      );
      console.log('CARDARRAY--->', this.cardArray);
      this.spinner.hide();
      let tabItem = this.tabArray[event.index];
      if (this.prevTab) {
        var logAction = {
          action: this.logEnum.changed,
          target: 'learn',
          instanceid: '',
          other:
            this.userDetails.username +
            ' ' +
            this.logEnum.changed +
            ' course tabs from ' +
            this.prevTab.CourseType +
            ' To ' +
            tabItem.CourseType,
          crud: 'r',
          area: 'learn'
        };
        this.logService.saveLogged(logAction);
      }
      this.prevTab = tabItem;
    }

  }

  hideCompCourse(slide) {
    console.log('Hide Completed Courses');
    var element = document.getElementById(slide);
    element.classList.toggle('closed');
    if (this.hideComp == true) {
      this.hideComp = false;
    } else {
      this.hideComp = true;
    }
  }

  goToCourseDetails(passData, index) {
    // console.log('DATA--->',passData);
    // console.log('Index--->',index);
    this.LSP.setcourseDetailList(passData);
    if(this.featureConfig.activity.modernui){
      this.router.navigate(['../../module-activity'], { relativeTo: this.routes });
    }
    if(this.featureConfig.activity.standardui){
      // this.router.navigate(['../../module-activity'], { relativeTo: this.routes });
      this.router.navigate(['course-detail'], { relativeTo: this.routes });
    }
    // this.router.navigate(['course-detail'], { relativeTo: this.routes });

  }

  // togglelike(item) {
  //   console.log('ITEM--->', item);
  //   if (!item.isLike) {
  //     item.isLike = true;
  //     item.noLikes = item.noLikes + 1;
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 1,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.addCourseFeedback;
  //     this.LSP.addCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //     if (item.isLike) {
  //       if (item.isDislike) {
  //         if (item.noDislikes > 0) {
  //           item.noDislikes = item.noDislikes - 1;
  //         }
  //         this.toggledislike(item);
  //         item.isDislike = false;
  //       }
  //     }
  //   } else {
  //     item.isLike = false;
  //     if (item.noLikes > 0) {
  //       item.noLikes = item.noLikes - 1;
  //     }
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 2,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.removeFeedback;
  //     this.LSP.removeCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //   }
  // }

  // toggledislike(item) {
  //   console.log('ITEM--->', item);
  //   if (!item.isDislike) {
  //     item.isDislike = true;
  //     item.noDislikes = item.noDislikes + 1;
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 2,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.addFeedback;
  //     this.LSP.addCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //     if (item.isDislike) {
  //       if (item.isLike) {
  //         if (item.noLikes > 0) {
  //           item.noLikes = item.noLikes - 1;
  //         }
  //         this.togglelike(item);
  //         item.isLike = false;
  //       }
  //     }
  //   } else {
  //     item.isDislike = false;
  //     if (item.noDislikes > 0) {
  //       item.noDislikes = item.noDislikes - 1;
  //     }
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 1,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.removeFeedback;
  //     this.LSP.removeCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //   }
  // }
  submitCourseFeedback(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId:userDetails.id,
      eId:  this.employeeId,
      tId: userDetails.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.addCourseFeedback;
      this.LSP.addCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }

  removeCourseFeedback(courseData, feedbackType) {
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.userDetails.id,
      eId: this.employeeId,
      tId: userDetails.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.removeCourseFeedback;
      this.LSP.removeCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }
  // enrolInCourse(data) {
  //   console.log('EnrolCourseData', data);
  //   var params = {
  //     enrolId: data.enrolId,
  //     ttt_workflowId: data.ttt_workflowId,
  //   };
  //   var url = ENUM.domain + ENUM.url.selfEnrol;
  //   this.LSP.selfEnrolCourse(url, params)
  //     .then((result: any) => {
  //       console.log('DATA--->', result);
  //       if (result.type == true) {
  //         this.toastr.success('Enrol Request Sent Successfully', 'Success!');
  //       } else {
  //         this.toastr.error(
  //           'Please try again after sometime',
  //           'Error Occured!'
  //         );
  //       }
  //     })
  //     .catch(result => {
  //       console.log('ServerResponseError :', result);
  //     });
  // }

  enrolInCourse(data) {
    console.log('EnrolCourseData', data);
    const params = {
      enrolId: data.enrolId,
      ttt_workflowId: data.ttt_workflowId,
    };
    const url = ENUM.domain + ENUM.url.selfEnrol;
    this.LSP.selfEnrolCourse(url, params)
      .then((result: any) => {
        console.log('DATA--->', result);
        const responseData = result['data'];
        if (result.type === true) {
          if (responseData.length !== 0 && responseData[0].length !== 0) {
            const enrollData = responseData[0][0];
            if (enrollData['approvalFlag'] === 1) {
              data['approvalStatus'] = 'Pending';
              this.toastr.success('Enrol Request Sent Successfully', 'Success!').onHidden.subscribe(()=>
              this.toastr.clear());
            }else if (enrollData['approvalFlag'] === 0 && enrollData['enrolId'] === 0 ) {
              this.toastr.warning('Unable to enroll', 'Warning!').onHidden.subscribe(()=>
              this.toastr.clear());
            }else {
              // if (this.view === 1){
              //   let currentSelfPosition = null;
              //   let openSelfPosition = null;
              //   let elementToBeSplicedAt = null;
              //   if (this.cardArray.length !== 0) {
              //     for (let index = 0; index < this.cardArray.length - 1; index++) {
              //       const element = this.cardArray[index];
              //       if (element.compStatus === 'CURRENT') {
              //         currentSelfPosition = index;
              //       }
              //       if (element.compStatus === 'OPEN') {
              //         openSelfPosition = index;
              //         for (let indexj = 0; indexj < element.list.length - 1; indexj++) {
              //           if (element.list[indexj].id === data.id) {
              //             elementToBeSplicedAt = indexj;
              //           }
              //         }
              //       }
              //     }
              //   }
              //   console.log('currentSelfPosition ===>', currentSelfPosition);
              //   console.log('openSelfPosition ===>', openSelfPosition);
              //   console.log('elementToBeSplicedAt ===>', elementToBeSplicedAt);
              //   this.cardArray[currentSelfPosition].list.push(data);
              //   this.cardArray[openSelfPosition].list.splice(elementToBeSplicedAt, 1);
              //   // Push to all enrolled
              //   const allEnrolledData  = this.tabArray[0];
              //   if (allEnrolledData.list.length !== 0){
              //     for (let index = 0; index < allEnrolledData.list.length - 1; index++) {
              //       const element = allEnrolledData.list[index];
              //       if (element.compStatus === 'CURRENT') {
              //         element.list.push(data);
              //       }
              //     }
              //   }
              // }
              this.searchData();
              this.toastr.success('Enrolled Successfully', 'Success!').onHidden.subscribe(()=>
              this.toastr.clear());
            }
          }
        } else {
          this.toastr.warning(
            'Please try again after sometime',
            'Error Occured!'
          ).onHidden.subscribe(()=>
          this.toastr.clear());
        }
      })
      .catch(result => {
        console.log('ServerResponseError :', result);
      });
  }

  // HM Carousal
  certificateArray = [
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo!',
      message2: 'You received a Security certificate',
      noofcourses: 4
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 1 !',
      message2: 'You received a Security certificate 1 ',
      noofcourses: 5
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 2 !',
      message2: 'You received a Security certificate 2 ',
      noofcourses: 6
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 3 !',
      message2: 'You received a Security certificate 3',
      noofcourses: 1
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 4 !',
      message2: 'You received a Security certificate 4',
      noofcourses: 2
    }
  ];
  // Slider
  currentIndex1 = 0;
  speed1 = 5000;
  infinite1 = true;
  direction1 = 'right';
  directionToggle1 = true;
  autoplay1 = true;

  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 768,
      number: 1
    },
    {
      width: 1024,
      number: 3
    }
  ];
  follow = true;
  enablePan = true;

  index = 8;
  speed = 3000;
  infinite = true;
  direction = 'right';
  directionToggle = true;
  autoplay = true;
  avatars = '123456789'.split('').map((x, i) => {
    const num = i;
    // const num = Math.floor(Math.random() * 1000);
    return {
      url: `https://picsum.photos/600/400/?${num}`,
      title: `${num}`
    };
  });

  ProgressInWhole(val){
    if (val) {
      return Math.round(val);
    }
    else{
      return 0;
    }
  }

  workflowClicked(data){
    this.LSP.setWorkflowData(data);
    this.router.navigate(['../../workflow_learn'], { relativeTo: this.routes });
  }
  bindBackgroundImage(img){
    if(img === null || img === '' || img === 'assets/images/courseicon.jpg' ) {
      return 'url(' + 'assets/images/open-book-leaf.jpg' + ')';
    } else {
      return 'url(' + img + ')';
    }
  }

  ngOnDestroy() {
    if (this.isSearchSubscribe) {
      this.isSearchSubscribe.unsubscribe();
    }
  }
}
