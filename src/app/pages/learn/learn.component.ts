import { Component, OnInit, ViewEncapsulation, ViewChild  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer , SafeStyle, SafeResourceUrl } from '@angular/platform-browser';
import { LearnServiceProvider } from '../../service/learn-service';
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { formatDate } from '@angular/common';
import { LogServices, LogEnum } from '../../service/logservice';
import { NbSidebarService } from '@nebular/theme';
import { NgxHmCarouselBreakPointUp } from 'ngx-hm-carousel';
import { feature } from '../../../environments/feature-environment';

@Component({
  selector: 'ngx-learn',
  templateUrl: './learn.component.html',
  styleUrls: ['./learn.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LearnComponent implements OnInit{
  toggle() {
    this.sidebarService.toggle(false, 'menu-sidebar');
  }
  @ViewChild('tab1')
  tabRef: any;
  setUpdateFlag: string = 'ALL_ENROLLED';

  public tabArray: any = [];
  public CourseTypeTabArray: any = [];
  public CourseDateTabArray: any = [];
  public constTabArray: any = [];
  public view: number = 1;
  public viewPath: string = 'assets/images/calendar.svg';
  public cardArray: any = [];
  public enrolStatus: string = 'ENROL';
  public hideComp: boolean = true;
  public selectedFeatures: any = [];
  public tabChangedFlag = false;
  public monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  public monthNames1 = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  public selectedIndex = 0;
  public areas = {};
  public userDetails: any;
  public employeeId: any;
  public prevTab: any;
  public passedString;

  CourseWorkflowArray: any = [];
  workflowCardArray: any = [];
  featureConfig = feature;
  constructor(
    public LSP: LearnServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    public logService: LogServices,
    public logEnum: LogEnum,
    private sanitizer: DomSanitizer,
    private sidebarService: NbSidebarService,
  ) {
    this.areas = this.logService.getArea('learn');

    if (localStorage.getItem('userDetails')) {
      this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    }
    if (localStorage.getItem('employeeId')) {
      this.employeeId = localStorage.getItem('employeeId');
    }
    var logAction = {
      action: this.logEnum.viewed,
      target: 'learn',
      instanceid: '',
      other:
        'Learn ' + this.logEnum.viewed + ' by ' + this.userDetails.username,
      crud: 'r',
      area: 'learn'
    };
    this.logService.saveLogged(logAction);
    this.getLearnData();
      this.routes.queryParams.subscribe(
        (params) => {
          console.log(params);
          if (params['courseType']) {
            this.setUpdateFlag = params['courseType'];
            this.tabChangedFlag = true;
          }
        }
      )
  }


  getLearnData(){
    this.spinner.show();
    this.tabArray = [];
    this.selectedFeatures = [];
    var param = {
      eId: this.employeeId,
      tId: this.userDetails.tenantId,
      uId: this.userDetails.id,
    };
    var url = ENUM.domain + ENUM.url.getLearn;
    this.LSP.getLearnData(url, param)
      .then(result => {
        var temp: any = result;
        console.log(temp,"tempdata")
        if (
          temp.data != undefined ||
          temp.data != null ||
          temp.data1 != undefined ||
          temp.data1 != null
        ) {
          if (temp.data.length > 0 && temp.data1.length) {
            console.log('DATA1--->', temp.data1);
            console.log('DATA--->', temp.data);
            console.log('Workflow DATA--->', temp.workflowData);
            for (let index = 0; index < temp.data.length; index++) {
             this.selectedFeatures.push(temp.data[index].CourseType);
            }
            console.log ("List Of Tabs ===>",  this.selectedFeatures);
            this.CourseTypeTabArray = temp.data;
            this.CourseDateTabArray = temp.data1;
            this.CourseWorkflowArray = temp.workflowData;
            let tArray = temp.data;
            if (tArray.length > 0) {
              for (let i = 0; i < tArray.length; i++) {
                let tabTempArray = tArray[i];
                if (tabTempArray.CourseType !== 'SELF') {
                  this.tabArray.push(tArray[i]);
                }
              }
              // this.prevTab = this.tabArray[0];
            }
            console.log('TABARRAY--->', this.tabArray);
            this.cardArray = this.getFormattedCourseData(temp.data[0].list);
            console.log('DATA--->', this.CourseTypeTabArray);
            // console.log('DATA--->',this.tabGroup.selectedIndex);
            // this.workflowCardArray = this.getFormattedCourseData(temp.CourseWorkflowArray);
            // console.log('Formatted workflow DATA--->', this.workflowCardArray);
            this.spinner.hide();
          } else {
            this.toastr.warning('Unable to get course data', 'Warning!').onHidden.subscribe(()=>
            this.toastr.clear());
            this.spinner.hide();
          }
        }
      })
      .catch(result => {
        this.spinner.hide();
        console.log('ServerResponseError :', result);
      });
  }
  getFormattedCourseData(DataCourseType) {
    for (let i = 0; i < DataCourseType.length; i++) {
      let temporary = DataCourseType[i];
      for (let j = 0; j < temporary.list.length; j++) {
        temporary.list[j].courseDate = this.formatDate(
          temporary.list[j].courseDate);
        if (!temporary.list[j].earnpoints) {
          temporary.list[j].earnpoints = 0;
        }
        if (!temporary.list[j].cpoints) {
          temporary.list[j].cpoints = 0;
        }
        temporary.list[j].endDate = this.formatDate(temporary.list[j].endDate);
        temporary.list[j].completed = this.formatCompData(
          temporary.list[j].completed_courses,
          temporary.list[j].total_courses,
        );
        if (temporary.list[j].workflowid) {
          temporary.list[j].type1Image = 'assets/images/blue-cog-hi.png';
        } else if (temporary.list[j].courseType == 'MANDATORY') {
          temporary.list[j].type1Image = 'assets/images/orangeLearn.svg';
        } else if (temporary.list[j].courseType == 'RECOMMENDED') {
          temporary.list[j].type1Image = 'assets/images/mountLearn.svg';
        } else {
          // if(temporary.list[j].courseType == 'ASPIRATIONAL'){
          temporary.list[j].type1Image = 'assets/images/tickLearn.svg';
        }
        if (temporary.list[j].courseType2 == 'Online') {
          temporary.list[j].type2Image = 'assets/images/online.svg';
        } else if (temporary.list[j].courseType2 == 'Classroom') {
          temporary.list[j].type2Image = 'assets/images/classroom.svg';
        }
        if (temporary.list[j].courseType2 == 'Preonboarding') {
          temporary.list[j].type2Image = 'assets/images/online.svg';
        }
        // if (temporary.list[j].percComplete == 0) {
        //   temporary.list[j].percComplete = 2;
        // }
      }
    }
    return DataCourseType;
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + d.getMonth(),
      day = '' + d.getDate(),
      year = d.getFullYear();

    month = this.monthNames[month];
    if (day.length < 2) day = '0' + day;

    var dateString = [day, month, year].join(' ');

    return dateString;
  }

  formatCompData(ccvar, tcvar) {
    ccvar = '' + ccvar;
    tcvar = '' + tcvar;

    if (ccvar.length < 2) ccvar = '0' + ccvar;
    if (tcvar.length < 2) tcvar = '0' + tcvar;

    var retString = ccvar + ' / ' + tcvar;

    return retString;
  }

  ngOnInit() {}

  switchView() {
    this.spinner.show();
    console.log('VIEW---->', this.view);
    if (this.view == 1) {
      var monIndex = new Date().getMonth();
      var compString = this.monthNames[monIndex];
      this.view = 2;
      this.viewPath = 'assets/images/list.svg';
      this.tabArray = [];
      this.tabArray = this.CourseDateTabArray;
      for (var i = 0; i < this.tabArray.length; i++) {
        var cD = this.tabArray[i].CourseMon;
        for (var j = 0; j < this.monthNames1.length; j++) {
          if (cD == this.monthNames1[j]) {
            var index = j;
          }
        }
        var cDM = this.monthNames[index];
        var cDY = new Date(this.tabArray[i].CourseDate)
          .getFullYear()
          .toString()
          .substr(-2);
        this.tabArray[i].CourseDateMonth = cDM;
        this.tabArray[i].CourseDateYear = cDY;
        if (compString == this.tabArray[i].CourseDateMonth) {
          this.selectedIndex = i;
        } else {
          this.selectedIndex = 0;
        }
      }
      this.cardArray = [];
      this.cardArray = this.getFormattedCourseData(
        this.tabArray[this.selectedIndex].list
      );
      console.log('TABARRAY===>', this.cardArray);
      this.spinner.hide();
    } else if (this.view == 2) {
      this.view = 1;
      this.viewPath = 'assets/images/calendar.svg';
      this.tabArray = [];
      this.tabArray = this.CourseTypeTabArray;
      this.cardArray = this.getFormattedCourseData(this.tabArray[0].list);
      console.log('TabArray===>',this.tabArray);
      this.spinner.hide();
    }
  }
  // workFlowdemo =   {
  //   'approvalStatus': null,
  //   'approverId': null,
  //   'cat_id': 1,
  //   'category': 'demoedit  category 1234678',
  //   'compStatus': 'CURRENT',
  //   'completed': '03 / 04',
  //   'completed_courses': 3,
  //   'courseDate': 'NaN  NaN',
  //   'courseDate1': 'March 7',
  //   'courseDesc':'This is demo course1',
  //   'courseMon': 'March',
  //   'courseMonth': '01 March 2019',
  //   'courseTitle': 'Demo Course23',
  //   'courseType': 'MANDATORY',
  //   'courseType2': 'Online',
  //   'cpoints': 110,
  //   'earnpoints':268,
  //   'endDate': '08 Mar 2019',
  //   'enrolId': 36,
  //   'id':40,
  //   'isBookmark':0,
  //   'isDislike': 0,
  //   'isLike': 1,
  //   'nextStage':"",
  //   'noDislikes':0,
  //   'noLikes':2,
  //   'noOfmodules':2,
  //   'percComplete': 75,
  //   'startDate': '2019-03-07T08:04:33.000Z',
  //   'tags': 'DC3,Demo3,Course3',
  //   'total_courses': 4,
  //   'type1Image' : 'assets/images/orangeLearn.svg',
  //   'type2Image': 'assets/images/online.svg',
  //   'typeImage': 'https://bhaveshedgetest.s3.ap-south-1.amazonaws.com/cat2.png',
  //   'IsworkFlow': false,
  // }
  tabChanged(event) {
    if(event){
      this.spinner.show();
      console.log('TABCHANGED!!!!!', event.index);
      this.cardArray = [];
      this.cardArray = this.getFormattedCourseData(
        this.tabArray[event.index].list,
      );
      console.log('CARDARRAY--->', this.cardArray);
      // this.cardArray[0].list.push(this.workFlowdemo);
      this.spinner.hide();
      let tabItem = this.tabArray[event.index];
      console.log("Tab Array", tabItem);
      if (this.prevTab) {
        var logAction = {
          action: this.logEnum.changed,
          target: 'learn',
          instanceid: '',
          other:
            this.userDetails.username +
            ' ' +
            this.logEnum.changed +
            ' course tabs from ' +
            this.prevTab.CourseType +
            ' To ' +
            tabItem.CourseType,
          crud: 'r',
          area: 'learn',
        };
        this.logService.saveLogged(logAction);
      }
      this.prevTab = tabItem;
      if (this.setUpdateFlag !== 'ALL_ENROLLED' && this.tabChangedFlag){
        // this.setUpdateFlag = '';
        // this.tabChanged({index: 1 });
        const index: number = this.getChangeTabIndex(this.setUpdateFlag);
        this.changeTab(index);
        this.tabChangedFlag = false;
      }
    }
  }
  hideCompCourse(slide) {
    console.log('Hide Completed Courses');
    var element = document.getElementById(slide);
    element.classList.toggle('closed');
    if (this.hideComp == true) {
      this.hideComp = false;
    } else {
      this.hideComp = true;
    }
  }

  goToCourseDetails(passData, index) {
     console.log('DATA--->', passData);
     console.log('Index--->', index);
    this.LSP.setcourseDetailList(passData);
    if(this.featureConfig.activity.standardui){
      this.router.navigate(['course-detail'], { relativeTo: this.routes });
    }
    if(this.featureConfig.activity.modernui) {
      this.router.navigate(['../module-activity'], { relativeTo: this.routes });
    }
    //

  }

  // togglelike(item) {
  //   console.log('ITEM--->', item);
  //   if (!item.isLike) {
  //     item.isLike = true;
  //     item.noLikes = item.noLikes + 1;
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 1,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.addCourseFeedback;
  //     this.LSP.addCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //     if (item.isLike) {
  //       if (item.isDislike) {
  //         if (item.noDislikes > 0) {
  //           item.noDislikes = item.noDislikes - 1;
  //         }
  //         this.toggledislike(item);
  //         item.isDislike = false;
  //       }
  //     }
  //   } else {
  //     item.isLike = false;
  //     if (item.noLikes > 0) {
  //       item.noLikes = item.noLikes - 1;
  //     }
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 2,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId
  //     };
  //     var url = ENUM.domain + ENUM.url.removeFeedback;
  //     this.LSP.removeCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //   }
  // }

  // toggledislike(item) {
  //   console.log('ITEM--->', item);
  //   if (!item.isDislike) {
  //     item.isDislike = true;
  //     item.noDislikes = item.noDislikes + 1;
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 2,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.addFeedback;
  //     this.LSP.addCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //     if (item.isDislike) {
  //       if (item.isLike) {
  //         if (item.noLikes > 0) {
  //           item.noLikes = item.noLikes - 1;
  //         }
  //         this.togglelike(item);
  //         item.isLike = false;
  //       }
  //     }
  //   } else {
  //     item.isDislike = false;
  //     if (item.noDislikes > 0) {
  //       item.noDislikes = item.noDislikes - 1;
  //     }
  //     var userDetails = JSON.parse(localStorage.getItem('userDetails'));
  //     var params = {
  //       cId: item.id,
  //       fType: 1,
  //       uId: userDetails.id,
  //       eId: this.employeeId,
  //       tId: userDetails.tenantId,
  //     };
  //     var url = ENUM.domain + ENUM.url.removeFeedback;
  //     this.LSP.removeCourseFeedback(url, params)
  //       .then(result => {
  //         console.log('DATA--->', result);
  //       })
  //       .catch(result => {
  //         console.log('ServerResponseError :', result);
  //       });
  //   }
  // }

  submitCourseFeedback(courseData, feedbackType) {
    if (feedbackType === 1) {
      if (courseData.isLike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isLike = 1;
        if (courseData.isDislike === 1) {
          this.removeCourseFeedback(courseData, 2);
          courseData.isDislike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isLike = 0;
      }
    } else if (feedbackType === 2) {
      if (courseData.isDislike === 0) {
        this.addCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 1;
        if (courseData.isLike === 1) {
          this.removeCourseFeedback(courseData, 1);
          courseData.isLike = 0;
        }
      } else {
        this.removeCourseFeedback(courseData, feedbackType);
        courseData.isDislike = 0;
      }
    }
  }

  addCourseFeedback(courseData, feedbackType) {
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId:userDetails.id,
      eId:  this.employeeId,
      tId: userDetails.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.addCourseFeedback;
      this.LSP.addCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }

  removeCourseFeedback(courseData, feedbackType) {
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    const param = {
      cId: courseData.id,
      fType: feedbackType,
      uId: this.userDetails.id,
      eId: this.employeeId,
      tId: userDetails.tenantId,
    };
    // this.load.presentLoading('Please wait....');
    var url = ENUM.domain + ENUM.url.removeCourseFeedback;
      this.LSP.removeCourseFeedback(url, param)
        .then(result => {
          console.log('DATA--->', result);
        })
        .catch(result => {
          console.log('ServerResponseError :', result);
        });
  }

  enrolInCourse(data) {
    console.log('EnrolCourseData', data);
    var params = {
      enrolId: data.enrolId,
      ttt_workflowId: data.ttt_workflowId,
    };
    var url = ENUM.domain + ENUM.url.selfEnrol;
    this.LSP.selfEnrolCourse(url, params)
      .then((result: any) => {
        console.log('DATA--->', result);
        const responseData = result['data'];
        if (result.type === true) {
          if (responseData.length !== 0 && responseData[0].length !== 0) {
            const enrollData = responseData[0][0];
            if (enrollData['approvalFlag'] === 1) {
              data['approvalStatus'] = 'Pending';
              this.toastr.success('Enrol Request Sent Successfully', 'Success!').onHidden.subscribe(()=>
              this.toastr.clear());
            }else if (enrollData['approvalFlag'] === 0 && enrollData['enrolId'] === 0 ) {
              this.toastr.warning('Unable to enroll', 'Warning!').onHidden.subscribe(()=>
              this.toastr.clear());
            }else {
              // if (this.view === 1){
              //   let currentSelfPosition = null;
              //   let openSelfPosition = null;
              //   let elementToBeSplicedAt = null;
              //   if (this.cardArray.length !== 0) {
              //     for (let index = 0; index < this.cardArray.length - 1; index++) {
              //       const element = this.cardArray[index];
              //       if (element.compStatus === 'CURRENT') {
              //         currentSelfPosition = index;
              //       }
              //       if (element.compStatus === 'OPEN') {
              //         openSelfPosition = index;
              //         for (let indexj = 0; indexj < element.list.length - 1; indexj++) {
              //           if (element.list[indexj].id === data.id) {
              //             elementToBeSplicedAt = indexj;
              //           }
              //         }
              //       }
              //     }
              //   }
              //   console.log('currentSelfPosition ===>', currentSelfPosition);
              //   console.log('openSelfPosition ===>', openSelfPosition);
              //   console.log('elementToBeSplicedAt ===>', elementToBeSplicedAt);
              //   this.cardArray[currentSelfPosition].list.push(data);
              //   this.cardArray[openSelfPosition].list.splice(elementToBeSplicedAt, 1);
              //   // Push to all enrolled
              //   const allEnrolledData  = this.tabArray[0];
              //   if (allEnrolledData.list.length !== 0){
              //     for (let index = 0; index < allEnrolledData.list.length - 1; index++) {
              //       const element = allEnrolledData.list[index];
              //       if (element.compStatus === 'CURRENT') {
              //         element.list.push(data);
              //       }
              //     }
              //   }
              // }
              this.getLearnData();
              this.toastr.success('Enrolled Successfully', 'Success!').onHidden.subscribe(()=>
              this.toastr.clear());
            }
          }
        } else {
          // this.toastr.error(
          //   'Please try again after sometime',
          //   'Error Occured!'
          // );
          this.toastr.warning('Unable to get course List', 'Warning!').onHidden.subscribe(()=>
          this.toastr.clear());
        }
      })
      .catch(result => {
        console.log('ServerResponseError :', result);
      });
  }

  // HM Carousal
  certificateArray = [
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo!',
      message2: 'You received a Security certificate',
      noofcourses: 4,
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 1 !',
      message2: 'You received a Security certificate 1 ',
      noofcourses: 5,
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 2 !',
      message2: 'You received a Security certificate 2 ',
      noofcourses: 6,
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 3 !',
      message2: 'You received a Security certificate 3',
      noofcourses: 1,
    },
    {
      certificateimg: './assets/images/Certificate.png',
      greetings: 'Woohooo 4 !',
      message2: 'You received a Security certificate 4',
      noofcourses: 2,
    }
  ];
  // Slider
  currentIndex1 = 0;
  speed1 = 5000;
  infinite1 = true;
  direction1 = 'right';
  directionToggle1 = true;
  autoplay1 = true;

  breakpoint: NgxHmCarouselBreakPointUp[] = [
    {
      width: 768,
      number: 1
    },
    {
      width: 1024,
      number: 3
    }
  ];
  follow = true;
  enablePan = true;

  index = 8;
  speed = 3000;
  infinite = true;
  direction = 'right';
  directionToggle = true;
  autoplay = true;
  avatars = '123456789'.split('').map((x, i) => {
    const num = i;
    // const num = Math.floor(Math.random() * 1000);
    return {
      url: `https://picsum.photos/600/400/?${num}`,
      title: `${num}`
    };
  });

  ProgressInWhole(val){
    if (val) {
      return Math.round(val);
    }
    else{
      return 0;
    }
  }

  changeTab(index){
    // console.log("value", RefTab);
    // console.log("value", JSON.stringify(selectAfterAdding));
    // RefTab.selectedIndex = 1;
    console.log("value of tabs ===>", this.tabRef);

    this.tabRef.selectedIndex = index;
    // console.log ("value of tabs ===>", this.tabRef._tabs._results);
  }
  getChangeTabIndex(value){
    return this.selectedFeatures.indexOf(value);
    // console.log ('Get Change Tab Index ===>', index);
  }

  demo = 123;
  workflowClicked(data){
    console.log(data,"workflow")
    this.LSP.setWorkflowData(data);
    this.router.navigate(['../workflow_learn'], { relativeTo: this.routes });
  }

  bindBackgroundImage(img):SafeStyle{
    if(img === null || img === '' || img === 'assets/images/courseicon.jpg' ) {
      // const profilePicUrl: SafeResourceUrl = 'assets/images/open-book-leaf.jpg';
      //  const style = `background-image: url(${profilePicUrl})`;
      // // sanitize the style expression
      // return this.sanitizer.bypassSecurityTrustStyle(style);
      return 'url(' + 'assets/images/open-book-leaf.jpg' + ')';
    } else {
    // const profilePicUrl:SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(img);
    //   const style = `background-image: url(${profilePicUrl})`;
    //  // sanitize the style expression
    //  return this.sanitizer.bypassSecurityTrustStyle(style);
      return 'url(' + img + ')';
    }
  }
}
