import { Component, OnInit, OnDestroy } from "@angular/core";
import { LearnServiceProvider } from "../../service/learn-service";
import { CourseDetailServiceProvider } from "../../service/course-detail-service";
import { CourseServiceProvider } from "../../service/course-service";
import { Router, ActivatedRoute } from "@angular/router";
import { ENUM } from "../../service/enum";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";

@Component({
  selector: "ngx-course-details",
  templateUrl: "./course-details.component.html",
  styleUrls: ["./course-details.component.scss"]
})
export class CourseDetailsComponent implements OnInit, OnDestroy {
  resetPwdForm: FormGroup;
  password: any;
  courseDetailSummary: any = [];
  courseDetailArray: any = [];
  courseDetailTabArray: any = [];
  ActiveTab: any;
  passCheck: boolean = false;
  passKey: string;
  passWrong: boolean = false;
  submitted: boolean = false;
  checkResponse: any;
  displayInstructions: boolean = false;
  activeRole: string;
  serviceButtonDisabled: boolean = false;
  disabledGoToCourse: boolean = false;
  isServiceDisabled: any;

  constructor(
    public LSP: LearnServiceProvider,
    public CDSP: CourseDetailServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    public CSP: CourseServiceProvider,
    private spinner: NgxSpinnerService,
    public toastr: ToastrService,
    private formBuilder: FormBuilder,
  ) {
    // this.getCourseDetails();
    this.isServiceDisabled = this.LSP.subject.subscribe(() => {
      console.log('Data changed ===>');
      this.getCourseDetails();
    });
    // if(this.courseDetailSummary.percComplete == 100){
    // 	this.courseDetailArray = this.CDSP.getCompletedList();
    // }else if(this.courseDetailSummary.percComplete == 2){
    // 	this.courseDetailArray = this.CDSP.getUpcomingCourse();
    // }else{
    // 	this.courseDetailArray = this.CDSP.getPartialCompletedList();
    // }
    // console.log('DATA--->',this.courseDetailArray);
  }

  ngOnInit() {
    this.resetPwdForm = this.formBuilder.group({
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }


  getCourseDetails(){
    this.spinner.show();
    this.courseDetailSummary = this.LSP.getcourseDetailList();
    console.log('this.courseDetailSummary', this.courseDetailSummary);
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.activeRole = localStorage.getItem('roleName');
    console.log('this.activeRole:', this.activeRole);
    console.log('userDetails course detail page:', userDetails);
    var param = {
      courseId: this.courseDetailSummary.id,
      tId: userDetails.tenantId,
      eId: this.courseDetailSummary.enrolId,
    };
    var url = ENUM.domain + ENUM.url.getLearnModules;
    this.CDSP.getCourseDetailsData(url, param)
      .then(result => {
        var temp: any = result;
        console.log('fet course modules:', result);
        this.courseDetailTabArray = temp.data;
        if (this.courseDetailTabArray.length > 0) {
          for (var i = 0; i < this.courseDetailTabArray.length; i++) {
            var temp: any = i + 1;
            this.courseDetailTabArray[i].id = i;
            // this.courseDetailTabArray[i].moduleName = 'Module' + ' ' + temp;
          }
          this.ActiveTab = this.courseDetailTabArray[0];
          this.courseDetailArray = this.courseDetailTabArray[0].list;
          for (var i = 0; i < this.courseDetailArray.length; i++) {
            var temp: any = i + 1;
            // this.courseDetailArray[i].img =
            //   "assets/images/open-book-leaf-2.jpg";
            this.courseDetailArray[i].img = 'assets/images/open-book-leaf-2.jpg';
            // if(this.courseDetailArray[i].activity_type === 'Quiz'){
            //   this.courseDetailArray[i].img = 'assets/images/activity_image/quiz.jpg';
            // }
            // else {
            //   this.courseDetailArray[i].img = 'assets/images/open-book-leaf-2.jpg';
            // }
            // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
            this.courseDetailArray[i].activityTempId = temp;
            var tempCourseI = this.courseDetailArray[i];
            //var tempDate = new Date(this.courseDetailArray[i].completed);
            for (let j = 0; j < this.courseDetailArray.length; j++) {
              var tempCourseJ = this.courseDetailArray[j];
              //this.courseDetailArray[j].activityDuration = this.makeTimeReady(this.courseDetailArray[j].activityDuration.split('.')[0]);
              if (tempCourseI.dependentActId != 0 || !tempCourseI.dependentActId) {
                if (tempCourseI.dependentActId == tempCourseJ.activityId) {
                  if (tempCourseJ.wCompleted === 0) {
                    tempCourseI.dependant = tempCourseJ.activityName;
                    // break;
                  }
                }
              }
            }
          }
          console.log("CD===>", this.courseDetailArray);
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.toastr.warning("No Data Available", "Warning!").onHidden.subscribe(()=>
          this.toastr.clear());
          // this.gotoLearn();
          this.goBack();
        }
      })
      .catch(result => {
        console.log("ServerResponseError :", result);
        this.spinner.hide();
      });
    console.log('SummaryDATA--->', this.courseDetailSummary);
  }

  bookMark(index) {
    if (this.courseDetailArray[index].bookbookmarked) {
      this.courseDetailArray[index].bookbookmarked = false;
    } else {
      this.courseDetailArray[index].bookbookmarked = true;
    }
  }

  gotoLearn() {
    this.router.navigate(["../../learn"], { relativeTo: this.routes });
  }

  goBack() {
    // this.router.navigate(["../../learn"], { relativeTo: this.routes });
    // window.history.back();
    this.router.navigate(['../'], { relativeTo: this.routes });
  }

  goToCourse(data) {
    this.disabledGoToCourse = true;
    this.spinner.show();
    this.CSP.setCourse(this.courseDetailSummary, data);
    console.log("DATA---->", data);
    if (data.activityTypeId == 5) {
      const userDetails = JSON.parse(localStorage.getItem("userDetails"));
      const employeeId = localStorage.getItem('employeeId');
      var param = {
        qId: data.quizId,
        tId: userDetails.tenantId,
        uId: employeeId,
        cId: data.courseId,
        mId: data.moduleId,
        aId: data.activityId,
        enId : data.enrolId,
      };
      var url = ENUM.domain + ENUM.url.checkQuizUser;
      this.CDSP.getCourseDetailsData(url, param)
        .then(result => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log("CHECk QUIZ FOR USER---->", temp.data[0]);
              this.checkResponse = temp.data[0];
              if (temp.data[0].flag == "true") {
                console.log("PassCheck===>", temp.data[0]);
                if (temp.data[0].qPassword == 0) {
                  if (this.checkResponse.instructions) {
                    this.disabledGoToCourse = false;
                    this.spinner.hide();
                    this.displayInstructions = true;
                  } else {
                    this.displayInstructions = false;
                    this.disabledGoToCourse = false;
                    this.spinner.hide();
                    this.router.navigate(["quiz"], { relativeTo: this.routes });
                  }
                } else {
                  this.disabledGoToCourse = false;
                  this.spinner.hide();
                  this.passCheck = true;
                  this.passKey = temp.data[0].passWord;
                }
              } else {
                this.disabledGoToCourse = false;
                this.spinner.hide();
                this.disabledGoToCourse = false;
                let message = temp.data[0].msg + ' ' + temp.data[0].nextAttemptDate;
                this.toastr.warning(message, "Warning!").onHidden.subscribe(()=>
                this.toastr.clear());
              }
            } else {
              this.disabledGoToCourse = false;
              this.spinner.hide();
              this.toastr.warning(
                "Something went wrong please try again later",
                "Error!"
              ).onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            this.spinner.hide();
            this.disabledGoToCourse = false;
            this.toastr.warning(
              "Something went wrong please try again later",
              "Error!"
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          this.spinner.hide();
          this.disabledGoToCourse = false;
          console.log("ServerResponseError :", result);
          this.toastr.warning(
            "Something went wrong please try again later",
            "Error!"
          ).onHidden.subscribe(()=>
          this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (data.activityTypeId == 6) {
      this.disabledGoToCourse = false;
      var employeeId = localStorage.getItem("employeeId");
      var userDetails = JSON.parse(localStorage.getItem("userDetails"));
      // var params = {
      //   fId: data.feedbackId,
      //   tId: userDetails.tenantId,
      //   eId: employeeId,
      // };
      // we pass this beacaues of right filteration eg : same feedback added to 2 courses if one course feedback
      // compleated then above expresion gives count =1 and
      //we cannot attempt feedback fpr second course we apply this filter
      var params = {
        fId: data.feedbackId,
        tId: userDetails.tenantId,
        eId: employeeId,
        cId: data.courseId,
        mId: data.moduleId,
        aId: data.activityId,
        enId : data.enrolId,
      };
      var url = ENUM.domain + ENUM.url.checkFeedbackUser;
      this.CDSP.getCourseDetailsData(url, params)
        .then(result => {
          var temp: any = result;
          if (temp.data !== null || temp.data !== undefined) {
            if (temp.data.length > 0) {
              console.log("CHECk FEEDBACK FOR USER---->", temp.data[0]);
              if (temp.data[0].flag == "true") {
                this.CDSP.setFeedbackSummary(data);
                this.disabledGoToCourse = false;
                this.spinner.hide();
                this.router.navigate(["feedback"], { relativeTo: this.routes });
              } else {
                this.disabledGoToCourse = false;
                this.spinner.hide();
                this.toastr.warning(temp.data[0].msg, "Warning!").onHidden.subscribe(()=>
                this.toastr.clear());
              }
            } else {
              this.disabledGoToCourse = false;
              this.spinner.hide();
              this.toastr.warning(
                "Something went wrong please try again later",
                "Error!"
              ).onHidden.subscribe(()=>
              this.toastr.clear());
            }
          } else {
            this.disabledGoToCourse = false;
            this.toastr.warning(
              "Something went wrong please try again later",
              "Error!"
            ).onHidden.subscribe(()=>
            this.toastr.clear());
          }
        })
        .catch(result => {
          this.disabledGoToCourse = false;
          this.spinner.hide();
          console.log("ServerResponseError :", result);
          this.toastr.warning(
            "Something went wrong please try again later",
            "Error!"
          ).onHidden.subscribe(()=>
          this.toastr.clear());
          // this.spinner.hide();
        });
    } else if (data.activityTypeId == 9) {
      this.disabledGoToCourse = false;
      this.spinner.hide();
      this.router.navigate(["mark-attendance"], { relativeTo: this.routes });
    }else if(data.activityTypeId == 11){
      this.disabledGoToCourse = false;
      this.spinner.hide();
      this.router.navigate(['course'], { relativeTo: this.routes });
    } else {
      this.disabledGoToCourse = false;
      this.spinner.hide();
      if (data && data['reference'] && data['reference'] !== ''){
        this.router.navigate(['course'], { relativeTo: this.routes });
      }else{
        this.toastr.warning(
          'No Data Available in this activity',
          'Warning!'
        ).onHidden.subscribe(()=>
        this.toastr.clear());
      }
    }
  }

  tabChanged(data) {
    this.spinner.show();
    console.log("DATA--->", data);
    this.ActiveTab = data;

    this.courseDetailArray = this.courseDetailTabArray[data.id].list;
    // for (var i = 0; i < this.courseDetailArray.length; i++) {
    //   var temp: any = i + 1;

    //   // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
    // }

    for (var i = 0; i < this.courseDetailArray.length; i++) {
      var temp: any = i + 1;
      // this.courseDetailArray[i].img =
      //   "assets/images/open-book-leaf-2.jpg";
      this.courseDetailArray[i].img = 'assets/images/open-book-leaf-2.jpg';
      // if(this.courseDetailArray[i].activity_type === 'Quiz'){
      //   this.courseDetailArray[i].img = 'assets/images/activity_image/quiz.jpg';
      // }
      // else {
      //   this.courseDetailArray[i].img = 'assets/images/open-book-leaf-2.jpg';
      // }
      // this.courseDetailArray[i].activityName = 'Activity' + ' ' + temp;
      this.courseDetailArray[i].activityTempId = temp;
      var tempCourseI = this.courseDetailArray[i];
      //var tempDate = new Date(this.courseDetailArray[i].completed);
      for (let j = 0; j < this.courseDetailArray.length; j++) {
        var tempCourseJ = this.courseDetailArray[j];
        //this.courseDetailArray[j].activityDuration = this.makeTimeReady(this.courseDetailArray[j].activityDuration.split('.')[0]);
        if (tempCourseI.dependentActId != 0 || !tempCourseI.dependentActId) {
          if (tempCourseI.dependentActId == tempCourseJ.activityId) {
            if (tempCourseJ.wCompleted === 0) {
              tempCourseI.dependant = tempCourseJ.activityName;
              // break;
            }
          }
        }
      }
    }
    this.spinner.hide();

  }

  close() {
    this.passCheck = false;
  }
  closeInstrction() {
    this.displayInstructions = false;
  }
  get f() {
    return this.resetPwdForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.serviceButtonDisabled = true;
    if (this.resetPwdForm.invalid) {
      return;
    } else {
      console.log("EnteredPassword--->", this.resetPwdForm.value.password);
      console.log("PasswordKey--->", this.passKey);
      if (this.resetPwdForm.value.password == this.passKey) {
        this.passCheck = false;
        if (this.checkResponse.instructions) {
          this.serviceButtonDisabled = true;
          this.displayInstructions = true;
        } else {
          this.serviceButtonDisabled = true;
          this.displayInstructions = false;
          this.router.navigate(["quiz"], { relativeTo: this.routes });
        }
      } else {
        this.passWrong = true;
      }
    }
  }

  onSubmit1() {
    this.router.navigate(["quiz"], { relativeTo: this.routes });
  }

  makeTimeReady(time) {
    let temp = time.split(":");
    let string = "";
    if (parseInt(temp[0], 10) > 0) {
      string = string + parseInt(temp[0], 10) + 'Hrs';
    }
    if (parseInt(temp[1], 10) > 0) {
      string = string + ' ' + parseInt(temp[1], 10) + 'Min';
    }
    if (parseInt(temp[2], 10) > 0) {
      string = string + ' ' + parseInt(temp[2], 10) + 'Seconds';
    }
    return string;
  }

  bindBackgroundImage(detail) {
    // console.log(detail,"detail")
    // detail.img = 'assets/images/open-book-leaf-2.jpg';
    // return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};
    if (detail.activity_type === 'Quiz') {
      detail.img = 'assets/images/activity_image/quiz.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.activity_type === 'Feedback') {
      detail.img = 'assets/images/activity_image/feedback.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'video' || detail.mimeType === 'video/mp4') {
      detail.img = 'assets/images/activity_image/video.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'audio' || detail.mimeType === 'audio/mpeg') {
      detail.img = 'assets/images/activity_image/audio.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'application' && detail.mimeType === 'application/zip') {
      detail.img = 'assets/images/activity_image/scrom.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'application' && detail.mimeType === 'application/pdf') {
      detail.img = 'assets/images/activity_image/pdf.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'kpoint' || detail.mimeType === 'embedded/kpoint') {
      detail.img = 'assets/images/activity_image/video.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'image') {
      detail.img = 'assets/images/activity_image/image.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.referenceType === 'application' && detail.mimeType === 'application/x-msdownload') {
      detail.img = 'assets/images/activity_image/url.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    } else if (detail.formatId == 9) {
      detail.img = 'assets/images/activity_image/practice_file.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    }
    else if (detail.formatId == 10) {
      detail.img = 'assets/images/activity_image/ppt.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    }
    else if (detail.formatId == 11) {
      detail.img = 'assets/images/activity_image/excel.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    }
    else if (detail.formatId == 12) {
      detail.img = 'assets/images/activity_image/word.jpg';
      return { 'background-image': 'url(' + detail.img + ')', 'background-position': 'left' };
    }
     else {
      detail.img = 'assets/images/open-book-leaf-2.jpg';
      return { 'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')' };
    }
  }
  finddependetactivity(detail) {
    if (detail.wCompleted == 1) {
      return 3;
    } else if (detail.wCompleted == 0) {
      if (detail.dependentActId == 0) {
        if (detail.byTrainer == 1) {
          return 2;
        } else {
          return 1;
        }
      } else if (detail.dependentActId != 0) {
        if (detail.dependentActCompStatus == 0) {
          return 4;
        } else if (detail.dependentActCompStatus == 1) {
          return 1;
        }
      } if (detail.byTrainer == 1) {
        return 2;
      }
    }
    // if(detail.dependentActId == 0){
    //   if(detail.wCompleted == 0 ){
    //     return 1;
    //   }
    // } else if(detail.wCompleted == 0 && detail.byTrainer == 1){
    //   return 2;
    // } else if(detail.wCompleted == 1){
    //   return 3;
    // } else if( detail.dependentActId != 0){
    //   if( detail.wCompleted == 0 && detail.dependentActCompStatus == 1){
    //     return 1;
    //   } else if(detail.wCompleted == 0 && detail.dependentActCompStatus == 0){
    //     return 4
    //   }
    // }
  }

  ngOnDestroy(){
    if(this.isServiceDisabled){
      this.isServiceDisabled.unsubscribe();
    }
  }
  preformActionOnAcitivty(event){
    if (event) {
      switch(event.type){
        case 'goToActivityPage': this.goToCourse(event.cardData);
                                 break;
        // case 'enrolSelf': this.enrolSelf(event.cardData);
        //                   break;
        // case 'likeDislike': this.likeDislike(event.cardData, event.liked);
        //                     break;
        // case 'goToWorflow': this.goToWorkflowDetails(event.cardData);
        //                     break;
      }
    }
}
}

