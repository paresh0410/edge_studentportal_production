import { Component, OnInit, ViewEncapsulation, ViewChild, Input } from '@angular/core';
import { PartnerService } from '../../service/partner.service';
import { ENUM } from '../../service/enum';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogModule,
  MatSelect
} from "@angular/material";
import { Observable } from "rxjs";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';


import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { take, takeUntil } from 'rxjs/operators';
import {VERSION} from '@angular/material';

interface Trainers {
  id: number;
  trainerName: string;
 }


@Component({
  selector: 'ngx-partner-nominee-call',
  templateUrl: './partner-nominee-call.component.html',
  styleUrls: ['./partner-nominee-call.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PartnerNomineeCallComponent implements OnInit {
  userDetails: any = [];
  mnomineedetail: any = [];
  upcomingcall: any = [];
  pastcall: any = [];
  ucall: boolean = false;
  pcall: boolean = false;
  editcall: boolean = false;
  calladdedit: any;
  calldetail: any = {};
  title: any;
  caid: any;
  min = new Date();
  submitted: boolean;
  addCallForm: FormGroup;
  search: any;
  public trainerCtrl: FormControl = new FormControl();
  public trainerFilterCtrl: FormControl = new FormControl();
  @ViewChild("singleSelect") singleSelect: MatSelect;
  version = VERSION;
  // compareWith: boolean;
  @Input() type = 'text';
  @Input() placeholderLabel = 'Search & Select Trainer';
  /** list of banks */
  private trainerlist: Trainers[] = [
    // {name: 'Bank A (Switzerland)', id: 'A'},
    // {name: 'Bank B (Switzerland)', id: 'B'},
    // {name: 'Bank C (France)', id: 'C'},
    // {name: 'Bank D (France)', id: 'D'},
    // {name: 'Bank E (France)', id: 'E'},
    // {name: 'Bank F (Italy)', id: 'F'},
    // {name: 'Bank G (Italy)', id: 'G'},
    // {name: 'Bank H (Italy)', id: 'H'},
    // {name: 'Bank I (Italy)', id: 'I'},
    // {name: 'Bank J (Italy)', id: 'J'},
    // {name: 'Bank K (Italy)', id: 'K'},
    // {name: 'Bank L (Germany)', id: 'L'},
    // {name: 'Bank M (Germany)', id: 'M'},
    // {name: 'Bank N (Germany)', id: 'N'},
    // {name: 'Bank O (Germany)', id: 'O'},
    // {name: 'Bank P (Germany)', id: 'P'},
    // {name: 'Bank Q (Germany)', id: 'Q'},
    // {name: 'Bank R (Germany)', id: 'R'} 
  ]

  /** list of banks filtered by search keyword */
  public filteredTrainers: ReplaySubject<Trainers[]> = new ReplaySubject<Trainers[]>(1);

  /** Subject that emits when the component has been destroyed. */
  private _onDestroy = new Subject<void>();
  minDatefrom: any;
  minDateTo: any;

  constructor(public partner: PartnerService, private formBuilder: FormBuilder, public spinner: NgxSpinnerService, private datePipe: DatePipe,
    public Toastr: ToastrService) {
      this.search = {};
    // this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
    // this.mnomineedetail = this.partner.nominee;
  }

  ngOnInit() {
    this.min.setDate(this.min.getDate() - 1);
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
    this.mnomineedetail = this.partner.nominee;
    this.calllist();
    this.gettrainerlist();

    this.addCallForm = this.formBuilder.group({
      trainerV: ['', Validators.required],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      callDate: ['', [Validators.required]],
    });

    this.setInitialValue();

  }


  // ngAfterViewInit() {
  // }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * Sets the initial value after the filteredBanks are loaded initially
   */
  private setInitialValue() {
    this.filteredTrainers
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {          
        // setting the compareWith property to a comparison function 
        // triggers initializing the selection according to the initial value of 
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredBanks are loaded initially 
        // and after the mat-option elements are available
        console.log('this.singleSelect', this.singleSelect);
        if ( this.singleSelect ) {
          this.singleSelect.compareWith = (a: Trainers, b: Trainers) => a.id === b.id;
        }
        
      });
  }

  private filterTrainer() {
    if (!this.trainerlist) {
      return;
    }
    // get the search keyword
    let search = this.trainerFilterCtrl.value;
    if (!search) {
      this.filteredTrainers.next(this.trainerlist.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredTrainers.next(
      this.trainerlist.filter(trainer => trainer.trainerName.toLowerCase().indexOf(search) > -1)
    );
  }

  get f() {
    return this.addCallForm.controls;
  } //form fields getter for easy access


  gettrainerlist() {
    const param = {
      partnerId: this.userDetails.referenceId,
      tId: this.userDetails.tenantId,
    };
    const url = ENUM.domain + ENUM.url.partner_trainer_list;
    this.partner.get(url, param).then(res => {
      console.log(res);
      if (res['type'] === true) {
        try {
          this.trainerlist = res['data'];
          console.log('this.trainerlist',this.trainerlist);
          
        // set initial selection
        this.trainerCtrl.setValue(this.trainerlist[10]);

        // load the initial bank list
        this.filteredTrainers.next(this.trainerlist.slice());
    
        // listen for search field value changes
        this.trainerFilterCtrl.valueChanges
          .pipe(takeUntil(this._onDestroy))
          .subscribe(() => {
            this.filterTrainer();
          });


        } catch(e){
            console.log(e);
         }
      }
    }, err => {
      console.log(err);
    });
  }
  back() {
    window.history.back();
  }
  calllist() {
    // this.spinner.show();
    const param = {
      empId: this.mnomineedetail.empId,
      tId: this.userDetails.tenantId,
    };
    const url = ENUM.domain + ENUM.url.partner_nominee_call;

    this.partner.get(url, param).then(res => {
      console.log(res);
      // this.spinner.hide();
      if (res['type'] === true) {
        try {
          for (let i = 0; i < res['upcoming_call'].length; i++) {
            res['upcoming_call'][i].enddate = this.formdate(res['upcoming_call'][i].callEndDate);
            res['upcoming_call'][i].startdate = this.formdate(res['upcoming_call'][i].callStartDate);
            res['upcoming_call'][i].calldate = this.formdate(res['upcoming_call'][i].callDateTime);
          }
          this.upcomingcall = res['upcoming_call'];
          console.log('this.upcomingcall',this.upcomingcall);
          if (this.upcomingcall.length === 0) {
            this.ucall = true;
          }
          for (let i = 0; i < res['past_call'].length; i++) {
            res['past_call'][i].enddate = this.formdate(res['past_call'][i].callEndDate);
            res['past_call'][i].startdate = this.formdate(res['past_call'][i].callStartDate);
            res['past_call'][i].calldate = this.formdate(res['past_call'][i].callDateTime);
          }
          this.pastcall = res['past_call'];
          console.log('this.pastcall',this.pastcall);
          if (this.pastcall.length === 0) {
            this.pcall = true;
          }
        } catch (e){
          console.log(e);
         }
      }
    }, err => {
      // this.spinner.hide();
      console.log(err);
    });
  }
  formdate(date) {
    if (date) {
      const formatted = this.datePipe.transform(date, "dd-MMM-yyyy");
      return formatted;
    }
  }

  onSearchFromDateChange(data){
    this.minDatefrom = data;
}

onSearchToDateChange(data){
  this.minDateTo = data;
}

  Edit(call, value) {
    this.calladdedit = value;
    this.calldetail = call;
    console.log('Call details', this.calldetail);
    if (this.calladdedit === 1) {
      this.title = 'Edit Call';
    this.calldetail.trainerId = call.trainerId;
    this.calldetail.callStartDate = call.callStartDate;
    this.calldetail.callEndDate = call.callEndDate;
    this.calldetail.callDateTime = call.callDateTime;

    } else if (this.calladdedit === 0) {
      this.title = 'Add Call';
      this.calldetail.trainerId = 0;
      this.calldetail.callStartDate = null;
      this.calldetail.callEndDate = null;
      this.calldetail.callDateTime = null;
      // this.calldetail = {};
    }

    this.editcall = true;
    console.log('Trainer Id' + this.calldetail.trainerId);
  }
  closemodel() {
    this.editcall = false;
  }
  savecall(calldetail) {
  this.submitted = true;
    if (this.addCallForm.invalid) {
      this.spinner.hide();
      return;
    } else {
      console.log(calldetail);
      if (this.calladdedit === 0) {
        this.caid = 0;
      } else {
        this.caid = this.calldetail.callId;
      }
      const param = {
        cId: this.caid,
        empId: calldetail.empId,
        sDate: calldetail.callStartDate,
        eDate: calldetail.callEndDate,
        cDateTime: calldetail.callDateTime,
        trainId: calldetail.trainerId,
        tId: this.userDetails.tenantId,
        userId: this.userDetails.id,
      };
      console.log(param);
      const url = ENUM.domain + ENUM.url.Add_Edit_trainer;
      this.partner.get(url, param).then(res => {
        console.log(res);
        if (res['type'] === true) {
          this.Toastr.success(res['data'][0].msg);
          this.editcall = false;
          this.calllist();
        }
      }, err => {
        console.log(err)
      });

      this.addCallForm.reset();
    }
    }
  
}
