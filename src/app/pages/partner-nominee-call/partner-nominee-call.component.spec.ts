import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerNomineeCallComponent } from './partner-nominee-call.component';

describe('PartnerNomineeCallComponent', () => {
  let component: PartnerNomineeCallComponent;
  let fixture: ComponentFixture<PartnerNomineeCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerNomineeCallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerNomineeCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
