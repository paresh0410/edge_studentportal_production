import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { CourseServiceProvider } from "../../service/course-service";
import { ENUM } from "../../service/enum";
import { NgxSpinnerService } from "ngx-spinner";
import { VideoPlayerService } from "../video-player/videoplayer.service";
import { AudioPlayerService, Track } from "../audio-player/audioplayer.service";
import { KPointService } from "../k-point/k-point.service";
import { DocumentViewerService } from "../document-viewer/documentviewer.service";
import { UserService } from "../../service/user.service";
import { ToastrService } from "ngx-toastr";
import { WebinarConfig } from "../webinar-multiple/webinar-entity";
import { feature } from "../../../environments/feature-environment";
import { MultiLanguageService } from "../../service/multi-language.service";
// import { Location } from '@angular/common';
@Component({
  selector: "ngx-course",
  templateUrl: "./course.component.html",
  styleUrls: ["./course.component.scss"],
})
export class CourseComponent implements OnInit {
  trainerData: any;
  usersData: any;
  mimetype: any;
  courseDataSummary: any;
  courseDataContent: any;
  courseTitle: any;
  audioTrack: Track;
  audioTrackList: Track[] = [];
  courseDetail: any;
  url: any;
  showdiv: boolean = false;
  googleProvider = ["pdf", "txt", "doc", "docx"];
  microsoftProvider = ["xlsx", "odt", "odp", "ods", "ppt", "pptx"];
  provider: boolean = false;
  document: any;
  description: any;
  userdetail: any;
  completionstatus: any;
  courseFrom: any;
  voiceCall: boolean;
  videoCall: boolean;
  tenantId: number;
  documentURL: any =
    // tslint:disable-next-line: max-line-length
    "https://docs.google.com/gview?url=https://bhaveshedgetest.s3.amazonaws.com/EDGE-UserManual_v1-01.pdf&embedded=true";
  courseReadyFlag: boolean;
  activityName: any;
  config: WebinarConfig;
  isYoutube: boolean;
  featureConfig;
  constructor(
    public CSP: CourseServiceProvider,
    private router: Router,
    private routes: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public cdf: ChangeDetectorRef,
    public videoservice: VideoPlayerService,
    public audioservice: AudioPlayerService,
    public documentservice: DocumentViewerService,
    public toastr: ToastrService,
    public kpointservice: KPointService,
    public userService: UserService,
    public multiLanguageService: MultiLanguageService
  ) // public location :Location,
  {
    this.completionstatus = "UP";
    this.provider = false;
    this.showdiv = false;
    this.spinner.show();
    this.courseReadyFlag = false;
    this.courseDetail = {
      fileinfo: {
        documenttype: null,
      },
      reference: null,
      show: false,
    };

    this.courseDataContent = CSP.getDataContent();
    this.courseDataSummary = CSP.getDataSummary();
    console.log("DATA--->", this.courseDataContent);
    console.log("DATA--->", this.courseDataSummary);
    // if(this.courseDataContent ){
    //   localStorage.setItem("courseDataContent", JSON.stringify(this.courseDataContent));
    // }else {
    //   this.courseDataContent = JSON.parse(localStorage.getItem("courseDataContent"))
    // }
    // if(this.courseDataSummary)
    // {
    //   localStorage.setItem("courseDataSummary", JSON.stringify(this.courseDataSummary));
    // }else {
    //   this.courseDataSummary = JSON.parse(localStorage.getItem("courseDataSummary"))
    // }
    if (localStorage.getItem("userdetail")) {
      this.userdetail = JSON.parse(localStorage.getItem("userdetail"));
      this.tenantId = this.userdetail.tenantId;
    }
    this.featureConfig = feature;
    // if(this.courseDataSummary){
    if (
      (this.courseDataSummary && this.courseDataSummary["courseFrom"] == 2) ||
      (this.courseDataSummary && this.courseDataSummary["courseFrom"] == 3)
    ) {
      if (this.courseDataSummary["courseTitle"]) {
        this.courseTitle = this.courseDataSummary["courseTitle"];
      }

      let params = {
        suptId: this.courseDataContent.supertypeId,
        subtId: this.courseDataContent.activityTypeId,
        aId: this.courseDataContent.activityId,
        uId: this.userdetail.userId,
        eId: null,
        enId: this.courseDataContent.enrolId
          ? this.courseDataContent.enrolId
          : null,
        // tempId: true,
      };
      if (Number(this.userdetail["roleId"]) === 8) {
        params["tempId"] = true;
      } else {
        params["tempId"] = false;
      }
      var url = ENUM.domain + ENUM.url.getModuleActivity;
      this.CSP.getCourseData(url, params)
        .then((result: any) => {
          if (result.type) {
            console.log("RESULT-->", result);
            this.courseDetail = result.data;
            if (params["tempId"]) {
              this.courseDetail["enrolId"] = this.courseDataContent.enrolId;
            }
            this.courseDetail['activityTypeId'] = this.courseDataContent.activityTypeId;
            this.courseDetail['actIcon'] = this.bindActivityIcon(this.courseDetail);
            this.setActivityDataAtStart();
            this.setAcitivityDataValues();
          } else {
            this.spinner.hide();
            this.toastr
              .warning(
                "Something went wrong please try again later",
                "Warning!"
              )
              .onHidden.subscribe(() => this.toastr.clear());
            // this.toastr.warning(result.data, 'Server Warning!');
            this.goBack();
          }
        })
        .catch((result) => {
          this.spinner.hide();
          this.courseDetail = null;
          this.toastr
            .warning("Something went wrong please try again later", "Warning!")
            .onHidden.subscribe(() => this.toastr.clear());
          console.log("ServerResponseError :", result);
        });
    } else {
      if (this.courseDataSummary && this.courseDataSummary["courseTitle"]) {
        this.courseTitle = this.courseDataSummary["courseTitle"];
      }
      let param;
      if (this.courseDataContent) {
        param = {
          suptId: this.courseDataContent.supertypeId,
          subtId: this.courseDataContent.activityTypeId,
          aId: this.courseDataContent.activityId,
          uId: this.userdetail.userId,
          eId: this.courseDataContent.enrolId
            ? this.courseDataContent.enrolId
            : null,
          enId: this.courseDataContent.enrolId
            ? this.courseDataContent.enrolId
            : null,
          tempId: this.courseDataContent.enrolId ? false : true,
        };
        if (this.courseDataSummary["courseFrom"] == 1) {
          if (Number(this.userdetail["roleId"]) === 8) {
            param["tempId"] = true;
          } else {
            param["tempId"] = false;
          }
        }
        var url = ENUM.domain + ENUM.url.getModuleActivity;
        if (
          this.courseDataContent.activityTypeId === 11 &&
          this.courseDataSummary["courseFrom"] == 1
        ) {
          this.voiceCall = true;
          this.videoCall = true;
          this.courseFrom = "trainer";
          this.getUsersData(this.courseDataContent);
          this.description = this.courseDataContent.cDescription;
          this.showdiv = true;
        } else {
          this.CSP.getCourseData(url, param)
            .then((result: any) => {
              if (result.type) {
                console.log("RESULT-->", result);
                this.courseDetail = result.data;
                this.courseDetail['activityTypeId'] = this.courseDataContent.activityTypeId;
            this.courseDetail['actIcon'] = this.bindActivityIcon(this.courseDetail);
                this.setActivityDataAtStart();
                this.setAcitivityDataValues();
              } else {
                this.spinner.hide();
                this.toastr
                  .warning(
                    "Something went wrong please try again later",
                    "Warning!"
                  )
                  .onHidden.subscribe(() => this.toastr.clear());
                this.goBack();
              }
            })
            .catch((result) => {
              this.toastr
                .warning(
                  "Something went wrong please try again later",
                  "Warning!"
                )
                .onHidden.subscribe(() => this.toastr.clear());
              this.spinner.hide();
              this.courseDetail = null;
              console.log("ServerResponseError :", result);
            });
        }
      }
    }
    // }else {
    //   this.goBack();
    // }
    console.log(
      "course=============================>>>>>>>>>>>>>",
      this.courseTitle
    );
  }

  ngOnInit() {}

  goBack() {
    // if (this.courseDataSummary.courseFrom === 1) {
    //   this.router.navigate(["../../../trainer-dashboard/trainer-details"], {
    //     relativeTo: this.routes
    //   });
    // } else if (this.courseDataSummary.courseFrom === 2) {
    //   this.router.navigate(["../../nomination"], {
    //     relativeTo: this.routes
    //   });
    // } else if (this.courseDataSummary.courseFrom === 3) {
    //   // this.router.navigate(["../../nomination"], {
    //   //   relativeTo: this.routes
    //   // });
    //  window.history.back();
    // } else {
    //   this.router.navigate(["../../../learn/course-detail"], {
    //     relativeTo: this.routes
    //   });
    // }
    //  this.location.back();

    this.router.navigate(["../"], { relativeTo: this.routes });
  }

  setAcitivityDataValues() {
    // this.imageError.flag = false;
    this.resetDependancyContainer();
    if (!this.courseDetail.completionstatus) {
      this.courseDetail.completionstatus = this.completionstatus;
    }
    // if(this.courseDetail && Array.isArray(this.courseDetail['multiLanguageList']){
    //   this.set
    // }
    if (this.courseDetail.fileinfo.documenttype === "video") {
      this.videoservice.setVideoTrack(this.courseDetail);
      if (
        !this.courseDetail.completionstatus &&
        this.courseDetail.completionstatus !== "Y"
      ) {
        this.courseDetail.completionstatus = this.completionstatus;
      }
      //this.courseDetail.contenttime = this.videoservice.getVideoDuration();
      this.videoservice.setActivity(this.courseDetail);
    } else if (this.courseDetail.fileinfo.documenttype === "audio") {
      this.audioTrack = {
        name: this.courseDetail.activityName,
        artist: this.courseDetail.activityName,
        url: this.courseDetail.reference,
        cover: "assets/images/cover1.jpg",
      };
      this.audioTrackList = [];
      this.audioTrackList.push(this.audioTrack);
      this.audioservice.setTrack(this.audioTrackList);
      if (
        !this.courseDetail.completionstatus &&
        this.courseDetail.completionstatus !== "Y"
      ) {
        this.courseDetail.completionstatus = this.completionstatus;
      }
      this.audioservice.setActivity(this.courseDetail);
      //this.courseDetail.contenttime = this.audioservice.getAudioDuration();
    } else if (this.courseDetail.fileinfo.documenttype === "image") {
      // this.courseDetail.completionstatus = "Y";
      // this.CSP.saveActivityCompletion(this.courseDetail);
      // this.saveActivityCompletionData(this.courseDetail);
    } else if (
      this.courseDetail.fileinfo.documenttype === "application" &&
      this.courseDetail.fileinfo.mimetype == "application/pdf"
    ) {
      let provider = this.findprovider(this.courseDetail.reference);
      this.documentservice.setDocument(this.courseDetail.reference, provider);
      this.provider = true;
      // this.courseDetail.completionstatus = "Y";
      // this.CSP.saveActivityCompletion(this.courseDetail);
      // this.saveActivityCompletionData(this.courseDetail);
      // .cdf.detectChanges();
    } else if (this.courseDetail.fileinfo.documenttype === "kpoint") {
      this.kpointservice.setActivity(this.courseDetail);
    } else if (this.courseDataContent.activityTypeId === 11) {
      if (this.courseDataSummary["courseFrom"] === undefined) {
        this.voiceCall = false;
        this.videoCall = false;
        this.gettrainersData(this.courseDetail);
        this.courseFrom = "learner";
      } else {
        this.voiceCall = true;
        this.videoCall = true;
        this.courseFrom = "trainer";
        this.getUsersData(this.courseDetail);
      }
    } else if (this.courseDetail.reference && this.courseDetail.formatId == 6) {
      this.isYoutube = this.fnYoutubeVideo(this.courseDetail.reference);
      this.fnYoutubeId(this.courseDetail.reference);
    }
    this.description = this.courseDetail.description;
    this.showdiv = true;
    // this.cdf.detectChanges();
    // console.log('DATA===>',temp.data);
    this.spinner.hide();
    // this.CSP.saveActivityCompletion(this.courseDetail);
  }
  findprovider(filename) {
    if (!filename) return null;
    let fileformat = this.getFileFormat(filename);
    let provider = "google";
    if (this.googleProvider.indexOf(fileformat) >= 0) {
      provider = "google";
    } else if (this.microsoftProvider.indexOf(fileformat) >= 0) {
      provider = "microsoft";
    }
    return provider;
  }

  getFileFormat(filename) {
    if (!filename) return null;
    return filename.split(".").pop();
  }

  saveKPointComp(event) {
    const activity = this.courseDetail;
    // const activity = this.playerService.getActivity();
    if (activity.completionstatus === "Y") {
      activity.completionstatus = "Y";
    } else if (event.status === "Y") {
      activity.completionstatus = "Y";
    } else {
      activity.completionstatus = "UP";
    }
    activity.contenttime = event.duration;
    activity.contentwatchedtime = event.currentTime;

    // if (this.saveActCompWorking === false) {
    // 	this.saveActivityCompletionData(activity);
    // }
  }

  ngOnDestroy(): void {}

  // this is for get trainer data and learnerdata
  gettrainersData(data) {
    // console.log('gettrainersData', data);
    const params = {
      cId: data.courseId,
      mId: data.moduleId,
      tId: data.tenantId,
    };
    const url = ENUM.domain + ENUM.url.getTrianerList;
    this.CSP.gettrianers(url, params)
      .then((result: any) => {
        if (result.type == true) {
          this.usersData = result["data"][0];
          this.config = {
            voice: this.voiceCall,
            video: this.videoCall,
            creator: this.userdetail.roleId === 7 ? true : false,
            areaId: null,
            instanceId: null,
            tenantId: null,
            type: null,
            recording: false,
            allowCalls: true,
            startDate: "",
            EndDate: "",
          };
          this.courseReadyFlag = true;
          this.spinner.hide();
          console.log(" this.trainerData --->", this.trainerData);
          // this.toastr.success('Enrol Request Sent Successfully', 'Success!');
        } else {
          // this.toastr.error(
          //   'Please try again after sometime',
          //   'Error Occured!'
          // );
          this.toastr
            .warning("Something went wrong please try again later", "Warning!")
            .onHidden.subscribe(() => this.toastr.clear());
        }
      })
      .catch((result) => {
        this.toastr
          .warning("Something went wrong please try again later", "Warning!")
          .onHidden.subscribe(() => this.toastr.clear());
        console.log("ServerResponseError :", result);
      });
  }
  getUsersData(data) {
    console.log("getUsersList", data);
    const params = {
      cId: data.courseId,
      aId: 2,
      tId: this.tenantId,
    };
    const url = ENUM.domain + ENUM.url.getUsersList;
    this.CSP.getUsers(url, params)
      .then((result: any) => {
        console.log("DATA--->", result);
        if (result.type == true) {
          this.usersData = result["data"][0];
          this.config = {
            voice: this.voiceCall,
            video: this.videoCall,
            creator: this.userdetail.roleId === 7 ? true : false,
            areaId: null,
            instanceId: null,
            tenantId: null,
            type: null,
            recording: this.userdetail.roleId === 7 ? true : false,
            allowCalls: true,
            startDate: "",
            EndDate: "",
          };
          this.courseReadyFlag = true;
          this.spinner.hide();
          // this.toastr.success('Enrol Request Sent Successfully', 'Success!');
        } else {
          // this.toastr.error(
          //   'Please try again after sometime',
          //   'Error Occured!'
          // );
          this.toastr
            .warning("Something went wrong please try again later", "Warning!")
            .onHidden.subscribe(() => this.toastr.clear());
        }
      })
      .catch((result) => {
        this.toastr
          .warning("Something went wrong please try again later", "Warning!")
          .onHidden.subscribe(() => this.toastr.clear());
        console.log("ServerResponseError :", result);
      });
  }

  disableCompletionCall = false;

  webinarActivityCompletion(event) {
    // console.log('this.courseFrom', this.courseFrom)
    // console.log('$event', $event);
    // if ($event) {
    //   this.courseDetail.completionstatus = "Y";
    //   // this.CSP.saveActivityCompletion(this.courseDetail);
    //   this.saveActivityCompletionData(this.courseDetail);
    // }
    if (event && !this.disableCompletionCall) {
      this.disableCompletionCall = true;
      if (event.webinarInformation.isCreator === true) {
        if (this.courseDataContent) {
          let params = {
            courseId: this.courseDataContent.courseId,
            moduleId: this.courseDataContent.moduleId,
            activityId: this.courseDataContent.activityId,
            tId: this.tenantId,
          };
          params["userStr"] = this.getJoinedDataForWebinar(
            event.calledParticipantsList
          );
          if (event.type === "callInvitation") {
            params["isCompleted"] = 0;
          }
          if (event.type === "callEnded") {
            params["isCompleted"] = 1;
          }
          const url = ENUM.domain + ENUM.url.saveActivityCompletionWebinar;
          this.CSP.callPostApi(url, params)
            .then((res) => {
              if (res["type"] === true) {
                // this.activityCompRes = res;
                // if (this.courseDetail.activitycompletionid === undefined || !this.courseDetail.activitycompletionid) {
                //   this.courseDetail.activitycompletionid = res['data'][0][0].lastid;
                // }
                this.disableCompletionCall = false;
                console.log("Activity completion res", res);
              } else {
                this.disableCompletionCall = false;
              }
            })
            .catch((result) => {
              // this.toastr.warning('Something went wrong please try again later', 'Warning!');
              this.disableCompletionCall = false;
              console.log("ServerResponseError :", result);
            });
        }
      }
    }
  }

  getJoinedDataForWebinar(calledParticipantsList) {
    const string = calledParticipantsList
      .map(function (elem) {
        return elem.id + "#" + elem.enrolId;
      })
      .join("|");
    return string;
  }
  activityCompRes: any;
  saveActivityCompletionData(activityData) {
    if (Number(this.userdetail["roleId"]) === 8) {
      this.CSP.saveActivityCompletion(activityData).then(
        (res) => {
          if (res["type"] === true) {
            this.activityCompRes = res;
            if (
              this.courseDetail.activitycompletionid === undefined ||
              !this.courseDetail.activitycompletionid
            ) {
              this.courseDetail.activitycompletionid = res["data"][0][0].lastid;
              this.courseDetail.complitionlangId = res["data"][0][0].languageId;
            }
            console.log("Activity completion res", res);
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      // params['tempId'] = false;
    }
  }
  open_browser(course) {
    console.log(course);
    this.url = course.reference;
    if (this.url) {
      this.courseDetail.completionstatus = "Y";
      this.saveActivityCompletionData(this.courseDetail);
      window.open(this.url, "_blank");
    } else {
      this.toastr
        .error("Please try again after sometime", "Error Occured!")
        .onHidden.subscribe(() => this.toastr.clear());
    }
  }
  downloadContent() {
    this.courseDetail.completionstatus = "Y";
    this.saveActivityCompletionData(this.courseDetail);
  }
  fnYoutubeId(url) {
    const VID_REGEX = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    if (url.match(VID_REGEX)) {
      this.youtubeId = url.match(VID_REGEX)[1];
    }
  }
  fnYoutubeVideo(url) {
    if (this.isURL(url)) {
      const list = ["youtu.be", "www.youtube.com"];
      this.isYoutube = false;
      const hostname = new URL(url).hostname;
      const index = list.indexOf(hostname);
      return index >= 0 ? true : false;
    }
  }

  isURL(str) {
    try {
      new URL(str);
    } catch (_) {
      return false;
    }
    return true;
  }
  /**
   * Youtube Player
   */
  player: YT.Player;
  youtubeId: string = "";

  savePlayer(player) {
    this.player = player;
    player.setSize("100%", 500);
    console.log("player instance", player);
  }
  onStateChange(event) {
    console.log("player state", event.data);
    const player = event.target;
    this.courseDetail.contenttime = player.getDuration();
    this.courseDetail.contentwatchedtime = player.getCurrentTime();
    if (this.courseDetail.completionstatus == "Y") {
      this.courseDetail.completionstatus == "Y";
    } else if (
      parseInt(player.getDuration()) == parseInt(player.getCurrentTime())
    ) {
      this.courseDetail.completionstatus = "Y";
    } else {
      this.courseDetail.completionstatus == "UP";
    }
    // this.CSP.saveActivityCompletion(this.courseDetail);
    this.saveActivityCompletionData(this.courseDetail);
  }

  // Multi language
  changeActivityLanguage(languageData, flag) {
    // console.log('Change Activity Language =>', languageData);
    this.isYoutube = false;
    this.showdiv = false;
    this.cdf.detectChanges();
    let multiContentActivity = null;
    if (
      this.courseDetail &&
      Array.isArray(this.courseDetail["multiLanguageList"])
    ) {
      for (
        let index = 0;
        index < this.courseDetail["multiLanguageList"].length;
        index++
      ) {
        const element = this.courseDetail["multiLanguageList"][index];
        if (
          Number(element["languageId"]) === Number(languageData["languageId"])
        ) {
          element["selected"] = true;
          multiContentActivity = element;
        } else {
          element["selected"] = false;
        }
      }
    }
    this.setActivityData(multiContentActivity, flag);

    // this.multiLanguageService.updateActivityLanguage(this.courseDetail);
    // console.log('Change Activity Language =>', languageData);
  }

  setActivityDataAtStart() {
    let multiContentActivity = null;
    if (
      this.courseDetail &&
      Array.isArray(this.courseDetail["multiLanguageList"]) &&
      this.courseDetail["multiLanguageList"].length !== 0
    ) {
      // this.courseDetail["multiLanguageList"][0]['selected'] = true;
      // multiContentActivity = this.courseDetail["multiLanguageList"][0];
      // this.setActivityData(multiContentActivity, false);
      this.changeActivityLanguage(this.courseDetail["multiLanguageList"][0], false);
    }
  }

  setActivityData(multiContentActivity, flag) {
    this.courseDetail["formatId"] = multiContentActivity["formatId"];
    this.courseDetail["defaultlangId"] = multiContentActivity["languageId"];
    // this.courseDetail['defaultlangId'] = multiContentActivity['languageId'];
    this.courseDetail["languageName"] = multiContentActivity["languageName"];
    this.courseDetail["mimeType"] = multiContentActivity["mimeType"];
    this.courseDetail["reference"] = multiContentActivity["reference"];
    this.courseDetail["referenceType"] = multiContentActivity["referenceType"];
    this.courseDetail["fileinfo"] = multiContentActivity["fileinfo"];
    //new change
    this.courseDetail["isDeleted"] = multiContentActivity["isDeleted"];
    this.courseDetail["isDeletedMsg"] = multiContentActivity["isDeletedMsg"];

    //end new change

    if (!flag) {
      if (
        !(
          Number(this.courseDetail.complitionlangId) ===
          Number(multiContentActivity["languageId"])
        )
      ) {
        this.courseDetail["contentwatchedtime"] = 0;
      }
      // this.multiLanguageService.updateActivityLanguage(this.courseDetail);
    }else {
      this.courseDetail["contentwatchedtime"] = 0;
    }

    setTimeout(() => {

      this.setAcitivityDataValues();
    }, 300);
  }
  scormStatus: any;
  getScormStatus(event) {
    this.scormStatus = event;
  }

  imageLoaded(event){
    console.log("Event", event);
    if (event && event.target) {
      const x = event.srcElement.x;
      const y = event.srcElement.y;
      if ((x !== 0 ) && (y !== 0)) {
        const width = event.srcElement.width;
        const height = event.srcElement.height;
        const portrait = height > width ? true : false;
        console.log('Loaded: ', width, height, 'portrait: ', portrait);
        if(this.courseDetail.completionstatus !== 'Y'){
          this.courseDetail.completionstatus = 'Y';
          this.saveActivityCompletionData(this.courseDetail);
        }
      }
    }
  }
  errorHandler(event) {
    console.log(event);
    if(event && event['type']){
      this.sendContentLoadErrorMessage('image', 'Image file corrupted or not accessible. Please Contact site administrator');
    }
    // if(event.type )
    // event.target.src = "https://cdn.browshot.com/static/images/not-found.png";
 }

 bindActivityIcon(detail) {
  // if (detail.activity_type === 'Quiz') {
  if (detail.activityTypeId == 5) {
    return "quiz";
    // } else if ( detail.activity_type  === 'Feedback') {
  } else if (detail.activityTypeId == 6) {
    return "feedback";
  } else if (detail.activityTypeId == 11) {
    return "webinar";
    // } else if ( detail.activity_type === 'Attendance') {
  } else if (detail.activityTypeId == 9) {
    return "attendance";
  } else if (detail.activityTypeId == 1 || detail.activityTypeId == 2) {
    switch (Number(detail.formatId)) {
      case 1: // video
        return "video";
      case 2: // Audio
        return "audio";
      case 3: // PDF
        return "pdf";
      case 4: // K-point
        return "kpoint";
      case 5: // Scrom
        return "scrom";
      case 6: // Youtube
        return "youtube";
      case 7: // Image
        return "image";
      case 8: // External link
        return "url";
      case 9: // Practice file
        return "practice_file";
      case 10: // PPT
        return "ppt";
      case 11: // Excel
        return "excel";
      case 12: // Word
        return "word";
      default:
        return "";
    }
  } else {
    return "";
  }

}
dependencyConatiner = {
  displayFlag: false,
  message: '',
}
sendContentLoadErrorMessage(type, content) {
  switch (type) {
    case 'pdf':
      if ('file_corrupt') {
        this.dependencyConatiner.message = 'PDF file is corrupted. Please Contact site administrator.';
      } else {
        this.dependencyConatiner.message = 'Unable to generate PDF link. Please try again later.';
      }
      this.dependencyConatiner.displayFlag = true;
      break;
    case 'image':
      this.dependencyConatiner.displayFlag = true;
      this.dependencyConatiner.message = content;
      // this.setDependencyConatinerDataReadyNew();
      break;
    default:
      break;
  }

}

resetDependancyContainer() {
  this.dependencyConatiner = {
    displayFlag: false,
    message: '',
  };
}
}
