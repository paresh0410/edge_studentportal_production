import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TrainerAutomationServiceProvider } from '../../service/trainer-automation.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ENUM } from '../../service/enum';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'ngx-level1',
  templateUrl: './level1.component.html',
  styleUrls: ['./level1.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class Level1Component implements OnInit {

  level1Details: any;
  // [
  //   { progName: "Day 1 Induction", location: "Pune", dateOfConduction: "10 Jan", content: 4, trainer: 8, objective: 7, score: 6, participantCount: 6 },
  //   { progName: "Day 1 Induction", location: "Pune", dateOfConduction: "4 Feb", content: 3, trainer: 3, objective: 4, score: 4 , participantCount: 5},
  //   { progNam[e: "Day 1 Induction", location: "Pune", dateOfConduction: "3 Mar", content: 7, trainer: 4, objective: 8, score: 7 , participantCount: 7},
  //   { progName: "Day 1 Induction", location: "Pune", dateOfConduction: "10 Apr", content: 3, trainer: 6, objective: 3, score: 2, participantCount: 4 },
  //   { progName: "Day 1 Induction", location: "Pune", dateOfConduction: "1 May", content: 6, trainer: 3, objective: 4, score: 6 , participantCount: 6},
  // ];

  noFeedData: boolean = false;
  // noAssessData: boolean;
  // noFeedbackModule: boolean;
  npm_data:any;
  objKeys: any = [];
  ActiveTab:any;
  activeTabIndex:number;
  assessmentData: any = [];
  person: any;
  passData: any = [];
  userDetail: any = [];
  Object = Object;
  feedback_data: any;
  avg_asess: any;
  trainerId: any;
  program_Data: any = [];
  sectionArray:any =[];
  displayDataOf:string;
  currentlyOpenedItemIndex = 1;
  // questionObjectKeys:any=[];
  // sectionlist: [{
  //   secId: 1,
  //   secName: 'goldberg',
  //   secavg: 23,
  //   feedback: [{
  //     id: 0,
  //     questionNme: "rate the match",
  //     cnt: 2.00
  //   }, {
  //     id: 1,
  //     questionNme: "rate the stedium",
  //     cnt: 3.00
  //   }]
  // }, {
  //   secId: 2,
  //   secName: 'john cena',
  //   feedback: [{
  //     id: 0,
  //     questionNme: "rate the match",
  //     cnt: 2.00
  //   }, {
  //     id: 1,
  //     questionNme: "rate the stedium",
  //     cnt: 3.00
  //   }]
  // }];
  feedback_section:any=[];
  // items = [
  //   {
  //     secId: 1,
  //     secName: 'goldberg',
  //     secavg: 23,
  //     feedback: [{
  //       id: 0,
  //       questionNme: "goldberg rate the match 123 ",
  //       cnt: 2.00
  //     },
  //     {
  //       id: 1,
  //       questionNme: "rate the stedium 2132",
  //       cnt: 3.00
  //     }]
  //   },
  //   {
  //     secId: 1,
  //     secName: 'goldberg3',
  //     secavg: 23,
  //     feedback: [{
  //       id: 0,
  //       questionNme: "goldberg3 rate the match12312",
  //       cnt: 2.00
  //     },
  //     {
  //       id: 1,
  //       questionNme: "rate the stedium 213213",
  //       cnt: 3.00
  //     }]
  //   },
  //   {
  //     secId: 1,
  //     secName: 'goldberg2',
  //     secavg: 23,
  //     feedback: [{
  //       id: 0,
  //       questionNme: " goldberg2 srate the match231 23 23",
  //       cnt: 2.00
  //     },
  //     {
  //       id: 1,
  //       questionNme: " goldberg2 rate the stedium 321321321",
  //       cnt: 3.00
  //     }]
  //   }
  // ];
  level1data = [
    // {
    //   id: 0,
    //   BatchName: 'row',
    //   coursecode: 123,
    //   sectionlist: [
    //     {
    //     secId: 1,
    //     secName: 'goldberg',
    //     secavg: 23,
    //     feedback: [{
    //       id: 0,
    //       questionNme: "rate the match",
    //       cnt: 2.00
    //     }, {
    //       id: 1,
    //       questionNme: "rate the stedium",
    //       cnt: 3.00
    //     }]
    //   }, {
    //     secId: 2,
    //     secName: 'john cena',
    //     feedback: [{
    //       id: 0,
    //       questionNme: "rate the match",
    //       cnt: 2.00
    //     }, {
    //       id: 1,
    //       questionNme: "rate the stedium",
    //       cnt: 3.00
    //     }]
    //   }]
    // }, {
    //   id: 1,
    //   BatchName: 'smackdown',
    //   coursecode: 1234,
    //   sectionlist: [{
    //     secId: 1,
    //     secName: 'kane',
    //     feedback: [{
    //       id: 0,
    //       questionNme: "rate the match",
    //       cnt: 2.00
    //     }, {
    //       id: 1,
    //       questionNme: "rate the stedium",
    //       cnt: 3.00
    //     }]
    //   }, {
    //     secId: 2,
    //     secName: 'Edge',
    //     feedback: [{
    //       id: 0,
    //       questionNme: "rate the match",
    //       cnt: 2.00
    //     }, {
    //       id: 1,
    //       questionNme: "rate the stedium",
    //       cnt: 3.00
    //     }]
    //   }]
    // }
  ]

  constructor(private router: Router, private routes: ActivatedRoute,
    private trainerService: TrainerAutomationServiceProvider,
    private spinner: NgxSpinnerService, private toaster: ToastrService) {
    this.userDetail = JSON.parse(localStorage.getItem("userdetail"));

    if (localStorage.getItem("userOtherDetails")) {
      let userOtherDetails = JSON.parse(
        localStorage.getItem("userOtherDetails")
      );
      console.log("userOtherDetails", userOtherDetails);
      let userOthersDetails = userOtherDetails[1];

      for (let i = 0; i < userOthersDetails.length; i++) {
        if (userOthersDetails[i].roleId == 7) {
          this.trainerId = userOthersDetails[i].fieldmasterId;
        }
      }
      console.log("trainerId", this.trainerId);
    }
    this.avg_asess = this.trainerService.get_dashboard_assess_data;
    this.feedback_data = this.trainerService.get_feedback_data;
    this.program_Data = this.trainerService.get_TA_program;
    this.npm_data = this.trainerService.get_npa_data;
    this.displayDataOf = this.trainerService.show_data_on_level;
    console.log("Program Data >>", this.program_Data);
    console.log("Average Asess data from dashboard >>", this.avg_asess);
    console.log("Feedback data from dashboard >> ", this.feedback_data);
    console.log("npa data from dashboard >> ", this.npm_data);
  }

  ngOnInit() {
    // this.getFeedbackSection();
    // if( this.displayDataOf == 'feedback'){
    //   this.getCourseList();
    // }
    // if( this.displayDataOf == 'quiz'){
    //   // this.getCourseList();
    //   console.log('Show Quiz');
    // }
    this.getCourseList();
    // this.getAssessmentData();

  }

  // getFeedbackSection(){
  //   let url = ENUM.domain + ENUM.url.getFeedbackSection;
  //   let param = {
  //     'pgmId': this.program_Data.id,
  //     'trnId': this.trainerId,
  //     'tId' : this.userDetail.tenantId,
  //   }
  //   this.trainerService.getFeedbackSection(url, param)
  //     .then((result: any) => {
  //       this.spinner.hide();
  //       console.log('Level 1 getFeedbackSection ==>', result);
  //       if (result.data.length == 0) {
  //         // this.noAssessData = true;
  //         this.noFeedbackModule = true;
  //       } else {
  //         this.sectionArray = result.data;
  //         this.ActiveTab = this.sectionArray[0];
  //         this.activeTabIndex = 0;
  //         this.getFeedbackData(this.ActiveTab)
  //         console.log('Level 1 getFeedbackSection ==>',  this.sectionArray);
  //       }

  //     }).catch(result => {
  //       this.spinner.hide();
  //       console.log('Level 1 data Assessment scores  ==>', result);
  //     })
  // }
  // Feedback object prepare
  passfromAssessTable: any;

  gotoLevel2(assessmentDataItem, i) {
    console.log(assessmentDataItem);

    this.trainerService.show_data_on_level2 = this.displayDataOf;
    assessmentDataItem.programName = this.program_Data.programName;
    this.trainerService.level1_Data = assessmentDataItem;
    // this.passfromAssessTable = {
    //   programName: this.program_Data.programName,
    //   // batch_name: assessmentDataItem.fullName,
    //   batch_code: assessmentDataItem.BatchCode,
    //   course_id: assessmentDataItem.courseId,
    //   avg_feed: assessmentDataItem['Avg Feedback'],
    //   avg_assess: '',
    //   programId:  this.program_Data.id,
    //   nps: assessmentDataItem.NPS,
    // }
    // this.getDataOfAssessment(assessmentDataItem, res => {
    //   console.log(res);
    //   this.passfromAssessTable.avg_assess = res;
    // });
    // console.log("Level 2 data to be passed", this.passfromAssessTable);
    // this.trainerService.level1_Data = assessmentDataItem;
    this.router.navigate(['level-2'], { relativeTo: this.routes });
  }

  getCourseList(){
    let url = ENUM.domain + ENUM.url.getLevel1CourseList;
    let param = {
      'pgmId': this.program_Data.id,
      'trainId': this.trainerId,
      'tId' : this.userDetail.tenantId,
      'flag': this.displayDataOf,
    }
    this.trainerService.getLevel1(url, param)
    .then((result: any) => {
      this.spinner.hide();
      console.log('Course List ===>', result);
      // this.level1data = result;
      // this.level1data = result;
      if(result.type){
        if(result.data.length != 0) {
          this.level1data = result.data;
          this.noFeedData = true;
        }
        else{
          // this.toaster.error('Error', 'Server Error');
          this.toaster.warning('Something went wrong please try again later', 'Warning!');
        }
      }

      // this.level1Details = result.data;

    }).catch(result => {
      this.spinner.hide();
      console.log('Level 1 Course List ===>', result);
      // this.toaster.error('Error', 'No Data Found');
    })
  }

  // getDataOfAssessment(item, cb) {
  //   if (this.assessmentData) {
  //     // this.assessmentData.forEach(element => {
  //     //   if (element.courseId === item.courseId) {
  //     //     console.log("Quiz score", element.quizScore);
  //     //     cb(element.quizScore);
  //     //   }
  //     //   else {
  //     //     cb(0);
  //     //   }
  //     // });
  //     for (let j = 0; j < this.assessmentData.length; j++) {
  //       if (this.assessmentData[j].courseId === item.courseId) {
  //         cb(this.assessmentData[j].quizScore);
  //         break;
  //       } else {
  //         cb(0);
  //       }

  //     }
  //   }

  // }

  // Feedback object prepare


  // Assessment Object prepare
  pass_level1_data: any = [];

  // gotoLevel2ByAssess(item, i) {
  //   this.pass_level1_data = {
  //     programName: this.program_Data.programName,
  //     batch_code: item.courseCode,
  //     course_id: item.courseId,
  //     avg_feed: '',  // to get feedback score,
  //     avg_assess: item.quizScore,
  //     programId:  this.program_Data.id,

  //   };
  //   this.getDataOfFeed(item, res => {
  //     console.log(res);
  //     this.pass_level1_data.avg_feed = res;
  //   })
  //   console.log("Level 2 data to be passed", this.pass_level1_data);
  //   this.trainerService.level1_Data = this.pass_level1_data;
  //   this.router.navigate(['level-2'], { relativeTo: this.routes });
  // }

  // getDataOfFeed(assessmentDataItem, cb) {
  //   if (this.level1Details) {
  //     // this.level1Details.forEach(element => {
  //     //   if (element.courseId === assessmentDataItem.courseId) {
  //     //     cb(element['Avg Score']);
  //     //   }
  //     //   else {
  //     //     cb(0);
  //     //   }
  //     // });
  //       for (let j = 0; j < this.level1Details.length; j++) {
  //         if (this.level1Details[j].courseId === assessmentDataItem.courseId) {
  //          cb(this.level1Details[j]['Avg Score']);
  //         } else {
  //           cb(0);
  //         }

  //      }
  //   }
  // }

  // Assessment Object prepare


  // getFeedbackData(data) {
  //   this.spinner.show();
  //   let url = ENUM.domain + ENUM.url.getLevel1FeedbackData;
  //   let param = {
  //     'pgmId': this.program_Data.id,
  //     'trnId': this.trainerId,
  //     'modeType': 'web',
  //     'tId' : this.userDetail.tenantId,
  //     'secId':data.secId,
  //   }
  //   this.trainerService.getLevel1Feedback(url, param)
  //     .then((result: any) => {
  //       this.spinner.hide();
  //       console.log('Level 1 data feedback ===>', result);
  //       if (result.data.length == 0) {
  //         this.noFeedData = true;
  //       } else {
  //         this.objKeys = Object.keys(result.data[0]);
  //         console.log('objKeys', this.objKeys);
  //         this.noFeedData = false;
  //         this.level1Details = result.data;
  //         console.log("Level 1 details: ", this.level1Details);
  //       }

  //       // this.level1Details = result.data;

  //     }).catch(result => {
  //       this.spinner.hide();
  //       console.log('Level 1 data feedback ===>', result);
  //     })
  // }

  // getAssessmentData() {
  //   this.spinner.show();
  //   let url = ENUM.domain + ENUM.url.getLevel1AssessData;
  //   let param = {
  //     "pgmId": this.program_Data.id,
  //     "trnId": this.trainerId,
  //     "tId": this.userDetail.tenantId,
  //   }
  //   this.trainerService.getLevel1Assess(url, param)
  //     .then((result: any) => {
  //       this.spinner.hide();
  //       console.log('Level 1 data Assessment scores ==>', result);
  //       if (result.data.length == 0) {
  //         this.noAssessData = true;
  //       } else {
  //         // this.objKeys = Object.keys(result.data[0]);
  //         // console.log('objKeys',this.objKeys);
  //         this.noAssessData = false;
  //         this.assessmentData = result.data;
  //         console.log("Level 1 details assessmentData: ", this.assessmentData);
  //       }

  //     }).catch(result => {
  //       this.spinner.hide();
  //       console.log('Level 1 data Assessment scores  ==>', result);
  //     })
  // }

  back() {
    this.trainerService.get_TA_program = this.program_Data.programName;
    this.router.navigate(["../"], { relativeTo: this.routes });
    // window.history.back();
  }

  // tabChanged(data,index){
  //   this.ActiveTab = data;
  //   this.activeTabIndex = index;
  //   // this.getFeedbackData(this.ActiveTab);
  // }
  noQues: boolean = false;
  setOpened(item,itemIndex, event) {
    // this.feedback_section = this.level1data[itemIndex].sectionlist;
    // this.questionObjectKeys =
    // this.objKeys = Object.keys(result.data[0]);
    let url = ENUM.domain + ENUM.url.getLevel1QuestionList;
    let param = {
      "pgmId": this.program_Data.id,
      'courseId': item.courseId,
      "trainId": this.trainerId,
      "tId": this.userDetail.tenantId,
      'flag': this.displayDataOf,
      "iId": item.instanceId
    }
    this.trainerService.getLevel1(url, param)
      .then((result: any) => {
        this.spinner.hide();
        console.log('Level 1 data Question List ==>', result);
        // this.feedback_section = this.level1data[itemIndex].sectionlist;
        if(result.type){
          if(result.data.length != 0) {
            this.feedback_section = result.data;
            this.currentlyOpenedItemIndex = itemIndex;
            this.noQues = false;
          }
          else{
            this.toaster.warning('Star rating or binary type questions are not available.', 'Warning');
            this.feedback_section = [];
            this.noQues = true;
           // this.setClosed(itemIndex);
          }
        }


      }).catch(result => {
        this.spinner.hide();
        console.log('Level 1 data Assessment scores  ==>', result);
      })

  }

  setClosed(itemIndex) {
    if(this.currentlyOpenedItemIndex === itemIndex) {
      this.currentlyOpenedItemIndex = -1;
    }
  }
}
