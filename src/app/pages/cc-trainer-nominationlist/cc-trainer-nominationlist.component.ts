import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CallCoachingService } from '../../service/call-coaching.service';
import { ENUM } from '../../service/enum';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'ngx-cc-trainer-nominationlist',
  templateUrl: './cc-trainer-nominationlist.component.html',
  styleUrls: ['./cc-trainer-nominationlist.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CcTrainerNominationlistComponent implements OnInit {
  employeeId: any;
  userDetails: any = [];
  userotherdetail: any;
  trainerId: any;
  list: boolean = false;
  constructor(public callservice: CallCoachingService, public datePipe: DatePipe, private router: Router,
    private routes: ActivatedRoute) {
    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
    this.userotherdetail = JSON.parse(localStorage.getItem('userOtherDetails'));
    let uDetail = this.userotherdetail[1]
    for (let i = 0; i < uDetail.length; i++) {
      if (uDetail[i].roleId == 7) {
        this.trainerId = uDetail[i].fieldmasterId;
      }
    }
  }

  ngOnInit() {
    this.nomineelist();
  }
  //noNominees: boolean = false;
  nomineelist() {
    const data = {
      trainId: this.trainerId,
      tId: this.userDetails.tenantId,
      lmt: null,
      pNo: null,
      reqFrom: 'WEB',
    };
    const url = ENUM.domain + ENUM.url.cc_trainer_nomineelist;

    this.callservice.get(url, data).then(res => {
      console.log(res);
      if (res['type'] === true) {
        if (res['data'][0].length == 0) {
          // this.noNominees = true;
          this.list = false;
        } else {
          //this.noNominees = false;
          for (let i = 0; i < res['data'][0].length; i++) {
            res['data'][0][i].date = this.formatDate(res['data'][0][i].nomDate);
          }
          this.nomineelist = res['data'][0];
          this.list = true;
          console.log("nomineelist", this.nomineelist);
        }
      }
    }, err => {
      console.log(err);
    });
  }
  formatDate(date) {
    let d = new Date(date);
    let formatted = this.datePipe.transform(d, "dd-MMM-yyyy");
    return formatted;
  }
  viewFeedback(data) {
    this.callservice.employeedata = data;
    this.router.navigate(['../call-coaching-list'], { relativeTo: this.routes })
  }
  back() {
    // window.history.back();
    this.router.navigate(['../trainer-dashboard'], { relativeTo: this.routes });
  }
}
