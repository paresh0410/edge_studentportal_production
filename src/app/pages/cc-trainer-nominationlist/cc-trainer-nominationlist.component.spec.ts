import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcTrainerNominationlistComponent } from './cc-trainer-nominationlist.component';

describe('CcTrainerNominationlistComponent', () => {
  let component: CcTrainerNominationlistComponent;
  let fixture: ComponentFixture<CcTrainerNominationlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcTrainerNominationlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcTrainerNominationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
