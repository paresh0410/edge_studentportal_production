import { WebinarServiceProvider } from '../../service/webinar.service';
import { LearnServiceProvider } from '../../service/learn-service';
import { ENUM } from '../../service/enum';
import { ToastrService, Toast } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

import { OpenVidu, Publisher, Session, StreamEvent, StreamManager, Subscriber } from 'openvidu-browser';
import { Toaster } from '../../component/toaster/toaster';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Input,
  Output,
  EventEmitter,
  HostListener,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import { WebinarEntity, WebinarConfig, WebinarType, WebinarMessage, WebinarSource } from './webinar-entity';
declare var window: any;
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
declare var window: any;
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UriBuilder } from 'uribuilder';
import { cleanPath } from 'cleanpath';
import 'rxjs/operator/toPromise';
import 'rxjs/operator/takeUntil';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ngx-webinar-multi-jitsi',
  templateUrl: './webinar-multi-jitsi.component.html',
  styleUrls: ['./webinar-multi-jitsi.component.scss']
})
export class WebinarMultiJitsiComponent implements OnInit, OnChanges, OnDestroy {

  @Input() course: any;
  @Input() userList: any;
  @Input() videoCall: any;
  @Input() voiceCall: any;
  @Input() areaId: any;
  @Input() tenantId: any;
  @Output() callEnd = new EventEmitter();
  @Output() callConnecting = new EventEmitter();
  @Output() trainerCutsCalls = new EventEmitter();
  @Input() config: WebinarConfig;
  instanceId: number;
  webinarType: WebinarType;
  userdetail: any = {};
  selectForCall: boolean = false;
  myInnerHeight: any = window.innerHeight - 160;
  chatboxHeight: any = this.myInnerHeight - 150;
  _videoworking: boolean = false;
  modalInfo: any = {
    title: 'Send Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Yes',
    event_second: 'No'
  };
  visibleEndModal = false;
  modalEndMessage: any = {
    title: 'Meeting is ended',
    subtitle: '',
    message: 'Your meeting in ended',
    event_first: 'Ok',
    event_second: ''
  };
  visibleEndedModal = false;
  modalEndedMessage: any = {
    title: 'End Meeting',
    subtitle: '',
    message: 'Do you want to end meeting?',
    event_first: 'Yes',
    event_second: 'Cancel'
  };
  visibleRejectModal = false;
  rejectJoinMeeting: string;
  modalRejectMessage: any = {
    title: 'Invitation Rejected',
    subtitle: '',
    message: 'User reject a call.',
    event_first: 'Ok',
    event_second: ''
  };
  visibleSendModal = false;
  senderJoinMeeting: string;
  modalSendInvitation: any = {
    title: 'Send Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Ok',
    event_second: 'Cancel'
  };
  visibleReceiveModal = false;
  modalReceiveInvitation: any = {
    title: 'Receive Invitation',
    subtitle: '',
    message: 'Are you sure to invite user?',
    event_first: 'Accept',
    event_second: 'Reject'
  };
  visibleConnectingModal = false;
  modalConnecting: any = {
    title: '',
    subtitle: '',
    message: 'Connecting...',
    event_first: '',
    event_second: 'Cancel'
  };
  visibleModal: boolean = false;
  userDetail: any = {
    id: 2,
    profilePic: './assets/images/eva.png',
    name: 'Steve',
    message: 'Test which is a new approach to have all solutions',
    datetime: Date(),
    messageType: 'text'
  };
  frienduserDetail: any = {
    id: 4,
    profilePic: './assets/images/jack.png',
    name: 'John',
    message: 'Test which is a new approach to have all solutions',
    datetime: Date(),
    messageType: 'text'
  };
  profilePic: string;
  name: string;
  messagePeview: string;
  lastMsgTime: string;
  pendingMsgs: string;
  activateChat: boolean;
  message: string = '';
  messages: any = [];
  replyMessage = '';

  prevIndex: number = 0;
  searchCtrl = new FormControl();
  filteredUsers: Observable<any[]>;

  users: Observable<any[]>;
  isLoading = false;
  myForm: FormGroup;
  chatUser: any = {};
  clientHeight: any = 0;
  spinner: any = false;
  chatInit: any = false;
  UserId: number;
  FriendUserId: number;
  webinarInvitedParticipants = [];
  public data: any = {
    username: '',
    chatlist: [],
    friendSocketId: null,
    selectedFriendId: null,
    selectedFriendName: null,
    messages: [],
    invite: false,
    inviteuserid: 0
  };
  webinarinfo: any = {
    SenderId: 0,
    ReceiverId: 0,
    SenderSocketId: '',
    RecevierSocketId: '',
    SenderUsername: '',
    ReceiverUsername: ''
  };
  userId: number;
  callId: number;
  private totoTemplate =
    '<p>Toastr to print toto</p>' +
    '<p><button class="btn btn-link" type="button" (click)="toto()">click here</button> to do something.</p>';
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChild('popupscrollMe') private popupScrollContainer: ElementRef;
  empId: any;
  coachId: any;
  bbbInfo: any;
  bbburl: any;
  openViduRequest: object = {
    clientData: null,
    userid: this.UserId,
    sessionName: null,
    tokenOptions: {
      serverData: null,
      role: null
    }
  }
  openViduWebinarInfo: any = {
    sessionName: null,
    tokens: [],
    myUserName: null
  }
  session: boolean = false;
  ovSession: Session;
  // tenantId: number = 1;
  webinar: WebinarEntity;
  joinflag: boolean = false;
  selectedUsersList: any = [];
  creator: boolean = false;
  joinflag_toaster: Toaster = {
    title: 'Webinar is running',
    subtitle: '',
    message: '',
    okTap: 'Join',
    cancelTap: 'Cancel',
    type: 'success'
  };
  publishAudio: boolean = true;
  publishVideo: boolean = true;
  ownDetail: any = {
    entityId: null,
    id: null,
    fullName: null,
    name: null,
    roleName: null,
    picture_url: null
  };
  warning_toaster: Toaster = {
    title: 'Unable to connect. Do you want to retry ?',
    subtitle: 'Do you want to retry ?',
    message: '',
    okTap: 'Yes',
    cancelTap: 'Cancel',
    type: 'success',
  };
  errorFlag = false;
  showLoader = false;
  setTimeOutInstance = null;
  constructor(
    private toastr: ToastrService,
    public chatService: WebinarServiceProvider,
    public cdf: ChangeDetectorRef,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    public datepipe: DatePipe
  ) { }

  scrollToBottom(): void {
    try {
      setTimeout(() => {
        // + this.myScrollContainer.nativeElement.scrollTop;
        if (this._videoworking && this.popupScrollContainer) {
          this.popupScrollContainer.nativeElement.scrollTop = this.popupScrollContainer.nativeElement.scrollHeight;
        } else if (this.myScrollContainer) {
          this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
      }, 10);
    } catch (err) {
      console.log(err);
    }
    // console.log(this.videoCallDiv.nativeElement.offsetWidth);
  }

  ngOnInit() {

    this.intializeWebinar();
    if (this.userList.length > 0) {
      this.addingNewParam();
      this.selectFriendToChat(this.userList[0]);
    }
    if (this.userList.length == 1) {
      this.activeUser(0, 0);
    } else {
      this.activeUser(0, 1);
    }
  }

  intializeWebinar(){
    this.instanceId = this.course.activityId || this.course.instanceId;
    this.data.chatlist = this.userList;
    this.callId = 10;
    this.bbburl = null;
    if (localStorage.getItem('userDetails')) {
      const userDetails = JSON.parse(localStorage.getItem('userDetails'));
      try {
        this.userDetail = userDetails;
        this.UserId = this.userDetail.id;
        this.ownDetail = {
          entityId: userDetails.id,
          id: userDetails.id,
          fullName: userDetails.firstname + ' ' + userDetails.lastname,
          name: userDetails.firstname,
          roleName: userDetails.roll,
          picture_url: userDetails.picture_url,
        }
      } catch (e) {
        console.log(e);
      }
    }

    this.connectSocket(this.UserId);
    this.incommingMessage();
    this.usercomming();
    this.ReceiveWebinar();
    this.Connecting();
    this.WebinarCreatedNotify();
    this.ApprovalReceive();
    this.EndedMeetingReceive();
    this.WebinarSessionRunningResponse();
    this.CheckWebinarSession();
    this.WebinarGroupChatReceive();
    this.WebinarGetGroupChatByWebinarIdResponse();
    this.EndedAllParticipinantMeetingReceive();
  }
  connectSocket(UserId) {
    this.chatService.connectSocketServer(UserId);
  }
  // Store Current Data
  currentUserIndex: number;
  activeUser(currentindex, previousindex) {
    this.currentUserIndex = currentindex;
    if (previousindex || previousindex == 0) {
      this.data.chatlist[previousindex].active = true;
    }
    if (currentindex || currentindex == 0) {
      this.data.chatlist[currentindex].active = false;
      this.data.chatlist[currentindex].badge = 0;
    }
    this.prevIndex = currentindex;
  }

  incommingMessage() {
    /*
     * This eventt will display the new incmoing message
     */
    this.chatService.socketOn('add-message-response', response => {
      // response.friendProfilePic = './assets/images/jack.png';
      let isToastRunning = false;
      if (this.areaId !== response.areaId || this.instanceId !== response.instanceId) {
        this.data.chatlist.forEach(element => {
            isToastRunning = true;
            this.toastr.success(response.message, element.name).onHidden.subscribe(()=>
            this.toastr.clear());
        });
      } else if (String(response.fromUserId) === String(this.FriendUserId)) {
        this.data.messages.push(response);
        this.scrollToBottom();
        try {
          this.cdf.detectChanges();
        } catch (e) {
          console.log(e);
        }
      } else {
        console.log('config ', this.config);
        console.log('areaId ', this.areaId);
        console.log('instanceId ', this.instanceId);
        this.data.chatlist.forEach(element => {
          if (String(element.id) === String(response.fromUserId)) {
            isToastRunning = true;
            this.toastr.success(response.message, element.name).onHidden.subscribe(()=>
            this.toastr.clear());
            if (isNaN(element.badge)) {
              element.badge = 0;
            }
            element.badge = element.badge + 1;
          }
        });
        if (!isToastRunning) {
          this.toastr.success('You received a message.').onHidden.subscribe(()=>
          this.toastr.clear());
        }
      }
    });
  }

  selectFriendToChat(friend) {
    /*
     * Highlighting the selected user from the chat list
     */
    // this.SelectedUsers(friend,'');
    this.spinner = true;
    // this.messages = [];
    this.frienduserDetail = friend;
    this.webinarinfo = {
      SenderId: this.UserId,
      ReceiverId: friend.id,
      SenderUsername: this.userDetail.username,
      ReceiverUsername: friend.name
    };
    this.FriendUserId = friend.id;
    if (this.FriendUserId && this.UserId) {
      this.checkUserFriendMap(this.UserId, this.FriendUserId);
      this.data.selectedFriendName = this.frienduserDetail['name'];
      this.data.selectedFriendId = this.frienduserDetail.id;
    }
    /**
     * This HTTP call will fetch chat between two users
     */
    if (this.chatUser.online === undefined) {
      this.data.invite = true;
      this.data.inviteuserid = this.frienduserDetail.id;
      this.data.messages = [];
    }
    this.chatService.getMessages(this.UserId, this.FriendUserId, this.areaId, this.instanceId).then((response: any) => {
      if (response.messages.length > 0) {
        this.chatInit = true;
        this.data.messages = response.messages[0];
        console.log(response.messages[0]);
      }
      this.scrollToBottom();
      this.spinner = false;
      // this.cdf.detectChanges();
    })
      .catch(error => {
        console.log(error);
        this.spinner = false;
        alert('Unexpected Error, Contact your Site Admin.');
      });
  }
  usercomming() {
    this.chatService.socketOn('user-comming-chat-list-response', response => {
      if (response.userinfo.length > 0) {
        if (response.userinfo[0].userid === this.FriendUserId) {
          this.data.friendSocketId = response.userinfo[0].socketid;
          this.webinarinfo.RecevierSocketId = response.userinfo[0].socketid;
        }
      }
    });
  }

  sendChatMessage(event, value) {

    if (!String(value).trim()) return null;

    let fromUserId = null;
    let toSocketId = null;
    /* Fetching the selected User from the chat list starts */
    let selectedFriendId = this.webinarinfo.ReceiverId;
    if (selectedFriendId === null) {
      return null;
    }

    /* Fetching the selected User from the chat list ends */
    /* Emmiting socket event to server with Message, starts */
    fromUserId = this.webinarinfo.SenderId;
    toSocketId = this.data['friendSocketId'];
    let messagePacket = {
      message: value,
      fromUserId: fromUserId,
      toUserId: selectedFriendId,
      toSocketId: toSocketId,
      userProfilePic: this.userDetail.picture_url, // './assets/images/eva.png',
      friendId: selectedFriendId,
      areaId: this.areaId,
      tenantId: this.tenantId,
      instanceId: this.course.activityId || this.course.instanceId, // this.webinar.instanceId
      friend: this.ownDetail
    };
    this.data.messages.push(messagePacket);
    this.chatService.socketEmit(`add-message`, messagePacket);
    this.message = '';
    this.scrollToBottom();
  }

  checkUserFriendMap(userId, friendId) {
    let param = {
      userId: userId,
      friendId: friendId
    };
    this.chatService
      .insertDirectUserFriendMap(param)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  /**
   * Start
   * @Author Bhavesh Tandel
   * Video Call Method
   */

  /**
   * Open Invitation Modal POPUP
   * @method OpenInviationPopup
   * @param coachId
   * @param trainerId
   */
  OpenInviationPopup() {
    console.log('Open Modal Up');
    if(this.config.allowCalls){
      this.visibleSendModal = true;
      this.visibleReceiveModal = false;
      this.visibleConnectingModal = false;
    }else{
      // let message = this.config.dontAllowCallsMessage;
      let  message = 'You cannot make a call ';
      this.toastr.info(message, 'Info').onHidden.subscribe(()=>
      this.toastr.clear());
    }

  }

  /**
   * Send invitaion to receiver
   * @method CheckSession
   * @event Accepted
   * @event Rejected
   */
  CheckWebinarSession() {
    let webinar: WebinarEntity = {
      meetingId: this.course.activityName || this.course.name,
      meetingName: this.course.activityName || this.course.name,
      areaId: this.areaId,
      instanceId: this.course.activityId || this.course.instanceId,
      userId: this.UserId,
      tenantId: this.tenantId,
      webinarId: null,
      webinarJoinId: null,
      status: false,
      username: this.webinarinfo.SenderUsername,
      token: null,
      sessionId: null,
      userids: null,
      role: 'PUBLISHER',
      creator: false,
      recording: this.config.recording,
      source: WebinarSource.PORTAL,
      socketId: this.chatService.customKey,
    }
    this.chatService.socketEmit(`jitsi-meet-webinar-running-status`, webinar);
  }
  WebinarSessionRunningResponse() {
    this.chatService.socketOn('jitsi-meet-webinar-running-status-response', response => {
      const webinar = response.data;
      console.log('webinar response :- ', webinar);
      this.webinar = webinar;
      const toast = {
        enableHtml: true
      }
      if (webinar.status === true) {
        this.joinflag_toaster.title = this.webinar.meetingName + ' is running.';
        this.joinflag_toaster.title = WebinarMessage.CONNECTTOEXISITING;
        this.joinflag = true;
        // this.toastr.success('<button type="button" class="btn clear" (click)="joinwebinar()">Yes</button><button type="button" class="btn danger clear" (click)="rejectwebinar()">No</button>',
        // webinar.meetingName, toast);
      }
    })
  }
  /**
   * Send invitaion to receiver
   * @method SendInviation
   * @param coachId
   * @param trainerId
   * @event Accepted
   * @event Rejected
   */
  SendInviation(event) {
    if (event) {
      this.openViduRequest = {
        clientData: this.webinarinfo.SenderUsername,
        userid: this.UserId,
        sessionName: this.course.activityName || this.course.name,
        tokenOptions: {
          serverData: this.webinarinfo.SenderUsername,
          role: 'PUBLISHER'
        },
        isCreator: true
      }
      const userids = this.ConvertToString();
      let webinar: WebinarEntity = {
        meetingId: null,
        meetingName: this.course.activityName || this.course.name,
        areaId: this.areaId,
        instanceId: this.course.activityId || this.course.instanceId,
        userId: this.UserId,
        tenantId: this.tenantId,
        webinarId: null,
        webinarJoinId: null,
        status: false,
        username: this.webinarinfo.SenderUsername,
        token: null,
        sessionId: null,
        userids: userids,
        role: 'PUBLISHER',
        creator: true,
        recording: this.config.recording,
        source: WebinarSource.PORTAL,
        socketId: this.chatService.customKey,
      }
      if (userids) {
        // this.chatService.socketEmit(`webinar-create-new-session`, webinar);
        this.chatService.socketEmit(`jitsi-meet-webinar-create-new-meeting`, webinar);
        this.toggelSpinner(true);
        this.visibleSendModal = false;
        this.visibleReceiveModal = false;
        this.presentSetTimeOut();
      } else {
        this.toastr.warning('Please add learner in list for webinar', 'Select users').onHidden.subscribe(()=>
        this.toastr.clear());
      }
    }
  }
  /**
   * @method Connecting
   * @param flag
   */
  Connecting() {
    console.log('Connecting');
    // Old
    // this.chatService.socketOn('webinar-created-session-response-to-creator', response => {
    // New
    this.chatService.socketOn('jitsi-meet-webinar-created-session-response-to-creator', response => {
      console.log(response);
      this.toggelSpinner(false);
      this.visibleSendModal = false;
      this.visibleReceiveModal = false;
      // this.visibleConnectingModal = true;
      this.webinar = response.data;
      this.openViduWebinarInfo = {
        sessionName: this.webinar.sessionId || this.webinar.meetingName,
        tokens: [],
        myUserName: this.webinar.username,
        domain: this.webinar.domain,

      }
      this.openViduWebinarInfo.tokens.push(this.webinar.token);

      this.openWebinarWindow(this.openViduWebinarInfo);
      this.chatService.socketEmit('jitsi-meet-webinar-send-notification-to-receiver', this.webinar);
    });
  }
  /**
   * @method Connecting
   * @param flag
   */
  WebinarCreatedNotify() {
    console.log('WebinarCreatedNotify');
    this.chatService.socketOn('jitsi-meet-webinar-created-session-response', response => {
      this.webinar = response.data;
      this.joinflag_toaster.title = this.webinar.meetingName + ' is running.';
      this.joinflag_toaster.title = WebinarMessage.NEWCALL;
      this.joinflag = true;
      const toast = {
        enableHtml: true
      }
      this.toastr.success('<button type="button" class="btn clear" (click)="joinwebinar()">Yes</button><button type="button" class="btn danger clear" (click)="rejectwebinar()">No</button>',
        this.webinar.meetingName, toast).onHidden.subscribe(()=>
        this.toastr.clear());
    });
  }
  /**
   * Receive Invitation from Sender
   * @method ReceiveInvitation
   *
   */
  ReceiveWebinar() {
    this.chatService.socketOn('jitsi-meet-webinar-joined-session-respone', response => {
      console.log(response);
      this.toggelSpinner(false);
      this.visibleSendModal = false;
      this.visibleReceiveModal = false;
      // this.visibleConnectingModal = true;
      this.webinar = response.data;

      this.openViduWebinarInfo = {
        sessionName:  this.webinar.sessionId || this.webinar.meetingName,
        tokens: [],
        myUserName: this.webinar.username,
        domain: this.webinar.domain,

      }
      this.openViduWebinarInfo.tokens.push(this.webinar.token);
      this.openWebinarWindow(this.openViduWebinarInfo);
      this.openReceiveModalPopUp();
      this.CloseModal(true);
    });
  }
  openReceiveModalPopUp() {
    console.log('Receive Invitation');
    this.visibleSendModal = false;
    this.visibleReceiveModal = true;
    this.visibleConnectingModal = false;
  }
  AcceptInvitation(event) {
    if (event === 'true' || event == true) {
      this.openViduRequest = {
        clientData: this.webinarinfo.SenderUsername,
        userid: this.UserId,
        sessionName: this.course.activityName || this.course.name,
        tokenOptions: {
          serverData: this.webinarinfo.SenderUsername,
          role: 'PUBLISHER'
        }
      }
      let webinar: WebinarEntity = {
        meetingId: this.webinar.meetingId,
        meetingName: this.webinar.meetingId,// this.course.activityName || this.course.name,
        areaId: this.webinar.areaId,//this.areaId,
        instanceId: this.webinar.instanceId, //this.course.activityId || this.course.instanceId,
        userId: this.UserId,
        tenantId: this.tenantId,
        webinarId: this.webinar.webinarId, // this.webinar.webinarId,
        webinarJoinId: null,
        status: true,
        username: this.webinarinfo.SenderUsername,
        token: null,
        sessionId: this.webinar.sessionId,
        userids: this.webinar.userids,
        role: 'PUBLISHER',
        creator: this.config.creator,
        recording: this.config.recording,
        source: WebinarSource.PORTAL,
        socketId: this.chatService.customKey,
      }
      this.chatService.socketEmit(`jitsi-meet-webinar-join-session`, webinar);
      this.toggelSpinner(true);
      this.presentSetTimeOut();
      this.joinflag = false;
    } else if (event === false || event == 'false') {
      this.openViduRequest = {
        clientData: this.webinarinfo.SenderUsername,
        userid: this.UserId,
        sessionName: this.course.activityName || this.course.name,
        tokenOptions: {
          serverData: this.webinarinfo.SenderUsername,
          role: 'PUBLISHER'
        }
      }
      let webinar: WebinarEntity = {
        meetingId: null,
        meetingName: this.course.activityName || this.course.name,
        areaId: this.areaId,
        instanceId: this.course.activityId || this.course.instanceId,
        userId: this.UserId,
        tenantId: this.tenantId,
        webinarId: this.webinar.webinarId,
        webinarJoinId: null,
        status: false,
        username: this.webinarinfo.SenderUsername,
        token: null,
        sessionId: this.webinar.sessionId,
        userids: this.webinar.userids,
        role: 'PUBLISHER',
        creator: false,
        recording: this.config.recording,
        source: WebinarSource.PORTAL,
        socketId: this.chatService.customKey,
      }
      this.chatService.socketEmit(`jitsi-meet-webinar-join-session`, webinar);
      this.toggelSpinner(true);
      this.presentSetTimeOut();
      this.joinflag = false;
      //this.chatService.socketEmit(`receiver-accept-request-webinar-call`, this.bbbInfo);
    }
    this.CloseModal(true);
  }

  /**
   * @method StartMeeting
   */
  StartMeeting() { }

  /**
   * @method JoinMeeting
   */
  JoinMeeting(event) {
    var obj = {
      meetingID: this.bbbInfo.meetingID,
      fullName: this.webinarinfo.ReceiverUsername,
      password: this.bbbInfo.moderatorPW,
      senderId: this.UserId,
      receiverId: this.FriendUserId, // this.coachId,
      senderName: this.webinarinfo.SenderUsername,
      receiverName: this.webinarinfo.ReceiverUsername
    };
    this.chatService.socketEmit('accept-request-webinar-call', obj);
  }

  CloseModal($event) {
    console.log('Close Event');
    if (this.visibleConnectingModal) {
      this.visibleConnectingModal = false;
    }
    if (this.visibleSendModal) {
      this.visibleSendModal = false;
    }
    if (this.visibleReceiveModal) {
      this.visibleReceiveModal = false;
    }
    if (this.visibleRejectModal) {
      this.visibleRejectModal = false;
    }
    if (this.visibleEndedModal) {
      this.visibleEndedModal = false;
    }
    this.visibleEndModal = false;
  }
  openRejectModalPopup() {
    this.CloseModal(false);
    this.visibleRejectModal = true;
  }
  ApprovalReceive() {
    this.chatService.socketOn('sender-receive-approval', data => {
      console.log('Socket - sender-receive-approval', data);
      this.CloseModal(null);
      // var senderUrl = this.senderJoinMeeting;
      if (data.status == true) {
        // this.openViduWebinarInfo.tokens.push(data.bbbInfo.token);
        this.openWebinarWindow(this.openViduWebinarInfo);
      } else {
        console.log('User Reject your call!');
        this.openRejectModalPopup();
      }
    });
    this.chatService.socketOn('join-receiver', data => {
      if (data.bbbInfo) {
        // this.bbbInfo.start = data.bbbInfo;
        const webinar = data.bbbInfo;
        this.openViduWebinarInfo = {
          sessionName: webinar.sessionId || webinar.sessionName,
          tokens: [],
          myUserName: webinar.userName,
          domain: this.webinar.domain,

        };
        this.openViduWebinarInfo.tokens.push(webinar.token);
        this.openWebinarWindow(this.openViduWebinarInfo);
      }
    });
  }
  EndedMeeting(event) {
    if (this.openViduWebinarInfo.tokens.length > 0) {
      // if (this.creator) {
      //   this.webinar.creator = this.creator;
      // }
      this.webinar.creator = this.config.creator || false;
    // if(this.config.creator){
      this.chatService.socketEmit('jitsi-meet-webinar-leave-session', this.webinar);
      const eventToBePassed = {
        calledParticipantsList : this.webinarInvitedParticipants,
        webinarInformation: this.webinar,
        type: 'callEnded',
      };
      this.callEnd.emit(eventToBePassed);

    // }
      this.leaveSession();
      this.CloseModal(true);
      this.bbbInfo = {};
      this.session = false;
      this._videoworking = false;
      this.visibleEndModal = true;
      this.openViduWebinarInfo = {
        sessionName: null,
        tokens: [],
        myUserName: null
      }

      //this.chatService.disconnect();
      // this.ChangeData.emit(this.videoCall);

  }
  }
  EndedMeetingReceive() {
    this.chatService.socketOn('jitsi-meet-webinar-leave-session-notify', result => {
      // this.ovSessionComponent.emitLeaveSessionEvent(null);
      if (result.data && (result.data.meetingName === this.openViduWebinarInfo.sessionName)) {
        this.toastr.success(null, result.data.msg).onHidden.subscribe(()=>
        this.toastr.clear());
      }
    });
  }
  EndedAllParticipinantMeetingReceive() {
    this.chatService.socketOn('jitsi-meet-webinar-end-session-notify', result => {
      // this.ovSessionComponent.emitLeaveSessionEvent(null);
      if (result.status) {
        this.joinflag = false;
      }
      if (result.leaveflag) {
        this.trainerCutsCalls.emit();
        this.EndedMeeting(undefined);
      }
    });
  }

  /**
   * End
   * @Author Bhavesh Tandel
   * Video Call Method
   */
  option:any ={};
  openWebinarWindow(value) {
    // this.bbburl = this.transform(url);
    this._videoworking = true;
    this.session = true;
    console.log(value);
    this.option =value;
    this.messages = [];
    this.WebinarGetGroupChatByWebinarId();
    //  this.joinSession();
  }
  endVideo() {
    this.visibleEndedModal = true;
  }
  encodeURI(url) {
    if (UriBuilder.parse(url).isRelative()) {
      url = cleanPath(document.baseURI + '/' + url);
    }
    return encodeURIComponent(url);
  }
  transform(url): any {
    // url = 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg';
    // url = 'https://angular.io/api/platform-browser/DomSanitizer#bypassSecurityTrustUrl';
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  ngOnDestroy() {
    this.handlerLeaveSessionEvent(null);
    if(this.setTimeOutInstance){
      clearTimeout(this.setTimeOutInstance);
    }
  }
  // Responsive Width
  // @ViewChild('videoCallDiv')
  // videoCallDiv: ElementRef;
  /**
   * @description Open Vidu Events
   * @method handlerJoinSessionEvent
   * @method handlerLeaveSessionEvent
   * @method handlerErrorEvent
   * @method myMethod
   */

  handlerJoinSessionEvent(event): void {
    this.myMethod();
  }

  handlerLeaveSessionEvent(event): void {
    this.EndedMeeting(event);
    this.openViduWebinarInfo.tokens = [];
    this.session = false;
    this.leaveSession();
    this.chatService.disconnect();
  }

  handlerErrorEvent(event): void {
    // Do something
    console.log('handlerErrorEvent', event);
  }

  myMethod() {
  }


  // OpenVidu objects
  OV: OpenVidu;
  ovsession: Session;
  publisher: Publisher; // Local
  subscribers: StreamManager[] = []; // Remotes
  // Join form
  mySessionId: string;
  myUserName: string;

  // Main video of the page, will be 'publisher' or one of the 'subscribers',
  // updated by click event in UserVideoComponent children
  mainStreamManager: StreamManager;
  joinSession() {
    let token = null;
    if (this.openViduWebinarInfo.tokens.length > 0) {
      token = this.openViduWebinarInfo.tokens[0];
    }
    // --- 1) Get an OpenVidu object ---

    this.OV = new OpenVidu();

    // --- 2) Init a session ---

    this.ovsession = this.OV.initSession();

    // --- 3) Specify the actions when events take place in the session ---

    // On every new Stream received...
    this.ovsession.on('streamCreated', (event: StreamEvent) => {

      // Subscribe to the Stream to receive it. Second parameter is undefined
      // so OpenVidu doesn't create an HTML video by its own
      let subscriber: Subscriber = this.ovsession.subscribe(event.stream, undefined);
      this.subscribers.push(subscriber);
    });

    // On every Stream destroyed...
    this.ovsession.on('streamDestroyed', (event: StreamEvent) => {

      // Remove the stream from 'subscribers' array
      this.deleteSubscriber(event.stream.streamManager);
    });

    // --- 4) Connect to the session with a valid user token ---

    // 'getToken' method is simulating what your server-side should do.
    // 'token' parameter should be retrieved and returned by your own backend
    // this.getToken().then(token => {

    // First param is the token got from OpenVidu Server. Second param can be retrieved by every user on event
    // 'streamCreated' (property Stream.connection.data), and will be appended to DOM as the user's nickname
    // this.ovsession.connect(token, { clientData: this.openViduWebinarInfo.myUserName + ' - portal' })
    this.ovsession.connect(token, { clientData: this.openViduWebinarInfo.myUserName })
      .then(() => {

        // --- 5) Get your own camera stream ---

        // Init a publisher passing undefined as targetElement (we don't want OpenVidu to insert a video
        // element: we will manage it on our own) and with the desired properties
        let publisher: Publisher = this.OV.initPublisher(undefined, {
          audioSource: undefined, // The source of audio. If undefined default microphone
          videoSource: undefined, // The source of video. If undefined default webcam
          publishAudio: true,     // Whether you want to start publishing with your audio unmuted or not
          publishVideo: true,     // Whether you want to start publishing with your video enabled or not
          resolution: '640x480',  // The resolution of your video
          frameRate: 30,          // The frame rate of your video
          insertMode: 'APPEND',   // How the video is inserted in the target element 'video-container'
          mirror: false           // Whether to mirror your local video or not
        });

        // --- 6) Publish your stream ---

        this.ovsession.publish(publisher);

        // Set the main video in the page to display our webcam and store our Publisher
        this.mainStreamManager = publisher;
        this.publisher = publisher;
        setTimeout(() => {
          this.chatService.socketEmit('start-recording-webinar-call', this.webinar);

        }, 200);
        const eventToBePassed = {
          calledParticipantsList : this.webinarInvitedParticipants,
          webinarInformation: this.webinar,
          type: 'callInvitation',
        };
        this.callConnecting.emit(eventToBePassed);
      })
      .catch(error => {
        console.log('There was an error connecting to the session:', error.code, error.message);
        this._videoworking = false;
        this.CloseModal(true);
        this.toastr.warning('Webinar Activity is closed').onHidden.subscribe(()=>
        this.toastr.clear());
      });
    // });
  }

  leaveSession() {

    // --- 7) Leave the session by calling 'disconnect' method over the Session object ---

    if (this.ovsession) { this.ovsession.disconnect(); };

    // Empty all properties...
    this.subscribers = [];
    delete this.publisher;
    delete this.session;
    delete this.OV;
    this.generateParticipantInfo();
  }
  private generateParticipantInfo() {
  }

  private deleteSubscriber(streamManager: StreamManager): void {
    let index = this.subscribers.indexOf(streamManager, 0);
    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  }

  updateMainStreamManager(streamManager: StreamManager) {
    this.mainStreamManager = streamManager;
  }
  @HostListener('window:beforeunload')
  beforeunloadHandler() {
    // On window closed leave session
    this.leaveSession();
  }
  toto() {
    alert('yes');
  }
  SelectedUsers(user, selectedUser) {
    if (this.selectedUsersList.indexOf(user.id) === -1) {
      this.selectedUsersList.push(user.id);
    }

    console.log('User list :- ', this.selectedUsersList)
  }
// !String(value).trim()
  webinarGroupChatSend(message) {
    if (!String(message).trim()) return null;

    const userids = this.webinar.userids;// this.ConvertToString();
    const messagePacket = {
      message: message,
      fromUserId: this.UserId,
      userProfilePic: this.userDetail.picture_url, // './assets/images/no-user-image.jpg',
      username: this.webinar.username,
    };
    let webinar = {
      meetingId: this.webinar.meetingId,
      meetingName: null,
      areaId: this.areaId,
      instanceId: this.course.activityId || this.course.instanceId,
      userId: this.UserId,
      tenantId: this.tenantId,
      webinarId: this.webinar.webinarId,
      webinarJoinId: this.webinar.webinarJoinId,
      status: false,
      username: this.webinarinfo.SenderUsername,
      token: null,
      sessionId: null,
      userids: userids,
      role: 'PUBLISHER',
      creator: true,
      message: messagePacket
    }
    if (message) {
      this.messages.push(messagePacket);
      this.chatService.socketEmit(`webinar-group-message`, webinar);
      this.message = null;
      this.scrollToBottom();
    }
  }
  WebinarGroupChatReceive() {
    this.chatService.socketOn('webinar-group-chat-response', response => {
      if (response.status == true) {
        const message = response.data.message;
        this.messages.push(message);
        this.scrollToBottom();
      }
    })
  }
  WebinarGetGroupChatByWebinarId() {
    const webinar = this.webinar;
    this.chatService.socketEmit(`webinar-get-group-message-by-id`, webinar);
  }
  WebinarGetGroupChatByWebinarIdResponse() {
    this.chatService.socketOn('webinar-get-group-message-by-id-response', response => {
      if (response.status == true) {
        this.messages = response.messages;
        this.scrollToBottom();
      }
    })
  }
  toggleaudio() {
    this.publishAudio = !this.publishAudio;
    this.publisher.publishAudio(this.publishAudio);
  }
  togglevideo() {
    this.publishVideo = !this.publishVideo;
    this.publisher.publishVideo(this.publishVideo);
  }
  userCount = 0;

  addCall(chatuser, i) {
    // this.data.chatlist[i].activecall = true;
    this.data.chatlist[i].activecall = !this.data.chatlist[i].activecall;
  }
  addingNewParam() {
    if (this.data.chatlist.length > 0) {
      this.data.chatlist.forEach((item) => {
        item.activecall = false;
      })
    }
  }
  ConvertToString() {
    this.webinarInvitedParticipants = [];
    let userids: string = '';
    if (this.data.chatlist.length > 0) {
      this.data.chatlist.forEach((user) => {
        if (user.activecall === true) {
          if (userids) {
            userids = userids + ',';
          }
          this.webinarInvitedParticipants.push(user);
          userids = userids + String(user.id);
        }
      })
    }
    return userids;
  }
  imgErrorHandler(event) {
    console.debug(event);
    event.target.src = "./assets/images/user.png";
  }
  onClearBadge(event) {
    event.badge = 0;
  }

  ngOnChanges(){
    this.data.chatlist = this.userList;
  }


  resetSocketConnects(){
    this.toggelSpinner(false);
    this.toggelErrorPopup(true);
    this.chatService.disconnect();
    this.intializeWebinar();
  }

  presentSetTimeOut(){
    if(this.setTimeOutInstance){
      clearTimeout(this.setTimeOutInstance);
    }
    this.setTimeOutInstance = setTimeout(() => {
      if (this.showLoader) {
          this.resetSocketConnects();
          // const title = 'Unable to connect';
          // const message = 'Do you want to retry ?';
          // this.CreateAlertMessage(title, message, true);
      }
  }, 120000);
  //
  }
  toggelSpinner(flag){
    this.showLoader = flag;
  }

  toggelErrorPopup(flag){
    this.errorFlag = flag;
  }
}
