export class WebinarEntity {
    sessionId: string;
    meetingId: string;
    meetingName: string;
    areaId: number;
    instanceId: number;
    userId: number;
    tenantId: number;
    webinarId: number;
    webinarJoinId: number;
    status: boolean;
    username: string;
    userids: string;
    token: string;
    role: string;
    creator: boolean;
    recording: boolean;
    source?: string;
    domain?: string;
    socketId?: string;
}

export class WebinarConfig {
    voice: boolean;
    video: boolean;
    creator: boolean;
    areaId: number;
    instanceId: number;
    tenantId: number;
    type: WebinarType;
    recording: boolean;
    allowCalls: boolean;
    startDate?: string;
    EndDate?: string;
    dontAllowCallsMessage?: string;
    constructor() {
        this.voice = true;
        this.video = true;
        this.creator = false;
        this.areaId = null;
        this.instanceId = null;
        this.tenantId= null;
        this.type= null;
        this.recording= false;
        this.allowCalls = false;
        this.startDate = '';
        this.EndDate = '';
        // this.domain = '';
    }
}

export enum WebinarType {
    MULTIPLE= 'MULTIPLE',
    SINGLE= 'SINGLE'
}

export enum WebinarMessage {
    NEWCALL= 'Do you want to join the call?',
    CONNECTTOEXISITING= ' Do you want to join the call ?'
}
export enum WebinarSource {
    PORTAL = 'portal',
    APP = 'app'
}
