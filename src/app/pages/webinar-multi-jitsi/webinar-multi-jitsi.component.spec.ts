import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarMultiJitsiComponent } from './webinar-multi-jitsi.component';

describe('WebinarMultiJitsiComponent', () => {
  let component: WebinarMultiJitsiComponent;
  let fixture: ComponentFixture<WebinarMultiJitsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarMultiJitsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarMultiJitsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
