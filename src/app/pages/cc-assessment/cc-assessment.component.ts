import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ENUM } from '../../service/enum';
import { CallCoachingService } from '../../service/call-coaching.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'ngx-cc-assessment',
  templateUrl: './cc-assessment.component.html',
  styleUrls: ['./cc-assessment.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CcAssessmentComponent implements OnInit {
  @Input() assessmentdata;
  @Input() calldetail;
  assessmentTemplate: any = [];
  employeeId: any;
  userDetails: any = [];
  calls: any = [];


  // isEnable - To avoid submitting feedback before call.
  // isEnableC - To check if the call is in future or not
  // isEnableW - To check webinar timing

  constructor(public callservice: CallCoachingService, public Toastr: ToastrService,
    public spinner: NgxSpinnerService) {
    this.employeeId = localStorage.getItem('employeeId');
    this.userDetails = JSON.parse(localStorage.getItem('userdetail'));
  }

  ngOnInit() {
    for (let i = 0; i < this.assessmentdata.length; i++) {
      for (let j = 0; j < this.assessmentdata[i].areaSubAssess.length; j++) {
        if (this.assessmentdata[i].areaSubAssess[j].score === null) {
          this.assessmentdata[i].areaSubAssess[j].score = 0;
        }

        if (this.assessmentdata[i].areaSubAssess[j].remarks === 'null') {
          this.assessmentdata[i].areaSubAssess[j].remarks = null;
        }
      }

    }
    this.assessmentTemplate = this.assessmentdata;
    console.log('this.assessmentTemplateAssPage', this.assessmentTemplate);
    this.calls = this.calldetail;
  }
  onRateChange(count, index, mainIndex) {
    console.log('index',index);
    var count = count.rating;
    for (let i = 0; i < this.assessmentTemplate.length; i++) {
      if (i == mainIndex) {
        for (let j = 0; j < this.assessmentTemplate[i].areaSubAssess.length; j++) {
          if (j == index) {
            // console.log('i',i);
            // console.log('j',j);
            this.assessmentTemplate[i].areaSubAssess[j].score = count ;
            //break;
          }
        }
      }
    }
  }

  submit(data) {
    // if(this.calldetail.isEnable == 'Y'){
    //   this.spinner.show();
    //   console.log('Assessment data', data);
    //   var newDataArr = [];
    //   for (let i = 0; i < data.length; i++) {
    //     for (let j = 0; j < data[i].areaSubAssess.length; j++) {
    //       var newDataObj = {
    //         assessName: data[i].areaSubAssess[j].assessName,
    //         atId: data[i].areaSubAssess[j].atId,
    //         compId: data[i].areaSubAssess[j].compId,
    //         compName: data[i].areaSubAssess[j].compName,
    //         remarks: data[i].areaSubAssess[j].remarks,
    //         score: data[i].areaSubAssess[j].score,
    //         spectrId: data[i].areaSubAssess[j].spectrId,
    //         spectrName: data[i].areaSubAssess[j].spectrName,
    //         areaId: data[i].areaId,
    //         areaName: data[i].areaName,
    //       };
    //       newDataArr.push(newDataObj);
    //     }
    //   }
    //   console.log('newDataArr', newDataArr);
    //   const allstr = this.getDataReadyForFeedbackCC(newDataArr);
    //   console.log('assess allstr:', allstr);
    //   const param = {
    //     callId: this.calls.callId,
    //     userId: this.userDetails.id,
    //     tId: this.userDetails.tenantId,
    //     allstr: allstr,
    //   };
    //   console.log(param);
    //   const url = ENUM.domain + ENUM.url.cc_add_assessment;
    //   this.callservice.get(url, param).then(res => {
    //     console.log(res);
    //     this.spinner.hide();
    //     if (res['type'] == true) {
    //       this.Toastr.success('Assement submitted successfully.');
    //       // this.Toastr.success(res[data].msg);
    //       // window.history.back();
    //     }
    //   }, err => {
    //     this.spinner.hide();
    //     console.log(err);
    //   });
    // }
    // else{
    //   this.Toastr.info(this.calldetail.msg,'Info');
    //   // this.Toastr.info('Assement will be submitted after call.','Info');
    // }
    this.spinner.show();
    console.log('Assessment data', data);
    var newDataArr = [];
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data[i].areaSubAssess.length; j++) {
        var newDataObj = {
          assessName: data[i].areaSubAssess[j].assessName,
          atId: data[i].areaSubAssess[j].atId,
          compId: data[i].areaSubAssess[j].compId,
          compName: data[i].areaSubAssess[j].compName,
          remarks: data[i].areaSubAssess[j].remarks,
          score: data[i].areaSubAssess[j].score,
          spectrId: data[i].areaSubAssess[j].spectrId,
          spectrName: data[i].areaSubAssess[j].spectrName,
          areaId: data[i].areaId,
          areaName: data[i].areaName,
        };
        newDataArr.push(newDataObj);
      }
    }
    console.log('newDataArr', newDataArr);
    const allstr = this.getDataReadyForFeedbackCC(newDataArr);
    console.log('assess allstr:', allstr);
    const param = {
      callId: this.calls.callId,
      userId: this.userDetails.id,
      tId: this.userDetails.tenantId,
      allstr: allstr,
    };
    console.log(param);
    const url = ENUM.domain + ENUM.url.cc_add_assessment;
    this.callservice.get(url, param).then(res => {
      console.log(res);
      this.spinner.hide();
      if (res['type'] == true) {
        this.Toastr.success('Assement submitted successfully.').onHidden.subscribe(()=>
        this.Toastr.clear());
        // this.Toastr.success(res[data].msg);
        // window.history.back();
      }
    }, err => {
      this.spinner.hide();
      console.log(err);
    });
  }

  getDataReadyForFeedbackCC(newDataArr) {
    console.log('newDataArrReady', newDataArr);
    var spectrId;
    var spectrName;
    var compId;
    var compName;
    var evalAreaId;
    var evalAreaName;
    var assessId;
    var assessName;
    var score;
    var remarks;
    var atId;
    var Finalline;
    for (let i = 0; i < newDataArr.length; i++) {

      if (newDataArr[i].areaId == null) {
        newDataArr[i].areaId = '';
      }
      if (newDataArr[i].areaName == null) {
        newDataArr[i].areaName = '';
      }

      spectrId = newDataArr[i].spectrId;
      spectrName = newDataArr[i].spectrName;
      compId = newDataArr[i].compId;
      compName = newDataArr[i].compName;
      evalAreaId = newDataArr[i].areaId;
      evalAreaName = newDataArr[i].areaName;
      assessName = newDataArr[i].assessName;
      score = newDataArr[i].score;
      remarks = newDataArr[i].remarks;
      atId = newDataArr[i].atId;
      let line = spectrId + "|" + spectrName + "|" + compId + "|" + compName + "|" + evalAreaId + "|" + evalAreaName
        + "|" + assessName + "|" + score + "|" + remarks + "|" + atId;
      if (i == 0) {
        Finalline = line;
      } else {
        Finalline += "#" + line;
      }

    }
    return Finalline;
    // console.log('Finalline',Finalline);
  }
}
