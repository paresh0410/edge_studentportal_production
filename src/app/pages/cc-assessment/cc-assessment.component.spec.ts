import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcAssessmentComponent } from './cc-assessment.component';

describe('CcAssessmentComponent', () => {
  let component: CcAssessmentComponent;
  let fixture: ComponentFixture<CcAssessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcAssessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
