import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EepProfileComponent } from './eep-profile.component';

describe('EepProfileComponent', () => {
  let component: EepProfileComponent;
  let fixture: ComponentFixture<EepProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EepProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EepProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
