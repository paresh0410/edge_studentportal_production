import { Component, Input, OnChanges, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { EEPService } from '../../service/eep.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ENUM } from '../../service/enum';
import { ImageCroppedEvent } from '../../../../node_modules/ngx-image-cropper';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'ngx-eep-profile',
  templateUrl: './eep-profile.component.html',
  styleUrls: ['./eep-profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EepProfileComponent implements OnInit, OnChanges {
  @Output() public passData1 = new EventEmitter();
  @Input() response_data;
  @Input() selectIndex;
  employeeDetail: any = {};
  options: FormGroup;
  cocProfileData: any;
  userDetails: any;
  tenantId: any;
  employeeId: any;
  userId: any;
  wfresonseId: any;
  profileForm: any = {};
  imageChangedEvent: any = "";
  croppedImage: any = "";
  showCropper = false;
  intialImage = false;
  selectImage = false;
  workflowdetail: any = [];
  selectedstep: any;
  itemsList = [
    {
      id: '1',
      name: 'Yes',
      value: 1,
    },
    {
      id: '2',
      name: 'No',
      value: 0,
    }];
  chosenItem;
  proData: any;
  constructor(private eepservice: EEPService,
    public spinner: NgxSpinnerService, fb: FormBuilder,
    private toastr: ToastrService) {
    if (this.eepservice.cocProfileData) {
      this.cocProfileData = this.eepservice.cocProfileData;
      console.log('this.cocProfileData', this.cocProfileData);
    }
    this.workflowdetail = this.eepservice.workflow;
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userId = this.userDetails.id;
    this.tenantId = this.userDetails.tenantId;
    this.employeeId = localStorage.getItem('employeeId');
    console.log('employeeId', this.employeeId);
    console.log('this.userDetails', this.userDetails);
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getCocData();
    this.getEmpData();
  }
  ngOnChanges() {
    console.log("Data ==>", this.response_data);
    if (this.response_data) {
      if (this.response_data.status === 1 && this.response_data.status === 0) {
        // this.showbtn = false;
      } else {
        this.getCocData();
        // this.showbtn = true;
      }
    }
  }
  getCocData() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.eep_codeofconduct;
    const param = {
      'wfId': this.workflowdetail.id,
      'empId': this.employeeId,
      'tId': this.tenantId,
    };
    this.eepservice.get(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT COC Success===>', result);
      console.log('result.data', result.data[0]);
      this.proData = result.data[0];
      if (this.proData) {
        this.profileForm = {
          'contact': this.proData.contactNo,
          'comment': this.proData.comment,
          'isTrained': this.proData.isTrained,
          'picRef': this.proData.profilePicRef,
          'wfrId': this.proData.wfrId,
        };
        if (!this.profileForm.picRef) {
          this.profileForm.picRef = "../../../assets/images/user.png";
        }
        this.chosenItem = this.profileForm.isTrained;
        console.log('this.profileForm', this.profileForm);
      } else {
        console.log('Please agree coc');
        this.profileForm = {};
      }
      this.wfresonseId = result.data[0].wfrId;
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });
  }
  getEmpData() {
    this.spinner.show();
    const url = ENUM.domain + ENUM.url.eep_emp_data;
    const param = {
      'empId': this.employeeId,
      'tId': this.tenantId,
    };
    this.eepservice.get(url, param).then((result: any) => {
      this.spinner.hide();
      console.log('RESULT COC Success===>', result);
      console.log('result.data', result.data[0]);
      const empData = result.data[0];
      if (empData) {
        this.employeeDetail = {
          ecn: empData.ecn,
          employeeName: empData.name,
          contact: empData.contact,
          email: empData.email,
          band: empData.band,
          business: empData.business,
          subDepartment: empData.subdept,
          reportingManager: empData.rmname,
        };
      }
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });
  }
  onItemChange(item) {
    console.log(item);
    this.profileForm.isTrained = item.value;
    console.log(this.profileForm);
  }
  changeProfile(event: any): void {
    this.intialImage = true;
    this.selectImage = true;
    this.imageChangedEvent = event;
    console.log(this.imageChangedEvent);
  }
  update(profileData) {
    if (this.proData.responseType === 1) {
      this.checkvalid(profileData, res => {
        if (res == 'true') {
          this.updateProfile(profileData);
        } else {
          this.toastr.warning('Please fill the required field', "error!").onHidden.subscribe(()=>
          this.toastr.clear());
        }
      })
    } else if (this.proData.responseType == 0) {
      this.toastr.warning('You are no longer accessibleto EEp workflow ', "Error!").onHidden.subscribe(()=>
      this.toastr.clear());
    } else {
      this.toastr.info('Please accept tearms & conditions first', "Info!").onHidden.subscribe(()=>
      this.toastr.clear());
    }
  }
  checkvalid(profileData, cb) {
    console.log(profileData);
    if (profileData.picRef) {
        cb('true');
    } else {
      cb('false');
    }
  }
  updateProfile(profileData) {
    console.log('profileData', profileData);

    this.spinner.show();
    const url = ENUM.domain + ENUM.url.eep_Updatetttprofile;
    const param = {
      'contNo': null,
      'cmt': profileData.comment,
      'isTrained':null,
      'picRef': profileData.picRef,
      'userId': this.userId,
      'wfrId': profileData.wfrId,
      'wfId': this.workflowdetail.id,
      'empId': this.employeeId,
      'tId': this.tenantId,
      'stepId': 1,
    };
    if (!param.picRef) {
      param.picRef = this.croppedImage;
    }
    console.log('param', param);
    this.eepservice.get(url, param).then((result: any) => {
      this.spinner.hide();
      if (result.type == true) {
        this.toastr.success(result.data[0].msg, "Success!").onHidden.subscribe(()=>
        this.toastr.clear());
        console.log('RESULT Profile Success===>', result);
        this.selectedstep = '1';
        this.passData1.emit(this.selectedstep);
      } else {
        this.toastr.warning(result.data[0].msg, "error!").onHidden.subscribe(()=>
        this.toastr.clear());
      }
    }).catch(result => {
      this.spinner.hide();
      console.log('RESULT COC Error===>', result);
    });

  }

  fileChangeEvent(event: any): void {
    this.intialImage = true;
    this.selectImage = true;
    this.imageChangedEvent = event;
    console.log(this.imageChangedEvent);
    // this.intialImage=true;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    console.log('Crop Image ===>', this.croppedImage);
  }

  imageLoaded() {
    this.showCropper = true;
    console.log("Image loaded");
  }

  dissmissImageCropPopModal(index) {
    this.intialImage = false;
    this.selectImage = false;
    this.profileForm.picRef = this.croppedImage;
  }
}
