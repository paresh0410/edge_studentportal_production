import { Component, OnInit } from '@angular/core';
import { InterestsServiceProvider } from '../../service/interests-service';
import { ENUM } from '../../service/enum';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner'

@Component({
  selector: 'ngx-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.scss']
})
export class InterestsComponent implements OnInit {

	public interestArr:any = [];
	displayButton: boolean = true;  
	selectedArray:any = [];
	tag:any;
	constructor(public ISP: InterestsServiceProvider, private router:Router, private routes:ActivatedRoute,
		public spinner: NgxSpinnerService) { 

      	var id= document.getElementById('nb-global-spinner');
		id.style.display = "none";

		var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		this.spinner.show();
		var params = {
			uId: userDetails.id,
			tId: userDetails.tenantId
		}
		var url = ENUM.domain + ENUM.url.interestPage;
		this.ISP.getInterestsData(url,params)
  		.then(result=>{
			var temp:any = result
			if(temp.data != undefined || temp.data != null){
				if(temp.data.length > 0){
					this.displayButton = true;
					this.interestArr = temp.data;
					this.spinner.hide();
					console.log('InterestsData--->',this.interestArr);
					for(var i=0;i<this.interestArr.length;i++){
						for(var j=0;j<this.interestArr[i].group.length;j++){
							if(this.interestArr[i].group[j].isSelected == 1){
								this.selectedArray.push(this.interestArr[i].group[j].tag);
							}
						}
					}
				}else{
					// this.displayButton = false;
					this.spinner.hide();
					this.skipPage();
				}
			}else{
				// this.displayButton = false;
				this.spinner.hide();
				this.skipPage();
			}
		}).catch(result =>{
			this.spinner.hide();
		  	console.log("ServerResponseError :",  result);
	  	})

  	}

  	ngOnInit() {
	}
	  
	interested(index1, index2, check){
		if(check == 0){
			this.interestArr[index1].group[index2].isSelected = 1;
			this.selectedArray.push(this.interestArr[index1].group[index2].tag);
		}else if(check == 1){
			this.interestArr[index1].group[index2].isSelected = 0;
			if(this.selectedArray.length > 1){
				for(var i=0;i<this.selectedArray.length;i++){
					if(this.selectedArray[i] == this.interestArr[index1].group[index2].tag){
						this.selectedArray.splice(i,1)
					}
				}
			}else if(this.selectedArray.length == 1){
				this.selectedArray = [];
			}
		}
	}

	skipPage(){
		// this.spinner.show();
		this.router.navigate(['../pages/learn'],{relativeTo:this.routes});
		// this.spinner.hide();
	}

	addInterests(){
		// this.spinner.show();
		var selectedString = this.selectedArray.join('|');
		console.log('ADD-DATA--->',selectedString);
		var userDetails = JSON.parse(localStorage.getItem('userDetails'));
		var params = {
			uId: userDetails.id,
			tId: userDetails.tenantId,
			tags: selectedString
		}
		var url = ENUM.domain + ENUM.url.addInterests;
		this.ISP.addInterestsData(url,params)
  		.then(result=>{
			console.log('DATA--->',result);
			// this.spinner.hide();
		}).catch(result =>{
			console.log("ServerResponseError :",  result);
			// this.spinner.show();
	  	})
	}

}
