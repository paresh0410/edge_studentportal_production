import { CourseCard } from './course-card.model';

export const catalogConfig: CourseCard = {
  showHover: true,
  showImage: true,
  image: 'coursePicRef',
  showHeart: true,
  isFav: 'wishList',
  course: 'isWorkFlow',
  classroom: 'cClassroom',
  courseName: 'courseTitle',
  showDate: true,
  courseDate: 'validToDate',
  showPoint: true,
  pointLabel: 'cPointLabel',
  points: 'cpoints',
  isLevel: true,
  levleLabel: 'cLevleLabel',
  level: 'courseLevelId',
  isPrice: true,
  coursePrice: 'cDiscountPrice',
  progressLike: false,
  completePercent: 'percComplete',
  // completePercentCourse: 'percComplete',
  // completePercentWorkflow: 'perComplete',
  showProgress: false,
  showLike: false,
  isLike: 'isLike',
  isDislike: 'isDislike',
  showaAuthor: true,
  authorName: 'cAuthorName',
  courseDescri: 'courseDesc',
  hoverPrice: true,
  courseDiscount: 'cCourseDiscount',
  originalPrice: 'cOriginalPrice',
  discountPrice: 'cDiscountPrice',
  showaEnroll: false,
  showaBuynow: true,
  compStatus: 'compStatus',
  courseType: 'courseType2',
  approvalStatus: 'approvalStatus',
  courseStartDate: 'startDate',
  courseEndDate: 'endDate',
};

export const openConfig: CourseCard = {
  showHover: true,
  showImage: true,
  image: 'typeImage',
  showHeart: false,
  isFav: 'cisFav',
  course: 'isWorkFlow',
  classroom: 'cClassroom',
  courseName: 'courseTitle',
  showDate: true,
  courseDate: 'endDate',
  showPoint: true,
  pointLabel: 'cPointLabel',
  points: 'cpoints',
  // levelPrice: false,
  isLevel: true,
  levleLabel: 'cLevleLabel',
  level: 'courseLevelId',
  isPrice: false,
  coursePrice: 'cCoursePrice',
  progressLike: false,
  completePercent: 'percComplete',
  // completePercentCourse: 'percComplete',
  // completePercentWorkflow: 'perComplete',
  showProgress: false,
  showLike: false,
  isLike: 'isLike',
  isDislike: 'isDislike',
  showaAuthor: true,
  authorName: 'cAuthorName',
  courseDescri: 'courseDesc',
  hoverPrice: false,
  courseDiscount: 'cCourseDiscount',
  originalPrice: 'cOriginalPrice',
  discountPrice: 'cDiscountPrice',
  showaEnroll: true,
  showaBuynow: false,
  compStatus: 'compStatus',
  courseType: 'courseType2',
  approvalStatus: 'approvalStatus',
  courseStartDate: 'startDate',
  courseEndDate: 'endDate',
};

export const wishConfig: CourseCard = {
  showHover: true,
  showImage: true,
  image: 'coursePicRef',
  showHeart: true,
  isFav: 'wishList',
  course: 'isWorkFlow',
  classroom: 'cClassroom',
  courseName: 'courseTitle',
  showDate: true,
  courseDate: 'validToDate',
  showPoint: true,
  pointLabel: 'cPointLabel',
  points: 'cpoints',
  isLevel: true,
  levleLabel: 'cLevleLabel',
  level: 'courseLevelId',
  isPrice: true,
  coursePrice: 'cDiscountPrice',
  progressLike: false,
  completePercent: 'percComplete',
  // completePercentCourse: 'percComplete',
  // completePercentWorkflow: 'perComplete',
  showProgress: false,
  showLike: false,
  isLike: 'isLike',
  isDislike: 'isDislike',
  showaAuthor: true,
  authorName: 'cAuthorName',
  courseDescri: 'courseDesc',
  hoverPrice: true,
  courseDiscount: 'cCourseDiscount',
  originalPrice: 'cOriginalPrice',
  discountPrice: 'cDiscountPrice',
  showaEnroll: false,
  showaBuynow: true,
  compStatus: 'compStatus',
  courseType: 'courseType2',
  approvalStatus: 'approvalStatus',
  courseStartDate: 'startDate',
  courseEndDate: 'endDate',
};

export const courseConfig: CourseCard = {
  showHover: true,
  showImage: true,
  image: 'typeImage',
  showHeart: false,
  isFav: 'cisFav',
  course: 'isWorkFlow',
  classroom: 'courseType2',
  courseName: 'courseTitle',
  showDate: true,
  courseDate: 'endDate',
  showPoint: true,
  pointLabel: 'cPointLabel',
  points: 'cpoints',
  // levelPrice: false,
  isLevel: false,
  levleLabel: 'cLevleLabel',
  level: 'courseLevelId',
  isPrice: false,
  coursePrice: 'cCoursePrice',
  progressLike: true,
  completePercent: 'percComplete',
  // completePercentCourse: 'percComplete',
  // completePercentWorkflow: 'perComplete',
  showProgress: true,
  showLike: true,
  isLike: 'isLike',
  isDislike: 'isDislike',
  showaAuthor: true,
  authorName: 'cAuthorName',
  courseDescri: 'courseDesc',
  hoverPrice: false,
  courseDiscount: 'cCourseDiscount',
  originalPrice: 'cOriginalPrice',
  discountPrice: 'cDiscountPrice',
  showaEnroll: false,
  showaBuynow: false,
  showaShare: true,
  compStatus: 'compStatus',
  courseType: 'courseType2',
  approvalStatus: 'approvalStatus',
  courseStartDate: 'startDate',
  courseEndDate: 'endDate',
};

export const pathwayConfig: CourseCard = {
  showHover: true,
  showImage: true,
  image: 'typeImage',
  showHeart: false,
  isFav: 'cisFav',
  course: 'isWorkFlow',
  classroom: 'courseType2',
  courseName: 'wname',
  showDate: true,
  courseDate: 'wf_enddate',
  showPoint: true,
  pointLabel: 'cPointLabel',
  points: 'workflowTotalPoints',
  // levelPrice: false,
  isLevel: false,
  levleLabel: 'cLevleLabel',
  level: 'courseLevelId',
  isPrice: false,
  coursePrice: 'cCoursePrice',
  progressLike: true,
  completePercent: 'perComplete',
  // completePercentCourse: 'percComplete',
  // completePercentWorkflow: 'perComplete',
  showProgress: true,
  showLike: false,
  isLike: 'isLike',
  isDislike: 'isDislike',
  showaAuthor: false,
  authorName: 'cAuthorName',
  courseDescri: 'courseDesc',
  hoverPrice: false,
  courseDiscount: 'cCourseDiscount',
  originalPrice: 'cOriginalPrice',
  discountPrice: 'cDiscountPrice',
  showaEnroll: false,
  showaBuynow: false,
  showaShare: true,
  compStatus: 'compStatus',
  courseType: 'courseType2',
  approvalStatus: 'approvalStatus',
  courseStartDate: 'wf_stdate',
  courseEndDate: 'wf_enddate'
};

export const learnTabs: any = [
  {
    id: 1,
    name: 'Catalog',
    type: 'ALL',
    visible: true,
    subTabs: [],
    courseConfig: catalogConfig,
    noDataFoundContainer: {
      image: 'assets/images/no-data.svg',
      title: 'Courses not available',
      discription: 'Courses you are looking for are not available this time, please try after some time.',
    },
  },
  {
      id: 2,
      name: 'My Courses',
      type: 'ALL',
      visible: true,
      subTabs: [
          {
              id: 1,
              name: 'On-Going',
              subType: 'CURRENT',
              courseList: [],
              courseConfig: courseConfig,
              currentPage: '',
              totalRows: '',
              status: true,
              visible: true,
          },
          {
              id: 2,
              name: 'Upcoming',
              subType: 'UPCOMING',
              courseList: [],
              courseConfig: courseConfig,
              currentPage: '',
              totalRows: '',
              status: false,
              visible: true,
          },
          {
              id: 3,
              name: 'Completed',
              subType: 'COMPLETED',
              courseList: [],
              courseConfig: courseConfig,
              currentPage: '',
              totalRows: '',
              status: false,
              visible: true,
          },
      ],
      courseConfig: courseConfig,
      noDataFoundContainer: {
        image: 'assets/images/no-data.svg',
        title: 'Courses not available',
        discription: 'Courses you are looking for are not available this time, please try after some time.',
      },
  },
  {
      id: 3,
      name: 'My Pathways',
      type: 'ALL',
      visible: true,
      subTabs: [
        {
            id: 1,
            name: 'On-Going',
            subType: 'CURRENT',
            courseList: [],
            courseConfig: pathwayConfig,
            currentPage: '',
            totalRows: '',
            status: true,
            visible: true,
        },
        {
            id: 2,
            name: 'Upcoming',
            subType: 'UPCOMING',
            courseList: [],
            courseConfig: pathwayConfig,
            currentPage: '',
            totalRows: '',
            status: false,
            visible: true,
        },
        {
            id: 3,
            name: 'Completed',
            subType: 'COMPLETED',
            courseList: [],
            courseConfig: pathwayConfig,
            currentPage: '',
            totalRows: '',
            status: false,
            visible: true,
        },
      ],
      courseConfig: pathwayConfig,
      noDataFoundContainer: {
        image: 'assets/images/no-data.svg',
        title: 'Courses not available',
        discription: 'Courses you are looking for are not available this time, please try after some time.',
      },
  },
  {
    id: 4,
    name: 'Open Courses',
    type: 'SELF',
    visible: true,
    subTabs: [
      {
          id: 1,
          name: 'On-Going',
          subType: 'OPEN',
          courseList: [],
          courseConfig: openConfig,
          currentPage: '',
          totalRows: '',
          status: false,
          visible: true,
      },
    ],
    courseConfig: openConfig,
    noDataFoundContainer: {
      image: 'assets/images/no-data.svg',
      title: 'Courses not available',
      discription: 'Courses you are looking for are not available this time, please try after some time.',
    },
  },
  {
    id: 5,
    name: 'Wishlisted',
    type: 'ALL',
    visible: true,
    subTabs: [
      {
        id: 1,
        name: 'Wishlist',
        subType: 'WISHLIST',
        courseList: [],
        courseConfig: wishConfig,
        currentPage: '',
        totalRows: '',
        status: false,
        visible: true,
      },
    ],
    courseConfig: wishConfig,
    noDataFoundContainer: {
      image: 'assets/images/no-data.svg',
      title: 'Your wishlist is empty',
      discription: 'Explore more and shortlist some courses.',
    },
  },
];
