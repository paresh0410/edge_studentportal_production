export class Filter {
    constructor(
        public ascending?: boolean,
        public descending?: boolean,
        public showDropdown?: boolean,
        public dropdownlabel?: string,
        public dropdownList?: any,
    ) { }
}

