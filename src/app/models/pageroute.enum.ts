export enum PageRouteEnum {
    MANAGER= 'manager',
    LEARNER= 'learner',
    TRAINER= 'trainer',
    PARTNER= 'partner'
}