export class Header {
    constructor(
        public showLogo?: boolean,
        public showCategories?: boolean,
        public showSearch?: boolean,
        public showCatalog?: boolean,
        public showCourses?: boolean,
        public showPathways?: boolean,
        public showNotifications?: boolean,
        public showProfile?: boolean,
        public showSignIn?: boolean,
        public showSignUp?: boolean,
    ) { }
}
