import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-course-details-skeleton',
  templateUrl: './course-details-skeleton.component.html',
  styleUrls: ['./course-details-skeleton.component.scss']
})
export class CourseDetailsSkeletonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
