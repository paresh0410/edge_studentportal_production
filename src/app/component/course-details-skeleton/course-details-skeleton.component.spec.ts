import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseDetailsSkeletonComponent } from './course-details-skeleton.component';

describe('CourseDetailsSkeletonComponent', () => {
  let component: CourseDetailsSkeletonComponent;
  let fixture: ComponentFixture<CourseDetailsSkeletonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseDetailsSkeletonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseDetailsSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
