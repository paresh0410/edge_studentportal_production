import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCardV2Component } from './course-card-v2.component';

describe('CourseCardV2Component', () => {
  let component: CourseCardV2Component;
  let fixture: ComponentFixture<CourseCardV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCardV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCardV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
