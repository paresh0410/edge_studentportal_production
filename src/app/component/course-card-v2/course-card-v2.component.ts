import { Component, OnInit, ElementRef, ViewChild, Input, ViewEncapsulation, EventEmitter, Output, OnChanges, ChangeDetectorRef } from '@angular/core';
import { CourseCard } from '../../models/course-card.model';

@Component({
  selector: 'ngx-course-card-v2',
  templateUrl: './course-card-v2.component.html',
  styleUrls: ['./course-card-v2.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class CourseCardV2Component implements OnInit, OnChanges {
  showDescri: boolean = false;
  liked: boolean = false;
  allsize: any;
  fixedRight: any;
  // isFav: any;

  @ViewChild('parent') parent?: ElementRef<HTMLElement>;
  @ViewChild('hoversec') hoversec?: ElementRef<HTMLElement>;

  @Input() cardsData: any;
  @Input() hover: any;
  @Input() config: CourseCard = {
    showHover: false,
    showImage: false,
    image: '',
    showHeart: false,
    isFav: '',
    course: '',
    classroom: '',
    courseName: '',
    showDate: false,
    courseDate: '',
    showPoint: false,
    pointLabel: '',
    points: '',
    levelPrice: false,
    levleLabel: '',
    level: '',
    coursePrice: '',
    progressLike: false,
    completePercent: '',
    showProgress: false,
    showLike: false,
    isLike: '',
    showaAuthor: false,
    authorName: '',
    courseDescri: '',
    hoverPrice: false,
    courseDiscount: '',
    originalPrice: '',
    discountPrice: '',
    showaEnroll: false,
    showaBuynow: false,
    showaShare: false,
    compStatus: '',
    isDislike: '',
    courseType: '',
    completePercentCourse: '',
    completePercentWorkflow: '',
    isLevel: false,
    isPrice: false,
    approvalStatus: '',
    courseStartDate: '',
    courseEndDate: '',
  };
  setHoverHeight: any;
  @Output() getEvent = new EventEmitter();

  constructor(
    public cdf: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    // this.isFav = 1;
    // console.log(this.config);
  }

  ngOnChanges(changes: any) {
    // console.log('Course card component changes Triggered : ', changes);
  }

  bindBackgroundImage(img) {
    if (img === null || img === '' || img === 'assets/images/courseicon.jpg') {
      // return 'url(' + 'assets/images/card.jpg' + ')';
      return 'assets/images/card.jpg';
    } else {
      return img;
    }
  }

  fireEvent(event, data, isLiked) {
    if (data.compStatus !== 'UPCOMING') {
      const dataOutput = {
        type : event,
        cardData: data,
        liked : isLiked,
      };
      this.getEvent.emit(dataOutput);
      console.log('Fire event called : ', dataOutput);
    } else {
      return false;
    }
  }

  whishlist() {
    // this.isFav = 1;
  }

  hoverdiv(event) {
    // console.log("hover event===", event);
    // console.log('this.parent', this.parent);
    if (this.hoversec) {
      var rect = this.parent.nativeElement.getBoundingClientRect();
      var position = this.hoversec.nativeElement;
      this.allsize = {
        width: rect.width,
        height: rect.height,
        left: rect.left,
        right: rect.right,
        top: rect.top,
        bottom: rect.bottom,
      };
      // console.log("allsize called", this.allsize);

      this.fixedRight = Math.floor(document.documentElement.clientWidth - (this.allsize.left + this.allsize.width));
      // console.log("fixedRight", this.fixedRight);

      if (this.fixedRight <= 285 ) {
        position.classList.add('window-left');
      }
      this.setHoverHeight = this.parent.nativeElement.offsetHeight + 'px';
      // console.log('this.setHoverHeight', this.setHoverHeight);
      this.cdf.detectChanges();
    }
  }

  ProgressInWhole(val) {
    if (val) {
      return Math.round(val);
    } else {
      return 0;
    }
  }

  errorHandler(event){
    if(event){
      // event.target.src = "assets/images/noImage1.png";
      this.cardsData[this.config.image] = 'assets/images/card.jpg';
    }
  }
}
