import { Component, Input, Output, EventEmitter, AfterViewInit, OnInit } from '@angular/core';
import '../../vendor/jitsi/external_api.js';
declare var JitsiMeetExternalAPI: any;
@Component({
  selector: 'app-jitsi',
  templateUrl: './jitsi.component.html',
  styleUrls: ['./jitsi.component.scss']
})
export class JitsiComponent implements AfterViewInit, OnInit {
  title: string = 'ng Jitsi';
  // domain: string = 'meet.nextedge.online';
  domain: string = 'meet.edgelearning.co.in';
  options: any;
  api: any;
  meet: any;
  @Input() opt: any;
  @Output() getEvent = new EventEmitter<any>();
  constructor() { }

  ngAfterViewInit(): void {
    // console.log('opt', this.opt)
    // this.options = {
    //   roomName: this.opt.sessionName,
    //   width: '100%', // window.innerWidth,
    //   height: window.innerHeight,
    //   parentNode: document.querySelector('#meet'),
    //   userInfo: {
    //     // email: 'harshumalave@gmail.com',
    //     displayName: this.opt.myUserName,
    //   }
    // }
    // this.api = new JitsiMeetExternalAPI(this.domain, this.options);
    // console.log('jitsi api =>', this.api);

    // this.api.addEventListener('videoConferenceJoined  ', function (event) {
    //   console.log('videoConferenceJoined   =>' + event);
    // });
    // this.api.addEventListener('videoConferenceLeft', (event) => {
    //   console.log('participantLeft =>' + event);

    //   this.getEvent.emit(event);
    //   //window.history.back();
    //   // this.onStateChangeEvent(event);
    // });

  }


  //  onStateChangeEvent(event){
  //   this.getEvent.emit(event); // now correct this
  // }

  ngOnInit(): void {
    console.log('opt', this.opt)
    this.options = {
      roomName: this.opt.sessionName,
      width: '100%', // window.innerWidth,
      height: window.innerHeight,
      parentNode: document.querySelector('#meet'),
      userInfo: {
        // email: 'harshumalave@gmail.com',
        displayName: this.opt.myUserName,
      }
    }
    // this.domain = this.opt.domain;
    if(this.opt.domain){
      const url = new URL(this.opt.domain);
      this.domain = url.hostname; // "aaa.bbb.ccc.com";
    }


    this.api = new JitsiMeetExternalAPI(this.domain, this.options);
    console.log('jitsi api =>', this.api);
    console.log('domain  =>', this.domain);

    this.api.addEventListener('videoConferenceJoined  ', function (event) {
      console.log('videoConferenceJoined   =>' + event);
    });
    this.api.addEventListener('videoConferenceLeft', (event) => {
      console.log('participantLeft =>' + event);

      this.getEvent.emit(event);
      //window.history.back();
      // this.onStateChangeEvent(event);
    });

  }
}
