export class Toaster {
    title: string;
    subtitle: string;
    message: string;
    type: string;
    okTap: string;
    cancelTap: string;
}