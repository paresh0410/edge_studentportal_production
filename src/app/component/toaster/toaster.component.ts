import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Toaster } from './toaster';
@Component({
  selector: 'ngx-toaster-custom',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.scss']
})
export class ToasterComponent implements OnInit {
  @Input() toaster: Toaster;
  @Output() okTap = new EventEmitter<any>();
  @Output() cancelTap = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    console.log('Toaster : ', this.toaster);
  }
  /**
   * @event - Ok Event
   */
  okTapEvent() {
    if (this.okTap) {
      this.okTap.emit(true);
    }
  }

  /**
   * @event - Cancel Event
   */
  cancelTapEvent() {
    if (this.cancelTap) {
      this.cancelTap.emit(false);
    }
  }
}
