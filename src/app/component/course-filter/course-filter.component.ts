import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CatalogueService } from '../../service/learn-container/catalogue.service';
import { ToasterService } from '../../service/learn-container/toaster.service';

@Component({
  selector: 'ngx-course-filter',
  templateUrl: './course-filter.component.html',
  styleUrls: ['./course-filter.component.scss']
})
export class CourseFilterComponent implements OnInit {
  filter: any = []

  filterInnertag: any = [];

  @Input() filtersData: any = [];
  @Input() categoryId: any = 0;
  @Output() getEvent = new EventEmitter();

  selectedFilters: any = '';

  // sortStr: any = '';
  // priceStr: any = '';
  // levelStr: any = '';
  // tagStr: any = '';

  constructor(
    public catalogueService: CatalogueService,
    private toastr: ToasterService,
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: any) {
    console.log('Course Filter component changes Triggered : ', changes);
    if (this.filtersData.length > 0) {
      this.getSelectedSortVal();
    }
  }

  showdiv(i, data) {
    // this.filter[i].show = !this.filter[i].show;
    this.filtersData[i].show = !this.filtersData[i].show;
  }

  checkChange(item) {
    // this.clickToCheck.emit(this.isChecked);
    item.checked = !item.checked;
    console.log(item);
  }

  fireEvent(event, data) {
    const dataOutput = {
      type : event,
      filter: data,
      updatedFilterData: this.filtersData,
    };
    // if (data.selected != undefined) {
    //   data.selected = !data.selected;
    // } else {
    //   data['selected'] = true;
    // }
    this.getEvent.emit(dataOutput);
    // console.log('Filter Fire event called : ', dataOutput);
    // console.log('Updated filter data : ', this.filtersData);
  }

  makeFilterDataReady() {

    // let priceStr = '';
    // let levelStr = '';
    // let tagStr = '';
    // let filterCount = 0;

    // for (let i = 0; i < this.filtersData.length; i++) {
    //   const filter = this.filtersData[i];
    //   if (filter.id == 1) {
    //     if (this.selectedFilters !== '') {
    //       priceStr = this.selectedFilters;
    //       filterCount++;
    //     }
    //   } else if (filter.id == 2) {
    //     for (let j = 0; j < filter.innerTag.length; j++) {
    //       const filterData = filter.innerTag[j];
    //       if (filterData.checked) {
    //         levelStr = this.getFilterString(levelStr, filterData);
    //         filterCount++;
    //       }
    //     }
    //   }  else if (filter.id == 4) {
    //     for (let j = 0; j < filter.innerTag.length; j++) {
    //       const filterData = filter.innerTag[j];
    //       if (filterData.checked) {
    //         tagStr = this.getFilterString(tagStr, filterData);
    //         filterCount++;
    //       }
    //     }
    //   }
    // }

    // const filters = {
    //   price: priceStr,
    //   level: levelStr,
    //   tag: tagStr,
    // };

    // // return filters;

    // if (filterCount > 0) {
    //   this.fireEvent('apply', filters);
    // } else {
    //   console.log('Please add filters...');
    // }

    // const filters = {
    //   arrFilters: this.filtersData,
    //   strFilters: this.selectedFilters,
    // };
    // const appliedFilters = this.catalogueService.makeFilterDataReady(filters);

    const selectedOptIndex = this.filtersData.findIndex((obj => obj.id == 1));
    const selectedOptArr = this.filtersData[selectedOptIndex].innerTag;
    this.filtersData[selectedOptIndex].innerTag = selectedOptArr.map(p =>
      String(p.id) == this.selectedFilters
        ? { ...p, checked: true }
        : { ...p, checked: false }
    );
    console.log('Updated filter data : ', this.filtersData);

    const appliedFilters = this.catalogueService.makeFilterDataReady(this.filtersData);

    if (appliedFilters) {
      this.fireEvent('apply', appliedFilters);
    } else {
      console.log('Please add filters...');
      this.toastr.prsentToast('Please add some filters first', 'No filters added!', 'warning');
    }
  }

  getFilterString(str, filter) {
    if (str == '') {
      str = filter.id;
    } else {
      str = str + ',' + filter.id;
    }
    return str;
  }

  onPriceSelected(event, i, arr) {
    console.log('Price selected : ', event, arr);
    this.selectedFilters = event.target.value;
  }

  getSelectedSortVal() {
    const selectedOptIndex = this.filtersData.findIndex((obj => obj.id == 1));
    const selectedOptArr = this.filtersData[selectedOptIndex].innerTag;
    for (let i = 0; i < selectedOptArr.length; i++) {
      const n = selectedOptArr[i];
      if (n.checked == true) {
          this.selectedFilters = n.id;
          break;
       }
    }
  }

  clearFilters() {
    const data = {
      id: this.categoryId,
    };
    this.fireEvent('clear', data);
  }

}
