import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-course-card-skeleton',
  templateUrl: './course-card-skeleton.component.html',
  styleUrls: ['./course-card-skeleton.component.scss']
})
export class CourseCardSkeletonComponent implements OnInit {

  @Input() count: any;

  constructor() { }

  ngOnInit() {
  }

  skeletons(n: number): any[] {
    return Array(n);
  }

}
