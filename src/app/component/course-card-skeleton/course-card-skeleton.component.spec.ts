import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCardSkeletonComponent } from './course-card-skeleton.component';

describe('CourseCardSkeletonComponent', () => {
  let component: CourseCardSkeletonComponent;
  let fixture: ComponentFixture<CourseCardSkeletonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCardSkeletonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCardSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
