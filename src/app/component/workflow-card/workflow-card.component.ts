import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-workflow-card',
  templateUrl: './workflow-card.component.html',
  styleUrls: ['./workflow-card.component.scss']
})
export class WorkflowCardComponent implements OnInit {

  @Input() data: any;
  // tslint:disable-next-line: no-output-native
  @Output() getEvent = new EventEmitter();
  constructor() {

   }

  ngOnInit() {
    console.log('Data For Cards ===>', this.data);
  }

  ProgressInWhole(val){
    if (val) {
      return Math.round(val);
    } else {
      return 0;
    }
  }
  bindBackgroundImage(img){
    if(img === null || img === '' || img === 'assets/images/courseicon.jpg' ) {
      // const profilePicUrl: SafeResourceUrl = 'assets/images/open-book-leaf.jpg';
      //  const style = `background-image: url(${profilePicUrl})`;
      // // sanitize the style expression
      // return this.sanitizer.bypassSecurityTrustStyle(style);
      return 'url(' + 'assets/images/open-book-leaf.jpg' + ')';
    } else {
    // const profilePicUrl:SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(img);
    //   const style = `background-image: url(${profilePicUrl})`;
    //  // sanitize the style expression
    //  return this.sanitizer.bypassSecurityTrustStyle(style);
      return 'url(' + img + ')';
    }
  }

  fireEvent(event, data) {
    const dataOutput = {
      type : event,
      cardData: data,
    };
    this.getEvent.emit(dataOutput);
  }
}
