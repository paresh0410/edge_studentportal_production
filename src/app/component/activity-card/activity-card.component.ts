import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from "@angular/core";

@Component({
  selector: "ngx-activity-card",
  templateUrl: "./activity-card.component.html",
  styleUrls: ["./activity-card.component.scss"],
})
export class ActivityCardComponent implements OnInit, OnChanges {
  @Input("data") activity: any;
  @Input() disabledAction = false;
  @Output() getEvent = new EventEmitter();

  constructor() {}

  ngOnInit() {
    // this.finddependetactivity(this.activity);
  }

  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    if (this.activity) {
      this.finddependetactivity(this.activity);
    }
  }

  bindBackgroundImage(detail) {
    // console.log("detail", detail);
    // console.log(detail,"detail")
    // detail.img = 'assets/images/open-book-leaf-2.jpg';
    // return {'background-image': 'linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(' + detail.img + ')'};
    if (detail.activity_type === "Quiz") {
      detail.img = "assets/images/activity_image/quiz.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.activity_type === "Feedback") {
      detail.img = "assets/images/activity_image/feedback.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "video" ||
      detail.mimeType === "video/mp4"
    ) {
      detail.img = "assets/images/activity_image/video.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "audio" ||
      detail.mimeType === "audio/mpeg"
    ) {
      detail.img = "assets/images/activity_image/audio.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "application" &&
      detail.mimeType === "application/zip"
    ) {
      detail.img = "assets/images/activity_image/scrom.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "application" &&
      detail.mimeType === "application/pdf"
    ) {
      detail.img = "assets/images/activity_image/pdf.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "kpoint" ||
      detail.mimeType === "embedded/kpoint"
    ) {
      detail.img = "assets/images/activity_image/video.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.referenceType === "image") {
      detail.img = "assets/images/activity_image/image.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (
      detail.referenceType === "application" &&
      detail.mimeType === "application/x-msdownload"
    ) {
      detail.img = "assets/images/activity_image/url.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.formatId == 9) {
      detail.img = "assets/images/activity_image/practice_file.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.formatId == 10) {
      detail.img = "assets/images/activity_image/ppt.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.formatId == 11) {
      detail.img = "assets/images/activity_image/excel.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.formatId == 12) {
      detail.img = "assets/images/activity_image/word.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else {
      detail.img = "assets/images/open-book-leaf-2.jpg";
      return {
        "background-image":
          "linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(" +
          detail.img +
          ")",
      };
    }
  }

  bindActivityIcon(detail) {
    console.log("detail", detail);
    // if (detail.activity_type === 'Quiz') {
    if (detail.activityTypeId == 5) {
      detail.img = "assets/images/activity_image/quiz.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
      // } else if ( detail.activity_type  === 'Feedback') {
    } else if (detail.activityTypeId == 6) {
      detail.img = "assets/images/activity_image/feedback.jpg";
      return {
        "background-image": "url(" + detail.img + ")",
        "background-position": "left",
      };
    } else if (detail.activityTypeId == 11) {
      detail.img = "assets/images/open-book-leaf-2.jpg";
      return {
        "background-image":
          "linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(" +
          detail.img +
          ")",
      };
      // } else if ( detail.activity_type === 'Attendance') {
    } else if (detail.activityTypeId == 9) {
      detail.img = "assets/images/open-book-leaf-2.jpg";
      return {
        "background-image":
          "linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(" +
          detail.img +
          ")",
      };
    } else if (detail.activityTypeId == 1 || detail.activityTypeId == 2) {
      switch (Number(detail.formatId)) {
        case 1: // video
          detail.img = "assets/images/activity_image/video.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 2: // Audio
          detail.img = "assets/images/activity_image/audio.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 3: // PDF
          detail.img = "assets/images/activity_image/pdf.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 4: // K-point
          detail.img = "assets/images/activity_image/video.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 5: // Scrom
          detail.img = "assets/images/activity_image/scrom.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 6: // Youtube
          detail.img = "assets/images/open-book-leaf-2.jpg";
          return {
            "background-image":
              "linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(" +
              detail.img +
              ")",
          };
        case 7: // Image
          detail.img = "assets/images/activity_image/image.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 8: // External link
          detail.img = "assets/images/activity_image/url.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 9: // Practice file
          detail.img = "assets/images/activity_image/practice_file.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 10: // PPT
          detail.img = "assets/images/activity_image/ppt.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 11: // Excel
          detail.img = "assets/images/activity_image/excel.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        case 12: // Word
          detail.img = "assets/images/activity_image/word.jpg";
          return {
            "background-image": "url(" + detail.img + ")",
            "background-position": "left",
          };
        default:
          detail.img = "assets/images/open-book-leaf-2.jpg";
          return {
            "background-image":
              "linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url(" +
              detail.img +
              ")",
          };
      }
    } else {
      return "";
    }
  }
  finddependetactivity(detail) {
    const currentUserData = JSON.parse(localStorage.getItem("userdetail"));
    const currentRole = currentUserData.roleId;
    if (currentRole === 8) {
      detail["begin"] = true;
      detail["wait"] = false;
      detail["completed"] = false;
      detail["pending"] = false;
      if (detail.wCompleted === 1) {
        detail.completed = true;
        detail.wait = false;
        detail.begin = false;
        detail.pending = false;
      } else if (detail.dependentActId !== 0) {
        detail["dependentActName"] = detail.dependentActName;
        if (detail.dependentActCompStatus === 1) {
          detail.wait = false;
          detail.begin = true;
          detail.completed = false;
          detail.pending = false;
        } else {
          detail.wait = true;
          detail.begin = false;
          detail.completed = false;
          detail.pending = false;
        }
      }
      // console.log('Data ==>', detail);
      // if (detail.wCompleted == 1) {
      //   return 3;
      // } else if (detail.wCompleted == 0) {
      //   if (detail.dependentActId == 0) {
      //     if (detail.byTrainer == 1) {
      //       return 2;
      //     } else {
      //       return 1;
      //     }
      //   } else if (detail.dependentActId != 0) {
      //     if (detail.dependentActCompStatus == 0) {
      //       return 4;
      //     } else if (detail.dependentActCompStatus == 1) {
      //       return 1;
      //     }
      //   } if (detail.byTrainer == 1) {
      //     return 2;
      //   }
      // }
      // if(detail.dependentActId == 0){
      //   if(detail.wCompleted == 0 ){
      //     return 1;
      //   }
      // } else if(detail.wCompleted == 0 && detail.byTrainer == 1){
      //   return 2;
      // } else if(detail.wCompleted == 1){
      //   return 3;
      // } else if( detail.dependentActId != 0){
      //   if( detail.wCompleted == 0 && detail.dependentActCompStatus == 1){
      //     return 1;
      //   } else if(detail.wCompleted == 0 && detail.dependentActCompStatus == 0){
      //     return 4
      //   }
      // }
    } else {
      detail["begin"] = true;
      detail["wait"] = false;
      detail["completed"] = false;
      detail["pending"] = false;
    }
  }
  fireEvent(event, data) {
    const dataOutput = {
      type: event,
      cardData: data,
    };
    this.getEvent.emit(dataOutput);
  }
}
