import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-table-checkbox',
  templateUrl: './table-checkbox.component.html',
  styleUrls: ['./table-checkbox.component.scss', '../table.component.scss']
})
export class TableCheckboxComponent implements OnInit {
  @Input() isChecked: boolean;
  @Output() clickToCheck = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  checkChange(event) {
    if(event){
      // console.log('checkevent', event.target.checked);
      // this.isChecked = !this.isChecked;
      this.clickToCheck.emit(event.target.checked);
    }
  }
}
