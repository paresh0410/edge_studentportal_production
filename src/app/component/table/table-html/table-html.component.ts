import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "ngx-table-html",
  templateUrl: "./table-html.component.html",
  styleUrls: ["./table-html.component.scss"],
})
export class TableHtmlComponent implements OnInit {
  @Input() text: string;
  constructor() {}

  ngOnInit() {}

  truncateHTML(text: string): string {
    const charlimit = 25;
    if (!text || text.length <= charlimit) {
      return text;
    }

    const without_html = text.replace(/<(?:.|\n)*?>/gm, "");
    const shortened = without_html.substring(0, charlimit) + "...";
    return shortened;
  }
  coverthtmltotext(data) {
    if (data) {
      const htmlString = data;
      const stripedHtml = htmlString.replace(/<[^>]+>/g, "");
      // console.log(stripedHtml);
      return stripedHtml;
    }
  }
}
