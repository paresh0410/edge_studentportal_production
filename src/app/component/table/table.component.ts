import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ngx-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() label: any = {};
  @Input() tableData: any = {};
  @Input() allCheck: any= false;
  @Input() paginationObject ? = {
    itemsPerPage: 10,
    id : '',
  };
  @Input() pagination: any= false;
  @Output() selectedItems  = new EventEmitter<any>();
  @Output() performAction  = new EventEmitter<any>();
  checkAllFlag = false;
  selectedData: any = [];
  // plainText: boolean = true;
  // checkbox: boolean = true;
  // input: boolean = true;
  // select: boolean = true;
  // date: boolean = true;
  // button: boolean = true;

  constructor() { }


  tableList: any = [
  ];

  ngOnInit() {
    this.paginationObject.id = Math.random().toString(36).substring(7);
    // this.returnSelectedItem();
    console.log('label', this.label);
  }


  checkChange(event,item, property) {
    item[property] = event;
    this.returnSelectedItem();
  }

  toggelChange(event, item, property){
    item[property] = event;
    this.passEventToParent('toggelChange', item);
  }
  clickToButton(item) {
    console.log("click button", item);
  }



  selectAll(event) {
    // console.log('tableData==================>',this.tableData);
    this.selectedData = [];
    // for(let i = 0; i< this.tableData.length; i++) {
    //   if(this.tableData[i].checked == undefined ||
    // this.tableData[i].checked == false || this.tableData[i].checked == true) {
    //     this.checkChange(this.tableData[i]);
    //   }
    // }
    // this.checkAll.emit();
    if (this.tableData){
      this.tableData.forEach(element => {
        element['checked'] = event;
        if(element['checked']){
          this.selectedData.push(element);
        }
      });
    }
    if (this.selectedData.length === this.tableData.length){
      this.checkAllFlag = true;
      // console.log('Checked All Changed:' , this.checkAllFlag);
    }else{

      this.checkAllFlag = false;
      // console.log('Checked All Changed:' , this.checkAllFlag);
    }
    this.selectedData = [...this.tableData];
    this.selectedItems.emit(this.selectedData);
  }

  returnSelectedItem() {
    this.selectedData = [];
    this.tableData.forEach(element => {
        if (element.checked){
          this.selectedData.push(element);
        }
    });
    if (this.selectedData.length === this.tableData.length){
      this.checkAllFlag = true;
    }else{
      this.checkAllFlag = false;
    }
    console.log(this.selectedData,"this.selected Data")
    this.selectedItems.emit(this.selectedData);
  }

  passEventToParent(action, ...args) {
    console.log('action to be performed ==>', action, args);
    const event = {
      action: action,
      argument: args,
    };
    console.log('action to be performed ==>', event);
    this.performAction.emit(event);
  }
}
