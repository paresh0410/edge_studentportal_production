import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'ngx-table-button',
  templateUrl: './table-button.component.html',
  styleUrls: ['./table-button.component.scss']
})
export class TableButtonComponent implements OnInit {

  @Input() labelText: string = 'button';
  @Output() buttonClicked  = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  sendButtonClickedEvent(){
    this.buttonClicked.emit();
  }
}
