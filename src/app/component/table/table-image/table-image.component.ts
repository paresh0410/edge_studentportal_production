import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-table-image',
  templateUrl: './table-image.component.html',
  styleUrls: ['./table-image.component.scss']
})
export class TableImageComponent implements OnInit {
  @Input() imgSrc: string;
  constructor() { }

  ngOnInit() {
  }

}
