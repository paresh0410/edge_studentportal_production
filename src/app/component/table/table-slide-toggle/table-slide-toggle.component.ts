import { Component, OnInit, Input, Output,  EventEmitter} from '@angular/core';

@Component({
  selector: 'ngx-table-slide-toggle',
  templateUrl: './table-slide-toggle.component.html',
  styleUrls: ['./table-slide-toggle.component.scss']
})
export class TableSlideToggleComponent implements OnInit {
  @Input() isChecked: boolean;
  @Output() clickToCheck = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  checkChange(event) {
    if(event){
      // console.log('checkevent', event.checked);
      // this.isChecked = !this.isChecked;
      this.clickToCheck.emit(event.checked);
    }
  }
}
