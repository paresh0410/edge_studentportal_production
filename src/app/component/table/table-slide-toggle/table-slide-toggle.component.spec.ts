import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSlideToggleComponent } from './table-slide-toggle.component';

describe('TableSlideToggleComponent', () => {
  let component: TableSlideToggleComponent;
  let fixture: ComponentFixture<TableSlideToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSlideToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSlideToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
