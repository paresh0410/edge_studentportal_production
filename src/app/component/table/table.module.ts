import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
// import { SafePipe } from '../../safe.pipe';
import { TableComponent } from './table.component';
import { TableCheckboxComponent } from './table-checkbox/table-checkbox.component';
import { TableTextComponent } from './table-text/table-text.component';
import { TableImageComponent } from './table-image/table-image.component';
import { TableSlideToggleComponent } from './table-slide-toggle/table-slide-toggle.component';
import { TableHtmlComponent } from './table-html/table-html.component';
import { TableButtonComponent } from './table-button/table-button.component';


const Components = [
  TableComponent,
  TableCheckboxComponent,
  TableTextComponent,
  TableImageComponent,
  TableSlideToggleComponent,
  TableHtmlComponent,
  TableButtonComponent,
];
@NgModule({
  declarations: [
    ...Components,
    // SafePipe,
  ],
  imports: [
    CommonModule,
    // FormsModule,
    MatSlideToggleModule,
  ],
  exports: [
    ...Components,
  ],
})
export class TableModule { }
