import { Component, OnInit } from '@angular/core';
import {ImageViewerConfig, CustomEvent} from '../image-viewer/image-viewer-config.model';
@Component({
  selector: 'app-image-viewer-container',
  templateUrl: './image-viewer-container.component.html',
  styleUrls: ['./image-viewer-container.component.scss']
})
export class ImageViewerContainerComponent implements OnInit {
  config: ImageViewerConfig = {customBtns: [{name: 'exit', icon: 'fa fa-times'}]};
  imageIndexOne = 0;
  images = [
    'https://images.unsplash.com/photo-1591027265828-5678792f7c33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'
    // 'https://i.ytimg.com/vi/nlYlNF30bVg/hqdefault.jpg',
    // 'https://www.askideas.com/media/10/Funny-Goat-Closeup-Pouting-Face.jpg'
  ];
  showContainer = false;
  toggleClassChange = false;
  constructor() { }

  ngOnInit(): void {
    // document.addEventListener('fullscreenchange', (event) => {
    //   // document.fullscreenElement will point to the element that
    //   // is in fullscreen mode if there is one. If there isn't one,
    //   // the value of the property is null.
    //   if (document.fullscreenElement) {
    //     console.log(`Element: ${document.fullscreenElement.id} entered full-screen mode.`);
    //   } else {
    //     console.log('Leaving full-screen mode.');
    //     this.toggleClassChange = false;
    //     setTimeout(()=>{
    //      this.showContainer = false;
    //     },2);

    //   }
    // });
  }



handleEvent(event: CustomEvent) {
  console.log(`${event.name} has been click on img ${event.imageIndex + 1}`);

  switch (event.name) {
    case 'exit':
      this.showContainer = false;
      console.log('exit logic');
      break;
  }
}

openInFullScreen(){
  this.showContainer = true;
  // this.toggleClassChange = true;
  // setTimeout(()=>openInFullScreen{
  //   const element = document.querySelector('ngx-image-viewer');
  //   this.makeElementFullScreen(element);
  // }, 2);

}
// makeElementFullScreen(element){
//   const document: any = window.document;
//   const fullScreen = document.fullscreenElement ||
//   document.webkitFullscreenElement ||
//   document.mozFullScreenElement;
//   if (!fullScreen) {
//     // can use exitFullscreen
//     if (element.requestFullscreen) {
//       element.requestFullscreen();
//     } else if (element.mozRequestFullScreen) { /* Firefox */
//       element.mozRequestFullScreen();
//     } else if (element.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
//       element.webkitRequestFullscreen();
//     } else if (element.msRequestFullscreen) { /* IE/Edge */
//       element.msRequestFullscreen();
//     }
//   }


// }

}
