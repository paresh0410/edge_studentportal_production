import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseCardV2Component } from './course-card-v2/course-card-v2.component';
import { CourseFilterComponent } from './course-filter/course-filter.component';
import { CourseCardSkeletonComponent } from './course-card-skeleton/course-card-skeleton.component';
import { InfiniteScrollComponent } from './infinite-scroll/infinite-scroll.component';
import { NoDataFoundComponent } from './no-data-found/no-data-found.component';
import { CourseCardWorkflowComponent } from './course-card-workflow/course-card-workflow.component';
import { NbSelectModule } from '@nebular/theme';
import { MatButtonModule } from '@angular/material/button';
// import { DemoMaterialModule } from '../pages/material-module';
import { CourseDetailsSkeletonComponent } from './course-details-skeleton/course-details-skeleton.component';
import { TruncateModule } from 'ng2-truncate';
import { StepperComponent } from './stepper/stepper.component';

// import { FooterComponent } from '../@theme/components/footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    NbSelectModule,
    MatButtonModule,
    TruncateModule,
    // DemoMaterialModule,
  ],
  declarations: [
    CourseCardV2Component,
    CourseFilterComponent,
    CourseCardSkeletonComponent,
    InfiniteScrollComponent,
    NoDataFoundComponent,
    CourseCardWorkflowComponent,
    CourseDetailsSkeletonComponent,
    StepperComponent,
    // FooterComponent,
  ],
  exports: [
    CourseCardV2Component,
    CourseFilterComponent,
    CourseCardSkeletonComponent,
    NoDataFoundComponent,
    CourseCardWorkflowComponent,
    CourseDetailsSkeletonComponent,
    StepperComponent,
    // FooterComponent,
  ],
  entryComponents: [
    CourseCardV2Component,
    CourseFilterComponent,
    CourseCardSkeletonComponent,
    NoDataFoundComponent,
    CourseCardWorkflowComponent,
    CourseDetailsSkeletonComponent,
    StepperComponent,
    // FooterComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ComponentModule { }
