import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-course-card-workflow',
  templateUrl: './course-card-workflow.component.html',
  styleUrls: ['./course-card-workflow.component.scss'],
})
export class CourseCardWorkflowComponent implements OnInit {

  @Input() cardsData: any;
  @Output() getEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
