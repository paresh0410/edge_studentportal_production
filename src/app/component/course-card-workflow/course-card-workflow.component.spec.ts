import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCardWorkflowComponent } from './course-card-workflow.component';

describe('CourseCardWorkflowComponent', () => {
  let component: CourseCardWorkflowComponent;
  let fixture: ComponentFixture<CourseCardWorkflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCardWorkflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCardWorkflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
